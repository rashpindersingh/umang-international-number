//
//  HomeFilterResultsVC.m
//  Umang
//
//  Created by admin on 22/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "HomeFilterResultsVC.h"


#import "ServiceCollectionViewCell.h"
#import "HomeContainerCell.h"
#import "HomeContainerCellView.h"
#import "NearMeTabVC.h"
#import "SDCapsuleButton.h"

#import "HCSStarRatingView.h"

#define fDeviceWidth ([UIScreen mainScreen].bounds.size.width)
#define fDeviceHeight ([UIScreen mainScreen].bounds.size.height)
#define DEFAULT_BLUE [UIColor colorWithRed:12.0/255.0 green:118.0/255.0 blue:157.0/255.0 alpha:1.0]


#import "HomeDetailVC.h"

#import "ShowMoreServiceVC.h"


#import "UIImageView+WebCache.h"


#import "HomeTabVC.h"
#import "FilterServicesBO.h"


@interface HomeFilterResultsVC ()
{
    SharedManager *singleton;
    NSMutableArray *arrStateNames;
    
    
    
}
@property (nonatomic, strong) HCSStarRatingView *starRatingView;


@property (strong, nonatomic) NSMutableArray *sampleData;
@property (strong, nonatomic)IBOutlet UITableView *tableView;


@end

@implementation HomeFilterResultsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.title = NSLocalizedString(@"Master", @"Master");
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    singleton = [SharedManager sharedSingleton];
    
    
    vw_noServiceFound.hidden=TRUE;
    // [self hitStateWiseAPI];//apply filter using state id its in gcmid in login
    
    
    noResultsVW = [[FilterNoResultView alloc] initWithFrame:CGRectMake(0, 65, fDeviceWidth, fDeviceHeight - 65)];
    [noResultsVW setFilterBtnTitle];
    [self.view addSubview:noResultsVW];
    [noResultsVW.btnEditFilter addTarget:self action:@selector(btnSettingAgainClicked:) forControlEvents:UIControlEventTouchUpInside];
    noResultsVW.hidden = true;
    [vw_noServiceFound removeFromSuperview];
    
    self.navigationController.navigationBar.hidden = YES;
    
    [self addFilterOptionsAtNavigationBar];
    arrStateNames = [NSMutableArray new];
    [arrStateNames addObject:NSLocalizedString(@"all", nil)];
    
    
    // Register the table cell
    [self.tableView registerClass:[HomeContainerCell class] forCellReuseIdentifier:@"HomeContainerCell"];
    
    // Add observer that will allow the nested collection cell to trigger the view controller select row at index path
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectItemFromCollectionView:) name:@"didSelectItemFromCollectionView" object:nil];
    self.view.backgroundColor=[UIColor colorWithRed:248/255.0 green:246/255.0 blue:247/255.0 alpha:1.0];
    [self.tableView setShowsHorizontalScrollIndicator:NO];
    [self.tableView setShowsVerticalScrollIndicator:NO];
    
    
    // Do any additional setup after loading the view, typically from a nib.
    
    lbl_noservicefound.text = [NSLocalizedString(@"no_result_found", nil) capitalizedString];
    
    //[self LoadfilterResults];
    
}


-(void)LoadfilterResults
{
    //[self fetchDatafromDB];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [self setNeedsStatusBarAppearanceUpdate];
    
    
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    [self setViewFont];
    [super viewWillAppear:NO];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    lbl_noservicefound.font = [AppFont regularFont:14];
    
}




- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didSelectItemFromCollectionView" object:nil];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sampleData count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"HomeContainerCell"];
    
    HomeContainerCell *cell=[tbl_homeresult cellForRowAtIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[HomeContainerCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // ORGContainerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ORGContainerCell"];
    
    NSLog(@"[indexPath section]=%ld",(long)[indexPath section]);
    NSLog(@"indexPath.row=%ld",(long)indexPath.row);
    
    if ([indexPath section]==0 )
    {
        self.view.backgroundColor = [UIColor colorWithRed:248.0/255.0f green:246.0/255.0f blue:247.0/255.0f alpha:1.0f];
        tableView.backgroundColor = [UIColor clearColor];
    }
    
    else
    {
        
        if ([self.sampleData count]==0)
        {
            //do nothing
            
        }
        else
        {
            
            NSDictionary *cellData = [self.sampleData objectAtIndex:[indexPath section]];
            NSArray *AllServicesData = [cellData objectForKey:@"SECTION_SERVICES"];
            //  NSLog(@"AadharCardData=%@",AadharCardData);
            
            [cell setCollectionData:AllServicesData];
            
            self.view.backgroundColor = [UIColor colorWithRed:248.0/255.0f green:246.0/255.0f blue:247.0/255.0f alpha:1.0f];
            tableView.backgroundColor = [UIColor clearColor];
        }
    }
    return cell;
}


/*- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
 if ([indexPath section ] == 0){
 return CGSizeMake(320, 100);
 }else
 return CGSizeMake(320, 180);
 
 }*/


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // This code is commented out in order to allow users to click on the collection view cells.
    //    if (!self.detailViewController) {
    //        self.detailViewController = [[ORGDetailViewController alloc] initWithNibName:@"ORGDetailViewController" bundle:nil];
    //    }
    //    NSDate *object = _objects[indexPath.row];
    //    self.detailViewController.detailItem = object;
    //    [self.navigationController pushViewController:self.detailViewController animated:YES];
}

#pragma mark UITableViewDelegate methods

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    NSDictionary *sectionData = [self.sampleData objectAtIndex:section];
//    NSString *header = [sectionData objectForKey:@"category"];
//    return header;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 0){
        return 0.001;
        
    }
    else
        return 0.001;
}

/*- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
 {
 if (section == 0){
 //do all the stuff here for CellA
 
 return 0.0001;
 
 
 }
 
 else
 return 48.0;
 }
 - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
 {
 //return 180.0;
 if([indexPath section]==0)
 {
 
 return 0;
 
 }
 
 else
 return 155;
 }
 
 */

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0){
        //do all the stuff here for CellA
        
        return 0.001;
        
        
    }
    else
        if ([[UIScreen mainScreen]bounds].size.height == 1024) {
            return 68.0;
        }
        else
        {
            return 40.0;
        }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //return 180.0;
    if([indexPath section]==0)
    {
        
        return 0;
        
    }
    
    else
        if ([[UIScreen mainScreen]bounds].size.height >= 1024) {
            return 230;
        }
    return 155;
}




- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section

{
    
    return nil;
    
}










-(void)fetchDatafromDB
{
    
    
    NSArray *arrServiceSection=[singleton.dbManager loadDataServiceSection];
    NSLog(@"arrServiceSection=%@",arrServiceSection);
    NSArray *arrServiceData=[singleton.dbManager loadDataServiceData];
    NSLog(@"arrServiceData=%@",arrServiceData);
    
    
    NSMutableArray *arr_serviceWithSection=[[NSMutableArray alloc]init];
    
    self.sampleData=[[NSMutableArray alloc]init];
    
    
    //do old stuff
    for (int i=0; i<[arrServiceSection count]; i++)
    {
        NSArray *itemsService = [[[arrServiceSection objectAtIndex:i]valueForKey:@"SECTION_SERVICES"] componentsSeparatedByString:@","];
        NSLog(@"itemsService=%@",itemsService);
        
        NSMutableArray *section_service=[[NSMutableArray alloc]init];
        
        for (int j=0; j<[itemsService count]; j++)
        {
            NSArray *sectionserviceitem=[singleton.dbManager getServiceData:[itemsService objectAtIndex:j]];
            NSLog(@"sectionserviceitem=%@",sectionserviceitem);
            if ([sectionserviceitem count]!=0) {
                [section_service addObject:[sectionserviceitem objectAtIndex:0]];
                
            }
        }
        
        NSString *SECTION_IMAGE=  [[arrServiceSection objectAtIndex:i]valueForKey:@"SECTION_IMAGE"];
        NSString  *SECTION_NAME= [[arrServiceSection objectAtIndex:i]valueForKey:@"SECTION_NAME"];
        NSString  *ID =[[arrServiceSection objectAtIndex:i]valueForKey:@"ID"];
        
        NSLog(@"section_service=%@",section_service);
        NSDictionary *dic_temp = [NSDictionary dictionaryWithObjectsAndKeys:section_service,@"SECTION_SERVICES",SECTION_IMAGE,@"SECTION_IMAGE",SECTION_NAME,@"SECTION_NAME",ID,@"ID", nil];
        [arr_serviceWithSection addObject:dic_temp];
    }
    NSLog(@"arr_serviceWithSection=%@",arr_serviceWithSection);
    
    //self.sampleData = [singleton.fav_Mutable_Arr mutableCopy];
    
    //self.sampleData=[arr_serviceWithSection mutableCopy];
    
    [self.sampleData addObject:@"banner"];
    
    
    for (int i=0; i<[arr_serviceWithSection count]; i++) {
        [self.sampleData addObject:[arr_serviceWithSection objectAtIndex:i]];
        
    }
    NSLog(@"self.sampleData=%@",self.sampleData);
    
    
    
    [tbl_homeresult reloadData];
    
}
/*
 - (NSInteger)numberOfRowsInSection:(NSInteger)section{
 return 1;
 }
 */

- (void)reloadRow0Section0 {
    [tbl_homeresult reloadSections:[[NSIndexSet alloc] initWithIndex:0] withRowAnimation:NO];
    
}



#pragma mark UITableViewDelegate methods
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    //Headerview
    
    
    // NSLog(@"viewForHeaderInSection =%d",[self.sampleData count]);
    if (section == 0)
    {
        //do all the stuff here for Cell banner
        
    }else{
        
        if ([self.sampleData count]>1)
        {
            
            // NSLog(@"viewForHeaderInSection inside =%d",[self.sampleData count]);
            
            
            UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(2.0, 0.0, 315.0, 48.0)];
            
            
            
            
            //---------- Footer View of header line-----------
            UIView *headerFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, myView.frame.size.height-1,fDeviceWidth,0.5)];
            //[myView addSubview:headerFooterView];
            
            
            
            
            
            
            NSDictionary *sectionData = (NSDictionary*)[self.sampleData objectAtIndex:section];
            
            //--------- Adding Category Icon------------------------------------------
            UIImageView *icon_category = [[UIImageView alloc] initWithFrame:CGRectMake(10, 20, 25, 25)];
            
            NSString* category_icon=[sectionData objectForKey:@"SECTION_IMAGE"];
            
            
            NSURL *url=[NSURL URLWithString:category_icon];
            
            [icon_category sd_setImageWithURL:url
                             placeholderImage:[UIImage imageNamed:@"icon_recent.png"]];
            
            
            NSLog(@"SECTION");
            
            [myView addSubview:icon_category];
            
            //--------- Adding Category Label------------------------------------------
            
            UILabel *labelHeader = [[UILabel alloc] initWithFrame:CGRectMake(40, 16, 180, 30)];
            NSString *header = [sectionData objectForKey:@"SECTION_NAME"];
            if(header.length>0)
            {
                header = [NSString stringWithFormat:@"%@%@",[[header substringToIndex:1] uppercaseString],[header substringFromIndex:1] ];
            }
            labelHeader.text = header;
            labelHeader.font = [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
            
            labelHeader.textColor = [UIColor blackColor];
            
            // myView.backgroundColor=[UIColor whiteColor];
            [myView addSubview:labelHeader];
            
            
            // headerFooterView.backgroundColor=[UIColor colorWithRed:248.0/255.0 green:248.0/255.0 blue:248.0/255.0 alpha:1.0];
            // headerTopView.backgroundColor=[UIColor colorWithRed:248.0/255.0 green:248.0/255.0 blue:248.0/255.0 alpha:1.0];
            // myView.backgroundColor=[UIColor colorWithRed:248.0/255.0 green:248.0/255.0 blue:248.0/255.0 alpha:1.0];
            
            
            
            if (singleton.isArabicSelected) {
                icon_category.frame = CGRectMake(fDeviceWidth - 35, 20, 25, 25);
                labelHeader.frame =  CGRectMake(10, 16, icon_category.frame.origin.x - 10, 30);
                labelHeader.textAlignment = NSTextAlignmentRight;
            }
            else{
                icon_category.frame = CGRectMake(10, 20, 25, 25);
                labelHeader.frame =  CGRectMake(40, 16, 180, 30);
                
                labelHeader.textAlignment = NSTextAlignmentLeft;
                
            }
            
            myView.backgroundColor= [UIColor colorWithRed:248.0/255.0 green:246.0/255.0 blue:247.0/255.0 alpha:1.0];
            
            headerFooterView.backgroundColor=  [UIColor colorWithRed:248.0/255.0 green:246.0/255.0 blue:247.0/255.0 alpha:1.0];
            
            return myView;
            
        }
        else
        {
            NSLog(@"viewForHeaderInSection inside Fetch =%lu",(unsigned long)[self.sampleData count]);
            
            // [self fetchDatafromDB];// show here your retry options
        }
        
    }
    return nil;
}



#pragma mark - NSNotification to select table cell


#pragma mark - NSNotification to select table cell

- (void) didSelectItemFromCollectionView:(NSNotification *)notification
{
    NSDictionary *cellData = [notification object];
    if (cellData)
    {
        
        
        HomeDetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
        [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        
        vc.dic_serviceInfo=cellData ;
        
        vc.tagComeFrom=@"OTHERS";
        
        [self presentViewController:vc animated:NO completion:nil];
        
        
    }
}

//-----------for filterview show----------
-(IBAction)btn_filterAction:(id)sender
{
    //code to open filter view here
    
    NSLog(@"Filter Pressed");
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    
    
    for (UIViewController *aViewController in allViewControllers)
    {
        if (_isFromStateFilter) {
            if ([aViewController isKindOfClass:[NearMeTabVC class]])
            {
                // [aViewController removeFromParentViewController];
                [self.navigationController popToViewController:aViewController
                                                      animated:YES];
                
            }
        }
        else if ([aViewController isKindOfClass:[HomeTabVC class]])
        {
            // [aViewController removeFromParentViewController];
            [self.navigationController popToViewController:aViewController
                                                  animated:YES];
            
        }
        
        
    }
    
    
}

- (IBAction)btnSettingAgainClicked:(id)sender
{
    
    if ([self.delegate respondsToSelector:@selector(filterChanged:andCategoryDict:)])
    {
        
        for (int i = 0; i< self.filterBOArray.count; i++)
        {
            FilterServicesBO *objBo = [self.filterBOArray objectAtIndex:i];
            
            if ([[self.dictFilterParams objectForKey:@"category_type"] containsObject:objBo.serviceName])
            {
                objBo.isServiceSelected = YES;
            }
            else
            {
                objBo.isServiceSelected = NO;
            }
        }
        
        
        NSMutableDictionary *categoryDict = [NSMutableDictionary new];
        
        if ([self.dictFilterParams valueForKey:@"state_name"] != nil)
        {
            [categoryDict setValue:[self.dictFilterParams valueForKey:@"state_name"] forKey:@"selectedStates"];
            
        }
        
        if ([self.dictFilterParams valueForKey:@"service_type"] != nil)
        {
            [categoryDict setValue:[self.dictFilterParams valueForKey:@"service_type"] forKey:@"selectedCategory"];
        }
        
        [self.delegate filterChanged:self.filterBOArray andCategoryDict:categoryDict];
    }
    [self.navigationController popViewControllerAnimated:YES];
}


-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}
-(void)addFilterOptionsAtNavigationBar
{
    
    //Remove existing button
    
    [[self.scrollView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    // First fetch State Name
    
    
    NSMutableArray *arrStates = [self.dictFilterParams objectForKey:@"state_name"];
    
    CGFloat xCord = 10;
    CGFloat yCord = 0;
    CGFloat padding = 5;
    CGFloat itemHeight = 30.0;
    NSString *_selectedNotificationType = [self.dictFilterParams objectForKey:@"service_type"];
    if (_selectedNotificationType.length)
    {
        CGRect frame = [self rectForText:_selectedNotificationType usingFont:TITLE_FONT boundedBySize:CGSizeMake(1000.0, itemHeight)];
        frame.size.width = frame.size.width + 30;
        SDCapsuleButton *btn = [[SDCapsuleButton alloc] initWithFrameWithFullBorder:CGRectMake(xCord, yCord, frame.size.width, itemHeight)];
        [btn setBtnTitle:_selectedNotificationType];
        [btn addTarget:self action:@selector(btnCapsuleServiceTypeClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn.btnCross addTarget:self action:@selector(btnCrossClicked:) forControlEvents:UIControlEventTouchUpInside];
        if ([_selectedNotificationType isEqualToString:NSLocalizedString(@"all", nil)])
        {
            btn.btnCross.hidden = YES;
            btn.userInteractionEnabled = NO;
        }
        else
        {
            btn.btnCross.hidden = NO;
        }
        [self.scrollView addSubview:btn];
        xCord+=frame.size.width + padding;
    }
    
    
    for (int i = 0; i < arrStates.count; i++)
    {
        
        NSString *stateName = arrStates[i];
        
        
        CGRect frame = [self rectForText:stateName usingFont:TITLE_FONT boundedBySize:CGSizeMake(1000.0, itemHeight)];
        frame.size.width = frame.size.width + 30;
        
        SDCapsuleButton *btn = [[SDCapsuleButton alloc] initWithFrameWithFullBorder:CGRectMake(xCord, yCord, frame.size.width, itemHeight)];
        [btn setBtnTitle:stateName];
        // [btn set]
        [btn addTarget:self action:@selector(btnCapsuleStateClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn.btnCross addTarget:self action:@selector(btnCrossClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([stateName isEqualToString:NSLocalizedString(@"all", nil)])
        {
            btn.btnCross.hidden = YES;
            btn.userInteractionEnabled = NO;
        }
        else
        {
            btn.btnCross.hidden = NO;
        }
        
        
        //  btn.btnCross.
        btn.tag = 2300+i;
        [self.scrollView addSubview:btn];
        xCord+=frame.size.width + padding;
    }
    
    
    // Now fetch Categories
    
    
    NSMutableArray *arrCategories = [self.dictFilterParams objectForKey:@"category_type"];
    
    for (int i = 0; i<arrCategories.count; i++) {
        NSString *serviceName = arrCategories[i];
        
        
        CGRect frame = [self rectForText:serviceName usingFont:TITLE_FONT boundedBySize:CGSizeMake(1000.0, itemHeight)];
        frame.size.width = frame.size.width + 30;
        
        SDCapsuleButton *btn = [[SDCapsuleButton alloc] initWithFrameWithFullBorder:CGRectMake(xCord, yCord, frame.size.width, itemHeight)];
        [btn setBtnTitle:serviceName];
        [btn addTarget:self action:@selector(btnCapsuleCategoryClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn.btnCross addTarget:self action:@selector(btnCrossClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        
        btn.tag = 2300+i;
        [self.scrollView addSubview:btn];
        xCord+=frame.size.width + padding;
    }
    
    [self.scrollView setContentSize:CGSizeMake(xCord, self.scrollView.frame.size.height)];
    
    if (self.isFromHomeFilter) {
        // called from Home Tab Vc
        [self getFilterDataForHomeScreen];
    }
    else if (self.isFromStateFilter) {
        // called from State Tab Vc
        [self getFilterDataForHomeScreen];
    }
    
}

-(void)btnCrossClicked:(SDCapsuleButton*)btnSender{
    
}


-(void)getFilterDataForHomeScreen{
    if (self.sampleData == nil) {
        self.sampleData = [[NSMutableArray alloc] init];
    }
    else{
        [self.sampleData removeAllObjects];
    }
    NSString *sortBy = [self.dictFilterParams objectForKey:@"sort_by"];
    NSString *serviceType = [self.dictFilterParams objectForKey:@"service_type"];
    NSArray *_arrStates = [self.dictFilterParams objectForKey:@"state_name"];
    
    //added here
    //added here
    //added here
    
  /*  if ([_arrStates count]==0) {
        serviceType=@"";
    }*/
    //added here
    //added here
    //added here
    
    NSMutableArray *arrSelectedCategories = [self.dictFilterParams objectForKey:@"category_type"];
    NSArray *arrFilterResponse =  [singleton.dbManager getFilteredServiceData:sortBy serviceType:serviceType stateIdAlist:_arrStates categoryList:arrSelectedCategories];
    
    //-------------
    
    
    NSMutableArray *arr_filterCategory=[[NSMutableArray alloc]init];
    
    arr_filterCategory=[arrFilterResponse valueForKey:@"SERVICE_ID"];
    
    
    
    NSLog(@"arr_filterCategory=%@",arr_filterCategory);
    
    
    
    
    
    
    NSArray *arrServiceSection=[singleton.dbManager loadDataServiceSection];
    NSLog(@"arrServiceSection=%@",arrServiceSection);
    NSArray *arrServiceData=[singleton.dbManager loadDataServiceData];
    NSLog(@"arrServiceData=%@",arrServiceData);
    
    
    NSMutableArray *arr_serviceWithSection=[[NSMutableArray alloc]init];
    
    self.sampleData=[[NSMutableArray alloc]init];
    
    @try {
        //do old stuff
        for (int i=0; i<[arrServiceSection count]; i++)
        {
            NSArray *itemsService = [[[arrServiceSection objectAtIndex:i]valueForKey:@"SECTION_SERVICES"] componentsSeparatedByString:@","];
            NSLog(@"itemsService=%@",itemsService);
            
            NSMutableArray *section_service=[[NSMutableArray alloc]init];
            
            for (int j=0; j<[itemsService count]; j++)
            {
                
                
                if ([arr_filterCategory containsObject: [itemsService objectAtIndex:j]]) // YES
                {
                    // Do something
                    NSArray *sectionserviceitem=[singleton.dbManager getServiceData:[itemsService objectAtIndex:j]];
                    
                    
                    
                    NSLog(@"sectionserviceitem=%@",sectionserviceitem);
                    if ([sectionserviceitem count]!=0) {
                        [section_service addObject:[sectionserviceitem objectAtIndex:0]];
                        
                    }
                    
                }
                
                else
                {
                    //do nothing
                }
                
                
            }
            
            if([section_service count]!=0)
            {
                if ([sortBy isEqualToString:NSLocalizedString(@"alphabetic", nil)])
                {
                    NSSortDescriptor *aphabeticDescriptor = [[NSSortDescriptor alloc] initWithKey:@"SERVICE_NAME" ascending:YES selector:@selector(caseInsensitiveCompare:)];
                    NSArray *sortDescriptors = [NSArray arrayWithObject:aphabeticDescriptor];
                    section_service = [[section_service sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
                }
                
                if ([sortBy isEqualToString:NSLocalizedString(@"most_popular", nil)])
                {
                    NSSortDescriptor *aphabeticDescriptor = [[NSSortDescriptor alloc] initWithKey:@"SERVICE_POPULARITY" ascending:NO];
                    NSArray *sortDescriptors = [NSArray arrayWithObject:aphabeticDescriptor];
                    section_service = [[section_service sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
                }
                
                
                if ([sortBy isEqualToString:NSLocalizedString(@"top_rated", nil)])
                {
                    NSSortDescriptor *aphabeticDescriptor = [[NSSortDescriptor alloc] initWithKey:@"SERVICE_RATING" ascending:NO];
                    NSArray *sortDescriptors = [NSArray arrayWithObject:aphabeticDescriptor];
                    section_service = [[section_service sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
                }
                
                
                
                
                NSString *SECTION_IMAGE=  [[arrServiceSection objectAtIndex:i]valueForKey:@"SECTION_IMAGE"];
                NSString  *SECTION_NAME= [[arrServiceSection objectAtIndex:i]valueForKey:@"SECTION_NAME"];
                NSString  *ID =[[arrServiceSection objectAtIndex:i]valueForKey:@"ID"];
                
                NSLog(@"section_service=%@",section_service);
                NSDictionary *dic_temp = [NSDictionary dictionaryWithObjectsAndKeys:section_service,@"SECTION_SERVICES",SECTION_IMAGE,@"SECTION_IMAGE",SECTION_NAME,@"SECTION_NAME",ID,@"ID", nil];
                [arr_serviceWithSection addObject:dic_temp];
            }
            else
            {
                //its empty service list
            }
        }
        NSLog(@"arr_serviceWithSection=%@",arr_serviceWithSection);
        
        //self.sampleData = [singleton.fav_Mutable_Arr mutableCopy];
        
        //self.sampleData=[arr_serviceWithSection mutableCopy];
        
        [self.sampleData addObject:@"banner"];
        
        
        for (int i=0; i<[arr_serviceWithSection count]; i++) {
            [self.sampleData addObject:[arr_serviceWithSection objectAtIndex:i]];
            
        }
        NSLog(@"self.sampleData=%@",self.sampleData);
        
        
        
        [tbl_homeresult reloadData];
        
        
        //------------------------------
        //[self.sampleData addObjectsFromArray:arrFilterResponse];
        
        if ([self.sampleData count]>1) {
            tbl_homeresult.hidden=FALSE;
            noResultsVW.hidden = true;

            
        }
        else
        {
            tbl_homeresult.hidden=TRUE;
            noResultsVW.hidden = false;

            
        }
        [tbl_homeresult reloadData];
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
}

-(void)btnCapsuleServiceTypeClicked:(SDCapsuleButton*)btnSender{
    //added here
    //added here
    //added here
    if ([[btnSender btnTitle] isEqualToString:NSLocalizedString(@"regional", nil)])
    {
        
        [self.dictFilterParams removeObjectForKey:@"service_type"];
        [self.dictFilterParams removeObjectForKey:@"state_name"];
        
    }
    else
    {
        [self.dictFilterParams removeObjectForKey:@"service_type"];
        
    }
    //added here
    //added here
    //added here
    [self addFilterOptionsAtNavigationBar];
}

-(void)btnCapsuleStateClicked:(SDCapsuleButton*)btnSender{
    
    NSLog(@"self.dictFilterParams=%@",self.dictFilterParams);
    NSMutableArray *arrStates = [self.dictFilterParams objectForKey:@"state_name"];
    [arrStates removeObject:[btnSender btnTitle]];
    //added here
    //added here
    //added here
    
    if ([arrStates count]==0) {
        [self.dictFilterParams removeObjectForKey:@"service_type"];
    }
    //added here
    //added here
    //added here
    [self.dictFilterParams setObject:arrStates forKey:@"state_name"];
    [self addFilterOptionsAtNavigationBar];
    
}


-(void)btnCapsuleCategoryClicked:(SDCapsuleButton*)btnSender{
    
    NSMutableArray *arrCategories = [self.dictFilterParams objectForKey:@"category_type"];
    
    [arrCategories removeObject:[btnSender btnTitle]];
    
    [self.dictFilterParams setObject:arrCategories forKey:@"category_type"];
    [self addFilterOptionsAtNavigationBar];
    
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation duration:(NSTimeInterval)duration
{
    [tbl_homeresult reloadData];
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/


- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

@end
