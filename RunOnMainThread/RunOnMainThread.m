//
//  RunOnMainThread.m
//  HiCast
//
//  Created by Sukhpreet Singh on 01/04/16.
//  Copyright © 2015 Seasia. All rights reserved.


#import "RunOnMainThread.h"

@implementation RunOnMainThread

/**
  *  Check if we are on the main thread or not.
  *  If not then Dispatch it on the main queue
  **/
+ (void)runBlockInMainQueueIfNecessary:(void (^)(void))block {
    if ([NSThread isMainThread]) {
        block();
    } else {
        
        dispatch_queue_t mainqueue=dispatch_get_main_queue();
        dispatch_async(mainqueue, block);
    }
}

@end
