//
//  UmangEulaVC.m
//  Umang
//
//  Created by admin on 15/02/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "UmangEulaVC.h"
#import "ViewController.h"
#import "UIView+Toast.h"

@interface UmangEulaVC ()

@end

@implementation UmangEulaVC


-(void)showToast :(NSString *)toast
{
    [self.view makeToast:toast duration:5.0 position:CSToastPositionBottom];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSUserDefaults standardUserDefaults] setInteger:kLanguageScreenCase forKey:kInitiateScreenKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self showToast:NSLocalizedString(@"you_can_change_language_txt", @"")];
    [btn_back setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btn_back.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btn_back.frame.origin.x, btn_back.frame.origin.y, btn_back.frame.size.width, btn_back.frame.size.height);
        
        [btn_back setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btn_back.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    SharedManager *singleton = [SharedManager sharedSingleton];
    
    NSString * tndcurl=[NSString stringWithFormat:@"%@",[singleton.arr_initResponse valueForKey:@"tndc"]];
    
    NSString * ppolurl=[NSString stringWithFormat:@"%@",[singleton.arr_initResponse valueForKey:@"ppol"]];
    
    NSLog(@"test ppolurl for push");
    
    
    NSString *msg = [NSString stringWithFormat:NSLocalizedString(@"eula_text1", nil),tndcurl,ppolurl];
    
    
    
    NSString *eulatextString;
    
    
    if (singleton.isArabicSelected)
    {
        eulatextString=[NSString stringWithFormat:@"<div style=\"text-align:right\"><ol><li><font face=\"sans-serif\">%@<br></li><li>%@<br></li><li>%@<br></li><li>%@<br></li><li>%@</font></li></ol></div>",msg,NSLocalizedString(@"eula_text2", nil),NSLocalizedString(@"eula_text3", nil),NSLocalizedString(@"eula_text4", nil),NSLocalizedString(@"eula_text5", nil)];
        //self.view.semanticContentAttribute = UISemanticContentAttributeForceRightToLeft;
        NSString *rtlString=[NSString stringWithFormat:@"<style>ol{margin:0 100px; padding: 0; }ol li {direction:rtl;}</style>"];
        eulatextString =[eulatextString stringByAppendingString:rtlString];
        
        //because inverted commas is showing inverted mirror type changes in it as well
        eulatextString = [eulatextString stringByReplacingOccurrencesOfString:@"”"
                                                                   withString:@"\""];
        
    }
    else
    {
        eulatextString=[NSString stringWithFormat:@"<div style=\"text-align:left\"><ol><li><font face=\"sans-serif\">%@<br></li><li>%@<br></li><li>%@<br></li><li>%@<br></li><li>%@</font></li></ol></div>",msg,NSLocalizedString(@"eula_text2", nil),NSLocalizedString(@"eula_text3", nil),NSLocalizedString(@"eula_text4", nil),NSLocalizedString(@"eula_text5", nil)];
    }
    
    NSString *htmleula = [NSString stringWithFormat:@"%@",eulatextString];
    //ignore
    /*NSString *htmleula = [NSString stringWithFormat:@"<ol><li><font face=\"sans-serif\">By accessing and using the UMANG (hereafter also referred to as UMANG, NeGD, MeitY, We, Our or LICENSOR), you as the End User (also referred to as You, Your, User or LICENSEE) agree to be bound by these Licensing Terms. You hereby give your explicit consent to access and use UMANG in accordance with these Terms along with<a href=\"%@\">Terms of Service/Use </a>& <a href=\"%@\"> Privacy Policy</a>.<br></li><li>As a LICENSEE of UMANG, You are granted a non-exclusive, non-transferable, revocable, limited license to access and use the UMANG.<br></li><li>You acknowledge and agree that the access and use of the UMANG by You is voluntary and of Your own accord. If You are aged below 18 years, please get your parent's/guardian's permission/consent before You provide information to us via UMANG.<br></li><li>You understand and agree that that the access and use of the UMANG shall not be construed to confer on You any rights of any nature including rights in any form of intellectual property contained in the services and/or UMANG or related to National e-Governance Division (NeGD)/Ministry of Electronics and Information Technology (MeitY), Government of India (LICENSOR). LICENSOR owns all right, title, and interest in and to UMANG and any Collateral Materials.<br></li><li>You acknowledge and hereby agree that Your access and use of the UMANG shall be subject to the applicable laws, rules and regulations for the time being in force in India.</font></li></ol>",tndcurl,ppolurl];*/
    
    
    
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [htmleula dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                            documentAttributes: nil
                                            error: nil
                                            ];
    
    txt_eula.attributedText = attributedString;
    
    
    [self addShadowToTheView:vw_bottom];
    [btnAccept setImage:[UIImage imageNamed:@"checkbox_outline"] forState:UIControlStateNormal];
    [btn_next setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
    
    // >
    
    NSString *nxtTitle=[NSString stringWithFormat:@"%@ >",NSLocalizedString(@"next", @"")];
    [btn_next setTitle:nxtTitle forState:UIControlStateNormal];
    lbl_iagree.text=NSLocalizedString(@"agree_to_terms_condition", @"");
    lbl_eulaHeading.text = NSLocalizedString(@"eula", nil);
    btn_next.enabled=FALSE;
    flagAccept=FALSE;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        CGRect frameLogo = img_logo.frame;
        CGRect framelblEula = lbl_eulaHeading.frame;
        CGRect frameTxtEula = txt_eula.frame;
        frameLogo.origin.y += 100;
        framelblEula.origin.y += 100;
        frameTxtEula.origin.y += 200;
        img_logo.frame = frameLogo;
        lbl_eulaHeading.frame = framelblEula;
        txt_eula.frame = frameTxtEula;
        [self.view layoutIfNeeded];
        
    }
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)addShadowToTheView:(UIView*)vwItem
{
    vwItem.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    vwItem.layer.shadowColor = [UIColor brownColor].CGColor;
    vwItem.layer.shadowRadius = 3;
    vwItem.layer.shadowOpacity = 0.5;
    vwItem.layer.cornerRadius = 3.0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)btnNextAction:(id)sender
{
    
    [[NSUserDefaults standardUserDefaults] setInteger:kTutorialScreenCase forKey:kInitiateScreenKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:vc animated:NO completion:nil];
}
-(IBAction)btnAccept:(id)sender
{
    
    if (flagAccept==FALSE)
    {
        [btnAccept setImage:[UIImage imageNamed:@"checkbox_Tick"] forState:UIControlStateNormal];
        flagAccept=TRUE;
        btn_next.enabled=TRUE;
        [btn_next setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
    }
    else
    {
        [btnAccept setImage:[UIImage imageNamed:@"checkbox_outline"] forState:UIControlStateNormal];
        btn_next.enabled=FALSE;
        [btn_next setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        
        flagAccept=FALSE;
        
    }
}
-(IBAction)btnback:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
    
    
}





-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    [txt_eula setContentOffset:CGPointZero animated:NO];
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}/*
  #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
  - (NSUInteger)supportedInterfaceOrientations
  #else
  - (UIInterfaceOrientationMask)supportedInterfaceOrientations
  #endif
  {
  // Return a bitmask of supported orientations. If you need more,
  // use bitwise or (see the commented return).
  return UIInterfaceOrientationMaskPortrait;
  // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
  }
  
  - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
  // Return the orientation you'd prefer - this is what it launches to. The
  // user can still rotate. You don't have to implement this method, in which
  // case it launches in the current orientation
  return UIInterfaceOrientationPortrait;
  }
  
  */
@end

