//
//  DeptAudioRecordVC.h
//  Umang
//
//  Created by admin on 12/06/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreAudio/CoreAudioTypes.h>
#import <AVFoundation/AVFoundation.h>
@protocol AudioBase64Delegate <NSObject>
- (void)audioBase64ReturnToDept:(NSDictionary*)parameters;

@end


@interface DeptAudioRecordVC : UIViewController
{
    IBOutlet UILabel *lbltimer;
    IBOutlet UILabel *lblAudiotimer;
    IBOutlet UIButton *audioPlayButton;
    IBOutlet UIButton *audioStopButton;
    
    __weak IBOutlet UIButton *btnDone;
    __weak IBOutlet UIButton *btnCancel;
    __weak IBOutlet UILabel *lblMaximumAudio;
    
    IBOutlet UIButton *audioRecordButton;
    IBOutlet NSLayoutConstraint *recordButtonCentre;
    
}
@property (nonatomic, retain) id<AudioBase64Delegate> delegate;

@property (strong, nonatomic) AVAudioRecorder *audioRecorder;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (strong, nonatomic) NSTimer *timer;
@property (strong, nonatomic) NSTimer *Recordtimer;
@property (assign, nonatomic) float *totalDataMB;


- (IBAction)audioRecord:(id)sender;
- (IBAction)audioStop:(id)sender;
- (IBAction)audioPlay:(id)sender;
- (IBAction)bgTouch:(id)sender;

@property (strong, nonatomic) IBOutlet UIProgressView *audioProgress;
@property (strong, nonatomic) IBOutlet UIProgressView *audioRecordProgress;

@end
