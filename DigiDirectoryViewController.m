//
//  DigiDirectoryViewController.m
//  Umang
//
//  Created by admin on 04/07/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "DigiDirectoryViewController.h"
#import "MBProgressHUD.h"
#import "SharedManager.h"
#import "UMAPIManager.h"
#import "FAQWebVC.h"
#import "Base64.h"

@interface DigiDirectoryViewController ()<UINavigationControllerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate>
{
    MBProgressHUD *hud;
    NSMutableArray *folderItems;
    SharedManager *singleton;
}

@end

@implementation DigiDirectoryViewController

- (IBAction)backButtonPressed:(UIButton *)sender
{
    [singleton traceEvents:@"Back Button" withAction:@"Clicked" withLabel:@"DigiLocker Directory View" andValue:0];

    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setViewFont];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [self.backButton.titleLabel setFont:[AppFont regularFont:17.0]];
    self.titleLabel.font = [AppFont semiBoldFont:17.0];
    _noRecordsLabel.font = [AppFont regularFont:16.0];
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [self.backButton setTitle:NSLocalizedString(@"back",nil) forState:UIControlStateNormal];
    self.titleLabel.text = self.folderNameString;
    
    singleton = [SharedManager sharedSingleton];
    folderItems = [NSMutableArray new];
    
    
    if (self.folderIdString.length != 0)
    {
        [self hitAPIToGetUploadedDocumentsWithId:self.folderIdString];
    }
    
    
    self.folderTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.noRecordsView.hidden = YES;
    self.noRecordsLabel.text  = NSLocalizedString(@"no_result_found", nil);
    
    //self.folderTableView.hidden = NO;
}


- (IBAction)uploadDocumentPressed:(UIButton *)sender
{
    [self openChooseFromSheet];
}
#pragma mark - OPEN ACTION SHEET

-(void)openChooseFromSheet
{
    //NSLog(@"openChooseFrom");
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Upload Document"
                                                             delegate:self
                                                    cancelButtonTitle:[NSLocalizedString(@"cancel", nil) capitalizedString]
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:[NSLocalizedString(@"camera", nil) capitalizedString],[NSLocalizedString(@"gallery", nil) capitalizedString], nil];
    
    actionSheet.delegate=self;
    [actionSheet showInView:self.view];
    
    
    
    
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    switch (buttonIndex) {
        case 0:
            [self openCamera];
            break;
        case 1:
            [self openGallary];
            break;
            
        default:
            break;
    }
}

-(void)openCamera
{
    [singleton traceEvents:@"Camera button" withAction:@"Clicked" withLabel:@"DigiLocker Directory View" andValue:0];

    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }
    else
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            
            
            
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                
                UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:picker];
                
                [popover presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
                
                
                
                
                
            } else {
                
                [self presentViewController:picker animated:NO completion:nil];
                
            }
            
        });
        
    }
}

-(void)openGallary
{
    [singleton traceEvents:@"Gallery button" withAction:@"Clicked" withLabel:@"DigiLocker Directory View" andValue:0];

    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        
        
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
            
            UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:picker];
            
            [popover presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
            
            
        }
        else
        {
            [self presentViewController:picker animated:NO completion:nil];
        }
        
    });
    
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *beforeCrop = [info objectForKey:UIImagePickerControllerEditedImage];
    
    
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/DigiUploadFolder"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    
    NSString *fileName = [NSString stringWithFormat:@"%@/Image-%@.jpeg",dataPath,dateString];
    
    
    NSData *imageData = UIImageJPEGRepresentation(beforeCrop, 1.0);
    
    
    NSString *base64String = [imageData base64Encoding]; //my base64Encoding function
    
    __block NSData *base64data = [[NSData alloc] initWithData:[NSData dataWithBase64EncodedString:base64String]];
    
    
    [base64data writeToFile:fileName atomically:YES];
    
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        [self hitAPIToUploadDocumentWithData:imageData andFilePath:fileName];
    }];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - UPLOAD DOCUMENT API

-(void)hitAPIToUploadDocumentWithData:(NSData *)baseData andFilePath:(NSString *)filePath
{
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    double CurrentTime = CACurrentMediaTime();
    
    NSString * timeInMS = [NSString stringWithFormat:@"%f", CurrentTime];
    
    timeInMS=  [timeInMS stringByReplacingOccurrencesOfString:@"." withString:@""];
    
    
    NSString *accessTokenString = [[NSUserDefaults standardUserDefaults]valueForKey:@"AccessTokenDigi"];
    
    
    
    NSString *fileName = [[filePath componentsSeparatedByString:@"/"] lastObject];
    
    
    NSLog(@"%@",self.navigationController.viewControllers);
    
    __block NSString *multipleDirectoryString = @"";
    [self.navigationController.viewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
       
        if ([obj isKindOfClass:[DigiDirectoryViewController class]])
        {
            DigiDirectoryViewController *directoryView = obj;
            
            NSString *hierarchyString = [NSString stringWithFormat:@"/%@",directoryView.titleLabel.text];
            multipleDirectoryString = [multipleDirectoryString stringByAppendingString:hierarchyString];
        }
        
    }];
    
    NSString *path = [NSString stringWithFormat:@"%@/%@",multipleDirectoryString,fileName];
    
    [dictBody setObject:timeInMS forKey:@"trkr"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:accessTokenString forKey:@"utkn"];
    [dictBody setObject:[NSString stringWithFormat:@"%lu",baseData.length] forKey:@"clength"];
    [dictBody setObject:path forKey:@"path"];
    [dictBody setObject:@"image/jpeg" forKey:@"ctype"];
    [dictBody setObject:baseData forKey:@"file"];
    
    NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if (selectedLanguage == nil)
    {
        selectedLanguage = @"en";
    }
    else{
        NSArray *arrTemp = [selectedLanguage componentsSeparatedByString:@"-"];
        selectedLanguage = [arrTemp firstObject];
    }
    
    NSLog(@"Applied Selected Language = %@",selectedLanguage);
    
    
    [dictBody setObject:selectedLanguage forKey:@"lang"];
    
    [dictBody setObject:@"app" forKey:@"mod"];
    
    [dictBody setObject:[singleton appUniqueID] forKey:@"did"];
    
    
    NSLog(@"Dictionary is %@",dictBody);
    
    
    [objRequest hitAPIForDigiLockerUploadFileWithPost:YES isAccessTokenRequired:YES webServiceURL:UM_API_UPLOAD_DOC withBody:dictBody andTag:TAG_REQUEST_DIGILOCKER_UPLOADDOC andFilePath:filePath completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        [hud hideAnimated:YES];
        NSLog(@"%@",response);
        
        
        
        if (error == nil)
        {
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"uploaded_successfully", nil) preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                               {
                                                   if (self.folderIdString.length != 0)
                                                   {
                                                       [self hitAPIToGetUploadedDocumentsWithId:self.folderIdString];
                                                   }
                                                   
                                                   
                                               }];
                [alert addAction:cancelAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
        else
        {
            
            
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"error", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                        {
                                            [self.navigationController popViewControllerAnimated:YES];
                                        }];
            
            
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            
        }
        
    }];
}

#pragma mark - API To Upload Document

-(void)hitAPIToGetUploadedDocumentsWithId:(NSString *)idString
{
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    double CurrentTime = CACurrentMediaTime();
    
    NSString * timeInMS = [NSString stringWithFormat:@"%f", CurrentTime];
    
    timeInMS=  [timeInMS stringByReplacingOccurrencesOfString:@"." withString:@""];
    
    
    
    
    NSString *accessTokenString = [[NSUserDefaults standardUserDefaults]valueForKey:@"AccessTokenDigi"];
    
    [dictBody setObject:timeInMS forKey:@"trkr"];
    [dictBody setObject:@"rgtadhr" forKey:@"ort"];
    [dictBody setObject:@"Y" forKey:@"rc"];
    [dictBody setObject:@"Y" forKey:@"mec"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:accessTokenString forKey:@"utkn"];
    [dictBody setObject:idString forKey:@"id"];
    
    NSLog(@"Dictionary is %@",dictBody);
    
    
    [objRequest hitAPIForDigiLockerAuthenticationWithPost:YES isAccessTokenRequired:YES webServiceURL:UM_API_GETUPLOADED_DOC withBody:dictBody andTag:TAG_REQUEST_DIGILOCKER_GETTOKEN completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        
        [hud hideAnimated:YES];
        // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
        
        
        if (error == nil)
        {
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                
                folderItems = [[response valueForKey:@"pd"] valueForKey:@"items"];
                
                if (folderItems.count == 0)
                {
                    self.noRecordsView.hidden = NO;
                    //self.folderTableView.hidden = YES;
                    
                    [self.view bringSubviewToFront:self.noRecordsView];
                }
                else
                {
                    self.noRecordsView.hidden = YES;
                    //self.folderTableView.hidden = NO;
                }
                
                [self.folderTableView reloadData];
            }
            
        }
        else
        {
            
            
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"error", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                        {
                                            [self.navigationController popViewControllerAnimated:YES];
                                        }];
            
            
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            
        }
        
    }];
    
}

#pragma mark - UITableView Delegate and DataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return folderItems.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 50.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FolderCell *cell = (FolderCell *)[tableView dequeueReusableCellWithIdentifier:@"FolderCellIdentifier"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    
    cell.folderNameLabel.text = [[folderItems objectAtIndex:indexPath.row] valueForKey:@"name"];
    cell.folderDateLabel.text = [NSString stringWithFormat:@"%@",[[[[folderItems objectAtIndex:indexPath.row] valueForKey:@"date"] componentsSeparatedByString:@"T"] objectAtIndex:0]];
    
    
    if ([[[folderItems objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"dir"])
    {
        cell.folderImageView.image = [UIImage imageNamed:@"adhr_icon_folder.png"];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else if ([[[folderItems objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"file"])
    {
        
        if ([[[[[folderItems objectAtIndex:indexPath.row] valueForKey:@"mime"] componentsSeparatedByString:@"/"] objectAtIndex:1] isEqualToString:@"jpeg"])
        {
            cell.folderImageView.image = [UIImage imageNamed:@"adhr_icon_jpg.png"];
        }
        else if ([[[[[folderItems objectAtIndex:indexPath.row] valueForKey:@"mime"] componentsSeparatedByString:@"/"] objectAtIndex:1] isEqualToString:@"png"])
        {
            cell.folderImageView.image = [UIImage imageNamed:@"adhr_icon_png.png"];
        }
        else if ([[[[[folderItems objectAtIndex:indexPath.row] valueForKey:@"mime"] componentsSeparatedByString:@"/"] objectAtIndex:1] isEqualToString:@"pdf"])
        {
            cell.folderImageView.image = [UIImage imageNamed:@"adhr_icon_pdf.png"];
            
        }
        
        
        UIButton *customAccessoryButton = [UIButton buttonWithType:UIButtonTypeCustom];
        customAccessoryButton.frame = CGRectMake(0, 0, 24, 24);
        [customAccessoryButton setImage:[UIImage imageNamed:@"download_icon_digi"] forState:UIControlStateNormal];
        
        
        cell.accessoryView = customAccessoryButton;
        
    }
    cell.folderNameLabel.font = [AppFont regularFont:16.0];
    cell.folderDateLabel.font = [AppFont regularFont:14.0];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([[[folderItems objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"dir"])
    {
        NSString *idString = [NSString stringWithFormat:@"%@",[[folderItems objectAtIndex:indexPath.row] valueForKey:@"id"]];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        
        DigiDirectoryViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"DigiDirectoryVC"];
        vc.folderIdString = idString;
        vc.folderNameString = [[folderItems objectAtIndex:indexPath.row] valueForKey:@"name"];
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    else
    {
        NSString *url = [NSString stringWithFormat:@"%@",[[folderItems objectAtIndex:indexPath.row] valueForKey:@"uri"]];
        
        NSString *fileName = [NSString stringWithFormat:@"%@",[[folderItems objectAtIndex:indexPath.row] valueForKey:@"name"]];
        
        
        [self hitAPIToDownloadDocumentWithURL:url withfileName:fileName];
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
}


#pragma mark - API To Download File

-(void)hitAPIToDownloadDocumentWithURL:(NSString *)url withfileName:(NSString*)fileName
{
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    double CurrentTime = CACurrentMediaTime();
    
    NSString * timeInMS = [NSString stringWithFormat:@"%f", CurrentTime];
    
    timeInMS=  [timeInMS stringByReplacingOccurrencesOfString:@"." withString:@""];
    
    
    
    NSString *accessTokenString = [[NSUserDefaults standardUserDefaults]valueForKey:@"AccessTokenDigi"];
    
    [dictBody setObject:timeInMS forKey:@"trkr"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:accessTokenString forKey:@"utkn"];
    [dictBody setObject:url forKey:@"uri"];
    
    [dictBody setObject:[singleton appUniqueID] forKey:@"did"];
    
    NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    
    if (selectedLanguage == nil)
    {
        selectedLanguage = @"en";
    }
    else
    {
        NSArray *arrTemp = [selectedLanguage componentsSeparatedByString:@"-"];
        selectedLanguage = [arrTemp firstObject];
    }
    
    NSLog(@"Applied Selected Language = %@",selectedLanguage);
    
    
    [dictBody setObject:selectedLanguage forKey:@"lang"];
    
    
    [dictBody setObject:@"app" forKey:@"mod"];
    
    NSLog(@"Dictionary is %@",dictBody);
    
    
    [objRequest hitAPIForDigiLockerDownloadFileWithPost:YES isAccessTokenRequired:YES webServiceURL:UM_API_DOWNLOAD_DOC withBody:dictBody andTag:TAG_REQUEST_DIGILOCKER_DOWNLOADDOC completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
     {
         
         
         [hud hideAnimated:YES];
         // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
         
         
         if (error == nil)
         {
             
             
             NSData *pdfDocumentData=(NSData*)response;
             //
             NSString *mimeType = [self mimeTypeForData:pdfDocumentData]; // how would I implement getMimeType
             
             NSError *error;
             NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
             NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
             NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/DigiFolder"];
             
             if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
                 [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
             
             
             
             
             NSString *selectedFileName;
             
             if ([fileName containsString:@"jpeg"] || [fileName containsString:@"pdf"] || [fileName containsString:@"png"])
             {
                 selectedFileName = fileName;
             }
             else
             {
                 
                 selectedFileName = [NSString stringWithFormat:@"%@.%@",fileName,[[mimeType componentsSeparatedByString:@"/"] objectAtIndex:1]];
             }
             
             
             NSString *documentPath = [NSString stringWithFormat:@"%@/%@", dataPath, selectedFileName];
             
             //NSData *anImageData = UIImagePNGRepresentation(anImage);
             
             
             
             
             NSString *base64String = [pdfDocumentData base64Encoding]; //my base64Encoding function
             
             NSData *base64data = [[NSData alloc] initWithData:[NSData dataWithBase64EncodedString:base64String]];
             
             [self openPDFfile:base64data withfilename:fileName withType:mimeType];
             
             
             [base64data writeToFile:documentPath atomically:YES];
             
             
             NSLog(@"mimeType=%@",mimeType);
             //NSLog(@"pdfDocumentData=%@",pdfDocumentData);
             
             
         }
         else
         {
             
             
             UIAlertController * alert=[UIAlertController alertControllerWithTitle:NSLocalizedString(@"error", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction* yesButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                         {
                                             [self.navigationController popViewControllerAnimated:YES];
                                         }];
             
             
             [alert addAction:yesButton];
             
             [self presentViewController:alert animated:YES completion:nil];
             
             
         }
         
     }];
    
}


-(void)openPDFfile:(NSData*)pdfDocumentData withfilename:(NSString*)name withType:(NSString*)type

{
    
    //NSLog(@"OpenWebViewFeedback");
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    vc.pdfData=pdfDocumentData;
    vc.titleOpen=name;
    vc.urltoOpen=@"";
    vc.isfrom=@"WEBVIEWHANDLEPDF";
    vc.file_name=name;
    vc.file_type=type;
    vc.file_shareType=@"ShareYes";
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    UIViewController *topvc=[self topMostController];
    [topvc presentViewController:vc animated:NO completion:nil];
    // [self performSelector:@selector(openPDFfile:) withObject:pdfDocumentData afterDelay:1];
    
    
}

-(NSString *)mimeTypeForData:(NSData *)data
{
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return @"image/jpeg";
            break;
        case 0x89:
            return @"image/png";
            break;
        case 0x47:
            return @"image/gif";
            break;
        case 0x49:
        case 0x4D:
            return @"image/tiff";
            break;
        case 0x25:
            return @"application/pdf";
            break;
        case 0xD0:
            return @"application/vnd";
            break;
        case 0x46:
            return @"text/plain";
            break;
        default:
            return @"application/octet-stream";
    }
    return nil;
}

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
        
        NSLog(@"topController name = %@",topController);
        
    }
    
    return topController;
}

#pragma mark -


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

#pragma mark - Folder Cell Interface and Implementation

@interface FolderCell()
@end

@implementation FolderCell

@end

