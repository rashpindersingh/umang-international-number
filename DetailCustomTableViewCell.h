//
//  DetailCustomTableViewCell.h
//  Umang
//
//  Created by Kuldeep Saini on 27/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailCustomTableViewCell : UITableViewCell

@property(nonatomic,assign)CGFloat fontSizeApplied;

@property(nonatomic,strong)UILabel *lblTitle;
@property(nonatomic,strong)UILabel *lblDesc;
@property(nonatomic,strong)UIImageView *imgSeparator;
@property(nonatomic,strong)UIButton *btnViewOnMap;

@property(nonatomic,strong)UIView *bottomMapView;

-(void)bindCellDataWithTitle:(NSString*)title andDesc:(NSString*)desc withScreenWidth:(CGFloat)width;

-(void)designInterfaceForFrame:(CGRect)frame withFontSizeApplied:(CGFloat)fontSize withMapOptionRequired:(BOOL)isMapRequired;

@end
