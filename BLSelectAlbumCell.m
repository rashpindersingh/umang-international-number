//
//  BLSelectAlbumCell.m
//  BlendPhotoAndVideo
//
//  Created by Anhtd on 1/23/15.
//  Copyright (c) 2015 ILandApp. All rights reserved.
//

#import "BLSelectAlbumCell.h"

@implementation BLSelectAlbumCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
