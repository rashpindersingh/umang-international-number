//
//  BLPhoto.m
//  BlendPhotoAndVideo
//
//  Created by Hoang Tran on 12/26/14.
//  Copyright (c) 2014 ILandApp. All rights reserved.
//

#import "BLPhoto.h"
#import "UIImage+FX.h"

@interface BLPhoto ()

@property (nonatomic, strong) UIImage *thumbnailImage;

@end

@implementation BLPhoto

-(UIImage*)thumbnail
{
    if (self.photo) {
      //  if (!_thumbnailImage)
        {
            _thumbnailImage = [self.photo imageScaledToSize:[self getThumbnailSizeFromSize:self.photo.size]];
        }
        return _thumbnailImage;
    } else {
        return [UIImage imageWithCGImage:[self.asset thumbnail]];
    }
}

-(UIImage*)fullSizeImage
{
    if (self.photo) {
        return self.photo;
    } else {
        return [UIImage imageWithCGImage:[[self.asset defaultRepresentation] fullScreenImage]];
    }
}

-(BOOL)isPhoto
{
    return YES;
}

@end
