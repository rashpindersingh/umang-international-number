//
//  DigiSessionManager.swift
//  Umang
//
//  Created by Rashpinder on 09/03/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import UIKit
import Foundation

protocol UploadTaskDelegate {
  
}
protocol DownloadTaskDelegate {
    
}

typealias APICompletionBlockSwift = ((_ response:[AnyHashable:Any]?)-> Void)
typealias FailCompletionBlock = ((_ error:NSError?)-> Void)
class DigiSessionManager: NSObject {
    var session: URLSession! = URLSession()
    var uploadTask: URLSessionUploadTask!
    var downloadTask: URLSessionDownloadTask!
    var dicActiveTaks:[String:LockerDocument]! = [String:LockerDocument]()
     var dicUploadTaks:[String:LockerDocument]! = [String:LockerDocument]()
    var arrFileName = [String]()
    // MARK:-  ===== Singleton Object=======
    static let instance: DigiSessionManager = {
        let instance = DigiSessionManager()
        // setup code
        instance.arrFileName = instance.getFileNames()
        return instance
    }()

}
extension DigiSessionManager
{
    
    func downloadFileWith(requestBody:[AnyHashable:Any],doc:LockerDocument? = nil ) -> LockerDocument?
    {
        let singleton = SharedManager.sharedSingleton() as! SharedManager
        var authToken = ""
        // Append auth token with Bearer
        if true {
            if let aKey = UserDefaults.standard.object(forKey: "DIGIAUTHREFRESHTOKEN") as? String {
                authToken = aKey
            }
            if authToken.count == 0 {
                authToken = "Bearer 3a0f8e45-cb57-398c-929a-b748d033e4ll"
            }
        }
        let randomID = Double(arc4random()).truncatingRemainder(dividingBy: 9000000000000000 + 1000000000000000);
        let requestURL = singleton.apiMode.wso2PathBASE + UM_API_DOWNLOAD_DOC //"\(singleton.apiMode.wso2PathBASE)\(apiURL)"
        print("url download --\(requestURL)")
       // var configuration = URLSessionConfiguration.default
        let configuration = URLSessionConfiguration.background(withIdentifier: "bgSessionConfiguration")
        self.session = URLSession(configuration: configuration,
                                  delegate: self, delegateQueue: nil)
        var request = URLRequest(url: URL(string: requestURL)!, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 90)
        request.httpMethod =  "POST" //: "GET"
        let query: String = UMAPIManager.init().joinQuery(with: requestBody)//joinQuery(withDictionary: requestBody)
        request.httpBody = query.data(using: .utf8)
        //[request setHTTPBody: requestData];
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: API_CONTENT_TYPE)
        request.setValue(authToken, forHTTPHeaderField: API_REQUEST_AUTHORIZATION)
        request.setValue("\(Int(randomID))", forHTTPHeaderField: "requestid")
        let dwnTask = self.session.dataTask(with: request)
        //       let dwnTask = self.session.downloadTask(with: request) { (url, response, error) in
        //            print("url--Download--\(url)")
        //            print("response--Download--\(response)")
        //            print("error--Download--\(error)")
        //        }
        if doc != nil  {
            dwnTask.taskDescription = doc!.uri
            doc?.downTask = GenericDownloadTask(task: dwnTask)
            doc?.taskDescription = doc!.uri
            dicActiveTaks[doc!.taskDescription] = doc!
        }
        dwnTask.resume()
        if doc != nil  {
            return doc
        }
        return nil
    }
    func uploadFileWith(requestBody:[AnyHashable:Any],doc:LockerDocument? = nil ) -> LockerDocument?
    {
        let singleton = SharedManager.sharedSingleton() as! SharedManager
        var authToken = ""
        // Append auth token with Bearer
        if true {
            if let aKey = UserDefaults.standard.object(forKey: "DIGIAUTHREFRESHTOKEN") as? String {
                authToken = aKey
            }
            if authToken.count == 0 {
                authToken = "Bearer 3a0f8e45-cb57-398c-929a-b748d033e4ll"
            }
        }
        let randomID = Double(arc4random()).truncatingRemainder(dividingBy: 9000000000000000 + 1000000000000000);
        let requestURL = singleton.apiMode.wso2PathBASE + UM_API_UPLOAD_DOC //"\(singleton.apiMode.wso2PathBASE)\(apiURL)"
        let configuration = URLSessionConfiguration.default
        //let configuration = URLSessionConfiguration.background(withIdentifier: "bgSessionConfiguration")
        let session = URLSession(configuration: configuration,
                                  delegate: self, delegateQueue: nil)
        
        
        var request = URLRequest(url: URL(string: requestURL)!, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 90)
        request.httpMethod =  "POST" //: "GET"
        request.setValue("multipart/form-data", forHTTPHeaderField: API_CONTENT_TYPE)
        request.setValue(authToken, forHTTPHeaderField: API_REQUEST_AUTHORIZATION)
        request.setValue("\(Int(randomID))", forHTTPHeaderField: "requestid")
        var boundary: NSString? = nil
        let post = UMAPIManager.init().multipartData(withParameters: requestBody, boundary: &boundary)//multipartData(withParameters: requestBody, boundary: boundary)
        if let aBoundary = boundary {
            request.setValue("multipart/form-data; boundary=" + (aBoundary as String), forHTTPHeaderField: "Content-type")
        }
        request.httpBody = post
        
        let dwnTask = session.uploadTask(with: request, from: post!)
        if doc != nil  {
            dwnTask.taskDescription = doc!.uri
            doc?.uploadTask = GenericDownloadTask(task: dwnTask)
            doc?.uploadTask?.buffer = post!
            doc?.taskDescription = doc!.uri
            self.dicUploadTaks[doc!.taskDescription] = doc!
        }
        print("doc uri upload task --\(doc!.taskDescription)")
        dwnTask.resume()
        if doc != nil  {
            return doc
        }
        return nil
    }
    
    
}
extension DigiSessionManager: URLSessionDataDelegate {
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse,
                    completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        
        guard  let task = dicActiveTaks[dataTask.taskDescription ?? ""] else {
            completionHandler(.allow)
            return 
        }
        if task.downTask != nil  {
            task.downTask!.expectedContentLength = Int64(task.size.toInteger())//response.expectedContentLength
        }
       
        completionHandler(.allow)
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data)
    {
        guard let task = dicActiveTaks[dataTask.taskDescription ?? ""] else {
            return
        }
        guard let _ = task.downTask else {
            return
        }
        task.downTask!.buffer.append(data)
        let percentageDownloaded = Double(task.downTask!.buffer.count) / Double(task.downTask!.expectedContentLength)
        //let progressPercent = Int(percentageDownloaded*100)
         task.downTask!.progress = Float(percentageDownloaded)
        dicActiveTaks[dataTask.taskDescription!] = task
        DispatchQueue.main.async {
            task.downTask?.progressHandler?(task)
        }
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?)
    {

        if  let downloadtask = dicActiveTaks[task.taskDescription ?? ""]  {
            if let dTask = downloadtask.downTask
            {
                DispatchQueue.main.async {[weak self] in
                    if let e = error {
                        self?.dicActiveTaks.removeValue(forKey: task.taskDescription ?? "")
                        dTask.completionHandler?(.failure(e))
                    } else {
                        downloadtask.localPath = self?.saveFileToLocalPath(downloadtask, data:dTask.buffer ) ?? ""
                        self?.dicActiveTaks.removeValue(forKey: task.taskDescription ?? "")
                        dTask.completionHandler?(.success(downloadtask))
                    }
                }
            }
        }
       
        if  let uploadTask = dicUploadTaks[task.taskDescription ?? ""] {
            if let uTask = uploadTask.uploadTask
            {
                DispatchQueue.main.async {[weak self] in
                    if let e = error {
                        self?.dicUploadTaks.removeValue(forKey: uploadTask.taskDescription )
                        uTask.completionHandler?(.failure(e))
                    } else {
                        self?.dicUploadTaks.removeValue(forKey: uploadTask.taskDescription)
                        DispatchQueue.main.async {
                            uTask.completionHandler?(.success(uploadTask))
                        }
                       
                    }
                }
            }
        }
      
       
    }
}
extension DigiSessionManager :URLSessionDelegate, URLSessionTaskDelegate {
    // Standard background session handler
    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
//        DispatchQueue.main.async {
//            if let appDelegate = UIApplication.shared.delegate as? AppDelegate,
//         let completionHandler = appDelegate.backgroundSessionCompletionHandler {
//                appDelegate.backgroundSessionCompletionHandler = nil
//                completionHandler()
//            }
//        }
    }

    
    func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        
        let uploadProgress:Float = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
        //let progressPercent = Int(uploadProgress*100)
        guard let docTask = dicUploadTaks[task.taskDescription ?? ""] else {
            return
        }
        if let _ = docTask.uploadTask  {
            docTask.uploadTask!.progress = Float(uploadProgress)
            dicUploadTaks[docTask.taskDescription] = docTask
            DispatchQueue.main.async {
              //  print("doc progressHandler upload task --\(docTask.taskDescription)")
                docTask.uploadTask?.progressHandler?(docTask)
            }
        }
    }
  
    
}

extension DigiSessionManager {
    func getFileNames() -> [String] {
        var names = [String]()
        let documentDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        guard let documentPath = documentDirectoryPath  else {
            return names
        }
        // create the custom folder path
        let logsPath = documentPath.appending("/DigiLocker")
        print(logsPath)
        let fileManager = FileManager.default
        do {
            names = try fileManager.contentsOfDirectory(atPath: logsPath)
        }catch {
            print("Error  folder in documents dir: \(error)")
        }
        if names.count != 0 {
            names = names.map({ (str) -> String in
                return str.components(separatedBy:"(UMANG)").first!
            })
        }
        return names
    }
}
