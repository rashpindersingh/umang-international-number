//
//  DigilockerSearchVC.swift
//  Umang
//
//  Created by admin on 09/03/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import UIKit
import DisplaySwitcher

class DigilockerSearchVC: UIViewController{
    
    //Outlets for UI
    @IBOutlet weak var collDocuments: UICollectionView!
    @IBOutlet weak var btn_back: UIButton!
    @IBOutlet weak var vw_searchBar: UISearchBar!
    @IBOutlet weak var bottomCollectionConstraint: NSLayoutConstraint!
    fileprivate var isTransitionAvailable = true
    
    fileprivate lazy var listLayout = DisplaySwitchLayout(staticCellHeight: listLayoutStaticCellHeight, nextLayoutStaticCellHeight: gridLayoutStaticCellHeight, layoutState: .list)
    
    fileprivate lazy var gridLayout = DisplaySwitchLayout(staticCellHeight: gridLayoutStaticCellHeight, nextLayoutStaticCellHeight: listLayoutStaticCellHeight, layoutState: .grid)
    
    var layoutState: LayoutState = .list
    var sortType = SortType.date
    var segmentType = SegmentType.issued
    var singleton = SharedManager.sharedSingleton() as! SharedManager
    var arrDocuments = [LockerDocument]()
    var arrDataSource = [LockerDocument]()
    var searchActive : Bool = false
    

   

  
    // MARK:- *** View Life Cycle ****
    override func viewDidLoad() {
        super.viewDidLoad()
        //delegate connect for collection view
       self.setupCollectionView()
       // self.vw_searchBar..font = AppFont.semiBoldFont(17)
        self.btn_back.titleLabel?.font = AppFont.regularFont(17)
      self.view.backgroundColor = UIColor(red: 247.0/255.0, green:  247.0/255.0, blue:  249.0/255.0, alpha: 1)
        if let textFieldInsideUISearchBar = vw_searchBar.value(forKey: "searchField") as? UITextField {
            let placeholderLabel       = textFieldInsideUISearchBar.value(forKey: "placeholderLabel") as? UILabel
            textFieldInsideUISearchBar.font =  AppFont.regularFont(14)
            placeholderLabel?.font  = AppFont.regularFont(14)
        }
      
        // Do any additional setup after loading the view.
    }
    // MARK:- *** Collection View Setup ***
    fileprivate func setupCollectionView() {
        collDocuments.delegate = self
        collDocuments.dataSource = self
        collDocuments.collectionViewLayout = layoutState == .list ? listLayout : gridLayout
        collDocuments.register(CellDocumentList.cellNib, forCellWithReuseIdentifier:CellDocumentList.id)
        collDocuments.backgroundColor = .white
        self.reloadCollectionOnMain()
        collDocuments.isScrollEnabled = true
//        refreshController = UIRefreshControl()
//        refreshController.addTarget(self, action: #selector(self.refreshDocumentView), for: .valueChanged)
//        collDocuments.addSubview(refreshController)
        self.collDocuments.alwaysBounceVertical = true;
        
    }
    
   
    
    @IBAction func btnbackAction(_ sender: Any)
    {
        //add back button action here //default pop it
        self.navigationController?.popViewController(animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func reloadCollectionOnMain() {
        // self.arrDataSource = segment.selectedSegmentIndex == SegmentType.issued.rawValue ? self.arrDataSource : self.arrDataSource
        DispatchQueue.main.async {[weak self] in
            //self?.sortedButtonTitleChanges()
            self?.checkCollectionViewHeight()
            self?.setNoRecordHidden()
            self?.collDocuments.reloadData()
        }
    }
    func checkCollectionViewHeight()  {
        var height :CGFloat = listLayoutStaticCellHeight
        switch layoutState {
        case .grid:
            height = gridLayoutStaticCellHeight
            if arrDataSource.count.remainderReportingOverflow(dividingBy: 3).partialValue != 0 {
                height = (CGFloat((self.arrDataSource.count / 3)) * gridLayoutStaticCellHeight) + gridLayoutStaticCellHeight
            }else {
                height = (CGFloat((self.arrDataSource.count / 3)) * gridLayoutStaticCellHeight)
            }
            
        case .list:
            height = (CGFloat(arrDataSource.count) * listLayoutStaticCellHeight)
        }
        
        if  height >= (self.view.frame.size.height - 64.0) {
            self.bottomCollectionConstraint.constant = 0.0
        }else {
            self.bottomCollectionConstraint.constant = self.view.frame.size.height - (64.0 + height )
        }
        //self.bottomCollectionConstraint.constant = 0.0
        self.view.layoutIfNeeded()
        self.view.updateConstraintsIfNeeded()
    }
    func setNoRecordHidden() {
        if self.arrDataSource.count == 0 {
            //self.noRecordView.isHidden = false
            self.collDocuments.isHidden = true
            //self.view.bringSubview(toFront: self.noRecordView)
        }
        else {
           // self.noRecordView.isHidden = true
            self.collDocuments.isHidden = false
            self.view.bringSubview(toFront: self.collDocuments)
        }
    }

    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
// MARK:- *** Search Bar Delegate  ****
extension DigilockerSearchVC: UISearchBarDelegate {
    
    @objc func dismissKeyboard() {
        vw_searchBar.resignFirstResponder()
    }
    
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let searchString = searchText
        if  searchString.isEmpty == false  {
            self.arrDataSource = self.arrDocuments.filter({ (locker) -> Bool in
                return locker.name.lowercased().contains(searchString.lowercased())
            })
        }
        
        self.reloadCollectionOnMain()
    }
   
    //MARK: Search Bar
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        self.reloadCollectionOnMain()

        //self.dismiss(animated: true, completion: nil)
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
        self.reloadCollectionOnMain()
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
        self.reloadCollectionOnMain()
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        //dismissKeyboard()
        self.reloadCollectionOnMain()
    }
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        if !searchActive {
            searchActive = true
            self.reloadCollectionOnMain()
        }
        dismissKeyboard()
    }
}
// MARK:- *** UICollectionViewDataSource UICollectionViewDelegate ****

extension DigilockerSearchVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellDocumentList.id, for: indexPath) as! CellDocumentList
        if layoutState == .grid {
            cell.setupGridLayoutConstraints(1, cellWidth: cell.frame.width)
        } else {
            cell.setupListLayoutConstraints(1, cellWidth: cell.frame.width)
        }
        // cell.bind(searchUsers[(indexPath as NSIndexPath).row])
        let doc = arrDataSource[indexPath.row]
        cell.btnMenu.tag = indexPath.row + 1000
        cell.btnMenuGrid.tag = indexPath.row
        cell.setDataSource(doc, segment: self.segmentType)
        if doc.downTask != nil && doc.downloading {
        cell.updateProgressOnMainQueue(Double(doc.downTask!.progress))
            doc.cell = cell
            doc.index = indexPath
            doc.segment = segmentType
            // self.trackDownloadTask(doc)
        }
        self.setButtonActioneMethods(cell)
        
        return cell
    }
    
    func setButtonActioneMethods(_ cell:CellDocumentList)
    {
        //cell.btnMenuGrid.addTarget(self, action: #selector(self.didTapCellMenuButtonAction(_:)), /for: .touchUpInside)
      //  cell.btnMenu.addTarget(self, action: #selector(self.didTapCellMenuButtonAction(_:)), for: .touchUpInside)
        
    }
    // MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    func collectionView(_ collectionView: UICollectionView, transitionLayoutForOldLayout fromLayout: UICollectionViewLayout, newLayout toLayout: UICollectionViewLayout) -> UICollectionViewTransitionLayout {
        let customTransitionLayout = TransitionLayout(currentLayout: fromLayout, nextLayout: toLayout)
        return customTransitionLayout
    }
    
    
    func collectionView(_ collectionView: UICollectionView,didSelectItemAt indexPath: IndexPath) {
        print("Hi \((indexPath as NSIndexPath).row)")
        let docuMent = arrDataSource[indexPath.row]
        if docuMent.type == DocType.folder.rawValue {
            self.pushToSubHomeVC(docuMent, segment: self.segmentType, layout: self.layoutState, sortType: self.sortType)
        }
        else if docuMent.type == DocType.file.rawValue {
            docuMent.index = indexPath
            let cell = collectionView.cellForItem(at: indexPath) as? CellDocumentList
            docuMent.cell = cell
            docuMent.segment = segmentType
            self.downloadFile(docuMent)
        }
    }
    func downloadFile(_ doc:LockerDocument)  {
        let task = self.downloadDocumentFile(doc) ?? doc
        task.downloading = true
        self.arrDataSource[doc.index.row] = task
        task.cell?.updateProgressOnMainQueue(0)
        task.downTask?.completionHandler = { [weak self] in
            switch $0 {
            case .failure(let error):
                print(error)
                self?.downloadFile(doc)
            case .success(let doc):
                self?.downloadSuccess(doc)
            }
        }
        task.downTask?.progressHandler = { [weak self] in
            self?.updateProgress($0)
        }
    }
    func updateProgress(_ doc:LockerDocument)  {
        let index = self.checkTaskinArrayUpload(doc: doc)
        if let cell = self.collDocuments.cellForItem(at: index) as? CellDocumentList
        {
        cell.updateProgressOnMainQueue(Double(doc.downTask!.progress))
        }
    }
    func downloadSuccess(_ doc:LockerDocument)  {
        guard doc.cell != nil else {
            return
        }
        if doc.segment! == segmentType {
            doc.downTask = nil
            doc.downloading = false
            doc.cell = nil
            self.arrDataSource[doc.index.row] = doc
            DispatchQueue.main.async {[weak self] in
                //cell.updateProgressOnMainQueue(1.0)
                self?.collDocuments.reloadItems(at: [doc.index])
            }
        }
    }
    func checkTaskinArrayUpload( doc:LockerDocument) -> IndexPath {
        var index : Int = 10000
        if doc.segment != nil {
            switch doc.segment! {
            case .issued:
                index = self.arrDataSource.indexOf(object: doc)
                if  index != 10000 {
                    doc.index = IndexPath(item: index, section: 0)
                    self.arrDataSource[index] = doc
                }
            case .uploaded:
                index = self.arrDataSource.indexOf(object: doc)
                if index != 10000 {
                    doc.index = IndexPath(item: index, section: 0)
                    self.arrDataSource[index] = doc
                }
            }
        }
        return IndexPath(item: index, section: 0)
    }
    
}
