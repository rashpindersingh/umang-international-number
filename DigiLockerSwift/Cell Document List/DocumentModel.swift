//
//  UserDataProvider.swift
//  YALLayoutTransitioning
//
//  Created by Roman on 02.03.16.
//  Copyright © 2016 Yalantis. All rights reserved.
//
import UIKit
enum SessionTaskType:Int {
    case upload = 2222
    case download
    case other
}
class DownloadUploadTask {
    var cell:CellDocumentList?
    var taskDescription:String = ""
    init(taskDescription: String, cell:CellDocumentList?) {
        self.taskDescription = taskDescription
        self.cell = cell
    }
    // Download service sets these values:
    var task: URLSessionDownloadTask?
    var isDownloading = false
    var taskType :SessionTaskType = SessionTaskType.other
    var resumeData: Data?
    // Download delegate sets this value:
    var progress: Float = 0
}
class LockerDocument {
    var size: String = ""
    var type: String = ""
    var uri: String = ""
    var date: Date? = Date()
    var description: String = ""
    var issuer: String = ""
    var mime: String = ""
    var name: String = ""
    var parent: String = ""
    var id = ""
    var uploadTask : GenericDownloadTask? = nil
    var downTask : GenericDownloadTask? = nil
    var index :IndexPath!
    var cell:CellDocumentList?
    var taskDescription:String = ""
    var localPath = ""
    var segment :SegmentType? = nil
    var downloaded = false
    var downloading = false
    var uploading = false
    init(dicLocker:[String:String]){
        self.size = dicLocker["size"] ?? ""
        self.type = dicLocker["type"] ?? ""
        self.uri = dicLocker["uri"] ?? ""
        if let dat = dicLocker["date"] {
            if let strDate = dat.components(separatedBy: "T").first
            {
                self.date = strDate.getDate()
            }
        }
        self.description = dicLocker["description"] ?? ""
        self.issuer = dicLocker["issuer"] ?? ""
        self.mime = dicLocker["mime"] ?? ""
        self.name = dicLocker["name"] ?? ""
        self.parent = dicLocker["parent"] ?? ""
        if let id =  dicLocker["id"] {
            self.id = id
        }
    }
}
public enum ResultType<T> {
    
    public typealias Completion = (ResultType<T>) -> Void
    case success(T)
    case failure(Swift.Error)
}

protocol DownloadTask {
    
    var completionHandler: ResultType<LockerDocument>.Completion? { get set }
    var progressHandler: ((LockerDocument) -> Void)? { get set }
    
    func resume()
    func suspend()
    func cancel()
}
class GenericDownloadTask {
    
    var completionHandler: ResultType<LockerDocument>.Completion?
    var progressHandler: ((LockerDocument) -> Void)?
    
    private(set) var task: URLSessionDataTask
    var expectedContentLength: Int64 = 0
    var buffer = Data()
    var progress:Float = 0.0
    init(task: URLSessionDataTask) {
        self.task = task
    }
    
    deinit {
        print("Deinit: \(task.originalRequest?.url?.absoluteString ?? "")")
    }
    
}

extension GenericDownloadTask: DownloadTask {
    
    func resume() {
        task.resume()
    }
    
    func suspend() {
        task.suspend()
    }
    
    func cancel() {
        task.cancel()
    }
}
/*Printing description of self->dic_serviceInfo:
{
    "SERVICE_LANG" = "bn,en,gu,hi,ml,mr,as,or,ta,te,ur,pa,kn";
    "SERVICE_LATITUDE" = "19.0039820000000006";
    "SERVICE_LONGITUDE" = "72.825760399999993";
    "SERVICE_NAME" = NPS;
    "SERVICE_OTHER_STATE" = "";
    "SERVICE_PHONE_NUMBER" = 1800222080;
    "SERVICE_POPULARITY" = 1;
    "SERVICE_RATING" = "4.2";
    "SERVICE_STATE" = 99;
    "SERVICE_SUB_CATEGORY" = 0;
    "SERVICE_URL" = "https://stgweb.umang.gov.in/nps/api/deptt/npsHtml";
    "SERVICE_WEBSITE" = "https://www.npscra.nsdl.co.in/index.php";
    "SERVICE_WORKINGHOURS" = "Monday to Friday (09:00 AM - 06:00 PM)";
}*/
 
class ServiceData {
    var ID: String = ""
    var OTHER_WEBSITE: String = ""
    var SERVICE_CATEGORY: String = ""
    var SERVICE_CATEGORY_ID: String = ""
    var SERVICE_DEPTADDRESS: String = ""
    var SERVICE_DEPTDESCRIPTION: String = ""
    var SERVICE_DESC: String = ""
    var SERVICE_EMAIL: String = ""
    var SERVICE_ID = ""
    var SERVICE_IMAGE:String = ""
    var SERVICE_IS_FAV = ""
    var SERVICE_IS_HIDDEN = ""
    var SERVICE_IS_NOTIF_ENABLED = ""
    var SERVICE_LANG = "";
    var SERVICE_LATITUDE = "19.0039820000000006";
    var SERVICE_LONGITUDE = "72.825760399999993";
    var SERVICE_NAME = "";
    var SERVICE_OTHER_STATE = "";
    var SERVICE_PHONE_NUMBER = "";
    var SERVICE_POPULARITY = "1";
    var SERVICE_RATING = "4.2";
    var SERVICE_STATE = "";
    var SERVICE_SUB_CATEGORY = "";
    var SERVICE_URL = "";
    var SERVICE_WEBSITE = "";
    var SERVICE_WORKINGHOURS = "";
    init(_ dicData:[String:String]){
      self.ID = dicData["ID"] ?? ""
      self.OTHER_WEBSITE = dicData["OTHER_WEBSITE"] ?? ""
      self.SERVICE_CATEGORY = dicData["SERVICE_CATEGORY"] ?? ""
      self.SERVICE_CATEGORY_ID = dicData["SERVICE_CATEGORY_ID"] ?? ""
      self.SERVICE_SUB_CATEGORY = dicData["SERVICE_SUB_CATEGORY"] ?? ""
      self.SERVICE_DESC = dicData["SERVICE_DESC"] ?? ""
      self.SERVICE_DEPTADDRESS = dicData["SERVICE_DEPTADDRESS"] ?? ""
      self.SERVICE_DEPTDESCRIPTION = dicData["SERVICE_DEPTDESCRIPTION"] ?? ""
      self.SERVICE_LANG = dicData["SERVICE_LANG"] ?? ""
      self.SERVICE_LATITUDE = dicData["SERVICE_LATITUDE"] ?? ""
      self.SERVICE_LONGITUDE = dicData["SERVICE_LONGITUDE"] ?? ""
      self.SERVICE_NAME = dicData["SERVICE_NAME"] ?? ""
      self.SERVICE_POPULARITY = dicData["SERVICE_POPULARITY"] ?? ""
      self.SERVICE_PHONE_NUMBER = dicData["SERVICE_PHONE_NUMBER"] ?? ""
      self.SERVICE_RATING = dicData["SERVICE_RATING"] ?? ""
      self.SERVICE_STATE = dicData["SERVICE_STATE"] ?? ""
      self.SERVICE_WEBSITE = dicData["SERVICE_WEBSITE"] ?? ""
      self.SERVICE_WORKINGHOURS = dicData["SERVICE_WORKINGHOURS"] ?? ""
      self.SERVICE_IS_FAV = dicData["SERVICE_IS_FAV"] ?? ""
      self.SERVICE_IS_HIDDEN = dicData["SERVICE_IS_HIDDEN"] ?? ""
      self.SERVICE_IS_NOTIF_ENABLED = dicData["SERVICE_IS_NOTIF_ENABLED"] ?? ""
      self.SERVICE_EMAIL = dicData["SERVICE_EMAIL"] ?? ""
      self.SERVICE_ID = dicData["SERVICE_ID"] ?? ""
      self.SERVICE_IMAGE = dicData["SERVICE_IMAGE"] ?? ""
      self.SERVICE_OTHER_STATE = dicData["SERVICE_OTHER_STATE"] ?? ""
      self.SERVICE_URL = dicData["SERVICE_URL"] ?? ""
    }
}
