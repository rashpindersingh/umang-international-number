//
//  Dictionary+Extension.swift
//  PassangerApp
//
//  Created by Netquall on 1/12/16.
//  Copyright © 2016 Netquall. All rights reserved.
//

import Foundation
import UIKit
enum PDData {
    case json
    case array
}
extension Dictionary {
    
    // Remove Null values from Dictionary Values 
    
    func formatDictionaryForNullValues(_ dicValues:[String:Any])-> [String:Any]? {
        
        var jsonString:String!
        var jsonData : Data! = Data()
        do {
            jsonData = try JSONSerialization.data(withJSONObject: dicValues, options: .prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
            print("\(jsonString!)")
        }catch{
            print("Error--- \(error)")
            return nil
        }
        if jsonString.contains("<null>") {
            jsonString = jsonString.replacingOccurrences(of: "<null>", with: "\"\"")
        }
        if jsonString.contains("null"){
            jsonString = jsonString.replacingOccurrences(of: "null", with: "\"\"")
        }
        let ObjectData = jsonString!.data(using: String.Encoding.utf8)
        var dicReturn = [String:Any]()
        do {
            dicReturn = try JSONSerialization.jsonObject(with: ObjectData!, options: .mutableContainers) as! [String:Any]
            return dicReturn
        }catch {
            print("json Error-::\(error)")
            for (key, value ) in dicValues {
                dicReturn[key] = value
                if String(describing: value).compareTwoString("<null>") || String(describing: value).compareTwoString("null") ||  String(describing: value).compareTwoString("") {
                    dicReturn[key] = ""
                }
            }
            return dicReturn
        }
        
    }
    // MARK:- === Autoclosure Value Get Methods  ===
    func value<T>(forKey key: Key, defaultValue: @autoclosure () -> T) -> T {
        guard let value = self[key] as? T else {
            return defaultValue()
        }
        
        return value
    }
    func dicValue(forKey key : Key) -> [String:Any] {
        return self.value(forKey: key, defaultValue: [String:Any]())
    }
    func arrayValue(forKey key : Key) -> [Any] {
        return self.value(forKey: key, defaultValue: [Any]())
    }
    func arrayOfJSON(forKey key : Key) -> [[String:String]] {
        return self.value(forKey: key, defaultValue: [[String:String]]())
    }
    func string(key : Key) -> String {
        return self.value(forKey: key, defaultValue: "")
    }
    func intValue(forKey key : Key) -> Int {
        return self.value(forKey: key, defaultValue: 0)
    }
    func floatValue(forKey key : Key) -> Float {
        return self.value(forKey: key, defaultValue: 0.0)
    }
    // MARK:- *** Get PD Data From JSON
    func pdJSON(key :Key) -> [String:Any]? {
        return self.value(forKey: key, defaultValue: [String:Any]())
    }
    func pdArray(key:Key) -> [[String:String]] {
        return self.value(forKey: key, defaultValue: [[String:String]]())
    }
    
    // Convert from JSON to nsdata
    func getData() -> Data?{
        do {
            return try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil;
    }
}

extension NSDictionary {
    
    // Remove Null values from Dictionary Values
    
    func formatDictionaryForNullValues(_ dicValues:NSDictionary)-> NSDictionary? {
        
        var jsonString:String!
        var jsonData : Data! = Data()
        do {
            jsonData = try JSONSerialization.data(withJSONObject: dicValues, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
            // print("response data \(jsonString)")
        }catch{
            print("Error--- \(error)")
            return nil
        }
        if jsonString.contains("<null>") == true {
            jsonString = jsonString.replacingOccurrences(of: "<null>", with: "\"\"")
        }
        if jsonString.contains("null"){
            jsonString = jsonString.replacingOccurrences(of: "null", with: "\"\"")
        }
        
        
        let ObjectData = jsonString.data(using: String.Encoding.utf8)
        var dicReturn : NSDictionary!
        do {
            dicReturn = try JSONSerialization.jsonObject(with: ObjectData!, options: .mutableContainers) as! NSDictionary
            return dicReturn!
        }catch {
            print("json Error-::\(error)")
            return nil
        }
        
    }
    
    
}
// MARK: - **** Data Convert into JSON ***
extension Data {
    func getJSON() -> [String:Any]? {
        do {
            return try JSONSerialization.jsonObject(with: self, options: .mutableContainers) as? [String : Any]
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil
    }
}
