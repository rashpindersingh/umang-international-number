//
//  CellDocumentList.swift
//  Umang
//
//  Created by Rashpinder on 05/03/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import UIKit
import DisplaySwitcher

protocol CellInterface {
    
    static var id: String { get }
    static var cellNib: UINib { get }
    
}

extension CellInterface {
    
    static var id: String {
        return String(describing: Self.self)
    }
    
    static var cellNib: UINib {
        return UINib(nibName: id, bundle: nil)
    }
    
}
private let avatarListLayoutSize: CGFloat = 45.0
class CellDocumentList: UICollectionViewCell,CellInterface
{
    
    @IBOutlet fileprivate weak var leftImageView: UIImageView!
    @IBOutlet fileprivate weak var nameListLabel: UILabel!
    @IBOutlet fileprivate weak var nameGridLabel: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var size: UILabel!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnMenuGrid: UIButton!

    @IBOutlet weak var imageGridView: UIImageView!
    @IBOutlet weak var lblBottomLine: UILabel!
    @IBOutlet weak var progressList: UIProgressView!
    @IBOutlet weak var imageDownloaded: UIImageView!
    
    @IBOutlet weak var vwPrgressList: UIView!
    
    @IBOutlet weak var lblProgressGrid: UILabel!
    @IBOutlet weak var vwProgressGrid: UIView!
    @IBOutlet weak var imageProgressGrid: UIImageView!
    @IBOutlet weak var nameOfProgressLabel: UILabel!
    
    @IBOutlet weak var progressGrid: UIProgressView!
    @IBOutlet weak var lblProgressList: UILabel!
    //    var progressGrid :UIProgressView?
//    var progressList :UIProgressView?
//    var lblProgressList :UILabel?
//    var lblProgressGrid :UILabel?
    
    @IBOutlet weak var leadingListContentConstraint: NSLayoutConstraint!{
        didSet {
            initialLabelsLeadingConstraintValue = leadingListContentConstraint.constant
        }
    }
    @IBOutlet weak var listContentView: UIView!
    @IBOutlet weak var gridContentView: UIView!
    // avatarImageView constraints
    @IBOutlet fileprivate weak var avatarImageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var avatarImageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var gridImageWidthConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var gridImageHeightConstraint: NSLayoutConstraint!
  
   
    
    fileprivate var avatarGridLayoutSize: CGFloat = 0.0
    fileprivate var initialLabelsLeadingConstraintValue: CGFloat = 0.0
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setFont()
    }
    func setFont() {
        self.date.font = AppFont.regularFont(14)
        self.nameListLabel.font = AppFont.regularFont(15)
        self.nameGridLabel.font = AppFont.regularFont(12)
        self.lblProgressList.font = AppFont.regularFont(12)
        self.lblProgressGrid.font = AppFont.regularFont(12)
        self.size.font = AppFont.regularFont(11)
        self.nameListLabel.numberOfLines = 2
        self.nameListLabel.lineBreakMode = .byWordWrapping
        self.nameOfProgressLabel.font = AppFont.regularFont(10)
        self.nameOfProgressLabel.numberOfLines = 1
        self.nameOfProgressLabel.lineBreakMode = .byTruncatingTail
    }
    func setDataSource(_ document: LockerDocument, segment:SegmentType) {
        self.progressHiddenUpdate(hide: false)
        nameListLabel.text = document.name
        nameGridLabel.text = nameListLabel.text
        nameOfProgressLabel.text = document.name
        date.text = document.date?.getString() ?? ""
        size.text = ""
        self.imageDownloaded.isHidden = true
        if  document.type == DocType.file.rawValue {
            date.text = date.text! + "   " + document.size.getSize()
             self.checkFileExist(document, arrNames: DigiSessionManager.instance.arrFileName)
        }
        if segment == .issued {
            date.text = document.issuer
        }
        let typeTupple = self.checkFileTypes(mime: document.mime)
        leftImageView.contentMode = .scaleAspectFit
        leftImageView.image = UIImage(named:typeTupple.image)
        imageGridView.image =  leftImageView.image
        imageProgressGrid.image = leftImageView.image
        let imageMenu = self.checkFileOrDocument(type: document.type)
        btnMenu.setImage(UIImage(named:imageMenu.image), for: .normal)
           }
    
    func checkFileOrDocument(type:String) -> (DocType, image:String) {
        if type.contains(DocType.file.rawValue) {
            return (DocType.file,"menu_AadharLink.png")
        }
        else if type.contains(DocType.folder.rawValue) {
            return (DocType.folder,"digiRightArrow")
        }
        return (DocType.file,"menu_AadharLink.png")
    }

    func setupGridLayoutConstraints(_ transitionProgress: CGFloat, cellWidth: CGFloat) {
//        avatarImageViewHeightConstraint.constant = ceil((cellWidth - avatarListLayoutSize) * transitionProgress + avatarListLayoutSize)
//        avatarImageViewWidthConstraint.constant = ceil(avatarImageViewHeightConstraint.constant)
        leadingListContentConstraint.constant = -avatarImageViewWidthConstraint.constant * transitionProgress + initialLabelsLeadingConstraintValue
        self.alpha = transitionProgress <= 0.5 ? 1 - transitionProgress : transitionProgress
        listContentView.alpha = 1 - transitionProgress
        leftImageView.alpha = listContentView.alpha
        imageDownloaded.alpha = leftImageView.alpha
        gridContentView.alpha = 1
        lblBottomLine.alpha = listContentView.alpha

    }
    func setupListLayoutConstraints(_ transitionProgress: CGFloat, cellWidth: CGFloat) {
        avatarImageViewHeightConstraint.constant = ceil(avatarGridLayoutSize - (avatarGridLayoutSize - avatarListLayoutSize) * transitionProgress)
        avatarImageViewWidthConstraint.constant = avatarImageViewHeightConstraint.constant
        leadingListContentConstraint.constant = avatarImageViewWidthConstraint.constant * transitionProgress + (initialLabelsLeadingConstraintValue - avatarImageViewHeightConstraint.constant)
        
        self.alpha = transitionProgress <= 0.5 ? 1 - transitionProgress : transitionProgress
        listContentView.alpha = transitionProgress
        gridContentView.alpha = 0
        leftImageView.alpha = listContentView.alpha
        imageDownloaded.alpha = leftImageView.alpha
        lblBottomLine.alpha = listContentView.alpha
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        if let attributes = layoutAttributes as? DisplaySwitchLayoutAttributes {
            if attributes.transitionProgress > 0 {
                if attributes.layoutState == .grid {
                    setupGridLayoutConstraints(attributes.transitionProgress, cellWidth: attributes.nextLayoutCellFrame.width)
                    avatarGridLayoutSize = attributes.nextLayoutCellFrame.width
                } else {
                    setupListLayoutConstraints(attributes.transitionProgress, cellWidth: attributes.nextLayoutCellFrame.width)
                }
            }
        }
    }
}
// MARK:--  **Progress Methods Here ***
extension CellDocumentList {
    func updateProgressOnMainQueue(_ progress:Double)  {
        if self.vwPrgressList.isHidden == true || self.vwProgressGrid.isHidden == true  {
            //DispatchQueue.main.async {[weak self] in
                self.showProgress()
                self.updateProgress(progress)
            //}
            return
        }else {
            //DispatchQueue.main.async {[weak self] in
                self.updateProgress(progress)
            //}
        }
    }
    func showProgress(){
        self.gridContentView.bringSubview(toFront: self.vwProgressGrid)
        self.listContentView.bringSubview(toFront: self.vwPrgressList)
        self.vwProgressGrid.layer.borderWidth = 0.8
        self.vwProgressGrid.layer.borderColor = UIColor.lightGray.cgColor
        self.progressHiddenUpdate(hide: true)
    }
    
    
    func updateProgress(_ progress:Double){
        if progress == 1.0 {
            self.progressCompleted()
            return
        }
        let progressPercent = Int(progress*100)
        if  self.progressGrid != nil {
            self.isUserInteractionEnabled = false
            self.progressGrid!.progress = Float(progress)
            self.lblProgressGrid.text = "\(progressPercent)%"
        }
        if  self.progressList != nil {
            self.isUserInteractionEnabled = false
            self.progressList!.progress = Float(progress)
            self.lblProgressList.text = "\(progressPercent)%"
        }
     }
    func progressCompleted() {
        DispatchQueue.main.async {[weak self] in
            self?.progressHiddenUpdate(hide: false)
        }
    }
    func progressHiddenUpdate(hide:Bool) {
        let alpha:CGFloat = hide ? 0.60 : 1.0
        self.nameListLabel.alpha = alpha
        self.nameGridLabel.alpha = alpha
        self.imageGridView.alpha = alpha
        self.leftImageView.alpha = alpha
        self.vwPrgressList.isHidden = !hide
        self.vwProgressGrid.isHidden = !hide
        self.progressList.isHidden = !hide
        self.progressGrid.isHidden = !hide
        self.lblProgressList.isHidden = !hide
        self.lblProgressGrid.isHidden = !hide
        self.date.isHidden = hide
        self.size.isHidden = hide
        self.btnMenu.isHidden = hide
        self.btnMenuGrid.isHidden = hide
        self.isUserInteractionEnabled = !hide
        
        //let height :CGFloat = hide ? 40.0 : 60.0
        //self.gridImageWidthConstraint.constant = height
       // self.gridImageHeightConstraint.constant = height
        self.layoutIfNeeded()
        self.updateConstraintsIfNeeded()
    }
    
}

extension CellDocumentList {
    func checkFileExist(_ doc:LockerDocument, arrNames:[String])
    {
        if arrNames.count != 0  && arrNames.contains(doc.uri){
            let logsPath = self.getDigiFolderPath()
            var fileName =  doc.uri + "(UMANG)" + doc.name
            fileName = "\(logsPath)/\(fileName)"
            fileName = fileName.removeBlankSpace()
            doc.localPath = fileName
            doc.downloaded = true
            self.imageDownloaded.isHidden = false
            return
        }
        self.imageDownloaded.isHidden = true
    }
    func getDigiFolderPath() -> String {
        let documentDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        guard let documentPath = documentDirectoryPath  else {
            return ""
        }
        // create the custom folder path
        return  documentPath.appending("/DigiLocker")
    }
}

