//
//  DigiLockerHomeVC.swift
//  Umang
//
//  Created by Rashpinder on 05/03/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import UIKit
import Photos
import DisplaySwitcher
import Foundation
 let animationDuration: TimeInterval = 0.0
 let listLayoutStaticCellHeight: CGFloat = 60
 var gridLayoutStaticCellHeight: CGFloat = 100
var kIssueSortKey = "issueSortKey"
var kUploadSortKey = "kUploadSortKey"
var kIssueLayoutKey = "kIssueLayoutKey"
var kUploadLayoutKey = "kUploadLayoutKey"
enum SegmentType:Int {
    case issued = 0
    case uploaded = 1
}
enum DocType:String {
    case file = "file"
    case folder = "dir"
    case pdf = "pdf"
    case image = "image"
    case jpg = "jpeg"
    case jpeg = "jpg"
    case png = "png"
    case pdfApplication = "application/pdf"
    case other = "other"
}
enum SortType:Int {
    case name = 100
    case date = 111
    
}
class DigiLockerHomeVC: UIViewController
{
    // MARK:- **** Property Decalarations *****
    

    @IBOutlet weak var btnGridView: UIButton!
    @IBOutlet weak var btnListView: UIButton!
    @IBOutlet weak var btnSort: UIButton!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnUploadDoc: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var bottomCollectionConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblNoRecord: UILabel!
    @IBOutlet weak var noRecordView: UIView!
    @IBOutlet weak var collDocuments: UICollectionView!
    @IBOutlet weak var naviView: UIView!
    fileprivate var isTransitionAvailable = true
    fileprivate lazy var listLayout = DisplaySwitchLayout(staticCellHeight: listLayoutStaticCellHeight, nextLayoutStaticCellHeight: gridLayoutStaticCellHeight, layoutState: .list)
    fileprivate lazy var gridLayout = DisplaySwitchLayout(staticCellHeight: gridLayoutStaticCellHeight, nextLayoutStaticCellHeight: listLayoutStaticCellHeight, layoutState: .grid)
    fileprivate var layoutState: LayoutState = .list
     var arrIssued = [LockerDocument]()
     var arrUploaded = [LockerDocument]()
      var arrDataSource = [LockerDocument]()
      var segmentType = SegmentType.issued
      var singleton = SharedManager.sharedSingleton() as! SharedManager
     var hud :MBProgressHUD!
     var serviceGroup: DispatchGroup?
    var refreshController = UIRefreshControl()
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    // MARK:- **** View Life Cycle *****

    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarStyle = .default
        DigiSessionManager.instance
    self.navigationController?.isNavigationBarHidden = true
        self.setupInitialView()
        setupCollectionView()
        self.callDispatchApi()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func callDispatchApi()  {
        if serviceGroup == nil  {
           serviceGroup = DispatchGroup()
        }
        hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.text = "loading".localized
        serviceGroup?.enter()
        self.fetchIssuedDocumentsApi()
        serviceGroup?.enter()
        self.fetchUploadedDocumentsApi("")
        let issueSortKey =  UserDefaults.standard.value(forKey: kIssueSortKey) as? Int ?? SortType.date.rawValue
        let sortIssue = SortType.init(rawValue: issueSortKey) ?? SortType.date
        let uploadSortKey =  UserDefaults.standard.integer(forKey: kUploadSortKey)
        let sortUpload = SortType.init(rawValue: uploadSortKey) ?? SortType.date
        if serviceGroup != nil  {
            serviceGroup?.notify(queue:.main, execute: {[weak self] in
                self?.updateViewWithSegmentChanges()
               // self?.reloadCollectionOnMain()
                self?.serviceGroup = nil
            })
        }
    }
    
    func setupInitialView(){
        self.segment.setTitle("Issued", forSegmentAt: 0)
        self.segment.setTitle("Uploaded", forSegmentAt: 1)
        self.lblTitle.text = "Digi Locker"
        self.btnBack.setTitle("back".localized, for: .normal)
        self.segment.selectedSegmentIndex = segmentType.rawValue
        self.lblTitle.font = AppFont.semiBoldFont(17)
        self.btnBack.titleLabel?.font = AppFont.regularFont(17)
        self.btnSort.titleLabel?.font = AppFont.regularFont(14)
    self.segment.setTitleTextAttributes([NSFontAttributeName:AppFont.regularFont(14)], for: .normal)
        
        self.naviView.backgroundColor = UIColor(red: 247.0/255.0, green:  247.0/255.0, blue:  249.0/255.0, alpha: 1)
        self.view.backgroundColor = UIColor(red: 239.0/255.0, green:  239.0/255.0, blue:  244.0/255.0, alpha: 1)

        //navi view-- 247 247 249
        // sort 239 239 244
        
    }
    // MARK:- *** Collection View Setup ***
    fileprivate func setupCollectionView() {
        collDocuments.delegate = self
        collDocuments.dataSource = self
        let layout =  UserDefaults.standard.value(forKey: kIssueLayoutKey) as? Int ?? LayoutState.list.rawValue
        layoutState = LayoutState.init(rawValue: layout) ?? LayoutState.list
        collDocuments.collectionViewLayout = layoutState == .list ? listLayout : gridLayout
        collDocuments.register(CellDocumentList.cellNib, forCellWithReuseIdentifier:CellDocumentList.id)
        collDocuments.backgroundColor = .white
        collDocuments.reloadData()
        collDocuments.isScrollEnabled = true
        refreshController = UIRefreshControl()
        refreshController.addTarget(self, action: #selector(self.refreshDocumentView), for: .valueChanged)
        collDocuments.addSubview(refreshController)
        self.collDocuments.alwaysBounceVertical = true;

    }
    @objc func refreshDocumentView()  {
        hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.text = "loading".localized
        switch segmentType {
        case .issued:
            self.fetchIssuedDocumentsApi()
        case .uploaded:
            self.fetchUploadedDocumentsApi()
        }
        if refreshController.isRefreshing {
            refreshController.endRefreshing()
        }
    }
// MARK: - ***** Custom Button Action Methods *****
    
    @IBAction func didTapBackButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func didTapSortByButtonAction(_ sender: UIButton) {
        
        let alertController = self.actionSheet()
        let nameAction = UIAlertAction(title: "Name (Alaphabetic)", style: .default) { [weak self](alert) in
           self?.sortDocumentsList(.name)
        }
        let dateAction = UIAlertAction(title: "Date (Newest - Oldest)", style: .default) {[weak self] (alert) in
            self?.sortDocumentsList(.date)
        }
        alertController.addAction(nameAction)
        alertController.addAction(dateAction)
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = sender
            if #available(iOS 9.0, *) {
                popoverController.canOverlapSourceViewRect = true
            } else {
                // Fallback on earlier versions
            }
            popoverController.sourceRect.origin.y = 25
            self.present(alertController, animated: true, completion: nil)
            return
        }
        let cancelAction = UIAlertAction(title:"cancel".localized , style: .cancel, handler: nil) //
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    @IBAction func didTapListViewButtonAction(_ sender: UIButton) {
        let arrData = segment.selectedSegmentIndex == SegmentType.issued.rawValue ? self.arrIssued : self.arrUploaded

        if !isTransitionAvailable ||  layoutState == .list || arrData.count == 0 {
            return
        }
        let key = segmentType == .issued ? kIssueLayoutKey : kIssueLayoutKey
        layoutState = .list
        UserDefaults.standard.setValue(layoutState.rawValue, forKey: key)

        self.updateTrasitionManager()
    }
    
    @IBAction func didTapGridViewButtonAction(_ sender: UIButton) {
         let arrData = segment.selectedSegmentIndex == SegmentType.issued.rawValue ? self.arrIssued : self.arrUploaded
        if !isTransitionAvailable ||  layoutState == .grid || arrData.count == 0{
            return
        }
        layoutState = .grid
        let key = segmentType == .issued ? kIssueLayoutKey : kIssueLayoutKey
        UserDefaults.standard.setValue(layoutState.rawValue, forKey: key)
        self.updateTrasitionManager()
    }
    
    func updateTrasitionManager() {
        self.updateListGridButtons()
        let layout = layoutState == .grid ? gridLayout : listLayout
          let transitionManager = TransitionManager(duration: animationDuration, collectionView: collDocuments!, destinationLayout: layout, layoutState: layoutState)
        DispatchQueue.main.async {[weak self] in
            self?.checkCollectionViewHeight()
            transitionManager.startInteractiveTransition()
        }
    }
    func updateListGridButtons()  {
        switch layoutState {
        case .list:
            btnListView.isSelected = true
            btnGridView.isSelected = false
        case .grid:
            btnListView.isSelected = false
            btnGridView.isSelected = true
        }
    }
    @IBAction func didTapMenuButtonAction(_ sender: UIButton) {
        
    }
    @IBAction func didTapSearchButtonAction(_ sender: UIButton) {
        let arrData = segment.selectedSegmentIndex == SegmentType.issued.rawValue ? self.arrIssued : self.arrUploaded
        if arrData.count != 0  {
            self.pushToSearchVC(self.segmentType, layout: layoutState, arrData: arrData)
        }
    }
    @IBAction func didTapUploadDocumentButtonAction(_ sender: UIButton){
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        DispatchQueue.main.async {[weak self] in
            self?.navigationController?.present(myPickerController, animated: true, completion: nil)
        }
    }
    @IBAction func didChangeValueSegment(_ sender: UISegmentedControl) {
        self.segmentType = SegmentType(rawValue: sender.selectedSegmentIndex)!
        self.updateViewWithSegmentChanges()
    }
    
    func updateViewWithSegmentChanges() {
        let sort = self.getSortyKeyWith(segmentType)
        self.sortDocumentsList(sort)
        switch segmentType {
        case .issued:
            self.btnUploadDoc.isHidden = true
            break
        case .uploaded:
            self.btnUploadDoc.isHidden = false
            break
        }
        //self.sortedButtonTitleChanges()
        self.updateListGridButtons()
        self.reloadCollectionOnMain()
    }
    func reloadAfterDelay()  {
        self.updateTrasitionManager()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DigiLockerHomeVC {
    //MARK:----====Action Sheet Methods =====
    func actionSheet(title:String? = nil, message:String? = nil) -> UIAlertController  {
        let alertController  = UIAlertController(title: title, message:message , preferredStyle: .actionSheet)
        return alertController
    }
    func sortDocumentsList(_ type:SortType, segment:SegmentType? = nil ){
        let vSegment = segment == nil ? self.segmentType :segment!
        switch vSegment {
        case .issued:
            UserDefaults.standard.set(type.rawValue, forKey: kIssueSortKey)
            self.arrIssued = type == .name ? self.arrIssued.sortWithName(self.arrIssued) : self.arrIssued.sortWithDate(self.arrIssued)
        case .uploaded:
            UserDefaults.standard.set(type.rawValue, forKey: kUploadSortKey)
             self.arrUploaded = type == .name ? self.arrUploaded.sortWithName(self.arrUploaded) : self.arrUploaded.sortWithDate(self.arrUploaded)
            break
       
        }
        UserDefaults.standard.synchronize()
        self.reloadCollectionOnMain()
    }
   
    
    func sortedButtonTitleChanges()  {
        switch segmentType {
        case .issued:
          let issueSortKey =  UserDefaults.standard.value(forKey: kIssueSortKey) as? Int ?? SortType.date.rawValue
          let sortIssue = SortType.init(rawValue: issueSortKey) ?? SortType.date
          let title = sortIssue == .name ? "Sort By Name" : "Sort By Date"
            self.btnSort.setTitle(title, for: .normal)
        case .uploaded:
            let issueSortKey =  UserDefaults.standard.value(forKey: kUploadSortKey) as? Int ?? SortType.date.rawValue
            let sortIssue = SortType.init(rawValue: issueSortKey) ?? SortType.date
            let title = sortIssue == .name ? "Sort By Name" : "Sort By Date"
            self.btnSort.setTitle(title, for: .normal)
        }
    }
}
// MARK:- ***  Web api Call Methods Here ****

extension DigiLockerHomeVC
{
    // MARK:- *** Get Issued Document list ****
    func fetchIssuedDocumentsApi()
    {
        let dictBody = NSMutableDictionary()
        let CurrentTime: Double = CACurrentMediaTime()
        var timeInMS = "\(CurrentTime)"
        timeInMS = timeInMS.replacingOccurrences(of: ".", with: "")
        var accessTokenString = UserDefaults.standard.value(forKey: "AccessTokenDigi") as? String
        accessTokenString = accessTokenString?.count == 0 ? "" : accessTokenString
        dictBody["trkr"] = timeInMS
        dictBody["ort"] = "rgtadhr"
        dictBody["rc"] = "Y"
        dictBody["mec"] = "Y"
        dictBody["tkn"] = singleton.user_tkn
        dictBody["utkn"] = accessTokenString
        print("Dictionary is \(dictBody)")
        self.callApi(with: dictBody, url: UM_API_GETISSUED_DOC, type: .issued)
    }
    func callApi(with dicBody:NSMutableDictionary, url:String, type:SegmentType) {
        let objRequest = UMAPIManager()
        objRequest.hitAPIForDigiLockerAuthentication(withPost: true, isAccessTokenRequired: true, webServiceURL: url, withBody: dicBody, andTag: TAG_REQUEST_DIGILOCKER_GETTOKEN, completionHandler: {[weak self](_ response: Any, _ error: Error?, _ tag: REQUEST_TAG) -> Void in
            guard let er = error else {
                guard let responseJson = response as? [String:Any] else {
                    self?.hud.hide(animated: true)
                    return
                }
                self?.checkResponse(responseJson, type: type)
                return
            }
            self?.showErrorAlert(error: er)
        })
    }
    func checkResponse(_ response:[String:Any],type:SegmentType)  {
        self.hud.hide(animated: true)
        let dicFormat = response.formatDictionaryForNullValues(response)
        if self.checkSuccessResponse(dicResponse: dicFormat!) {
            self.processIssuedApi(dicFormat!, type: type)
        }
    }
    func showErrorAlert(error:Error)  {
        self.hud.hide(animated: true)
        let alert = UIAlertView(title: "error".localized, message: error.localizedDescription, delegate: nil, cancelButtonTitle:"ok".localized, otherButtonTitles: "")
        alert.show()
    }
    // MARK:- *** Get Uploaded Document list ****
    func fetchUploadedDocumentsApi(_ idString:String = "")
    {
        
        let objRequest = UMAPIManager()
        let dictBody = NSMutableDictionary()
        let CurrentTime: Double = CACurrentMediaTime()
        var timeInMS = "\(CurrentTime)"
        timeInMS = timeInMS.replacingOccurrences(of: ".", with: "")
        let accessTokenString = UserDefaults.standard.value(forKey: "AccessTokenDigi") as? String ?? ""
        dictBody["id"] = idString
        dictBody["trkr"] = timeInMS
        dictBody["ort"] = "rgtadhr"
        dictBody["rc"] = "Y"
        dictBody["mec"] = "Y"
        dictBody["tkn"] = singleton.user_tkn
        dictBody["utkn"] = accessTokenString
        print("Dictionary is \(dictBody)")
        self.callApi(with: dictBody, url: UM_API_GETUPLOADED_DOC,type: .uploaded)
    }
    func processIssuedApi(_ dicJson:[String:Any], type:SegmentType)
    {
        self.hud.hide(animated: true)

        print("API Success")
        guard let pdJson = dicJson["pd"] as? [String:Any] else {
            return
        }
        guard let itemsJson = pdJson["items"] as? [[String:String]] else {
            return
        }
        var arrDocuments = [LockerDocument]()
        let activeTask = DigiSessionManager.instance.dicActiveTaks
        for dicItems in itemsJson {
            var lockDoc = LockerDocument(dicLocker: dicItems)
            if activeTask?.count != 0
            {
                if let task = activeTask![lockDoc.uri] {
                    lockDoc = task
                }
            }
            arrDocuments.append(lockDoc)
        }
        switch type {
        case .issued:
            self.arrIssued = arrDocuments
        case .uploaded:
            self.arrUploaded = arrDocuments
        }
        if serviceGroup != nil {
            serviceGroup?.leave()
            // serviceGroup = nil ;
        }else {
            self.updateViewWithSegmentChanges()
        }
    }
    func reloadCollectionOnMain() {
        self.reloadCollectionOnly()
    }
    func reloadCollectionOnly() {
        DispatchQueue.main.async {[weak self] in
            self?.checkCollectionViewHeight()
            self?.sortedButtonTitleChanges()
            self?.setNoRecordHidden()
            self?.collDocuments.reloadData()
        }
    }
    func checkCollectionViewHeight()  {
         let arrData = segment.selectedSegmentIndex == SegmentType.issued.rawValue ? self.arrIssued : self.arrUploaded
        var height :CGFloat = 0
        switch layoutState {
        case .grid:
            height = gridLayoutStaticCellHeight
            if arrData.count.remainderReportingOverflow(dividingBy: 3).partialValue != 0 {
                height = (CGFloat((arrData.count / 3)) * gridLayoutStaticCellHeight) + gridLayoutStaticCellHeight
            }else {
                 height = (CGFloat((arrData.count / 3)) * gridLayoutStaticCellHeight)
            }
        case .list:
            height = CGFloat(arrData.count) * listLayoutStaticCellHeight
        }
        if  height >= (self.view.frame.size.height - 140.0) {
            self.bottomCollectionConstraint.constant = 0.0
        }else {
            self.bottomCollectionConstraint.constant = self.view.frame.size.height - (140.0 + height)
        }
        //self.bottomCollectionConstraint.constant = 0.0
        self.view.layoutIfNeeded()
        self.view.updateConstraintsIfNeeded()
    }
    func setNoRecordHidden() {
        let arrData = segment.selectedSegmentIndex == SegmentType.issued.rawValue ? self.arrIssued : self.arrUploaded
        if arrData.count == 0 {
            self.noRecordView.isHidden = false
            self.collDocuments.isHidden = true
            self.view.bringSubview(toFront: self.noRecordView)
        }
        else {
            self.noRecordView.isHidden = true
            self.collDocuments.isHidden = false
            self.view.bringSubview(toFront: self.collDocuments)
        }
    }
}
// MARK:- *** UICollectionViewDataSource UICollectionViewDelegate ****

extension DigiLockerHomeVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch self.segmentType {
        case .issued:
            return arrIssued.count
        case .uploaded:
            return arrUploaded.count
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellDocumentList.id, for: indexPath) as! CellDocumentList
        if layoutState == .grid {
            cell.setupGridLayoutConstraints(1, cellWidth: cell.frame.width)
        } else {
            cell.setupListLayoutConstraints(1, cellWidth: cell.frame.width)
        }
        
        let doc = self.segmentType == .issued ? self.arrIssued[indexPath.row] : self.arrUploaded[indexPath.row]//arrDataSource[indexPath.row]
        // doc.index = indexPath
        cell.setDataSource(doc, segment: self.segmentType)
        cell.btnMenu.tag = indexPath.row + 1000
        cell.btnMenuGrid.tag = indexPath.row
        if doc.downTask != nil && doc.downloading {
        cell.updateProgressOnMainQueue(Double(doc.downTask!.progress))
            doc.cell = cell
            doc.index = indexPath
            doc.segment = segmentType
        }
        if doc.uploadTask != nil && doc.uploading {
       cell.updateProgressOnMainQueue(Double(doc.uploadTask!.progress))
           // doc.cell = cell
           // doc.index = indexPath
           // doc.segment = segmentType
        }
        self.setButtonActioneMethods(cell)
        return cell
    }
    func setButtonActioneMethods(_ cell:CellDocumentList)
    {
        cell.btnMenuGrid.addTarget(self, action: #selector(self.didTapCellMenuButtonAction(_:)), for: .touchUpInside)
        cell.btnMenu.addTarget(self, action: #selector(self.didTapCellMenuButtonAction(_:)), for: .touchUpInside)

    }
    // MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    func collectionView(_ collectionView: UICollectionView, transitionLayoutForOldLayout fromLayout: UICollectionViewLayout, newLayout toLayout: UICollectionViewLayout) -> UICollectionViewTransitionLayout {
        let customTransitionLayout = TransitionLayout(currentLayout: fromLayout, nextLayout: toLayout)
        return customTransitionLayout
    }
    
    func collectionView(_ collectionView: UICollectionView,didSelectItemAt indexPath: IndexPath) {
       // let docuMent = arrDataSource[indexPath.row]
        let docuMent = self.segmentType == .issued ? self.arrIssued[indexPath.row] : self.arrUploaded[indexPath.row]
        if docuMent.type == DocType.folder.rawValue {
            self.pushToSubHomeVC(docuMent, segment: self.segmentType, layout: self.layoutState, sortType: self.getSortyKeyWith(self.segmentType))
        }
        else if docuMent.type == DocType.file.rawValue {
            docuMent.index = indexPath
            let cell = collectionView.cellForItem(at: indexPath) as? CellDocumentList
            docuMent.cell = cell
            docuMent.segment = segmentType
            self.downloadFile(docuMent)
        }
        
    }
    func downloadFile(_ doc:LockerDocument)
    {
        let task = self.downloadDocumentFile(doc) ?? doc
        task.downloading = true
        // self.segmentType == .issued ? self.arrIssued[indexPath.row]  : self.arrUploaded[indexPath.row]
        self.saveTaskToArray(doc)
      //  self.arrDataSource[doc.index.row] = task
        task.cell?.updateProgressOnMainQueue(0)
        task.downTask?.completionHandler = { [weak self] in
            switch $0 {
            case .failure(let error):
                print(error)
                self?.downloadFile(doc)
            case .success(let doc):
                self?.downloadSuccess(doc)
            }
        }
        task.downTask?.progressHandler = { [weak self] in
            self?.updateProgress($0)
        }
    }
    func saveTaskToArray(_ doc:LockerDocument)  {
        if doc.segment != nil {
            switch doc.segment! {
            case .issued:
                self.arrIssued[doc.index.row] = doc
            case .uploaded:
                self.arrUploaded[doc.index.row] = doc
            }
        }
    }
    
   func updateProgress(_ doc:LockerDocument)
    {
        self.saveTaskToArray(doc)
       if doc.segment! == segmentType
         {
      if let cell = self.collDocuments.cellForItem(at: doc.index) as? CellDocumentList
        {
        cell.updateProgressOnMainQueue(Double(doc.downTask!.progress))
          }
        }
    
    }

    func downloadSuccess(_ doc:LockerDocument)  {
        self.saveTaskToArray(doc)
        if doc.segment! == segmentType {
            doc.downTask = nil
            doc.downloaded = true
            doc.cell = nil
          //  self.arrDataSource[doc.index.row] = doc
            DispatchQueue.main.async {[weak self] in
                // cell.updateProgressOnMainQueue(1.0)
                self?.collDocuments.reloadItems(at: [doc.index])
            }
        }
    }
}
// MARK:- ***** Cell Button Actione Here ****
extension DigiLockerHomeVC{
    func didTapCellMenuButtonAction(_ sender:UIButton)  {
        let tag = sender.tag >= 1000 ? sender.tag - 1000 : sender.tag
        let doc = self.segmentType == .issued ? self.arrIssued[tag] : self.arrUploaded[tag]//self.arrDataSource[tag]
        if doc.localPath.isEmpty {
            return
        }
        if doc.type == DocType.folder.rawValue {
            self.pushToSubHomeVC(doc, segment: self.segmentType, layout: self.layoutState, sortType: self.getSortyKeyWith(self.segmentType))
            return
        }
//        var localData :Data? = nil
//        if let url = URL(fileURLWithPath: doc.localPath) as? URL {
//            do {
//                localData = try Data.init(contentsOf: url)//init(contentsOf: url, options: .alwaysMapped)
//            }
//            catch {
//                print("error Data INit --\(doc.localPath) with error --\(error)")
//            }
//        }
       // self.openDocumentfile(localData, doc: doc)
        let documentVC = UIDocumentInteractionController(url: URL(fileURLWithPath: doc.localPath))
        documentVC.name = doc.name
        documentVC.delegate = self
        documentVC.presentPreview(animated: true)
//        if localData == nil  {
//        }
    }
}
extension DigiLockerHomeVC :UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isTransitionAvailable = false
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        isTransitionAvailable = true
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
    
}
//MARK:-  UIDocumentInteractionController delegates


extension DigiLockerHomeVC: UIDocumentInteractionControllerDelegate,UINavigationControllerDelegate
{
   func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
    return self
  }
}
//MARK:- **** UIImagePickerControllerDelegate Upload Files ***

extension DigiLockerHomeVC : UIImagePickerControllerDelegate
{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
       var fileName = ""
      if let imageURL = info[UIImagePickerControllerReferenceURL] as? URL {
            let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
            let asset  = result.firstObject
           fileName = asset?.value(forKey: "filename") as? String ?? ""
        }
     if let beforeCrop = info[UIImagePickerControllerOriginalImage] as? UIImage {
         let imageData = UIImageJPEGRepresentation(beforeCrop, 1)
       
        fileName  = fileName.isEmpty ? "Image" + Date().getString()! : fileName
        if imageData!.getSize() > 10.0 {
            return
        }
        let task = self.uploadFileWith(imageData!, path: fileName)
           // task.localPath = tuple!.path
            task.segment = self.segmentType
            task.uploading = true
            task.uploadTask!.progress = 0.0
            self.uploadTask(task)
        }
        self.dismiss(animated: true, completion: nil)
     }
    func uploadTask(_ doc:LockerDocument){
        var task = doc
        task.uploading = true
        task = self.saveUploadTaskToArray(doc)
        let sort = self.getSortyKeyWith(task.segment!)
        self.sortDocumentsList(sort, segment: task.segment!)
        let index = self.checkTaskinArrayUpload(doc: doc)
        self.reloadCollectionOnMain()
        task.index = index
    DigiSessionManager.instance.dicUploadTaks[task.taskDescription] = task
        if  task.uploadTask?.completionHandler != nil  {
            task.uploadTask?.completionHandler = { [weak self] in
                switch $0 {
                case .failure(let error):
                    print(error)
                //self?.
                case .success(let doc):
                    self?.uploadSuccess(doc)
                }
            }
        }
       if  task.uploadTask?.progressHandler != nil  {
            task.uploadTask?.progressHandler = { [weak self] in
            self?.uploadProgress($0)
          }
        }
      
        //self.uploadProgress(task)
    }
    func uploadProgress(_ doc:LockerDocument)  {
        if doc.segment! == segmentType
        {
           // self.saveTaskToArray(doc)
            let index = self.checkTaskinArrayUpload(doc: doc)
            if let cell = self.collDocuments.cellForItem(at:index) as? CellDocumentList
            {
        cell.updateProgressOnMainQueue(Double(doc.uploadTask!.progress))
            }
        }
    }
    func checkTaskinArrayUpload( doc:LockerDocument) -> IndexPath {
        var index : Int = 11110
        if doc.segment != nil {
            switch doc.segment! {
            case .issued:
                index = self.arrIssued.indexOf(object: doc)
                 if  index != 11110 {
                    self.arrIssued[index] = doc
                 }
            case .uploaded:
                  index = self.arrUploaded.indexOf(object: doc)
                 if index != 11110 {
                    self.arrUploaded[index] = doc
                }
            }
        }
        return IndexPath(item: index, section: 0)
    }
    func uploadSuccess(_ doc:LockerDocument) {
        if DigiSessionManager.instance.dicUploadTaks.count != 0 {
            return
        }
       // DispatchQueue.main.async { [weak self] in
            self.refreshDocumentView()
        //}
    }
    func saveUploadTaskToArray(_ doc:LockerDocument) -> LockerDocument {
        var docR = doc
        if docR.segment != nil {
            switch docR.segment! {
            case .issued:
                self.arrIssued.insert(docR, at: 0)
              //  self.arrIssued.append(docR)
              self.arrIssued.first?.index = IndexPath(item: 0, section: 0)
                docR = self.arrIssued.first ?? docR
            case .uploaded:
                self.arrUploaded.insert(docR, at: 0)
                //  self.arrIssued.append(docR)
                self.arrUploaded.first?.index = IndexPath(item: 0, section: 0)
                docR = self.arrUploaded.first ?? docR
//                self.arrUploaded.append(docR)
//             self.arrUploaded.last?.index = IndexPath(item: self.arrUploaded.count - 1, section: 0)
//                docR = self.arrUploaded.last ?? docR
            }
        }
        return docR
    }
}
extension UIViewController {
    func pushToSubHomeVC(_ doc:LockerDocument, segment:SegmentType, layout:LayoutState, sortType:SortType)  {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DigiLockerSubHomeVC") as! DigiLockerSubHomeVC
        vc.sortType = sortType//self.getSortyKeyWith(self.segmentType)
        vc.layoutState = layout//self.layoutState
        vc.folderId = doc.id
        vc.strTitle = doc.name
        vc.segmentType = segment//self.segmentType
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func pushToSearchVC(_ segment:SegmentType,layout:LayoutState,arrData:[LockerDocument])  {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DigilockerSearchVC") as! DigilockerSearchVC
        //vc.sortType = sortType//self.getSortyKeyWith(self.segmentType)
        vc.layoutState = layout//self.layoutState
        vc.arrDocuments = arrData
        vc.segmentType = segment//self.segmentType
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func downloadDocumentFile(_ doc:LockerDocument) -> LockerDocument?
    {
         let singleton = SharedManager.sharedSingleton() as! SharedManager
        var dictBody = [AnyHashable:Any]()//NSMutableDictionary()
        let CurrentTime: Double = CACurrentMediaTime()
        var timeInMS = "\(CurrentTime)"
        timeInMS = timeInMS.replacingOccurrences(of: ".", with: "")
        var accessTokenString = UserDefaults.standard.value(forKey: "AccessTokenDigi") as? String
        accessTokenString = accessTokenString?.count == 0 ? "" : accessTokenString
        dictBody["trkr"] = timeInMS
        dictBody["ort"] = "rgtadhr"
        dictBody["rc"] = "Y"
        dictBody["mec"] = "Y"
        dictBody["tkn"] = singleton.user_tkn
        dictBody["utkn"] = accessTokenString
        print("Dictionary is \(dictBody)")
        dictBody["uri"] = doc.uri
        dictBody["did"] = singleton.appUniqueID()
        var selectedLanguage = UserDefaults.standard.object(forKey: "PreferedLocale") as? String
        if selectedLanguage == nil {
            selectedLanguage = "en"
        }
        dictBody["lang"] = selectedLanguage
        dictBody["mod"] = "app"
        let task =  DigiSessionManager.instance.downloadFileWith(requestBody: dictBody, doc: doc)//digiDownloadFile(tokenRequired: true, apiURL: UM_API_DOWNLOAD_DOC, isPost: true, requestBody: dictBody, successBlock: nil, failerBlock: nil, doc: doc)
        return task
    }
    
func openDocumentfile(_ pdfDocumentData: Data?,doc:LockerDocument)
{
    ( SharedManager.sharedSingleton() as! SharedManager).traceEvents("Open FAQWeb", withAction: "Clicked", withLabel: "DigiLocker Upload", andValue: 0)
        //NSLog(@"OpenWebViewFeedback");
    print("doc Opened--\(doc)")
//    let storyboard = UIStoryboard(name: "Main", bundle: nil)
//    let vc = storyboard.instantiateViewController(withIdentifier: "FAQWebVC") as! FAQWebVC
//    vc.pdfData = pdfDocumentData
//    vc.titleOpen = doc.name
//    vc.urltoOpen = ""
//    vc.isfrom = "WEBVIEWHANDLEPDFOPEN"
//    vc.file_name = doc.localPath
//   // let type = self.checkFileTypes(mime: doc.mime)
//    vc.file_type = doc.mime
//    print("file type --\(vc.file_type)")
//    vc.file_shareType = "ShareYes"
//    vc.modalTransitionStyle = .crossDissolve
//    self.navigationController?.present(vc, animated: false, completion: nil)
  }
    func getUploadFilePath(_ image:UIImage, name:String) -> (path:String, data:Data)? {
        let documentDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        var logsPath = ""
        if let documentDirectoryPath = documentDirectoryPath
        {
        // create the custom folder path
        logsPath = documentDirectoryPath.appending("/DigiUploadFolder")
        let fileManager = FileManager.default
        if !fileManager.fileExists(atPath: logsPath)
        {
        do {
            try fileManager.createDirectory(atPath: logsPath,withIntermediateDirectories: false,attributes: nil)
        } catch {
        print("Error creating images folder in documents dir: \(error)")
           }
          }
        }
        // Get documents folder
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let dateString: String = dateFormatter.string(from: Date())
        let fileName = "\(logsPath)/Image-\(dateString).jpeg"
        let imageData = UIImageJPEGRepresentation(image, 1)
        let base64String: String = imageData!.base64EncodedString()
        let base64data = Data.init(base64Encoded: base64String)
        do {
        try base64data!.write(to: URL(fileURLWithPath: fileName), options: .atomic)
        return (fileName,base64data!)
        } catch {
        print("base64data save digiFolder--=\(error)")
        }
        return nil
    }
    
    func uploadFileWith(_ data:Data, path:String) -> LockerDocument
    {
        
        var dictBody = [AnyHashable: Any]()
        let singleton = SharedManager.sharedSingleton() as! SharedManager
        let CurrentTime: Double = CACurrentMediaTime()
        var timeInMS = "\(CurrentTime)"
        timeInMS = timeInMS.replacingOccurrences(of: ".", with: "")
        var accessTokenString = UserDefaults.standard.value(forKey: "AccessTokenDigi") as? String
        accessTokenString = accessTokenString?.count == 0 ? "" : accessTokenString
       //// let fileName: String? = path.components(separatedBy: "/").last
        //let path = "/" + fileName!
        dictBody["trkr"] = timeInMS
        dictBody["tkn"] = singleton.user_tkn
        dictBody["utkn"] = accessTokenString
        dictBody["clength"] = "\(data.count)"
        dictBody["path"] = path
        dictBody["ctype"] = "image/jpeg"
        dictBody["file"] = data
        dictBody["did"] = singleton.appUniqueID()
        var selectedLanguage = UserDefaults.standard.object(forKey: "PreferedLocale") as? String
        if selectedLanguage == nil {
            selectedLanguage = "en"
        }
        dictBody["lang"] = selectedLanguage
        dictBody["mod"] = "app"
        
        var dicLocker = [String:String]()
        dicLocker["size"] = "\(data.count)"
        dicLocker["type"] = DocType.file.rawValue
        let n = Int(arc4random_uniform(42))
        dicLocker["uri"]  = path + "\(n)"
        dicLocker["mime"] = "image/jpeg"
        var nameFile = path
        if path.contains("/") {
            nameFile = path.components(separatedBy: "/").last!
        }
        dicLocker["name"] = nameFile
        let doc = LockerDocument(dicLocker: dicLocker)
        let task =  DigiSessionManager.instance.uploadFileWith(requestBody: dictBody, doc: doc)
        task?.date = Date()
        return task!
    }
    func getSortyKeyWith(_ type:SegmentType) -> SortType {
        let key = type == .issued ? kIssueSortKey : kUploadSortKey
        let issueSortKey =  UserDefaults.standard.value(forKey:key ) as? Int ?? SortType.date.rawValue
        return  SortType.init(rawValue: issueSortKey) ?? SortType.date
    }
}

