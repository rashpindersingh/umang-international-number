
//
//  HomeDetailVC.m
//  Umang
//
//  Created by spice_digital on 29/09/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "HomeDetailVC.h"
#import "sliderRecentCell.h"
#import "UIImageView+WebCache.h"
#import "itemMoreInfoVC.h"
#import "UMAPIManager.h"
#import "NSString+MD5.h"
//#import "NSData+MD5.h"
#import "MBProgressHUD.h"
#import "DetailServiceNewVC.h"
#import "Umang-Swift.h"

#import "FAQWebVC.h"
#import "UIView+Toast.h"
#import "StateList.h"
#import "AdvanceSearchVC.h"

#import <CoreLocation/CoreLocation.h>
#import "Base64.h"
#import "LoginAppVC.h"


#import "downloadBookTableViewCell.h"

#import "AppDelegate.h"
#import "LiveChatVC.h"
#import "DeptAudioRecordVC.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import "BLSelectPhotosAndVideosVC.h"
#import "BLPhoto.h"
#import "BLMediaItem.h"
#import <ImageIO/ImageIO.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "DetailServiceVC.h"
#import "PDFImageConverter.h"
#import "RunOnMainThread.h"
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/UTCoreTypes.h>
static int totalviewload=0;

@interface BackgroundView : UIView<UIDocumentInteractionControllerDelegate>
@property(nonatomic,retain)UIDocumentInteractionController *documentInteractionController;
@end

@implementation BackgroundView

+ (Class)layerClass
{
    return [CAShapeLayer class];
}
@end


@interface HomeDetailVC ()<UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate,NSURLSessionDelegate,MFMailComposeViewControllerDelegate,UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UIDocumentInteractionControllerDelegate,NSURLSessionDelegate,AudioBase64Delegate,SelectImageVideoBase64Delegate,MFMessageComposeViewControllerDelegate>
{
    
    CLLocationManager *locationManager;
    
    
    IBOutlet UIImageView *img_underConst;
    
    __weak IBOutlet UILabel *lbl_underConstruction;
    
    
    IBOutlet UIButton *btnRecent;
    SharedManager*  singleton ;
    itemMoreInfoVC *itemMoreVC;
    MBProgressHUD *hud;
    NSData* data ;
    NSURL *postURI;
    __weak IBOutlet UIButton *btnViewonMap;
    __weak IBOutlet UILabel *lblRecentlyService;
    
    __weak IBOutlet UILabel *lblUmangHome;
    __weak IBOutlet UIButton *btnShare;
    __weak IBOutlet UIButton *btnRate;
    
    __weak IBOutlet UIButton *btn_livechat;
    __weak IBOutlet UIButton *btnInfo;
    
    bool flagBack;
    
    bool flagViewAppear;
    
    //------------profile parameter---------------
    //----- local values  to be save and show from API-----------
    NSString *str_name;
    NSString *str_gender;
    NSString *str_dob;
    NSString *str_qualification;
    NSString *str_occupation;
    NSString *str_state;
    NSString *str_district;
    NSString *str_registerMb;
    NSString *str_emailAddress;
    NSString *str_alternateMb;
    NSString *str_emailVerifyStatus;
    NSString *str_amnosVerifyStatus;
    NSString *str_address;
    
    __weak IBOutlet UIButton *btnBack;
    NSString *str_Url_pic;
    
    NSMutableArray *socialpd;
    
    
    
    StateList *obj;
    
    
    NSMutableArray *statelocalPlist;
    
    CGRect vw_recentFrame;
    BOOL flag_homebtn;
    //------------End parameter--------
    
    
    UIActivityIndicatorView *activityIndicator ;
    
    
    
    //===========ebook==========
    
    BOOL downloadCheck;
    NSMutableData *receivedData;
    long long expectedBytes;
    NSString *currentebookURL;
    double currentPercentvalue;
    
    NSURLSessionDownloadTask *downtask;
    NSMutableArray*selectedButonsToPlay;
    
    NSMutableArray*downloadCancel;
    AppDelegate *delegate ;
    
    
    
    NSMutableArray *akpsImageDataArray;
    NSString *fileSizeString;
    
    IBOutlet UIButton *btnMenu;
    
    NSInteger cropWidth;
    NSInteger cropHeight;
    
    BOOL needCrop;
}
@property(nonatomic, assign) int downloadFileno;

@property(nonatomic, retain)NSString* cameraTag;

@property(nonatomic, retain)NSString* callBackresponse;
@property(nonatomic, retain)NSDictionary* bookJson;


//--------Record video------------
@property(nonatomic,retain)NSString *videoSuccessCallBack;
@property(nonatomic,retain)NSString *videoFailCallBack;
//--------image video------------
@property(nonatomic,retain)NSString *imageSuccessCallBack;
@property(nonatomic,retain)NSString *imageFailCallBack;
//--------Record Audio------------
@property(nonatomic,retain)NSString *AudioSuccessCallBack;
@property(nonatomic,retain)NSString *AudioFailCallBack;

//==========ebook============


@property(nonatomic,retain)UIDocumentInteractionController *documentInteractionController;
//------ Camera Handling------------
@property(nonatomic,retain)NSString *camera_failCallback;
@property(nonatomic,retain)NSString *camera_successCallback;
@property(nonatomic,retain)NSString *camera_img_base64;

@property(nonatomic,retain)NSString *str_gender;

//------End Camera Handling--------

@property(nonatomic,retain)NSString *SERVICE_ID_to_Pass;

@property(nonatomic,retain)IBOutlet UIWebView *webView;
@property(nonatomic,assign)int iTag;
@property(nonatomic,retain)NSArray *tableData;
@property(nonatomic,retain)NSString*service_ID;

@property(nonatomic,retain)NSString*locationResponse;
@property(nonatomic,retain)NSString*loadingStatus;


@property(nonatomic,retain)NSString*mainDocumentURL;

@end

@implementation HomeDetailVC
@synthesize titleStr;
@synthesize urlString;
@synthesize dic_serviceInfo;

@synthesize tagComeFrom;
@synthesize service_ID;
@synthesize SERVICE_ID_to_Pass;

@synthesize locationResponse;
@synthesize loadingStatus;


@synthesize camera_failCallback;
@synthesize camera_successCallback;
@synthesize camera_img_base64;
@synthesize str_gender;
@synthesize mainDocumentURL;


@synthesize cameraTag;



//==============ebook=============
@synthesize downloadBookArry;
@synthesize downloadFileno;
@synthesize lbl_downloadbook;
@synthesize lbl_downloadcomplete;
@synthesize vw_progressBar;
@synthesize callBackresponse;
@synthesize bookJson;
//=============ebook=============

@synthesize videoSuccessCallBack,videoFailCallBack;
@synthesize imageSuccessCallBack,imageFailCallBack;
@synthesize AudioSuccessCallBack,AudioFailCallBack;


//==== state id pass value====
@synthesize isStateSelected;
@synthesize state_nametopass;
@synthesize state_idtopass;
//==== state id pass value====




- (void)viewDidUnload
{
    self.webView = nil;
    
    [super viewDidUnload];
}

//-------- recent view home button action --------
-(IBAction)btn_home_vw_recent:(id)sender
{
    flagBack=TRUE;
    // vw_recent.hidden=TRUE;
    [self rightoleftClose];
    
    
    // [self dismissViewControllerAnimated:NO completion:nil];
    
    
    //NSInteger currentIndexOfTab=[singleton getSelectedTabIndex];
    //   NSString *selectedTab = [[NSUserDefaults standardUserDefaults] objectForKey:@"SELECTED_TAB"];
    
    /*
     UITabBarController *tbc = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
     tbc.selectedIndex=0;
     
     tbc.selectedIndex=currentIndexOfTab;
     [self presentViewController:tbc animated:NO completion:nil];
     */
    
    
    int defaulttab= [[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_TAB_INDEX"];
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SELECTED_TAB_INDEX"];
    [[NSUserDefaults standardUserDefaults]  synchronize];
    dispatch_async(dispatch_get_main_queue(), ^{
        //here task #1 that takes 10 seconds to run
        //self.window.rootViewController = nil;
        UIStoryboard *storyboard  = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
        tbc.selectedIndex=[singleton getSelectedTabIndex];
        [self presentViewController:tbc animated:NO completion:nil];
        
        // self.window.rootViewController = tbc;
        //[UIView transitionWithView:self.window duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:nil];
        
        NSLog(@"Task #1 finished");
    });
    NSLog(@"Task #1 scheduled");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSString *defaulttabstr=[NSString stringWithFormat:@"%d",defaulttab];
        [self performSelector:@selector(resetDefaultTab:) withObject:defaulttabstr afterDelay:4];
        //here task #2 that takes 5s to run
        NSLog(@"Task #2 finished");
        
    });
    
    
    // SharedManager * singleton = [SharedManager sharedSingleton];
    /* if ([selectedTab isEqualToString:NSLocalizedString(@"home_small", nil)]) {
     [self dismissViewControllerAnimated:NO completion:nil];
     }
     else
     {
     UITabBarController *tbc = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
     tbc.selectedIndex=currentIndexOfTab;
     [self presentViewController:tbc animated:NO completion:nil];
     }
     */
    
    
    
}

-(void)resetDefaultTab:(NSString*)tabstrnumber
{
    
    int tabnumber=[tabstrnumber intValue];
    SharedManager * sharedMySingleton = [SharedManager sharedSingleton];
    
    [[NSUserDefaults standardUserDefaults] setInteger:tabnumber forKey:@"SELECTED_TAB_INDEX"];
    sharedMySingleton.tabSelectedIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_TAB_INDEX"];
    [[NSUserDefaults standardUserDefaults]  synchronize];
    
}


//-------- menu view Map button action --------

-(IBAction)btn_map_vw_menu:(id)sender
{
    vw_menu.hidden=TRUE;
    
    
    NSString *latitude=[NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_LATITUDE"]];
    
    NSString *longitute=[NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_LONGITUDE"]];
    
    NSString *deptName=[NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_NAME"]];
    deptName = [deptName stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    
    
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://"]])
        
    {
        NSLog(@"Map App Found");
        
        NSString* googleMapsURLString = [NSString stringWithFormat:@"comgooglemaps://?q=%.6f,%.6f(%@)&center=%.6f,%.6f&zoom=15&views=traffic",[latitude doubleValue], [longitute doubleValue],deptName,[latitude doubleValue], [longitute doubleValue]];
        
        // googleMapsURLString = [googleMapsURLString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]; //IOS 9 and above use this line
        
        NSURL *mapURL=[NSURL URLWithString:[googleMapsURLString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
        NSLog(@"mapURL= %@",mapURL);
        
        [[UIApplication sharedApplication] openURL:mapURL];
        
        
    } else
    {
        NSString* googleMapsURLString = [NSString stringWithFormat:@"https://maps.google.com/maps?&z=15&q=%.6f+%.6f&ll=%.6f+%.6f",[latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
        
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:googleMapsURLString]];
        
    }
    
}

-(IBAction)btn_searchAction:(id)sender
{
    flagViewAppear=FALSE;
    
    AdvanceSearchVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AdvanceSearchVC"];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self presentViewController:vc animated:NO completion:nil];
    
    
}
-(IBAction)btn_information:(id)sender
{
    /* vw_menu.hidden=TRUE;
     flagViewAppear=FALSE;
     
     UIStoryboard* storyboard = [UIStoryboard storyboardWithName: kDetailServiceStoryBoard bundle:nil];
     // UIStoryboard *storyboard = [self grabStoryboard];
     //#import "DetailServiceNewVC.h"
     DetailServiceNewVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DetailServiceNewVC"];
     
     vc.dic_serviceInfo=dic_serviceInfo;//change it to URL on demand
     vc.isFrom=@"DEPARTMENT";
     //[topvc.navigationController pushViewController:vc animated:YES];
     
     [self presentViewController:vc animated:NO completion:nil];*/
    
    vw_menu.hidden=TRUE;
    flagViewAppear=FALSE;
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName: kDetailServiceStoryBoard bundle:nil];
    // UIStoryboard *storyboard = [self grabStoryboard];
    //#import "DetailServiceNewVC.h"
    DetailServiceNewVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DetailServiceNewVC"];
    
    vc.dic_serviceInfo=dic_serviceInfo;//change it to URL on demand
    vc.isFrom=@"DEPARTMENT";
    //[topvc.navigationController pushViewController:vc animated:YES];
    
    [self presentViewController:vc animated:NO completion:nil];
    
}




//-------- menu view Rate button action --------

-(IBAction)btn_rate_vw_menu:(id)sender
{
    
    /*  flagViewAppear=FALSE;
     
     vw_menu.hidden=TRUE;
     UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
     // UIStoryboard *storyboard = [self grabStoryboard];
     //#import "DetailServiceNewVC.h"
     DetailServiceNewVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DetailServiceNewVC"];
     
     vc.dic_serviceInfo=dic_serviceInfo;//change it to URL on demand
     UIViewController *topvc=[self topMostController];
     //[topvc.navigationController pushViewController:vc animated:YES];
     
     [topvc presentViewController:vc animated:NO completion:nil];
     */
    
    flagViewAppear=FALSE;
    
    vw_menu.hidden=TRUE;
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName: kDetailServiceStoryBoard bundle:nil];
    // UIStoryboard *storyboard = [self grabStoryboard];
    //#import "DetailServiceNewVC.h"
    DetailServiceNewVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DetailServiceNewVC"];
    
    vc.dic_serviceInfo=dic_serviceInfo;//change it to URL on demand
    UIViewController *topvc=[self topMostController];
    //[topvc.navigationController pushViewController:vc animated:YES];
    
    [topvc presentViewController:vc animated:NO completion:nil];
    
    
    
}



//----------- END OF MORE INFO POP UP VIEW---------------

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
        
        NSLog(@"topController name = %@",topController);
        
    }
    
    return topController;
}






- (void)showSMS:(NSString*)file
{
    NSString *textToShare =file;
    // NSURL *myWebsite = [NSURL URLWithString:@"http://www.umang.com/"];
    
    NSArray *objectsToShare = @[textToShare];
    [[UIPasteboard generalPasteboard] setString:textToShare];
    
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    //if iPhone
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [self presentViewController:controller animated:YES completion:nil];
    }
    //if iPad
    else {
        // Change Rect to position Popover
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:controller];
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
    
}

//-------- menu view Share button action --------

-(IBAction)btn_share_vw_menu:(id)sender
{
    vw_menu.hidden=TRUE;
    
    [self hitFetchdeptMsg];
}

-(void)hitFetchdeptMsg
{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSString *serviceID=[dic_serviceInfo valueForKey:@"SERVICE_ID"];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:serviceID forKey:@"sid"];//Enter mobile number of user
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"st"];  //get from mobile default email //not supported iphone
    
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [dictBody setObject:@"" forKey:@"lang"];  //get from mobile default email //not supported iphone
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_FETCH_DPT_MSG withBody:dictBody andTag:TAG_REQUEST_FETCH_DPT_MSG completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        if (error == nil) {
            //NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            //            NSString *rc=[response valueForKey:@"rc"];
            //            NSString *rs=[response valueForKey:@"rs"];
            
            NSArray  *listMsg=[[response valueForKey:@"pd"]valueForKey:@"listMsg"];
            
            //NSLog(@"value of rc =%@ \n value of rs=%@ \n value of listMsg=%@ ",rc,rs,listMsg);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                
                NSString *selectedFile=@"";
                if ([listMsg count]>0)
                {
                    @try
                    {
                        selectedFile = [[listMsg objectAtIndex:0] valueForKey:@"deptMsg"];
                    }
                    @catch (NSException *exception)
                    {
                        
                    }
                    @finally
                    {
                        
                    }
                    
                }
                [self showSMS:selectedFile];
                
            }
            
        }
        else{
            //NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}
//-------- Support view Call button action --------

-(IBAction)btn_call_vwSupport:(id)sender
{
    vw_support.hidden=TRUE;
    
    //NSLog(@"CONTACT=%@", [NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_PHONE_NUMBER"]]);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Contact"
                                                    message:@"Sure to call?"
                                                   delegate:self
                                          cancelButtonTitle:@"CANCEL"
                                          otherButtonTitles:@"OK", nil];
    alert.tag=101;
    [alert show];
    
}
//-------- Support view Chat button action --------


-(IBAction)btn_chat_vwSupport:(id)sender
{
    vw_menu.hidden=TRUE;
    flagViewAppear=FALSE;
    
    /*  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry!"
     message:@"Not available in this version!"
     delegate:self
     cancelButtonTitle:nil
     otherButtonTitles:@"OK", nil];
     [alert show];*/
    
    
    /*NSString *javascriptString = [NSString stringWithFormat:@"openChat();"];
     [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];*/
    
    /* NSString *javascriptString = [NSString stringWithFormat:@"openChat();"];
     [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
     */
    // force need to reset the home tab as it effects to load service twice
    //int defaulttab= [[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_TAB_INDEX"];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SELECTED_TAB_INDEX"];
    [[NSUserDefaults standardUserDefaults]  synchronize];
    
    
    //XMPPChatViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"XMPPChat"];
    
    /* XMPPChatViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"XMPPChat"];
     controller.comingFromString=@"Department";
     //tbc.selectedIndex=3;
     [self presentViewController:controller animated:NO completion:nil];*/
    
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Chat" bundle:nil];
    LiveChatVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LiveChatVC"];
    vc.hidesBottomBarWhenPushed = YES;
    vc.comingFromString = @"Department";
    //[self.navigationController pushViewController:vc animated:YES];
    [self presentViewController:vc animated:NO completion:nil];
}

-(void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100 && buttonIndex == 1)
    { // handle the altdev
        
        NSString *numberString = [dic_serviceInfo valueForKey:@"SERVICE_PHONE_NUMBER"];
        
        numberString = [numberString stringByReplacingOccurrencesOfString:@" "  withString:@""];
        
        NSString *mobileNumber=[NSString stringWithFormat:@"telprompt://%@",numberString];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mobileNumber]];
        
    }
    else if (alertView.tag == 101 && buttonIndex == 1)
    { // handle the donate
        NSString *url=[NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_WEBSITE"]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        
        
    }
}



-(void)addShadowToTheView:(UIView*)vwItem
{
    vwItem.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    vwItem.layer.shadowColor = [UIColor grayColor].CGColor;
    vwItem.layer.shadowRadius = 3;
    vwItem.layer.shadowOpacity = 0.5;
    vwItem.layer.cornerRadius = 3.0;
}

-(IBAction)bgtouch:(id)sender
{
    //do noting
}
-(IBAction)backtouch:(id)sender
{
    
    vw_support.hidden=TRUE;
    vw_menu.hidden=TRUE;
    // vw_recent.hidden=TRUE;
    [self rightoleftClose];
    
    [self backBtnAction:self];
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


-(void)getLocation
{
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"value of totalviewload=%d",totalviewload);
    
    
    akpsImageDataArray = [NSMutableArray new];
    
    cameraTag=@"";
    
    
    
    lbl_downloadingtxt.text=NSLocalizedString(@"downloading_ebook", nil);
    //lbl_download.text=@"";
    //self.webView=nil;
    //self.webView=[[UIWebView alloc]init];
    //=======ebook========
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    selectedButonsToPlay=[NSMutableArray new];
    vw_progressBar.transform = CGAffineTransformMakeScale(1.0, 5.0);
    
    delegate.downloadComplete=[NSMutableArray new];
    delegate.downloadFilesArry=[NSMutableArray new];
    delegate.downloadBookNo=0;
    
    
    
    downloadBookArry=[NSMutableArray new];
    downloadCancel=[NSMutableArray new];
    downloadFileno=0;
    
    
    vw_filedownloadManager.hidden=TRUE;
    vw_download.hidden=FALSE;
    
    [self StartDownloadingPress];
    
    downloadCheck=TRUE;
    [self isDownloadingProgress];
    
    tbldonwload.tag=909;
    tbl_recent.tag=808;
    tbldonwload.delegate=self;
    tbldonwload.dataSource=self;
    
    tbl_recent.delegate=self;
    tbl_recent.dataSource=self;
    
    
    //=======ebook========
    
    
    [self loadviewBase];
    
    self.webView.scrollView.bounces = NO;
    
    UISwipeGestureRecognizer *rtlGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(openRecentDrawer:)];
    rtlGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.webView addGestureRecognizer:rtlGesture];
}

#pragma mark - Recent Drawer Animations

-(void)closeRecentDrawer:(UISwipeGestureRecognizer *)gesture
{
    //------- Start Remove blur effect----------------
    UIView *removeView;
    while((removeView = [self.view viewWithTag:1010]) != nil) {
        [removeView removeFromSuperview];
    }
    
    [UIView animateWithDuration:0.3
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         //vw_recent.frame = vw_recentFrame;
                         vw_recent.frame = CGRectMake(fDeviceWidth+100, 20, 100, fDeviceHeight-20);
                         
                     }
                     completion:^(BOOL finished){
                         if (finished)
                             // [vw_recent removeFromSuperview];
                             NSLog(@"dd");
                     }];
}


-(void)openRecentDrawer:(UISwipeGestureRecognizer *)gesture
{
    //------- Start Remove blur effect----------------
    UIView *removeView;
    while((removeView = [self.view viewWithTag:1010]) != nil) {
        [removeView removeFromSuperview];
    }
    vw_support.hidden=FALSE;
    //-------End Remove blur effect----------------
    
    
    //------- Start Adding blur effect----------------
    UIView *viewBg=[[UIView alloc]initWithFrame:self.view.frame];
    viewBg.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.5];
    viewBg.tag=1010;
    [self.webView addSubview:viewBg];
    //------- End Adding blur effect----------------
    
    
    UISwipeGestureRecognizer *ltrGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(closeRecentDrawer:)];
    ltrGesture.direction = UISwipeGestureRecognizerDirectionRight;
    [viewBg addGestureRecognizer:ltrGesture];
    
    
    vw_support.hidden=FALSE;
    [tbl_recent reloadData];
    
    [UIView animateWithDuration:0.3
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         //vw_recent.frame = vw_recentFrame;
                         vw_recent.frame = CGRectMake(fDeviceWidth-100, 20, 100, fDeviceHeight-20);
                         
                     }
                     completion:^(BOOL finished){
                         if (finished)
                             // [vw_recent removeFromSuperview];
                             NSLog(@"dd");
                     }];
}

#pragma mark -

-(void)loadviewBase
{
    //[self dismissViewControllerAnimated:YES completion:nil];
    
    activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);
    [self.view addSubview: activityIndicator];
    
    
    
    
    vw_recentFrame=vw_recent.frame;//
    [self recentBtnAction:self];
    [self handleTap];
    //--------------------------forcefully clear the cache to work it------
    //------------------------- Added by me to test Encrypt Value------------------------
    //--------------------------xxxxxxxxxx-------------------------
    
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    // Encrypt
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSHTTPCookie *cookie;
    for (cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    NSMutableArray *cookieArray = [[NSMutableArray alloc] init];
    [[NSUserDefaults standardUserDefaults] setValue:cookieArray forKey:@"cookieArray"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    //------- Added by me to remove cache to work chat but cause automatic load issues--------
    //------------------------------xxxxxxxxxx-------------------------------------------------
    //-----------------------------End it need a solution for it -------------------------
    
    
    
    
    flag_homebtn=FALSE;
    singleton = [SharedManager sharedSingleton];
    // _lbltitle.adjustsFontSizeToFitWidth = YES;
    
    obj=[[StateList alloc]init];
    [obj hitStateQualifiAPI];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        // [self hitFetchProfileAPI];
        
    });
    
    
    
    
    locationManager = [[CLLocationManager alloc] init];
    flagViewAppear=TRUE;
    
    
    //[self getLocation];
    
    loadingStatus=@"false";//by default it false
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: HOME_DETAIL_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    _webView.delegate = self;
    _webView.scalesPageToFit = YES;
    self.web_back_btn.enabled=false;
    
    /*NSAssert((self.back.target == self.webView) && (self.back.action = @selector(goBack)), @"Your back button action is not connected to goBack.");
     NSAssert((self.stop.target == self.webView) && (self.stop.action = @selector(stopLoading)), @"Your stop button action is not connected to stopLoading.");
     NSAssert((self.refresh  .target == self.webView) && (self.refresh.action = @selector(reload)), @"Your refresh button action is not connected to reload.");
     NSAssert((self.forward.target == self.webView) && (self.forward.action = @selector(goForward)), @"Your forward button action is not connected to goForward.");
     NSAssert(self.webView.scalesPageToFit, @"You forgot to check 'Scales Page to Fit' for your web view.");*/
    
    // Do any additional setup after loading the view, typically from a nib.
    
    
    
    [btnRate setTitle:NSLocalizedString(@"rate", nil) forState:UIControlStateNormal];
    [btnViewonMap setTitle:NSLocalizedString(@"view_on_map", nil) forState:UIControlStateNormal];
    [btnShare setTitle:NSLocalizedString(@"share", nil) forState:UIControlStateNormal];
    [btn_livechat setTitle:NSLocalizedString(@"help_live_chat", nil) forState:UIControlStateNormal];
    
    [btnInfo setTitle:NSLocalizedString(@"information", nil) forState:UIControlStateNormal];
    
    
    
    
    
    
    lblUmangHome.text = NSLocalizedString(@"umang_home", nil);
    lblRecentlyService.text = NSLocalizedString(@"recently_used_services", nil);
    
    
    
    
    
    
    //vc.tagComeFrom=@"NOTIFICATIONVIEW";
    
    if ([tagComeFrom isEqualToString:@"ADVANCESEARCH"])
    {
        
        SERVICE_ID_to_Pass=[dic_serviceInfo valueForKey:@"DEPT_ID"];
        titleStr=[dic_serviceInfo valueForKey:@"SERVICE_NAME"];
        _lbltitle.text = titleStr;
        [_backBtnHome setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
        urlString=[dic_serviceInfo valueForKey:@"URL"];
        
        
    }
    
    else if ([tagComeFrom isEqualToString:@"NOTIFICATIONVIEW"]) {
        
        SERVICE_ID_to_Pass=[dic_serviceInfo valueForKey:@"SERVICE_ID"];
        titleStr=[dic_serviceInfo valueForKey:@"NOTIF_TITLE"];
        _lbltitle.text = titleStr;
        [_backBtnHome setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
        urlString=[dic_serviceInfo valueForKey:@"NOTIF_URL"];
        
        
    }
    else if ([tagComeFrom isEqualToString:@"OTHERS"])
        
    {
        SERVICE_ID_to_Pass=[dic_serviceInfo valueForKey:@"SERVICE_ID"];
        titleStr=[dic_serviceInfo valueForKey:@"SERVICE_NAME"];
        _lbltitle.text = titleStr;
        urlString=[dic_serviceInfo valueForKey:@"SERVICE_URL"];
    }
    
    /*  else if ([tagComeFrom isEqualToString:@"BANNERS"])
     
     {
     SERVICE_ID_to_Pass=[dic_serviceInfo valueForKey:@"SERVICE_ID"];
     titleStr=[dic_serviceInfo valueForKey:@"BANNERS"];
     _lbltitle.text = titleStr;
     urlString=[dic_serviceInfo valueForKey:@"SERVICE_URL"];
     }
     */
    
    
    
    
    else
        
    {
        SERVICE_ID_to_Pass=[dic_serviceInfo valueForKey:@"SERVICE_ID"];
        titleStr=[dic_serviceInfo valueForKey:@"SERVICE_NAME"];
        _lbltitle.text = titleStr;
        urlString=[dic_serviceInfo valueForKey:@"SERVICE_URL"];
    }
    
    // Objective-C
    // _lbltitle.lineBreakMode = NSLineBreakByWordWrapping;
    //  _lbltitle.numberOfLines = 0;
    //CGRect currentFrame = _lbltitle.frame;
    // CGSize max = CGSizeMake(_lbltitle.frame.size.width-20, 100);
    // CGSize expected = [titleStr sizeWithFont:_lbltitle.font constrainedToSize:max lineBreakMode:_lbltitle.lineBreakMode];
    //  if (expected.height>35) {
    //  currentFrame.size.height = expected.height;
    
    //    _lbltitle.frame = currentFrame;
    //  }
    
    
    service_ID=SERVICE_ID_to_Pass;
    
    // _lbltitle.adjustsFontSizeToFitWidth = YES;
    
    //--- End Else condition -----------------
    
    NSArray *sectionserviceitem=[singleton.dbManager getServiceData:SERVICE_ID_to_Pass];
    //NSLog(@"sectionserviceitem =%@",sectionserviceitem);
    [_backBtnHome setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
    
    
    
    //  BOOL LangCheck=[singleton.dbManager getServiceLanguage:SERVICE_ID  withDeviceLang:@"en"];
    // NSLog(LangCheck ? @"Yes" : @"No");
    
    
    dic_serviceInfo=[[NSMutableDictionary alloc]init];
    
    if ([sectionserviceitem count]!=0)
    {
        dic_serviceInfo=[[sectionserviceitem objectAtIndex:0]mutableCopy];
        
    }
    @try {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SERVICE_ID contains[c] %@",[dic_serviceInfo valueForKey:@"SERVICE_ID"]]; // if you need case sensitive search avoid '[c]' in the predicate
        
        
        
        NSArray *results = [singleton.arr_recent_service filteredArrayUsingPredicate:predicate];
        
        
        if ([results count]>0) {
            NSUInteger objIdx = [singleton.arr_recent_service indexOfObject: [results objectAtIndex:0]];
            
            NSLog(@"objIdx=%lu",(unsigned long)objIdx);
            [singleton.arr_recent_service removeObjectAtIndex:objIdx];
        }
        
        
        
        [singleton.arr_recent_service addObject:dic_serviceInfo];
        
        
        NSMutableArray *arrayId=[[NSMutableArray alloc]init];
        
        for (int i=0; i<[singleton.arr_recent_service count];i++)
        {
            NSString *currentServiceId=[NSString stringWithFormat:@"%@",[[singleton.arr_recent_service objectAtIndex:i]valueForKey:@"SERVICE_ID"]];
            NSArray *sectionserviceitem=[singleton.dbManager getServiceData:currentServiceId];
            
            [arrayId addObject:[sectionserviceitem objectAtIndex:0]];
            
        }
        
        [singleton.arr_recent_service removeAllObjects];
        
        singleton.arr_recent_service =[arrayId mutableCopy];
        
        //
        
        
        NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:singleton.arr_recent_service];
        NSArray*  myArray = [[NSMutableArray alloc] initWithArray:[orderedSet array]];
        NSMutableArray *reversedArray = [NSMutableArray arrayWithCapacity:4]; // will grow if needed.
        reversedArray = [[[myArray reverseObjectEnumerator] allObjects] mutableCopy];
        if ([reversedArray count]>5) {
            NSArray *tempArray = [reversedArray subarrayWithRange:NSMakeRange(0, 5)];
            self.tableData = tempArray;
        }
        else
        {
            self.tableData = reversedArray;
        }
        
        
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
    
    //NSLog(@"dic_serviceInfo=%@",dic_serviceInfo);
    self.view.backgroundColor=[UIColor whiteColor];
    vw_support.hidden=TRUE;
    vw_menu.hidden=TRUE;
    //vw_recent.hidden=TRUE;
    [self rightoleftClose];
    
    [self addShadowToTheView:vw_support];
    [self addShadowToTheView:vw_menu];
    [self addShadowToTheView:vw_recent];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
    tbl_recent.tableFooterView = [UIView new];//remove empty table rows
    //[self loadHtml];
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView.tag==808)
    {
        
        
        static NSString *simpleTableIdentifier = @"sliderRecentCell";
        
        sliderRecentCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[sliderRecentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        
        if (indexPath.row==0)
        {
            CGRect frame = cell.contentView.frame;
            
            
            frame.size.width=cell.contentView.frame.size.width;
            frame.size.height=cell.contentView.frame.size.width;
            
            
            cell.backgroundView = [[BackgroundView alloc] init];
            
            cell.backgroundView.frame=CGRectMake(2, 5, cell.contentView.frame.size.width-4, cell.contentView.frame.size.height-10);
            
            // cell.contentView.backgroundColor=[UIColor redColor];
            CGFloat corner = 8.0f;
            
            UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:cell.backgroundView.frame byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(corner, corner)];
            
            
            CAShapeLayer  *shapeLayer = (CAShapeLayer *)cell.backgroundView.layer;
            shapeLayer.path = path.CGPath;
            shapeLayer.fillColor = cell.textLabel.backgroundColor.CGColor;
            shapeLayer.strokeColor = [UIColor colorWithRed:6.0/255.0 green:84.0/255.0 blue:141.0/255.0 alpha:1.0].CGColor;
            shapeLayer.lineWidth = 2.0f;
            
            
            float x= shapeLayer.frame.origin.x;
            float y = shapeLayer.frame.origin.y;
            float w= shapeLayer.bounds.size.width;
            float h = shapeLayer.bounds.size.height;
            NSLog(@"Point(%f,%f)",x,y);
            NSLog(@"Size(%f,%f)",w,h);
        }
        cell.backgroundColor=[UIColor clearColor];
        // cell.separatorInset = UIEdgeInsetsMake(0, 1000, 0, 0);
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.contentView.layoutMargins = UIEdgeInsetsMake(0, 10, 0, 10);
        cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
        [tableView setSeparatorColor:[UIColor grayColor]];
        cell.lbl_servicenme.text = [[self.tableData objectAtIndex:indexPath.row] valueForKey:@"SERVICE_NAME"];
        
        
        NSURL *url=[NSURL URLWithString:[[self.tableData objectAtIndex:indexPath.row] valueForKey:@"SERVICE_IMAGE"]];
        
        [cell.img_service sd_setImageWithURL:url
                            placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
        
        cell.lbl_servicenme.font = [AppFont regularFont:14.0];
        return cell;
    }
    else
        
    {
        static NSString *simpleTableIdentifier = @"downloadBookTableViewCell";
        
        
        downloadBookTableViewCell *downloadcell = [tbldonwload dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        downloadcell.accessoryType = UITableViewCellAccessoryNone;
        if (downloadcell == nil)
        {
            downloadcell = [[downloadBookTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        downloadcell.lbl_bookCount.text=[NSString stringWithFormat:@"%ld",(long)indexPath.row+1];
        
        @try {
            downloadcell.lbl_bookName.text=[NSString stringWithFormat:@"%@",[[downloadBookArry objectAtIndex:indexPath.row] valueForKey:@"chapterTitle"]];
            
            NSLog(@"downloadFileno==>>>%d &&&& index=%ld",downloadFileno,(long)indexPath.row);
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        
        
        if (downloadFileno ==indexPath.row)
        {
            downloadcell.vw_downloadStatusBar.progress=currentPercentvalue;
            downloadcell.lbl_bookDownloadPercent.text=[NSString stringWithFormat:@"%0.f%%",currentPercentvalue];
            //----------------------------------------------------------------------------------------
            [downloadcell.btn_Status setImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateNormal];
            [downloadcell.btn_Status setImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateSelected];
            [downloadcell.btn_Status addTarget:self
                                        action:@selector(cancelDownload:) forControlEvents:UIControlEventTouchDown];
            //------------------IF download in progress------------------------------------------------
            
        }
        
        if ([delegate.downloadComplete containsObject:[downloadBookArry objectAtIndex:indexPath.row]])
        {
            //--------------IF Download Completed----------------------------------------------------
            [downloadcell.btn_Status setImage:[UIImage imageNamed:@"completed"] forState:UIControlStateNormal];
            [downloadcell.btn_Status setImage:[UIImage imageNamed:@"completed"] forState:UIControlStateSelected];
            //----------------------------------------------------------------------------------------
            downloadcell.vw_downloadStatusBar.progress=1.0f;
            downloadcell.lbl_bookDownloadPercent.text=[NSString stringWithFormat:@"100%%"];
        }
        
        /* else if ([downloadComplete containsObject:[downloadBookArry objectAtIndex:indexPath.row]])
         {
         
         //do nothing
         
         
         }*/
        // if (indexPath.row>downloadFileno)
        //if ([downloadCancel containsObject:[downloadBookArry objectAtIndex:indexPath.row]])
        else
        {
            //----------------------------------------------------------------------------------------
            [downloadcell.btn_Status setImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateNormal];
            [downloadcell.btn_Status setImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateSelected];
            [downloadcell.btn_Status addTarget:self
                                        action:@selector(cancelDownload:) forControlEvents:UIControlEventTouchDown];
            //------------------IF download in progress------------------------------------------------
            
            
            
            
            
            downloadcell.vw_downloadStatusBar.progress=0.0f;
            downloadcell.lbl_bookDownloadPercent.text=[NSString stringWithFormat:@"0%%"];
            
        }
        
        
        downloadcell.btn_Status.tag=indexPath.row;
        
        
        downloadcell.vw_downloadStatusBar.transform = CGAffineTransformMakeScale(1.0, 3.0);
        
        // Cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
        
        downloadcell.lbl_bookName.font = [AppFont semiBoldFont:11.0];
        downloadcell.lbl_bookCount.font = [AppFont semiBoldFont:12.0];
        downloadcell.lbl_bookDownloadPercent.font = [AppFont regularFont:12.0];
        
        return downloadcell;
    }
}



-(IBAction)cancelDownload:(UIButton*)sender
{
    
    
    NSLog(@"I Clicked a button %ld",(long)sender.tag);
    int index=(int)sender.tag;
    
    
    
    
    @try {
        
        [downloadCancel addObject:[downloadBookArry objectAtIndex:index]];
        NSString *errormsg=[NSString stringWithFormat:@"Cancel Download %@",[[downloadBookArry objectAtIndex:index]valueForKey:@"chapterTitle"]];
        
        [self showToast:errormsg];
        
        
        if (index<=[downloadBookArry count])
        {
            NSString * downloadEndFMsg= [NSString stringWithFormat:@"%@ Download Fail",[[downloadBookArry objectAtIndex:index]valueForKey:@"chapterTitle"]];
            
            [self showToast:downloadEndFMsg];
            
            NSLog(@"downloadEndFMsg End=%@",downloadEndFMsg);
            
            NSString *cId=  [NSString stringWithFormat:@"%@",[[downloadBookArry objectAtIndex:index]valueForKey:@"id"]];
            
            NSString * bookId=  [NSString stringWithFormat:@"%@",[bookJson valueForKey:@"bookId"]];
            
            
            
            NSDictionary *idDic = [NSDictionary dictionaryWithObjectsAndKeys:
                                   cId, @"id",nil];
            NSMutableArray *chapterArray=[[NSMutableArray alloc]init];
            [chapterArray addObject:idDic];
            NSDictionary *pdDic = [NSDictionary dictionaryWithObjectsAndKeys:
                                   bookId, @"bookId",chapterArray, @"chapters",nil];
            NSDictionary *jsontoPass = [NSDictionary dictionaryWithObjectsAndKeys:
                                        @"F", @"status",pdDic, @"pd",nil];
            NSError *error;
            NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:jsontoPass options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
            NSLog(@"jsonString==>%@",jsonString);
            
            jsonString=[self JSONString:jsonString];
            NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",callBackresponse,jsonString];
            
            
            
            
            
            
            [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
        }
        
        
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    [vw_progressBar setProgress:0];
    
    // CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:tbldonwload];
    // NSIndexPath *indexPath = [tbldonwload indexPathForRowAtPoint:buttonPosition];
    
    if (index==downloadFileno) {
        [downtask cancel];
        // downloadFileno++;
        NSLog(@"Succeeded! Received %d bytes of data",downloadFileno);
        
        if (downloadFileno<=[downloadBookArry count]) {
            // downloadFileno++;
            
            @try {
                [downloadBookArry removeObjectAtIndex:index];
                
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            // downloadFileno++;
            if (index==[downloadBookArry count])
            {
                
                [self closeBtn:self];
                
            }
            
            else
            {
                [self StartDownloadingPress];
                
                downloadCheck=TRUE;
                [self isDownloadingProgress];
            }
        }
        [tbldonwload reloadData];
    }
    else
    {
        @try {
            [downloadBookArry removeObjectAtIndex:index];
            
            
            if (index<[downloadBookArry count])
            {
                
                [vw_progressBar setProgress:0];
                
                NSString *bookdownload=[NSString stringWithFormat:@"%@",[[downloadBookArry objectAtIndex:downloadFileno]valueForKey:@"chapterTitle"]];
                
                NSString *bookcurrentlydownload=[NSString stringWithFormat:@"%d/%lu%@",downloadFileno+1,(unsigned long)[downloadBookArry count],NSLocalizedString(@"download_completed", nil)];
                
                dispatch_after(0, dispatch_get_main_queue(), ^{
                    
                    [lbl_downloadbook setText:bookdownload];
                    [lbl_downloadcomplete setText:bookcurrentlydownload];
                    [lbl_downloadcomplete sizeToFit];
                    
                });
                
                
                // currentURL=[[downloadBookArry objectAtIndex:downloadFileno]valueForKey:@"epubLink"];
                [lbl_downloadbook setNeedsDisplay];
                
                [lbl_downloadcomplete setNeedsDisplay];
                
                
                
                downloadCheck=TRUE;
                [self isDownloadingProgress];
                [tbldonwload reloadData];
                
                //[tbldonwload deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
    }
    
    
    
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView.tag==808)
    {
        if ([[UIScreen mainScreen]bounds].size.height == 1024)
        {
            return 150;
        }
        else
        {
            return 100;
        }
    }
    else
    {
        return 48;
    }
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if (tableView.tag==808) {
        return [self.tableData count];
    }
    else
        return [downloadBookArry count];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath   *)indexPath
{
    if (tableView.tag==808)
    {
        
        
        [tbl_recent reloadData];
        if ([self.webView canGoBack])
        {
            flagViewAppear=TRUE;
            
            //[self.webView loadHTMLString:@"about:blank" baseURL:nil];
            self.webView=nil;
            self.webView=[[UIWebView alloc]init];
            
        }
        else
        {
            
        }
        
        
        //vw_recent.hidden=TRUE;
        [self rightoleftClose];
        
        flagBack=TRUE;
        
        
        
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
        
        
        NSLog(@"Not navigationArray=%@",navigationArray);
        
        // [self dismissViewControllerAnimated:NO  completion:^{
        
        // self.webView=[[UIWebView alloc]initWithFrame:self.webView.frame];
        
        HomeDetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
        vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        vc.dic_serviceInfo=[self.tableData objectAtIndex:indexPath.row];
        
        totalviewload++;//increase in case more then one view are loaded
        
        //  dispatch_after(0, dispatch_get_main_queue(), ^{
        [self presentViewController:vc animated:NO completion:nil];
        
        
        
        
    }
    else
    {
        // do nothing
    }
    
    
    
    
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}



-(NSString*)getStateEnglishName:(NSString*)state_id
{
    
    NSDictionary* dummyDictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"StateData" ofType:@"plist"]];
    
    
    
    NSArray* sortedCategories = [dummyDictionary.allKeys sortedArrayUsingDescriptors:nil];
    
    
    
    
    NSString *categoryName;
    NSArray *currentCategory;
    
    statelocalPlist=[[NSMutableArray alloc]init];
    
    
    for (int i = 0; i < [dummyDictionary.allKeys count]; i++)
    {
        
        categoryName = [sortedCategories objectAtIndex:i];
        currentCategory = [dummyDictionary objectForKey:categoryName];
        [statelocalPlist addObject:currentCategory];
        
        
        
        
    }
    NSLog(@"statelocalPlist=%@",statelocalPlist);
    
    
    NSArray *arry=[NSArray arrayWithArray:statelocalPlist];
    NSArray *resultArray = [arry filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"State_ID == %@", state_id]];
    NSString *stateName=@"";
    
    if([resultArray count]>0)
    {
        stateName=[[resultArray objectAtIndex:0] valueForKey:@"State_NAME"];
        
    }
    if ([stateName length]==0) {
        stateName=@"";
    }
    return stateName;
}

//---------Loading HTML On webview----------
-(void)loadHtml
{
    
    
    // SharedManager *singleton = [SharedManager sharedSingleton];
    
    //-------- Code to load cookies----------------
    
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    [cookieStorage setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
    
    //-------- Code to load cookies----------------
    
    
    NSString *sid=SERVICE_ID_to_Pass;
    NSString *nam=singleton.profileNameSelected ;
    NSString *addr=singleton.profileUserAddress ;
    
    NSString *st=[NSString stringWithFormat:@"%@",singleton.profilestateSelected];
    NSLog(@"st=%@",st);
    if([st length]==0||[st isEqualToString:@"9999"])
    {
        st=@"";
    }
    
    
    NSString *cty=@"";
    NSString *dob=singleton.profileDOBSelected;
    NSString *gndr=str_gender;
    //NSString *qual=;
    
    NSString *qual=[obj getQualiListCode:singleton.user_Qualification];
    NSString *occup=[obj getOccuptCode:singleton.user_Occupation];
    
    NSString *email=singleton.profileEmailSelected;
    NSString *amno=singleton.altermobileNumber;
    NSString *lang=@""; //add condition for it
    
    
    
    NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if (selectedLanguage == nil)
    {
        selectedLanguage = @"en";
    }
    else{
        NSArray *arrTemp = [selectedLanguage componentsSeparatedByString:@"-"];
        selectedLanguage = [arrTemp firstObject];
    }
    
    //NSLog(@"Applied Selected Language = %@",selectedLanguage);
    
    bool checklang=[singleton.dbManager getServiceLanguage:service_ID withDeviceLang:selectedLanguage];
    
    if (checklang==true)
    {
        lang=selectedLanguage; //add condition for it
        
    }
    else
    {
        lang=@"en";
    }
    
    
    
    
    // NSString *aadhr= singleton.user_aadhar_number;
    NSString *aadhr= singleton.objUserProfile.objAadhar.aadhar_number;
    
    NSString *ntfp=@"1";
    NSString *ntft=@"1";
    NSString *pic=singleton.user_profile_URL;
    NSString *psprt=@"";
    NSString *dist=singleton.notiTypDistricteSelected;
    NSString *amnos=@""; //pass it
    NSString *emails=@"";//pass it
    NSString *ivrlang=@""; //pass it
    //NSString *gcmid=singleton.devicek_tkn;
    NSString *gcmid=[obj getStateCode:singleton.profilestateSelected ];
    
    
    
    // st=[self getStateEnglishName:gcmid];
    
    //NSString *gcmid=[obj getStateCode:[@"Uttarakhand" lowercaseString]];
    
    NSString *mno=singleton.mobileNumber;
    NSString *uid=singleton.user_id; //pass it
    
    if ([uid length]==0) {
        uid=@"";
    }
    
    
    
    if([nam length]==0)
    {
        nam=@"";
    }
    if([addr length]==0)
    {
        addr=@"";
    }
    
    if([dob length]==0)
    {
        dob=@"";
    }
    
    if([gndr length]==0)
    {
        gndr =@"";
    }
    
    if([qual length]==0)
    {
        qual=@"";
    }
    
    if([occup length]==0)
    {
        occup=@"";
    }
    
    if([email length]==0)
    {
        email=@"";
    }
    
    if([amno length]==0)
    {
        amno=@"";
    }
    
    
    
    if([pic length]==0)
    {
        pic=@"";
    }
    
    if([dist length]==0)
    {
        dist=@"";
    }
    if([aadhr length]==0)
    {
        aadhr=@"";
    }
    
  

    
    
    
    
    NSString *adhr_name=singleton.objUserProfile.objAadhar.name;
    NSString *adhr_dob=singleton.objUserProfile.objAadhar.dob;
    NSString *adhr_co=singleton.objUserProfile.objAadhar.father_name;
    NSString *adhr_gndr=singleton.objUserProfile.objAadhar.gender;
    NSString *adhr_imgurl=singleton.objUserProfile.objAadhar.aadhar_image_url;
    NSString *adhr_state=singleton.objUserProfile.objAadhar.state;
    NSString *adhr_dist=singleton.objUserProfile.objAadhar.district;
    NSString *adhr_mobilenumber = singleton.objUserProfile.objAadhar.mobile_number;
    
    if([adhr_state length]==0)
    {
        adhr_state=@"";
    }
    NSLog(@"adhar=%@",adhr_state);
    
    if([adhr_name length]==0)
    {
        adhr_name=@"";
    }
    
    if([adhr_dob length]==0)
    {
        adhr_dob=@"";
    }
    
    if([adhr_co length]==0)
    {
        adhr_co=@"";
    }
    if(adhr_mobilenumber == nil || [adhr_mobilenumber length]==0)
    {
        adhr_mobilenumber=@"";
    }
    
    
    
    if([adhr_gndr isEqualToString:@"M"])
    {
        adhr_gndr=@"Male";
    }
    if([adhr_gndr isEqualToString:@"F"])
    {
        adhr_gndr=@"Female";
    }
    
    if([adhr_gndr length]==0)
    {
        adhr_gndr=@"";
    }
    
    if([adhr_imgurl length]==0)
    {
        adhr_imgurl=@"";
    }
    
    
    if([adhr_dist length]==0)
    {
        adhr_dist=@"";
    }
    
    if (adhr_mobilenumber.length != 0)
    {
        mno = adhr_mobilenumber;
    }
    
    if([gcmid length]==0)
    {
        gcmid=@"";
    }
    
    if([gcmid isEqualToString:@"9999"])
    {
        gcmid=@"";
    }
    
    
    
    //------------------------ If needed Open this code-----------------------
    
    // st=@"";
    // dist=@"";
    if (![lang isEqualToString:@"en"]) {
        dist=@"";
        st=@"";
    }
    
    
    
   // STATETABSELECT
    //------------------------ Close this code-----------------------
    
    
    NSMutableDictionary *userinfo = [NSMutableDictionary new];
    [userinfo setObject:sid forKey:@"sid"];
    [userinfo setObject:nam forKey:@"nam"];
    [userinfo setObject:addr forKey:@"addr"];
    [userinfo setObject:st forKey:@"st"];
    [userinfo setObject:cty forKey:@"cty"];
    [userinfo setObject:dob forKey:@"dob"];
    [userinfo setObject:gndr forKey:@"gndr"];
    [userinfo setObject:qual forKey:@"qual"];
    [userinfo setObject:email forKey:@"email"];
    [userinfo setObject:amno forKey:@"amno"];
    [userinfo setObject:lang forKey:@"lang"];
    
    
    [userinfo setObject:ntfp forKey:@"ntfp"];
    [userinfo setObject:ntft forKey:@"ntft"];
    [userinfo setObject:pic forKey:@"pic"];
    [userinfo setObject:psprt forKey:@"psprt"];
    [userinfo setObject:occup forKey:@"occup"];
    [userinfo setObject:dist forKey:@"dist"];
    [userinfo setObject:amnos forKey:@"amnos"];
    [userinfo setObject:emails forKey:@"emails"];
    [userinfo setObject:ivrlang forKey:@"ivrlang"];
    [userinfo setObject:gcmid forKey:@"gcmid"];
    [userinfo setObject:mno forKey:@"mno"];
    [userinfo setObject:uid forKey:@"uid"];
    
    
    
    
    
    
    
    NSMutableDictionary * adhrinfo = [NSMutableDictionary new];
    [adhrinfo setObject:adhr_name forKey:@"adhr_name"];
    [adhrinfo setObject:adhr_dob forKey:@"adhr_dob"];
    [adhrinfo setObject:adhr_co forKey:@"adhr_co"];
    [adhrinfo setObject:adhr_gndr forKey:@"adhr_gndr"];
    [adhrinfo setObject:adhr_imgurl forKey:@"adhr_imgurl"];
    [adhrinfo setObject:adhr_dist forKey:@"adhr_dist"];
    [adhrinfo setObject:adhr_mobilenumber forKey:@"mno"];
    [adhrinfo setObject:adhr_state forKey:@"adhr_state"];
    
    
    NSString *tkn=singleton.user_tkn;
    
    
    
    if([tkn length]==0)
    {
        tkn=@"";
    }
    
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody addEntriesFromDictionary:[self getCommonParametersForRequestBody]];
    [dictBody setObject:@"app" forKey:@"mod"];
    [dictBody setObject:@"" forKey:@"peml"];
    [dictBody setObject:lang forKey:@"lang"];
    [dictBody setObject:tkn forKey:@"tkn"];
    [dictBody setObject:mno forKey:@"mno"];
    [dictBody setObject:service_ID forKey:@"service_id"];
    
    if (singleton.objUserProfile.objAadhar.aadhar_number.length)
    {
        [dictBody setObject:aadhr forKey:@"aadhr"];
        
        
    }
    
    //---------- add userinfo -------------------
    [dictBody setObject:userinfo forKey:@"userinfo"];
    //---------- add adhrinfo-------------------
    
    
    
    if (singleton.objUserProfile.objAadhar.aadhar_number.length)
    {
        [dictBody setObject:adhrinfo forKey:@"adhrinfo"];
        
        
    }
    else
    {
        //dont add it
    }
    
    
    
    NSString *checkLinkStatus=[[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"LINKDIGILOCKERSTATUS"];
    
    if ([checkLinkStatus isEqualToString:@"YES"])
    {
        
        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
        
        NSString *username=[[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"digilocker_username"];
        NSString *password=[[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"digilocker_password"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        NSMutableDictionary *digilocker = [NSMutableDictionary new];
        [digilocker setObject:username forKey:@"username"];
        [digilocker setObject:password forKey:@"password"];
        
        /*
         "digilocker": {
         "username": "harrypandher",
         "password": "harry@0370"
         },
         */
        
        [dictBody setObject:digilocker forKey:@"digilocker"];
        
        //------------------------- Encrypt Value------------------------
        
    }
    
    
    
    
    
    
    NSString *urlAddress =urlString;//<null>
    
    if ([urlAddress isEqualToString:@"<null>"])
    {
        urlAddress=@"";
    }
    
    
    
    //===================================================================================
    //         START  NEW CHANGES OF ADDING STATE ID AND STATE NAME
    //===================================================================================

    if (state_nametopass.length==0)
    {
        state_nametopass=@"";
        
    }
    if (state_idtopass.length==0)
    {
        state_idtopass=@"";
    }
    NSLog(@"state_nametopass =%@",state_nametopass);
    NSLog(@"state_idtopass =%@",state_idtopass);

    if ([isStateSelected isEqualToString:@"TRUE"])
    {
        
        
       
        //add both parameter in dicBody in case of state ONLY else its is not added in the dicBody
        // pass key here with new paramenter
        [dictBody setObject:state_nametopass forKey:@"tab_state_name"];
        [dictBody setObject:state_idtopass forKey:@"tab_state_id"];

        
    }
    else
    {
        state_nametopass=@"";
        state_idtopass=@"";
    }
    
    //===================================================================================
    //          END NEW CHANGES OF ADDING STATE ID AND STATE NAME
    //===================================================================================

    
    
    
    
    
    
    
    
    
    //================ADDED =========================
    //  urlAddress= [urlAddress stringByReplacingOccurrencesOfString: @"https://web.umang.gov.in/uw/" withString:@"https://web.umang.gov.in/uwstg/"];
    //================ADDED =========================
    //NSLog(@"urlAddress=%@",urlAddress);
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dictBody options:0 error:&err];
    NSString * contentToEncode = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    //NSLog(@"contentToEncode %@",contentToEncode);
    
    contentToEncode = [contentToEncode stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"contentToEncode %@",contentToEncode);
    
    NSString *contentEncoding = @"application/vnd.umang.web+json; charset=UTF-8";
    
    postURI=[NSURL URLWithString:urlAddress];
    
    NSString *verb = @"GET";
    
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: @"EEE, d MMM yyyy HH:mm:ss z"];
    NSString *currentDate = [dateFormatter stringFromDate: [NSDate date]]; //Put whatever date you want to convert
    NSString *currentDatetrim = [currentDate stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    //NSLog(@"contentEncoding =%@",contentEncoding);
    //NSLog(@"postURI =%@",postURI);
    //NSLog(@"verb =%@",verb);
    //NSLog(@"currentDate =%@",currentDate);
    
    NSString *USERNAME=@"UM4NG";
    NSString *contentMd5=[NSString stringWithFormat:@"%@",[contentToEncode MD5]];
    
    NSString *contentMd5trim = [contentMd5 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    //NSLog(@"contentMd5trim =%@",contentMd5trim);
    //NSLog(@"postURI %@", postURI.path);
    
    NSString *postURItrim=[postURI.path stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *toSign =[NSString stringWithFormat:@"%@\n%@\n%@\n%@\n%@",verb,contentMd5trim,contentEncoding,currentDatetrim,postURItrim];
    //NSLog(@"toSign %@",toSign);
    
    // NSString *deepak1=[@"Deepak" hmacsha1:@"Deepak" secret:SaltSHA1MAC];
    ////NSLog(@"deepak=%@",deepak1);
    NSString *hmac=[toSign hmacsha1:toSign secret:SaltSHA1MAC];
    //NSLog(@"hmac %@",hmac);
    NSString *hmactrim=[hmac stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *XappAuth=[NSString stringWithFormat:@"%@:%@",USERNAME,hmactrim];
    
    
    
    //NSLog(@"postURL===>%@",postURI);
    // NSLog(@"postURItrim===>%@",postURItrim);
    
    //NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:postURI cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:30.0];
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:postURI];
    
    
    //// NSCachedURLResponse* response = [[NSURLCache sharedURLCache]
    //  cachedResponseForRequest:[self.webView request]];
    
    /*  NSDictionary *headers = @{@"Access-Control-Allow-Origin" : @"*", @"Access-Control-Allow-Headers" : @"Content-Type"};
     NSHTTPURLResponse *urlresponse = [[NSHTTPURLResponse alloc] initWithURL:request.URL statusCode:200 HTTPVersion:@"1.1" headerFields:headers];
     
     NSCachedURLResponse *cachedResponse = [[NSCachedURLResponse alloc] initWithResponse:urlresponse data:data];
     data = [cachedResponse data];
     */
    self.webView.delegate=self;
    
    //[request setHTTPMethod: @"GET"];
    [request addValue:XappAuth forHTTPHeaderField:@"X-App-Authorization"];
    [request addValue:currentDate forHTTPHeaderField:@"X-App-Date"];
    [request addValue:contentMd5trim forHTTPHeaderField:@"X-App-Content"];
    [request addValue:contentToEncode forHTTPHeaderField:@"X-App-Data"];
    [request addValue:contentEncoding forHTTPHeaderField:@"Content-Type"];
    
    
    //---- Adding in cookies--------------
    
    [request setHTTPShouldHandleCookies:YES];
    
    if([cookies count]>1)
    {
        [self addCookies:cookies forRequest:request];
    }
    //---- Adding in cookies--------------
    
    [self.webView loadRequest: request];
    
    
}

//-------- WEB CACHE CODE START------------


//-------- Code to load cookies----------------

- (void)addCookies:(NSArray *)cookies forRequest:(NSMutableURLRequest *)request
{
    if ([cookies count] > 0)
    {
        NSHTTPCookie *cookie;
        NSString *cookieHeader = nil;
        for (cookie in cookies)
        {
            if (!cookieHeader)
            {
                cookieHeader = [NSString stringWithFormat: @"%@=%@",[cookie name],[cookie value]];
            }
            else
            {
                cookieHeader = [NSString stringWithFormat: @"%@; %@=%@",cookieHeader,[cookie name],[cookie value]];
            }
        }
        if (cookieHeader)
        {
            [request setValue:cookieHeader forHTTPHeaderField:@"Cookie"];
        }
    }
}
//-------- Code to load cookies----------------


- (void)webViewDidStartLoad:(UIWebView *)webView {
    
    // [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    // hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    //hud.label.text = NSLocalizedString(@"loading",nil);
    
    [activityIndicator startAnimating];
    
    
    loadingStatus=@"false";
    
    [self updateButtons];
    
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    [activityIndicator stopAnimating];
    
    
    //[hud hideAnimated:YES];
    NSLog(@"error=%@",error);
    if([error code] == NSURLErrorCancelled)
        return;
    /*  if (error) {
     [self.webView loadData:singleton.cachedata MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:postURI];
     }*/
    // [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    loadingStatus=@"true";
    [self updateButtons];
    
}

#pragma mark - ==== Show Hint Screen =======
-(MaterialShowcase*)showcaseWithtext:(NSString*)text withView:(UIView*)view {
    
    MaterialShowcase *showcase = [[MaterialShowcase alloc] init];
    showcase.backgroundPromptColor = [UIColor colorWithRed:0.0/255.0 green:59.0/255.0 blue:187.0/255.0 alpha:1];
    
    [showcase setTargetViewWithView:view];
    showcase.primaryText =NSLocalizedString(@"recent_department", nil);
    [showcase setDelegate:self];
    showcase.secondaryText = text;
    return  showcase;
    
}
//showCaseDidDismiss(showcase: MaterialShowcase)
-(void)showCaseWillDismissWithShowcase:(MaterialShowcase*)showcase {
    NSLog(@"showcase%@", showcase);
}
-(void)showCaseDidDismissWithShowcase:(MaterialShowcase*)showcase {
    NSLog(@"showcase%@", showcase);
    [self recentBtnAction:btnRecent];
    //    switch (showcase.tag) {
    //        case 6660:
    //
    //            break;
    //        case 6661:
    //            show = [self showcaseWithtext:NSLocalizedString(@"hint_sort", nil) withView:btn_filter];
    //            show.tag = 6662;
    //            break;
    //        case 6662:
    //            lbl = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMinX(txt_searchField.frame), CGRectGetMinY(txt_searchField.frame), 70, CGRectGetHeight(txt_searchField.frame))];
    //            lbl.text = txt_searchField.placeholder;
    //            lbl.textAlignment = NSTextAlignmentCenter;
    //            lbl.textColor = [UIColor colorWithRed:0.0/255.0 green:59.0/255.0 blue:187.0/255.0 alpha:0.70];
    //            show = [self showcaseWithtext:NSLocalizedString(@"hint_search", nil) withView:lbl];
    //            show.tag = 6663;
    //            break;
    //        case 6663:
    //            [self showHomeDialogueBox];
    //        default:
    //            break;
    //    }
    
    
}

#pragma mark - Updating the UI



- (void)webViewDidFinishLoad:(UIWebView *)webView {
    //NSLog(@"%s", __PRETTY_FUNCTION__);
    // [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    // [hud hideAnimated:YES];
    [activityIndicator stopAnimating];
    
    loadingStatus=@"true";
    [self updateButtons];
    
    
}


-(NSString *)JSONString:(NSString *)aString {
    NSMutableString *s = [NSMutableString stringWithString:aString];
    [s replaceOccurrencesOfString:@"\"" withString:@"\\\"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"/" withString:@"\\/" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\n" withString:@"\\n" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\b" withString:@"\\b" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\f" withString:@"\\f" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\r" withString:@"\\r" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\t" withString:@"\\t" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    return [NSString stringWithString:s];
}



-(void)openPDFfiletoshare:(NSData*)pdfDocumentData

{
    flagViewAppear=FALSE;
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    vc.pdfData=pdfDocumentData;
    vc.titleOpen=@"PDF View";
    vc.urltoOpen=@"";
    vc.isfrom=@"WEBVIEWHANDLEPDF";
    vc.file_type=@"application/pdf";
    vc.file_shareType=@"ShareYes";
    UIViewController *topvc=[self topMostController];
    [topvc presentViewController:vc animated:NO completion:nil];
    
}



-(void)openPDFfile:(NSData*)pdfDocumentData

{
    
    /*  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
     
     // get the data here
     
     UIImage *image = [UIImage imageWithData:data];
     
     dispatch_async(dispatch_get_main_queue(), ^{
     
     UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
     
     [self showToast:@"Your Document Save to Photo Library Successfully!"];
     });
     });
     */
    
    flagViewAppear=FALSE;
    
    //NSLog(@"OpenWebViewFeedback");
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    vc.pdfData=pdfDocumentData;
    vc.titleOpen=@"PDF View";
    vc.urltoOpen=@"";
    vc.isfrom=@"WEBVIEWHANDLEPDF";
    vc.file_type=@"application/pdf";
    // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    UIViewController *topvc=[self topMostController];
    [topvc presentViewController:vc animated:NO completion:nil];
    
}



//-------- WEB CACHE CODE START------------

-(BOOL)webView:(UIWebView *)mainWebView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    // hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    ////NSLog(@"url -->%@",[[request URL] absoluteString]);
    
    //    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
    //        //Allows for twitter links
    //        [self.webView loadRequest:request];
    //        return NO;
    //  che-01252  }
    
    
    
    //[webview loadRequest:request];
    NSString *requestString = [[request URL] absoluteString];
    NSLog(@"requestString -->%@",requestString);
    
    
    
    /* if ([[[request URL] absoluteString] isEqualToString:urlString])
     
     {
     
     [_backBtnHome setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
     if ([self.webView canGoBack])
     {
     NSLog(@">>>>>>=====================> User landing page can go back to home");
     
     flagBack=TRUE;
     
     
     self.webView = nil;
     [self dismissViewControllerAnimated:NO completion:nil];
     
     }
     else
     {
     //do nothing
     NSLog(@">>>>>>=====================> User landing page Do Nothing");
     
     }
     return YES;
     
     
     }*/
    
    
    
    //https://stgweb.umang.gov.in/uw/bpcl.jsp#/
    
    flag_homebtn=FALSE;
    
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::getPageLoadingStatus::"])
    {
        
        [self getPageLoadingStatus];
        return YES;
    }
    
    if ([[[request URL] absoluteString] hasSuffix:@"#/"])
    {
        
        // mainDocumentURL = [[request URL]absoluteString];
        NSURL * currentURL = _webView.request.URL.absoluteURL;
        mainDocumentURL = [NSString stringWithFormat:@"%@",currentURL];
        
        
        NSLog(@"mainDocumentURL = %@",mainDocumentURL);
        
        flag_homebtn=TRUE;
        [self updateButtons];
        return NO;
        //return YES;
        
        
    }
    
    
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::traceUrl:"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        NSString *functionName = (NSString*)[components objectAtIndex:1];
        //NSString *argsAsString = (NSString*)[components objectAtIndex:2];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        
        NSData *data = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSArray *arr_JsonArray = [NSArray arrayWithObjects:[NSJSONSerialization JSONObjectWithData:data options:0 error:nil],nil];
        
        
        
        /* if ([self.webView canGoBack])
         {
         // [webView goBack];
         // [_backBtnHome setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
         
         [_backBtnHome setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
         
         self.web_back_btn.enabled=TRUE;
         }
         else
         {
         [_backBtnHome setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
         
         self.web_back_btn.enabled=FALSE;
         
         }*/
        
        
        
        if ([self.webView canGoBack])
        {
            // [webView goBack];
            // [_backBtnHome setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
            
            @try {
                NSString *nextUrl=[NSString stringWithFormat:@"%@",[[arr_JsonArray valueForKey:@"next"] objectAtIndex:0]];
                if ([nextUrl hasSuffix:@"#/"])
                {
                    
                    [_backBtnHome setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
                    
                }
                else
                {
                    [_backBtnHome setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
                    
                    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
                    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
                    {
                        
                        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                                       @{NSFontAttributeName: [UIFont systemFontOfSize:_backBtnHome.titleLabel.font.pointSize]}];
                        
                        CGRect framebtn=CGRectMake(_backBtnHome.frame.origin.x, _backBtnHome.frame.origin.y, _backBtnHome.frame.size.width, _backBtnHome.frame.size.height);
                        
                        [_backBtnHome setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
                        _backBtnHome.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
                        
                    }
                }
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            
            
            
            // next #/
            
            //  [_backBtnHome setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
            
            
            // [_webView goBack];
        }
        else
        {
            [_backBtnHome setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
            
            // self.webView = nil;
            
            //[self dismissViewControllerAnimated:NO completion:nil];
            
            
        }
        
        
        
        return NO;
        
    }
    
    //----------------------------------
    //----------------------------------
    //----------------------------------
    //----------------------------------
    //----------------------------------
    //----------------------------------
    //----------------------------------
    
    
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::downloadFileBase64"]||[[[request URL] absoluteString] hasPrefix:@"iOS::downloadFileBase64"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        //  NSString *functionName = (NSString*)[components objectAtIndex:1];
        //NSString *argsAsString = (NSString*)[components objectAtIndex:2];
        
        // data:application/pdf;base64,
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        
        NSData *jsondata = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSArray *arr_Json = [NSArray arrayWithObjects:[NSJSONSerialization JSONObjectWithData:jsondata options:0 error:nil],nil];
        
        @try {
            NSString* base64String=[[arr_Json objectAtIndex:0] valueForKey:@"base64"];
            
            // NSData *data = [NSData dataFromBase64String:originalString];
            NSData *base64data = [[NSData alloc] initWithData:[NSData dataWithBase64EncodedString:base64String]];
            
            //[self openPDFfile:base64data]; //close as per new request
            [self openPDFfiletoshare:base64data];
            
            
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        return NO;
    }
    
    
    
    //----------------------------------
    //----------------------------------
    //----------------------------------
    //----------------------------------
    //----------------------------------
    //----------------------------------
    
    
    
    
    //window.location="iOS::setPageTitle::"+{"title":"PMKVY"};
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::setPageTitle"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        //  NSString *functionName = (NSString*)[components objectAtIndex:1];
        //NSString *argsAsString = (NSString*)[components objectAtIndex:2];
        
        
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        
        NSData *jsondata = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSArray *arr_Json = [NSArray arrayWithObjects:[NSJSONSerialization JSONObjectWithData:jsondata options:0 error:nil],nil];
        
        @try {
            NSString *gettitle=[[arr_Json objectAtIndex:0] valueForKey:@"title"];
            if (![gettitle isEqualToString:@""])
            {
                [self setPageTitle:gettitle];
                [_backBtnHome setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
                
            }
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        return NO;
        
        
    }
    
    
    
    //window.location="iOS::showToast::"+{"title":"This is my toast"};
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::showToast"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        
        NSData *jsondata = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSArray *arr_Json = [NSArray arrayWithObjects:[NSJSONSerialization JSONObjectWithData:jsondata options:0 error:nil],nil];
        
        @try {
            NSString *gettitle=[[arr_Json objectAtIndex:0] valueForKey:@"title"];
            if (![gettitle isEqualToString:@""])
            {
                [self showToast:gettitle];
            }
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        return NO;
        
        
    }
    
    
    
    //window.location="iOS::openChatScreen::open";
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::openChatScreen::"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        //  NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        @try {
            NSString *url=[json valueForKey:@"URL"];
            NSString *title=[json valueForKey:@"title"];
            [self openChatScreen:title withURL:url];
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        
        
        return NO;
        
        
    }
    
    //  window.location="iOS::downloadFile::"+{"url":"https://google.com","data":"jsonString"}
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::downloadFile::"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        
        // NSString *encodedString=[self JSONString:argsAsString];
        
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        
        
        
        
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        
        
        @try {
            NSString *url=[json valueForKey:@"url"];
            NSString *data1=[json valueForKey:@"data"];
            
            [self downloadFile:url withjsonString:data1];
            
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        return NO;
        
    }
    
    
    
    //   window.location="iOS::downloadFileDept::"+{"url":"https://google.com","data":"jsonString"}
    if ([[[request URL] absoluteString] hasPrefix:@"ios::downloadFileDept::"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        //  NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        
        
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        
        
        
        
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        
        
        @try {
            NSString *url=[json valueForKey:@"url"];
            NSString *data1=[json valueForKey:@"data"];
            
            [self downloadFileDept:url withjsonString:data1];
            
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        return NO;
        
        
        
    }
    
    // window.location="iOS::getCurrentLocation::open"
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::getCurrentLocation"])
    {
        [self getLocation];
        // NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        // NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        @try
        {
            [self getCurrentLocation];
        }
        @catch (NSException *exception)
        {
            
        }
        @finally
        {
            
        }
        return NO;
        
    }
    // window.location="iOS::openPDF::"+{"path":"/test/test/test"}
    if ([[[request URL] absoluteString] hasPrefix:@"ios::openPDF::"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        
        
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        
        
        
        
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        
        
        @try {
            NSString *path=[json valueForKey:@"path"];
            
            [self openPDF:path];//later
            
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        return NO;
    }
    
    
    
    
    
    
    
    // window.location="iOS::fetchLocation::"+{"response":"this is response"}
    if ([[[request URL] absoluteString] hasPrefix:@"ios::fetchLocation::"])
    {
        
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        @try {
            NSString *response=[json valueForKey:@"response"];
            [self fetchLocation:response];
        } @catch (NSException *exception) {
        } @finally {
        }
        return NO;
    }
    
    
    // window.location="iOS::openWebViewFeedback::"+{"url":"https://google.com","title":"successCallBack"}
    if ([[[request URL] absoluteString] hasPrefix:@"ios::openWebViewFeedback::"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        /// NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        @try {
            NSString *url=[json valueForKey:@"url"];
            NSString *title=[json valueForKey:@"title"];
            [self openWebViewFeedback:url withTitle:title];
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        return NO;
    }
    
    
    // window.location="iOS::openWebView::"+{"url":"https://google.com","title":"successCallBack"}
    if ([[[request URL] absoluteString] hasPrefix:@"ios::openWebView::"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        //  NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        @try {
            NSString *url=[json valueForKey:@"url"];
            NSString *title=[json valueForKey:@"title"];
            [self openWebView:url withTitle:title];
        } @catch (NSException *exception) {
        } @finally {
        }
        return NO;
    }
    
    
    
    //window.location="iOS::shareFile::"+{"email":"mahaveer.sikarwar@@spicedigital.in"}
    
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::shareFile::"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        //   NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        @try {
            NSString *email=[json valueForKey:@"email"];
            [self openEmailShareWithAttachment:email];
            
        } @catch (NSException *exception) {
        } @finally {
        }
        return NO;
    }
    
    
    
    
    /* if ([[[request URL] absoluteString] hasPrefix:@"ios::traceUrl:"])
     {
     NSArray *components = [requestString componentsSeparatedByString:@"::"];
     NSString *functionName = (NSString*)[components objectAtIndex:1];
     //NSString *argsAsString = (NSString*)[components objectAtIndex:2];
     
     
     NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
     //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
     
     //   NSData *data = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
     
     //  NSArray *arr_Json = [NSArray arrayWithObjects:[NSJSONSerialization JSONObjectWithData:data options:0 error:nil],nil];
     
     
     
     if ([self.webView canGoBack])
     {
     // [webView goBack];
     self.web_back_btn.enabled=TRUE;
     }
     else
     {
     self.web_back_btn.enabled=FALSE;
     
     }
     return NO;
     
     }
     */
    
    
    
    
    
    
    // window.location="iOS::openChooseFrom::"+{"requestFor":"Camera","success":"successFunction","failure":"failureFunction"};DONE
    
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::openChooseFrom::"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        @try {
            NSString *requestFor=[json valueForKey:@"requestFor"];
            NSString *success=[json valueForKey:@"success"];
            NSString *failure=[json valueForKey:@"failure"];
            
            cameraTag=@"";
            
            
            
            [self openChooseFrom:requestFor withsuccessCallBack:success withfailureCallback:failure];
            
        } @catch (NSException *exception) {
        } @finally {
        }
        return NO;
    }
    
    
    //window.location="iOS::getAllBooksData::open"
    
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::getAllBooksData::"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        /* argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
         argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
         argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
         NSError *jsonError;
         NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
         NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
         options:NSJSONReadingMutableContainers
         error:&jsonError];*/
        
        
        
        
        
        @try {
            
            
            [self getAllBooksData:argsAsString];
            
            
            
        } @catch (NSException *exception) {
        } @finally {
        }
        return NO;
    }
    
    
    // window.location="iOS::getChapterDataFromBookId::"+{"bookId":"data to pass"}
    
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::getChapterDataFromBookId::"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        //  NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        @try {
            
            NSString *bookId=[json valueForKey:@"bookId"];
            
            [self getChapterDataFromBookId:bookId];
            
            
            
        } @catch (NSException *exception) {
        } @finally {
        }
        return NO;
    }
    
    
    
    
    // window.location="iOS::getChaptersFromBookId::"+{"chapterId":"data to pass"}
    
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::getChaptersFromBookId::"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        @try {
            
            NSString *bookId=[json valueForKey:@"bookId"];
            
            [self getChaptersFromBookId:bookId];
            
            
            
        } @catch (NSException *exception) {
        } @finally {
        }
        return NO;
    }
    
    
    
    
    
    //window.location="iOS::startChapterDownLoad::"+{"data":"data to pass","response":"startChapterCallback"}
    
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::startChapterDownLoad::"])
    {
        
        /*   /"  “
         and
         \"  “
         
         */
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\\\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        // argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"" withString:@""];
        
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        
        
        
        @try {
            
            NSDictionary *resdata=(NSDictionary*)[json valueForKey:@"data"];
            // NSDictionary *response=(NSDictionary*)[json valueForKey:@"response"];
            
            NSString *response=[json valueForKey:@"response"];
            [self startChapterDownLoad:resdata withresponse:response];
            
            
            
        } @catch (NSException *exception) {
        } @finally {
        }
        return NO;
    }
    
    
    
    // window.location="iOS::openChapter::"+{"chapterId":"1"}
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::openChapter::"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        @try {
            
            NSString *chapterId=[json valueForKey:@"chapterId"];
            
            [self openChapter:chapterId];
            
            
            
        } @catch (NSException *exception) {
        } @finally {
        }
        return NO;
    }
    
    // window.location="iOS::deleteChapter::"+{"json": {"name": "samplename","value": "samplevalue}}
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::deleteChapter::"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        @try {
            
            NSString *chapterId=[json valueForKey:@"chapterId"];
            NSString *responseCallback=[json valueForKey:@"responseCallback"];
            
            
            
            
            [self deleteChapter:chapterId withresponseCallback:responseCallback];
            
            
            
        } @catch (NSException *exception) {
        } @finally {
        }
        return NO;
    }
    
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::viewDirection::"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        //NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        
        // NSString *encodedString=[self JSONString:argsAsString];
        
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        
        
        
        
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        
        
        @try {
            NSString *googleURL=[json valueForKey:@"googleURL"];
            
            [self openapplemap:googleURL];
            
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        return NO;
        
    }
    
    
    //ios::forcelogout::logout
    if ([[[request URL] absoluteString] hasPrefix:@"ios::forcelogout"])
    {
        [self openLoginView];
        
        return NO;
    }
    
    
    
    
    
    // window.location="ios::saveJsonString:::textstringtobesavehere
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::saveJsonString::"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        
        if([argsAsString length]>0)
        {
            [[NSUserDefaults standardUserDefaults] setObject:argsAsString forKey:@"jsonStringtoSave"];
            [[NSUserDefaults standardUserDefaults]  synchronize];
        }
        return NO;
        
    }
    
    
    
    // window.location=ios::getJsonString::yes
    // call back to call from native to server  returnJsonValueBack
    if ([[[request URL] absoluteString] hasPrefix:@"ios::getJsonString::"])
    {
        
        NSString *jsonStringtoSave=[[NSUserDefaults standardUserDefaults] objectForKey:@"jsonStringtoSave"];;
        [[NSUserDefaults standardUserDefaults]  synchronize];
        
        NSString *javascriptString = [NSString stringWithFormat:@"returnJsonValueBack('%@');",jsonStringtoSave];
        // [self.webView stringByEvaluatingJavaScriptFromString:javascriptString];
        [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
        
        return NO;
    }
    //=====================================================================
    //================== PAN CARD CALL BACK METHOD=========================
    //=====================================================================
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::panCallbackios::"])
    {
        
        
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\\\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        // argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"" withString:@""];
        
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        
        
        
        @try {
            
            /* {
             email = "Dsrawat@gmail.com";
             formname = "49A.pdf";
             mailbody = "Please find attahced PAN 49A form";
             subject = "PAN 49A form";
             title = panCallbackMethod;
             type = email;
             url = "https://www.utiitsl.com/UTIITSL_SITE/forms/49A_Form_Updated.pdf";
             }
             */// NSDictionary *response=(NSDictionary*)[json valueForKey:@"response"];
            NSString *email=[json valueForKey:@"email"];
            
            NSString *formname=[json valueForKey:@"formname"];
            NSString *mailbody=[json valueForKey:@"mailbody"];
            NSString *subject=[json valueForKey:@"subject"];
            
            NSString *title =[json valueForKey:@"title"];
            NSString *type  =[json valueForKey:@"type"];
            NSString *url   =[json valueForKey:@"url"];
            
            
            [self PanDeptFileHandling:email withSub:subject withType:type withMailBody:mailbody withfile:url withFormName:formname withCallback:title];
            
            
            
        } @catch (NSException *exception) {
        } @finally {
        }
        return NO;
    }
    
    
    //=====================================================================
    //==================       Select/Record Video ========================
    //=====================================================================
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::startRecordVideo::"])
    {
        
        
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\\\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        // argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"" withString:@""];
        
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        
        
        @try {
            
            
            videoSuccessCallBack=[json valueForKey:@"success"];
            
            videoFailCallBack=[json valueForKey:@"failure"];
            
            
            
            [self openRecordVideoMethod];
            
            
        } @catch (NSException *exception) {
        } @finally {
        }
        return NO;
    }
    //=====================================================================
    //==================       Select/Record AUDIO ========================
    //=====================================================================
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::startRecordAudio::"])
    {
        
        
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\\\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        // argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"" withString:@""];
        
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        
        
        
        @try {
            
            
            AudioSuccessCallBack=[json valueForKey:@"success"];
            
            AudioFailCallBack=[json valueForKey:@"failure"];
            
            [self openRecordAudioMethod];
            
            
        } @catch (NSException *exception) {
        } @finally {
        }
        return NO;
    }
    
    //=====================================================================
    //==================      Select/Capture Images ========================
    //=====================================================================
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::selectImage::"])
    {
        
        
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\\\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        // argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"" withString:@""];
        
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        
        
        
        @try {
            
            
            imageSuccessCallBack=[json valueForKey:@"success"];
            
            imageFailCallBack=[json valueForKey:@"failure"];
            
            [self openSelectImageMethod];
            
            
        } @catch (NSException *exception) {
        } @finally {
        }
        return NO;
    }
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::navigatetofirsturl::"])
    {
        
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        @try
        {
            [[NSURLCache sharedURLCache] removeAllCachedResponses];
            
            NSURL *URL = [NSURL URLWithString:argsAsString];
            NSURLRequest *request = [NSURLRequest requestWithURL:URL];
            
            [self.webView loadRequest:request];
            
        } @catch (NSException *exception)
        {
            
        } @finally
        {
            
        }
        return NO;
    }
    
    
    
    //=================================================================================
    //========================= OPEN EXTERNAL BROWSER =================================
    //=================================================================================
    
    // window.location="ios::openBrowse::"+{"url":"here is the url to be open"}
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::openBrowser::"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        //  NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        
        @try
        {
            NSString *url=[json valueForKey:@"url"];
            url = [url stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            
            [self openBrowser:url];
            
        } @catch (NSException *exception)
        {
            
        } @finally {
        }
        return NO;
    }
    
    
    //=================================================================================
    //============     METHOD TO OPEN DOWNLOADBASE64FORSHARE ==========================
    //=================================================================================
    
    
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::downloadfilebase64ForShare"]||[[[request URL] absoluteString] hasPrefix:@"iOS::downloadfilebase64ForShare"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        //  NSString *functionName = (NSString*)[components objectAtIndex:1];
        //NSString *argsAsString = (NSString*)[components objectAtIndex:2];
        // data:application/pdf;base64,
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        NSData *jsondata = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSArray *arr_Json = [NSArray arrayWithObjects:[NSJSONSerialization JSONObjectWithData:jsondata options:0 error:nil],nil];
        @try {
            NSString* base64String=[[arr_Json objectAtIndex:0] valueForKey:@"base64"];
            
            // NSData *data = [NSData dataFromBase64String:originalString];
            NSData *base64data = [[NSData alloc] initWithData:[NSData dataWithBase64EncodedString:base64String]];
            
            //[self openPDFfile:base64data]; //close as per new request
            
            
            NSError *error;
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/PANFile"];
            if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])//Check
                [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Will Create folder
            
            NSString* file_type= @"application/pdf";
            NSString *localImgpath;
            NSArray* foo = [file_type componentsSeparatedByString:@"/"];
            if ([foo count]>0) {
                NSString* mimetype = [foo objectAtIndex: 1];
                
                localImgpath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@",file_type,mimetype]];
                
                [base64data writeToFile:localImgpath atomically:YES];
            }
            
            NSString *msg=NSLocalizedString(@"download_success", nil);
            [self showToast:msg];
            
            [self shareBase64MethodPANFile:localImgpath];
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        return NO;
    }
    
    
    
    //=================================================================================================
    //Proof of Address, DOB Proof and Proof of Identitywill support PDF, PNG and JPEG only and maximum
    //                              file size allowed is 300 Kb.
    //=================================================================================================
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::proofImage::"])
    {
        
        
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\\\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        // argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"" withString:@""];
        
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        
        
        
        @try {
            
            
            imageSuccessCallBack=[json valueForKey:@"success"];
            
            imageFailCallBack=[json valueForKey:@"failure"];
            
            
            cameraTag=@"proofImage";
            
            [self openChooseFrom:@"requestfor" withsuccessCallBack:imageSuccessCallBack withfailureCallback:imageFailCallBack];
            
        } @catch (NSException *exception) {
        } @finally {
        }
        return NO;
    }
    
    //=================================================================================================
    //Photo File will support PNG and JPEG only. Photo Scanning should be 300 dpi,Colour,213 X 213 px
    //                                  (Size:less than 30 kb)
    //=================================================================================================
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::getPhotoFile::"])
    {
        
        
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\\\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        // argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"" withString:@""];
        
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        
        
        
        @try {
            
            
            imageSuccessCallBack=[json valueForKey:@"success"];
            
            imageFailCallBack=[json valueForKey:@"failure"];
            
            
            cameraTag=@"getPhotoFile";
            
            [self openChooseFrom:@"requestfor" withsuccessCallBack:imageSuccessCallBack withfailureCallback:imageFailCallBack];
            
        } @catch (NSException *exception) {
        } @finally {
        }
        return NO;
    }
    
    
    //=================================================================================================
    //Signature File will support PNG and JPEG only .Signature scanning should be 600 dpi black and white
    //                                 (Size:less than 60 kb)
    //=================================================================================================
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::getSignatureFile::"])
    {
        
        
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\\\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        // argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"" withString:@""];
        
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        
        
        
        @try {
            
            
            imageSuccessCallBack=[json valueForKey:@"success"];
            
            imageFailCallBack=[json valueForKey:@"failure"];
            
            
            
            cameraTag=@"getSignatureFile";
            [self openChooseFrom:@"requestfor" withsuccessCallBack:imageSuccessCallBack withfailureCallback:imageFailCallBack];
            
            
        } @catch (NSException *exception) {
        } @finally {
        }
        return NO;
    }
    if ([[[request URL] absoluteString] hasPrefix:@"ios::getEdistrictOriginalImage::"])
    {
        
        
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\\\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        // argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"" withString:@""];
        
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        
        
        
        @try {
            
            
            imageSuccessCallBack=[json valueForKey:@"success"];
            
            imageFailCallBack=[json valueForKey:@"failure"];
            
            fileSizeString = [json valueForKey:@"fileSizeStr"];
            
            
            cameraTag=@"getEdistrictOriginalImage";
            [self openChooseFrom:@"requestfor" withsuccessCallBack:imageSuccessCallBack withfailureCallback:imageFailCallBack];
            
            
        } @catch (NSException *exception) {
        } @finally {
        }
        return NO;
    }
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::getPDF::"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        @try
        {
            
            cameraTag = @"getPDF";
            
            NSString *requestFor=[json valueForKey:@"requestFor"];
            NSString *success=[json valueForKey:@"success"];
            NSString *failure=[json valueForKey:@"failure"];
            
            fileSizeString = [NSString stringWithFormat:@"%@",[json valueForKey:@"fileSizeStr"]];
            
            [self openChooseFrom:requestFor withsuccessCallBack:success withfailureCallback:failure];
            
        } @catch (NSException *exception) {
        } @finally {
        }
        return NO;
    }
    
    //=====================================================================
    //===================ePathshala share feature==========================
    //=====================================================================
    
    
    
    
    //      ios::ePathsalashare::{ json: to be share }
    
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::ePathsalashare::"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        @try {
            
            NSString *chapterId=[json valueForKey:@"chapterId"];
            
            [self ShareEpathsalaChapter:chapterId];
            
            
            
        } @catch (NSException *exception) {
        } @finally {
        }
        return NO;
    }
    
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::getDigilockerCredentials::"])
    {
        
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:&jsonError];
        @try
        {
            
            NSString *utoken = [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessTokenDigi"];
            NSString *rtoken = [[NSUserDefaults standardUserDefaults] valueForKey:@"RefreshTokenDigi"];
            
            NSString *resultString;
            
            NSString *success=[json valueForKey:@"success"];
            
            if (utoken.length != 0 && rtoken.length != 0)
            {
                
                resultString = [NSString stringWithFormat:@"%@,%@",utoken,rtoken];
            }
            else
            {
                resultString = @"false";
            }
            
            
            NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",success,resultString];
            // [self.webView stringByEvaluatingJavaScriptFromString:javascriptString];
            [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
            
            
            
        } @catch (NSException *exception) {
        } @finally {
        }
        
        
        return NO;
        
    }
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::getDocument::"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        @try
        {
            
            cameraTag = @"getDocument";
            
            NSString *requestFor=[json valueForKey:@"requestFor"];
            NSString *success=[json valueForKey:@"imageSuccessCallBack"];
            NSString *failure=[json valueForKey:@"imageFailCallBack"];
            
            fileSizeString       = [NSString stringWithFormat:@"%@",[json valueForKey:@"size"]];
            cropWidth  = [[json valueForKey:@"cropwidth"] integerValue];
            cropHeight = [[json valueForKey:@"cropheight"] integerValue];
            
            needCrop          = [[json valueForKey:@"crop"] boolValue];
            
            [self openChooseFrom:requestFor withsuccessCallBack:success withfailureCallback:failure];
            
        } @catch (NSException *exception) {
        } @finally {
        }
        return NO;
    }
    
    //===================================================================
    //================    Method to send SMS ============================
    //===================================================================

    
    //  window.location="iOS::openComponseSms::"+{"number":"+919999999999","body":"this is message body"}

    if ([[[request URL] absoluteString] hasPrefix:@"ios::openComponseSms::"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        //   NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        @try {
            NSString *number=[json valueForKey:@"number"];
            NSString *txtBody=[json valueForKey:@"body"];
            [self openSMSComposer:number withTxtBody:txtBody];
            
        } @catch (NSException *exception) {
        } @finally {
        }
        return NO;
    }
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::mpMobileCallback::"])
    {
        
        
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [components objectAtIndex:2];
        NSLog(@"CONSOLE OUTPUT=%@",argsAsString);
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\\\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        // argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"" withString:@""];
        
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        
        
        
        @try {
            
            NSString *email=[json valueForKey:@"email"];
            
            NSString *formname=[json valueForKey:@"formname"];
            NSString *mailbody=[json valueForKey:@"mailbody"];
            NSString *subject=[json valueForKey:@"subject"];
            
            NSString *title =[json valueForKey:@"title"];
            NSString *type  =[json valueForKey:@"type"];
            NSString *url   =[json valueForKey:@"url"];
            
            
            [self PanDeptFileHandling:email withSub:subject withType:type withMailBody:mailbody withfile:url withFormName:formname withCallback:title];
            
            
            
        } @catch (NSException *exception) {
        } @finally {
        }
        return NO;
    }
    
    
    
    
    return YES;
    
}


//=====================================================================
//====================   openSMSComposer  =============================
//=====================================================================


-(void)openSMSComposer:(NSString*)number withTxtBody:(NSString*)txtbody
{
    
    
    NSString *numberString=[NSString stringWithFormat:@"%@",number];
    
    NSArray *numberitems = [numberString componentsSeparatedByString:@","];

    if([MFMessageComposeViewController canSendText])
    {
        MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init]; // Create message VC
        messageController.messageComposeDelegate = self; // Set delegate to current instance
        
        NSMutableArray *recipients = [[NSMutableArray alloc] init]; // Create an array to hold the recipients
        
        recipients = [numberitems mutableCopy];
        //[recipients addObject:number]; // Append example phone number to array
       
    
        messageController.recipients = recipients; // Set the recipients of the message to the created array
        
        messageController.body = txtbody; // Set initial text to example message
        
        dispatch_async(dispatch_get_main_queue(), ^{ // Present VC when possible
            [self presentViewController:messageController animated:YES completion:NULL];
            
        });
    }
    
}



- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil) message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}



//=====================================================================
//=============ShareEpathsalaChapter with ChapterId====================
//=====================================================================



-(void)ShareEpathsalaChapter:(NSString*)chapterId
{
    
    NSArray *bookInfo=[singleton.dbManager getChapterDetailFromChapterId:singleton.user_id withchapterId:chapterId];
    
    NSLog(@"array=%@",bookInfo);
    
    if ([bookInfo count]>0) {
        NSString *chapterPath=[[bookInfo objectAtIndex:0] valueForKey:@"CHAPTER_PATH"];
        
        NSLog(@"%@ is a directory", chapterPath);
        NSLog(@"Is directory");
        NSLog(@"chapterPath=%@",chapterPath);
        flagViewAppear=FALSE;
        
        
        [self shareEpathsalaEpubBook:chapterPath];
        
        
    }
}

//=============ShareEpathsalaChapter with ChapterId====================

- (void)shareEpathsalaEpubBook:(NSString*)file
{
    // create url
  /*
    NSURL *url = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingString:@"My File.txt"]];
    NSData *data = [NSData dataWithContentsOfFile:file];
    // write data
    [data writeToURL:url atomically:NO];
*/
    
    
    
    NSURL *URL = [NSURL fileURLWithPath:file];
    
   // NSError* error = nil;
    // create activity view controller
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[URL] applicationActivities:nil];
    
    [activityViewController setCompletionWithItemsHandler:^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError)
     {
        //Delete file
       /* NSError *errorBlock;
        if([[NSFileManager defaultManager] removeItemAtURL:URL error:&errorBlock] == NO)
        {
            NSLog(@"error deleting file %@",error);
            return;
        }
         */
         return;

    }];
    UIViewController *topvc=[self topMostController];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [topvc presentViewController:activityViewController animated:YES completion:nil];
    }
    //if iPad
    else {
        // Change Rect to position Popover
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityViewController];
        [popup presentPopoverFromRect:CGRectMake(topvc.view.frame.size.width/2, topvc.view.frame.size.height/4, 0, 0)inView:topvc.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    }
        
    }
    
    
    
     /*
      
      //commit below code as its not working
    if (file) {
        
        NSData *pdfData = [NSData dataWithContentsOfFile:file];
        
        
        
        
        
      
        UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:@[@"Test", pdfData] applicationActivities:nil];
        
        //if iPhone
        
        UIViewController *topvc=[self topMostController];
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            
            [topvc presentViewController:controller animated:YES completion:nil];
            
        }
        
        //if iPad
        
        else {
            
            // Change Rect to position Popover
            
            UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:controller];
            
            [popup presentPopoverFromRect:CGRectMake(topvc.view.frame.size.width/2, topvc.view.frame.size.height/4, 0, 0)inView:topvc.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            
        }
        
      
    }
    
}

*/


//==========================================
//        DPI conversion code
//==========================================



-(NSData *)changeMetaDataInImage:(NSData*)sourceImageData withDpi:(int)dpiUsed
{
    // NSData *sourceImageData = [[NSData alloc] initWithContentsOfFile:@"~/Desktop/1.jpg"];
    if (sourceImageData != nil)
    {
        CGImageSourceRef source = CGImageSourceCreateWithData((__bridge CFDataRef)sourceImageData, NULL);
        
        NSDictionary *metadata = (__bridge_transfer NSDictionary *)CGImageSourceCopyPropertiesAtIndex(source, 0, NULL);
        
        NSMutableDictionary *tempMetadata = [metadata mutableCopy];
        [tempMetadata setObject:[NSNumber numberWithInt:dpiUsed] forKey:@"DPIHeight"];
        [tempMetadata setObject:[NSNumber numberWithInt:dpiUsed] forKey:@"DPIWidth"];
        
        NSMutableDictionary *EXIFDictionary = [[tempMetadata objectForKey:(NSString *)kCGImagePropertyTIFFDictionary] mutableCopy];
        [EXIFDictionary setObject:[NSNumber numberWithInt:dpiUsed] forKey:(NSString *)kCGImagePropertyTIFFXResolution];
        [EXIFDictionary setObject:[NSNumber numberWithInt:dpiUsed] forKey:(NSString *)kCGImagePropertyTIFFYResolution];
        
        NSMutableDictionary *JFIFDictionary = [[NSMutableDictionary alloc] init];
        [JFIFDictionary setObject:[NSNumber numberWithInt:dpiUsed] forKey:(NSString *)kCGImagePropertyJFIFXDensity];
        [JFIFDictionary setObject:[NSNumber numberWithInt:dpiUsed] forKey:(NSString *)kCGImagePropertyJFIFYDensity];
        [JFIFDictionary setObject:@"1" forKey:(NSString *)kCGImagePropertyJFIFVersion];
        
        [tempMetadata setObject:EXIFDictionary forKey:(NSString *)kCGImagePropertyTIFFDictionary];
        [tempMetadata setObject:JFIFDictionary forKey:(NSString *)kCGImagePropertyJFIFDictionary];
        
        NSMutableData *destinationImageData = [NSMutableData data];
        
        CFStringRef UTI = CGImageSourceGetType(source);
        
        CGImageDestinationRef destination = CGImageDestinationCreateWithData((__bridge CFMutableDataRef)destinationImageData, UTI, 1, NULL);
        
        CGImageDestinationAddImageFromSource(destination, source,0, (__bridge CFDictionaryRef) tempMetadata);
        
        CGImageDestinationFinalize(destination);
        
        return destinationImageData;
    }
    return sourceImageData;
}



//=====================================================================
//==================         Black & White Image Convert       ===================
//=====================================================================


-(UIImage *)convertImageToGrayScale:(UIImage *)image {
    // Create image rectangle with current image width/height
    CGRect imageRect = CGRectMake(0, 0, image.size.width, image.size.height);
    
    // Grayscale color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    
    // Create bitmap content with current image size and grayscale colorspace
    CGContextRef context = CGBitmapContextCreate(nil, image.size.width, image.size.height, 8, 0, colorSpace, kCGImageAlphaNone);
    
    // Draw image into current context, with specified rectangle
    // using previously defined context (with grayscale colorspace)
    CGContextDrawImage(context, imageRect, [image CGImage]);
    
    // Create bitmap image info from pixel data in current context
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    
    // Release colorspace, context and bitmap information
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    
    context = CGBitmapContextCreate(nil,image.size.width, image.size.height, 8, 0, nil, kCGImageAlphaOnly );
    CGContextDrawImage(context, imageRect, [image CGImage]);
    CGImageRef mask = CGBitmapContextCreateImage(context);
    
    // Create a new UIImage object
    UIImage *newImage = [UIImage imageWithCGImage:CGImageCreateWithMask(imageRef, mask)];
    CGImageRelease(imageRef);
    CGImageRelease(mask);
    
    // Return the new grayscale image
    return newImage;
}



//=====================================================================
//==================         Camera Delegates       ===================
//=====================================================================


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    // cameraTag=@"getSignatureFile";
    
    if ([cameraTag isEqualToString:@"RecordVideo"])
    {
        // grab our movie URL
        flagViewAppear=FALSE;
        
        NSURL *chosenMovie = [info objectForKey:UIImagePickerControllerMediaURL];
        // save it to the documents directory
        NSURL *fileURL = [self grabFileURL:@"video.mov"];
        NSData *movieData = [NSData dataWithContentsOfURL:chosenMovie];
        [movieData writeToURL:fileURL atomically:YES];
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        [self performSelector:@selector(setSizeValueinLabel:) withObject:fileURL afterDelay:3];
        // [self setSizeValueinLabel:fileURL];
        // save it to the Camera Roll
        UISaveVideoAtPathToSavedPhotosAlbum([chosenMovie path], nil, nil, nil);
        
        // and dismiss the picker
        
    }
    else if ([cameraTag isEqualToString:@"LoadVideo"])
    {
        flagViewAppear=FALSE;
        
        NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        
        NSLog(@"VideoURL = %@", videoURL);
        [picker dismissViewControllerAnimated:YES completion:NULL];
        
        NSData *movieData = [NSData dataWithContentsOfURL:videoURL];
        NSURL *fileURL = [self grabFileURL:@"video.mov"];
        
        [movieData writeToURL:fileURL atomically:YES];
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        [self performSelector:@selector(setSizeValueinLabel:) withObject:fileURL afterDelay:3];
        // [self setSizeValueinLabel:fileURL];
        // save it to the Camera Roll
        
        
        
    }
    else if ([cameraTag isEqualToString:@"proofImage"])
    {
        UIImage *beforeCrop = [info objectForKey:UIImagePickerControllerEditedImage];
        
        
        NSData *imgData = UIImageJPEGRepresentation(beforeCrop, 1); //1 it represents the quality of the image.
        
        NSData *DpiImgData=[self changeMetaDataInImage:imgData withDpi:300];
        
        NSLog(@"Size of Image(kb):%d",(int)round([DpiImgData length] / 1024));
        
        UIImage *dpi300image = [[UIImage alloc] initWithData:DpiImgData];
        
        int imgSize=(int)round([DpiImgData length] / 1024);
        
        
        if(imgSize>300)//size should be less then 300kb
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Image Size" message:@"Image Size is greater then 300Kb" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [alert show];
            
            //fail call back
            NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",imageFailCallBack,@"Action canceled"];
            [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
            
        }
        else
        {
            camera_img_base64 = [UIImagePNGRepresentation(dpi300image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];//[UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            // [self encodeToBase64String:dpi300image];
            NSString *returnString=[NSString stringWithFormat:@"data:image/png;base64,%@",camera_img_base64];
            NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",imageSuccessCallBack,returnString];
            NSString *javascriptStringtrim = [javascriptString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            NSString *newString = [[javascriptStringtrim componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
            //   NSLog(@"newString  =%@",newString);
            [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:newString waitUntilDone:NO];
            //            camera_img_base64 = [self encodeToBase64String:dpi300image];
            //
            //           // NSString *trimmedString = [camera_img_base64 stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            //           // NSString *newString1 = [trimmedString stringByReplacingOccurrencesOfString:@" " withString:@""];
            //
            //
            //            NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",imageSuccessCallBack,camera_img_base64];
            //            NSString *javascriptStringtrim = [javascriptString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            //            NSString *newString = [[javascriptStringtrim componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
            //
            //            [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:newString waitUntilDone:NO];
            
        }
        
        flagViewAppear=FALSE;
        [picker dismissViewControllerAnimated:YES completion:NULL];
        
    }
    
    else if ([cameraTag isEqualToString:@"getPhotoFile"])
    {
        UIImage *beforeCrop = [info objectForKey:UIImagePickerControllerEditedImage];
        CGSize destinationSize = CGSizeMake(213, 213);
        UIGraphicsBeginImageContext(destinationSize);
        [beforeCrop drawInRect:CGRectMake(0,0,destinationSize.width,destinationSize.height)];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        
        
        NSData *imgData = UIImageJPEGRepresentation(newImage, 1); //1 it represents the quality of the image.
        
        
        NSData *DpiImgData=[self changeMetaDataInImage:imgData withDpi:300];
        
        NSLog(@"Size of Image(kb):%d",(int)round([DpiImgData length] / 1024));
        
        UIImage *dpi300image = [[UIImage alloc] initWithData:DpiImgData];
        
        int imgSize=(int)round([DpiImgData length] / 1024);
        if(imgSize>30)//size should be less then 30kb
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Image Size" message:@"Image Size is greater then 30Kb" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [alert show];
            //fail callback
            NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",imageFailCallBack,@"Action canceled"];
            [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
            
        }
        else
        {
            camera_img_base64 = [UIImagePNGRepresentation(dpi300image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];//[UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            // [self encodeToBase64String:dpi300image];
            NSString *returnString=[NSString stringWithFormat:@"data:image/png;base64,%@",camera_img_base64];
            NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",imageSuccessCallBack,returnString];
            NSString *javascriptStringtrim = [javascriptString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            NSString *newString = [[javascriptStringtrim componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
            //   NSLog(@"newString  =%@",newString);
            [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:newString waitUntilDone:NO];
            //            camera_img_base64 = [self encodeToBase64String:dpi300image];
            //            NSString *returnString=[NSString stringWithFormat:@"data:image/png;base64,%@",camera_img_base64];
            //            NSString *javascriptString = [NSString stringWithFormat:@"setTimeout(%@('%@'),0);",imageSuccessCallBack,returnString];
            //            NSString *javascriptStringtrim = [javascriptString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            //            NSString *newString = [[javascriptStringtrim componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
            //            NSLog(@"newString  =%@",newString);
            //            [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:newString waitUntilDone:NO];
            
        }
        flagViewAppear=FALSE;
        [picker dismissViewControllerAnimated:YES completion:NULL];
        
    }
    else if ([cameraTag isEqualToString:@"getSignatureFile"])
    {
        UIImage *beforeCrop = [info objectForKey:UIImagePickerControllerEditedImage];
        
        CGSize destinationSize = CGSizeMake(213, 213);
        UIGraphicsBeginImageContext(destinationSize);
        [beforeCrop drawInRect:CGRectMake(0,0,destinationSize.width,destinationSize.height)];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        
        
        NSData *imgData = UIImageJPEGRepresentation(newImage, 1); //1 it represents the quality of the image.
        
        NSData *DpiImgData=[self changeMetaDataInImage:imgData withDpi:600];
        
        NSLog(@"Size of Image(kb):%d",(int)round([DpiImgData length] / 1024));
        
        UIImage *dpi600image = [[UIImage alloc] initWithData:DpiImgData];
        
        UIImage *blackwhiteImg=[self convertImageToGrayScale:dpi600image];
        
        NSData *BWimgData = UIImageJPEGRepresentation(blackwhiteImg, 1);
        
        int imgSize=(int)round([BWimgData length] / 1024);
        
        
        
        if(imgSize>60)//size should be less then 30kb
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Image Size" message:@"Image Size is greater then 60Kb" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [alert show];
            //fail callback
            NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",imageFailCallBack,@"Action canceled"];
            [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
            
        }
        else
        {
            camera_img_base64 = [UIImagePNGRepresentation(blackwhiteImg) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];//[UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            // [self encodeToBase64String:dpi300image];
            NSString *returnString=[NSString stringWithFormat:@"data:image/png;base64,%@",camera_img_base64];
            NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",imageSuccessCallBack,returnString];
            NSString *javascriptStringtrim = [javascriptString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            NSString *newString = [[javascriptStringtrim componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
            //   NSLog(@"newString  =%@",newString);
            [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:newString waitUntilDone:NO];
            
            //            camera_img_base64 = [self encodeToBase64String:blackwhiteImg];
            //            NSString *returnString=[NSString stringWithFormat:@"data:image/png;base64,%@",camera_img_base64];
            //            NSString *javascriptString = [NSString stringWithFormat:@"setTimeout(%@('%@'),0);",imageSuccessCallBack,returnString];
            //            NSString *javascriptStringtrim = [javascriptString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            //            NSString *newString = [[javascriptStringtrim componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
            //            NSLog(@"newString  =%@",newString);
            //            [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:newString waitUntilDone:NO];
            
        }
        flagViewAppear=FALSE;
        [picker dismissViewControllerAnimated:YES completion:NULL];
        
    }
    else if ([cameraTag isEqualToString:@"getPDF"])
    {
        UIImage *beforeCrop = [info objectForKey:UIImagePickerControllerOriginalImage];
        CGSize destinationSize = CGSizeMake(500, 500);
        UIGraphicsBeginImageContext(destinationSize);
        [beforeCrop drawInRect:CGRectMake(0,0,destinationSize.width,destinationSize.height)];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        NSData *pdfData =  [PDFImageConverter convertImageToPDF:newImage];
        
        
        
        int imgSize=(int)round([pdfData length] / 1024);
        
        fileSizeString = fileSizeString.length == 0 || fileSizeString == nil || fileSizeString == (NSString *)[NSNull null]? 0 : fileSizeString;
        
        if(imgSize > [fileSizeString integerValue])//size should be less then 30kb
        {
            //            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"PDF Size" message:@"PDF Size is greater then 200Kb" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            //            [alert show];
            //fail callback
            NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",camera_failCallback,@"Action canceled"];
            [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
            flagViewAppear=FALSE;
            
            [picker dismissViewControllerAnimated:YES completion:NULL];
        }
        else
        {
            camera_img_base64 = [pdfData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            
            //camera_img_base64 = [self encodeToBase64String:newImage];
            NSString *returnString=[NSString stringWithFormat:@"data:application/pdf;base64,%@",camera_img_base64];
            NSString *javascriptString = [NSString stringWithFormat:@"setTimeout(%@('%@'),0);",camera_successCallback,returnString];
            NSString *javascriptStringtrim = [javascriptString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            NSString *newString = [[javascriptStringtrim componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
            
            NSLog(@"newString  =%@",newString);
            [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:newString waitUntilDone:NO];
            flagViewAppear=FALSE;
            [picker dismissViewControllerAnimated:YES completion:NULL];
        }
        
        
    }
    // [cameraTag isEqualToString:@"getEdistrictOriginalImage"]
    else if ([cameraTag isEqualToString:@"getEdistrictOriginalImage"])
    {
        UIImage *beforeCrop = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        NSData *imageData = UIImageJPEGRepresentation(beforeCrop, 0.5);
        
        if (imageData.length/1024 > [fileSizeString integerValue])
        {
            NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",imageFailCallBack,@"File Size Exceed"];
            [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
            flagViewAppear=FALSE;
            [picker dismissViewControllerAnimated:YES completion:NULL];
        }
        else
        {
            camera_img_base64 = [self encodeToBase64String:beforeCrop];
            
            NSString *returnString=[NSString stringWithFormat:@"data:image/png;base64,%@",camera_img_base64];
            NSString *javascriptString = [NSString stringWithFormat:@"setTimeout(%@('%@'),0);",imageSuccessCallBack,returnString];
            NSString *javascriptStringtrim = [javascriptString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            NSString *newString = [[javascriptStringtrim componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
            
            NSLog(@"newString  =%@",newString);
            [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:newString waitUntilDone:NO];
            flagViewAppear=FALSE;
            [picker dismissViewControllerAnimated:YES completion:NULL];
        }
        
        
    }
    else if ([cameraTag isEqualToString:@"getDocument"])
    {
        UIImage *beforeCrop = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        CGSize destinationSize = CGSizeMake(cropWidth, cropHeight);
        UIGraphicsBeginImageContext(destinationSize);
        [beforeCrop drawInRect:CGRectMake(0,0,destinationSize.width,destinationSize.height)];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        
        NSData *imageData = UIImageJPEGRepresentation(newImage, 0.5);
        
        if (imageData.length/1024 > [fileSizeString integerValue])
        {
            NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",camera_failCallback,@"File Size Exceed"];
            [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
            flagViewAppear=FALSE;
            [picker dismissViewControllerAnimated:YES completion:NULL];
        }
        else
        {
            camera_img_base64 = [self encodeToBase64String:beforeCrop];
            
            NSString *returnString=[NSString stringWithFormat:@"data:image/png;base64,%@",camera_img_base64];
            NSString *javascriptString = [NSString stringWithFormat:@"setTimeout(%@('%@'),0);",camera_successCallback,returnString];
            NSString *javascriptStringtrim = [javascriptString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            NSString *newString = [[javascriptStringtrim componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
            
            NSLog(@"newString  =%@",newString);
            [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:newString waitUntilDone:NO];
            flagViewAppear=FALSE;
            [picker dismissViewControllerAnimated:YES completion:NULL];
        }
    }
    else
    {
        UIImage *beforeCrop = [info objectForKey:UIImagePickerControllerEditedImage];
        CGSize destinationSize = CGSizeMake(100, 100);
        UIGraphicsBeginImageContext(destinationSize);
        [beforeCrop drawInRect:CGRectMake(0,0,destinationSize.width,destinationSize.height)];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        camera_img_base64 = [self encodeToBase64String:newImage];
        NSString *returnString=[NSString stringWithFormat:@"data:image/png;base64,%@",camera_img_base64];
        NSString *javascriptString = [NSString stringWithFormat:@"setTimeout(%@('%@'),0);",camera_successCallback,returnString];
        NSString *javascriptStringtrim = [javascriptString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSString *newString = [[javascriptStringtrim componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
        
        NSLog(@"newString  =%@",newString);
        [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:newString waitUntilDone:NO];
        flagViewAppear=FALSE;
        [picker dismissViewControllerAnimated:YES completion:NULL];
    }
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    if ([cameraTag isEqualToString:@"RecordVideo"])
    {
        NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",videoFailCallBack,@"F"];
        
        [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
        flagViewAppear=FALSE;
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
    
    
    else if ([cameraTag isEqualToString:@"LoadVideo"])
    {
        
        
        NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",videoFailCallBack,@"F"];
        
        [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
        flagViewAppear=FALSE;
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
    else if ([cameraTag isEqualToString:@"getPhotoFile"]||[cameraTag isEqualToString:@"proofImage"]||[cameraTag isEqualToString:@"getSignatureFile"])
    {
        NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",imageFailCallBack,@"Action canceled"];
        [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
        flagViewAppear=FALSE;
        [picker dismissViewControllerAnimated:YES completion:NULL];
    }
    else if ([cameraTag isEqualToString:@"getEdistrictOriginalImage"])
    {
        NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",imageFailCallBack,@"Action canceled"];
        [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
        flagViewAppear=FALSE;
        [picker dismissViewControllerAnimated:YES completion:NULL];
    }
    else
    {
        NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",camera_failCallback,@"Action canceled"];
        // [self.webView stringByEvaluatingJavaScriptFromString:javascriptString];
        [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
        
        
        flagViewAppear=FALSE;
        
        
        [picker dismissViewControllerAnimated:YES completion:NULL];
    }
}

//=====================================================================
//==================         SHARE PDF FILE       ========================
//=====================================================================

-(void)shareBase64MethodPANFile:(NSString*)localImgpath
{
    NSString *path = localImgpath;
    NSURL *targetURL = [NSURL fileURLWithPath:path];
    
    NSArray *activityItems = [NSArray arrayWithObjects:targetURL, nil];
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    
    ;
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [self presentViewController:activityViewController animated:YES completion:nil];
    }
    //if iPad
    else {
        // Change Rect to position Popover
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityViewController];
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
    
}


//=====================================================================
//==================         openBrowser       ========================
//=====================================================================
-(void)openBrowser:(NSString*)fileURL
{
    
    NSURL *urltoOpen=[NSURL URLWithString:fileURL];
    [[UIApplication sharedApplication] openURL:urltoOpen];
    
    
}


//=====================================================================
//==================       Select/Record Video ========================
//=====================================================================
-(void)setSizeValueinLabel:(NSURL*)fileURL
{
    
    NSNumber *fileSizeValue = nil;
    NSError *fileSizeError = nil;
    [fileURL getResourceValue:&fileSizeValue
                       forKey:NSURLFileSizeKey
                        error:&fileSizeError];
    if (fileSizeValue) {
        NSLog(@"value for %@ is %@", fileURL, fileSizeValue);
    }
    else {
        NSLog(@"error getting size for url %@ error was %@", fileURL, fileSizeError);
        
    }
    
    
    NSLog(@"downloadfile size %@", [self readableValueWithBytes:fileSizeValue]);
    
    NSString *filesizeString=[NSString stringWithFormat:@"%@",[self readableValueWithBytes:fileSizeValue]];
    
    if ([filesizeString rangeOfString:@"MB"].location == NSNotFound) {
        NSLog(@"filesizeString does not contain MB");
        
        if ([filesizeString rangeOfString:@"KB"].location == NSNotFound)
        {
            NSLog(@"filesizeString does not contain KB");
        }
        else
        {
            NSFileManager *fileManager = [NSFileManager defaultManager];
            NSLog(@"value of fileURL=%@",[fileURL path]);
            
            
            NSString * base64String =[self base64StringFromFileAtPath:fileURL];
            
            if([base64String length]==0)
            {
                NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",videoFailCallBack,@"F"];
                
                [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
            }
            
            else
            {
                
                NSString * trimmedString = [base64String stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
                trimmedString = [NSString stringWithFormat:@"%@#%@",trimmedString,fileSizeValue];
                
                NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",videoSuccessCallBack,trimmedString];
                
                [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
                
                
                
            }
            [fileManager removeItemAtPath:[fileURL path]  error:NULL];
        }
        
        
    } else {
        NSLog(@"filesizeString contains MB!");
        NSString *str=[filesizeString stringByReplacingOccurrencesOfString:@"MB" withString:@""];
        
        str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        
        int mbValue=[str intValue];
        if (mbValue>10)
        {
            
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"File Size More then 10 MB to attach" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            NSLog(@"value of fileURL=%@",[fileURL path]);
            [fileManager removeItemAtPath:[fileURL path]  error:NULL];
            
            NSString * base64String =[self base64StringFromFileAtPath:fileURL];
            
            if([base64String length]==0)
            {
                NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",videoFailCallBack,@"F"];
                
                [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
            }
            
            else
            {
                
                NSString * trimmedString = [base64String stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
                trimmedString = [NSString stringWithFormat:@"%@#%@",trimmedString,fileSizeValue];
                
                NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",videoSuccessCallBack,trimmedString];
                
                [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
                
                
                
            }
            
            
        }
        else
        {
            NSString * base64String =[self base64StringFromFileAtPath:fileURL];
            
            NSString * trimmedString = [base64String stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            
            trimmedString = [NSString stringWithFormat:@"%@#%@",trimmedString,fileSizeValue];
            
            
            NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",videoSuccessCallBack,trimmedString];
            
            [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
            
            
            
        }
    }
    
    
    
}

- (NSString *) base64StringFromFileAtPath: (NSURL*) filePath {
    // NSData * dataFromFile = [NSData dataWithContentsOfFile:filePath];
    
    // NSString *path = [filePath path];
    NSData *fileData = [NSData dataWithContentsOfFile:filePath.path];
    
    if(fileData==nil)
    {
        //No file found base64 blank
    }
    
    return [fileData base64Encoding];
}


- (NSString *)readableValueWithBytes:(id)bytes
{
    NSString *readable;
    
    
    //round bytes to one kilobyte, if less than 1024 bytes
    if (([bytes longLongValue] < 1024)){
        
        unsigned long long b = ([bytes longLongValue]);
        float a = (float) b;
        readable = [NSString stringWithFormat:@"%.02f KB", a];
        
        //readable = [NSString stringWithFormat:@"1 KB"];
    }
    
    //kilobytes
    if (([bytes longLongValue]/1024)>=1){
        
        
        
        //readable = [NSString stringWithFormat:@"%lld KB", ([bytes longLongValue]/1024)];
        
        unsigned long long b = ([bytes longLongValue]/1024);
        float a = (float) b;
        
        readable = [NSString stringWithFormat:@"%.02f KB", a];
        
    }
    
    //megabytes
    if (([bytes longLongValue]/1024/1024)>=1){
        
        //  readable = [NSString stringWithFormat:@"%lld MB", ([bytes longLongValue]/1024/1024)];
        
        
        unsigned long long b = ([bytes longLongValue]/1024/1024);
        float a = (float) b;
        
        readable = [NSString stringWithFormat:@"%.02f MB", a];
        
    }
    
    return readable;
}


- (NSURL*)grabFileURL:(NSString *)fileName
{
    
    // find Documents directory
    NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    
    // append a file name to it
    documentsURL = [documentsURL URLByAppendingPathComponent:fileName];
    
    
    
    
    return documentsURL;
}


-(void)openRecordingVideo
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        flagViewAppear=FALSE;
        cameraTag=@"RecordVideo";
        
        UIImagePickerController *picker = [[UIImagePickerController alloc]init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.videoQuality=UIImagePickerControllerQualityTypeLow;
        //picker.videoQuality=UIImagePickerControllerQualityTypeHigh;
        picker.videoMaximumDuration = 60.0f;
        
        
        picker.delegate = self;
        
        picker.allowsEditing = NO;
        
        NSArray *mediaTypes = [[NSArray alloc]initWithObjects:(NSString *)kUTTypeMovie, nil];
        
        picker.mediaTypes = mediaTypes;
        
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
            UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:picker];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [popover presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
            });
        }
        else
        {
            [self presentViewController:picker animated:YES completion:nil];
        }
        
    } else {
        
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"I'm afraid there's no camera on this device!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    
    
}

-(void)openRecordVideoMethod
{
    flagViewAppear=FALSE;
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:[NSLocalizedString(@"camera", nil) capitalizedString],[NSLocalizedString(@"gallery", nil) capitalizedString], nil];
    
    actionSheet.delegate=self;
    actionSheet.tag=999;
    [actionSheet showInView:self.view];
    
    
    
    // Your code here for record video
    
    /*
     // success case
     
     NSString *base64VideoString=@<get base 64 string video">
     
     
     NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",videoSuccessCallBack,base64VideoString];
     
     [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
     }
     
     */
    /*
     // FAIL case
     
     
     
     NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",videoFailCallBack,@"F"];
     
     [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
     }
     
     */
    
}







/*
 
 - (IBAction)playbackButton:(id)sender {
 
 // pick a video from the documents directory
 NSURL *video = [self grabFileURL:@"video.mov"];
 
 // create a movie player view controller
 MPMoviePlayerViewController * controller = [[MPMoviePlayerViewController alloc]initWithContentURL:video];
 [controller.moviePlayer prepareToPlay];
 [controller.moviePlayer play];
 
 // and present it
 [self presentMoviePlayerViewControllerAnimated:controller];
 
 }
 
 */

//=====================================================================
//==================       Select/Record Audio ========================
//=====================================================================

-(void)openRecordAudioMethod
{
    flagViewAppear=FALSE;
    
    DeptAudioRecordVC *audioVC=[[DeptAudioRecordVC alloc] initWithNibName:@"DeptAudioRecordVC" bundle:nil];
    audioVC.delegate=self;
    audioVC.providesPresentationContextTransitionStyle = YES;
    audioVC.definesPresentationContext = YES;
    [audioVC setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self presentViewController:audioVC animated:NO completion:nil];
}


- (void)audioBase64ReturnToDept:(NSDictionary*)parameters;
{
    
    
    NSLog(@"the base64String parameters: %@",parameters[@"base64String"]);
    
    
    NSString *baseString = parameters[@"base64String"];
    if (parameters == nil || baseString.length == 0)
    {
        NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",AudioFailCallBack,@"F"];
        
        [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
        
        return;
    }
    
    NSString *base64AudioString=[NSString stringWithFormat:@"%@#%@",parameters[@"base64String"],parameters[@"audioData"]];
    
    base64AudioString = [base64AudioString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
    if ([base64AudioString length]==0||base64AudioString==nil)
    {
        NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",AudioFailCallBack,@"F"];
        
        [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
    }
    else
    {
        NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",AudioSuccessCallBack,base64AudioString];
        
        [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
    }
}







//=====================================================================
//==================     Select/Capture Images ========================
//=====================================================================

-(void)openSelectImageMethod
{
    flagViewAppear=FALSE;
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"ImageStoryboard" bundle:nil];
    BLSelectPhotosAndVideosVC *blvc = [storyboard instantiateViewControllerWithIdentifier:@"BLSelectPhotosAndVideosVC"];
    blvc.chooseMediaType = @"TypePhoto";//TypeVideo
    blvc.delegate=self;
    [self presentViewController:blvc animated:NO completion:nil];
    
    
    // Your code here for record video
    
    /*
     // success case
     
     NSString *base64ImageString=@<get base 64 string images max 3 comma seprated">
     
     
     NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",imageSuccessCallBack,base64ImageString];
     
     [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
     }
     
     */
    /*
     // FAIL case
     
     
     
     NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",imageFailCallBack,@"F"];
     
     [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
     }
     
     */
    
}
- (void)selectimageVideoReturnToDept:(NSDictionary*)parameters;
{
    //=========== get base 64 from video its size and then convert base 64========to pass
    NSLog(@"parameters=%@",parameters);
    
    
    [akpsImageDataArray removeAllObjects];
    
    NSString *type=[NSString stringWithFormat:@"%@",[parameters valueForKey:@"ChooseMediaType"]];
    //===============================
    
    
    if ([type isEqualToString:@"TypePhoto"]) //case image
    {
        
        NSLog(@"ArrayImgVid=%@",[parameters valueForKey:@"ArrayImgVid"]);
        
        
        NSMutableArray *dataArray = [NSMutableArray new];
        
        
        NSMutableArray *base64Array=[NSMutableArray new];
        NSArray *imageArray=[parameters valueForKey:@"ArrayImgVid"];
        for (int i=0; i<[imageArray count]; i++)
        {
            
            
            dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),^
                          {
                              //set your image on main thread.
                              BLPhoto *item1= (BLPhoto *)[imageArray objectAtIndex:i];
                              UIImage *image=item1.thumbnail;
                              
                              CGSize destinationSize = CGSizeMake(100, 100);
                              UIGraphicsBeginImageContext(destinationSize);
                              [image drawInRect:CGRectMake(0,0,destinationSize.width,destinationSize.height)];
                              UIImage *newCropImage = UIGraphicsGetImageFromCurrentImageContext();
                              UIGraphicsEndImageContext();
                              
                              
                              NSData *imageData = UIImagePNGRepresentation(newCropImage);
                              
                              if (imageData != nil)
                              {
                                  [dataArray addObject:[NSNumber numberWithInteger:imageData.length]];
                              }
                              
                              NSString *img_base64 = [self encodeToBase64String:newCropImage];
                              dispatch_async(dispatch_get_main_queue(),^{
                                  
                                  [base64Array addObject:img_base64];
                                  
                              });
                          });
            
            
            
        }
        
        
        akpsImageDataArray = dataArray;
        
        [self performSelector:@selector(callwithDelayMultiImage:) withObject:base64Array afterDelay:5];
        
        
        
    }
    else //case video
    {
        NSLog(@"ArrayImgVid=%@",[parameters valueForKey:@"ArrayImgVid"]);
        
        
    }
    
    
    
}


// ------lokesh sir changes -----
-(void)callwithDelayMultiImage:(NSMutableArray*)base64Array
{
    NSLog(@"base64 Array=%@",base64Array);
    
    //  NSString*base64String=[base64Array componentsJoinedByString: @","];
    
    NSString *sample1 = @"";
    
    for (int i =0; i < base64Array.count; i++)
    {
        
        NSString *combinedString = @"";
        
        if (i == base64Array.count-1)
        {
            combinedString = [NSString stringWithFormat:@"%@#%@",[base64Array objectAtIndex:i],[akpsImageDataArray objectAtIndex:i]];
        }
        else
        {
            combinedString = [NSString stringWithFormat:@"%@#%@,",[base64Array objectAtIndex:i],[akpsImageDataArray objectAtIndex:i]];
        }
        
        sample1 = [sample1 stringByAppendingString:combinedString];
        
    }
    
    NSLog(@"%@",sample1);
    
    
    
    
    
    
    //NSString *base64ImageString=[base64Array componentsJoinedByString: @","];
    
    NSString *base64 = [sample1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
    //base64 = [NSString stringWithFormat:@"%@%@",base64,base64ImageString];
    
    NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",imageSuccessCallBack,base64];
    
    NSString *javascriptStringtrim = [javascriptString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *newString = [[javascriptStringtrim componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
    
    
    [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:newString waitUntilDone:NO];
}
- (void)selectimageVideoFailCaseToDept:(NSDictionary*)parameters
{
    NSLog(@"parameters=%@",parameters);
    
    
    
    NSString *type=[NSString stringWithFormat:@"%@",[parameters valueForKey:@"ChooseMediaType"]];
    //===============================
    
    if ([type isEqualToString:@"TypePhoto"]) //case image
    {
        NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",imageFailCallBack,@"F"];
        
        [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
        
        
    }
    else //case video
    {
        NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",videoFailCallBack,@"F"];
        
        [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
        
    }
    
    
    
}


//=====================================================================
//=====================================================================
//=====================================================================


//----- added later---------

-(void)clearValueOnlogout
{
    singleton = [SharedManager sharedSingleton];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:NO forKey:@"LOGIN_KEY"];
    [defaults synchronize];
    
    //------------------------- Encrypt Value------------------------
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    // Encrypt
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"TOKEN_KEY"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"USER_PIC"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"PROFILE_COMPELTE_KEY"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    //------------------------- Encrypt Value------------------------
    //——Remove Sharding Value——
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"NODE_KEY"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //----- remove sharding--------
    
    
    
    singleton.profileUserAddress = @"";
    singleton.imageLocalpath=@"";
    singleton.notiTypeGenderSelected=@"";
    singleton.profileNameSelected =@"";
    singleton.profilestateSelected=@"";
    singleton.notiTypeCitySelected=@"";
    singleton.notiTypDistricteSelected=@"";
    singleton.profileDOBSelected=@"";
    singleton.altermobileNumber=@"";
    singleton.user_Qualification=@"";
    singleton.user_Occupation=@"";
    singleton.user_profile_URL=@"";
    singleton.profileEmailSelected=@"";
    singleton.mobileNumber=@"";
    singleton.user_id=@"";
    singleton.user_tkn=@"";
    singleton.user_mpin=@"";
    singleton.user_aadhar_number=@"";
    singleton.objUserProfile = nil;
    singleton.imageLocalpath=@"";
    @try {
        [singleton.arr_recent_service removeAllObjects];
        
    } @catch (NSException *exception)
    {
        
    } @finally {
        
    }
    
    
    [[NSUserDefaults standardUserDefaults] setInteger:kLoginScreenCase forKey:kInitiateScreenKey];
    
    
    
    //------------------------- Encrypt Value------------------------
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    // Encrypt
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"lastFetchDate"];
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"lastFetchV1"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"USER_ID"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"TOKEN_KEY"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"USER_PIC"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"PROFILE_COMPELTE_KEY"];
    
    //------------------------- Encrypt Value------------------------
    
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSHTTPCookie *cookie;
    for (cookie in [storage cookies])
    {
        
        [storage deleteCookie:cookie];
        
    }
    NSMutableArray *cookieArray = [[NSMutableArray alloc] init];
    [[NSUserDefaults standardUserDefaults] setValue:cookieArray forKey:@"cookieArray"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //——Remove Sharding Value——
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"NODE_KEY"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // Logout from social frameworks as well.
    
    
    //------------------------- Encrypt Value------------------------
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    // Encrypt
    [[NSUserDefaults standardUserDefaults] encryptValue:@"NO" withKey:@"LINKDIGILOCKERSTATUS"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"digilocker_username"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"digilocker_password"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //------------------------- Encrypt Value------------------------
    
    [singleton.dbManager deleteBannerHomeData];
    [singleton.dbManager  deleteAllServices];
    [singleton.dbManager  deleteSectionData];
    singleton.imageLocalpath=@"";
    singleton.dbManager = [[UMSqliteManager alloc] initWithDatabaseFilename:@"UMANG_DATABASE.db"];
    [singleton.dbManager createUmangDB];
    // Start the notifier, which will cause the reachability object to retain itself!
    
    [defaults synchronize];
    
    
    
}
-(void)openLoginView
{
    singleton = [SharedManager sharedSingleton];
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    [objRequest clearValueOnlogout];
    
    [singleton.reach stopNotifier];
    //----later add
    [[NSUserDefaults standardUserDefaults] setInteger:kLoginScreenCase forKey:kInitiateScreenKey];
    [[NSUserDefaults standardUserDefaults] setBool:false forKey:kKeepMeLoggedIn];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //------
    
    
    UIStoryboard *storyboard =  [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    LoginAppVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginAppVC"];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        vc = [[LoginAppVC alloc] initWithNibName:@"LoginAppVC_iPad" bundle:nil];
    }
    
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    
    
    UIViewController *vc1=[self topMostController];
    
    
    [vc1 presentViewController:vc animated:NO completion:nil];
    
    
    
}

















//--------------------------------------------------------------
//-----------------START JAVASCRIPT METHODS---------------------
//--------------------------------------------------------------



-(void)openapplemap:(NSString*)url
{
    flagViewAppear=FALSE;
    
    
    
    NSString *googleMapsURLString = [url stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    //NSString *googleMapsURLString = [NSString stringWithFormat:@"%@",url];
    
    
    
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://"]])
        
    {
        NSLog(@"Map App Found");
        
        // googleMapsURLString = [googleMapsURLString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]; //IOS 9 and above use this line
        
        NSURL *mapURL=[NSURL URLWithString:[googleMapsURLString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
        NSLog(@"mapURL= %@",mapURL);
        
        [[UIApplication sharedApplication] openURL:mapURL];
        
    } else
    {
        //NSString* googleMapsURLString = [NSString stringWithFormat:@"https://maps.google.com/maps?&z=15&q=%.6f+%.6f&ll=%.6f+%.6f",[latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
        
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:googleMapsURLString]];
        
    }
    
    
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:map_url]];
    /*  UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
     vc.urltoOpen=map_url;
     vc.titleOpen=@"View Directions";
     vc.isfrom=@"WEBVIEWHANDLE";
     //[vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
     UIViewController *topvc=[self topMostController];
     [topvc presentViewController:vc animated:NO completion:nil];
     
     */
    
}
-(void)openEmailShareWithAttachment:(NSString*)emailReciept
{
    //  [dic_serviceInfo valueForKey:@"SERVICE_EMAIL"]
    
    
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    if ([MFMailComposeViewController canSendMail]) {
        
        
        NSString *receipt=emailReciept;
        // Email Subject
        NSString *emailTitle = @"Umang Services Email";
        // Email Content
        NSString *messageBody = @"Sent by Umang App!";
        // To address
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:receipt];
        
        picker.mailComposeDelegate = self;
        [picker setSubject:emailTitle];
        [picker setMessageBody:messageBody isHTML:YES];
        [picker setToRecipients:toRecipents];
        
        // Present mail view controller on screen
        [self presentViewController:picker animated:YES completion:NULL];
    }
    
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


-(void)openGoogleMap:(NSString*)urltoPass  withtitle:(NSString*)title withlat:(NSString*)latitude withlong:(NSString*)longitude
{
    flagViewAppear=FALSE;
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    vc.urltoOpen=urltoPass;
    vc.titleOpen=title;
    vc.isfrom=@"WEBVIEWHANDLE";
    // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    UIViewController *topvc=[self topMostController];
    [topvc presentViewController:vc animated:NO completion:nil];
    
    
}


//-----  Set Page Title------
/*
 @JavascriptInterface
 public void setPageTitle(final String title)
 */
-(void)setPageTitle:(NSString*)title
{
    //NSLog(@"setPageTitle final");
    // lblUmangHome.text=title;
    _lbltitle.text = title;
    // Objective-C
    // _lbltitle.lineBreakMode = NSLineBreakByWordWrapping;
    //_lbltitle.numberOfLines = 0;
    
    // CGRect currentFrame = _lbltitle.frame;
    //CGSize max = CGSizeMake(_lbltitle.frame.size.width-20, 100);
    //CGSize expected = [title sizeWithFont:_lbltitle.font constrainedToSize:max lineBreakMode:_lbltitle.lineBreakMode];
    
    //if (expected.height>35) {
    //  currentFrame.size.height = expected.height;
    //_lbltitle.frame = currentFrame;
    //}
    //_lbltitle.adjustsFontSizeToFitWidth = YES;
    
}



/*
 @JavascriptInterface
 public void showToast(String toast)
 */

-(void)showToast :(NSString *)toast
{
    [self.view makeToast:toast duration:5.0 position:CSToastPositionBottom];
}


//----- Open Chat Window
-(void)openChatScreen:(NSString*)title withURL:(NSString*)urltoPass
{
    
    
    flagViewAppear=FALSE;
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    vc.urltoOpen=urltoPass;
    vc.titleOpen=title;
    vc.isfrom=@"WEBVIEWHANDLE";
    // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    UIViewController *topvc=[self topMostController];
    [topvc presentViewController:vc animated:NO completion:nil];
    
    
}

//----- Open WebView Feedback Screen/ openWebViewFeedback(String url, String title)
-(void)openWebViewFeedback:(NSString*)url withTitle:(NSString*)title
{
    if ([self.webView canGoBack])
    {
        //flagViewAppear=TRUE;
        
        //[self.webView loadHTMLString:@"about:blank" baseURL:nil];
        self.webView=nil;
        self.webView=[[UIWebView alloc]init];
        // flagBack=TRUE;
        // flagViewAppear=true;
    }
    else
    {
        
    }
    
    
    flagViewAppear=FALSE;
    
    //NSLog(@"OpenWebViewFeedback");
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    vc.urltoOpen=url;
    vc.titleOpen=title;
    vc.isfrom=@"WEBVIEWHANDLEFEEDBACK";
    //[vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    UIViewController *topvc=[self topMostController];
    [topvc presentViewController:vc animated:NO completion:nil];
}



//----- Open Webview//openWebView(String url, String title)
-(void)openWebView:(NSString*)url withTitle:(NSString*)title
{
    flagViewAppear=FALSE;
    
    //NSLog(@"openWebView");
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    vc.urltoOpen=url;
    vc.titleOpen=title;
    vc.isfrom=@"WEBVIEWHANDLE";
    // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    UIViewController *topvc=[self topMostController];
    [topvc presentViewController:vc animated:NO completion:nil];
    
}



/*@JavascriptInterface
 public void downloadFile(String url, String jsonString)
 */


//-----   Digilocker File Download
-(void)downloadFile :(NSString*)fileurl withjsonString:(NSString*)jsonString
{
    //NSLog(@"downloadFile");
    // startDigiLockerDownload();
    
    
    NSError *error;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:fileurl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    /*NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
     @"IOS TYPE", @"typemap",
     nil];*/
    NSData *postData = [NSJSONSerialization dataWithJSONObject:jsonString options:0 error:&error];
    [request setHTTPBody:postData];
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          
                                          {
                                              
                                              //NSLog(@"data=%@",data);
                                              // [self saveDataToPDF:data];
                                              //NSLog(@"response=%@",response);
                                              //NSLog(@"error=%@",error);
                                              
                                          }];
    
    [postDataTask resume];
    
}

- (void)saveDataToPDF:(NSData *)pdfDocumentData
{
    //Create the pdf document reference
    CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData((CFDataRef)pdfDocumentData);
    CGPDFDocumentRef document = CGPDFDocumentCreateWithProvider(dataProvider);
    
    //Create the pdf context
    CGPDFPageRef page = CGPDFDocumentGetPage(document, 1); //Pages are numbered starting at 1
    CGRect pageRect = CGPDFPageGetBoxRect(page, kCGPDFMediaBox);
    CFMutableDataRef mutableData = CFDataCreateMutable(NULL, 0);
    
    ////NSLog(@"w:%2.2f, h:%2.2f",pageRect.size.width, pageRect.size.height);
    CGDataConsumerRef dataConsumer = CGDataConsumerCreateWithCFData(mutableData);
    CGContextRef pdfContext = CGPDFContextCreate(dataConsumer, &pageRect, NULL);
    
    
    if (CGPDFDocumentGetNumberOfPages(document) > 0)
    {
        //Draw the page onto the new context
        //page = CGPDFDocumentGetPage(document, 1); //Pages are numbered starting at 1
        
        CGPDFContextBeginPage(pdfContext, NULL);
        CGContextDrawPDFPage(pdfContext, page);
        CGPDFContextEndPage(pdfContext);
    }
    else
    {
        //NSLog(@"Failed to create the document");
    }
    
    CGContextRelease(pdfContext); //Release before writing data to disk.
    
    //Write to disk
    // [(__bridge NSData *)mutableData writeToFile:@"/Users/David/Desktop/test.pdf" atomically:YES];
    
    //NSString*pdfPath =[NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], @"downloadfile.pdf"];
    [(__bridge NSData *)mutableData writeToFile:[NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], @"downloadfile.pdf"] atomically:YES];
    //Clean up
    CGDataProviderRelease(dataProvider); //Release the data provider
    CGDataConsumerRelease(dataConsumer);
    CGPDFDocumentRelease(document);
    CFRelease(mutableData);
    [self performSelector:@selector(openPDFfile:) withObject:pdfDocumentData afterDelay:1];
    
}


/*
 public void startDigiLockerDownload() {
 ((DigiLockerWebViewScreen) mAct).startDownLoading(digiUrl, digiJson);
 
 
 }
 */






/*
 @JavascriptInterface
 public void downloadFileDept(String url, String jsonString)
 */


//- Digilocker Aadhaar/RC/License with open file dialog ///pdf
-(void)downloadFileDept :(NSString*)fileurl withjsonString:(NSString*)jsonString
{
    //NSLog(@"downloadFile");
    // startDigiLockerDownload();
    
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.label.text = NSLocalizedString(@"Please wait...", @"Please wait...");
    
    
    
    NSString *docName=[jsonString valueForKey:@"docName"];
    NSString *uri=[jsonString valueForKey:@"uri"];
    NSString *trkr=[jsonString valueForKey:@"trkr"];
    NSString *action=[jsonString valueForKey:@"action"];
    NSString *txn=[jsonString valueForKey:@"txn"];
    NSString *type=[jsonString valueForKey:@"type"];
    NSString *username=[jsonString valueForKey:@"username"];
    NSString *password=[jsonString valueForKey:@"password"];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    
    
    NSURL * url = [NSURL URLWithString:fileurl];
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    NSString * params =[NSString stringWithFormat:@"docName=%@&uri=%@&trkr=%@&action=%@&txn=%@&type=%@&username=%@&password=%@",docName,uri,trkr,action,txn,type,username,password];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [urlRequest setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                       completionHandler:^(NSData *data1, NSURLResponse *response, NSError *error) {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               [hud hideAnimated:YES];
                                                           });                                                           //NSLog(@"Response:%@ %@\n", response, error);
                                                           if(error == nil)
                                                           {
                                                               //NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                                                               
                                                               //NSLog(@"Data = %@",text);
                                                               
                                                               [self saveDataToPDF:data1];
                                                           }
                                                           
                                                       }];
    [dataTask resume];
    
    
}




//=================================================================================
//=================================================================================
//=================      Start EMAIL PAN CARD DEPT METHOD        ==================
//=================================================================================
//=================================================================================
//=================================================================================

-(void)PanDeptFileHandling:(NSString*)receipt withSub:(NSString*)subject withType:(NSString*)type withMailBody:(NSString*)mailBody withfile:(NSString*)fileURL withFormName:(NSString*)formName withCallback:(NSString*)callbackMethod

{
    //NSURL *URL = [NSURL URLWithString:@"http://www.axmag.com/download/pdfurl-guide.pdf"];
    NSURL *URL = [NSURL URLWithString:fileURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    NSURLSession *session = [NSURLSession sharedSession];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"Please wait...", @"Please wait...");
    
    NSURLSessionDownloadTask *downloadTask = [session downloadTaskWithRequest:request
                                                            completionHandler:
                                              ^(NSURL *location, NSURLResponse *response, NSError *error) {
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [hud hideAnimated:YES];
                                                  });
                                                  NSString *finalPath ;
                                                  if (error==nil)
                                                  {
                                                      
                                                      if ([response isKindOfClass:[NSHTTPURLResponse class]])
                                                      {
                                                          NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *)response;
                                                          NSString * contentType = [httpResponse.allHeaderFields valueForKey:@"Content-Type"];
                                                          
                                                          /*
                                                           Check any header here and make the descision.
                                                           */
                                                          NSString      *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
                                                          
                                                          finalPath = [documentsPath stringByAppendingPathComponent:formName];
                                                          NSFileManager *fileManager   = [NSFileManager defaultManager];
                                                          
                                                          NSError *fileManagerError;
                                                          if ([fileManager fileExistsAtPath:finalPath])
                                                          {
                                                              [fileManager removeItemAtPath:finalPath error:&fileManagerError];
                                                          }
                                                          
                                                          [fileManager moveItemAtURL:location toURL:[NSURL fileURLWithPath:finalPath] error:&fileManagerError];
                                                          
                                                          NSLog(@"finished %@", finalPath);
                                                          
                                                          
                                                          if ([type isEqualToString:@"download"])
                                                          {
                                                              
                                                              
                                                              [self getImageFromPDFWithPath:finalPath];
                                                              
                                                              //                                                              if (image != nil)
                                                              //                                                              {
                                                              //                                                                  UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
                                                              //
                                                              //                                                                  [RunOnMainThread runBlockInMainQueueIfNecessary:^{
                                                              //                                                                      [self.view makeToast:@"Saved in photo library" duration:5.0 position:CSToastPositionBottom];
                                                              //                                                                  }];
                                                              //
                                                              //
                                                              //                                                              }
                                                              
                                                          }
                                                          
                                                          else if ([type isEqualToString:@"open"])
                                                          {
                                                              [self openPanFile:receipt withSub:subject withType:type withMailBody:mailBody withfile:finalPath withFormName:formName];
                                                              
                                                              
                                                              if([contentType containsString:@"application/pdf"])
                                                              {
                                                                  
                                                                  
                                                                  
                                                                  
                                                              }
                                                              else
                                                              {
                                                                  
                                                              }
                                                          }
                                                          else
                                                          {
                                                              [self openPanFile:receipt withSub:subject withType:type withMailBody:mailBody withfile:finalPath withFormName:formName];
                                                          }
                                                          
                                                          
                                                          
                                                          
                                                          
                                                          
                                                          
                                                          
                                                          //success condition
                                                          //return T callback and value
                                                      }
                                                      
                                                      NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",callbackMethod,@"T"];
                                                      
                                                      [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
                                                  }
                                                  else
                                                  {
                                                      //failure condition
                                                      //return fail callback and value
                                                      
                                                      NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",callbackMethod,@"F"];
                                                      
                                                      [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
                                                  }
                                                  
                                              }];
    
    [downloadTask resume];
}

-(void)getImageFromPDFWithPath:(NSString *)path
{
    
    CFURLRef pdfURL = (__bridge CFURLRef)[NSURL fileURLWithPath:path];
    
    CGPDFDocumentRef pdf = CGPDFDocumentCreateWithURL((CFURLRef)pdfURL);
    
    size_t numberOfPages = CGPDFDocumentGetNumberOfPages(pdf);
    
    
    for (int currentPage = 1; currentPage <= numberOfPages; currentPage++)
    {
        CGPDFPageRef page = CGPDFDocumentGetPage(pdf, currentPage);
        
        CGRect cropBox = CGPDFPageGetBoxRect(page, kCGPDFCropBox);
        
        UIGraphicsBeginImageContext(CGSizeMake(cropBox.size.width , cropBox.size.height));
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        CGContextTranslateCTM(context, 0.0, cropBox.size.height);
        
        CGContextScaleCTM(context, 1.0, -1.0);
        
        
        //rotate = CGPDFPageGetRotationAngle(page);
        
        CGContextSaveGState(context);
        
        CGAffineTransform pdfTransform = CGPDFPageGetDrawingTransform(page, kCGPDFCropBox, cropBox, 0, true);
        
        CGContextConcatCTM(context, pdfTransform);
        
        CGContextDrawPDFPage(context, page);
        
        CGContextRestoreGState(context);
        
        UIImage *resultingImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        //return resultingImage;
        
        if (resultingImage != nil)
        {
            UIImageWriteToSavedPhotosAlbum(resultingImage, nil, nil, nil);
        }
    }
    
    [RunOnMainThread runBlockInMainQueueIfNecessary:^{
        [self.view makeToast:@"Saved in photo library" duration:5.0 position:CSToastPositionBottom];
    }];
}

//----------- END OF MORE INFO POP UP VIEW--------------
-(void)openFAQloadpdf
{
    
}
-(void)openPanFile:(NSString*)receipt withSub:(NSString*)subject withType:(NSString*)type withMailBody:(NSString*)mailBody withfile:(NSString*)fileURL withFormName:(NSString*)formName
{
    //  [dic_serviceInfo valueForKey:@"SERVICE_EMAIL"]
    flagViewAppear=FALSE;
    
    // if type is download then we have to preview it with save option
    if([type isEqualToString:@"open"])
    {
        
        //send to preview in faq
        
        __block NSString *encodedFileURL;
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            //Your code goes here
            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
            vc.titleOpen=formName;
            vc.urltoOpen=@"";
            vc.isfrom=@"WEBVIEWHANDLEPDFOPENLOCALPATH";
            vc.file_shareType = @"ShareYesPDF";
            vc.localPathString = fileURL;
            
            NSData *localPDFData = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:fileURL]];
            
            encodedFileURL = [fileURL stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            
            vc.file_name=encodedFileURL;
            vc.file_type=type;
            vc.pdfData = localPDFData;
            
            //[vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            UIViewController *topvc=[self topMostController];
            [topvc presentViewController:vc animated:NO completion:nil];
            
            
            
        });
        
    }
    else //case of email to download and compose it
    {
        
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        if ([MFMailComposeViewController canSendMail]) {
            
            
            NSArray *toRecipents = [NSArray arrayWithObject:receipt];
            
            picker.mailComposeDelegate = self;
            [picker setSubject:subject];
            [picker setMessageBody:mailBody isHTML:YES];
            [picker setToRecipients:toRecipents];
            
            
            // Determine the file name and extension
            NSArray *filepart = [formName componentsSeparatedByString:@"."];
            NSString *filename = [filepart objectAtIndex:0];
            NSString *extension = [filepart objectAtIndex:1];
            
            // Get the resource path and read the file using NSData
            //NSString *filePath = [[NSBundle mainBundle] pathForResource:filename ofType:extension];
            // NSString *filePath = [[NSBundle mainBundle] pathForResource:fileURL ofType:extension];
            NSData *fileData = [NSData dataWithContentsOfFile:fileURL];
            
            // Determine the MIME type
            NSString *mimeType;
            if ([extension isEqualToString:@"jpg"]) {
                mimeType = @"image/jpeg";
            } else if ([extension isEqualToString:@"png"]) {
                mimeType = @"image/png";
            } else if ([extension isEqualToString:@"doc"]) {
                mimeType = @"application/msword";
            } else if ([extension isEqualToString:@"ppt"]) {
                mimeType = @"application/vnd.ms-powerpoint";
            } else if ([extension isEqualToString:@"html"]) {
                mimeType = @"text/html";
            } else if ([extension isEqualToString:@"pdf"]) {
                mimeType = @"application/pdf";
            }
            
            // Add attachment
            [picker addAttachmentData:fileData mimeType:mimeType fileName:filename];
            
            // Present mail view controller on screen
            [self presentViewController:picker animated:YES completion:NULL];
            
        }
        // Present mail view controller on screen
        // [self presentViewController:picker animated:YES completion:NULL];
    }
    
    
}



//=================================================================================
//=================================================================================
//=================   END EMAIL PAN CARD DEPT METHOD        =======================
//=================================================================================
//=================================================================================
//=================================================================================


/*
 
 
 public void startDigiLockerDownloadDept() {
 //((DigiLockerWebViewScreen) mAct).startDownLoading(digiUrl, digiJson);
 
 ((WebActivity) mAct).startDownLoading(digiUrl, digiJson);
 }
 
 */



/*
 @JavascriptInterface
 public String getCurrentLocation()
 */
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    /*  UIAlertView *errorAlert = [[UIAlertView alloc]
     initWithTitle:@"Error"
     message:@"Failed to Get Your Location"
     delegate:nil
     cancelButtonTitle:@"OK"
     otherButtonTitles:nil];
     [errorAlert show];*/
    
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *newLocation = locations[[locations count] -1];
    CLLocation *currentLocation = newLocation;
    
    @try {
        // NSString *longitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        //NSString *latitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        
        if (currentLocation != nil) {
            //NSLog(@"latitude: %@", latitude);
            //NSLog(@"longitude:%@", longitude);
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    /*  else {
     UIAlertView *errorAlert = [[UIAlertView alloc]
     initWithTitle:@"Error" message:@"Failed to Get Your Location"
     delegate:nil
     cancelButtonTitle:@"OK"
     otherButtonTitles:nil];
     [errorAlert show];
     }*/
    
}



-(void)getCurrentLocation
{
    NSString* locationStr = @"";
    if (locationManager.location.coordinate.latitude == 0.0 && locationManager.location.coordinate.longitude == 0.0) {
        //                    gpsTracker.showSettingsAlert();
        locationStr = @"";
    } else
    {
        locationStr=[NSString stringWithFormat:@"%f,%f", locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude];
    }
    
    if ([locationStr isEqualToString:@""]) {
        // ((WebActivity) mAct).sendLocationCallBack("F", "", locationResponse);
        NSString *javascriptString = [NSString stringWithFormat:@"getCurrentLocationCallBack('%@','%@');", @"F",@""];
        // [self.webView stringByEvaluatingJavaScriptFromString:javascriptString];
        [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
        
        
    }
    else
    {
        //  ((WebActivity) mAct).sendLocationCallBack("S", locationStr, locationResponse);
        NSString *javascriptString = [NSString stringWithFormat:@"getCurrentLocationCallBack('%@','%@');", @"S",locationStr];
        //[self.webView stringByEvaluatingJavaScriptFromString:javascriptString];
        [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
        
        
    }
    
    
}


/*
 -  @JavascriptInterface
 public void fetchLocation(String response)
 */
-(void)fetchLocation:(NSString*)response
{
    //NSLog(@"fetchLocation");
    locationResponse = response;
    [self checkLocationPermission];
    
}

-(void)checkLocationPermission
{
    if([CLLocationManager locationServicesEnabled]){
        
        //NSLog(@"Location Services Enabled");
        
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:@"App Permission Denied"
                                                               message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                                              delegate:nil
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            [self startLocationTask];
        }
    }
    
    
}

-(void)startLocationTask
{
    [self getMyLocation];
}

-(void)getMyLocation
{
    NSString* locationStr = @"";
    if (locationManager.location.coordinate.latitude == 0.0 && locationManager.location.coordinate.longitude == 0.0) {
        //                    gpsTracker.showSettingsAlert();
        locationStr = @"";
    } else
    {
        locationStr=[NSString stringWithFormat:@"latitude: %f,longitude: %f", locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude];
    }
    
    if ([locationStr isEqualToString:@""]) {
        // ((WebActivity) mAct).sendLocationCallBack("F", "", locationResponse);
        NSString *javascriptString = [NSString stringWithFormat:@"sendLocationCallBack('%@','%@','%@');", @"F",@"", locationResponse];
        //[self.webView stringByEvaluatingJavaScriptFromString:javascriptString];
        [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
        
    }
    else
    {
        //  ((WebActivity) mAct).sendLocationCallBack("S", locationStr, locationResponse);
        NSString *javascriptString = [NSString stringWithFormat:@"sendLocationCallBack('%@','%@','%@');", @"S",locationStr, locationResponse];
        //[self.webView stringByEvaluatingJavaScriptFromString:javascriptString];
        [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
        
    }
    
}



//------------- Similiar method of android comman Interface----------



//------------- Similiar method of android location Interface----------

/* ------not getting it -------------
 
 @JavascriptInterface
 public void fetchLocation(String response) {
 //startLocationTask(success,failure);
 locationResponse = response;
 //        locationFailure = failure;
 checkLocationPermission();
 }
 
 */








//----------------- Camera interfacemethods--------

/*
 @JavascriptInterface
 public void openAppHome()
 */

-(void)openAppHome
{
    //NSLog(@"openAppHome");
    [self backBtnAction:self];
    
}


/*
 public void openPDF(String pdfPath)
 
 */



-(void)openPDF:(NSString*)pdfURL
{
    /* NSURL *targetURL = [NSURL URLWithString:pdfURL];
     NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
     [self.webView loadRequest:request];*/
    
    NSURL *targetURL = [NSURL URLWithString:pdfURL];
    if (targetURL) {
        // Initialize Document Interaction Controller
        self.documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:targetURL];
        // Configure Document Interaction Controller
        [self.documentInteractionController setDelegate:self];
        // Preview PDF
        [self.documentInteractionController presentPreviewAnimated:YES];
    }
    
    
}

- (UIViewController *) documentInteractionControllerViewControllerForPreview: (UIDocumentInteractionController *) controller {
    return self;
}
/*
 @JavascriptInterface
 public String getPageLoadingStatus()
 */


//-----  Get Page Loading Status true/false
-(void)getPageLoadingStatus
{
    //NSLog(@"getPageLoadingStatus =%@",loadingStatus);
    
    NSString *javascriptString = [NSString stringWithFormat:@"sendpageLoadingCallBack('%@');",loadingStatus];
    // [self.webView stringByEvaluatingJavaScriptFromString:javascriptString];
    [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
    
    //return pref.getPref(MyPreferences.PREF_WEBPAGE_LOADING_BOOLEAN, "true");
}

/*
 @JavascriptInterface
 public String getString()
 */
-(NSString*)getString
{
    return @"How are you?";
    
}


/*  not getting
 @JavascriptInterface
 public void openChooseFrom(String requestFor, String successCallBack,String failureCallback)
 
 */

-(void)openChooseFrom :(NSString*)requestFor withsuccessCallBack:(NSString*)successCallBack withfailureCallback:(NSString*)failureCallback
{
    //NSLog(@"openChooseFrom");
    flagViewAppear=FALSE;
    
    camera_failCallback=failureCallback;
    camera_successCallback=successCallBack;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:[NSLocalizedString(@"camera", nil) capitalizedString],[NSLocalizedString(@"gallery", nil) capitalizedString], nil];
    
    actionSheet.delegate=self;
    [actionSheet showInView:self.view];
    
    
    
    
}

-(void)openVideoGallary
{
    flagViewAppear=FALSE;
    
    UIImagePickerController *videoPicker = [[UIImagePickerController alloc] init];
    videoPicker.delegate = self;
    videoPicker.modalPresentationStyle = UIModalPresentationCurrentContext;
    videoPicker.mediaTypes =[UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    videoPicker.mediaTypes = @[(NSString*)kUTTypeMovie, (NSString*)kUTTypeAVIMovie, (NSString*)kUTTypeVideo, (NSString*)kUTTypeMPEG4];
    videoPicker.videoQuality = UIImagePickerControllerQualityTypeLow;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:videoPicker];
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [popover presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
        });
    }
    else
    {
        [self presentViewController:videoPicker animated:YES completion:nil];
    }
    
}


- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (popup.tag==999)
    {
        switch (buttonIndex) {
            case 0:
            {
                cameraTag=@"RecordVideo";
                
                [self openRecordingVideo];
            }
                break;
            case 1:
            {
                cameraTag=@"LoadVideo";
                
                [self openVideoGallary];
            }
                break;
                
            default:
                break;
        }
    }
    else
    {
        
        switch (buttonIndex)
        {
            case 0:
                [self openCamera];
                break;
            case 1:
                [self openGallary];
                break;
                
            default:
                break;
        }
    }
    
}




-(void)openCamera
{
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }
    else
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = [cameraTag isEqualToString:@"getEdistrictOriginalImage"] || [cameraTag isEqualToString:@"getDocument"] ? NO:  YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        if ([cameraTag isEqualToString:@"getEdistrictOriginalImage"] || [cameraTag isEqualToString:@"getDocument"])
        {
            [picker setMediaTypes: [NSArray arrayWithObject:kUTTypeImage]];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            
            
            
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
            {
                UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:picker];
                
                [popover presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
            }
            else
            {
                [self presentViewController:picker animated:NO completion:nil];
            }
            
        });
        flagViewAppear=FALSE;
        
    }
}


-(void)openGallary
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = [cameraTag isEqualToString:@"getEdistrictOriginalImage"] || [cameraTag isEqualToString:@"getDocument"] ? NO:  YES;
    
    
    
    
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    
    if ([cameraTag isEqualToString:@"getEdistrictOriginalImage"] || [cameraTag isEqualToString:@"getDocument"])
    {
        [picker setMediaTypes: [NSArray arrayWithObject:kUTTypeImage]];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
            
            UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:picker];
            
            [popover presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
        }
        else
        {
            [self presentViewController:picker animated:YES completion:nil];
        }
        
    });
    flagViewAppear=FALSE;
    
    
}




- (NSString *)encodeToBase64String:(UIImage *)image
{
    if([cameraTag isEqualToString:@"getEdistrictOriginalImage"])
    {
        return [UIImageJPEGRepresentation(image, 0.5) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    }
    return [UIImageJPEGRepresentation(image, 1.0) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    //return [UIImageJPEGRepresentation(image,1.0) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    // return [UIImageJPEGRepresentation(image, <#CGFloat compressionQuality#>)]
}




//------------- Similiar method of ContactInterface  Interface----------
/*
 @JavascriptInterface ------need detail
 public void getPhoneContacts(String success,String failue){
 
 */



//------------- Similiar method of download  Interface----------



/*
 @JavascriptInterface
 public void shareFile(String email)
 */
-(void)shareFile :(NSString*)email
{
    //((DigiLockerWebViewScreen) mAct).openEmailShareWithAttachment(email);
    
}


/*
 @JavascriptInterface
 public void startChapterDownLoad(String json, String response)
 
 */


//==========================================================
//                  EBOOK CODE
//==========================================================
-(void) StartDownloadingPress
{
    
    
    if (downloadFileno<[downloadBookArry count])
    {
        
        [vw_progressBar setProgress:0];
        @try {
            
            NSString *bookdownload=[NSString stringWithFormat:@"%@",[[downloadBookArry objectAtIndex:downloadFileno]valueForKey:@"chapterTitle"]];
            NSString *bookcurrentlydownload=[NSString stringWithFormat:@"%d/%lu%@",downloadFileno+1,(unsigned long)[downloadBookArry count],NSLocalizedString(@"download_completed", nil)];
            
            
            dispatch_after(0, dispatch_get_main_queue(), ^{
                
                [lbl_downloadbook setText:bookdownload];
                [lbl_downloadcomplete setText:bookcurrentlydownload];
                [lbl_downloadcomplete sizeToFit];
                
            });
            
            
            
            
            [lbl_downloadbook setNeedsDisplay];
            
            [lbl_downloadcomplete setNeedsDisplay];
            
            
            
            
            currentebookURL=[[downloadBookArry objectAtIndex:downloadFileno]valueForKey:@"epubLink"];
            
            NSLog(@"xxxxxxxxxxlbl_downloadbook=%@",lbl_downloadbook.text);
            NSLog(@" lbl_downloadcomplete.text=%@", lbl_downloadcomplete.text);
            NSLog(@"currentebookURL=%@", currentebookURL);
            
            
            
            NSString * downloadstartMsg
            = [NSString stringWithFormat:@"%@ %@",[[downloadBookArry objectAtIndex:downloadFileno] valueForKey:@"chapterTitle"],NSLocalizedString(@"download_started", nil)];
            
            
            [self showToast:downloadstartMsg];
            
            
            
            
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        NSURL *URL = [NSURL URLWithString:currentebookURL];
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:nil];
        // NSURLSessionDownloadTask *downloadTask = [session downloadTaskWithURL:URL];
        
        downtask = [session downloadTaskWithURL:URL];
        
        [downtask resume];
        
    }
    else if (downloadFileno==[downloadBookArry count])
    {
        [vw_progressBar setProgress:1];
        
        
        dispatch_after(0, dispatch_get_main_queue(), ^{
            
            [self closeBtn:self];
            
        });
        
        
        
    }
    else
    {
        [vw_progressBar setProgress:0];
        
    }
}


-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    NSLog(@"%@",[error localizedDescription]);
    
    [tbldonwload reloadData];
}
-(void) URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    dispatch_async(dispatch_get_main_queue(), ^{
        vw_progressBar.progress = (double)totalBytesWritten/(double)totalBytesExpectedToWrite;
        currentPercentvalue =(double)totalBytesWritten/(double)totalBytesExpectedToWrite;
        NSLog(@"currentPercentvalue====>%f",currentPercentvalue);
        
        @try {
            
            NSString *dwonloadbook=[NSString stringWithFormat:@"%@",[[downloadBookArry objectAtIndex:downloadFileno]valueForKey:@"chapterTitle"]];
            
            lbl_download.text=dwonloadbook;
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        //UITableViewCell *swipeCell  = [self.tableView cellForRowAtIndexPath:indexPath];
        
        
        
        NSIndexPath *myIP = [NSIndexPath indexPathForRow:downloadFileno inSection:0] ;
        
        //downloadBookTableViewCell *downloadcell = [tbldonwload dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        downloadBookTableViewCell *downloadcell = [tbldonwload cellForRowAtIndexPath:myIP];
        downloadcell.vw_downloadStatusBar.progress=(double)totalBytesWritten/(double)totalBytesExpectedToWrite;;
        
        
        downloadcell.lbl_bookDownloadPercent.text=[NSString stringWithFormat:@"%0.f%%",currentPercentvalue*100];
        
        downloadcell.vw_downloadStatusBar.transform = CGAffineTransformMakeScale(1.0, 3.0);
        
        
    });
}

-(void) URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes
{
    
}

/*
-(void) URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    
    @try {
        NSString *cId= [NSString stringWithFormat:@"%@",[[downloadBookArry objectAtIndex:downloadFileno] valueForKey:@"id"]];
        NSString * userId= [NSString stringWithFormat:@"%@",singleton.user_id];
        NSString * bookId= [NSString stringWithFormat:@"%@",[bookJson valueForKey:@"bookId"]];
        NSString * bookClass= [NSString stringWithFormat:@"%@",[bookJson valueForKey:@"bookCls"]];
        NSString * bookImage= [NSString stringWithFormat:@"%@",[bookJson valueForKey:@"bookImage"]];
        NSString * bookLang= [NSString stringWithFormat:@"%@",[bookJson valueForKey:@"bookLng"]];
        NSString * bookName= [NSString stringWithFormat:@"%@",[bookJson valueForKey:@"bookName"]];
        NSString * bookSub= [NSString stringWithFormat:@"%@",[bookJson valueForKey:@"bookSub"]];
        
        //"checked": true,
        // user_id  -> MDF folder then save it there
        NSString * cClassBook= [NSString stringWithFormat:@"%@",[[downloadBookArry objectAtIndex:downloadFileno] valueForKey:@"classBook"]];
        
        
        NSString * cEpubLink=[NSString stringWithFormat:@"%@",[[downloadBookArry objectAtIndex:downloadFileno] valueForKey:@"epubLink"]];
        
        
        
        NSString * cTitle=[NSString stringWithFormat:@"%@",[[downloadBookArry objectAtIndex:downloadFileno] valueForKey:@"chapterTitle"]];
        
        
        NSString * cNo= [NSString stringWithFormat:@"%@",[[downloadBookArry objectAtIndex:downloadFileno] valueForKey:@"chapterNo"]];
        
        
        NSString * cEnmLay=[NSString stringWithFormat:@"%@",[[downloadBookArry objectAtIndex:downloadFileno] valueForKey:@"enmLayout"]];
        
        
        
        NSString * cAllData= [NSString stringWithFormat:@"%@",[[downloadBookArry objectAtIndex:downloadFileno] valueForKey:@"allData"]];
        
        NSString * cEnmType=[NSString stringWithFormat:@"%@",[[downloadBookArry objectAtIndex:downloadFileno] valueForKey:@"enmType"]];
        
        
        NSString * cHashKey= [NSString stringWithFormat:@"%@",[[downloadBookArry objectAtIndex:downloadFileno] valueForKey:@"$$hashKey"]];
        
        
        
        
        
        [delegate.downloadComplete addObject:[downloadBookArry objectAtIndex:downloadFileno]];
        
        
        
        
        NSString *userDicPath= [NSString stringWithFormat:@"%@",[singleton.user_id MD5]];
        
        NSString *filePath=[self createDirectory:userDicPath];
        
        
        NSString* filenamewithBook=[NSString stringWithFormat:@"%@/%@.epub",filePath,cId];
        NSLog(@"filenamewithBook path=%@",filenamewithBook);
        
        BOOL success;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        NSError *fileManagerError;
        if ([fileManager fileExistsAtPath:filenamewithBook]) {
            success = [fileManager removeItemAtPath:filenamewithBook error:&fileManagerError];
            NSAssert(success, @"removeItemAtPath error: %@", fileManagerError);
        }
        
        success = [fileManager moveItemAtURL:location toURL:[NSURL fileURLWithPath:filenamewithBook] error:&fileManagerError];
        NSAssert(success, @"moveItemAtURL error: %@", fileManagerError);
        
        
        NSString * cPath= [NSString stringWithFormat:@"%@",filenamewithBook];
        
        
        
        [singleton.dbManager insertBooksData:userId bookId:bookId bookClass:bookClass bookImage:bookImage bookLang:bookLang bookName:bookName bookSub:bookSub cId:cId cClassBook:cClassBook cEpubLink:cEpubLink cTitle:cTitle cNo:cNo cEnmLay:cEnmLay cAllData:cAllData cEnmType:cEnmType cHashKey:cHashKey cPath:cPath];
        
        
        NSString * downloadEndMsg
        = [NSString stringWithFormat:@"%@ %@",[[downloadBookArry objectAtIndex:downloadFileno] valueForKey:@"chapterTitle"],NSLocalizedString(@"download_completed", nil)];
        [self showToast:downloadEndMsg];
        
        
        
        
        NSDictionary *idDic = [NSDictionary dictionaryWithObjectsAndKeys:
                               cId, @"id",nil];
        NSMutableArray *chapterArray=[[NSMutableArray alloc]init];
        [chapterArray addObject:idDic];
        NSDictionary *pdDic = [NSDictionary dictionaryWithObjectsAndKeys:
                               bookId, @"bookId",chapterArray, @"chapters",nil];
        NSDictionary *jsontoPass = [NSDictionary dictionaryWithObjectsAndKeys:
                                    @"S", @"status",pdDic, @"pd",nil];
        NSError *error;
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:jsontoPass options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        NSLog(@"jsonString==>%@",jsonString);
        
        jsonString=[self JSONString:jsonString];
        
        
        NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",callBackresponse,jsonString];
        
        [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
        
        
        
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
    
    downloadFileno++;
    delegate.downloadBookNo++;
    
    NSLog(@"Succeeded! Received %d bytes of data",downloadFileno);
    
    if (downloadFileno<=[downloadBookArry count]) {
        [self StartDownloadingPress];
        
        downloadCheck=TRUE;
        [self isDownloadingProgress];
        
    }
    [tbldonwload reloadData];
    
    
}

*/


-(void) URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    
    @try {
        NSString *cId= [NSString stringWithFormat:@"%@",[[downloadBookArry objectAtIndex:downloadFileno] valueForKey:@"id"]];
        NSString * userId= [NSString stringWithFormat:@"%@",singleton.user_id];
        NSString * bookId= [NSString stringWithFormat:@"%@",[bookJson valueForKey:@"bookId"]];
        NSString * bookClass= [NSString stringWithFormat:@"%@",[bookJson valueForKey:@"bookCls"]];
        NSString * bookImage= [NSString stringWithFormat:@"%@",[bookJson valueForKey:@"bookImage"]];
        NSString * bookLang= [NSString stringWithFormat:@"%@",[bookJson valueForKey:@"bookLng"]];
        NSString * bookName= [NSString stringWithFormat:@"%@",[bookJson valueForKey:@"bookName"]];
        NSString * bookSub= [NSString stringWithFormat:@"%@",[bookJson valueForKey:@"bookSub"]];
        
        
        NSString * cbookCategory= [NSString stringWithFormat:@"%@",[bookJson valueForKey:@"bookCategory"]];//new code added here
        
        if ([cbookCategory length]==0|| [cbookCategory isEqualToString:@"(null)"]) {
            cbookCategory =@"";
        }
        
        
        
        //===== New code added for epathsala category key
        NSString * Category= [NSString stringWithFormat:@"%@",[bookJson valueForKey:@"category"]];//new code added here
        
        if ([Category length]==0|| [Category isEqualToString:@"(null)"]) {
            Category =@"";
        }
        
        //===== end ====

        
        
        
        //"checked": true,
        // user_id  -> MDF folder then save it there
        NSString * cClassBook= [NSString stringWithFormat:@"%@",[[downloadBookArry objectAtIndex:downloadFileno] valueForKey:@"classBook"]];
        
        
        NSString * cEpubLink=[NSString stringWithFormat:@"%@",[[downloadBookArry objectAtIndex:downloadFileno] valueForKey:@"epubLink"]];
        
        
        
        NSString * cTitle=[NSString stringWithFormat:@"%@",[[downloadBookArry objectAtIndex:downloadFileno] valueForKey:@"chapterTitle"]];
        
        
        NSString * cNo= [NSString stringWithFormat:@"%@",[[downloadBookArry objectAtIndex:downloadFileno] valueForKey:@"chapterNo"]];
        
        
        NSString * cEnmLay=[NSString stringWithFormat:@"%@",[[downloadBookArry objectAtIndex:downloadFileno] valueForKey:@"enmLayout"]];
        
        
        
        NSString * cAllData= [NSString stringWithFormat:@"%@",[[downloadBookArry objectAtIndex:downloadFileno] valueForKey:@"allData"]];
        
        NSString * cEnmType=[NSString stringWithFormat:@"%@",[[downloadBookArry objectAtIndex:downloadFileno] valueForKey:@"enmType"]];
        
        
        NSString * cHashKey= [NSString stringWithFormat:@"%@",[[downloadBookArry objectAtIndex:downloadFileno] valueForKey:@"$$hashKey"]];
        
        
       
        
        [delegate.downloadComplete addObject:[downloadBookArry objectAtIndex:downloadFileno]];
        
        
        
        
        NSString *userDicPath= [NSString stringWithFormat:@"%@",[singleton.user_id MD5]];
        
        NSString *filePath=[self createDirectory:userDicPath];
        
        
        NSString* filenamewithBook=[NSString stringWithFormat:@"%@/%@.epub",filePath,cId];
        NSLog(@"filenamewithBook path=%@",filenamewithBook);
        
        BOOL success;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        NSError *fileManagerError;
        if ([fileManager fileExistsAtPath:filenamewithBook]) {
            success = [fileManager removeItemAtPath:filenamewithBook error:&fileManagerError];
            NSAssert(success, @"removeItemAtPath error: %@", fileManagerError);
        }
        
        success = [fileManager moveItemAtURL:location toURL:[NSURL fileURLWithPath:filenamewithBook] error:&fileManagerError];
        NSAssert(success, @"moveItemAtURL error: %@", fileManagerError);
        
        
        NSString * cPath= [NSString stringWithFormat:@"%@",filenamewithBook];
        
        
       /*
        [singleton.dbManager insertBooksData:userId bookId:bookId bookClass:bookClass bookImage:bookImage bookLang:bookLang bookName:bookName bookSub:bookSub cId:cId cClassBook:cClassBook cEpubLink:cEpubLink cTitle:cTitle cNo:cNo cEnmLay:cEnmLay cAllData:cAllData cEnmType:cEnmType cHashKey:cHashKey cPath:cPath   cbookCategory:cbookCategory];
        */
        //new category parameter added
        [singleton.dbManager insertBooksData:userId bookId:bookId bookClass:bookClass bookImage:bookImage bookLang:bookLang bookName:bookName bookSub:bookSub cId:cId cClassBook:cClassBook cEpubLink:cEpubLink cTitle:cTitle cNo:cNo cEnmLay:cEnmLay cAllData:cAllData cEnmType:cEnmType cHashKey:cHashKey cPath:cPath   cbookCategory:cbookCategory category:Category];
        
        
        
        NSString * downloadEndMsg
        = [NSString stringWithFormat:@"%@ %@",[[downloadBookArry objectAtIndex:downloadFileno] valueForKey:@"chapterTitle"],NSLocalizedString(@"download_completed", nil)];
        [self showToast:downloadEndMsg];
        
        
        
        
        NSDictionary *idDic = [NSDictionary dictionaryWithObjectsAndKeys:
                               cId, @"id",nil];
        NSMutableArray *chapterArray=[[NSMutableArray alloc]init];
        [chapterArray addObject:idDic];
        NSDictionary *pdDic = [NSDictionary dictionaryWithObjectsAndKeys:
                               bookId, @"bookId",chapterArray, @"chapters",nil];
        NSDictionary *jsontoPass = [NSDictionary dictionaryWithObjectsAndKeys:
                                    @"S", @"status",pdDic, @"pd",nil];
        NSError *error;
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:jsontoPass options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        NSLog(@"jsonString==>%@",jsonString);
        
        jsonString=[self JSONString:jsonString];
        
        
        NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",callBackresponse,jsonString];
        
        [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
        
        
        
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
    
    downloadFileno++;
    delegate.downloadBookNo++;
    
    NSLog(@"Succeeded! Received %d bytes of data",downloadFileno);
    
    if (downloadFileno<=[downloadBookArry count]) {
        [self StartDownloadingPress];
        
        downloadCheck=TRUE;
        [self isDownloadingProgress];
        
    }
    [tbldonwload reloadData];
    
    
}




-(void)isDownloadingProgress
{
    if (downloadCheck==TRUE) {
        vw_download.hidden=FALSE;
    }
    else
    {
        vw_download.hidden=TRUE;
        
    }
}

//-  Chapter Download
-(void)startChapterDownLoad :(NSDictionary*)json withresponse:(NSString*)callBack
{
    NSLog(@"callBackresponse=%@",callBack);
    NSLog(@"json=%@",json);
    
    bookJson=json;
    callBackresponse= [NSString stringWithFormat:@"%@", callBack];
    
    NSMutableArray *arr_bookChapter = (NSMutableArray *)[json valueForKey:@"pd"];
    
    downloadBookArry=[NSMutableArray new];//added
    
    
    NSLog(@"arr_bookChapter=%@",arr_bookChapter);
    
    for (int i=0; i<[arr_bookChapter count]; i++)
    {
        [downloadBookArry addObject:[arr_bookChapter objectAtIndex:i]];
    }
    
    NSLog(@"downloadBookArry=%@",downloadBookArry);
    
    
    
    
    downloadFileno=0;
    downloadCheck=TRUE;
    [self isDownloadingProgress];
    [self StartDownloadingPress];
    downloadCancel=[NSMutableArray new];
    [tbldonwload reloadData];
    
}






//==========================================================
//                  EBOOK CODE
//==========================================================

-(NSString*) createDirectory : (NSString *) dirName
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Fetch path for document directory
    NSString* filePath = (NSMutableString *)[documentsDirectory stringByAppendingPathComponent:dirName];
    
    NSError *error;
    
    if (![[NSFileManager defaultManager] createDirectoryAtPath:filePath withIntermediateDirectories:NO attributes:nil error:&error]) {
        NSLog(@"Couldn't create directory error: %@", error);
    }
    else {
        NSLog(@"directory created!");
    }
    
    NSLog(@"filePath : %@ ",filePath); // Path of folder created
    
    return filePath;
}


/*
 
 public void downloadChapter() {
 //        new DownloadBookTask(mAct, json).execute();
 startDownloadChapterService();
 }
 
 */


/*
 
 private void startDownloadChapterService(){
 
 
 
 
 
 
 
 try {
 Toast.makeText(mAct, mAct.getResources().getString(R.string.download_started), Toast.LENGTH_SHORT).show();
 JSONObject job = new JSONObject(json);
 JSONArray pd = job.getJSONArray("pd");
 
 for (int i = 0; i < pd.length(); i++) {
 
 Intent intent = new Intent(mAct, BookDownloadService.class);
 intent.putExtra("chapterJson", pd.getJSONObject(i).toString());
 intent.putExtra(Constants.BOOK_ID, job.getString(Constants.BOOK_ID));
 intent.putExtra(Constants.BOOK_CLASS, job.getString(Constants.BOOK_CLASS));
 intent.putExtra(Constants.BOOK_IMAGE, job.getString(Constants.BOOK_IMAGE));
 intent.putExtra(Constants.BOOK_LANG, job.getString(Constants.BOOK_LANG));
 intent.putExtra(Constants.BOOK_SUBJECT, job.getString(Constants.BOOK_SUBJECT));
 intent.putExtra(Constants.BOOK_NAME, job.getString(Constants.BOOK_NAME));
 mAct.startService(intent);
 
 //                BookBean bean = new BookBean();
 //                bean.setBookId(job.getString(Constants.BOOK_ID));
 //                bean.setBookClass(job.getString(Constants.BOOK_CLASS));
 //                bean.setBookImage(job.getString(Constants.BOOK_IMAGE));
 //                bean.setBookLang(job.getString(Constants.BOOK_LANG));
 //                bean.setBookName(job.getString(Constants.BOOK_NAME));
 //                bean.setBookSub(job.getString(Constants.BOOK_SUBJECT));
 //                bean.setcId(pd.getJSONObject(i).getString(Constants.CH_ID));
 //                bean.setcClassBook(pd.getJSONObject(i).getString(Constants.CH_CLASS_BOOK));
 //                bean.setcEpubLink(pd.getJSONObject(i).getString(Constants.CH_EPUB_LINK));
 //                bean.setcTitle(pd.getJSONObject(i).getString(Constants.CH_TITLE));
 //                bean.setcNo(pd.getJSONObject(i).getString(Constants.CH_NO));
 //                bean.setcEnmLay(pd.getJSONObject(i).getString(Constants.CH_ENM_LAYOUT));
 //                bean.setcAllData(pd.getJSONObject(i).getString(Constants.CH_ALL_DATA));
 //                bean.setcEnmType(pd.getJSONObject(i).getString(Constants.CH_ENM_TYPE));
 //                bean.setcHashKey(pd.getJSONObject(i).getString(Constants.CH_HASH_KEY));
 //                bean.setcPath("");
 //                bean.setDownloaded(false);
 //                bookAlist.add(bean);
 }
 
 } catch (Exception e) {
 e.printStackTrace();
 }
 
 
 
 }
 
 
 
 */

/*
 @JavascriptInterface
 public void openChapter(String chapterId)
 */
//-  Open Chapter
-(void)openChapter :(NSString*)chapterId
{
    //NSLog(@"openChapter");//pass userid aswell
    
    //            openChapter();
    
    NSArray *bookInfo=[singleton.dbManager getChapterDetailFromChapterId:singleton.user_id withchapterId:chapterId];
    
    NSLog(@"array=%@",bookInfo);
    
    if ([bookInfo count]>0) {
        NSString *chapterPath=[[bookInfo objectAtIndex:0] valueForKey:@"CHAPTER_PATH"];
        
        NSLog(@"%@ is a directory", chapterPath);
        NSLog(@"Is directory");
        NSLog(@"chapterPath=%@",chapterPath);
        flagViewAppear=FALSE;
        
        
        [self openDocument:chapterPath];
        
        /* EpubReaderViewController *_epubReaderViewController=[[EpubReaderViewController alloc] initWithNibName:@"EpubReaderViewController" bundle:nil];
         _epubReaderViewController._strFileName=chapterPath;
         
         _epubReaderViewController._bookName=[[bookInfo objectAtIndex:0] valueForKey:@"BOOK_NAME"];
         
         NSString *chaptertitle=[[bookInfo objectAtIndex:0] valueForKey:@"CHAPTER_TITLE"];
         chaptertitle = [chaptertitle stringByTrimmingCharactersInSet:
         [NSCharacterSet whitespaceCharacterSet]];
         _epubReaderViewController._chapterId=chaptertitle;
         
         [_epubReaderViewController setTitlename:chaptertitle];
         
         [self presentViewController:_epubReaderViewController animated:NO completion:nil];
         
         */
        
        
    }
}

- (void)openDocument:(NSString*)file
{
    
    //NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    
    // NSBundle *mainBundle = [NSBundle mainBundle];
    // NSString *storePath = [mainBundle pathForResource:file ofType: @"epub"];
    
    
    //  NSString *storePath = [applicationDocumentsDir stringByAppendingPathComponent:@"5775DA0DC9F6C9498F75D7C58AA8FB5A.epub"];
    //NSString *storePath = [applicationDocumentsDir stringByAppendingPathComponent:@"/pdf/daskleinebuch.pdf"];
    // NSURL *URL = [NSURL fileURLWithPath:storePath];
    
    NSURL *URL = [NSURL fileURLWithPath:file];
    
    //NSURL *URL = [[NSBundle mainBundle] URLForResource:@"sample" withExtension:@"pdf"];
    if (URL) {
        // Initialize Document Interaction Controller
        self.documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:URL];
        // Configure Document Interaction Controller
        [self.documentInteractionController setDelegate:self];
        // Present Open In Menu
        [self.documentInteractionController presentOpenInMenuFromRect:CGRectMake(0, fDeviceHeight-100, 100, 100) inView:self.view animated:YES];
        
        //[button frame]
    }
    
}





/*
 
 
 
 
 
 private boolean checkIfBookExistInStorage(Context context, String chapterId) {
 
 String userId = "";
 try {
 UserProfileHandler userProfileHandler = new UserProfileHandler(context);
 userId = userProfileHandler.getUserID();
 } catch (Exception e) {
 e.printStackTrace();
 userId = "";
 }
 if (userId == null) {
 userId = "";
 }
 
 File ebookFolder = new File(Environment.getExternalStorageDirectory(), "UMANG/eBooks/" + MyUtils.getMD5(userId));
 File[] dirFiles = ebookFolder.listFiles();
 
 boolean fileExist = false;
 
 if (dirFiles.length != 0) {
 for (int i = 0; i < dirFiles.length; i++) {
 String fileOutput = dirFiles[i].getName();
 Log.d(TAG, "files...................." + fileOutput);
 
 if (fileOutput.equalsIgnoreCase(chapterId + ".epub")) {
 fileExist = true;
 break;
 }
 
 }
 }
 
 return fileExist;
 
 }
 
 
 
 
 
 
 
 public void openChapter() {
 
 String userId = "";
 try {
 UserProfileHandler userProfileHandler = new UserProfileHandler(mAct);
 userId = userProfileHandler.getUserID();
 } catch (Exception e) {
 e.printStackTrace();
 userId = "";
 }
 if (userId == null) {
 userId = "";
 }
 
 DatabaseManager dbManager = new DatabaseManager(mAct);
 
 BookBean bookBean = dbManager.getChapterDetailFromChapterId(userId, chapterId);
 
 if (bookBean != null) {
 
 if (checkIfBookExistInStorage(mAct, chapterId)) {
 Log.d(TAG, "book exist in storage.............................");
 try {
 final Book recent = new Book(
 Long.parseLong(bookBean.getcId()),
 bookBean.getcPath(),
 bookBean.getBookName(),
 ZLFileImage.ENCODING_NONE,
 ZLFileImage.ENCODING_NONE);
 
 String layoutType = bookBean.getcEnmLay();
 Log.d(TAG, "layoutType...................."+layoutType);
 if (layoutType.equalsIgnoreCase("CONTENT") || layoutType.equalsIgnoreCase("C")) {
 FBReader.openBookActivity(this.mAct, recent, null, bookBean.getcTitle());
 } else {
 Intent openZipBook = new Intent(((WebActivity) mAct), SiegmannEpubActivity.class);
 //openZipBook.putExtra("BOOKJSON",bookJSON);
 openZipBook.putExtra("BOOK_TITLE", bookBean.getcTitle());
 openZipBook.putExtra(Constants.INTENT_BOOKPATH, bookBean.getcPath());
 ((WebActivity) mAct).startActivity(openZipBook);
 
 }
 } catch (Exception e) {
 e.printStackTrace();
 }
 } else {
 Log.d(TAG, "book is not in storage.............................");
 dbManager.deleteChapter(userId, chapterId);
 }
 
 
 } else {
 Toast.makeText(mAct, mAct.getResources().getString(R.string.book_not_found), Toast.LENGTH_SHORT).show();
 }
 
 
 }
 
 
 
 
 
 
 */



/*
 
 @JavascriptInterface
 public String getChaptersFromBookId(String bookId)
 */
//-  Get Chapter from book Id saved in local database
-(void)getChaptersFromBookId :(NSString*)bookId
{
    //NSLog(@"getChaptersFromBookId");
    
    NSArray *array=[singleton.dbManager getChaptersIdFromBookId:singleton.user_id withbookId:bookId];
    //NSLog(@"array=%@",array);
    
    
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    for(int i=0;i<[array count];i++)
    {
        NSMutableDictionary *dic =[NSMutableDictionary new];
        
        NSString *chptrid=[[array objectAtIndex:i]valueForKey:@"CHAPTER_ID"];
        [dic setObject:chptrid forKey:@"id"];
        [arr addObject:dic];
    }
    
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arr options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSLog(@"jsonData as string:\n%@", jsonString);
    
    jsonString=[self JSONString:jsonString];
    
    
    NSString *javascriptString = [NSString stringWithFormat:@"getChaptersFromBookIdIosCallBack('%@');",jsonString];
    [self.webView stringByEvaluatingJavaScriptFromString:javascriptString];
    // [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
    
    
    /*
     DatabaseManager dbManager = DatabaseManager.getDBAdapterInstance(mAct);
     
     String userId = "";
     
     try {
     UserProfileHandler userProfileHandler = new UserProfileHandler(mAct);
     userId = userProfileHandler.getUserID();
     } catch (Exception e) {
     e.printStackTrace();
     userId = "";
     }
     if (userId == null) {
     userId = "";
     }
     
     ArrayList<String> dataAlist = new ArrayList<>();
     dataAlist = dbManager.getChaptersIdFromBookId(userId, bookId);
     
     JSONArray jarr = new JSONArray();
     try {
     for (int i = 0; i < dataAlist.size(); i++) {
     JSONObject job = new JSONObject();
     job.put("id", dataAlist.get(i));
     jarr.put(job);
     }
     } catch (Exception e) {
     e.printStackTrace();
     }
     Log.d(TAG, "getChaptersFromBookId........................" + jarr.toString());
     
     return jarr.toString();
     */
}


//-  Get All books
/*
 ios::getAllBooksData::ncertEBook
 ios::getAllBooksData::ncertCollection
 ios::getAllBooksData::ncertCategory
 ios::getAllBooksData::ncertBookDetails
 
 
 {
 "bookCls": "Class 1",
 "bookId": "5200",
 "bookImage": "http://www.ncert.nic.in/tbEPUB/epub/aeen1cc.jpg",
 "bookLng": "English",
 "bookName": "Marigold",
 "bookSub": "English"
 }
 
 
 
 DatabaseManager dbManager = new DatabaseManager(mAct);
 
 String userId = "";
 
 try {
 UserProfileHandler userProfileHandler = new UserProfileHandler(mAct);
 userId = userProfileHandler.getUserID();
 } catch (Exception e) {
 e.printStackTrace();
 userId = "";
 }
 if (userId == null) {
 userId = "";
 }
 
 ArrayList<BookBean> dataAlist = new ArrayList<>();
 dataAlist = dbManager.getUniqueBooksData(userId);
 
 
 JSONArray jarr = new JSONArray();
 try {
 for (int i = 0; i < dataAlist.size(); i++) {
 JSONObject job = new JSONObject();
 job.put(Constants.BOOK_CLASS, dataAlist.get(i).getBookClass());
 job.put(Constants.BOOK_ID, dataAlist.get(i).getBookId());
 job.put(Constants.BOOK_IMAGE, dataAlist.get(i).getBookImage());
 job.put(Constants.BOOK_LANG, dataAlist.get(i).getBookLang());
 job.put(Constants.BOOK_NAME, dataAlist.get(i).getBookName());
 job.put(Constants.BOOK_SUBJECT, dataAlist.get(i).getBookSub());
 jarr.put(job);
 }
 } catch (Exception e) {
 e.printStackTrace();
 }
 
 Log.d(TAG, "getAllBooksData......................" + jarr.toString());
 return jarr.toString();
 
 
 
 
 */


/*
-(void)getAllBooksData:(NSString*)sourceFrom
{
    //NSLog(@"getChaptersFromBookId");
    
 
    
    
    
    NSMutableArray *array=[[NSMutableArray alloc]init];
    
    NSArray *bookInfo=[singleton.dbManager getAllBooksData:singleton.user_id];
    for(int i=0;i<[bookInfo count];i++)
    {
        @try {
            
 
            
            
            NSString *bookCls=[[bookInfo objectAtIndex:i] valueForKey:@"BOOK_CLASS"];
            NSString *bookId=[[bookInfo objectAtIndex:i] valueForKey:@"BOOK_ID"];
            NSString *bookImage=[[bookInfo objectAtIndex:i] valueForKey:@"BOOK_IMAGE"];
            NSString *bookLng=[[bookInfo objectAtIndex:i] valueForKey:@"BOOK_LANGUAGE"];
            NSString *bookName=[[bookInfo objectAtIndex:i] valueForKey:@"BOOK_NAME"];
            NSString *bookSub=[[bookInfo objectAtIndex:i] valueForKey:@"BOOK_SUBJECT"];
            
            NSMutableDictionary *dicBody=[NSMutableDictionary new];
            [dicBody setObject:bookCls forKey:@"bookCls"];
            [dicBody setObject:bookId forKey:@"bookId"];
            [dicBody setObject:bookImage forKey:@"bookImage"];
            [dicBody setObject:bookLng forKey:@"bookLng"];
            [dicBody setObject:bookName forKey:@"bookName"];
            [dicBody setObject:bookSub forKey:@"bookSub"];
            
            [array addObject:dicBody]; //body
            
            
            
        } @catch (NSException *exception)
        {
            
        } @finally
        {
            
        }
        
        
        
    }
    
    
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //NSLog(@"jsonData as string:\n%@", jsonString);
    
    jsonString=[self JSONString:jsonString];
    NSLog(@"jsonString=%@",jsonString);
    
    NSString *javascriptString = [NSString stringWithFormat:@"iosreturnAllBooksData('%@','%@');", jsonString,sourceFrom];
    
    [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
    
    
    //[self.webView stringByEvaluatingJavaScriptFromString:javascriptString];
    //
    //NSLog(@"bookInfo=%@",bookInfo);
    // NSString *encodedString=[self JSONString:jsonStr];
    

    
}
 */



//-  Get All books
-(void)getAllBooksData:(NSString*)sourceFrom
{
    
    
    NSMutableArray *array=[[NSMutableArray alloc]init];
    
    NSArray *bookInfo=[singleton.dbManager getAllBooksData:singleton.user_id];
    for(int i=0;i<[bookInfo count];i++)
    {
        @try {
            
            
            NSString *bookCls=[[bookInfo objectAtIndex:i] valueForKey:@"BOOK_CLASS"];
            NSString *bookId=[[bookInfo objectAtIndex:i] valueForKey:@"BOOK_ID"];
            NSString *bookImage=[[bookInfo objectAtIndex:i] valueForKey:@"BOOK_IMAGE"];
            NSString *bookLng=[[bookInfo objectAtIndex:i] valueForKey:@"BOOK_LANGUAGE"];
            NSString *bookName=[[bookInfo objectAtIndex:i] valueForKey:@"BOOK_NAME"];
            NSString *bookSub=[[bookInfo objectAtIndex:i] valueForKey:@"BOOK_SUBJECT"];
            
            NSString * bookCategory =[[bookInfo objectAtIndex:i] valueForKey:@"BOOK_CATEGORY"]; //new line added
            
            
            if ([bookCategory length]==0 ||[bookCategory isEqualToString:@"(null)"])  //new line added
            {
                bookCategory =@"";
            }
            
            // Add category in epathsala db
            NSString * Category =[[bookInfo objectAtIndex:i] valueForKey:@"CATEGORY"]; //new line added
            
            if ([Category length]==0 ||[Category isEqualToString:@"(null)"])  //new line added
            {
                Category =@"";
            }
            
            
            
            
            NSMutableDictionary *dicBody=[NSMutableDictionary new];
            [dicBody setObject:bookCls forKey:@"bookCls"];
            [dicBody setObject:bookId forKey:@"bookId"];
            [dicBody setObject:bookImage forKey:@"bookImage"];
            [dicBody setObject:bookLng forKey:@"bookLng"];
            [dicBody setObject:bookName forKey:@"bookName"];
            [dicBody setObject:bookSub forKey:@"bookSub"];
            [dicBody setObject:bookCategory forKey:@"bookCategory"];//new line added
            //add category in epathsala
            [dicBody setObject:Category forKey:@"category"];//new line added

             [array addObject:dicBody]; //body
             
             
             
             } @catch (NSException *exception)
             {
                 
             } @finally
             {
                 
             }
             
             
             
             }
             
             
             
             NSError *error;
             NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:&error];
             NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
             //NSLog(@"jsonData as string:\n%@", jsonString);
             
             jsonString=[self JSONString:jsonString];
             NSLog(@"jsonString=%@",jsonString);
             
             NSString *javascriptString = [NSString stringWithFormat:@"iosreturnAllBooksData('%@','%@');", jsonString,sourceFrom];
             
             [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
             
             
             
             
             }
             

             
             
             
//-  Get Chapter Data From book ID
-(void)getChapterDataFromBookId:(NSString*)bookId
{
    //NSLog(@"getChapterDataFromBookId");
    
    
    NSArray *bookInfo=[singleton.dbManager getChaptersDataFromBookId:singleton.user_id withBookId:bookId];
    
    
    
    
    
    NSMutableArray *array=[[NSMutableArray alloc]init];
    
    //  NSArray *bookInfo=[singleton.dbManager getAllBooksData:singleton.user_id];
    for(int i=0;i<[bookInfo count];i++)
    {
        @try {
            
            
            
            // BOOK_LANGUAGE
            // CHAPTER_ID
            // CHAPTER_CLASS_BOOK
            
            NSString *idtype=[[bookInfo objectAtIndex:i]valueForKey:@"CHAPTER_ID"];//????
            
            NSString *classBook=[[bookInfo objectAtIndex:i]valueForKey:@"BOOK_CLASS"];
            
            NSString *epubLink=[[bookInfo objectAtIndex:i]valueForKey:@"CHAPTER_EPUB_LINK"];
            
            NSString *chapterTitle=[[bookInfo objectAtIndex:i]valueForKey:@"CHAPTER_TITLE"];
            
            NSString *chapterNo=[[bookInfo objectAtIndex:i]valueForKey:@"CHAPTER_NO"];
            
            NSString *enmLayout=[[bookInfo objectAtIndex:i]valueForKey:@"CHAPTER_ENM_LAYOUT"];
            
            NSString *allData=[[bookInfo objectAtIndex:i]valueForKey:@"CHAPTER_ALL_DATA"];
            
            NSString *enmType=[[bookInfo objectAtIndex:i]valueForKey:@"CHAPTER_ENM_TYPE"];
            
            NSString *$$hashKey=[[bookInfo objectAtIndex:i]valueForKey:@"CHAPTER_HASH_KEY"];
            
            NSString *bookName=[[bookInfo objectAtIndex:i]valueForKey:@"BOOK_NAME"];
            
            NSString *bookImage=[[bookInfo objectAtIndex:i]valueForKey:@"BOOK_IMAGE"];
            
            NSString *bookSub=[[bookInfo objectAtIndex:i]valueForKey:@"BOOK_SUBJECT"];
            
            NSString *path=[[bookInfo objectAtIndex:i]valueForKey:@"CHAPTER_PATH"];
            NSString *isDownloaded=@"false";
            
            
            
            
            
            
            NSMutableDictionary *dicBody=[NSMutableDictionary new];
            [dicBody setObject:idtype forKey:@"id"];
            [dicBody setObject:classBook forKey:@"classBook"];
            [dicBody setObject:epubLink forKey:@"epubLink"];
            [dicBody setObject:chapterTitle forKey:@"chapterTitle"];
            [dicBody setObject:chapterNo forKey:@"chapterNo"];
            [dicBody setObject:enmLayout forKey:@"enmLayout"];
            [dicBody setObject:allData forKey:@"allData"];
            [dicBody setObject:enmType forKey:@"enmType"];
            [dicBody setObject:$$hashKey forKey:@"$$hashKey"];
            [dicBody setObject:bookName forKey:@"bookName"];
            [dicBody setObject:bookImage forKey:@"bookImage"];
            [dicBody setObject:bookSub forKey:@"bookSub"];
            [dicBody setObject:path forKey:@"path"];
            [dicBody setObject:isDownloaded forKey:@"isDownloaded"];
            
            [array addObject:dicBody]; //body
            
            
        }
        @catch (NSException *exception)
        {
            
        } @finally
        {
            
        }
        
        
        
    }
    
    
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //NSLog(@"jsonData as string:\n%@", jsonString);
    
    jsonString=[self JSONString:jsonString];
    NSLog(@"jsonString=%@",jsonString);
    NSString *javascriptString = [NSString stringWithFormat:@"getChapterDataFromBookIdIosCallback('%@');", jsonString];
    [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:YES];
    
    //[self.webView stringByEvaluatingJavaScriptFromString:javascriptString];
    
    //NSLog(@"chapterDataFromBookId=%@",chapterDataFromBookId);
    
    /*
     Log.d(TAG, "bookId........................" + bookId);
     DatabaseManager dbManager = new DatabaseManager(mAct);
     
     String userId = "";
     
     try {
     UserProfileHandler userProfileHandler = new UserProfileHandler(mAct);
     userId = userProfileHandler.getUserID();
     } catch (Exception e) {
     e.printStackTrace();
     userId = "";
     }
     if (userId == null) {
     userId = "";
     }
     
     ArrayList<BookBean> dataAlist = new ArrayList<>();
     dataAlist = dbManager.getChaptersDataFromBookId(userId, bookId);
     
     JSONArray jarr = new JSONArray();
     try {
     for (int i = 0; i < dataAlist.size(); i++) {
     JSONObject job = new JSONObject();
     job.put(Constants.CH_ID, dataAlist.get(i).getcId());
     job.put(Constants.CH_CLASS_BOOK, dataAlist.get(i).getcClassBook());
     job.put(Constants.CH_EPUB_LINK, dataAlist.get(i).getcEpubLink());
     job.put(Constants.CH_TITLE, dataAlist.get(i).getcTitle());
     job.put(Constants.CH_NO, dataAlist.get(i).getcNo());
     job.put(Constants.CH_ENM_LAYOUT, dataAlist.get(i).getcEnmLay());
     job.put(Constants.CH_ALL_DATA, dataAlist.get(i).getcAllData());
     job.put(Constants.CH_ENM_TYPE, dataAlist.get(i).getcEnmType());
     job.put(Constants.CH_HASH_KEY, dataAlist.get(i).getcHashKey());
     job.put(Constants.BOOK_NAME, dataAlist.get(i).getBookName());
     job.put(Constants.BOOK_IMAGE, dataAlist.get(i).getBookImage());
     job.put(Constants.BOOK_SUBJECT, dataAlist.get(i).getBookSub());
     job.put(Constants.CH_PATH, dataAlist.get(i).getcPath());
     job.put(Constants.CH_IS_DOWNLOADED, "" + dataAlist.get(i).isDownloaded());
     jarr.put(job);
     }
     } catch (Exception e) {
     e.printStackTrace();
     }
     
     Log.d(TAG, "getChapterDataFromBookId.............................." + jarr.toString());
     
     return jarr.toString();
     
     
     
     */
}


//-  Get Chapter Data From book ID
-(void)deleteChapter:(NSString*)chapterId withresponseCallback:(NSString*)responseCallback
{
    //NSLog(@"deleteChapter");
    
    NSString *delStatus=[singleton.dbManager deleteChapter:singleton.user_id  withChapterid:chapterId];
    NSString *javascriptString = [NSString stringWithFormat:@"%@('%@');",responseCallback,delStatus];
    [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
    /*
     
     Log.d(TAG, "chapterId............................" + chapterId);
     
     deleteChapterId = chapterId;
     chapterDeleteCallback = responseCallback;
     
     if (ContextCompat.checkSelfPermission(mAct,
     Manifest.permission.WRITE_EXTERNAL_STORAGE)
     != PackageManager.PERMISSION_GRANTED) {
     
     if (ActivityCompat.shouldShowRequestPermissionRationale(mAct,
     Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
     
     ((WebActivity) mAct).sendChapterDeleteCallback("FAIL", chapterDeleteCallback);
     //                    Toast.makeText(this, "Please allow the permission so that app can work properly", Toast.LENGTH_LONG).show();
     MyUtils.openPermissionSettingsDialog(mAct, mAct.getResources().getString(R.string.allow_write_storage_permission_for_delete_help_text));
     
     } else {
     ActivityCompat.requestPermissions(mAct,
     new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
     Constants.MY_PERMISSIONS_WRITE_EXTERNAL_STORATE_FOR_DELETE);
     }
     } else {
     
     deleteFile();
     
     }
     
     
     */
    
}

/*
 
 
 
 public void deleteFile() {
 
 String userId = "";
 
 try {
 UserProfileHandler userProfileHandler = new UserProfileHandler(mAct);
 userId = userProfileHandler.getUserID();
 } catch (Exception e) {
 e.printStackTrace();
 userId = "";
 }
 if (userId == null) {
 userId = "";
 }
 
 
 File ebookFolder = new File(Environment.getExternalStorageDirectory(), "UMANG/eBooks/" + MyUtils.getMD5(userId));
 File[] dirFiles = ebookFolder.listFiles();
 
 boolean fileExist = false;
 File f = null;
 if (dirFiles.length != 0) {
 for (int i = 0; i < dirFiles.length; i++) {
 String fileOutput = dirFiles[i].getName();
 Log.d(TAG, "files...................." + fileOutput);
 
 if (fileOutput.equalsIgnoreCase(deleteChapterId + ".epub")) {
 Log.d(TAG, "files exit...................." + fileOutput);
 fileExist = true;
 f = dirFiles[i];
 break;
 }
 
 }
 }
 Log.d(TAG, "fileExist.............." + fileExist);
 
 DatabaseManager dbManager = DatabaseManager.getDBAdapterInstance(mAct);
 dbManager.deleteChapter(userId, deleteChapterId);
 
 if (f != null) {
 if (fileExist) {
 f.delete();
 ((WebActivity) mAct).sendChapterDeleteCallback("SUCCESS", chapterDeleteCallback);
 } else {
 ((WebActivity) mAct).sendChapterDeleteCallback("FAIL", chapterDeleteCallback);
 }
 } else {
 Log.d(TAG, "file null..............");
 ((WebActivity) mAct).sendChapterDeleteCallback("FAIL", chapterDeleteCallback);
 }
 
 
 }
 
 
 
 */

/*
 
 @JavascriptInterface
 public void openPDF(String pdfPath) {
 //File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/example.pdf");
 File file = new File(pdfPath);
 Intent intent = new Intent(Intent.ACTION_VIEW);
 intent.setDataAndType(Uri.fromFile(file), "application/pdf");
 intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
 mAct.startActivity(intent);
 }
 
 
 
 */





//--------------------------------------------------------------
//-----------------END JAVASCRIPT METHODS-----------------------
//--------------------------------------------------------------




- (void)updateButtons
{
    self.web_back_btn.enabled = self.webView.canGoBack;
    
    
    NSURL * currentURL = _webView.request.URL.absoluteURL;
    
    NSLog(@"currentURL = %@",currentURL);
    
    
    /*
     NSLog(@"mainDocumentURL = %@",mainDocumentURL);
     
     
     if ([mainDocumentURL hasSuffix:@"#/"])
     {
     
     
     }
     */
    
    if ([self.webView canGoBack])
    {
        if(flag_homebtn==TRUE)
        {
            
            [_backBtnHome setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
            //flag_homebtn=FALSE;
        }
        else
        {
            
            
            [_backBtnHome setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
            NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
            if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
            {
                
                CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                               @{NSFontAttributeName: [UIFont systemFontOfSize:_backBtnHome.titleLabel.font.pointSize]}];
                
                CGRect framebtn=CGRectMake(_backBtnHome.frame.origin.x, _backBtnHome.frame.origin.y, _backBtnHome.frame.size.width, _backBtnHome.frame.size.height);
                
                [_backBtnHome setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
                _backBtnHome.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
                
            }
            
        }
        
    }
    else
    {
        [_backBtnHome setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
        
        
        
    }
    
    
    
}



//-------- WEB CACHE CODE ENDS------------




-(NSMutableDictionary*)getCommonParametersForRequestBody
{
    
    SharedManager *objShared = [SharedManager sharedSingleton];
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    
    //    // Mobile Number
    //    [dict setObject:@"8000000000" forKey:@"mno"];
    // IMEI
    //[dict setObject:[objShared appUniqueID] forKey:@"imei"];
    [dict setObject:@"" forKey:@"imei"];
    
    // Device ID
    
    
    [dict setObject:[objShared appUniqueID] forKey:@"did"];
    
    // IMSI
    // [dict setObject:[objShared appUniqueID] forKey:@"imsi"];
    [dict setObject:@"" forKey:@"imsi"];
    
    // Handset Make
    [dict setObject:@"Apple" forKey:@"hmk"];
    
    // Handset Model
    [dict setObject:[[UIDevice currentDevice] model] forKey:@"hmd"];
    
    
    //TODO:// HARD CODED Bundle Idnetifier. It should be dynaimc
    
    
    
    
    // Rooted
    [dict setObject:[self isJailbroken]?@"yes":@"no" forKey:@"rot"];
    
    // OS
    [dict setObject:@"ios" forKey:@"os"];
    
    // ver
    [dict setObject:[SharedManager appVersion] forKey:@"ver"];
    
    // Country Code
    [dict setObject:@"404" forKey:@"mcc"];
    
    // Mobile Network Code
    [dict setObject:@"02" forKey:@"mnc"];
    
    // Lcoation Area Code
    [dict setObject:@"2065" forKey:@"lac"];
    
    // Cell ID
    [dict setObject:@"11413" forKey:@"clid"];
    
    
    // Location Based Params
    // Accuracy
    [dict setObject:@"" forKey:@"acc"];
    
    // Latitude Code
    [dict setObject:@"" forKey:@"lat"];
    
    // Longitude Code
    [dict setObject:@"" forKey:@"lon"];
    
    // State Name
    //[dict setObject:@"" forKey:@"st"];
    
    
    // Selected Language Name
    /*  NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
     if (selectedLanguage == nil) {
     selectedLanguage = @"en";
     }
     
     selectedLanguage = @"en";
     
     [dict setObject:selectedLanguage forKey:@"lang"];*/
    
    
    NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if (selectedLanguage == nil)
    {
        selectedLanguage = @"en";
    }
    else{
        NSArray *arrTemp = [selectedLanguage componentsSeparatedByString:@"-"];
        selectedLanguage = [arrTemp firstObject];
    }
    
    bool checklang=[singleton.dbManager getServiceLanguage:service_ID withDeviceLang:selectedLanguage];
    
    NSString *language=@"";
    if (checklang==true)
    {
        language=selectedLanguage; //add condition for it
        
    }
    else
    {
        language=@"en";
    }
    
    
    //NSLog(@"Applied Selected Language = %@",selectedLanguage);
    
    [dict setObject:language forKey:@"lang"];
    
    //[dict setObject:@"en" forKey:@"lang"];
    NSString *node = [[NSUserDefaults standardUserDefaults] objectForKey:@"NODE_KEY"];
    
    if (node == nil)
    {
        node =@"";
    }
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [dict setObject:node forKey:@"node"];
    //-------- Add Sharding logic here-------
    // Selected Mode (Domain(Web / App/Mobile Web)
    [dict setObject:@"app" forKey:@"mod"];
    
    
    return dict;
}


/*-(BOOL)isJailbroken {
 NSURL* url = [NSURL URLWithString:@"cydia://package/com.example.package"];
 return [[UIApplication sharedApplication] canOpenURL:url];
 }
 */

- (BOOL)isJailbroken
{
    BOOL jailbroken = NO;
    NSArray *jailbrokenPath = [NSArray arrayWithObjects:@"/Applications/Cydia.app",  @"/Applications/RockApp.app",  @"/Applications/Icy.app",  @"/usr/sbin/sshd",  @"/usr/bin/sshd",  @"/usr/libexec/sftp-server",  @"/Applications/WinterBoard.app",  @"/Applications/SBSettings.app",  @"/Applications/MxTube.app",  @"/Applications/IntelliScreen.app",  @"/Library/MobileSubstrate/DynamicLibraries/Veency.plist",  @"/Applications/FakeCarrier.app",  @"/Library/MobileSubstrate/DynamicLibraries/LiveClock.plist",  @"/private/var/lib/apt",  @"/Applications/blackra1n.app",  @"/private/var/stash",  @"/private/var/mobile/Library/SBSettings/Themes",  @"/System/Library/LaunchDaemons/com.ikey.bbot.plist",  @"/System/Library/LaunchDaemons/com.saurik.Cydia.Startup.plist",  @"/private/var/tmp/cydia.log",  @"/private/var/lib/cydia", nil];for(NSString *string in jailbrokenPath)
    {
        if ([[NSFileManager defaultManager] fileExistsAtPath:string]){
            jailbroken = YES;
            break;}
    }
    return jailbroken;
}




- (UIViewController*)topmostViewController
{
    UIViewController* vc = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    while(vc.presentedViewController) {
        vc = vc.presentedViewController;
    }
    return vc;
}


- (void)returnToRootViewController
{
    
    
    UIViewController *vc = [self topmostViewController];
    while (vc)
    {
        /*  if([vc isKindOfClass:[UINavigationController class]]) {
         [(UINavigationController*)vc popToRootViewControllerAnimated:NO];
         }*/
        if(vc.presentingViewController) {
            if ([vc isKindOfClass:[HomeDetailVC class]])
            {
                //flagBack=TRUE;
                
                @try
                {
                    
                    flagBack=TRUE;
                    
                    dispatch_after(0, dispatch_get_main_queue(), ^{
                        HomeDetailVC *homeVC =(HomeDetailVC*) [self vcWithClass:[HomeDetailVC self]];
                        if (homeVC!=nil)
                        {
                            UIViewController *presented = homeVC.presentedViewController;
                            [presented dismissViewControllerAnimated:NO completion:^{}];
                            
                        }
                        
                        
                    });
                    
                    
                } @catch (NSException *exception) {
                    
                    NSLog(@"exception=%@",exception);
                } @finally {
                    
                }
                
                
            }
            else
            {
                @try {
                    if (vc!=nil)
                    {
                        [self dismissViewControllerAnimated:NO completion:nil];
                        
                    }
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                
            }
            
        }
        vc = vc.presentingViewController;
    }
    
    
}

- (UIViewController*)vcWithClass:(Class)klass {
    UIViewController* vc = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    while(![vc.presentedViewController isKindOfClass:klass]) {
        vc = vc.presentedViewController;
    }
    return vc;
}
- (void)returnToCarVC {
    HomeDetailVC *carVC =(HomeDetailVC*) [self vcWithClass:[HomeDetailVC self]];
    UIViewController *presented = carVC.presentedViewController;
    [presented dismissViewControllerAnimated:NO completion:^{}];
}

-(IBAction)backBtnAction:(id)sender
{
    //NSString *currentURL = _webView.request.URL.absoluteString;
    
    
    if ([_backBtnHome.titleLabel.text isEqualToString:NSLocalizedString(@"back", nil)])
    {
        [_webView goBack];
        
    }
    
    if ([_backBtnHome.titleLabel.text isEqualToString:NSLocalizedString(@"home_small", nil)])
    {
        
        flagBack=TRUE;
        
        if (totalviewload==0)
        {
            totalviewload=0;
            [self dismissViewControllerAnimated:NO completion:nil];
        }
        else
        {
            
            
            [self returnToRootViewController];
            totalviewload=0;
            
        }
        
        //[self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
        // [self.presentingViewController.presentingViewController
        // dismissViewControllerAnimated:YES completion:nil];
        
        /*
         
         
         UIViewController *vc = self.presentingViewController;
         while (vc.presentingViewController)
         {
         vc = vc.presentingViewController;
         
         if ([vc isKindOfClass:[AdvanceSearchVC class]])
         {
         [vc dismissViewControllerAnimated:NO completion:NULL];
         
         }
         if ([vc isKindOfClass:[HomeDetailVC class]])
         {
         flagBack=TRUE;
         [vc dismissViewControllerAnimated:NO completion:NULL];
         
         }
         
         }
         
         
         flagBack=TRUE;
         [self dismissViewControllerAnimated:NO completion:^{
         [self dismissViewControllerAnimated:NO completion:nil];
         }];
         */
        
    }
    
    
    
    if(flagBack!=TRUE)
    {
        if ([self.webView canGoBack])
        {
            [_backBtnHome setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
            
            NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
            if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
            {
                
                CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                               @{NSFontAttributeName: [UIFont systemFontOfSize:_backBtnHome.titleLabel.font.pointSize]}];
                
                CGRect framebtn=CGRectMake(_backBtnHome.frame.origin.x, _backBtnHome.frame.origin.y, _backBtnHome.frame.size.width, _backBtnHome.frame.size.height);
                
                [_backBtnHome setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
                _backBtnHome.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
                
            }
            
        }
        else
        {
            [_backBtnHome setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
            
        }
        
    }
    
    
    /*   if ([self.webView canGoBack])
     {
     // [webView goBack];
     // [_backBtnHome setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
     [_backBtnHome setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
     [_webView goBack];
     }
     else
     {
     [_backBtnHome setTitle:NSLocalizedString(@"home_small", nil) forState:UIControlStateNormal];
     self.webView = nil;
     [self dismissViewControllerAnimated:NO completion:nil];
     }
     */
    
    
    
    // [self.navigationController popViewControllerAnimated:YES];
    
    
    // self.view.tag=9999;
    
    // [self.navigationController popViewControllerAnimated:YES];
    
    // self.tabBarController.selectedIndex = 1;
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:NO];
    
    [self loadviewwillappear];
    // [self.navigationController.view addGestureRecognizer:gestureRecognizer];
    
    //[[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    BOOL isHintShown = [[NSUserDefaults standardUserDefaults] boolForKey:@"HintShownRecent"];
    if (!isHintShown)
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HintShownRecent"];
        
        MaterialShowcase *show =  [self showcaseWithtext:NSLocalizedString(@"dept_hint_text", nil) withView:btnRecent];
        [show showWithCompletion:nil];
        
    }
    
    [self setViewFont];
}

#pragma mark- Font Set to View
-(void)setViewFont
{
    [_backBtnHome.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    lbl_underConstruction.font = [AppFont boldFont:18.0];
    _lbltitle.font = [AppFont semiBoldFont:17.0];
    lbl_download.font = [AppFont semiBoldFont:18.0];
    lbl_download.font = [AppFont semiBoldFont:18.0];
    btnViewonMap.titleLabel.font = [AppFont regularFont:18.0];
    btnRate.titleLabel.font = [AppFont regularFont:18.0];
    btnShare.titleLabel.font = [AppFont regularFont:18.0];
    btn_livechat.titleLabel.font = [AppFont regularFont:18.0];
    btnInfo.titleLabel.font = [AppFont regularFont:18.0];
    lblRecentlyService.font = [AppFont mediumFont:13.0];
    lblUmangHome.font = [AppFont mediumFont:14.0];
    
    lbl_downloadingtxt.font = [AppFont regularFont:12.0];
    lbl_downloadbook.font = [AppFont regularFont:12.0];
    lbl_downloadcomplete.font = [AppFont regularFont:12.0];
    
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}

#pragma mark -
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    //[[NSNotificationCenter defaultCenter]removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    
}

/*- (void)orientationChanged:(NSNotification *)notification{
 [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
 }*/


-(void)loadviewwillappear
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    
    if(flagViewAppear==TRUE)
    {
        
        
        if (urlString.length==0) {
            //show under construction page
            self.webView.hidden=TRUE;
            img_underConst.hidden=FALSE;
            lbl_underConstruction.hidden=FALSE;
        }
        else
        {
            self.webView.hidden=FALSE;
            img_underConst.hidden=TRUE;
            lbl_underConstruction.hidden=TRUE;
            //[self loadHtml];
            
            [self hitFetchProfileAPI];
            //load url with passed url
        }
        //[self setNeedsStatusBarAppearanceUpdate];
        
        
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                              action:@selector(handleTap)];
        tap.numberOfTapsRequired = 1;
        tap.delegate = self;
        
        [self.webView addGestureRecognizer:tap];
        
        
        
        //----- Code for adding back gesture to reload back view of webview----------------
        /*  UISwipeGestureRecognizer *gestureRightRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightoleftClose)];
         [gestureRightRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
         gestureRightRecognizer.delegate=self;
         [self.webView addGestureRecognizer:gestureRightRecognizer];
         
         
         
         UISwipeGestureRecognizer *gestureLeftRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(lefttorightOpen)];
         [gestureLeftRecognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
         gestureLeftRecognizer.delegate=self;
         [self.webView addGestureRecognizer:gestureLeftRecognizer];
         
         */
        
        //__weak id weakSelf = self;
        //self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
        
        
        
        
        
        
    }
    
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation
{
    
    
    
    
    vw_recent.hidden=TRUE;
    UIView *removeView;
    while((removeView = [self.view viewWithTag:1010]) != nil) {
        [removeView removeFromSuperview];
    }
    
    if (UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication.statusBarOrientation))
    {
        vw_recent.frame = CGRectMake(fDeviceWidth+100, 20, 100, fDeviceHeight-20);
        activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);
    }
    else
    {
        // vw_recent.frame = vw_recentFrame;
        vw_recent.frame = CGRectMake(fDeviceWidth+100, 20, 100, fDeviceHeight-20);
        
        activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);
    }
    [tbl_recent reloadData];
    //[self recentBtnAction:self];
    // [self handleTap];
    
    
    
    
}

/*
 - (void)viewWillLayoutSubviews {
 
 
 vw_recent.hidden=TRUE;
 
 if (UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication.statusBarOrientation))
 {
 vw_recent.frame = CGRectMake(fDeviceWidth-100, 0, 100, fDeviceHeight);
 activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);
 }
 else
 {
 // vw_recent.frame = vw_recentFrame;
 vw_recent.frame = CGRectMake(fDeviceWidth-100, 0, 100, fDeviceHeight);
 
 activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);
 }
 [self recentBtnAction:self];
 [self handleTap];
 [super viewWillLayoutSubviews];
 }
 
 */

-(void)lefttorightOpen
{
    vw_recent.hidden=FALSE;
    
    if (UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication.statusBarOrientation))
    {
        //------- Start Remove blur effect----------------
        UIView *removeView;
        while((removeView = [self.view viewWithTag:1010]) != nil) {
            [removeView removeFromSuperview];
        }
        vw_support.hidden=FALSE;
        //-------End Remove blur effect----------------
        
        
        //------- Start Adding blur effect----------------
        UIView *viewBg=[[UIView alloc]initWithFrame:self.view.frame];
        viewBg.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.5];
        viewBg.tag=1010;
        [self.webView addSubview:viewBg];
        //------- End Adding blur effect----------------
        
        vw_support.hidden=FALSE;
        [tbl_recent reloadData];
        
        [UIView animateWithDuration:0.2
                              delay:0.01
                            options: UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             vw_recent.frame = CGRectMake(fDeviceWidth-100, 20, 100, fDeviceHeight-20);
                         }
                         completion:^(BOOL finished){
                             if (finished)
                                 // [vw_recent removeFromSuperview];
                                 NSLog(@"dd");
                         }];
        
        
        
    }
    else
    {
        //------- Start Remove blur effect----------------
        UIView *removeView;
        while((removeView = [self.view viewWithTag:1010]) != nil) {
            [removeView removeFromSuperview];
        }
        vw_support.hidden=FALSE;
        //-------End Remove blur effect----------------
        
        
        //------- Start Adding blur effect----------------
        UIView *viewBg=[[UIView alloc]initWithFrame:self.view.frame];
        viewBg.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.5];
        viewBg.tag=1010;
        [self.webView addSubview:viewBg];
        //------- End Adding blur effect----------------
        
        vw_support.hidden=FALSE;
        [tbl_recent reloadData];
        
        [UIView animateWithDuration:0.2
                              delay:0.01
                            options: UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             //vw_recent.frame = vw_recentFrame;
                             vw_recent.frame = CGRectMake(fDeviceWidth-100, 20, 100, fDeviceHeight-20);
                             
                         }
                         completion:^(BOOL finished){
                             if (finished)
                                 // [vw_recent removeFromSuperview];
                                 NSLog(@"dd");
                         }];
        
    }
    
}

-(void)rightoleftClose
{
    if (UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication.statusBarOrientation))
    {
        
        
        
        UIView *removeView;
        while((removeView = [self.view viewWithTag:1010]) != nil) {
            [removeView removeFromSuperview];
        }
        vw_support.hidden=FALSE;
        //-------End Remove blur effect----------------
        
        
        
        
        [UIView animateWithDuration:0.2
                              delay:0.01
                            options: UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             // vw_recent.frame = CGRectMake(vw_recentFrame.origin.x+vw_recentFrame.size.width, vw_recentFrame.origin.y, vw_recentFrame.size.width, vw_recentFrame.size.height);
                             
                             
                             vw_recent.frame = CGRectMake(fDeviceWidth+100, 20, 100, fDeviceHeight-20);
                             
                         }
                         completion:^(BOOL finished){
                         }];
    }
    
    
    
    
    
    
    
    else
    {
        //------- Start Remove blur effect----------------
        UIView *removeView;
        while((removeView = [self.view viewWithTag:1010]) != nil) {
            [removeView removeFromSuperview];
        }
        vw_support.hidden=FALSE;
        //-------End Remove blur effect----------------
        
        
        vw_support.hidden=FALSE;
        
        
        [UIView animateWithDuration:0.2
                              delay:0.01
                            options: UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             // vw_recent.frame = CGRectMake(vw_recentFrame.origin.x+vw_recentFrame.size.width, vw_recentFrame.origin.y, vw_recentFrame.size.width, vw_recentFrame.size.height);
                             
                             vw_recent.frame = CGRectMake(fDeviceWidth+100, 20, 100, fDeviceHeight-20);
                             
                         }
                         completion:^(BOOL finished){
                         }];
    }
}
//------------------------------------------






-(IBAction)recentBtnAction:(id)sender
{
    //vw_recent.hidden=FALSE;
    [self lefttorightOpen];
    vw_support.hidden=TRUE;
    vw_menu.hidden=TRUE;
    //vw_recent.hidden=TRUE;
    
    [tbl_recent reloadData];
    
}
//------------------------------------------


-(void) handleTap {
    //NSLog(@"tap");
    vw_support.hidden=TRUE;
    [self rightoleftClose];
    vw_menu.hidden=TRUE;
    // vw_recent.hidden=TRUE;
}

//----- hitAPI for IVR OTP call Type registration ------
-(void)hitFetchProfileAPI
{
    //hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    // hud.label.text = NSLocalizedString(@"Please wait...", @"Please wait...");
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"lang"];//lang is Status of the account
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [dictBody setObject:@"" forKey:@"st"];  //get from mobile default email //not supported iphone
    [dictBody setObject:@"both" forKey:@"type"];  //get from mobile default email //not supported iphone
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_VIEW_PROFILE withBody:dictBody andTag:TAG_REQUEST_VIEW_PROFILE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        // [hud hideAnimated:YES];
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            // NSString *rd=[response valueForKey:@"rd"];
            
            
            NSString *responseStr=[NSString stringWithFormat:@"%@",response];
            if ([responseStr length]>0)
            {
                
                
                if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                    
                {
                    
                    @try {
                        
                        
                        //singleton.user_tkn=tkn;
                        NSMutableArray *aadharpd=[[NSMutableArray alloc]init];
                        aadharpd=[[response valueForKey:@"pd"]valueForKey:@"aadharpd"];
                        
                        
                        NSMutableArray *generalpd=[[NSMutableArray alloc]init];
                        generalpd=[[response valueForKey:@"pd"]valueForKey:@"generalpd"];
                        
                        
                        
                        socialpd=[[NSMutableArray alloc]init];
                        
                        singleton.user_id=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"uid"];
                        NSString *abbreviation = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"abbr"];
                        
                        if ([abbreviation length]==0) {
                            abbreviation=@"";
                        }
                        
                        singleton.user_StateId = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ostate"];
                        [singleton setStateId:singleton.user_StateId];
                        
                        NSString *emblemString = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"stemblem"];
                        
                        emblemString = emblemString.length == 0 ? @"":emblemString;
                        
                        [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
                        
                        
                        [[NSUserDefaults standardUserDefaults] setObject:abbreviation forKey:@"ABBR_KEY"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        
                        //-------- Add later----------
                        singleton.shared_ntft=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntft"];
                        singleton.shared_ntfp=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntfp"];
                        //-------- Add later----------
                        
                        
                        if ([singleton.user_id length]==0) {
                            singleton.user_id=@"";
                        }
                        //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        
                        // [defaults setObject:singleton.user_id forKey:@"USER_ID"];
                        
                        //[defaults synchronize];
                        
                        
                        //------------------------- Encrypt Value------------------------
                        
                        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                        // Encrypt
                        [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_id withKey:@"USER_ID"];
                        [[NSUserDefaults standardUserDefaults]synchronize];
                        //------------------------- Encrypt Value------------------------
                        
                        
                        //NSString *identifier =( [[response valueForKey:@"pd"]valueForKey:@"socialpd"];
                        socialpd=(NSMutableArray *)[[response valueForKey:@"pd"]valueForKey:@"socialpd"];
                        
                        
                        
                        //----- local values  to be save and show from API-----------
                        
                        
                        
                        // NSString * sid=[generalpd valueForKey:@"sid"];
                        
                        str_name=[generalpd valueForKey:@"nam"];
                        str_registerMb =[generalpd valueForKey:@"mno"];
                        str_alternateMb=[generalpd valueForKey:@"amno"];
                        //str_city=[generalpd valueForKey:@"cty"];
                        str_state=[generalpd valueForKey:@"st"];
                        
                        // state_id=[obj getStateCode:str_state];
                        
                        
                        str_district=[generalpd valueForKey:@"dist"];
                        str_dob=[generalpd valueForKey:@"dob"];
                        str_gender=[generalpd valueForKey:@"gndr"];
                        str_emailAddress=[generalpd valueForKey:@"email"];
                        str_emailVerifyStatus=[generalpd valueForKey:@"emails"];
                        str_amnosVerifyStatus=[generalpd valueForKey:@"amnos"];
                        str_Url_pic=[generalpd valueForKey:@"pic"];
                        str_address=[generalpd valueForKey:@"addr"];
                        
                        NSLog(@"str_gender=%@",str_gender);
                        
                        /* if (str_gender.length>0) {
                         _tblUserProfile.tableHeaderView =  [self designHeaderView];
                         }
                         */
                        NSString *quali_id=[generalpd valueForKey:@"qual"];
                        NSString *Occu_id=[generalpd valueForKey:@"occup"];
                        
                        
                        str_qualification=[obj getqualName:quali_id];
                        str_occupation=[obj getOccuptname:Occu_id];
                        
                        NSLog(@"str_Url_pic  =>>> %@",str_Url_pic);
                        /*
                         "gcmid": "",
                         "opid": "",
                         "refid": "",
                         "tkn": "",
                         */
                        
                        if ([[response objectForKey:@"socialpd"] isKindOfClass:[NSDictionary class]]) {
                            singleton.objUserProfile = nil;
                            singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"socialpd"]];
                        }
                        if ([[response objectForKey:@"pd"] isKindOfClass:[NSDictionary class]]) {
                            singleton.objUserProfile = nil;
                            singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
                        }
                        
                        [self setProfileData];
                        
                    } @catch (NSException *exception) {
                        
                    } @finally {
                        
                    }
                }
            }
            
            
        }
        else
        {
            
            NSLog(@"Error Occured = %@",error.localizedDescription);
            /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
             message:error.localizedDescription
             delegate:self
             cancelButtonTitle:@"OK"
             otherButtonTitles:nil];
             [alert show];
             */
            [self setProfileData];
        }
        
    }];
    
}

-(NSString *)UpperFirstWord:(NSString*)inputString
{
    
    if ([inputString length]!=0) {
        inputString = [inputString stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[inputString substringToIndex:1] uppercaseString]];
    }
    return inputString;
}


-(void)setProfileData
{
    
    //----- local values  to be save and show from API-----------
    
    
    str_name=[self UpperFirstWord:str_name];
    // str_gender=str_dob;
    str_qualification=[self UpperFirstWord:str_qualification];
    str_occupation=[self UpperFirstWord:str_occupation];
    str_state=[self UpperFirstWord:str_state];
    str_district=[self UpperFirstWord:str_district];
    str_address=[self UpperFirstWord:str_address];
    str_registerMb=[self UpperFirstWord:str_registerMb];
    //str_emailAddress=str_emailAddress;
    str_alternateMb=[self UpperFirstWord:str_alternateMb];
    
    
    
    
    //  if ([str_Url_pic length]!=0 ) {
    
    singleton.user_profile_URL=str_Url_pic;
    // }
    
    
    
    
    
    
    if ([str_gender length]!=0)
    {
        str_gender=[str_gender uppercaseString];
        
        /*  if ([str_gender isEqualToString:@"M"]) {
         stringGender= NSLocalizedString(@"gender_male", nil);
         singleton.notiTypeGenderSelected= NSLocalizedString(@"gender_male", nil);
         
         }
         else  if ([str_gender isEqualToString:@"F"]) {
         singleton.notiTypeGenderSelected=NSLocalizedString(@"gender_female", nil);
         stringGender=NSLocalizedString(@"gender_female", nil);
         
         }
         else  if ([str_gender isEqualToString:@"T"]||[str_gender isEqualToString:@"t"]) {
         singleton.notiTypeGenderSelected=NSLocalizedString(@"gender_transgender", nil);
         stringGender=NSLocalizedString(@"gender_transgender", nil);
         
         }
         
         else
         {
         singleton.notiTypeGenderSelected=@"";
         stringGender=@"";
         }
         */
        
    }
    
    str_name=[self UpperFirstWord:str_name];
    //str_gender=str_dob;
    str_qualification = [self UpperFirstWord:str_qualification];
    str_occupation = [self UpperFirstWord:str_occupation];
    str_state = [self UpperFirstWord:str_state];
    str_district = [self UpperFirstWord:str_district];
    str_address = [self UpperFirstWord:str_address];
    str_registerMb = [self UpperFirstWord:str_registerMb];
    //str_emailAddress=str_emailAddress;
    str_alternateMb = [self UpperFirstWord:str_alternateMb];
    //singleton.notiTypeGenderSelected
    singleton.profileNameSelected = str_name;
    singleton.profilestateSelected = str_state;
    singleton.notiTypeCitySelected = @"";
    singleton.notiTypDistricteSelected = str_district;
    singleton.profileDOBSelected = str_dob;
    singleton.altermobileNumber = str_alternateMb;
    singleton.user_Qualification = str_qualification;
    singleton.user_Occupation = str_occupation;
    singleton.profileEmailSelected = str_emailAddress;
    singleton.profileUserAddress = str_address;
    
    NSLog(@"str_gender=%@",str_gender);
    /* if ([str_gender isEqualToString:@"M"]) {
     
     stringGender=NSLocalizedString(@"gender_male", nil);
     
     }
     else if ([str_gender isEqualToString:@"F"]) {
     
     stringGender=NSLocalizedString(@"gender_female", nil);
     
     
     }
     else if ([str_gender isEqualToString:@"T"])
     {
     stringGender=NSLocalizedString(@"gender_transgender", nil);
     
     
     
     }
     else {
     stringGender=@"";
     
     
     
     }
     */
    
    [self loadHtml];
    
    
    
    
}



-(void)gobackWebview
{
    //[self.webView goBack];
    
    if ([self.webView canGoBack])
    {
        [self.webView goBack];
        //self.web_back_btn.enabled=TRUE;
    }
    else
    {
        self.web_back_btn.enabled=FALSE;
        
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//------------------------------------------

-(IBAction)customerCareBtnAction:(id)sender
{
    //  [self displayContentController:[self getHomeDetailLayerLeftController]];
    vw_support.hidden=false;
    
    //vw_support.hidden=TRUE;
    vw_menu.hidden=TRUE;
    // vw_recent.hidden=TRUE;
    [self rightoleftClose];
    
}



-(IBAction)moreBtnAction:(id)sender
{
    vw_menu.hidden=FALSE;
    
    vw_support.hidden=TRUE;
    [self rightoleftClose];
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    actionSheet.view.tintColor = [UIColor blackColor];
    
    UIAlertAction *alertActionCancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                                         {
                                             
                                             
                                         }];
    [alertActionCancel setValue:[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0] forKey:@"titleTextColor"];
    [actionSheet addAction:alertActionCancel];
    
    
    UIAlertAction *viewOnMapAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"view_on_map", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self btn_map_vw_menu:self];
    }];
    UIImage *viewonMapImage = [UIImage imageNamed:@"map_menu"];
    viewonMapImage = [viewonMapImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [viewOnMapAction setValue:viewonMapImage forKey:@"image"];
    
    [viewOnMapAction setValue:[NSNumber numberWithInteger:0] forKey:@"titleTextAlignment"];

    [actionSheet addAction:viewOnMapAction];
    
    
    UIAlertAction *rateAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"rate", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self btn_rate_vw_menu:self];
    }];
    UIImage *rateImage = [UIImage imageNamed:@"rate_menu"];
    rateImage = [rateImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [rateAction setValue:rateImage forKey:@"image"];
    [rateAction setValue:[NSNumber numberWithInteger:0] forKey:@"titleTextAlignment"];
    
    [actionSheet addAction:rateAction];
    
    
    UIAlertAction *shareAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"share", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self btn_share_vw_menu:self];
    }];
    UIImage *shareImage = [UIImage imageNamed:@"share_menu"];
    shareImage = [shareImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [shareAction setValue:shareImage forKey:@"image"];
    [shareAction setValue:[NSNumber numberWithInteger:0] forKey:@"titleTextAlignment"];
    [actionSheet addAction:shareAction];
    
    
    UIAlertAction *liveChatAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"help_live_chat", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self btn_chat_vwSupport:self];
    }];
    [liveChatAction setValue:[NSNumber numberWithInteger:0] forKey:@"titleTextAlignment"];
    UIImage *liveChatImage = [UIImage imageNamed:@"live_chat_menu"];
    liveChatImage = [liveChatImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [liveChatAction setValue:liveChatImage forKey:@"image"];
    
    [actionSheet addAction:liveChatAction];
    
    
    UIAlertAction *informationAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"information", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self btn_information:self];
    }];
    UIImage *informationImage = [UIImage imageNamed:@"info_menu"];
    informationImage = [informationImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [informationAction setValue:informationImage forKey:@"image"];
    [informationAction setValue:[NSNumber numberWithInteger:0] forKey:@"titleTextAlignment"];
    [actionSheet addAction:informationAction];
    
    // Present action sheet.
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        /*UIPopoverPresentationController *popPresenter = [actionSheet
                                                         popoverPresentationController];
        popPresenter.sourceView = self.view;
        popPresenter.sourceRect = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0);
        [self presentViewController:actionSheet animated:YES completion:nil];*/
        
        UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:actionSheet];
        
        [popover presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 0, 0)inView:self.view permittedArrowDirections:0 animated:NO];
        
    }
    else
    {
        [self presentViewController:actionSheet animated:YES completion:nil];
    }
    
    
    
}

- (void)presentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion {
    
    // Unsure why WKWebView calls this controller - instead of it's own parent controller
    if (self.presentedViewController) {
        [self.presentedViewController presentViewController:viewControllerToPresent animated:flag completion:completion];
    } else {
        [super presentViewController:viewControllerToPresent animated:flag completion:completion];
    }
}

-(void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion
{
    if (self.presentedViewController)
    {
        [super dismissViewControllerAnimated:flag completion:completion];
    }
    else if(flagBack==TRUE)
    {
        [super dismissViewControllerAnimated:flag completion:completion];
        
    }
}
//============ebook code==================

//============================= Start Here===============================
-(IBAction)closeBtn:(id)sender
{
    vw_filedownloadManager.hidden=TRUE;
    if (downloadFileno>=[downloadBookArry count]) {
        downloadCheck=FALSE;
        [self isDownloadingProgress];
    }
    else
    {
        
    }
}
-(IBAction)clickonDownloadview:(id)sender
{
    /*  vw_filedownloadManager.hidden=FALSE;
     vw_filedownloadManager.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.4];
     [tbldonwload reloadData];
     [self addShadowToTheView:vw_filedownload];*/
}




//============ ebook code end==========


#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation
 {
 
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 
 }
 */


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

