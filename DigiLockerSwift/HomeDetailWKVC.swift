//
//  HomeDetailWKVC.swift
//  Umang
//
//  Created by Rashpinder on 23/03/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

import UIKit
import WebKit

class HomeDetailWKVC: UIViewController
{
    // MARK:- **** Property Decalaration *****
    @IBOutlet var naviView: UIView!
    @IBOutlet var btnBack: UIButton!
    
    @IBOutlet var lblTitle: UILabel!
    
    var webView: WKWebView!
    var serviceData:ServiceData!
    var dicService:[String:String]!
    var postURI:URL!
    var tagComeFrom = ""
    var state_Selected = false
    var urlRequest :URLRequest!
    
    var singleton = SharedManager.sharedSingleton() as! SharedManager
    
    
    
    // MARK:- **** View Life Cycle  *****
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewLoadDid()
        self.initializeWkWebView()
        
        // Do any additional setup after loading the view.
    }
    @IBAction func didTapBackButtonAction(_ sender: UIButton) {
        if self.webView.canGoBack {
            self.webView.goBack()
            return
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
// MARK:- **** WKWebView *** Webview Setup *****
extension HomeDetailWKVC {
    func initializeWkWebView() {
        let config = WKWebViewConfiguration()
         config.preferences.javaScriptCanOpenWindowsAutomatically = true
        config.preferences.javaScriptEnabled = true
        var frame = self.view.frame
         frame.origin.y = 64.0
        frame.size.height = self.view.frame.size.height - 64.0
         self.webView = WKWebView(frame: frame, configuration: config)
        // self.webView.uiDelegate = self
        self.webView.navigationDelegate = self
       //  let cockies = self.loadCockieArray()
        var userinfo = [AnyHashable: Any]()
        userinfo["sid"] = self.serviceData.SERVICE_ID
        userinfo["nam"] = singleton.profileNameSelected
        userinfo["addr"] = singleton.profileUserAddress
        var st = singleton.profilestateSelected!
        if st.count == 0 || (st == "9999") {
            st = ""
        }
        let obj = StateList()
        userinfo["st"] = st
        userinfo["cty"] = ""
        userinfo["dob"] = singleton.profileDOBSelected
        userinfo["gndr"] = ""
        userinfo["qual"] = obj.getQualiListCode(singleton.user_Qualification)
        userinfo["email"] = singleton.profileEmailSelected
        userinfo["amno"] = singleton.altermobileNumber
        var selectedLanguage = UserDefaults.standard.object(forKey: "PreferedLocale") as? String
        if selectedLanguage == nil {
            selectedLanguage = "en"
        }
        else {
            let arrTemp = selectedLanguage?.components(separatedBy: "-")
            selectedLanguage = arrTemp?.first
        }
        let checklang: Bool = singleton.dbManager.getServiceLanguage(serviceData.SERVICE_ID, withDeviceLang: selectedLanguage)
        var lang = ""
        if checklang == true {
            lang = selectedLanguage!
            //add condition for it
        }
        else {
            lang = "en"
        }
        userinfo["lang"] = lang
        userinfo["ntfp"] = "1"
        userinfo["ntft"] = "1"
        userinfo["pic"] = singleton.user_profile_URL
        userinfo["psprt"] = ""
        userinfo["occup"] = obj.getOccuptCode(singleton.user_Occupation)
        userinfo["dist"] = ""//singleton.notiTypDistricteSelected!
        userinfo["amnos"] = ""
        userinfo["emails"] = ""
        userinfo["ivrlang"] = ""
        userinfo["gcmid"] = obj.getStateCode(singleton.profilestateSelected )
        userinfo["mno"] = singleton.mobileNumber
        userinfo["uid"] = singleton.user_id
        var tkn = singleton.user_tkn
        if tkn?.count == 0 {
            tkn = ""
        }
        let dictBody = NSMutableDictionary()
      // let apiManager = HomeDetailVC()
       // dictBody.addEntries(from: apiManager.getCommonParametersForRequestBody() as! [AnyHashable : Any])
        dictBody["mod"] = "app"
        dictBody["peml"] = ""
        dictBody["lang"] = lang
        dictBody["tkn"] = tkn
        dictBody["mno"] = singleton.mobileNumber
        dictBody["service_id"] = serviceData.SERVICE_ID
        if let adhadar = singleton.objUserProfile?.objAadhar?.aadhar_number,  !adhadar.isEmpty
        {
            
            dictBody["aadhr"] = adhadar
            var adhrinfo = [AnyHashable: Any]()
            adhrinfo["adhr_name"] = singleton.objUserProfile.objAadhar.name
            adhrinfo["adhr_dob"] = singleton.objUserProfile.objAadhar.dob
            adhrinfo["adhr_co"] = singleton.objUserProfile.objAadhar.father_name
            adhrinfo["adhr_gndr"] = singleton.objUserProfile.objAadhar.gender
            adhrinfo["adhr_imgurl"] = singleton.objUserProfile.objAadhar.aadhar_image_url
            adhrinfo["adhr_dist"] = singleton.objUserProfile.objAadhar.district
            adhrinfo["mno"] = singleton.objUserProfile.objAadhar.mobile_number
            adhrinfo["adhr_state"] = singleton.objUserProfile.objAadhar.state
            dictBody["adhrinfo"] = adhrinfo
        }
        //---------- add userinfo -------------------
        dictBody["userinfo"] = userinfo
        //---------- add adhrinfo-------------------
        let checkLinkStatus = UserDefaults.standard.decryptedValue(forKey: "LINKDIGILOCKERSTATUS")
        if (checkLinkStatus == "YES") {
            UserDefaults.standard.setAESKey("UMANGIOSAPP")//AESKey =
            let username = UserDefaults.standard.decryptedValue(forKey: "digilocker_username")
            let password = UserDefaults.standard.decryptedValue(forKey: "digilocker_password")
            UserDefaults.standard.synchronize()
            var digilocker = [AnyHashable: Any]()
            digilocker["username"] = username
            digilocker["password"] = password

            dictBody["digilocker"] = digilocker
        }
        let urlAddress = serviceData.SERVICE_URL
        let state_nametopass = ""
        let state_idtopass = ""
        if (state_Selected) {
            //add both parameter in dicBody in case of state ONLY else its is not added in the dicBody
            // pass key here with new paramenter
            dictBody["tab_state_name"] = state_nametopass
            dictBody["tab_state_id"] = state_idtopass
        }
       var jsonData: Data? = nil
        do {
            jsonData = try? JSONSerialization.data(withJSONObject: dictBody, options: .init(rawValue: 0))
        }
        var contentToEncode: NSString = ""
        if let aData = jsonData {
            contentToEncode = String(data: aData, encoding: .utf8)! as NSString
        }
        contentToEncode = contentToEncode.removingPercentEncoding! as NSString//contentToEncode.removingPercentEncoding
        print("contentToEncode \(contentToEncode)")
        let contentEncoding = "application/vnd.umang.web+json; charset=UTF-8"
        postURI = URL(string: urlAddress)
        let verb = "GET"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, d MMM yyyy HH:mm:ss z"
        let currentDate = dateFormatter.string(from: Date())
        let currentDatetrim = currentDate.trimmingCharacters(in: CharacterSet.whitespaces)
        let USERNAME = "UM4NG"
        let contentMd5 = contentToEncode.md5()
        let contentMd5trim = contentMd5?.trimmingCharacters(in: CharacterSet.whitespaces)
        let postURItrim = postURI.path.trimmingCharacters(in: CharacterSet.whitespaces)
        let toSign = "\(verb)\n\(contentMd5trim!)\n\(contentEncoding)\n\(currentDatetrim)\n\(postURItrim)" as NSString
        print("toSign \(toSign)")

        let hmac = toSign.hmacsha1(toSign as String, secret:SaltSHA1MAC )//hmacsha1(toSign, secret: SaltSHA1MAC)
        //NSLog(@"hmac %@",hmac);
        let hmactrim = hmac?.trimmingCharacters(in: CharacterSet.whitespaces) ?? ""//(in: CharacterSet.whitespaces)
        let XappAuth = "\(USERNAME):\(hmactrim)"
        var request = URLRequest(url: postURI)
        //[request setHTTPMethod: @"GET"];
        request.addValue(XappAuth, forHTTPHeaderField: "X-App-Authorization")
        request.addValue(currentDate, forHTTPHeaderField: "X-App-Date")
        request.addValue(contentMd5trim!, forHTTPHeaderField: "X-App-Content")
        request.addValue(contentToEncode as String , forHTTPHeaderField: "X-App-Data")
        request.addValue(contentEncoding, forHTTPHeaderField: "Content-Type")
        //---- Adding in cookies--------------
        request.httpShouldHandleCookies = true
//        if cockies != nil && cockies!.count > 0 {
//            request.addCookies(cockies!)
//        }
        print("URL    ---\(request.allHTTPHeaderFields)")
        print("NSURL  ---\(urlRequest.allHTTPHeaderFields)")
        self.webView.load(request)
        self.view.addSubview(self.webView)
    }
   
   
    func viewLoadDid()  {
        self.serviceData = ServiceData(dicService)
        self.btnBack.setTitle("Back", for: .normal)
        self.lblTitle.text = serviceData.SERVICE_NAME
        
    }
}
// MARK:- **** WKUIDelegate WKUIDelegate WKUIDelegate WKUIDelegate *****

extension HomeDetailWKVC : WKUIDelegate {
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        return self.webView
    }
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Void) {
        
        completionHandler(true)
    }
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
         completionHandler(defaultText)
    }
    
}
// MARK:- **** WKNavigationDelegate ****  WKNavigationDelegate   *****
extension HomeDetailWKVC : WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let urlString = navigationAction.request.url!.absoluteString
        print("urlString navigationResponse -\(urlString)")
        if  urlString.contains("ios::") {
            decisionHandler(.cancel)
            return
        }else {
            decisionHandler(.allow)
        }
    }
//    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
//        print("decidePolicyFor navigationResponse -\(navigationResponse.response)")
//        let urlString = navigationResponse.response.url?.absoluteString
//        print("urlString navigationResponse -\(urlString)")
//
//        if urlString?.count != 0 && urlString!.hasPrefix("ios::") {
//            decisionHandler(.cancel)
//            return
//        }
//        decisionHandler(.allow)
//    }
//    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
//        print("didReceive URLAuthenticationChallenge -\(challenge.error)")
//
//    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("didFail navigation -\(error)")
    }
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print("didFailProvisionalNavigation navigation -\(error)")

    }
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        print("didCommit navigation -\(navigation.description)")

    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("didFinish navigation -\(navigation)")

    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("didStartProvisionalNavigation navigation -\(navigation)")

    }
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        print("didReceiveServerRedirectForProvisionalNavigation navigation -\(navigation)")

    }
}
// MARK:- **** Java Script Mathods *****
extension HomeDetailWKVC {
    enum IOSMethods :String {
        case pageLoad = "ios::getPageLoadingStatus::"
        case traceURL = "ios::traceUrl:"
        case downB64 = "ios::downloadFileBase64"
        case down1B64 = "iOS::downloadFileBase64"
        case pageTitle = "ios::setPageTitle"
        case showToast = "ios::showToast"
        case chatScreen = "ios::openChatScreen::"
        case downFile = "ios::downloadFile::"
        case downDeptFile = "ios::downloadFileDept::"
        case location = "ios::getCurrentLocation"
        case openPDF = "ios::openPDF::"
        case fetchLOC = "ios::fetchLocation::"
        case webFeedback = "ios::openWebViewFeedback::"
        case openWebview = "ios::openWebView::"
        case shareFile = "ios::shareFile::"
        case chooseFrom = "ios::openChooseFrom::"
        case allBooksData = "ios::getAllBooksData::"
        case chapterDataFromBook = "ios::getChapterDataFromBookId::"
        case chapterFromBook = "ios::getChaptersFromBookId::"
        case downChapter = "ios::startChapterDownLoad::"
        case openChapter = "ios::openChapter::"
        case deleteChapter = "ios::deleteChapter::"
        case viewDirection = "ios::viewDirection::"
        case forceLogout = "ios::forcelogout"
        case saveJSON = "ios::saveJsonString::"
        case getJSON = "ios::getJsonString::"
        case paniOS = "ios::panCallbackios::"
        case videoRecord = "ios::startRecordVideo::"
        case audioRecord = "ios::startRecordAudio::"
        case imageSelect = "ios::selectImage::"
        case naviTo1stURL = "ios::navigatetofirsturl::"
        case openBrowser = "ios::openBrowser::"
        case downB64_Share = "ios::downloadfilebase64ForShare"
        case down1B64_Share = "iOS::downloadfilebase64ForShare"
        case proofImage = "ios::proofImage::"
        case photoFile = "ios::getPhotoFile::"
        case signFile = "ios::getSignatureFile::"
        case eDistt_Org_Image = "ios::getEdistrictOriginalImage::"
        case getPDF = "ios::getPDF::"
        case ePathsala_share = "ios::ePathsalashare::"
        case digiCredentials = "ios::getDigilockerCredentials::"
        case getDocument = "ios::getDocument::"
        case openSMS = "ios::openComponseSms::"
        case mobileCall = "ios::mpMobileCallback::"
        case shareText = "ios::shareText::"
        case openPayment = "ios::openpayment::"
    }
}
