//
//  TxnDetailVC.m
//  Umang
//
//  Created by admin on 13/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "TxnDetailVC.h"
#import "txnDetailCell.h"

#import "MBProgressHUD.h"

#import "UMAPIManager.h"
#import "UIImageView+WebCache.h"

@interface TxnDetailVC ()<UIGestureRecognizerDelegate>
{
    
    __weak IBOutlet UILabel *lblTitle;
    __weak IBOutlet UIButton *btnBack;
    MBProgressHUD *hud;
    int tagvalue;
    txnDetailCell *txncell;
    SharedManager *singleton ;
}
@property(nonatomic,retain)NSMutableArray *table_data;
@property(nonatomic,retain)NSString *pageValue;



@end

@implementation TxnDetailVC
@synthesize table_data;
@synthesize sdltid;
@synthesize srid;
@synthesize dic_txnInfo;
@synthesize pageValue;


- (void)viewDidLoad {
    singleton = [SharedManager sharedSingleton];

    pageValue=@"1";
    _tbl_txndetails.tableHeaderView =  [self designHeaderView];
    table_data = [[NSMutableArray alloc]init];
    lblTitle.text = NSLocalizedString(@"transaction_history_detail", nil);
    [btnBack setTitle: NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    [self hitAPIForTransactionDetail];
    self.view.backgroundColor= [UIColor colorWithRed:235/255.0 green:234/255.0 blue:241/255.0 alpha:1.0];
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}



-(void)hitAPIForTransactionDetail

{
    SharedManager *singleton = [SharedManager sharedSingleton];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Set the label text.
    
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    
    
    if ([sdltid length]==0) {
        sdltid=@"";
    }
    if ([srid length]==0) {
        srid=@"";
    }
    
    
    
    [dictBody setObject:sdltid forKey:@"sdltid"];
    [dictBody setObject:@"" forKey:@"keywd"];
    [dictBody setObject:srid forKey:@"srid"];
    [dictBody setObject:@"" forKey:@"cgid"];
    [dictBody setObject:@"date_time" forKey:@"sortby"];
    [dictBody setObject:pageValue forKey:@"page"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:@"" forKey:@"peml"];
    [dictBody setObject:@"" forKey:@"lang"];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];
    
    
    
    NSLog(@"Dict body is :%@",dictBody);
    
    
    
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_TRANSACTION_HISTORY withBody:dictBody andTag:TAG_REQUEST_TRANSACTION_HISTORY completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        [hud hideAnimated:YES];
        
        
        
        if (error == nil) {
            
            NSLog(@"Server Response = %@",response);
            
            
            //            NSString *rc=[response valueForKey:@"rc"];
            //
            //            NSString *rs=[response valueForKey:@"rs"];
            
            
            
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            // NSString *rd=[response valueForKey:@"rd"];
            
            @try {
                NSString *pages=[[response valueForKey:@"pd"] valueForKey:@"page"];
                
                NSArray *pageArray = [pages componentsSeparatedByString:@"|"];
                
                NSString*  temppage =[pageArray objectAtIndex:0];
                if ([temppage isEqualToString:@"0"])
                {
                    
                    //do nothing
                }
                else
                {
                    pageValue=[pageArray objectAtIndex:1];
                }
                
                NSLog(@"page = %@",pageValue);
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
                
                
            {
                
                table_data=[[NSMutableArray alloc]init];
                
                table_data=[[response valueForKey:@"pd"] valueForKey:@"transList"];
                
                
                [_tbl_txndetails reloadData];
                
                //  [self alertwithMsg:rd];
                
            }
            
            
            
        }
        
        else{
            
            NSLog(@"Error Occured = %@",error.localizedDescription);
            
            
        }
        
        
        
    }];
    
}



- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 
 if ([cell respondsToSelector:@selector(tintColor)]) {
 CGFloat cornerRadius = 5.f;
 cell.backgroundColor = UIColor.clearColor;
 CAShapeLayer *layer = [[CAShapeLayer alloc] init];
 CGMutablePathRef pathRef = CGPathCreateMutable();
 CGRect bounds = CGRectInset(cell.bounds, 10, 0);
 BOOL addLine = NO;
 if (indexPath.row == 0 && indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
 CGPathAddRoundedRect(pathRef, nil, bounds, cornerRadius, cornerRadius);
 } else if (indexPath.row == 0) {
 CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds));
 CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds), CGRectGetMidX(bounds), CGRectGetMinY(bounds), cornerRadius);
 CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
 CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));
 addLine = YES;
 } else if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
 CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds));
 CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds), CGRectGetMidX(bounds), CGRectGetMaxY(bounds), cornerRadius);
 CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
 CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds));
 } else {
 CGPathAddRect(pathRef, nil, bounds);
 addLine = YES;
 }
 layer.path = pathRef;
 CFRelease(pathRef);
 //set the border color
 layer.strokeColor = [UIColor lightGrayColor].CGColor;
 //set the border width
 layer.lineWidth = 1;
 layer.fillColor = [UIColor colorWithWhite:1.f alpha:1.0f].CGColor;
 
 
 if (addLine == YES) {
 CALayer *lineLayer = [[CALayer alloc] init];
 CGFloat lineHeight = (1.f / [UIScreen mainScreen].scale);
 lineLayer.frame = CGRectMake(CGRectGetMinX(bounds), bounds.size.height-lineHeight, bounds.size.width, lineHeight);
 lineLayer.backgroundColor = tableView.separatorColor.CGColor;
 [layer addSublayer:lineLayer];
 }
 
 UIView *testView = [[UIView alloc] initWithFrame:bounds];
 [testView.layer insertSublayer:layer atIndex:0];
 testView.backgroundColor = UIColor.clearColor;
 cell.backgroundView = testView;
 }
 }
 */
-(UIView *)designHeaderView
{
    UIView *  vwHeader = [[UIView alloc]initWithFrame:CGRectMake(0, 0, _tbl_txndetails.frame.size.width,180.0)];
    [_tbl_txndetails addSubview:vwHeader];
    vwHeader.backgroundColor = [UIColor whiteColor];
    UIImageView *imageUserProfile =[[UIImageView alloc]initWithFrame:CGRectMake((_tbl_txndetails.frame.size.width/2)-50, 5, 100, 100)];
    imageUserProfile.layer.backgroundColor=[[UIColor clearColor] CGColor];
    [vwHeader addSubview: imageUserProfile];
    
    
    [imageUserProfile setUserInteractionEnabled:YES];
    
    NSURL *url=[NSURL URLWithString:[dic_txnInfo valueForKey:@"image"]];
    [imageUserProfile sd_setImageWithURL:url
                        placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
    
    
    imageUserProfile.clipsToBounds=YES;
    
    UIView  *vwHeaderLine = [[UIView alloc]initWithFrame:CGRectMake(0,vwHeader.frame.size.height-0.5, vwHeader.frame.size.width,0.5 )];
    vwHeaderLine.backgroundColor = [UIColor lightGrayColor];
    [vwHeader addSubview:vwHeaderLine];
    
    
    
    UILabel *lbl_servicename=[[UILabel alloc]initWithFrame:CGRectMake(0, imageUserProfile.frame.size.height+imageUserProfile.frame.origin.y + 5, fDeviceWidth, 30)];
    lbl_servicename.text=[dic_txnInfo valueForKey:@"servicename"];//hard coded value
    lbl_servicename.textAlignment = NSTextAlignmentCenter;
    lbl_servicename.font=[UIFont systemFontOfSize:16 weight:UIFontWeightSemibold];
    
    [vwHeader addSubview:lbl_servicename];
    
    
    
    UILabel *lbl_servicedesc=[[UILabel alloc]initWithFrame:CGRectMake(0, lbl_servicename.frame.size.height+lbl_servicename.frame.origin.y + 5, fDeviceWidth, 30)];
    lbl_servicedesc.text=[dic_txnInfo valueForKey:@"dept_resp"];//hard coded value
    
    lbl_servicedesc.font=[UIFont systemFontOfSize:14 weight:UIFontWeightLight];
    
    
    lbl_servicedesc.textAlignment = NSTextAlignmentCenter;
    [vwHeader addSubview:lbl_servicedesc];
    
    
    
    
    
    
    return vwHeader;
}

-(void)viewWillAppear:(BOOL)animated
{
    
    // [self setNeedsStatusBarAppearanceUpdate];
    //  [self performSelector:@selector(setHeightOfTableView) withObject:nil afterDelay:.1];
    /*UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(backbtnAction:)];
     [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
     [self.navigationController.view addGestureRecognizer:gestureRecognizer];
     */
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [super viewWillAppear:NO];
}

-(IBAction)backbtnAction:(id)sender
{
    [singleton traceEvents:@"Back Button" withAction:@"Clicked" withLabel:@"Transactional Detail" andValue:0];

    [self.navigationController popViewControllerAnimated:YES];
    
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 2)
    {
        CGFloat retVal = 44;
        CGRect dynamicHeight = [self rectForText:[[table_data objectAtIndex:2]valueForKey:@"value"] usingFont:[UIFont systemFontOfSize:14.0] boundedBySize:CGSizeMake(txncell.lbl_desc.frame.size.width, 500.0)];
        retVal = MAX(44, dynamicHeight.size.height)+10;
        
        //    NSLog(@"Height = %lf",retVal);
        
        return retVal;
    }
    else
    {
        return 44;
    }
    
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //Here you must return the number of sectiosn you want
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //Here, for each section, you must return the number of rows it will contain
    
    return table_data.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    //For each section, you must return here it's label
    return @"";
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return  40;
}





- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"txnDetailCell";
    
    txncell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    txncell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    if (txncell == nil) {
        txncell = [[txnDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    txncell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if ([table_data count]>0)
    {
        txncell.lbl_title.text=[[table_data objectAtIndex:indexPath.row] valueForKey:@"key"];
        txncell.lbl_title.font=[UIFont systemFontOfSize:14 weight:UIFontWeightSemibold];
        txncell.lbl_desc.text=[[table_data objectAtIndex:indexPath.row] valueForKey:@"value"];
        txncell.lbl_desc.numberOfLines = 0;
        // cell.lbl_desc.adjustsFontSizeToFitWidth = YES;
        txncell.lbl_desc.font=[UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
        
    }
    if (indexPath.row == 2)
    {
        CGRect dynamicHeight = [self rectForText:[[table_data objectAtIndex:2]valueForKey:@"value"] usingFont:[UIFont systemFontOfSize:14.0] boundedBySize:CGSizeMake(txncell.lbl_desc.frame.size.width, 500.0)];
        
        CGRect frameBG = txncell.frame;
        if ( frameBG.size.height <= 44) {
            frameBG.size.height = MAX(44.0, dynamicHeight.size.height)-20;
        }
        else
            frameBG.size.height = MAX(44.0, dynamicHeight.size.height)+20;
        txncell.frame = frameBG;
        
        CGRect frametxtfield = txncell.lbl_desc.frame;
        txncell.lbl_desc.frame=CGRectMake(frametxtfield.origin.x, frametxtfield.origin.y, frametxtfield.size.width, frameBG.size.height);
        
        txncell.lbl_desc.numberOfLines = 0;
        
    }
    else{
        
    }
    
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.separatorColor = [UIColor clearColor];
    
    txncell.accessoryType = UITableViewCellAccessoryNone;
    txncell.tag = indexPath.row;
    /* UILongPressGestureRecognizer *recognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
     recognizer.delegate = self;
     recognizer.delaysTouchesBegan = YES;
     [txncell addGestureRecognizer:recognizer];
     */
    
    return txncell;
}

-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

#pragma mark - Menu controller
/*
 - (void)longPress:(UILongPressGestureRecognizer *)recognizer
 {
 // tagvalue    = (int)recognizer.view;
 UIGestureRecognizer *recog = (UIGestureRecognizer*) recognizer;
 tagvalue = (int)recog.view.tag;
 NSLog(@"tagvalue=====%d",tagvalue);
 
 if ( recognizer.state == UIGestureRecognizerStateEnded )
 {
 txnDetailCell *cell = (txnDetailCell *)recognizer.view;
 
 
 UIMenuItem *Copy = [[UIMenuItem alloc] initWithTitle:@"Copy" action:@selector(copyAction:)];
 UIMenuController *menu = [UIMenuController sharedMenuController];
 [menu setMenuItems:[NSArray arrayWithObjects:Copy, nil]];
 [menu setTargetRect:cell.frame inView:cell.superview];
 [menu setMenuVisible:YES animated:YES];
 }
 }
 
 - (void)copyAction:(int)sender {
 NSLog(@"Cell was flagged");
 UIPasteboard *pb = [UIPasteboard generalPasteboard];
 @try {
 if ([table_data count]>0) {
 [pb setString:[[table_data objectAtIndex:tagvalue] valueForKey:@"value"]];
 
 }
 
 } @catch (NSException *exception) {
 
 } @finally {
 
 }
 }
 */
- (BOOL)tableView:(UITableView *)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL)tableView:(UITableView *)tableView canPerformAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    return (action == @selector(copy:));
}

- (void)tableView:(UITableView *)tableView performAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    if (action == @selector(copy:)){
        // UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        // [[UIPasteboard generalPasteboard] setString:cell.textLabel.text];
        
        if ([table_data count]>0) {
            [[UIPasteboard generalPasteboard] setString:[[table_data objectAtIndex:indexPath.row] valueForKey:@"value"]];
            
        }
        
    }
}

//- (void)approve:(id)sender {
//    NSLog(@"Cell was approved");
//}
//
//- (void)deny:(id)sender {
//    NSLog(@"Cell was denied");
//}
//




#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*
 #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }
 
 
 */


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
