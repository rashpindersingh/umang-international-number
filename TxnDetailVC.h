//
//  TxnDetailVC.h
//  Umang
//
//  Created by admin on 13/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TxnDetailVC : UIViewController
@property(nonatomic,retain)IBOutlet UILabel *lbl_title;
@property(nonatomic,retain)IBOutlet UIButton *btn_back;
@property(nonatomic,retain)IBOutlet UITableView *tbl_txndetails;
@property(nonatomic,retain)NSString *sdltid;
@property(nonatomic,retain)NSString *srid;
@property(nonatomic,retain)NSDictionary *dic_txnInfo;

@end
