//
//  ReportIssueVC.m
//  Umang
//
//  Created by admin on 15/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "ReportIssueVC.h"
#import "FeedbackHeaderCell.h"
#import "FeedbackCell.h"
#import "FeedbackTextCell.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "UITextView+Placeholder.h"
#define kOFFSET_FOR_KEYBOARD 120.0
#import "UIView+Toast.h"


@interface ReportIssueVC ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UITextViewDelegate>
{
    int expandedRow;
    int expandedSection;
    NSInteger currentSelectedSection;
    NSMutableArray *arrChooseCategory;
    NSMutableArray *arrChooseDepartment;
    NSMutableArray *arrChooseCateImage;
    NSMutableArray *arrChooseDepartImage;
    NSMutableArray *arrSelectedObj;
    
    __weak IBOutlet UIButton *btnHelpback;
    
    __weak IBOutlet UILabel *lblTitleReportIsse;
    FeedbackHeaderCell *feedbackHeader;
    MBProgressHUD *hud ;
    SharedManager*  singleton ;
    __weak IBOutlet UIButton *btnMoreBack;
    
    NSString *header_dpt_txt;
    NSString *select_category_txt;
    NSString *select_dept_txt;
    NSString *text_user_feedback;
    
    BOOL shouldHidden;
    NSString *serviceid;
    
    UITextField *myTextField;
    
    UITextView *_txtvwFeedback;
    UIToolbar* numberToolbar;
    
    UIImage *attachmentImage;
    
    NSMutableArray * arr_importtype;
    
    NSMutableArray *arrChooseDepartmentService;
    NSString *header_service_txt;
    
}

@property(nonatomic,retain)NSString *text_user_feedback;
@property(nonatomic,retain)NSString *base64img;
@property(nonatomic,retain)NSIndexPath *selectedIndexPath;
@property(nonatomic,retain)NSIndexPath *selectedfirstIndexPath;

@property(nonatomic,retain)NSString *serviceIDtoPass;

@property(nonatomic,retain)NSString *select_serviceID_txt;


@end



@implementation ReportIssueVC
@synthesize selectedIndexPath;

@synthesize text_user_feedback;
@synthesize base64img;
@synthesize selectedfirstIndexPath;



@synthesize serviceIDtoPass;
@synthesize select_serviceID_txt;


- (void)viewDidLoad {
    singleton = [SharedManager sharedSingleton];
    
    //    [btn setTitle:NSLocalizedString(@"feedback_help_txt", nil) forState:UIControlStateNormal];
    //    btnHelpSupportSection.titleLabel.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    
    arrChooseDepartmentService=[NSMutableArray new];
    serviceIDtoPass=@"";
    select_serviceID_txt=@"";
    
    
    [_txtvwFeedback setDelegate:self];
    [btnMoreBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnMoreBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnMoreBack.frame.origin.x, btnMoreBack.frame.origin.y, btnMoreBack.frame.size.width, btnMoreBack.frame.size.height);
        
        [btnMoreBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnMoreBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    [_btnSubmit setTitle:NSLocalizedString(@"submit", nil) forState:UIControlStateNormal];
    [btnHelpback setTitle:NSLocalizedString(@"help", nil) forState:UIControlStateNormal];
    lblTitleReportIsse.text = NSLocalizedString(@"help_report_an_issue", nil);
    
    
    base64img=@"";
    serviceid=@"";
    header_dpt_txt=NSLocalizedString(@"choose_department", nil);
    header_service_txt= NSLocalizedString(@"choose_service", nil);
    
    // select_category_txt=@"";
    
    select_category_txt = @"appfd";
    
    select_dept_txt=@"";
    
    shouldHidden=YES;
    attachmentImage = nil;
    _tblFeedback.delegate = self;
    _tblFeedback.dataSource = self;
    
    arrSelectedObj = [[NSMutableArray alloc]init];
    //  arrChooseDepartment = [[NSMutableArray alloc]initWithObjects:@"Aaple Sarkar",@"EPFO",@"CPGRAMS",@"Edisha",@"Aaple Sarkar",@"EPFO",@"CPGRAMS",@"Edisha", nil];
    
    
    arrChooseDepartment =[[singleton.dbManager loadDataServiceData] mutableCopy];
    
    
    //  arrChooseDepartImage =[[NSMutableArray alloc]initWithObjects:@"aaple_sarkar",@"epfo",@"cpgrams",@"edisha",@"aaple_sarkar",@"epfo",@"cpgrams",@"edisha", nil];
    
    arrChooseCategory = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"send_umang_app_issue_txt", nil),NSLocalizedString(@"send_department_app_issue_txt", nil),NSLocalizedString(@"other", nil), nil];
    
    arrChooseCateImage = [[NSMutableArray alloc]initWithObjects:@"feed_app",@"feed_department",@"feed_other", nil];
    
    
    
    // Do any additional setup after loading the view.
    
    currentSelectedSection = -1;
    
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
    
    [super viewDidLoad];
    
    
    //------ Add dismiss keyboard while background touch-------
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    
}


-(void) textViewDidBeginEditing:(UITextView *)textView
{
    
    if (fDeviceHeight<=568) {
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.3];
        [UIView setAnimationBeginsFromCurrentState:TRUE];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -kOFFSET_FOR_KEYBOARD, self.view.frame.size.width, self.view.frame.size.height);
        
        [UIView commitAnimations];
        
    }
    
    
}






- (void)textViewDidEndEditing:(UITextView *)textView;
{    if (fDeviceHeight<=568) {
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +kOFFSET_FOR_KEYBOARD, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (fDeviceHeight<=568) {
        
        UITouch * touch = [touches anyObject];
        if(touch.phase == UITouchPhaseBegan) {
            [_txtvwFeedback resignFirstResponder];
            
        }
    }
}



-(void)hideKeyboard
{
    [self.view endEditing:YES];
    
    // [self.txt_mobileNo resignFirstResponder];
}


-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.shouldRotate = NO;
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    // [self setNeedsStatusBarAppearanceUpdate];
    //  [self performSelector:@selector(setHeightOfTableView) withObject:nil afterDelay:.1];
    /* UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(backbtnAction:)];
     [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
     [self.navigationController.view addGestureRecognizer:gestureRecognizer];
     */
    
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    
    [_tblFeedback reloadData];
    [_tblFeedback setNeedsLayout ];
    [_tblFeedback layoutIfNeeded ];
    [_tblFeedback reloadData];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    [self setViewFont];
    [super viewWillAppear:NO];
    
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btnHelpback.titleLabel setFont:[AppFont regularFont:17.0]];
    [btnMoreBack.titleLabel setFont:[AppFont regularFont:17.0]];
    lblTitleReportIsse.font = [AppFont semiBoldFont:17.0];
    [_btnSubmit.titleLabel setFont:[AppFont regularFont:20.0]];
}

-(IBAction)backbtnAction:(id)sender
{
    // [self dismissViewControllerAnimated:NO completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

- (UIStatusBarStyle)preferredStatusBarStyle

{
    return UIStatusBarStyleDefault;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 3 ) {
        return 100.0;
    }
    else if ( indexPath.section == 4)
    {
        return 140.0;
    }
    else
    {
        if (indexPath.section==1||indexPath.section==2)//====
        {
            if (shouldHidden==YES) {
                return 0.0001;
            }
            else
            return 50;
        }
        return 50.0;
    }
    
    return 0;
}


#pragma mark - TableViewDataSource & Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    switch (section)
    {
        case 0:
        return arrChooseCategory.count;
        break;
        
        case 1:
        if (section != currentSelectedSection) {
            return  0;
        }
        else
        {
            return arrChooseDepartment.count;
        }
        break;
        
        case 2:
        if (section != currentSelectedSection) {
            return  0;
        }
        else
        {
            return arrChooseDepartmentService.count;
        }
        break;
        
        case 3:
        return 1;
        break;
        case 4:
        return 1;
        break;
        
        
        
        default:
        break;
    }
    return 0;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0:
        return 54;
        break;
        
        case 1:
        {// return 64;
            
            if (shouldHidden==YES) {
                return 0.0001;
            }
            else
            return 64;
        }
        break;
        
        case 2:
        {// return 64;
            
            if (shouldHidden==YES) {
                return 0.0001;
            }
            else
            return 64;
        }
        break;
        case 3:
        return 20;
        break;
        case 4:
        return 20;
        break;
        
        default:
        break;
        
    }
    return 0;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.001;
}






- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    static NSString *CellIdentifier = @"FeedbackHeaderCell";
    feedbackHeader = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (feedbackHeader == nil)
    {
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    feedbackHeader.sectionHeaderImage.tag = 100+section;
    [feedbackHeader.sectionHeaderImage addTarget:self action:@selector(btnSectionClicked:) forControlEvents:UIControlEventTouchUpInside];
    //-------- added by me--------
    [feedbackHeader.sectionHeaderbtnfull addTarget:self action:@selector(btnSectionClicked:) forControlEvents:UIControlEventTouchUpInside];
    feedbackHeader.sectionHeaderbtnfull.backgroundColor=[UIColor clearColor];
    feedbackHeader.sectionHeaderbtnfull.tag = 100+section;
    //-------- added by me--------
    feedbackHeader.sectionHeaderbtnfull.titleLabel.font = [AppFont regularFont:18.0];
    feedbackHeader.feedbackSectionHeader.font = [AppFont regularFont:16.0];
    if (section == 0)
    {
        feedbackHeader.feedbackSectionHeader.text = NSLocalizedString(@"choose_category", nil);
        feedbackHeader.sectionHeaderImage.hidden = YES;
        CGRect labelFrame =   feedbackHeader.feedbackSectionHeader.frame;
        labelFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tableView.frame) - 100 : 15;
        feedbackHeader.feedbackSectionHeader.frame = labelFrame;
        
        feedbackHeader.feedbackSectionHeader.textColor =[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0];
        feedbackHeader.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
        //  [feedbackHeader.vwHeader removeFromSuperview];
        
    }
    else if (section == 1)
    {
        
        if (shouldHidden==YES) {
            return nil;
        }
        UIView *vw = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
        [self.view addSubview:vw];
        vw.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
        
        feedbackHeader.sectionHeaderImage.hidden = NO;
        feedbackHeader.feedbackSectionHeader.text =header_dpt_txt;
        
        //        CGRect labelFrame =   feedbackHeader.feedbackSectionHeader.frame;
        //        labelFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tableView.frame) - 100 : 15;
        //        feedbackHeader.feedbackSectionHeader.frame = labelFrame;
        //
        //DragDrop image
        feedbackHeader.vwHeader.hidden = NO;
        
        if(currentSelectedSection == section)
        {
            [feedbackHeader.sectionHeaderImage setImage:[UIImage imageNamed:@"arrow_u"] forState:UIControlStateSelected];
        }
        else
        {
            
            [feedbackHeader.sectionHeaderImage setImage:[UIImage imageNamed:@"down_arrow"] forState:UIControlStateNormal];
            
        }
        feedbackHeader.backgroundColor = [UIColor whiteColor];
        
        
    }
    else if (section == 2)
    {
        
        if (shouldHidden==YES) {
            return nil;
        }
        feedbackHeader.vwForSection2.backgroundColor = [UIColor whiteColor];
        UIView *vw = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
        [self.view addSubview:vw];
        vw.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
        
        
        feedbackHeader.sectionHeaderImage.hidden = NO;
        // feedbackHeader.feedbackSectionHeader.text =NSLocalizedString(@"select_service", nil);
        feedbackHeader.feedbackSectionHeader.text =header_service_txt;
        
        
        //DragDrop image
        feedbackHeader.vwHeader.hidden = NO;
        
        if(currentSelectedSection == section)
        {
            [feedbackHeader.sectionHeaderImage setImage:[UIImage imageNamed:@"arrow_u"] forState:UIControlStateSelected];
        }
        else
        {
            
            [feedbackHeader.sectionHeaderImage setImage:[UIImage imageNamed:@"down_arrow"] forState:UIControlStateNormal];
            
        }
        feedbackHeader.backgroundColor = [UIColor whiteColor];
        
        
    }
    
    
    else if (section == 3)
    {
        feedbackHeader.feedbackSectionHeader.text = @"";
        feedbackHeader.sectionHeaderImage.hidden = YES;
        feedbackHeader.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
        feedbackHeader.vwHeader.hidden = YES;
    }
    
    
    
    
    
    return feedbackHeader;
}


-(void)btnSectionClicked:(UIButton*)btnSender
{
    NSInteger section = btnSender.tag - 100;
    
    
    if (section == 1) {
        if(currentSelectedSection == btnSender.tag - 100)
        {
            currentSelectedSection = -1;
            [_tblFeedback reloadData];
            return;
        }
        currentSelectedSection = btnSender.tag - 100;
        [_tblFeedback reloadData];
    }
    else{
        
    }
    if (section == 2) {
        if(currentSelectedSection == btnSender.tag - 100)
        {
            currentSelectedSection = -1;
            [_tblFeedback reloadData];
            return;
        }
        currentSelectedSection = btnSender.tag - 100;
        [_tblFeedback reloadData];
    }
    else{
        
    }
    
}




- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    text_user_feedback = _txtvwFeedback.text;
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return _txtvwFeedback.text.length + (text.length - range.length) <= 500;
    
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    NSLog(@"yeah inform someone of my change %@", textField.text);
    text_user_feedback=textField.text;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    return YES;
}




- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell1 forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([cell1 isKindOfClass:[FeedbackCell class]] )
    {
        
        FeedbackCell *feedCell = (FeedbackCell*)cell1;
        
        CGRect imgFeedbackFrame =  feedCell.imgFeedback.frame;
        imgFeedbackFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tableView.frame) - 70 : 15;
        feedCell.imgFeedback.frame = imgFeedbackFrame;
        
        
        
        
        //        feedCell.lblFeedback.text = [arrChooseCategory objectAtIndex:indexPath.row];
        feedCell.lblFeedback.textColor = [UIColor blackColor];
        feedCell.lblFeedback.font = [UIFont systemFontOfSize:14.0];
        
        feedCell.lblFeedback.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
        
        
        
        CGRect lblFeedbackFrame =  feedCell.lblFeedback.frame;
        lblFeedbackFrame.origin.x = singleton.isArabicSelected ? fDeviceWidth - 80 - lblFeedbackFrame.size.width : 50;
        feedCell.lblFeedback.frame = lblFeedbackFrame;
        
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        static NSString *CellIdentifier = @"FeedbackCell";
        
        FeedbackCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        
        
        cell.imgFeedback.image = [UIImage imageNamed:[arrChooseCateImage objectAtIndex:indexPath.row]];
        cell.lblFeedback.text = [arrChooseCategory objectAtIndex:indexPath.row];
        cell.lblFeedback.font = [AppFont regularFont:15.0];

        cell.lblFeedback.textColor = [UIColor blackColor];
        
        if(indexPath.row == selectedfirstIndexPath.row)
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        //        cell.lblFeedback.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
        //
        //        CGRect lblFeedbackFrame =  cell.lblFeedback.frame;
        //        lblFeedbackFrame.origin.x = singleton.isArabicSelected ? fDeviceWidth - 80 - lblFeedbackFrame.size.width : 50;
        //        cell.lblFeedback.frame = lblFeedbackFrame;
        
        
        
        
        cell.tintColor = [UIColor colorWithRed:(0.0/255.0) green:(122.0/255.0) blue:(255.0/255.0) alpha:1.0];
        
        
        return cell;
    }
    
    else if (indexPath.section == 1)
    {
        static NSString *CellIdentifier = @"FeedbackCell";
        
        FeedbackCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        
        
        
        cell.lblFeedback.text =[[arrChooseDepartment valueForKey:@"SERVICE_NAME"]objectAtIndex:indexPath.row];
         cell.lblFeedback.font = [AppFont regularFont:14.0];
        
        NSURL *url=[NSURL URLWithString:[[arrChooseDepartment valueForKey:@"SERVICE_IMAGE"]objectAtIndex:indexPath.row]];
        
        [cell.imgFeedback sd_setImageWithURL:url
                            placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
        
        cell.lblFeedback.font = [AppFont regularFont:14.0];
        if (fDeviceWidth<=568) {
            cell.lblFeedback.font = [AppFont regularFont:12.0];
        }
        
        
        
        if (indexPath == selectedIndexPath)
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        
        CGRect imgTypeFrame =  cell.imgFeedback.frame;
        imgTypeFrame.origin.x = singleton.isArabicSelected ? fDeviceWidth - 40: 15;
        cell.imgFeedback.frame = imgTypeFrame;
        
        
        CGRect labelType =  cell.lblFeedback.frame;
        labelType.origin.x = singleton.isArabicSelected ? fDeviceWidth - 50 - labelType.size.width : 50;
        cell.lblFeedback.frame = labelType;
        cell.lblFeedback.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
        
        
        
        
        
        
        cell.tintColor = [UIColor colorWithRed:(0.0/255.0) green:(122.0/255.0) blue:(255.0/255.0) alpha:1.0];
        
        return cell;
        
    }
    else if (indexPath.section == 2)
    {
        static NSString *CellIdentifier = @"FeedbackCell";
        
        FeedbackCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.lblFeedback.text =[[arrChooseDepartmentService valueForKey:@"serverName"]objectAtIndex:indexPath.row];
        
        
        
        cell.lblFeedback.font = [AppFont regularFont:14.0];
        if (fDeviceWidth<=568) {
            cell.lblFeedback.font = [AppFont regularFont:12.0];
        }
        
        if (indexPath == selectedIndexPath)
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        
        
        
        cell.imgFeedback.image=nil;
        
        CGRect labelType =  cell.lblFeedback.frame;
        labelType.origin.x = singleton.isArabicSelected ? fDeviceWidth - 15 - labelType.size.width : 15;
        cell.lblFeedback.frame = labelType;
        cell.lblFeedback.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
        
        
        return cell;
        
    }
    
    else if (indexPath.section == 3)
    {
        static NSString *CellIdentifier = nil;
        
        FeedbackTextCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[FeedbackTextCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        _txtvwFeedback = [[UITextView alloc] initWithFrame:CGRectMake(10,10,tableView.frame.size.width-20,80)];
        _txtvwFeedback.backgroundColor = [UIColor clearColor];
        _txtvwFeedback.placeholder = NSLocalizedString(@"your_text_here", nil);
        //_txtvwFeedback.inputAccessoryView = [self designToolBarForKeyboard];
        _txtvwFeedback.delegate=self;
        _txtvwFeedback.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
        [cell.contentView addSubview:_txtvwFeedback];
        _txtvwFeedback.text = text_user_feedback;
        _txtvwFeedback.font = [AppFont regularFont:14.0];
        
        return cell;
        
    }
    else {
        
        static NSString *CellIdentifier = nil;
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        UIButton *btnAddAttachement = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addShadowToTheView:btnAddAttachement];
        btnAddAttachement.frame = CGRectMake(10, 10, 80,120);
        [btnAddAttachement addTarget:self action:@selector(btnAddAttachmentTapped) forControlEvents:UIControlEventTouchUpInside];
        [btnAddAttachement setTitle:@"" forState:UIControlStateNormal];
        [btnAddAttachement setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btnAddAttachement setImage:[UIImage imageNamed:@"attachImg"] forState:UIControlStateNormal];
        
        
        [cell.contentView addSubview:btnAddAttachement];
        
        if (attachmentImage) {
            [btnAddAttachement setImage:attachmentImage forState:UIControlStateNormal];
            [btnAddAttachement setTitle:@"" forState:UIControlStateNormal];
        }
        
        return cell;
    }
}



-(void)addShadowToTheView:(UIView*)vwItem
{
    vwItem.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    vwItem.layer.shadowColor = [UIColor brownColor].CGColor;
    vwItem.layer.shadowRadius = 3;
    vwItem.layer.shadowOpacity = 0.5;
    vwItem.layer.cornerRadius = 3.0;
}

-(void)btnAddAttachmentTapped
{
    // arr_importtype
    arr_importtype=[[NSMutableArray alloc]init];
    
    [arr_importtype addObject:NSLocalizedString(@"choose_from_gallery", nil)];
    [arr_importtype addObject:NSLocalizedString(NSLocalizedString(@"take_photo", nil), nil)];
    
    
    
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"choose_image_from", nil)
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    // ObjC Fast Enumeration
    for (NSString *title in arr_importtype) {
        [actionSheet addButtonWithTitle:title];
    }
    
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:NSLocalizedString(@"cancel", nil)];
    
    
    
    [actionSheet showInView:self.view];
    
    
}



- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    
    if (buttonIndex == popup.cancelButtonIndex)
    {
        
        return;
        
    }
    
    //NSLog(@"buttonTitleAtIndex=%ld",(long)buttonIndex);
    
    //Gallery,Camera,Import From Facebook,Import From Google,Import From Twitter
    
    NSString *titletocheck=[arr_importtype objectAtIndex:buttonIndex];
    if ([titletocheck isEqualToString:NSLocalizedString(@"choose_from_gallery", nil)])
    {
        
        [self GalleryOpen];
    }
    if ([titletocheck isEqualToString:NSLocalizedString(NSLocalizedString(@"take_photo", nil), nil)])
    {
        [self CameraOpen];
        
        
        
    }
    
    
    
    
    
}








-(void)CameraOpen
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing=YES;
    
    imagePickerController.sourceType =  UIImagePickerControllerSourceTypeCamera;
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        
        
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            
            UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:imagePickerController];
            
            [popover presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
            
            
            
            
            
        } else {
            
            [self presentViewController:imagePickerController animated:NO completion:nil];
            
        }
        
    });
    
}


-(void)GalleryOpen
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing=YES;
    imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    dispatch_async(dispatch_get_main_queue(), ^ {
        
        
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
            
            UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:imagePickerController];
            
            [popover presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
            
        }
        else
        {
            [self presentViewController:imagePickerController animated:YES completion:nil];
        }
        
    });
}

- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    
    //this works, image is displayed
    // UIImage *beforeCrop = [editingInfo objectForKey:UIImagePickerControllerOriginalImage];
    
    attachmentImage=image;
    
    //this doesn't work, image is nil
    UIImage *afterCrop = [editingInfo objectForKey:UIImagePickerControllerEditedImage];
    
    
    //self.imgView_user.image=image;
    
    CGSize destinationSize = CGSizeMake(200, 200);
    UIGraphicsBeginImageContext(destinationSize);
    [afterCrop drawInRect:CGRectMake(0,0,destinationSize.width,destinationSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       base64img = [self encodeToBase64String:newImage];});
    
    [picker dismissViewControllerAnimated:YES completion:^{
        [_tblFeedback reloadData];
    }];
}
- (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}








-(UIToolbar*)designToolBarForKeyboard{
    
    if (numberToolbar == nil) {
        numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, fDeviceWidth, 50)];
        numberToolbar.barStyle = UIBarStyleBlackTranslucent;
        numberToolbar.items = @[
                                [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"done", nil)style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonClicked)]];
        [numberToolbar sizeToFit];
    }
    
    return numberToolbar;
}

-(void)doneButtonClicked{
    text_user_feedback = _txtvwFeedback.text;
    [_txtvwFeedback resignFirstResponder];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self doneButtonClicked];
    
    
    selectedIndexPath = indexPath;
    if (indexPath.section == 0)
    {
        selectedfirstIndexPath= indexPath;
        
        if (indexPath.row==0) {
            select_category_txt = @"appfd";
            
            shouldHidden=YES;
            header_dpt_txt=NSLocalizedString(@"choose_department", nil);
            
            
        }
        if (indexPath.row==1) {
            /*select_category_txt = @"departmentfd";
             shouldHidden=YES;
             header_dpt_txt=@"Choose Department";*/
            shouldHidden=NO;
            select_category_txt = @"departmentfd";
            
            
        }
        /* if (indexPath.row==2) {
         shouldHidden=NO;
         select_category_txt = @"appfd";
         
         
         }*/
        if (indexPath.row==2) {
            select_category_txt = @"otherfd";
            shouldHidden=YES;
            header_dpt_txt=NSLocalizedString(@"choose_department", nil);
            
            
        }
        
    }
    /*  {
     cate = appfd;
     email = "";
     feedback = "Test Case ";
     sid = 11;
     st = "";
     }*/
    
    
    
    if (indexPath.section == 1)
    {
        
        //        if (arrChooseDepartment == nil) {
        //            arrChooseDepartment = [NSMutableArray new];
        //        }
        //        else{
        //            [arrChooseDepartment removeAllObjects];
        //        }
        //
        select_dept_txt=[[arrChooseDepartment valueForKey:@"SERVICE_NAME"]objectAtIndex:indexPath.row];
        //select_dept_txt=[arrChooseDepartment objectAtIndex:indexPath.row];
        
        select_dept_txt=[[arrChooseDepartment valueForKey:@"SERVICE_NAME"]objectAtIndex:indexPath.row];
        serviceid=[NSString stringWithFormat:@"%@",[[arrChooseDepartment valueForKey:@"SERVICE_ID"] objectAtIndex:indexPath.row]];
        
        header_dpt_txt=select_dept_txt;
        currentSelectedSection = -1;
        [self hitAPIForDeptService:serviceid];
        header_service_txt= NSLocalizedString(@"choose_service", nil);
        
    }
    
    if (indexPath.section == 2)
    {
        //        if (arrChooseDepartment == nil) {
        //            arrChooseDepartment = [NSMutableArray new];
        //        }
        //        else{
        //            [arrChooseDepartment removeAllObjects];
        //        }
        
        
        select_serviceID_txt=[[arrChooseDepartmentService valueForKey:@"serverName"]objectAtIndex:indexPath.row];
        serviceIDtoPass=[NSString stringWithFormat:@"%@",[[arrChooseDepartmentService valueForKey:@"serviceID"] objectAtIndex:indexPath.row]];
        header_service_txt=select_serviceID_txt;
        
        currentSelectedSection = -1;
        //[self hitAPIForDeptService:serviceid];
    }
    
    [_tblFeedback reloadData];
}




-(void)hitAPIForDeptService:(NSString*)departmentID
{
    
    
    @autoreleasepool {
        
        
        UMAPIManager *objRequest = [[UMAPIManager alloc] init];
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        // Set the label text.
        hud.label.text = NSLocalizedString(@"loading",nil);        if ([base64img length]==0) {
            base64img=@"";
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           
                           NSMutableDictionary *dictBody = [NSMutableDictionary new];
                           
                           [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
                           [dictBody setObject:departmentID forKey:@"departmentID"];
                           
                           //  singleton.mobileNumber=txtMobileNumber.text; //save mobile number for future use of user
                           
                           
                           [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_FETCH_FEED_SERVICELIST withBody:dictBody andTag:TAG_REQUEST_FTHDS completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
                            {
                                [hud hideAnimated:YES];
                                
                                if (error == nil) {
                                    NSLog(@"Server Response = %@",response);
                                    
                                    //----- below value need to be forword to next view according to requirement after checking Android apk-----
                                    
                                    
                                    NSString *rc=[response valueForKey:@"rc"];//API0114
                                    // NSString *rd=[response valueForKey:@"rd"];
                                    //NSString *rs=[response valueForKey:@"rs"];//S
                                    
                                    // pd
                                    if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1]||[rc isEqualToString:@"API0114"])
                                    {
                                        
                                        arrChooseDepartmentService=[NSMutableArray new];
                                        
                                        
                                        NSDictionary *responsedic=(NSDictionary*)[response valueForKey:@"pd"];
                                        
                                        /*{
                                         serviceDataList =     (
                                         {
                                         serverName = "Approved Institutions";
                                         serviceID = 405;
                                         }
                                         );
                                         }
                                         
                                         {
                                         serverName = "Vidyalaya Darshika";
                                         serviceID = 401;
                                         }
                                         
                                         */
                                        
                                        arrChooseDepartmentService=(NSMutableArray*)[responsedic valueForKey:@"serviceDataList"];;
                                        
                                        
                                        
                                        
                                    }
                                    
                                }
                                else{
                                    NSLog(@"Error Occured = %@",error.localizedDescription);
                                    
                                    
                                    NSString *errormsg=[NSString stringWithFormat:@"%@",error.localizedDescription];
                                    [self showToast:errormsg];
                                    
                                }
                                
                            }];
                           
                       });                }
}




-(void)showToast :(NSString *)toast
{
    [self.view makeToast:toast duration:5.0 position:CSToastPositionBottom];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)btnSubmitClicked:(id)sender
{
    
    
    if ([select_category_txt length]==0) {
        // select_category_txt
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:@"Choose your category first!" delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
        [alert show];
        
    }
    
    
    
    if ([text_user_feedback isEqualToString:NSLocalizedString(@"your_text_here", nil)]||[text_user_feedback length]==0)
    {
        // select_category_txt
        
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"feedback_blank", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
        [alert show];
    }
    
    /*else if ([text_user_feedback isEqualToString:NSLocalizedString(@"your_text_here", nil)]||[text_user_feedback length]<10)
     {
     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"please_enter_feedback", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
     [alert show];
     }
     */
    
    
    
    else if (shouldHidden==NO)
    {
        if ([select_dept_txt length]==0) {
            // select_category_txt
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"please_choose_department", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
            [alert show];
        }
        
        else if ([select_serviceID_txt length]==0||[header_service_txt isEqualToString: NSLocalizedString(@"choose_service", nil)])
        {
            // select_category_txt
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"please_choose_service", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
            [alert show];
        }
        else
        [self hitAPI];
    }
    
    else
    {
        [self hitAPI];
    }
}

-(void)hitAPI
{
    
    
    
    @autoreleasepool {
        
        
        UMAPIManager *objRequest = [[UMAPIManager alloc] init];
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        // Set the label text.
        hud.label.text = NSLocalizedString(@"loading",nil);
        
        if ([base64img length]==0) {
            base64img=@"";
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           
                           NSMutableDictionary *dictBody = [NSMutableDictionary new];
                           
                           [dictBody setObject:text_user_feedback forKey:@"feedback"];
                           [dictBody setObject:select_category_txt forKey:@"cate"];
                           [dictBody setObject:@"issue" forKey:@"catetype"];
                           [dictBody setObject:base64img forKey:@"pic"];
                           [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
                           [dictBody setObject:@"" forKey:@"email"];
                           [dictBody setObject:@"" forKey:@"st"];
                           [dictBody setObject:serviceid forKey:@"sid"];
                           [dictBody setObject:serviceIDtoPass forKey:@"serviceID"];
                           
                           //  singleton.mobileNumber=txtMobileNumber.text; //save mobile number for future use of user
                           
                           
                           [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_FEEDBACK withBody:dictBody andTag:TAG_REQUEST_FEEDBACK completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
                            {
                                [hud hideAnimated:YES];
                                
                                if (error == nil) {
                                    NSLog(@"Server Response = %@",response);
                                    
                                    //----- below value need to be forword to next view according to requirement after checking Android apk-----
                                    NSString *man=[[response valueForKey:@"pd"] valueForKey:@"man"];
                                    NSString *rtry=[[response valueForKey:@"pd"] valueForKey:@"rtry"];
                                    NSString *tmsg=[[response valueForKey:@"pd"] valueForKey:@"tmsg"];
                                    NSString *tout=[[response valueForKey:@"pd"] valueForKey:@"tout"];
                                    NSString *wmsg=[[response valueForKey:@"pd"] valueForKey:@"wmsg"];
                                    
                                    NSString *rc=[response valueForKey:@"rc"];
                                    NSString *rd=[response valueForKey:@"rd"];
                                    NSString *rs=[response valueForKey:@"rs"];
                                    
                                    NSLog(@"value of man =%@ \n value of rtry=%@ \n value of tmsg=%@ \n value of tout=%@ \n value of wmsg=%@ \n value of rc=%@ \n value of rd=%@ \n value of rs=%@",man,rtry,tmsg,tout,wmsg,rc,rd,rs);
                                    //----- End value need to be forword to next view according to requirement after checking Android apk-----
                                    
                                    if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                                    {
                                        [self alertwithMsg:rd];
                                        
                                        
                                        //                singleton.mobileNumber=txtMobileNumber.text;
                                        //
                                        //
                                        //
                                        //                RegStep2ViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"RegStep2ViewController"];
                                        //                [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                                        //                [self presentViewController:vc animated:YES completion:nil];
                                        //                vc.lblScreenTitleName.text = @"Mobile Number Verification";
                                        //                vc.TYPE_LOGIN_CHOOSEN = ISFROMLOGINWITHOTP;
                                    }
                                    
                                }
                                else{
                                    NSLog(@"Error Occured = %@",error.localizedDescription);
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                                                    message:error.localizedDescription
                                                                                   delegate:self
                                                                          cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                                          otherButtonTitles:nil];
                                    [alert show];
                                    
                                }
                                
                            }];
                           
                       });                }
}

-(void)alertwithMsg:(NSString*)msg
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                   {
                                       
                                       [self backbtnAction:self];
                                   }];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*
 #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }
 */

@end
