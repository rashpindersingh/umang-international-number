
//
//  ChangeRegMobMpinVC.m
//  Umang
//
//  Created by admin on 09/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "ChangeRegMobMpinVC.h"

#define kOFFSET_FOR_KEYBOARD 80.0
#import "JKLLockScreenViewController.h"
#import "MyTextField.h"
#import "MBProgressHUD.h"
#import "UMAPIManager.h"
#import "EnterNewMobNumVC.h"


@interface ChangeRegMobMpinVC ()<MyTextFieldDelegate,UITextFieldDelegate,JKLLockScreenViewControllerDataSource, JKLLockScreenViewControllerDelegate>
{
    MBProgressHUD *hud ;
    SharedManager *singleton;
    IBOutlet UIView *vw_base;
    NSString *typeString;
    JKLLockScreenViewController *viewController;
    
}
@property (nonatomic, strong) NSString * enteredPincode;

@property (weak, nonatomic) IBOutlet UITextField *txt_pin1;
@property (weak, nonatomic) IBOutlet UITextField *txt_pin2;
@property (weak, nonatomic) IBOutlet UITextField *txt_pin3;
@property (weak, nonatomic) IBOutlet UITextField *txt_pin4;

@property (weak, nonatomic) IBOutlet UIView *vw_txtcontain;



@end

@implementation ChangeRegMobMpinVC

@synthesize dic_info;
@synthesize user_img;

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField setFont:[UIFont systemFontOfSize:24]];
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField.textAlignment = NSTextAlignmentCenter;
    
    [textField resignFirstResponder];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField setFont:[UIFont systemFontOfSize:24]];
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField.textAlignment = NSTextAlignmentCenter;
    
    [textField resignFirstResponder];
    
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self callMpinVCWithType:0];
    [_txt_pin1 becomeFirstResponder];
    singleton = [SharedManager sharedSingleton];
    typeString=@"";
    // self.btn_next.enabled=NO;
    self.view.userInteractionEnabled = YES;
    self.view.backgroundColor = [UIColor whiteColor] ;
    [self addShadowToTheView:self.view];
    [self addShadowToTheView:vw_base];
    _txt_pin1.delegate=self;
    _txt_pin2.delegate=self;
    _txt_pin3.delegate=self;
    _txt_pin4.delegate=self;
    
    [_txt_pin1 setFont:[UIFont systemFontOfSize:24]];
    [_txt_pin2 setFont:[UIFont systemFontOfSize:24]];
    [_txt_pin3 setFont:[UIFont systemFontOfSize:24]];
    [_txt_pin4 setFont:[UIFont systemFontOfSize:24]];
    //self.view.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.8];
    
    if ([[dic_info valueForKey:@"TAG"] isEqualToString:@"fingerprint"])
    {
        NSLog(@"Coming from finger print screen");
        self.cancelButton.hidden = YES;
    }
    else
    {
        self.cancelButton.hidden = NO;
    }
    
    // Do any additional setup after loading the view.
}


-(void)addShadowToTheView:(UIView*)vwItem
{
    vwItem.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    vwItem.layer.shadowColor = [UIColor brownColor].CGColor;
    vwItem.layer.shadowRadius = 3;
    vwItem.layer.shadowOpacity = 0.5;
    vwItem.layer.cornerRadius = 3.0;
}


-(IBAction)keyPress:(UIButton *)sender
{
    int tag=(int)[sender tag];
    
    
    
    
    
    
    if ([typeString length]<4 || tag==111)
    {
        
        
        switch (tag) {
            case 101:
            {
                typeString=[NSString stringWithFormat:@"%@1",typeString];
                
            }
                break;
            case 102:
            {
                typeString=[NSString stringWithFormat:@"%@2",typeString];
                
            }
                break;
            case 103:
            {
                typeString=[NSString stringWithFormat:@"%@3",typeString];
                
            }
                break;
            case 104:
            {
                typeString=[NSString stringWithFormat:@"%@4",typeString];
                
            }
                break;
            case 105:
            {
                typeString=[NSString stringWithFormat:@"%@5",typeString];
                
            }
                break;
            case 106:
            {
                typeString=[NSString stringWithFormat:@"%@6",typeString];
                
            }
                break;
            case 107:
            {
                typeString=[NSString stringWithFormat:@"%@7",typeString];
                
            }
                break;
            case 108:
            {
                typeString=[NSString stringWithFormat:@"%@8",typeString];
                
            }
                break;
            case 109:
            {
                typeString=[NSString stringWithFormat:@"%@9",typeString];
                
            }
                break;
            case 110:
            {
                typeString=[NSString stringWithFormat:@"%@0",typeString];
                
            }
                break;
            case 111:
            {
                [self deleteDigit];
            }
                break;
                
                
            default:
                break;
        }
        
        
        //NSLog(@"value =%@",typeString);
        [self setNumber:typeString];
    }
    else
    {
        
        //  [self shake:_vw_txtcontain];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (!textField.secureTextEntry) {
        return YES;
    }
    
    textField.text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (textField.text.length > 0) {
        textField.font = [UIFont systemFontOfSize:24.0f];
    } else {
        textField.font = [UIFont systemFontOfSize:24.0f];
    }
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField.textAlignment = NSTextAlignmentCenter;
    
    return NO;
}

-(void)setNumber:(NSString*)typString
{
    
    
    
    int length=(int)[typeString length];
    
    switch (length) {
        case 1:
        {
            [_txt_pin1 becomeFirstResponder];
            _txt_pin1.text = [NSString stringWithFormat:@"%c",[typeString characterAtIndex:0]];
            [_txt_pin1 setFont:[UIFont systemFontOfSize:24]];
            [_txt_pin1 resignFirstResponder];
            
            
        }
            break;
        case 2:
        {
            [_txt_pin2 becomeFirstResponder];
            _txt_pin2.text = [NSString stringWithFormat:@"%c",[typeString characterAtIndex:1]];
            [_txt_pin2 setFont:[UIFont systemFontOfSize:24]];
            [_txt_pin2 resignFirstResponder];
            
        }
            break;
            
        case 3:
        {
            [_txt_pin3 becomeFirstResponder];
            
            _txt_pin3.text = [NSString stringWithFormat:@"%c",[typeString characterAtIndex:2]];
            
            [_txt_pin3 setFont:[UIFont systemFontOfSize:24]];
            [_txt_pin3 resignFirstResponder];
            
            
        }
            break;
            
        case 4:
        {            [_txt_pin4 becomeFirstResponder];
            _txt_pin4.text = [NSString stringWithFormat:@"%c",[typeString characterAtIndex:3]];
            
            [_txt_pin4 setFont:[UIFont systemFontOfSize:24]];
            [_txt_pin4 resignFirstResponder];
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               [self checkValidation];
                           });
            
            
        }
            // [self checkValidation];
            break;
            
            
        default:
            break;
    }
    
    [self deleteback:length];
    
    
}



-(void)shake:(UIView*)view
{
    typeString=@"";
    if (viewController != nil) {
        NSUInteger x = 0;
        for (x = 0; x<4; x++) {
            [viewController.pincodeView removeLastPincode];
        }
    }
    
    view.transform = CGAffineTransformMakeTranslation(20, 0);
    [UIView animateWithDuration:0.4 delay:0.0 usingSpringWithDamping:0.2 initialSpringVelocity:1.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        view.transform = CGAffineTransformIdentity;
        
        
    } completion:nil];
}

-(void)deleteback:(int)length
{
    if (length==0)
    {
        _txt_pin1.text=@"";
        _txt_pin2.text=@"";
        _txt_pin3.text=@"";
        _txt_pin4.text=@"";
    }
    if (length==1)
    {
        _txt_pin2.text=@"";
        _txt_pin3.text=@"";
        _txt_pin4.text=@"";
    }
    if (length==2)
    {
        _txt_pin3.text=@"";
        _txt_pin4.text=@"";
    }
    if (length==3)
    {
        _txt_pin4.text=@"";
    }
    
}



- (void)deleteDigit
{
    if ([typeString length]<=4 &&[typeString length]>0)
    {
        NSString * current = typeString;
        NSString * new = [current substringToIndex:[current length] - 1];
        if ([new length] > 0)
        {
            typeString = new;
        }
        else
        {
            typeString = @"";
        }
        
    }
    //NSLog(@"after delete value =%@",typeString);
    [self setNumber:typeString];
    
}




-(void)checkValidation

{
    if ([typeString length]<4) {
        /* UIAlertView *errorAlert = [[UIAlertView alloc]
         initWithTitle:@"Error" message:@"MPIN must be 4 digit!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
         [errorAlert show];
         */
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           
                           [self hitAPI];
                           //hit api
                       });
        
        
    }
}


//----- hitAPI for Validate MPIN ------
-(void)hitAPI
{
    NSString *encryptedInputID = @"";
    NSString *strSaltMPIN = SaltMPIN;//[[SharedManager sharedSingleton] getKeyWithTag:KEYCHAIN_SaltMPIN];

    NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",typeString ,strSaltMPIN];
    encryptedInputID=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:@"" forKey:@"peml"];
    [dictBody setObject:encryptedInputID forKey:@"mpin"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:@"" forKey:@"lang"];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.label.text = NSLocalizedString(@"loading",nil);
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_VMPIN withBody:dictBody andTag:TAG_REQUEST_VMPIN completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            
            //            NSString *rc=[response valueForKey:@"rc"];
            //            NSString *rd=[response valueForKey:@"rd"];
            //            NSString *rs=[response valueForKey:@"rs"];
            
            
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                
                if ([[dic_info valueForKey:@"TAG"] isEqualToString:@"fingerprint"])
                {
                    [self dismissViewControllerAnimated:YES completion:nil];
                }
                else
                {
                    //self.cancelButton.hidden = NO;
                    [self jumptoEnterRegMobile];
                }
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            typeString=@"";
            _txt_pin1.text=@"";
            _txt_pin2.text=@"";
            _txt_pin3.text=@"";
            _txt_pin4.text=@"";
            [self shake:_vw_txtcontain];
            
        }
        
    }];
    
    
}

-(void)jumptoEnterRegMobile
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    EnterNewMobNumVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"EnterNewMobNumVC"];
    // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    vc.userenterMPIN=typeString;
    [self.navigationController pushViewController:vc animated:YES];
}

/*-(void)closeEditProfile
 {
 if ([[dic_info valueForKey:@"Tag"] isEqualToString:@"UPDATE"])
 {
 
 NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
 
 
 for (UIViewController *aViewController in allViewControllers)
 {
 if ([aViewController isKindOfClass:[UserProfileVC class]])
 {
 // [aViewController removeFromParentViewController];
 [self.navigationController popToViewController:aViewController
 animated:YES];
 [self closeView];
 
 }
 }
 
 }
 
 
 }
 */


- (IBAction)btn_close_Action:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}




-(IBAction)doneClicked:(id)sender
{
    //    [self checkValidation];
    
}



-(void)callMpinVCWithType:(NSUInteger)type {
    
    viewController = [[UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil] instantiateViewControllerWithIdentifier:@"JKLLockScreenViewController"] ;
    [viewController setLockScreenMode:LockScreenModeNormal];    // enum { LockScreenModeNormal, LockScreenModeNew, LockScreenModeChange }
    [viewController setDelegate:self];
    [viewController setDataSource:self];
    [viewController setTintColor:[UIColor whiteColor]];
    // [self presentViewController:viewController animated:NO completion:NULL];
    [self addChildViewController:viewController];
    [self.view addSubview:viewController.view];
    [self.view bringSubviewToFront:viewController.view];
    [viewController didMoveToParentViewController:self];
    
    
}
#pragma mark -
#pragma mark YMDLockScreenViewControllerDelegate
- (void)unlockWasCancelledLockScreenViewController:(JKLLockScreenViewController *)lockScreenViewController {
    [self.navigationController popViewControllerAnimated:YES];
    [viewController.view removeFromSuperview];
    [viewController removeFromParentViewController];
    NSLog(@"LockScreenViewController dismiss because of cancel");
}

- (void)unlockWasSuccessfulLockScreenViewController:(JKLLockScreenViewController *)lockScreenViewController pincode:(NSString *)pincode {
    
    typeString = pincode;
    [self checkValidation];
}

#pragma mark -
#pragma mark YMDLockScreenViewControllerDataSource
- (BOOL)lockScreenViewController:(JKLLockScreenViewController *)lockScreenViewController pincode:(NSString *)pincode {
    
#ifdef DEBUG
    NSLog(@"Entered Pincode : %@", self.enteredPincode);
#endif
    
    return [typeString isEqualToString:pincode];
}

- (BOOL)allowTouchIDLockScreenViewController:(JKLLockScreenViewController *)lockScreenViewController {
    
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

