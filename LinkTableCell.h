//
//  LinkTableCell.h
//  Umang
//
//  Created by admin on 27/03/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LinkTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgLinkCell;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleDescrition;

@end
