//
//  DigiLockOtpVerifyVC.h
//  Umang
//
//  Created by admin on 06/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DigiLockOtpVerifyVC : UIViewController
{
    IBOutlet UIView *resendOTPview;
    IBOutlet UIScrollView *scrollView;
    
    
}
@property(nonatomic,strong) NSString *aadharNumber;
@property(nonatomic,strong) NSString *retryCount;
@property(nonatomic,assign) NSInteger timeOut;
@property(nonatomic,retain)NSString *maskedMobileNumber;
@property(nonatomic,retain)NSString *maskedEmailNumber;

@property (weak, nonatomic) IBOutlet UILabel *lblOTPRegister;

@property (weak, nonatomic) IBOutlet UILabel *lblRetry;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UITextField *txtFldOTP;

- (IBAction)btnResendOTPTapped:(id)sender;

@end
