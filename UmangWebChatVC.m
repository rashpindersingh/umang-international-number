//
//  UmangWebChatVC.m
//  Umang
//
//  Created by admin on 23/02/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "UmangWebChatVC.h"
#import "UIImageView+WebCache.h"
#import "UMAPIManager.h"
#import "NSString+MD5.h"
#import "MBProgressHUD.h"
#import "UIView+Toast.h"
#import "StateList.h"
#import "FAQWebVC.h"
//#import "NSData+MD5.h"
#import <CommonCrypto/CommonDigest.h>
#import "NSData+Base64.h"

@interface UmangWebChatVC ()
{
    
    SharedManager*  singleton ;
    MBProgressHUD *hud;
    NSData* data ;
    NSURL *postURI;
    
    //------------profile parameter---------------
    //----- local values  to be save and show from API-----------
    NSString *str_name;
    NSString *str_gender;
    NSString *str_dob;
    NSString *str_qualification;
    NSString *str_occupation;
    NSString *str_state;
    NSString *str_district;
    NSString *str_registerMb;
    NSString *str_emailAddress;
    NSString *str_alternateMb;
    NSString *str_emailVerifyStatus;
    NSString *str_amnosVerifyStatus;
    NSString *str_address;
    
    __weak IBOutlet UIButton *btnBack;
    NSString *str_Url_pic;
    
    NSMutableArray *socialpd;
    
    
    
    StateList *obj;
    IBOutlet UILabel *lbltitle;
    
    NSMutableArray *statelocalPlist;
    //------------End parameter--------
    bool flagViewAppear;
    
    BOOL flag_chat;
    
    UIActivityIndicatorView *activityIndicator ;
    
}
@property(nonatomic,retain)NSString *str_gender;
@property(nonatomic,retain)NSString *urlString;
@property(nonatomic,retain)IBOutlet UIWebView *webView;
@property(nonatomic,retain)NSString*service_ID;


@end

@implementation UmangWebChatVC
@synthesize service_ID;
@synthesize webView;
@synthesize str_gender;
@synthesize urlString;
- (void)viewDidUnload
{
    self.webView = nil;
    
    [super viewDidUnload];
}

/*- (NSString *)hmacsha1:(NSString *)data1 secret:(NSString *)key
 {
 
 const char *cKey  = [key cStringUsingEncoding:NSASCIIStringEncoding];
 const char *cData = [data1 cStringUsingEncoding:NSASCIIStringEncoding];
 
 unsigned char cHMAC[CC_SHA1_DIGEST_LENGTH];
 
 CCHmac(kCCHmacAlgSHA1, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
 
 NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
 
 NSString *hash = [HMAC base64EncodedString];
 
 return hash;
 }
 */

- (NSString *)hmacsha1:(NSString *)data1 secret:(NSString *)key
{
    NSString*tempKey=[key mutableCopy];
    NSString*tempdata=[data1 mutableCopy];
    const char *cKey  = [tempKey cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [tempdata cStringUsingEncoding:NSASCIIStringEncoding];
    if(cKey == NULL ||[tempKey length]==0)
    {
        cKey  = [tempKey UTF8String];
    }
    if(cData == NULL ||[tempdata length]==0)
    {
        cData  = [tempdata UTF8String];
    }
    unsigned char cHMAC[CC_SHA1_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA1, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    //NSString *hash = [HMAC base64EncodedString];
    NSString *hash = [HMAC base64EncodedStringWithOptions:0];
    return hash;
}


-(NSMutableDictionary*)getCommonParametersForRequestBody
{
    
    SharedManager *objShared = [SharedManager sharedSingleton];
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    
    //    // Mobile Number
    //    [dict setObject:@"8000000000" forKey:@"mno"];
    // IMEI
    //[dict setObject:[objShared appUniqueID] forKey:@"imei"];
    [dict setObject:@"" forKey:@"imei"];
    
    // Device ID
    
    
    [dict setObject:[objShared appUniqueID] forKey:@"did"];
    
    // IMSI
    // [dict setObject:[objShared appUniqueID] forKey:@"imsi"];
    [dict setObject:@"" forKey:@"imsi"];
    
    // Handset Make
    [dict setObject:@"Apple" forKey:@"hmk"];
    
    // Handset Model
    [dict setObject:[[UIDevice currentDevice] model] forKey:@"hmd"];
    
    
    
    
    // Rooted
    //[dict setObject:[self isJailbroken]?@"yes":@"no" forKey:@"rot"];
    [dict setObject: [objShared.dbManager isLibertyPatchJailBroken]?@"yes":@"no" forKey:@"rot"];

    // OS
    [dict setObject:@"ios" forKey:@"os"];
    
    // ver
    [dict setObject:[SharedManager appVersion] forKey:@"ver"];
    
    // Country Code
    [dict setObject:@"404" forKey:@"mcc"];
    
    // Mobile Network Code
    [dict setObject:@"02" forKey:@"mnc"];
    
    // Lcoation Area Code
    [dict setObject:@"2065" forKey:@"lac"];
    
    // Cell ID
    [dict setObject:@"11413" forKey:@"clid"];
    
    
    // Location Based Params
    // Accuracy
    [dict setObject:@"" forKey:@"acc"];
    
    // Latitude Code
    [dict setObject:@"" forKey:@"lat"];
    
    // Longitude Code
    [dict setObject:@"" forKey:@"lon"];
    
    // State Name
    //[dict setObject:@"" forKey:@"st"];
    
    
    // Selected Language Name
    /*  NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
     if (selectedLanguage == nil) {
     selectedLanguage = @"en";
     }
     
     selectedLanguage = @"en";
     
     [dict setObject:selectedLanguage forKey:@"lang"];*/
    
    
    NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if (selectedLanguage == nil)
    {
        selectedLanguage = @"en";
    }
    
    else{
        NSArray *arrTemp = [selectedLanguage componentsSeparatedByString:@"-"];
        selectedLanguage = [arrTemp firstObject];
    }
    /* bool checklang=[singleton.dbManager getServiceLanguage:service_ID withDeviceLang:selectedLanguage];
     
     NSString *language=@"";
     if (checklang==true)
     {
     language=selectedLanguage; //add condition for it
     
     }
     else
     {
     language=@"en";
     }*/
    
    
    //NSLog(@"Applied Selected Language = %@",selectedLanguage);
    
    [dict setObject:selectedLanguage forKey:@"lang"];
    
    //[dict setObject:@"en" forKey:@"lang"];
    NSString *node = [[NSUserDefaults standardUserDefaults] objectForKey:@"NODE_KEY"];
    
    if (node == nil)
    {
        node =@"";
    }
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [dict setObject:node forKey:@"node"];
    //-------- Add Sharding logic here-------
    // Selected Mode (Domain(Web / App/Mobile Web)
    [dict setObject:@"app" forKey:@"mod"];
    
    
    return dict;
}

-(BOOL)isJailbroken {
    NSURL* url = [NSURL URLWithString:@"cydia://package/com.example.package"];
    return [[UIApplication sharedApplication] canOpenURL:url];
}


/*- (BOOL)isJailbroken
 {
 BOOL jailbroken = NO;
 NSArray *jailbrokenPath = [NSArray arrayWithObjects:@"/Applications/Cydia.app",  @"/Applications/RockApp.app",  @"/Applications/Icy.app",  @"/usr/sbin/sshd",  @"/usr/bin/sshd",  @"/usr/libexec/sftp-server",  @"/Applications/WinterBoard.app",  @"/Applications/SBSettings.app",  @"/Applications/MxTube.app",  @"/Applications/IntelliScreen.app",  @"/Library/MobileSubstrate/DynamicLibraries/Veency.plist",  @"/Applications/FakeCarrier.app",  @"/Library/MobileSubstrate/DynamicLibraries/LiveClock.plist",  @"/private/var/lib/apt",  @"/Applications/blackra1n.app",  @"/private/var/stash",  @"/private/var/mobile/Library/SBSettings/Themes",  @"/System/Library/LaunchDaemons/com.ikey.bbot.plist",  @"/System/Library/LaunchDaemons/com.saurik.Cydia.Startup.plist",  @"/private/var/tmp/cydia.log",  @"/private/var/lib/cydia", nil];for(NSString *string in jailbrokenPath)
 {
 if ([[NSFileManager defaultManager] fileExistsAtPath:string]){
 jailbroken = YES;
 break;}
 }
 return jailbroken;
 }*/




-(IBAction)backBtnAction:(id)sender
{
    self.webView = nil;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated

{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [super viewWillAppear:NO];
    
    
}


-(void)openchat
{
    NSString *javascriptString = [NSString stringWithFormat:@"openChat();"];
    // [self.webView stringByEvaluatingJavaScriptFromString:javascriptString];
    [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:javascriptString waitUntilDone:NO];
}
- (void)viewDidLoad {
    
    activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);
    [self.view addSubview: activityIndicator];
    
    
    
    
    
    singleton = [SharedManager sharedSingleton];
    flag_chat=TRUE;
    obj=[[StateList alloc]init];
    [obj hitStateQualifiAPI];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        // [self hitFetchProfileAPI];
        
    });
    webView.delegate = self;
    webView.scalesPageToFit = YES;
    flagViewAppear=TRUE;
    
    [self hitFetchProfileAPI];
    
    lbltitle.text =NSLocalizedString(@"customer_support", nil);
    [_backBtnHome setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        // [btnBack sizeToFit];
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:_backBtnHome.titleLabel.font.pointSize]}];
        
        //or whatever font you're using
        CGRect framebtn=CGRectMake(_backBtnHome.frame.origin.x, _backBtnHome.frame.origin.y, _backBtnHome.frame.size.width, _backBtnHome.frame.size.height);
        
        
        [_backBtnHome setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        _backBtnHome.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    // urlString=[dic_serviceInfo valueForKey:@"URL"];
    
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)hitFetchProfileAPI
{
    //hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    // hud.label.text = NSLocalizedString(@"Please wait...", @"Please wait...");
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"lang"];//lang is Status of the account
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [dictBody setObject:@"" forKey:@"st"];  //get from mobile default email //not supported iphone
    [dictBody setObject:@"both" forKey:@"type"];  //get from mobile default email //not supported iphone
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_VIEW_PROFILE withBody:dictBody andTag:TAG_REQUEST_VIEW_PROFILE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        // [hud hideAnimated:YES];
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            //  NSString *rc=[response valueForKey:@"rc"];
            //  NSString *rs=[response valueForKey:@"rs"];
            
            //NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            //NSString *rd=[response valueForKey:@"rd"];
            
            
            NSString *responseStr=[NSString stringWithFormat:@"%@",response];
            if ([responseStr length]>0)
            {
                
                
                if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                    
                {
                    
                    @try {
                        
                        
                        //singleton.user_tkn=tkn;
                        NSMutableArray *aadharpd=[[NSMutableArray alloc]init];
                        aadharpd=[[response valueForKey:@"pd"]valueForKey:@"aadharpd"];
                        
                        
                        NSMutableArray *generalpd=[[NSMutableArray alloc]init];
                        generalpd=[[response valueForKey:@"pd"]valueForKey:@"generalpd"];
                        
                        
                        
                        socialpd=[[NSMutableArray alloc]init];
                        
                        singleton.user_id=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"uid"];
                        //-------- Add later----------
                        singleton.shared_ntft=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntft"];
                        singleton.shared_ntfp=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntfp"];
                        //-------- Add later----------
                        
                        
                        
                        if ([singleton.user_id length]==0) {
                            singleton.user_id=@"";
                        }
                        //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        
                        // [defaults setObject:singleton.user_id forKey:@"USER_ID"];
                        
                        //[defaults synchronize];
                        
                        
                        //------------------------- Encrypt Value------------------------
                        
                        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                        // Encrypt
                        [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_id withKey:@"USER_ID"];
                        [[NSUserDefaults standardUserDefaults]synchronize];
                        //------------------------- Encrypt Value------------------------
                        
                        
                        //NSString *identifier =( [[response valueForKey:@"pd"]valueForKey:@"socialpd"];
                        socialpd=(NSMutableArray *)[[response valueForKey:@"pd"]valueForKey:@"socialpd"];
                        
                        
                        
                        //----- local values  to be save and show from API-----------
                        
                        
                        
                        // NSString * sid=[generalpd valueForKey:@"sid"];
                        
                        str_name=[generalpd valueForKey:@"nam"];
                        str_registerMb =[generalpd valueForKey:@"mno"];
                        str_alternateMb=[generalpd valueForKey:@"amno"];
                        //str_city=[generalpd valueForKey:@"cty"];
                        str_state=[generalpd valueForKey:@"st"];
                        
                        // state_id=[obj getStateCode:str_state];
                        
                        
                        str_district=[generalpd valueForKey:@"dist"];
                        str_dob=[generalpd valueForKey:@"dob"];
                        str_gender=[generalpd valueForKey:@"gndr"];
                        str_emailAddress=[generalpd valueForKey:@"email"];
                        str_emailVerifyStatus=[generalpd valueForKey:@"emails"];
                        str_amnosVerifyStatus=[generalpd valueForKey:@"amnos"];
                        str_Url_pic=[generalpd valueForKey:@"pic"];
                        str_address=[generalpd valueForKey:@"addr"];
                        
                        NSLog(@"str_gender=%@",str_gender);
                        
                        
                        NSString *quali_id=[generalpd valueForKey:@"qual"];
                        NSString *Occu_id=[generalpd valueForKey:@"occup"];
                        
                        
                        str_qualification=[obj getqualName:quali_id];
                        str_occupation=[obj getOccuptname:Occu_id];
                        
                        NSLog(@"str_Url_pic  =>>> %@",str_Url_pic);
                        
                        
                        if ([[response objectForKey:@"socialpd"] isKindOfClass:[NSDictionary class]]) {
                            singleton.objUserProfile = nil;
                            singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"socialpd"]];
                        }
                        if ([[response objectForKey:@"pd"] isKindOfClass:[NSDictionary class]]) {
                            singleton.objUserProfile = nil;
                            singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
                        }
                        
                        [self setProfileData];
                        
                    } @catch (NSException *exception) {
                        
                    } @finally {
                        
                    }
                }
            }
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            
            [self setProfileData];
            
        }
        
    }];
    
}

-(NSString *)UpperFirstWord:(NSString*)inputString
{
    
    
    if ([inputString length]!=0) {
        inputString = [inputString stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[inputString substringToIndex:1] uppercaseString]];
    }
    return inputString;
}


-(void)setProfileData
{
    
    //----- local values  to be save and show from API-----------
    
    
    str_name=[self UpperFirstWord:str_name];
    // str_gender=str_dob;
    str_qualification=[self UpperFirstWord:str_qualification];
    str_occupation=[self UpperFirstWord:str_occupation];
    str_state=[self UpperFirstWord:str_state];
    str_district=[self UpperFirstWord:str_district];
    str_address=[self UpperFirstWord:str_address];
    str_registerMb=[self UpperFirstWord:str_registerMb];
    //str_emailAddress=str_emailAddress;
    str_alternateMb=[self UpperFirstWord:str_alternateMb];
    
    
    
    
    //  if ([str_Url_pic length]!=0 ) {
    
    singleton.user_profile_URL=str_Url_pic;
    //   }
    
    
    
    
    
    
    if ([str_gender length]!=0) {
        str_gender=[str_gender uppercaseString];
        
        /*  if ([str_gender isEqualToString:@"M"]) {
         stringGender= NSLocalizedString(@"gender_male", nil);
         singleton.notiTypeGenderSelected= NSLocalizedString(@"gender_male", nil);
         
         }
         else  if ([str_gender isEqualToString:@"F"]) {
         singleton.notiTypeGenderSelected=NSLocalizedString(@"gender_female", nil);
         stringGender=NSLocalizedString(@"gender_female", nil);
         
         }
         else  if ([str_gender isEqualToString:@"T"]||[str_gender isEqualToString:@"t"]) {
         singleton.notiTypeGenderSelected=NSLocalizedString(@"gender_transgender", nil);
         stringGender=NSLocalizedString(@"gender_transgender", nil);
         
         }
         
         else
         {
         singleton.notiTypeGenderSelected=@"";
         stringGender=@"";
         }
         */
        
    }
    
    str_name=[self UpperFirstWord:str_name];
    //str_gender=str_dob;
    str_qualification=[self UpperFirstWord:str_qualification];
    str_occupation=[self UpperFirstWord:str_occupation];
    str_state=[self UpperFirstWord:str_state];
    str_district=[self UpperFirstWord:str_district];
    str_address=[self UpperFirstWord:str_address];
    str_registerMb=[self UpperFirstWord:str_registerMb];
    //str_emailAddress=str_emailAddress;
    str_alternateMb=[self UpperFirstWord:str_alternateMb];
    //singleton.notiTypeGenderSelected
    singleton.profileNameSelected =str_name;
    singleton.profilestateSelected=str_state;
    singleton.notiTypeCitySelected=@"";
    singleton.notiTypDistricteSelected=str_district;
    singleton.profileDOBSelected=str_dob;
    singleton.altermobileNumber=str_alternateMb;
    singleton.user_Qualification=str_qualification;
    singleton.user_Occupation=str_occupation;
    singleton.profileEmailSelected=str_emailAddress;
    singleton.profileUserAddress=str_address;
    
    NSLog(@"str_gender=%@",str_gender);
    
    
    [self loadHtml];
    
    
    
    
}





- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}


//---------Loading HTML On webview----------
-(void)loadHtml
{
    
    
    // SharedManager *singleton = [SharedManager sharedSingleton];
    
    
    
    NSString *sid=@"";
    NSString *nam=singleton.profileNameSelected ;
    NSString *addr=singleton.profileUserAddress ;
    
    NSString *st=[NSString stringWithFormat:@"%@",singleton.profilestateSelected];
    NSLog(@"st=%@",st);
    if([st length]==0||[st isEqualToString:@"9999"])
    {
        st=@"";
    }
    
    
    NSString *cty=@"";
    NSString *dob=singleton.profileDOBSelected;
    NSString *gndr=str_gender;
    //NSString *qual=;
    
    NSString *qual=[obj getQualiListCode:singleton.user_Qualification];
    NSString *occup=[obj getOccuptCode:singleton.user_Occupation];
    
    NSString *email=singleton.profileEmailSelected;
    NSString *amno=singleton.altermobileNumber;
    NSString *lang=@""; //add condition for it
    
    
    
    NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if (selectedLanguage == nil)
    {
        selectedLanguage = @"en";
    }
    else{
        NSArray *arrTemp = [selectedLanguage componentsSeparatedByString:@"-"];
        selectedLanguage = [arrTemp firstObject];
    }
    lang=selectedLanguage;
    //===fix lint-----
    NSLog(@"Applied Selected Language = %@",selectedLanguage);
    
    /* bool checklang=[singleton.dbManager getServiceLanguage:service_ID withDeviceLang:selectedLanguage];
     
     if (checklang==true)
     {
     lang=selectedLanguage; //add condition for it
     
     }
     else
     {
     lang=@"en";
     }
     */
    
    
    
    // NSString *aadhr= singleton.user_aadhar_number;
    NSString *aadhr= singleton.objUserProfile.objAadhar.aadhar_number;
    
    NSString *ntfp=@"1";
    NSString *ntft=@"1";
    NSString *pic=singleton.user_profile_URL;
    NSString *psprt=@"";
    NSString *dist=singleton.notiTypDistricteSelected;
    NSString *amnos=@""; //pass it
    NSString *emails=@"";//pass it
    NSString *ivrlang=@""; //pass it
    //NSString *gcmid=singleton.devicek_tkn;
    NSString *gcmid=[obj getStateCode:singleton.profilestateSelected ];
    
    
    
    // st=[self getStateEnglishName:gcmid];
    
    //NSString *gcmid=[obj getStateCode:[@"Uttarakhand" lowercaseString]];
    
    NSString *mno=singleton.mobileNumber;
    NSString *uid=singleton.user_id; //pass it
    
    if ([uid length]==0) {
        uid=@"";
    }
    
    
    
    if([nam length]==0)
    {
        nam=@"";
    }
    if([addr length]==0)
    {
        addr=@"";
    }
    
    if([dob length]==0)
    {
        dob=@"";
    }
    
    if([gndr length]==0)
    {
        gndr =@"";
    }
    
    if([qual length]==0)
    {
        qual=@"";
    }
    
    if([occup length]==0)
    {
        occup=@"";
    }
    
    if([email length]==0)
    {
        email=@"";
    }
    
    if([amno length]==0)
    {
        amno=@"";
    }
    
    
    
    if([pic length]==0)
    {
        pic=@"";
    }
    
    if([dist length]==0)
    {
        dist=@"";
    }
    if([aadhr length]==0)
    {
        aadhr=@"";
    }
    
    
    
    
    
    NSString *adhr_name=singleton.objUserProfile.objAadhar.name;
    NSString *adhr_dob=singleton.objUserProfile.objAadhar.dob;
    NSString *adhr_co=singleton.objUserProfile.objAadhar.father_name;
    NSString *adhr_gndr=singleton.objUserProfile.objAadhar.gender;
    NSString *adhr_imgurl=singleton.objUserProfile.objAadhar.aadhar_image_url;
    NSString *adhr_state=singleton.objUserProfile.objAadhar.state;
    NSString *adhr_dist=singleton.objUserProfile.objAadhar.district;
    
    if([adhr_name length]==0)
    {
        adhr_name=@"";
    }
    
    if([adhr_dob length]==0)
    {
        adhr_dob=@"";
    }
    
    if([adhr_co length]==0)
    {
        adhr_co=@"";
    }
    
    if([adhr_gndr length]==0)
    {
        adhr_gndr=@"";
    }
    
    if([adhr_imgurl length]==0)
    {
        adhr_imgurl=@"";
    }
    
    if([adhr_state length]==0)
    {
        adhr_state=@"";
    }
    if([adhr_dist length]==0)
    {
        adhr_dist=@"";
    }
    //--- fix lint
    NSLog(@"adhr_state=%@",adhr_state);
    
    
    
    if([gcmid length]==0)
    {
        gcmid=@"";
    }
    
    if([gcmid isEqualToString:@"9999"])
    {
        gcmid=@"";
    }
    
    
    //------------------------ If needed Open this code-----------------------
    
    // st=@"";
    // dist=@"";
    if (![lang isEqualToString:@"en"]) {
        dist=@"";
        st=@"";
    }
    
    //------------------------ Close this code-----------------------
    
    
    NSMutableDictionary *userinfo = [NSMutableDictionary new];
    [userinfo setObject:sid forKey:@"sid"];
    [userinfo setObject:nam forKey:@"nam"];
    [userinfo setObject:addr forKey:@"addr"];
    [userinfo setObject:st forKey:@"st"];
    [userinfo setObject:cty forKey:@"cty"];
    [userinfo setObject:dob forKey:@"dob"];
    [userinfo setObject:gndr forKey:@"gndr"];
    [userinfo setObject:qual forKey:@"qual"];
    [userinfo setObject:email forKey:@"email"];
    [userinfo setObject:amno forKey:@"amno"];
    [userinfo setObject:lang forKey:@"lang"];
    
    
    [userinfo setObject:ntfp forKey:@"ntfp"];
    [userinfo setObject:ntft forKey:@"ntft"];
    [userinfo setObject:pic forKey:@"pic"];
    [userinfo setObject:psprt forKey:@"psprt"];
    [userinfo setObject:occup forKey:@"occup"];
    [userinfo setObject:dist forKey:@"dist"];
    [userinfo setObject:amnos forKey:@"amnos"];
    [userinfo setObject:emails forKey:@"emails"];
    [userinfo setObject:ivrlang forKey:@"ivrlang"];
    [userinfo setObject:gcmid forKey:@"gcmid"];
    [userinfo setObject:mno forKey:@"mno"];
    [userinfo setObject:uid forKey:@"uid"];
    
    
    
    
    
    
    
    NSMutableDictionary * adhrinfo = [NSMutableDictionary new];
    [adhrinfo setObject:adhr_name forKey:@"adhr_name"];
    [adhrinfo setObject:adhr_dob forKey:@"adhr_dob"];
    [adhrinfo setObject:adhr_co forKey:@"adhr_co"];
    [adhrinfo setObject:adhr_gndr forKey:@"adhr_gndr"];
    [adhrinfo setObject:adhr_imgurl forKey:@"adhr_imgurl"];
    [adhrinfo setObject:adhr_dist forKey:@"adhr_dist"];
    
    
    
    NSString *tkn=singleton.user_tkn;
    
    
    
    if([tkn length]==0)
    {
        tkn=@"";
    }
    
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody addEntriesFromDictionary:[self getCommonParametersForRequestBody]];
    [dictBody setObject:@"app" forKey:@"mod"];
    [dictBody setObject:@"" forKey:@"peml"];
    [dictBody setObject:lang forKey:@"lang"];
    [dictBody setObject:tkn forKey:@"tkn"];
    [dictBody setObject:mno forKey:@"mno"];
    //[dictBody setObject:service_ID forKey:@"service_id"];
    [dictBody setObject:@"" forKey:@"service_id"];
    
    if (singleton.objUserProfile.objAadhar.aadhar_number.length)
    {
        [dictBody setObject:aadhr forKey:@"aadhr"];
        
        
    }
    
    //---------- add userinfo -------------------
    [dictBody setObject:userinfo forKey:@"userinfo"];
    //---------- add adhrinfo-------------------
    
    
    
    if (singleton.objUserProfile.objAadhar.aadhar_number.length)
    {
        [dictBody setObject:adhrinfo forKey:@"adhrinfo"];
        
        
    }
    else
    {
        //dont add it
    }
    
    
    
    NSString *checkLinkStatus=[[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"LINKDIGILOCKERSTATUS"];
    
    if ([checkLinkStatus isEqualToString:@"YES"])
    {
        
        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
        
        NSString *username=[[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"digilocker_username"];
        NSString *password=[[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"digilocker_password"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        NSMutableDictionary *digilocker = [NSMutableDictionary new];
        [digilocker setObject:username forKey:@"username"];
        [digilocker setObject:password forKey:@"password"];
        
        
        
        [dictBody setObject:digilocker forKey:@"digilocker"];
        
        //------------------------- Encrypt Value------------------------
        
    }
    
    //Staging
    // urlString=@"https://stgweb.umang.gov.in/aicte/api/deptt/aicte/?redirectUrl=customerCareChat";
    
    //production
    urlString=@"https://web.umang.gov.in/aicte/api/deptt/aicte/?redirectUrl=customerCareChat";
    
    
    //urlString=@"https://web.umang.gov.in/uw/vahan.jsp#/customerCareChat";
    NSString *urlAddress =urlString;//<null>
    
    if ([urlAddress isEqualToString:@"<null>"])
    {
        urlAddress=@"";
    }
    
    
    
    NSError * err;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:dictBody options:0 error:&err];
    NSString * contentToEncode = [[NSString alloc] initWithData:jsonData   encoding:NSUTF8StringEncoding];
    //NSLog(@"contentToEncode %@",contentToEncode);
    
    contentToEncode = [contentToEncode stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"contentToEncode %@",contentToEncode);
    
    NSString *contentEncoding = @"application/vnd.umang.web+json; charset=UTF-8";
    
    postURI=[NSURL URLWithString:urlAddress];
    
    NSString *verb = @"GET";
    
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: @"EEE, d MMM yyyy HH:mm:ss z"];
    NSString *currentDate = [dateFormatter stringFromDate: [NSDate date]]; //Put whatever date you want to convert
    NSString *currentDatetrim = [currentDate stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    NSString *USERNAME=@"UM4NG";
    NSString *contentMd5=[NSString stringWithFormat:@"%@",[contentToEncode MD5]];
    
    NSString *contentMd5trim = [contentMd5 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    NSString *postURItrim=[postURI.path stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *toSign =[NSString stringWithFormat:@"%@\n%@\n%@\n%@\n%@",verb,contentMd5trim,contentEncoding,currentDatetrim,postURItrim];
    NSString *hmac=[self hmacsha1:toSign secret:SaltSHA1MAC];
    //NSLog(@"hmac %@",hmac);
    NSString *hmactrim=[hmac stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *XappAuth=[NSString stringWithFormat:@"%@:%@",USERNAME,hmactrim];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:postURI];
    self.webView.delegate=self;
    [request addValue:XappAuth forHTTPHeaderField:@"X-App-Authorization"];
    [request addValue:currentDate forHTTPHeaderField:@"X-App-Date"];
    [request addValue:contentMd5trim forHTTPHeaderField:@"X-App-Content"];
    [request addValue:contentToEncode forHTTPHeaderField:@"X-App-Data"];
    [request addValue:contentEncoding forHTTPHeaderField:@"Content-Type"];
    
    
    [self.webView loadRequest: request];
    
    
}

//-------- WEB CACHE CODE START------------

- (void)webViewDidStartLoad:(UIWebView *)webView {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [activityIndicator startAnimating];
    
    
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    
    NSLog(@"error=%@",error);
    if([error code] == NSURLErrorCancelled)
        return;
    /*  if (error) {
     [self.webView loadData:singleton.cachedata MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:postURI];
     }*/
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [activityIndicator stopAnimating];
    
    
}

#pragma mark - Updating the UI


- (void)webViewDidFinishLoad:(UIWebView *)webView {
    //NSLog(@"%s", __PRETTY_FUNCTION__);
    
    if(flag_chat==TRUE)
    {
        //[self performSelector:@selector(openchat) withObject:nil afterDelay:3];
        flag_chat=FALSE;
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [activityIndicator stopAnimating];
    
    
}


//-------- WEB CACHE CODE START------------

-(BOOL)webView:(UIWebView *)mainWebView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSString *requestString = [[request URL] absoluteString];
    NSLog(@"requestString -->%@",requestString);
    if ([[[request URL] absoluteString] hasPrefix:@"ios::openChatScreen::"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        @try {
            NSString *url=[json valueForKey:@"URL"];
            NSString *title=[json valueForKey:@"title"];
            [self openChatScreen:title withURL:url];
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        
        
        return NO;
        
        
    }
    
    if ([[[request URL] absoluteString] hasPrefix:@"ios::BackHandleNative::"]||[[[request URL] absoluteString] hasPrefix:@"ios::closeChatWindow::Yes"])
    {
        self.webView = nil;
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
    return YES;
    
}



//----- Open Chat Window
-(void)openChatScreen:(NSString*)title withURL:(NSString*)urltoPass
{
    flagViewAppear=FALSE;
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    vc.urltoOpen=urltoPass;
    vc.titleOpen=title;
    vc.isfrom=@"WEBVIEWHANDLE";
    // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:vc animated:NO completion:nil];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
