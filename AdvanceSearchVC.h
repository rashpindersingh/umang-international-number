//
//  AdvanceSearchVC.h
//  Umang
//
//  Created by deepak singh rawat on 26/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdvanceSearchVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    IBOutlet UITableView *tbl_search;
    IBOutlet UITextField *txt_search;
    IBOutlet UIView *vw_line;
    NSArray *searchResults;
    
    NSMutableArray *pastServices;
    NSMutableArray *autocompleteService;
    
    IBOutlet UIButton *btn_closeSearch;
    IBOutlet UIButton *btn_filter;
    
    
    IBOutlet UILabel *lbl_results;
    int tagCheck;
    
    IBOutlet UIImageView *searchIconImage;
}
-(IBAction)btn_filter:(id)sender;

-(IBAction)btnSearchClose:(id)sender;
-(IBAction)cancelBtnAction:(id)sender;
@end

#pragma mark - New Search Cell Interface
@interface NewSearchCell : UITableViewCell

@property(nonatomic,retain)IBOutlet UIImageView *img_service;
@property(nonatomic,retain)IBOutlet UILabel *lbl_servicetitle;
@property(nonatomic,retain)IBOutlet UILabel *lbl_servicedesc;
@property(nonatomic,retain)IBOutlet UILabel *lbl_rating;
@property(nonatomic,retain)IBOutlet UIImageView *img_star;
@property(nonatomic,retain)IBOutlet UILabel *lbl_category;
@property (strong, nonatomic) IBOutlet UILabel *lbl_deptName;

@property(retain, nonatomic) IBOutlet UIScrollView *scrollView_nouse;

-(void)addCategoryStateItemsToScrollView:(NSArray*)arrCat state:(NSArray*)arrStates withFont:(CGFloat)fontSize cell:(NewSearchCell*)cell andTagIndex:(NSIndexPath *)indexpath;
@end
