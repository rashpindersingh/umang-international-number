//
//  EmailHelpViewController.m
//  Umang
//
//  Created by Lokesh Jain on 12/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "EmailHelpViewController.h"

@interface EmailHelpViewController ()
{
    __weak IBOutlet UIButton *backBtn;
    
}

@end

@implementation EmailHelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [backBtn setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
       CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:backBtn.titleLabel.font.pointSize]}];
        CGRect framebtn=CGRectMake(backBtn.frame.origin.x, backBtn.frame.origin.y, backBtn.frame.size.width, backBtn.frame.size.height);
        [backBtn setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        backBtn.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
    }
    
}
- (IBAction)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    
}


#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}


@end
