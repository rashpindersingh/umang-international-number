//
//  ChangeRegMobMpinVC.h
//  Umang
//
//  Created by admin on 09/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangeRegMobMpinVC : UIViewController
@property(nonatomic,retain)NSMutableDictionary *dic_info;
@property(nonatomic,retain)UIImage *user_img;

@property (strong, nonatomic) IBOutlet UIButton *cancelButton;

@property (nonatomic,strong) NSString *comingFrom;
-(IBAction)doneClicked:(id)sender;



-(IBAction)keyPress:(id)sender;


@end
