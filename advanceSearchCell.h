//
//  advanceSearchCell.h
//  AdvanceSearch
//
//  Created by deepak singh rawat on 27/12/16.
//  Copyright © 2016 deepak singh rawat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface advanceSearchCell : UITableViewCell
@property(nonatomic,retain)IBOutlet UIImageView *img_service;
@property(nonatomic,retain)IBOutlet UILabel *lbl_servicetitle;
@property(nonatomic,retain)IBOutlet UILabel *lbl_servicedesc;
@property(nonatomic,retain)IBOutlet UILabel *lbl_rating;
@property(nonatomic,retain)IBOutlet UIImageView *img_star;
@property(nonatomic,retain)IBOutlet UILabel *lbl_category;

@end
