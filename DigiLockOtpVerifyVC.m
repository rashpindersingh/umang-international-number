//
//  DigiLockOtpVerifyVC.m
//  Umang
//
//  Created by admin on 06/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "DigiLockOtpVerifyVC.h"
#define kOFFSET_FOR_KEYBOARD 80.0
#import "DigiLockVerifyStatusVC.h"

#import "MBProgressHUD.h"
#import <QuartzCore/QuartzCore.h>

#import "MyTextField.h"
#import "UMAPIManager.h"

#define MAX_LENGTH 6


@interface DigiLockOtpVerifyVC ()<UITextFieldDelegate>
{
    SharedManager *singleton;
    MBProgressHUD *hud ;
    NSString *retryAdhar;
    __weak IBOutlet UILabel *lblWaitingSMS;
    
    __weak IBOutlet UIButton *btnBack;
    
    __weak IBOutlet UILabel *lblHeaderTitle;
    
    __weak IBOutlet UILabel *lblSubHeaderDescription;
    __weak IBOutlet UILabel *lblHeaderDescription;
    
    // __weak IBOutlet UILabel *lblDidntReceive;
    __weak IBOutlet UILabel *lblEnter6DigitOTp;
    
    int count;
    
}


@property (nonatomic, strong) NSTimer *myTimer;


//--- code for otp and resend handling---------


@property (nonatomic, strong) IBOutlet UIProgressView *progressView;

@property (weak, nonatomic) IBOutlet UILabel *lb_timer;



@property (weak, nonatomic) IBOutlet UITextField *txt1;
@property (weak, nonatomic) IBOutlet UILabel *vw_line1;

@end

@implementation DigiLockOtpVerifyVC
@synthesize aadharNumber;

//-------- Code for resend otp and IVR---------
@synthesize timeOut;
@synthesize retryCount;
@synthesize lblOTPRegister;
@synthesize lblRetry;
@synthesize txtFldOTP;
@synthesize lblTime;
@synthesize maskedEmailNumber;
@synthesize maskedMobileNumber;



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.txtFldOTP.delegate = self;
    
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    lblWaitingSMS.text = NSLocalizedString(@"waiting_for_sms", nil);
    
    
    singleton = [SharedManager sharedSingleton];
    singleton.user_aadhar_number = self.aadharNumber;
    
    NSString *strMobileNumber = maskedMobileNumber;
    NSString *strEmailNumber = maskedEmailNumber;
    NSString *strCombined = [NSString stringWithFormat:NSLocalizedString(@"otp_sent_to_mobile", nil),maskedMobileNumber] ;
    strCombined = [strCombined stringByReplacingOccurrencesOfString:@"EMAILID" withString:strEmailNumber];
    strCombined = [strCombined stringByReplacingOccurrencesOfString:@"MOBILENUMBER" withString:strMobileNumber];
    
    
    
    lblHeaderDescription.text = strCombined;
    
    
    lblSubHeaderDescription.text = NSLocalizedString(@"register_verify_otp_sub_heading", nil);
    // lblDidntReceive.text = NSLocalizedString(@"didnt_receive_otp", nil);
    lblEnter6DigitOTp.text = NSLocalizedString(@"enter_6_digit_otp", nil);
    
    //----- Setting delegate for Custom textfield so back space operation work smooth
    _txt1.delegate = self;
    [_txt1 addTarget:self
              action:@selector(textFieldDidChange:)
    forControlEvents:UIControlEventEditingChanged];
    
    
    
    [_txt1 becomeFirstResponder];
    // self.vw_line.frame=CGRectMake(53, 492, 268, 0.5);
    self.view.userInteractionEnabled = YES;
    
    
    
    
    
    
    
    self.vw_line1.backgroundColor=[UIColor lightGrayColor];
    
    
    // Do any additional setup after loading the view.
    //------ Add dismiss keyboard while background touch-------
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [scrollView addGestureRecognizer:tapGesture];
    
    timeOut = 120;
    [self startOTPTimer];
    
    //UMGIOSINT-1193 FIX
    // _txt1.keyboardType=UIKeyboardTypeNumbersAndPunctuation;
    _txt1.keyboardType = UIKeyboardTypeNumberPad;
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setViewFont];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    lblHeaderTitle.font = [AppFont semiBoldFont:17];
    lblHeaderDescription.font = [AppFont semiBoldFont:14.0];
    lblSubHeaderDescription.font = [AppFont mediumFont:15.0];
    _txt1.font = [AppFont regularFont:21.0];
    txtFldOTP.font = [AppFont regularFont:21.0];
    lblEnter6DigitOTp.font = [AppFont mediumFont:14.0];
    _lb_timer.font = [AppFont mediumFont:14];
    lblWaitingSMS.font = [AppFont mediumFont:13];
}
-(void)hideKeyboard{
    [self.view endEditing:YES];
}

-(void)startOTPTimer{
    
    count = timeOut; // Total time in seconds.
    
    
    if ( [self.myTimer isValid]){
        [self.myTimer invalidate], self.myTimer=nil;
    }
    
    self.myTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateTimeValue:) userInfo:nil repeats:YES];
    
}

-(void)updateTimeValue:(id)sender{
    
    count --;
    
    if (count <=0) // When count reaches 0 means timeout
    {
        [self.myTimer invalidate];
        self.myTimer = nil;
        return;
    }
    
    self.progressView.progress = (float)count/timeOut;
    self.lb_timer.text=[self timeFormatted:count];
}

- (NSString *)timeFormatted:(int)totalSeconds{
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    // int hours = totalSeconds / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
}


- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(string.length==0)
    {
        return YES;
    }
    else
    {
        NSString *validRegEx =@"^[0-9]$"; //change this regular expression as your requirement
        NSPredicate *regExPredicate =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", validRegEx];
        BOOL myStringMatchesRegEx = [regExPredicate evaluateWithObject:string];
        if (myStringMatchesRegEx)
            return YES;
        else
            return NO;
    }
    
    
}

- (void)textFieldDidChange:(UITextField *)textField
{
    
    if (textField.text.length >= MAX_LENGTH)
    {
        textField.text = [textField.text substringToIndex:MAX_LENGTH];
        [textField resignFirstResponder];
        // aditi
        [self checkValidation];
    }
    
}


-(void)checkValidation
{
    if (_txt1.text.length < 6)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"enter_otp_is_not_valid", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
        [alert show];
        
        
        
    }
    else
    {
        [self hideKeyboard];
        
        [self hitAPIForDigilockerOTP];
        
        
    }
    
}

-(void)hitAPIForDigilockerOTP
{
    NSString *otpString= _txt1.text;
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    double CurrentTime = CACurrentMediaTime();
    
    NSString * timeInMS = [NSString stringWithFormat:@"%f", CurrentTime];
    
    timeInMS=  [timeInMS stringByReplacingOccurrencesOfString:@"." withString:@""];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    //  [dictBody setObject:singleton.aadharNumber forKey:@"mno"];//Enter aadhar number of user
    
    
    [dictBody setObject:@"aadhar" forKey:@"type"];
    NSLog(@"aadhar Number is = %@",self.aadharNumber);
    [dictBody setObject:self.aadharNumber forKey:@"uid"];
    [dictBody setObject:self.aadharNumber forKey:@"aadhr"];
    [dictBody setObject:timeInMS forKey:@"trkr"];
    
    [dictBody setObject:otpString forKey:@"otp"];
    [dictBody setObject:@"rgtadhr" forKey:@"ort"];
    [dictBody setObject:@"Y" forKey:@"rc"];
    [dictBody setObject:@"Y" forKey:@"mec"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    
    
    NSLog(@"Dictioary is %@",dictBody);
    
    
    [objRequest hitAPIForDigiLockerAuthenticationWithPost:YES isAccessTokenRequired:YES webServiceURL:UM_API_VALIDATE_DIGILOCKER_OTP withBody:dictBody andTag:TAG_REQUEST_VALID_DIGILOCKER completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        
        [hud hideAnimated:YES];
        // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
        
        
        if (error == nil)
        {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            //NSString *rc=[response valueForKey:@"rc"];
            NSString *rd=[response valueForKey:@"rd"];
            // NSString *rs=[response valueForKey:@"rs"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                [self alertwithMsg:rd];
                
            }
            else if ([[response objectForKey:@"rs"] isEqualToString:@"F"] && [[response valueForKey:@"rd"] isEqualToString:@"Aadhaar service temporarily unavailable. Please try again. If problem persists, check back in some time. "])
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                                message:[response valueForKey:@"rd"]
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                      otherButtonTitles:nil];
                [alert show];
            }
            
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            
        }
        else{
            _txt1.text=@"";
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
    return;
    
}


-(void)alertwithMsg:(NSString*)msg
{
    [self openNextView];
    
    
}

-(void)openNextView
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    DigiLockVerifyStatusVC  *vc = [storyboard instantiateViewControllerWithIdentifier:@"DigiLockVerifyStatusVC"];
    //   RS3vc.isFromAadharRegistration = YES;
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    //[self.navigationController pushViewController:aadharLink animated:YES];
    [self presentViewController:vc animated:NO completion:nil];
    
}

- (IBAction)btnBackClicked:(id)sender {
    [self.myTimer invalidate];
    self.myTimer = nil;
    
    [self dismissViewControllerAnimated:NO completion:nil];
}


- (void) viewDidDisappear:(BOOL)animated
{
    [self.myTimer invalidate];
    self.myTimer = nil;
    
    [super viewDidDisappear:NO];
    
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)btnResendOTPTapped:(id)sender {
}
@end
