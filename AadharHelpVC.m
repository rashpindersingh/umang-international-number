//
//  AadharHelpVC.m
//  Umang
//
//  Created by admin on 03/03/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "AadharHelpVC.h"
#import "SocialHelpIconCell.h"
#import "HelpViewController.h"
#import "FAQWebVC.h"
#import "SubmitQueryVC.h"

@interface AadharHelpVC ()<UITableViewDelegate,UITableViewDataSource>
{
    //__weak
    IBOutlet UITableView *tblAadharHelp;
    
    __weak IBOutlet UILabel *lblAadharTitle;
    __weak IBOutlet UILabel *lblAadharDescriptipn;
    __weak IBOutlet UIView *vwBG;
    __weak IBOutlet UIButton *btnBackAadhar;
    NSMutableArray *arrAadharHelpName;
    NSMutableArray *arrAadharHelpImage;
     SharedManager *singleton;
}
@end

@implementation AadharHelpVC


- (void)viewDidLoad
{
    
    singleton=[SharedManager sharedSingleton];
    [super viewDidLoad];
    tblAadharHelp.delegate = self;
    tblAadharHelp.dataSource = self;
    
    vwBG.layer.borderWidth = 0.5;
    vwBG.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [btnBackAadhar setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        // [btnBack sizeToFit];
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBackAadhar.titleLabel.font.pointSize]}];
        
        //or whatever font you're using
        CGRect framebtn=CGRectMake(btnBackAadhar.frame.origin.x, btnBackAadhar.frame.origin.y, btnBackAadhar.frame.size.width, btnBackAadhar.frame.size.height);
        
        
        [btnBackAadhar setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBackAadhar.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    arrAadharHelpName = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"help_and_support", nil),NSLocalizedString(@"help_faq", nil),NSLocalizedString(@"user_manual", nil), nil];
    
    arrAadharHelpImage = [[NSMutableArray alloc]initWithObjects:@"more_help_support",@"faq_Updated",@"_SocialHelp", nil];
    
    lblAadharTitle.text = NSLocalizedString(@"aadhaar", nil);
    CGRect lblAadharTitleFrame =  lblAadharTitle.frame;
    lblAadharTitleFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tblAadharHelp.frame)-lblAadharTitleFrame.size.width + 50 : 15;
    lblAadharTitle.frame = lblAadharTitleFrame;
    
    lblAadharDescriptipn.text = NSLocalizedString(@"link_aadhaar_help_txt", nil);
    lblAadharTitleFrame.origin.x = singleton.isArabicSelected ?  NSTextAlignmentRight : NSTextAlignmentLeft;
    
    
    CGRect imgSocialFrame =  self._imgSocial.frame;
    imgSocialFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tblAadharHelp.frame) -50: 15;
    self._imgSocial.frame =  imgSocialFrame;
    
    
    
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setViewFont];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBackAadhar.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    lblAadharTitle.font = [AppFont regularFont:16.0];
    lblAadharDescriptipn.font = [AppFont regularFont:16.0];
    
    
    
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}
- (IBAction)btnBackAadharClicked:(id)sender
{
   // [self dismissViewControllerAnimated:YES completion:nil];
    
    [self.navigationController popViewControllerAnimated:YES];

}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section

{
    return  44;
}


- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    //check header height is valid
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,fDeviceWidth,50)];
    headerView.backgroundColor=[UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
    
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(15,15,300,25);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithRed:0.298 green:0.337 blue:0.424 alpha:1.0];
    
    
    CGRect labelFrame =  label.frame;
    labelFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tableView.frame) - 100 : 15;
    label.frame = labelFrame;

    label.font = [UIFont systemFontOfSize:14.0];
    
    label.text = NSLocalizedString(@"more_help_lbl", nil);
    
    
    [headerView addSubview:label];
    
    return headerView;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"HelpCell";
    
    SocialHelpIconCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    if (cell == nil)
    {
        cell = [[SocialHelpIconCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [arrAadharHelpName objectAtIndex:indexPath.row];
    cell.textLabel.font = [AppFont regularFont:17.0];
    cell.imageView.image = [UIImage imageNamed:[arrAadharHelpImage objectAtIndex:indexPath.row]];
      cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0;
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row==0)//tab picker
    {
       
        [self myHelp_Action];
        
    }
    
    if (indexPath.row==1)//language picker
    {
       
        
        [self openFAQWebVC];
        
    }
    
    if (indexPath.row==2)
    {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

        FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
        if ([singleton.arr_initResponse  count]>0) {
            vc.urltoOpen=[singleton.arr_initResponse valueForKey:@"usrman"];
            
        }
        
        vc.titleOpen= NSLocalizedString(@"user_manual", nil);
       // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

-(void)myHelp_Action
{
    NSLog(@"My Help Action");
    //  UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    HelpViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}

-(void)openFAQWebVC
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    if ([singleton.arr_initResponse  count]>0) {
        vc.urltoOpen=[singleton.arr_initResponse valueForKey:@"faq"];
        
    }
    vc.titleOpen= NSLocalizedString(@"help_faq", nil);
    //[vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
