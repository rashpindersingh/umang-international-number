//
//  TxnFilterResultsVC.h
//  Umang
//
//  Created by admin on 22/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TxnFilterResultsVC : UIViewController
{
    IBOutlet UIView *vw_noresults;
    
    IBOutlet UIImageView *img_no_result;
    IBOutlet UILabel *lbl_noresult;
}
@property (weak, nonatomic) IBOutlet UITableView *tblTransactionHistory;
@property(nonatomic,retain)    NSMutableArray *arrTransactionHistory;
@property(nonatomic,retain)NSDictionary *dictFilterParams;
@end
