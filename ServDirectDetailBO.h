//
//  ServDirectDetailBO.h
//  Umang
//
//  Created by spice_digital on 28/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServDirectDetailBO : NSObject

@property(nonatomic,strong)NSString *strEmailId;
@property(nonatomic,strong)NSString *strEmailImage;

@end
