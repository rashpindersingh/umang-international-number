//
//  VerifyOTPNewMbVC.m
//  Umang
//
//  Created by admin on 09/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "VerifyOTPNewMbVC.h"

#define MAX_LENGTH 6
#define kOFFSET_FOR_KEYBOARD 80.0

#import "MBProgressHUD.h"
#import <QuartzCore/QuartzCore.h>

#import "MyTextField.h"
#import "UMAPIManager.h"

#import "LoginAppVC.h"

#import "ChangeRegMobMpinVC.h"
#import "EnterNewMobNumVC.h"
#import "SecuritySettingVC.h"



@interface VerifyOTPNewMbVC ()<UITextFieldDelegate>
{
    __weak IBOutlet UIButton *btnBack;
    SharedManager *singleton;
    MBProgressHUD *hud ;
    IBOutlet UIButton *btn_resend;
    IBOutlet UIButton *btn_callme;
    NSString *retryResend;
    NSString *retryCall;
    NSString *retryAdhar;
    
    __weak IBOutlet UILabel *lblWaitingForSMS;
    
    __weak IBOutlet UILabel *lblHeaderTitle;
    
    __weak IBOutlet UILabel *lblDidntReceive;
    __weak IBOutlet UILabel *lblEnter6Digit;
    __weak IBOutlet UILabel *lblSubHeaderDescritn;
    __weak IBOutlet UILabel *lblHeaderDescription;
    
    int count;
    
}
@property (weak, nonatomic) IBOutlet UILabel *lb_rtryResend;
@property (weak, nonatomic) IBOutlet UILabel *lb_rtryCall;
//--- code for otp and resend handling---------


@property (nonatomic, strong) IBOutlet UIProgressView *progressView;
@property (nonatomic, strong) NSTimer *myTimer;
@property (weak, nonatomic) IBOutlet UILabel *lb_timer;

@property (weak, nonatomic) IBOutlet UIView *vw_line;
@property (weak, nonatomic) IBOutlet UITextField *txt1;

@property (weak, nonatomic) IBOutlet UILabel *vw_line1;




@end

@implementation VerifyOTPNewMbVC
@synthesize strAadharNumber;

//-------- Code for resend otp and IVR---------
@synthesize newmobile;
@synthesize TAGFROM;
@synthesize rtry;
@synthesize tout;
@synthesize scrollView;


-(IBAction)btn_resendAction:(id)sender
{
    _txt1.text=@"";
    
    resendOTPview.hidden=TRUE;
    [self hitResendOtpAPI];
    //chnl_resend=@"sms";
    
   // [self parameterForApiCall:chnl_resend];
}

-(IBAction)btn_callmeAction:(id)sender
{
    resendOTPview.hidden=TRUE;
    
   // chnl_resend=@"ivr";
    [self parameterForApiCall:@"ivr"];
    
}

- (void)updateUI:(NSTimer *)timer
{
    NSLog(@"count=%d",count);
    
    
    if (count <=0)
    {
        [self.myTimer invalidate];
        self.myTimer = nil;
        resendOTPview.hidden=FALSE;
        
        
        
        int rtryOtp=[retryResend intValue];
        int rtryCall=[retryCall intValue];
        int rtryAdhar=[retryAdhar intValue];
        
        if (rtryAdhar>0)
        {
            resendOTPview.hidden=FALSE;
            
        }
        else
        {
            if (rtryOtp <=0)
            {
                btn_resend.hidden=TRUE;
                self.lb_rtryResend.hidden=TRUE;
                
            }
            if (rtryCall<=0)
            {
                
                btn_callme.hidden=TRUE;
                self.lb_rtryCall.hidden=TRUE;
                
                
            }
            if (rtryOtp <=0 && rtryCall<=0)
            {
                resendOTPview.hidden=TRUE;
            }
            
        }
        
        
    }
    else
    {
        
        count --;
        
        self.progressView.progress = (float)count/120.0f;
        
        self.lb_timer.text=[self timeFormatted:count];
        
    }
    
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, resendOTPview.isHidden ? scrollView.frame.size.height:scrollView.frame.size.height+150)];
    

    
    
}


-(void)parameterForApiCall:(NSString*)chnlresend
{
    btn_callme.hidden=FALSE;
    self.lb_rtryCall.hidden=FALSE;
    [self hitAPIResendOTPwithIVR:chnlresend];
}






//----- hitAPI  registration ------
-(void)hitResendOtpAPI
{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary* dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];
    [dictBody setObject:newmobile forKey:@"nmno"];
    [dictBody setObject:@"updtmob" forKey:@"ort"];
    [dictBody setObject:@"" forKey:@"email"];
    [dictBody setObject:self.mpinEncrypt forKey:@"mpin"];
    [dictBody setObject:@"" forKey:@"dgt"];
    [dictBody setObject:@"" forKey:@"st"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:@"initiate" forKey:@"type"];
    
   // [NSUserDefaults standardUserDefaults] objectForKey:@"USER_ENTER_TEMP_MPIN_KEY"]
    
    //[[NSUserDefaults standardUserDefaults] setObject:singleton.bannerStatus forKey:@"USER_ENTER_TEMP_MPIN_KEY"];
   // NSString *user_mpin_entered = [[NSUserDefaults standardUserDefaults] objectForKey:@"USER_ENTER_TEMP_MPIN_KEY"];
    //[[NSUserDefaults standardUserDefaults]synchronize];

    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
 hud.label.text = NSLocalizedString(@"loading",nil);
    
    
    
    //singleton.mobileNumber=_txt_mobileNo.text; //save mobile number for future use of user
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_UMOBILE withBody:dictBody andTag:TAG_REQUEST_UMOBILE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);

            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            tout=[[[response valueForKey:@"pd"] valueForKey:@"tout"] intValue];
            rtry=[[response valueForKey:@"pd"] valueForKey:@"rtry"];
            
            
            
//            NSString *rc=[response valueForKey:@"rc"];
//            NSString *rd=[response valueForKey:@"rd"];
//            NSString *rs=[response valueForKey:@"rs"];
            
            
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                rtry=[[response valueForKey:@"pd"] valueForKey:@"rtry"];
                tout=[[[response valueForKey:@"pd"] valueForKey:@"tout"] intValue];
                NSArray *retryItems = [rtry componentsSeparatedByString:@"|"];
                
                retryResend=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:0]];
                retryCall=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:1]];
                
                //retryAdhar=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:2]];
                @try {
                    retryAdhar=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:2]];
                    
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                btn_resend.enabled=TRUE;
                btn_callme.enabled=TRUE;
                NSString *strRetryValue = NSLocalizedString(@"attempts_left", nil);
                
                self.lb_rtryResend.text= [NSString stringWithFormat:@"(%@ %@)",retryResend,strRetryValue];
                
                
                self.lb_rtryCall.text= [NSString stringWithFormat:@"(%@ %@)",retryCall,strRetryValue];
                
                
                int rtryOtp=[retryResend intValue];
                int rtryCall=[retryCall intValue];
                int rtryAdhar=[retryAdhar intValue];
                
                if (rtryAdhar>0)
                {
                    resendOTPview.hidden=FALSE;
                    
                }
                else
                {
                    if (rtryOtp <=0)
                    {
                        btn_resend.hidden=TRUE;
                        self.lb_rtryResend.hidden=TRUE;
                        
                    }
                    if (rtryCall<=0)
                    {
                        
                        btn_callme.hidden=TRUE;
                        self.lb_rtryCall.hidden=TRUE;
                        
                        
                    }
                    if (rtryOtp <=0 && rtryCall<=0)
                    {
                        resendOTPview.hidden=TRUE;
                    }
                    
                }
                count =tout;// count++;
                
                // wmsg=[[response valueForKey:@"pd"] valueForKey:@"wmsg"];
                
                if ( [self.myTimer isValid]){
                    [self.myTimer invalidate], self.myTimer=nil;
                }
                
                self.myTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateUI:) userInfo:nil repeats:YES];
                 [[NSRunLoop mainRunLoop] addTimer:_myTimer forMode:NSRunLoopCommonModes];
                
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}



-(void)hitAPIResendOTPwithIVR:(NSString*)channelType
{
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"Wait...", @"Wait");
    // You can also adjust other label properties if needed.
    // hud.label.font = [UIFont italicSystemFontOfSize:16.f];

    
       UMAPIManager *objRequest = [[UMAPIManager alloc] init];
        NSMutableDictionary *dictBody = [NSMutableDictionary new];
        [dictBody setObject:newmobile forKey:@"mno"];//Enter mobile number of user
        [dictBody setObject:@"ivr" forKey:@"chnl"]; //chnl : type sms for OTP and IVR for call
    
    
    
    [dictBody setObject:@"updtmob" forKey:@"ort"];
//    [dictBody setObject:self.mpinEncrypt forKey:@"mpin"];
    [dictBody setObject:@"" forKey:@"dgt"];
    [dictBody setObject:@"" forKey:@"st"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    //[dictBody setObject:@"initiate" forKey:@"type"];

    
    
    
    
    
    
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_IVR_OTP withBody:dictBody andTag:TAG_REQUEST_IVR_OTP completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            //------ Sharding Logic parsing---------------
            NSString *node=[response valueForKey:@"node"];
            if([node length]>0)
            {
                [[NSUserDefaults standardUserDefaults] setValue:node forKey:@"NODE_KEY"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            

            
            //------ Sharding Logic parsing---------------
            
            

            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                rtry=[[response valueForKey:@"pd"] valueForKey:@"rtry"];
                tout=[[[response valueForKey:@"pd"] valueForKey:@"tout"] intValue];
                NSArray *retryItems = [rtry componentsSeparatedByString:@"|"];
                
                retryResend=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:0]];
                retryCall=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:1]];
                
               // retryAdhar=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:2]];
                @try {
                    retryAdhar=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:2]];
                    
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                btn_resend.enabled=TRUE;
                btn_callme.enabled=TRUE;
                NSString *strRetryValue = NSLocalizedString(@"attempts_left", nil);
                
                self.lb_rtryResend.text= [NSString stringWithFormat:@"(%@ %@)",retryResend,strRetryValue];
                
                
                self.lb_rtryCall.text= [NSString stringWithFormat:@"(%@ %@)",retryCall,strRetryValue];
                
                
                int rtryOtp=[retryResend intValue];
                int rtryCall=[retryCall intValue];
                int rtryAdhar=[retryAdhar intValue];
                
                if (rtryAdhar>0)
                {
                    resendOTPview.hidden=FALSE;
                    
                }
                else
                {
                    if (rtryOtp <=0)
                    {
                        btn_resend.hidden=TRUE;
                        self.lb_rtryResend.hidden=TRUE;
                        
                    }
                    if (rtryCall<=0)
                    {
                        
                        btn_callme.hidden=TRUE;
                        self.lb_rtryCall.hidden=TRUE;
                        
                        
                    }
                    if (rtryOtp <=0 && rtryCall<=0)
                    {
                        resendOTPview.hidden=TRUE;
                    }
                    
                }
                count =tout;// count++;
                
                // wmsg=[[response valueForKey:@"pd"] valueForKey:@"wmsg"];
                
                if ( [self.myTimer isValid]){
                    [self.myTimer invalidate], self.myTimer=nil;
                }
                
                self.myTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateUI:) userInfo:nil repeats:YES];
                
                [[NSRunLoop mainRunLoop] addTimer:_myTimer forMode:NSRunLoopCommonModes];

                
            }
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}
//@synthesize dic_pass;


- (void)viewDidLoad
{
    
    [super viewDidLoad];

    [self.btnNext setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [self.btnNext setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btnNext.layer.cornerRadius = 3.0f;
    self.btnNext.clipsToBounds = YES;
    [self enableBtnNext:NO];
    btn_resend.alpha = 1.0;
    btn_callme.alpha = 1.0;
    //-------- Code for resend otp and IVR---------
    singleton = [SharedManager sharedSingleton];
    //singleton.user_aadhar_number = self.strAadharNumber;
    
    lblWaitingForSMS.text = NSLocalizedString(@"waiting_for_sms", nil);
    
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    
    lblHeaderTitle.text = NSLocalizedString(@"verify_using_otp", nil);
     lblDidntReceive.text = NSLocalizedString(@"didnt_receive_otp", nil);
    lblEnter6Digit.text = NSLocalizedString(@"enter_6_digit_otp", nil);
    lblSubHeaderDescritn.text =  NSLocalizedString(@"register_verify_otp_sub_heading", nil);
    lblHeaderDescription.text = NSLocalizedString(@"register_verify_otp_heading", nil);
    
    [btn_callme setTitle:NSLocalizedString(@"call_me", nil) forState:UIControlStateNormal];
    [btn_resend setTitle:NSLocalizedString(@"resend_otp", nil) forState:UIControlStateNormal];
    
    
    //----- Setting delegate for Custom textfield so back space operation work smooth
    _txt1.delegate = self;
    [_txt1 addTarget:self
              action:@selector(textFieldDidChange:)
    forControlEvents:UIControlEventEditingChanged];
    
    

    
   // self.vw_line.frame=CGRectMake(53, 492, 268, 0.5);
    self.view.userInteractionEnabled = YES;
    
    
    
    
    
   
    
    self.vw_line1.backgroundColor=[UIColor lightGrayColor];
    
    // Do any additional setup after loading the view.
    //------ Add dismiss keyboard while background touch-------
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [self.scrollView addGestureRecognizer:tapGesture];
    
    //UMGIOSINT-1193 FIX
    // _txt1.keyboardType=UIKeyboardTypeNumbersAndPunctuation;
    _txt1.keyboardType = UIKeyboardTypeNumberPad;

    
    
    if (self.view.frame.size.height < 500)
    {
        resendOTPview.frame = CGRectMake(resendOTPview.frame.origin.x, self.view.frame.size.height - 200, resendOTPview.frame.size.width, resendOTPview.frame.size.height);
    }
    
}
-(void)hideKeyboard
{
    [scrollView setContentOffset:
     CGPointMake(0, -scrollView.contentInset.top) animated:YES];
    
    
    [self.view endEditing:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.shouldRotate = NO;
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——

    //----- code for handling ----
    
    resendOTPview.hidden=TRUE;
    NSArray *retryItems = [rtry componentsSeparatedByString:@"|"];
    
    retryResend=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:0]];
    retryCall=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:1]];
    
   // retryAdhar=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:2]];
    
    @try {
        retryAdhar=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:2]];
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
    NSString *strRetryValue = NSLocalizedString(@"attempts_left", nil);
    
    self.lb_rtryResend.text= [NSString stringWithFormat:@"(%@ %@)",[retryItems objectAtIndex:0],strRetryValue];
    
    
    self.lb_rtryCall.text= [NSString stringWithFormat:@"(%@ %@)",[retryItems objectAtIndex:1],strRetryValue];
    
    count =tout;// count++;
    if ( [self.myTimer isValid]){
        [self.myTimer invalidate], self.myTimer=nil;
    }
    
    self.myTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateUI:) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:_myTimer forMode:NSRunLoopCommonModes];

    //--------- Code for handling -------------------
    [self setViewFont];
    [super viewWillAppear:NO];
}


#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    lblHeaderTitle.font = [AppFont semiBoldFont:22.0];
    _lblScreenTitleName.font = [AppFont semiBoldFont:22.0];
    lblHeaderDescription.font = [AppFont semiBoldFont:17.0];
    //lblMaskedMNumber.font = [AppFont regularFont:18.0];
    lblSubHeaderDescritn.font = [AppFont mediumFont:15.0];
    _txt1.font = [AppFont regularFont:21.0];
    lblEnter6Digit.font = [AppFont mediumFont:14.0];
    _lb_timer.font = [AppFont mediumFont:14];
    lblWaitingForSMS.font = [AppFont mediumFont:14];
    lblDidntReceive.font = [AppFont regularFont:16.0];
    btn_resend.titleLabel.font = [AppFont mediumFont:15.0];
    btn_callme.titleLabel.font = [AppFont mediumFont:15.0];
    _lb_rtryResend.font = [AppFont regularFont:13.0];
    _lb_rtryCall.font = [AppFont regularFont:13.0];
    
    
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}


- (IBAction)btnBackClicked:(id)sender {
    [self.myTimer invalidate];
    self.myTimer = nil;
    
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    
    
    for (UIViewController *aViewController in allViewControllers)
    {
        if ([aViewController isKindOfClass:[EnterNewMobNumVC class]])
        {
            // [aViewController removeFromParentViewController];
            [self.navigationController popToViewController:aViewController
                                                  animated:YES];
            //[self closeView];
            
        }
    }
    



}


- (void) viewDidDisappear:(BOOL)animated
{
    [self.myTimer invalidate];
    self.myTimer = nil;
    [super viewDidDisappear:NO];
}




- (NSString *)timeFormatted:(int)totalSeconds{
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    // int hours = totalSeconds / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark UITextFieldDelegate methods
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

/*-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    int MAXLENGTH=6;
    
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if(newText.length==MAXLENGTH) {
        NSLog(@"string====%@",newText);
        _txt1.text = newText;
        [self checkValidation];
        return NO;
    }
    return YES;
}
*/

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(string.length==0)
    {
        return YES;
    }
    else
    {
        NSString *validRegEx =@"^[0-9]$"; //change this regular expression as your requirement
        NSPredicate *regExPredicate =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", validRegEx];
        BOOL myStringMatchesRegEx = [regExPredicate evaluateWithObject:string];
        if (myStringMatchesRegEx)
            return YES;
        else
            return NO;
    }
}

- (void)textFieldDidChange:(UITextField *)textField
{

    if (textField.text.length >= MAX_LENGTH)
    {
        textField.text = [textField.text substringToIndex:MAX_LENGTH];
        [textField resignFirstResponder];
        
       [self checkValidation];
    }
   
}
-(void)enableBtnNext:(BOOL)status
{
    self.btnNext.userInteractionEnabled = status;

    if (status ==YES)
    {
        [self hideKeyboard];
        [self.btnNext setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];
    }
    else
    {
        [self.btnNext setBackgroundColor:[UIColor colorWithRed:176.0/255.0f green:176.0/255.0f blue:176.0/255.0f alpha:1.0f]];
    }
    
}

- (IBAction)didTapNextBtnAction:(UIButton *)sender {
    if (_txt1.text.length < 6)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"enter_otp_is_not_valid", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
        [alert show];
        
        
        
    }
    else
    {
        //[_txt1 resignFirstResponder];
       // [self hideKeyboard];
        
        [self hitAPI];
        
        
    }
    
}

-(void)checkValidation
{
    [self enableBtnNext:YES];
    return;
    if (_txt1.text.length < 6)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"enter_otp_is_not_valid", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
        [alert show];
        

        
    }
    else
    {
        //[_txt1 resignFirstResponder];
        [self hideKeyboard];
        
            [self hitAPI];
       
        
    }
    
}


//----- hitAPI for IVR OTP call Type registration ------
-(void)hitAPI
{
   
    NSString *strOtp= _txt1.text;
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];
    [dictBody setObject:newmobile forKey:@"nmno"];
    [dictBody setObject:@"updtmob" forKey:@"ort"];
    [dictBody setObject:@"" forKey:@"email"];
    [dictBody setObject:self.mpinEncrypt forKey:@"mpin"];
    [dictBody setObject:@"" forKey:@"dgt"];
    [dictBody setObject:@"" forKey:@"st"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:@"" forKey:@"type"];
    [dictBody setObject:strOtp forKey:@"otp"];


    
    // json.otp = strOtp;
    
    
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
 hud.label.text = NSLocalizedString(@"loading",nil);
    
    
    //singleton.mobileNumber=_txt_mobileNo.text; //save mobile number for future use of user
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_UMOBILE withBody:dictBody andTag:TAG_REQUEST_UMOBILE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
                          //----- below value need to be forword to next view according to requirement after checking Android apk-----
                      
           // NSString *rc=[response valueForKey:@"rc"];
            NSString *rd=[response valueForKey:@"rd"];
          //  NSString *rs=[response valueForKey:@"rs"];
            
            singleton.nccode=@"";

            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                //[self openNextView];
                
                
                [self alertwithMsg:rd];
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            alert.tag=101;
            [alert show];
            
        }
        
    }];
    
}

-(void)alertwithMsg:(NSString*)msg
{
    
        //[self openLoginView];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"success", nil)
                                                    message:msg
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                          otherButtonTitles:nil];
    alert.tag=100;
    [alert show];
    
    
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==100)
    {
        
        if (buttonIndex == 0)
        { // handle the donate
            //----later add
            [[NSUserDefaults standardUserDefaults] setInteger:kLoginScreenCase forKey:kInitiateScreenKey];
            [[NSUserDefaults standardUserDefaults] setBool:false forKey:kKeepMeLoggedIn];
            [[NSUserDefaults standardUserDefaults] synchronize];
            //------
            LoginAppVC *vc;
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginAppVC"];
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                vc = [[LoginAppVC alloc] initWithNibName:@"LoginAppVC_iPad" bundle:nil];
            }
            
            [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [self presentViewController:vc animated:YES completion:nil];
            

            
        }
        else
        {
            //
        }
    }
    if (alertView.tag==100)
    {
       
        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
        
        
        for (UIViewController *aViewController in allViewControllers)
        {
            if ([aViewController isKindOfClass:[SecuritySettingVC class]])
            {
                // [aViewController removeFromParentViewController];
                [self.navigationController popToViewController:aViewController
                                                      animated:YES];
                //[self closeView];
            }
        }

    }
    
}


-(void)openLoginView
{
    LoginAppVC *vc;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginAppVC"];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        vc = [[LoginAppVC alloc] initWithNibName:@"LoginAppVC_iPad" bundle:nil];
    }
    
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:vc animated:YES completion:nil];
    
   
    
}





-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (fDeviceHeight<=568) {
        [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height+150)];
        [scrollView setContentOffset:CGPointMake(0, 40) animated:YES];
    }
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height)];
    [scrollView setContentOffset:CGPointZero animated:YES];
    
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (fDeviceHeight<=568) {
        
        UITouch * touch = [touches anyObject];
        if(touch.phase == UITouchPhaseBegan) {
            [self.txt1 resignFirstResponder];
                   }
    }
}


#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}


/*

#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}

*/

@end
