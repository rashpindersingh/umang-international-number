//
//  ChatTabVC.m
//  Umang
//
//  Created by admin on 29/04/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "ChatTabVC.h"

#import "LiveChatVC.h"

@interface ChatTabVC ()

@end

@implementation ChatTabVC


-(void)viewWillAppear:(BOOL)animated

{
    //------------- Network View Handle------------
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"TABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
  /*  [[self.tabBarController.tabBar.items objectAtIndex:0] setTitle:NSLocalizedString(@"home_small", @"")];
    [[self.tabBarController.tabBar.items objectAtIndex:1] setTitle:NSLocalizedString(@"favourites_small", @"")];
    [[self.tabBarController.tabBar.items objectAtIndex:2] setTitle:NSLocalizedString(@"all_services_small", @"")];
    [[self.tabBarController.tabBar.items objectAtIndex:3] setTitle:NSLocalizedString(@"help_live_chat", @"")];
    
    [[self.tabBarController.tabBar.items objectAtIndex:4] setTitle:NSLocalizedString(@"more", @"")];
   */
    self.navigationController.navigationBar.hidden = YES;
    [self setNeedsStatusBarAppearanceUpdate];
    //———— Add to handle network bar of offline——
    //———— Add to handle network bar of offline——
    
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Chat" bundle:nil];
    LiveChatVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LiveChatVC"];
    vc.hidesBottomBarWhenPushed = YES;
    vc.comingFromString = @"tab";
    [self.navigationController pushViewController:vc animated:NO];
    
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [super viewWillAppear:NO];
    
    
}


- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}



- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
