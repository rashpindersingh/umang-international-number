//
//  VerifyAlternateMbVC.h
//  Umang
//
//  Created by deepak singh rawat on 05/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerifyAlternateMbVC : UIViewController<UIScrollViewDelegate>
{
    
    IBOutlet UIScrollView *scrollview;
    
    IBOutlet UIView *resendOTPview;
    
    // testing commint
}
-(IBAction)bgtouch:(id)sender;

-(IBAction)backbtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@property(nonatomic,retain)NSString *TAGFROM;
@property(nonatomic,retain)NSString *altmobile;
@property(nonatomic,retain)NSString *rtry;
@property(assign)int tout;

@property (weak, nonatomic) IBOutlet UILabel *lblScreenTitleName;

@end
