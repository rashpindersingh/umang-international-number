//
//  UmangEulaVC.h
//  Umang
//
//  Created by admin on 15/02/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UmangEulaVC : UIViewController
{
    IBOutlet UIImageView *img_logo;
    IBOutlet UITextView *txt_eula;
    IBOutlet UIButton *btnAccept;
    IBOutlet UILabel *lbl_iagree;
    IBOutlet UILabel *lbl_eulaHeading;
    IBOutlet UIButton *btn_next;
    IBOutlet UIView *vw_bottom;
    BOOL flagAccept;
    
    IBOutlet UIButton *btn_back;
    
}

-(IBAction)btnNextAction:(id)sender;
-(IBAction)btnAccept:(id)sender;
-(IBAction)btnback:(id)sender;


@end

