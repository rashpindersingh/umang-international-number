//
//  AadharOTPViaNotLinkVC.m
//  RegistrationProcess
//
//  Created by admin on 07/01/17.
//  Copyright © 2017 SpiceLabs. All rights reserved.
//

#import "AadharOTPViaNotLinkVC.h"
#define MAX_LENGTH 6
#define kOFFSET_FOR_KEYBOARD 80.0

#import "MBProgressHUD.h"
#import <QuartzCore/QuartzCore.h>

#import "MyTextField.h"
#import "UMAPIManager.h"
#import "AadharCardViewCon.h"
#import "UIView+Toast.h"
#import "MoreTabVC.h"

@interface AadharOTPViaNotLinkVC ()<UITextFieldDelegate>
{
    SharedManager *singleton;
    MBProgressHUD *hud ;
    IBOutlet UIButton *btn_resend;
    IBOutlet UIButton *btn_callme;
    NSString *retryResend;
    NSString *retryCall;
    NSString *retryAdhar;
    __weak IBOutlet UILabel *lblWaitingForSMS;
    
    
    __weak IBOutlet UILabel *lblMobileNumberMasked;
    __weak IBOutlet UIButton *btnBack;
    __weak IBOutlet UILabel *lblHeader;
    __weak IBOutlet UILabel *lblHeaderDescriptn;
    __weak IBOutlet UILabel *lblSubHeaderDescriptn;
    
    __weak IBOutlet UILabel *lblDidntReceive;
    __weak IBOutlet UILabel *lblEnter6Digit;
    
    
    int count;
    
}
@property (weak, nonatomic) IBOutlet UILabel *lb_rtryResend;
@property (weak, nonatomic) IBOutlet UILabel *lb_rtryCall;
@property(nonatomic,retain)NSString *mno_resend;
@property(nonatomic,retain)NSString *chnl_resend;
@property(nonatomic,retain)NSString *tkn_resend;
@property(nonatomic,retain)NSString *ort_resend;

//--- code for otp and resend handling---------


@property (nonatomic, strong) IBOutlet UIProgressView *progressView;
@property (nonatomic, strong) NSTimer *myTimer;
@property (weak, nonatomic) IBOutlet UILabel *lb_timer;

@property (weak, nonatomic) IBOutlet UIView *vw_line;
@property (weak, nonatomic) IBOutlet UITextField *txt1;



@end

@implementation AadharOTPViaNotLinkVC

@synthesize strAadharNumber;

//-------- Code for resend otp and IVR---------
@synthesize altmobile;
@synthesize TAGFROM;
@synthesize rtry;
@synthesize tout;
@synthesize scrollView;

@synthesize maskedMobileNumber;
@synthesize mno_resend;
@synthesize chnl_resend;
@synthesize tkn_resend;
@synthesize ort_resend;

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}
- (id)initWithNibName:(NSString* )nibNameOrNil bundle:(NSBundle* )nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        self =[super initWithNibName:@"AadharOTPViaNotLinkVC_iPad" bundle:nil];
    }
    return self;
}


-(void)showToast :(NSString *)toast
{
    [self.view makeToast:toast duration:5.0 position:CSToastPositionBottom];
}

-(IBAction)btn_resendAction:(id)sender
{
    _txt1.text=@"";

    resendOTPview.hidden=TRUE;

    chnl_resend=@"sms";
    
    [self parameterForApiCall:chnl_resend];
}

-(IBAction)btn_callmeAction:(id)sender
{
    resendOTPview.hidden=TRUE;

    chnl_resend=@"ivr";
    [self parameterForApiCall:chnl_resend];
    
}

- (void)updateUI:(NSTimer *)timer
{
    NSLog(@"count=%d",count);
    
    
    if (count <=0)
    {
        [self.myTimer invalidate];
        self.myTimer = nil;
        resendOTPview.hidden=FALSE;
        
        
        
        
        //----------
        int rtryOtp=[retryResend intValue];
        int rtryCall=[retryCall intValue];
        int rtryAdhar=[retryAdhar intValue];
        
        if (rtryAdhar>0)
        {
            resendOTPview.hidden=FALSE;
            
        }
        else
        {
            if (rtryOtp <=0)
            {
                btn_resend.enabled=false;
            }
            if (rtryCall<=0)
            {
                btn_callme.enabled=false;
            }
            if (rtryOtp <=0 && rtryCall<=0)
            {
                resendOTPview.hidden=TRUE;
            }
            
        }
        
        //----------
        
        
    }
    else
    {
        
        count --;
        
        self.progressView.progress = (float)count/120.0f;
        
        self.lb_timer.text=[self timeFormatted:count];
        
    }
    
    
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, resendOTPview.isHidden ? scrollView.frame.size.height:scrollView.frame.size.height+150)];
    

    
}


-(void)parameterForApiCall:(NSString*)chnlresend
{
    
        btn_callme.hidden=TRUE;
    self.lb_rtryCall.hidden=TRUE;
    
   /*
    if (TYPE_RESEND_OTP_FROM ==ISFROMFORGOTMPINVC)
    {
        mno_resend=singleton.mobileNumber;
        tkn_resend=@"";//singleton.user_tkn;
        ort_resend=@"frgtmpn";
        
        
    }
    if (TYPE_RESEND_OTP_FROM ==ISFROMLOGINWITHOTPVC)
    {
        mno_resend=singleton.mobileNumber;
        tkn_resend=@"";//singleton.user_tkn;
        ort_resend=@"loginmob";
        
        
    }
    if (TYPE_RESEND_OTP_FROM ==ISFROMLOGINAPPVC)
    {
        mno_resend=strAadharNumber;
        tkn_resend=@"";//singleton.user_tkn;
        ort_resend=@"loginadhr";
        
        
    }
    if (TYPE_RESEND_OTP_FROM ==ISFROMREGISTRATIONSTEP1VC)
    {
        mno_resend=singleton.mobileNumber;
        tkn_resend=@"";//singleton.user_tkn;
        ort_resend=@"rgtmob";
        
        
    }
    */
    [self hitAPIResendOTPwithIVR:chnlresend];
}



-(void)hitAPIResendOTPwithIVR:(NSString*)channelType
{
   
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"Wait...", @"Wait");
    // You can also adjust other label properties if needed.
    // hud.label.font = [UIFont italicSystemFontOfSize:16.f];
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    // ort_resend=@"loginadhr";
    [dictBody setObject:@"rgtadhr" forKey:@"ort"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:strAadharNumber forKey:@"aadhr"];
    [dictBody setObject:@"aadhar" forKey:@"type"];
    [dictBody setObject:chnl_resend forKey:@"chnl"];
    [dictBody setObject:@"" forKey:@"lang"]; //lang
    
    
    
    
    if ([self.TAGFROM isEqualToString:@"RESYNCAADHAARCARD"])
    {
        [dictBody setObject:@"relink" forKey:@"chnl"];
        
    }
   
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_REGISTRATION withBody:dictBody andTag:TAG_REQUEST_IVR_OTP completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            //------ Sharding Logic parsing---------------
            NSString *node=[response valueForKey:@"node"];
            if([node length]>0)
            {
                [[NSUserDefaults standardUserDefaults] setValue:node forKey:@"NODE_KEY"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            

            
            
            //------ Sharding Logic parsing---------------
            

            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
 
                rtry=[[response valueForKey:@"pd"] valueForKey:@"rtry"];
                tout=[[[response valueForKey:@"pd"] valueForKey:@"tout"] intValue];
                NSArray *retryItems = [rtry componentsSeparatedByString:@"|"];
                
                retryResend=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:0]];
                retryCall=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:1]];
                
                //retryAdhar=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:2]];
                @try {
                    retryAdhar=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:2]];
                    
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                btn_resend.enabled=TRUE;
                btn_callme.enabled=TRUE;
                NSString *strRetryValue = NSLocalizedString(@"attempts_left", nil);
                
                self.lb_rtryResend.text= [NSString stringWithFormat:@"(%@ %@)",retryAdhar,strRetryValue];
                
                
                self.lb_rtryCall.text= [NSString stringWithFormat:@"(%@ %@)",retryCall,strRetryValue];
                
                
              /*  int rtryOtp=[retryResend intValue];
                int rtryCall=[retryCall intValue];
                int rtryAdhar=[retryAdhar intValue];
                
                if (rtryAdhar>0)
                {
                    resendOTPview.hidden=FALSE;
                    
                }
                else
                {
                    if (rtryOtp <=0)
                    {
                        btn_resend.enabled=false;
                    }
                    if (rtryCall<=0)
                    {
                        btn_callme.enabled=false;
                    }
                    if (rtryOtp <=0 && rtryCall<=0)
                    {
                        resendOTPview.hidden=TRUE;
                    }
                    
                }*/
                count =tout;// count++;
                
                // wmsg=[[response valueForKey:@"pd"] valueForKey:@"wmsg"];
                
                if ( [self.myTimer isValid]){
                    [self.myTimer invalidate], self.myTimer=nil;
                }
                self.myTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateUI:) userInfo:nil repeats:YES];
                 [[NSRunLoop mainRunLoop] addTimer:_myTimer forMode:NSRunLoopCommonModes];
                
                
            }
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}


- (void)viewDidLoad
{
    lblWaitingForSMS.text = NSLocalizedString(@"waiting_for_sms", nil);
    [_txt1 becomeFirstResponder];
    lblMobileNumberMasked.text =  maskedMobileNumber;

    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    [self.btnNext setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [self.btnNext setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btnNext.layer.cornerRadius = 3.0f;
    self.btnNext.clipsToBounds = YES;
    [self enableBtnNext:NO];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:AADHAR_OTP_VIA_NOT_LINKED];
    
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];

    tkn_resend=@"";
    btn_resend.alpha = 1.0;
    btn_callme.alpha = 1.0;
    
    //-------- Code for resend otp and IVR---------
    
    
    singleton = [SharedManager sharedSingleton];
    singleton.user_aadhar_number = self.strAadharNumber;
    
    
    
    lblHeader.text = NSLocalizedString(@"verify_otp_label", nil);
    lblHeaderDescriptn.text = NSLocalizedString(@"register_aadhaar_verify_otp_heading", nil);
    lblSubHeaderDescriptn.text = NSLocalizedString(@"register_verify_otp_sub_heading", nil);
    lblDidntReceive.text = NSLocalizedString(@"didnt_receive_otp", nil);
    lblEnter6Digit.text = NSLocalizedString(@"enter_6_digit_otp", nil);
    
    
    [btn_callme setTitle:NSLocalizedString(@"call_me", nil) forState:UIControlStateNormal];
     [btn_resend setTitle:NSLocalizedString(@"resend_otp", nil) forState:UIControlStateNormal];
    
    //----- Setting delegate for Custom textfield so back space operation work smooth
    _txt1.delegate = self;
    
    //self.vw_line.frame=CGRectMake(53, 492, 268, 0.5);
    self.view.userInteractionEnabled = YES;
    
    
    
    
    
    [self textfieldInit];
     [super viewDidLoad];
    // Do any additional setup after loading the view.
    //------ Add dismiss keyboard while background touch-------
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [self.scrollView addGestureRecognizer:tapGesture];
    
    //UMGIOSINT-1193 FIX
    // _txt1.keyboardType=UIKeyboardTypeNumbersAndPunctuation;
    _txt1.keyboardType = UIKeyboardTypeNumberPad;

}
-(void)hideKeyboard
{
    [scrollView setContentOffset:
     CGPointMake(0, -scrollView.contentInset.top) animated:YES];
    [self.view endEditing:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.shouldRotate = NO;
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——

    //----- code for handling ----
    
    resendOTPview.hidden=TRUE;
    btn_callme.hidden=TRUE;
    self.lb_rtryCall.hidden=TRUE;
    NSArray *retryItems = [rtry componentsSeparatedByString:@"|"];
    
    retryResend=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:0]];
    retryCall=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:1]];
    
    //retryAdhar=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:2]];
    
    
    @try {
        retryAdhar=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:2]];
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    NSString *strRetryValue = NSLocalizedString(@"attempts_left", nil);
    
    self.lb_rtryResend.text= [NSString stringWithFormat:@"(%@ %@)",[retryItems objectAtIndex:2],strRetryValue];
    
    
    self.lb_rtryCall.text= [NSString stringWithFormat:@"(%@ %@)",[retryItems objectAtIndex:1],strRetryValue];
    
    count =tout;// count++;
    if ( [self.myTimer isValid]){
        [self.myTimer invalidate], self.myTimer=nil;
    }
    
    self.myTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateUI:) userInfo:nil repeats:YES];
    
    [[NSRunLoop mainRunLoop] addTimer:_myTimer forMode:NSRunLoopCommonModes];

    //--------- Code for handling -------------------
    
    [self setViewFont];
    [super viewWillAppear:NO];
}

#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    lblHeader.font = [AppFont semiBoldFont:22.0];
    _lblScreenTitleName.font = [AppFont semiBoldFont:22.0];
    lblHeaderDescriptn.font = [AppFont semiBoldFont:17.0];
    lblMobileNumberMasked.font = [AppFont regularFont:18.0];
    lblSubHeaderDescriptn.font = [AppFont mediumFont:14.0];
    _txt1.font = [AppFont regularFont:21.0];
    lblEnter6Digit.font = [AppFont mediumFont:14.0];
    _lb_timer.font = [AppFont mediumFont:14];
    lblWaitingForSMS.font = [AppFont mediumFont:14];
    lblDidntReceive.font = [AppFont regularFont:16.0];
    btn_resend.titleLabel.font = [AppFont mediumFont:15.0];
    btn_callme.titleLabel.font = [AppFont mediumFont:15.0];
    _lb_rtryResend.font = [AppFont regularFont:13.0];
    _lb_rtryCall.font = [AppFont regularFont:13.0];
    
    
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}

//150386


- (IBAction)btnBackClicked:(id)sender {
    
    [self.navigationController popToRootViewControllerAnimated:NO];
    
//    
//    UIViewController *vc = self.presentingViewController;
//    while (vc.presentingViewController) {
//        vc = vc.presentingViewController;
//        
//        if ([vc isKindOfClass:[MoreTabVC class]])
//        {
//            [vc dismissViewControllerAnimated:YES completion:NULL];
//            
//            
//        }
//        
//    }

    //[self dismissViewControllerAnimated:NO completion:nil];
}


- (void) viewDidDisappear:(BOOL)animated
{
    [self.myTimer invalidate];
    self.myTimer = nil;
    [super viewDidDisappear:NO];

}




- (NSString *)timeFormatted:(int)totalSeconds{
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    // int hours = totalSeconds / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
}



-(void)textfieldInit
{
    [_txt1 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
   }
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark UITextFieldDelegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(string.length==0)
    {
        return YES;
    }
    else
    {
        NSString *validRegEx =@"^[0-9]$"; //change this regular expression as your requirement
        NSPredicate *regExPredicate =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", validRegEx];
        BOOL myStringMatchesRegEx = [regExPredicate evaluateWithObject:string];
        if (myStringMatchesRegEx)
            return YES;
        else
            return NO;
    }
}

- (void)textFieldDidChange:(UITextField *)textField
{
    
    if (textField.text.length >= MAX_LENGTH)
    {
        textField.text = [textField.text substringToIndex:MAX_LENGTH];
        [textField resignFirstResponder];
        
        [self checkValidation];
    }
    
}
-(void)enableBtnNext:(BOOL)status
{
    self.btnNext.userInteractionEnabled = status;

    if (status ==YES)
    {
        [self hideKeyboard];
        [self.btnNext setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];
    }
    else
    {
        [self.btnNext setBackgroundColor:[UIColor colorWithRed:176.0/255.0f green:176.0/255.0f blue:176.0/255.0f alpha:1.0f]];
    }
    
}
- (IBAction)didTapNextBtnAction:(UIButton *)sender {
    if (_txt1.text.length < 6)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"enter_otp_is_not_valid", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
        [alert show];
        
        
        
    }
    else
        
    {
        //[self hideKeyboard];
        
        
        [self hitAPIForLinkProfile];
        
        
    }
}


-(void)checkValidation
{
    [self enableBtnNext:YES];
    return;
    if (_txt1.text.length < 6)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"enter_otp_is_not_valid", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
        [alert show];
        
        
        
    }
    else
    
    {
        [self hideKeyboard];
        
        
     [self hitAPIForLinkProfile];
        
    
    }
    
}


-(void)hitAPIForLinkProfile
{
    NSString *otpString=[NSString stringWithFormat:@"%@",_txt1.text];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
   hud.label.text = NSLocalizedString(@"loading",nil);
    
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    //  [dictBody setObject:singleton.aadharNumber forKey:@"mno"];//Enter aadhar number of user
    
    
    [dictBody setObject:@"aadhar" forKey:@"type"];
    NSLog(@"aadhar Number is = %@",self.strAadharNumber);
    [dictBody setObject:self.strAadharNumber forKey:@"aadhr"];
    [dictBody setObject:otpString forKey:@"otp"];
    [dictBody setObject:@"rgtadhr" forKey:@"ort"];
    [dictBody setObject:@"Y" forKey:@"rc"];
    [dictBody setObject:@"Y" forKey:@"mec"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    if ([self.TAGFROM isEqualToString:@"RESYNCAADHAARCARD"])
    {
        [dictBody setObject:@"relink" forKey:@"chnl"];
        
    }
    NSLog(@"Dictioary is %@",dictBody);
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_LINK_AADHAAR withBody:dictBody andTag:TAG_REQUEST_LINK_AADHAR completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        
        [hud hideAnimated:YES];
        // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
        
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
         //   NSString *rc=[response valueForKey:@"rc"];
            NSString *rd=[response valueForKey:@"rd"];
           // NSString *rs=[response valueForKey:@"rs"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                [self alertwithMsg:rd];
                [self showToast:rd];//dsrawat4u

                singleton.objUserProfile = nil;
                singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
                
                
                
            }
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
    return;
    
}
-(void)alertwithMsg:(NSString*)msg
{
    
    if ([self.TAGFROM isEqualToString:@"RESYNCAADHAARCARD"])
    {
       // [dictBody setObject:@"relink" forKey:@"chnl"];
        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
        
        
        for (UIViewController *aViewController in allViewControllers)
        {
            

            if ([aViewController isKindOfClass:[AadharCardViewCon class]])
            {
                
                [self.navigationController popToViewController:aViewController
                                                      animated:YES];
                
                
            }
         
            
        }

    }
    else
    {
    
    [self openNextView];
    }
    
}




-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (fDeviceHeight<=568) {
        [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height+150)];
        [scrollView setContentOffset:CGPointMake(0, 40) animated:YES];
    }
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height)];
    [scrollView setContentOffset:CGPointZero animated:YES];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (fDeviceHeight<=568) {
     UITouch * touch = [touches anyObject];
    if(touch.phase == UITouchPhaseBegan) {
        [self.txt1 resignFirstResponder];
     
        
    }
    }
}

-(void)openNextView
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    AadharCardViewCon *aadharLink = [storyboard instantiateViewControllerWithIdentifier:@"AadharCardViewCon"];
    //   RS3vc.isFromAadharRegistration = YES;
    //[aadharLink setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self.navigationController pushViewController:aadharLink animated:YES];
    
}


#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}*/

@end




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


