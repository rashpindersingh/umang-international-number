//
//  DigiLockLoginVC.m
//  Umang
//
//  Created by admin on 06/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "DigiLockLoginVC.h"
#import "MBProgressHUD.h"
#import "UMAPIManager.h"
#import "DigiLockHomeVC.h"
#import "Umang-Swift.h"


@interface DigiLockLoginVC ()
{
    UIActivityIndicatorView *activityIndicator ;
    SharedManager *singleton;
    MBProgressHUD *hud;

}
@end

@implementation DigiLockLoginVC


- (void)viewDidLoad
{
    singleton = [SharedManager sharedSingleton];

    activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);
    [self.view addSubview: activityIndicator];
    
    [self loadHtml];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*https://api.digitallocker.gov.in/public/oauth2/1/authorize?response_type=code&client_id=BIXLNE76&redirect_uri=http://localhost/callback/&state=123*/
//vw_webDigiLock

//---------Loading HTML On webview----------
-(void)loadHtml
{
   
     NSString *durl=[NSString stringWithFormat:@"%@",[singleton.arr_initResponse valueForKey:@"durl"]];
    
   NSString* urlString=[NSString stringWithFormat:@"%@&redirect_uri=http://localhost/callback/&state=123",durl];
  
    
    
  NSURL * postURI=[NSURL URLWithString:urlString];
    

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:postURI];
    vw_webDigiLock.delegate=self;
  
        //send webview a message

    [vw_webDigiLock loadRequest: request];

    
}


- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [activityIndicator startAnimating];
    
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    
    NSLog(@"error=%@",error);
    if([error code] == NSURLErrorCancelled)
        return;
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [activityIndicator stopAnimating];
    
    
}
- (IBAction)backButtonAction:(UIButton *)sender
{
    [singleton traceEvents:@"Back Button" withAction:@"Clicked" withLabel:@"DigiLocker Login" andValue:0];

    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Updating the UI


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    //NSLog(@"%s", __PRETTY_FUNCTION__);
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [activityIndicator stopAnimating];
    
}


-(BOOL)webView:(UIWebView *)mainWebView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSString *requestString = [[request URL] absoluteString];
    NSLog(@"requestString -->%@",requestString);
    if ([[[request URL] absoluteString] hasPrefix:@"ios::openChatScreen::"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"::"];
        // NSString *functionName = (NSString*)[components objectAtIndex:1];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
        argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
        NSError *jsonError;
        NSData *objectData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        @try
        {
            //NSString *url=[json valueForKey:@"URL"];
            //NSString *title=[json valueForKey:@"title"];
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        
        
        return NO;
        
        
    }
    
    else if ([[[request URL] absoluteString] hasPrefix:@"http://localhost/callback/"])
    {
        
        
        
        NSString *digilockSuccessURL = [[request URL] absoluteString];
        
        if([digilockSuccessURL length]!=0)
        {
            NSString *callBackUrl = [[digilockSuccessURL componentsSeparatedByString:@"?"] objectAtIndex:0];
            
            NSString *prefix = @"code=";
            NSString *suffix = @"&state=";
            NSRange prefixRange = [digilockSuccessURL rangeOfString:prefix];
            NSRange suffixRange = [[digilockSuccessURL substringFromIndex:prefixRange.location+prefixRange.length] rangeOfString:suffix];
            NSRange codeRange = NSMakeRange(prefixRange.location+prefix.length, suffixRange.location);
            NSString *codeValue = [digilockSuccessURL substringWithRange:codeRange];
            NSLog(@"codeValue: %@", codeValue);
            
            callBackUrl = callBackUrl.length == 0 ? @"" : callBackUrl;
            [self hitGetTokenApiWithCode:codeValue andCallBackUrl:callBackUrl];
           
        }
           
        
        
        return NO;
    }
    
    
    return YES;
    
}


-(void)hitGetTokenApiWithCode:(NSString *)codeString andCallBackUrl:(NSString *)callBackURL
{
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];

    double CurrentTime = CACurrentMediaTime();
    
    NSString * timeInMS = [NSString stringWithFormat:@"%f", CurrentTime];
    
    timeInMS=  [timeInMS stringByReplacingOccurrencesOfString:@"." withString:@""];
    
    [dictBody setObject:timeInMS forKey:@"trkr"];
    [dictBody setObject:@"rgtadhr" forKey:@"ort"];
    [dictBody setObject:@"Y" forKey:@"rc"];
    [dictBody setObject:@"Y" forKey:@"mec"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:callBackURL forKey:@"ruri"];
    [dictBody setObject:codeString forKey:@"code"];
    
    NSLog(@"Dictionary is %@",dictBody);
    
    
    [objRequest hitAPIForDigiLockerAuthenticationWithPost:YES isAccessTokenRequired:YES webServiceURL:UM_API_GETDIGI_TOKEN withBody:dictBody andTag:TAG_REQUEST_DIGILOCKER_GETTOKEN completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        
        [hud hideAnimated:YES];
        // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
        
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            //NSString *rc=[response valueForKey:@"rc"];
            // NSString *rs=[response valueForKey:@"rs"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                
                NSString *accessToken = [[response valueForKey:@"pd"] valueForKey:@"access_token"];
                NSString *refreshToken = [[response valueForKey:@"pd"] valueForKey:@"refresh_token"];
                
                
                [[NSUserDefaults standardUserDefaults] setValue:accessToken forKey:@"AccessTokenDigi"];
                [[NSUserDefaults standardUserDefaults] setValue:refreshToken forKey:@"RefreshTokenDigi"];
                
        
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@""
                    message:NSLocalizedString(@"logged_in_success", nil) preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yesButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                {
                    [self jumpToNextView];
                    
                }];
                
                
                [alert addAction:yesButton];
                
                [self presentViewController:alert animated:YES completion:nil];
                
                
            }
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            
        }
        else
        {
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];

}

-(void)jumpToNextView
{
    [singleton traceEvents:@"Open DigiLocker Home" withAction:@"Clicked" withLabel:@"DigiLocker Login" andValue:0];
    /*UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDigiLockerStoryBoard bundle:nil];

    DigiLockerHomeVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DigiLockerHomeVC"];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];*/
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DigiLockHomeVC *digiView = [storyboard instantiateViewControllerWithIdentifier:@"DigiLockHomeVC"];
    digiView.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:digiView animated:YES];
    
}


@end
