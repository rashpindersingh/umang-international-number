 //
//  AadharRegAfterMobileVC.h
//  RegistrationProcess
//
//  Created by admin on 07/01/17.
//  Copyright © 2017 SpiceLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AadharRegAfterMobileVC :  UIViewController<UIScrollViewDelegate>
{
    IBOutlet UIScrollView *scrollView;

}
@property (weak, nonatomic) IBOutlet UIButton *btn_acceptTerm;
@property (weak, nonatomic) IBOutlet UIButton *btnSkip;


//----- added for passing rty and tout-----
@property(nonatomic,retain)NSString *rtry;
@property(assign)int tout;
//----- added for passing rty and tout-----

@end
