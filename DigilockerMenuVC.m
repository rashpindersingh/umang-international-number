//
//  DigilockerMenuVC.m
//  Umang
//
//  Created by admin on 06/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "DigilockerMenuVC.h"
#import "DigiLockSinghUpVC.h"
#import "DigiLockLoginVC.h"
#import "DigiLockCreateAccountVC.h"


@interface DigilockerMenuVC ()
{
    SharedManager *singleton;
}
@end

@implementation DigilockerMenuVC

-(void)viewWillAppear:(BOOL)animated
{
    //------------- Network View Handle------------
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"TABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    [self setViewFont];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btn_back.titleLabel setFont:[AppFont regularFont:17.0]];
    [btn_help.titleLabel setFont:[AppFont regularFont:17.0]];
    [btn_login.titleLabel setFont:[AppFont mediumFont:21.0]];
    [btn_createAcct.titleLabel setFont:[AppFont mediumFont:21.0]];
    
    
}
- (void)viewDidLoad
{
    
    singleton=[SharedManager sharedSingleton];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [btn_login setTitle:NSLocalizedString(@"login_caps", nil) forState:UIControlStateNormal];
    [btn_createAcct setTitle:NSLocalizedString(@"create_account", nil) forState:UIControlStateNormal];
    [btn_back setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    [btn_help setTitle:@"" forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btn_back.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btn_back.frame.origin.x, btn_back.frame.origin.y, btn_back.frame.size.width, btn_back.frame.size.height);
        
        [btn_back setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btn_back.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)backbtnAction:(id)sender
{
    [singleton traceEvents:@"Back Button" withAction:@"Clicked" withLabel:@"Digilocker Menu" andValue:0];

    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)helpAction:(id)sender
{
    
}
-(IBAction)loginAction:(id)sender
{
    [singleton traceEvents:@"Login" withAction:@"Clicked" withLabel:@"Digilocker Menu" andValue:0];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    DigiLockLoginVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DigiLockLoginVC"];
    [self.navigationController pushViewController:vc animated:YES];
}
-(IBAction)createAccountAction:(id)sender
{
    [singleton traceEvents:@"Create Account" withAction:@"Clicked" withLabel:@"Digilocker Menu" andValue:0];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    DigiLockCreateAccountVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DigiLockCreateAccountVC"];
    [self.navigationController pushViewController:vc animated:YES];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
