//
//  BLMediaItem.h
//  BlendPhotoAndVideo
//
//  Created by Hoang Tran on 12/26/14.
//  Copyright (c) 2014 ILandApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>


#import <Photos/Photos.h>

@interface BLMediaItem : NSObject

@property (nonatomic, strong) ALAsset *asset;
@property (nonatomic, assign) float opacity;
@property (nonatomic, assign) float scale;
@property (nonatomic, assign) CGPoint contentOffset;
@property (nonatomic, assign) CGSize zoomedSize;
@property (nonatomic, assign) BOOL isEdited;

-(UIImage*)thumbnail;
-(UIImage*)fullSizeImage;

-(BOOL)isVideo;
-(BOOL)isPhoto;

-(CGSize)getThumbnailSizeFromSize:(CGSize)size;

@end
