//
//  ProfileAfterAdharVC.m
//  Umang
//
//  Created by admin on 10/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "ProfileAfterAdharVC.h"
#import "EditUserCell.h"

#define kOFFSET_FOR_KEYBOARD 80.0

#import "UMAPIManager.h"
#import "MBProgressHUD.h"
#import "StateList.h"

#import "EnterRegFieldVC.h"
#import "AlternateMobileVC.h"
#import "UpdMpinVC.h"

#import "AddEmailVC.h"
#import "UIImageView+WebCache.h"

@interface ProfileAfterAdharVC ()<UITextFieldDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate>
{
    NSMutableArray *arrGeneralInformation;
    NSMutableArray *arrAccountInformation;
    UIView *vwHeader;
    UIView *vwFooter;
    
    
    EnterRegFieldVC *customVC;
    MBProgressHUD *hud ;
    
    UIButton*  btnDone;
    UIButton*  btncancel;
    UIDatePicker *datepicker;
    UIView*  vw_dateToolbar;
    StateList *obj;
    BOOL flag_apiHitStatus;
    NSDate *pickdate;
    UpdMpinVC *updMPinVC;
    NSString *base64Image;
    NSString *facebookId;
    NSString *googleId;
    NSString *twitterId;
    BOOL flagURL;
    __weak IBOutlet UIButton *btnskip;
    
    __weak IBOutlet UILabel *lblTitleProfileInformatn;
    
    NSString *user_name;
    NSString *user_gender;
    NSString *user_dob;
    NSString *user_quali;
    NSString *user_Occup;
    NSString *user_state;
    NSString *user_dist;
    NSString *user_address;
    NSString *user_email;
    NSString *user_altermob;
    UIImageView *imageUserProfile;
    
    
    NSString *user_token;
    
}
@property (weak, nonatomic) IBOutlet UITableView *tblProfileEdit;
@property (weak, nonatomic) IBOutlet UIView *vwProfileEdit;
- (IBAction)bgTouch:(id)sender;
@property(retain,nonatomic)NSString *base64Image;
@property(retain,nonatomic)NSString *header_title_pass;
@property(retain,nonatomic)NSMutableArray *arr_table_pass;
@property(retain,nonatomic)NSString *TAG_pass;
@property(nonatomic,retain)NSMutableArray *citiesList;
@property(nonatomic,retain)NSMutableArray *districtList;
@property (nonatomic, strong)NSString *state_id;
@property (nonatomic, strong)NSString *city_id;
@property (nonatomic, strong)NSString *district_id;
@property (nonatomic, strong)NSString *old_state;
@property (nonatomic, strong)NSString *old_district;

@property(nonatomic,retain)NSMutableArray *arr_importtype;


@end

@implementation ProfileAfterAdharVC

@synthesize tblProfileEdit,vwProfileEdit;

@synthesize arr_table_pass;
@synthesize header_title_pass;
@synthesize TAG_pass;

@synthesize citiesList,districtList;
@synthesize state_id,city_id,district_id;
@synthesize dic_info;
@synthesize tagFrom;
@synthesize base64Image;
@synthesize arr_importtype;


-(void)loadDataGender
{
    TAG_pass=TAG_GENDER;
    header_title_pass= NSLocalizedString(@"profile_gender", nil);
    
    arr_table_pass=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"gender_male", nil),NSLocalizedString(@"gender_female", nil),NSLocalizedString(@"gender_transgender", nil), nil];
    
    [self callCustomPicker];
}


- (IBAction)bgTouch:(id)sender
{
    
    
}

-(void)openDOBpicker
{
    
    
    for(UIView *subview in [self.view subviews]) {
        
        /*  if ([subview isKindOfClass:[UIButton class]]) {
         [subview removeFromSuperview];
         }*/
        if ([subview isKindOfClass:[UIDatePicker class]]) {
            [subview removeFromSuperview];
        }
        if ([subview isKindOfClass:[UIView class]]) {
            [vw_dateToolbar removeFromSuperview];
        }
        
    }
    [self.view endEditing:YES];
    
    vw_dateToolbar =[[UIView alloc]initWithFrame:CGRectMake(0, fDeviceHeight-270.0, fDeviceWidth, 100)];
    
    vw_dateToolbar.backgroundColor=[UIColor blueColor];
    [self.view addSubview:vw_dateToolbar];
    
    btncancel = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btncancel setTitle:NSLocalizedString(@"cancel", nil) forState:UIControlStateNormal];
    btncancel.titleLabel.textColor=[UIColor whiteColor];
    
    btncancel.frame = CGRectMake(10.0,5.0,100.0, 40.0);
    [btncancel addTarget:self
                  action:@selector(cancelPicker:)
        forControlEvents:UIControlEventTouchUpInside];
    [vw_dateToolbar addSubview:btncancel];
    
    
    
    
    
    btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    btnDone.titleLabel.textColor=[UIColor blueColor];
    
    [btnDone setTitle:NSLocalizedString(@"done", nil) forState:UIControlStateNormal];
    btnDone.frame = CGRectMake(fDeviceWidth-110,5,100.0, 40.0);
    [btnDone addTarget:self
                action:@selector(HidePicker:)
      forControlEvents:UIControlEventTouchUpInside];
    [vw_dateToolbar addSubview:btnDone];
    
    
    datepicker= [[UIDatePicker alloc] initWithFrame:CGRectMake(0, fDeviceHeight-220, fDeviceWidth,220 )];
    datepicker.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    datepicker.datePickerMode = UIDatePickerModeDate;
    datepicker.backgroundColor = [UIColor whiteColor];
    
    NSDateComponents *components = [datepicker.calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[NSDate date]];
    
    NSInteger year = components.year;
    NSInteger month = components.month;
    NSInteger day = components.day;
    NSInteger minimumYear = year - 1900;//Given some year here for example
    NSInteger minimumMonth = month - 1;
    NSInteger minimumDay = day - 1;
    [components setYear:-minimumYear];
    [components setMonth:-minimumMonth];
    [components setDay:-minimumDay];
    NSDate *minDate = [datepicker.calendar dateByAddingComponents:components toDate:[NSDate date] options:0];
    
    [datepicker setMinimumDate:minDate];
    [datepicker setMaximumDate:[NSDate date]];
    
    [self.view addSubview:datepicker];
}

-(IBAction)cancelPicker:(id)sender
{
    [self resignFirstResponder];
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         datepicker.frame = CGRectMake(0, -250, fDeviceWidth, 50);
                     } completion:^(BOOL finished) {
                         [datepicker removeFromSuperview];
                         [btnDone removeFromSuperview];
                         [btncancel removeFromSuperview];
                         [vw_dateToolbar removeFromSuperview];
                         
                         
                     }];
    
    [datepicker removeFromSuperview];
}



-(IBAction)HidePicker:(id)sender{
    [self resignFirstResponder];
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         datepicker.frame = CGRectMake(0, -250, fDeviceWidth, 50);
                     } completion:^(BOOL finished) {
                         [datepicker removeFromSuperview];
                         [btnDone removeFromSuperview];
                         [btncancel removeFromSuperview];
                         [vw_dateToolbar removeFromSuperview];
                         
                         
                     }];
    
    [datepicker removeFromSuperview];
    
    
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd-MM-yyyy"];
    NSString *formattedDate = [df stringFromDate:[datepicker date]];
    pickdate=[datepicker date];
    //NSLog(@"formattedDate String : %@",formattedDate);
    
    
    
    NSTimeInterval secondsBetween = [ [NSDate date] timeIntervalSinceDate:pickdate];
    
    int numberOfDays = secondsBetween / 86400;
    
    //NSLog(@"There are %d days in between the two dates.", numberOfDays);
    
    
    if (numberOfDays <365)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                        message:NSLocalizedString(@"age_check_text", nil)                                                           delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil];
        [alert show];
        
    }
    else
    {
        singleton.profileDOBSelected= formattedDate;
        
        user_dob=formattedDate;
        
        [tblProfileEdit reloadData];
    }
    
    // 30-11-2001
}




- (IBAction)btnGenderAction:(id)sender {
    header_title_pass= NSLocalizedString(@"profile_gender", nil);
    
    [self loadDataGender];
}
- (IBAction)btnDobAction:(id)sender {
    [self openDOBpicker];
    
}
- (IBAction)btnQualifyAction:(id)sender
{
    NSArray *qualification_arr=  [obj getQualiList];
    TAG_pass=TAG_QUALIFI_PROFILE;
    header_title_pass=NSLocalizedString(@"qualication", nil);
    arr_table_pass=[[NSMutableArray alloc]init];
    for (int i=0; i<[qualification_arr count]; i++) {
        [arr_table_pass addObject:[qualification_arr objectAtIndex:i]];
    }
    [self callCustomPicker];
    
}
- (IBAction)btnOccupAction:(id)sender
{
    
    NSArray*occup_arr=[obj getOccupList];
    occup_arr = [occup_arr sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    
    TAG_pass=TAG_OCCUPATION_PROFILE;
    header_title_pass=NSLocalizedString(@"occupation", nil);
    arr_table_pass=[[NSMutableArray alloc]init];
    for (int i=0; i<[occup_arr count]; i++) {
        [arr_table_pass addObject:[occup_arr objectAtIndex:i]];
    }
    [self callCustomPicker];
    
}
- (IBAction)btnStateAction:(id)sender
{
    [self loadDataState];
    flag_apiHitStatus=FALSE;
}
- (IBAction)btnDistrictAction:(id)sender {
    if ([state_id length]!=0) {
        header_title_pass=NSLocalizedString(@"district", nil);
        [self loadDataDistrict];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                        message:NSLocalizedString(@"please_select_state", nil)                                                           delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil];
        [alert show];
        
    }
}


-(void)loadDataDistrict
{
    TAG_pass=TAG_DISTRICT;
    
    header_title_pass=NSLocalizedString(@"district", nil);
    
    
    arr_table_pass=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[districtList count]; i++) {
        [arr_table_pass addObject:[[districtList objectAtIndex:i] valueForKey:@"dnam"]];
    }
    [self callCustomPicker];
}


-(void)callCustomPicker
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    customVC = [storyboard instantiateViewControllerWithIdentifier:@"EnterRegFieldVC"];
    
    customVC.get_title_pass=header_title_pass;
    customVC.get_arr_element=arr_table_pass;
    customVC.get_TAG=TAG_pass;
    customVC.hidesBottomBarWhenPushed = YES;
    
    if ([tagFrom isEqualToString:@"ISFROMPROFILEUPDATE"]) {
        
        customVC.TAG_FROM=@"ISFROMPROFILEUPDATE";
        [self.navigationController pushViewController:customVC animated:YES];
    }
    if ([tagFrom isEqualToString:@"ISFROMREGISTRATION"]) {
        
        customVC.TAG_FROM=@"ISFROMREGISTRATION";
        
        [self presentViewController:customVC animated:NO completion:nil];
    }
}







//----- hitAPI for IVR OTP call Type registration ------
-(void)hitCityAPI
{
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"lang"];//lang is Status of the account
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [dictBody setObject:state_id forKey:@"stid"];  //get from mobile default email //not supported iphone
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_FETCH_CITY withBody:dictBody andTag:TAG_REQUEST_FETCH_CITY completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        if (error == nil) {
            //NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            //            NSString *rc=[response valueForKey:@"rc"];
            //            NSString *rs=[response valueForKey:@"rs"];
            
            //NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            //   NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                flag_apiHitStatus=TRUE;
                //singleton.user_tkn=tkn;
                citiesList=[[NSMutableArray alloc]init];
                citiesList=[[response valueForKey:@"pd"]valueForKey:@"cities"];
                
                districtList=[[NSMutableArray alloc]init];
                districtList=[[response valueForKey:@"pd"]valueForKey:@"district"];
                
                
                
                
                /*    cid = 9878;
                 cnam = Zira;
                 did = 295;
                 dnam = Amritsar;
                 
                 */
                [self refreshdata];
                
            }
            
        }
        else{
            flag_apiHitStatus=FALSE;
            //NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}


-(void)refreshdata
{
    
    
    
    if ([citiesList count]!=0) {
        NSPredicate *cityfilter = [NSPredicate predicateWithFormat:@"cnam =%@",singleton.notiTypeCitySelected];
        NSArray *cityIdFilter = [citiesList filteredArrayUsingPredicate:cityfilter];
        
        NSPredicate *distfilter = [NSPredicate predicateWithFormat:@"dnam =%@",singleton.notiTypDistricteSelected];
        
        if ([singleton.notiTypDistricteSelected length]!=0) {
            user_dist=[NSString stringWithFormat:@"%@",singleton.notiTypDistricteSelected];
            
        }
        NSArray *distIdFilter = [districtList filteredArrayUsingPredicate:distfilter];
        
        district_id=@"";
        
        if ([cityIdFilter count]!=0) {
            city_id=[[cityIdFilter objectAtIndex:0]valueForKey:@"cid"];
            
        }
        if ([distIdFilter count]!=0)
        {
            
            district_id=[[distIdFilter objectAtIndex:0]valueForKey:@"did"];
        }
        
        //NSLog(@"cityIdFilter=%@",cityIdFilter);
        //NSLog(@"distIdFilter=%@",distIdFilter);
    }
    
    [tblProfileEdit reloadData];
    
    
}




-(void)loadDataState
{
    TAG_pass=TAG_STATE_PROFILE;
    header_title_pass=NSLocalizedString(@"states", nil);
    
    NSArray*arry=[obj getStateList];
    arry = [arry sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    header_title_pass=NSLocalizedString(@"states", nil);
    
    arr_table_pass=[[NSMutableArray alloc]init];
    
    arr_table_pass=[arry mutableCopy];
    
    singleton.notiTypeCitySelected=@"";
    singleton.notiTypDistricteSelected=@"";
    
    [self callCustomPicker];
    
}


/*- (IBAction)btnAddEmailAction:(id)sender
 {
 
 AddEmailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddEmailVC"];
 [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
 vc.tagtopass=tagFrom;
 
 if ([user_email length]!=0) {
 vc.titletopass=NSLocalizedString(@"update_email_address", nil);
 }
 else
 {
 vc.titletopass=NSLocalizedString(@"add_email_address", nil);
 
 }
 
 if ([tagFrom isEqualToString:@"ISFROMPROFILEUPDATE"]) {
 [self.navigationController pushViewController:vc animated:YES];
 
 }
 if ([tagFrom isEqualToString:@"ISFROMREGISTRATION"]) {
 [self presentViewController:vc animated:NO completion:nil];
 }
 }
 
 
 - (IBAction)btn_AlterMbAction:(id)sender
 {
 
 
 AlternateMobileVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AlternateMobileVC"];
 [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
 vc.tagtopass=tagFrom;
 if ([user_altermob length]!=0) {
 vc.titletopass=NSLocalizedString(@"update_alt_mob_num", nil);
 }
 else
 {
 vc.titletopass=NSLocalizedString(@"add_alt_mob_num", nil);
 
 }
 
 if ([tagFrom isEqualToString:@"ISFROMPROFILEUPDATE"]) {
 [self.navigationController pushViewController:vc animated:YES];
 
 }
 if ([tagFrom isEqualToString:@"ISFROMREGISTRATION"]) {
 [self presentViewController:vc animated:NO completion:nil];
 }
 
 
 }
 */


- (IBAction)btnAddEmailAction:(id)sender
{
    
    if (user_email.length>0)
    {
        
        
        UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", nil)  destructiveButtonTitle:nil otherButtonTitles:                                NSLocalizedString(@"update", nil),
                                NSLocalizedString(@"remove", nil),
                                nil];
        popup.tag = 1010;
        [popup showInView:self.view];
        
        
    }
    else
    {
        [sender openAddEmailView];
    }
    
}


-(void)openAddEmailView
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    AddEmailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"AddEmailVC"];
    //[vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    vc.tagtopass=tagFrom;
    
    if ([user_email length]!=0) {
        vc.titletopass=NSLocalizedString(@"update_email_address", nil);
    }
    else
    {
        vc.titletopass=NSLocalizedString(@"add_email_address", nil);
        
    }
    
    if ([tagFrom isEqualToString:@"ISFROMPROFILEUPDATE"]) {
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    if ([tagFrom isEqualToString:@"ISFROMREGISTRATION"])
    {
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self presentViewController:vc animated:NO completion:nil];
        });
        
    }
}

- (IBAction)btn_AlterMbAction:(id)sender
{
    
    if (user_altermob.length>0)
    {
        
        
        UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", nil)  destructiveButtonTitle:nil otherButtonTitles:
                                NSLocalizedString(@"update", nil),
                                NSLocalizedString(@"remove", nil),
                                nil];
        popup.tag = 2020;
        [popup showInView:self.view];
        
        
    }
    else
    {
        [sender openAlternateMobileView];
    }
    
    
    
}

-(void)removeEmail
{
    // flagEditCheck=TRUE;
    NSLog(@"remove email and reload");
    user_email=@"";
    [tblProfileEdit reloadData];
}

-(void)removeAlternateMobile
{
    // flagEditCheck=TRUE;
    NSLog(@"remove alter mobile and reload");
    user_altermob=@"";
    [tblProfileEdit reloadData];
}


- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (popup.tag==1010)
    {
        
        switch (buttonIndex)
        {
            case 0:
                [self openAddEmailView];
                break;
            case 1:
                [self removeEmail];
                break;
            default:
                break;
        }
        
    }
    else  if (popup.tag==2020)
    {
        
        switch (buttonIndex)
        {
            case 0:
                [self openAlternateMobileView];
                break;
            case 1:
                [self removeAlternateMobile];
                break;
            default:
                break;
        }
        
    }
}

-(void)openAlternateMobileView
{
    AlternateMobileVC   *vc;
    // new condition add for ipad
    if ([[UIScreen mainScreen]bounds].size.height == 1024)
    {
        vc = [[AlternateMobileVC alloc] initWithNibName:@"AlternateMobile_iPad" bundle:nil];
        
        
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        vc = [storyboard instantiateViewControllerWithIdentifier:@"AlternateMobileVC"];
        
        
    }
    
    vc.tagtopass=tagFrom;
    
    if ([user_email length]!=0) {
        vc.titletopass=NSLocalizedString(@"update_alt_mob_num", nil);
    }
    else
    {
        vc.titletopass=NSLocalizedString(@"add_alt_mob_num", nil);
        
    }
    
    if ([tagFrom isEqualToString:@"ISFROMPROFILEUPDATE"]) {
        
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    if ([tagFrom isEqualToString:@"ISFROMREGISTRATION"])
    {
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self presentViewController:vc animated:NO completion:nil];
        });
    }
    
    
    
    
    
    
    /*
     AlternateMobileVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AlternateMobileVC"];
     [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
     vc.tagtopass=tagFrom;
     if ([user_altermob length]!=0) {
     vc.titletopass=NSLocalizedString(@"update_alt_mob_num", nil);
     }
     else
     {
     vc.titletopass=NSLocalizedString(@"add_alt_mob_num", nil);
     
     }
     
     if ([tagFrom isEqualToString:@"ISFROMPROFILEUPDATE"]) {
     [self.navigationController pushViewController:vc animated:YES];
     
     }
     if ([tagFrom isEqualToString:@"ISFROMREGISTRATION"]) {
     [self presentViewController:vc animated:NO completion:nil];
     }
     */
    
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}


-(void)hideKeyboard
{
    [self.view endEditing:YES];
    
    // [self.txt_mobileNo resignFirstResponder];
}

- (IBAction)btnNextAction:(id)sender
{
    
    [self hideKeyboard];
    
    
    UITextField *txtFldAddress = (UITextField*)[self.view viewWithTag:201];
    singleton.profileUserAddress = txtFldAddress.text;
    user_address = txtFldAddress.text;
    
    
    
    // Take from  Take photo /choose from gallery / or If link Import from facebook /Import from google
    
    /*  if ([self validateNameWithString:user_name]!=TRUE  && [user_name length]!=0)
     
     {
     
     //NSLog(@"Wrong Name");
     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil)message:@"Invalid Name" delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
     [alert show];
     }
     
     
     else */
    if ([self validateEmailWithString:user_email]!=TRUE && [user_email length]!=0){
        
        //NSLog(@"Wrong email");
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:@"Invalid email-id" delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
        [alert show];
    }
    
    
    else  if ([self validatePhone:user_altermob]!=TRUE && [user_altermob length]!=0)
    {
        
        //NSLog(@"Wrong Mobile");
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:@"Invalid Mobile Number" delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
        [alert show];
    }
    
    else
    {
        
        [self hitAPI];
        
        
    }
    
}


//----- hitAPI for IVR OTP call Type registration ------
-(void)hitAPI
{
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //(dd-mm-yyyy)
    //  NSString *encrytSHA256=[NSString stringWithFormat:@"|%@|%@|",mpinStr,SaltRequestControl];
    //NSString *mpinStrEncrypted=[singleton.user_mpin sha256HashFor:singleton.user_mpin];
    NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",singleton.user_mpin,SaltMPIN];
    NSString *mpinStrEncrypted=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
    //NOTE mpinStrEncrypted value will be pass everywher
    // [dictBody setObject:mpinStrEncrypted forKey:@"mpin"];
    
    NSString *userGender=@"";
    
    if([user_gender isEqualToString:@"Male"]||[user_gender isEqualToString:@"male"]||[user_gender isEqualToString:@"M"]||[user_gender isEqualToString:@"m"]||[user_gender isEqualToString:@"MALE"]|| [user_gender isEqualToString : NSLocalizedString(@"gender_male", nil)])
    { userGender=@"m";
    }
    else if([user_gender isEqualToString:@"Female"]||[user_gender isEqualToString:@"female"]||[user_gender isEqualToString:@"F"]||[user_gender isEqualToString:@"f"]||[user_gender isEqualToString:@"FEMALE"]|| [user_gender isEqualToString : NSLocalizedString(@"gender_female", nil)])
    {
        userGender=@"f";
        
    }
    else if([user_gender isEqualToString:@"Other"]||[user_gender isEqualToString:@"other"]||[user_gender isEqualToString:@"T"]||[user_gender isEqualToString:@"t"] ||[user_gender isEqualToString : NSLocalizedString(@"gender_transgender", nil)])
    {
        userGender=@"t";
        
    }
    else
    {
        userGender=@"";
        
    }
    state_id=[obj getStateCode:user_state];
    if ([state_id isEqualToString:@"9999"])
    {
        state_id=@"";
    }
    //NSLog(@"state_id=%@",state_id);
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    
    
    NSString *name=user_name;
    NSString *dob=user_dob;
    NSString *amno=user_altermob;
    NSString *email=user_email;
    NSString *addr = user_address;
    
    NSLog(@"addr = %@",addr);
    NSLog(@"user_address = %@",user_address);
    
    if ([name length]==0) {
        name=@"";
    }
    if ([dob length]==0) {
        dob=@"";
    }
    if ([amno length]==0) {
        amno=@"";
    }
    if ([email length]==0) {
        email=@"";
    }
    /*  if ([qual length]==0) {
     qual=@"";
     }
     if ([occup length]==0) {
     occup=@"";
     }*/
    if ([addr length]==0) {
        addr=@"";
    }
    
    if ([state_id length]==0) {
        state_id=@"";
    }
    
    if ([district_id length]==0) {
        district_id=@"";
    }
    
    
    
    
    
    
    
    if ([base64Image length]==0) {
        base64Image=@"";
    }
    
    user_token=singleton.user_tkn;//[[NSUserDefaults standardUserDefaults] stringForKey:@"TOKEN_KEY"];
    
    
    //[[NSUserDefaults standardUserDefaults] setValue:singleton.user_tkn forKey:@"TOKEN_KEY"];
    //[[NSUserDefaults standardUserDefaults]synchronize];
    
    //------------------------- Encrypt Value------------------------
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    // Encrypt
    [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_tkn withKey:@"TOKEN_KEY"];
    
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    //------------------------- Encrypt Value------------------------
    
    if ([singleton.profileUserAddress length]==0) {
        singleton.profileUserAddress=@"";
    }
    
    
    NSLog(@"user_token=%@",user_token);
    
    [dictBody setObject:name forKey:@"nam"]; //Enter user name
    [dictBody setObject:userGender forKey:@"gndr"];//Enter gender of user(m/f/t)
    [dictBody setObject:dob forKey:@"dob"]; //Enter DOB of User
    [dictBody setObject:@"" forKey:@"cty"]; //Enter District of User
    [dictBody setObject:state_id forKey:@"st"]; //Enter State Of user
    [dictBody setObject:district_id forKey:@"dist"]; //Enter District of User
    [dictBody setObject: singleton.profileUserAddress forKey:@"addr"];
    
    [dictBody setObject:amno forKey:@"amno"];//Enter alternate mobile number of user
    [dictBody setObject:email forKey:@"email"];//Enter email address by user
    if (singleton.mobileNumber) {
        [dictBody setObject:singleton.mobileNumber forKey:@"mno"];
    }
    
    NSString *qual_id=[obj getQualiListCode:user_quali];
    NSString *occup_id=[obj getOccuptCode:user_Occup];
    
    
    
    // //Enter mobile number of user
    
    //tkn number
    [dictBody setObject:mpinStrEncrypted forKey:@"mpin"];  //MPIN
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [dictBody setObject:occup_id forKey:@"occup"];
    [dictBody setObject:addr forKey:@"addr"];
    [dictBody setObject:qual_id forKey:@"qual"];
    [dictBody setObject:@"FIRST" forKey:@"flag"];  //get from mobile default email //not supported iphone
    [dictBody setObject:user_token forKey:@"tkn"];//Enter mobile number of user
    
    [dictBody setObject:@"" forKey:@"pic"];  //Check for pic upload
    
    //flag = URL
    //At registration flag=FIRST
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:YES webServiceURL:UM_API_UPDATE_PROFILE withBody:dictBody andTag:TAG_REQUEST_UPDATE_PROFILE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        
        if (error == nil)
        {
            
            
            //NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            //            NSString *rc=[response valueForKey:@"rc"];
            //            NSString *rs=[response valueForKey:@"rs"];
            // NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
            //NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
            //singleton.user_tkn = tkn;
            // [[NSUserDefaults standardUserDefaults] setObject:tkn forKey:@"TOKEN_KEY"];
            // [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
            //  }
            
            //NSLog(@"value of rc =%@ \n value of rs=%@ \n value of tkn=%@ ",rc,rs,tkn);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                
                NSString*  abbr=[[response valueForKey:@"pd"] valueForKey:@"abbr"];
                NSLog(@"value of abbr=%@",abbr);
                
                if ([abbr length]==0) {
                    
                    abbr=@"";
                    
                }
                singleton.user_StateId = [[response valueForKey:@"pd"] valueForKey:@"ostate"];
                [singleton setStateId:singleton.user_StateId];
                
                NSString *emblemString = [[response valueForKey:@"pd"] valueForKey:@"stemblem"];
                emblemString = emblemString.length == 0 ? @"":emblemString;
                [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
                
                
                [[NSUserDefaults standardUserDefaults] setObject:abbr forKey:@"ABBR_KEY"];
                
                
                [self alertwithMsg:rd];
            }
            
        }
        else{
            //NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}


-(void)alertwithMsg:(NSString*)msg
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"profile_label", nil) message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                   {
                                       
                                       [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                                       [[NSUserDefaults standardUserDefaults] encryptValue:@"YES" withKey:@"SHOW_PROFILEBAR"];
                                       [[NSUserDefaults standardUserDefaults] synchronize];
                                       
                                       
                                       // [self dismissViewControllerAnimated:NO completion:nil];
                                       
                                       // singleton.user_mpin=@"";
                                       UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                       
                                       UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
                                       // tbc.selectedIndex=[SharedManager getSelectedTabIndex];
                                       tbc.selectedIndex=0;
                                       
                                       [self presentViewController:tbc animated:NO completion:nil];
                                       
                                       
                                       
                                   }];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}






- (IBAction)btnSkipAction:(id)sender
{
    
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"YES" withKey:@"SHOW_PROFILEBAR"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    
    singleton.objUserProfile = nil;
    singleton.notiTypeGenderSelected=@"";
    singleton.profileDOBSelected = nil;
    singleton.profileUserAddress = nil;
    
    
    user_dob=@"";
    user_address = @"";
    
    if (self.old_state.length == 0)
    {
        singleton.profilestateSelected = @"";
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
    //tbc.selectedIndex=[SharedManager getSelectedTabIndex];
    tbc.selectedIndex=0;
    [self presentViewController:tbc animated:NO completion:nil];
}


- (void)reloadTable:(NSNotification *)notif {
    
    obj=[[StateList alloc]init];
    [obj hitStateQualifiAPI];
    [self performSelector:@selector(loadwithdelay) withObject:nil afterDelay:0.1];
    
}


- (void)viewDidLoad {
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadTable:)
                                                 name:@"STARTNOTIFIERREGIS"
                                               object:nil];
    
    singleton=[SharedManager sharedSingleton];
    
    
    obj=[[StateList alloc]init];
    [obj hitStateQualifiAPI];
    [self performSelector:@selector(loadwithdelay) withObject:nil afterDelay:0.5];
    
    [btnskip setTitle:NSLocalizedString(@"skip", nil) forState:UIControlStateNormal];
    
    btnskip.titleLabel.font = [UIFont systemFontOfSize:btnskip.titleLabel.font.pointSize weight:UIFontWeightMedium];
    
    self.old_state=[NSString stringWithFormat:@"%@",singleton.profilestateSelected];
    NSLog(@"self.old_state=%@",self.old_state);
    
    flagURL=FALSE;
    //NSLog(@"dic_info=%@",dic_info);
    flag_apiHitStatus=FALSE;
    
    obj=[[StateList alloc]init];
    state_id=@"";
    
    NSLog(@"%@",singleton.user_mpin);
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
    lblTitleProfileInformatn.text = NSLocalizedString(@"profile_label", nil);
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    tblProfileEdit.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    tblProfileEdit.tableFooterView =  [self designFooterView];
    
    
    arrGeneralInformation = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"name_caps", nil),NSLocalizedString(@"gender_caps", nil),NSLocalizedString(@"date_of_birth_caps", nil),NSLocalizedString(@"qualication_caps", nil),NSLocalizedString(@"occupation_caps", nil),NSLocalizedString(@"state_txt_caps", nil),NSLocalizedString(@"district_caps", nil),NSLocalizedString(@"address_caps", nil), nil];
    
    arrAccountInformation = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"email_address", nil),NSLocalizedString(@"alt_mob_num", nil), nil];
    
    
    [self fillDataInString];
    [tblProfileEdit reloadData];
    
    
}



- (BOOL)validatePhone:(NSString *)phoneNumber
{
    //NSString *phoneRegex = @"[789][0-9]{3}([0-9]{6})?";
    //NSString *phoneRegex =@"[6789][0-9]{9}";
   /* NSString *phoneRegex =@"^[0-9]+$";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [test evaluateWithObject:phoneNumber];
    */
    if (phoneNumber.length == 0 || phoneNumber.length > 19 )
    {
        return false;
    }
    return true;
}


- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}



-(BOOL)validateNameWithString:(NSString*)nametopass
{
    NSString *nameRegex =@"^[a-zA-Z\\s]*$";
    
    NSPredicate *testRegex = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nameRegex];
    if(![testRegex evaluateWithObject:nametopass])
        return NO;
    else
        return YES;
    
}
//------- End of validation-------



-(void)setFontforView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       if ([view isKindOfClass:[UITextField class]])
                       {
                           
                           UITextField *txtfield = (UITextField *)view;
                           NSString *fonttxtFieldName = txtfield.font.fontName;
                           CGFloat fonttxtsize =txtfield.font.pointSize;
                           txtfield.font = nil;
                           
                           txtfield.font = [UIFont fontWithName:fonttxtFieldName size:fonttxtsize];
                           
                           [txtfield layoutIfNeeded]; //Fixes iOS 9 text bounce glitch
                       }
                       
                       
                   });
    
    if ([view isKindOfClass:[UITextView class]])
    {
        
        UITextView *txtview = (UITextView *)view;
        NSString *fonttxtviewName = txtview.font.fontName;
        CGFloat fontbtnsize =txtview.font.pointSize;
        
        txtview.font = [UIFont fontWithName:fonttxtviewName size:fontbtnsize];
        
    }
    
    
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        NSString *fontName = lbl.font.fontName;
        CGFloat fontSize = lbl.font.pointSize;
        
        lbl.font = [UIFont fontWithName:fontName size:fontSize];
    }
    
    
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *button = (UIButton *)view;
        NSString *fontbtnName = button.titleLabel.font.fontName;
        CGFloat fontbtnsize = button.titleLabel.font.pointSize;
        
        [button.titleLabel setFont: [UIFont fontWithName:fontbtnName size:fontbtnsize]];
    }
    
    if (isSubViews)
    {
        
        for (UIView *sview in view.subviews)
        {
            [self setFontforView:sview andSubViews:YES];
        }
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    
    
    //   [self performSelector:@selector(loadwithdelay) withObject:nil afterDelay:0.1];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [super viewWillAppear:YES];
    
    [self setFontforView:self.view andSubViews:YES];
}



-(void)fillDataInString
{
    //SharedManager *sharedObject = [SharedManager sharedSingleton];
    
    user_email = @"";// singleton.objUserProfile.objAadhar.email;
    NSLog(@"aadhar email=%@ and email=%@",user_email,singleton.objUserProfile.objAadhar.email);
    user_name = singleton.objUserProfile.objAadhar.name;
    user_gender = singleton.objUserProfile.objAadhar.gender;
    user_gender = [user_gender stringByTrimmingCharactersInSet:
                   [NSCharacterSet whitespaceCharacterSet]];
    user_dob = singleton.objUserProfile.objAadhar.dob;
    user_address = singleton.objUserProfile.objAadhar.address;
    // user_state= singleton.objUserProfile.objAadhar.state;
    // user_dist = singleton.objUserProfile.objAadhar.district;
    //-------- new line-----------
    /*user_email =
     user_altermob =
     imageUserProfile =*/
    
    
}



-(void)loadwithdelay
{
    
    [tblProfileEdit reloadData];
    
    //NSLog(@"value of state=%@",singleton.profilestateSelected);
    
    
    
    
    if ([singleton.notiTypeGenderSelected length]!=0) {
        user_gender=[NSString stringWithFormat:@"%@",singleton.notiTypeGenderSelected];
        
    }
    
    if ([singleton.profilestateSelected length]!=0) {
        user_state=[NSString stringWithFormat:@"%@",singleton.profilestateSelected];
        
    }
    if ([singleton.notiTypDistricteSelected length]!=0) {
        user_dist=[NSString stringWithFormat:@"%@",singleton.notiTypDistricteSelected];
        
    }
    if ([singleton.profileDOBSelected length]!=0) {
        user_dob=[NSString stringWithFormat:@"%@",singleton.profileDOBSelected];
        
    }
    
    
    // user_name=[dic_info valueForKey:@"str_name"];
    /* if ([singleton.profileNameSelected length]!=0)
     {
     user_name=[NSString stringWithFormat:@"%@",singleton.profileNameSelected];
     }
     */
    
    
    NSLog(@"dic_info=%@",dic_info);
    
    
    // singleton.profileEmailSelected=[dic_info valueForKey:@"str_emailAddress"];
    // user_email=[dic_info valueForKey:@"str_emailAddress"];
    /* if ([user_email length]!=0) {
     singleton.profileEmailSelected=[NSString stringWithFormat:@"%@",user_email];
     }
     
     */
    if ([singleton.profileEmailSelected length]!=0) {
        user_email=[NSString stringWithFormat:@"%@",singleton.profileEmailSelected];
        
    }
    
    
    user_altermob=[dic_info valueForKey:@"str_alternateMb"];
    
    /* if ([user_altermob length]!=0) {
     singleton.altermobileNumber=[NSString stringWithFormat:@"%@",user_altermob];
     }
     */
    
    if ( [singleton.altermobileNumber length]!=0)
    {
        user_altermob=[NSString stringWithFormat:@"%@", singleton.altermobileNumber];
    }
    
    
    
    
    if ([dic_info valueForKey:@"str_occupation"]) {
        
        user_Occup=[dic_info valueForKey:@"str_occupation"];
    }
    
    if ([dic_info valueForKey:@"str_qualification"]) {
        
        user_quali=[dic_info valueForKey:@"str_qualification"];
    }
    if ([singleton.user_Occupation length]!=0) {
        user_Occup=[NSString stringWithFormat:@"%@",singleton.user_Occupation];
        
    }
    
    if ([singleton.user_Qualification length]!=0) {
        user_quali=[NSString stringWithFormat:@"%@",singleton.user_Qualification];
        
    }
    
    if ([dic_info valueForKey:@"str_address"]) {
        user_address=[dic_info valueForKey:@"str_address"];
        
    }
    if ([singleton.profileUserAddress length]!=0) {
        user_address=[NSString stringWithFormat:@"%@",singleton.profileUserAddress];
        
    }
    
    if (![self.old_state isEqualToString:singleton.profilestateSelected]) {
        user_dist=@"";
        // self.old_district=singleton.notiTypDistricteSelected;
        //singleton.notiTypDistricteSelected=@"";
        NSLog(@"user state are not same old state=%@ and new state=%@",self.old_state , singleton.profilestateSelected);
    }
    
    if (![singleton.profilestateSelected isEqualToString:@""])
    {
        state_id= [obj getStateCode:user_state];
        
        // flag_apiHitStatus=TRUE;
        if (flag_apiHitStatus ==FALSE)
        {
            [self hitCityAPI];
            
        }
    }
    
    
    
    
    [self refreshdata];
    
}
- (IBAction)btnBackAction:(id)sender {
    
    
    if ([tagFrom isEqualToString:@"ISFROMPROFILEUPDATE"]) {
        
        customVC.TAG_FROM=@"ISFROMPROFILEUPDATE";
        [self.navigationController popViewControllerAnimated:YES];
    }
    if ([tagFrom isEqualToString:@"ISFROMREGISTRATION"]) {
        
        customVC.TAG_FROM=@"ISFROMREGISTRATION";
        
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    
    
}


-(UIImage *)makeRoundedImage:(UIImage *) image
                      radius: (float) radius;
{
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    imageLayer.contents = (id) image.CGImage;
    imageLayer.masksToBounds = YES;
    imageLayer.cornerRadius = radius;
    UIGraphicsBeginImageContext(image.size);
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return roundedImage;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        
        if (indexPath.row==0)
        {
            //NSLog(@"indexpath row Name");
            
            
        }
        if (indexPath.row==1)
        {
            [self btnGenderAction:self];
            
        }
        if (indexPath.row==2)
        {
            
            [self btnDobAction:self];
            
        }
        if (indexPath.row==3)
        {
            [self btnQualifyAction:self];
            
        }
        if (indexPath.row==4)
        {
            [self btnOccupAction:self];
            
            
        }
        if (indexPath.row==5)
        {
            
            [self btnStateAction:self];
            
            
        }
        if (indexPath.row==6)
        {
            [self btnDistrictAction:self];
            
        }
        if (indexPath.row==7)
        {
            
            //NSLog(@"indexpath row Address");
            
            
        }
        
        
        
    }
    else  if (indexPath.section == 1)
        
    {
        if (indexPath.row==0)
        {
            
            [self btnAddEmailAction:self];
            
        }
        if (indexPath.row==1)
        {
            [self btn_AlterMbAction:self];
            
            
        }
        
        
        
    }
    
    
}



-(UIView *)designHeaderView
{
    vwHeader = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tblProfileEdit.frame.size.width, 230.0)];
    [tblProfileEdit addSubview:vwHeader];
    vwHeader.backgroundColor = [UIColor whiteColor];
    
    
    UIView  *vwHeaderLine = [[UIView alloc]initWithFrame:CGRectMake(0,vwHeader.frame.size.height-0.5, vwHeader.frame.size.width,0.5 )];
    vwHeaderLine.backgroundColor = [UIColor lightGrayColor];
    [vwHeader addSubview:vwHeaderLine];
    
    
    return vwHeader;
}



-(UIView*)designFooterView
{
    vwFooter = [[UIView alloc]initWithFrame:CGRectMake(0,tblProfileEdit.frame.size.height-150, tblProfileEdit.frame.size.width, 150.0)];
    [tblProfileEdit addSubview:vwFooter];
    vwFooter.backgroundColor = [UIColor whiteColor];
    
    UIButton *btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    btnNext.frame=CGRectMake((tblProfileEdit.frame.size.width/2)-100,20, 200, 40);
    [btnNext setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnNext.backgroundColor = [UIColor colorWithRed:46.0/255.0 green:156.0/255.0 blue:78.0/255.0 alpha:1.0];
    [btnNext setTitle:NSLocalizedString(@"save_and_proceed", nil) forState:UIControlStateNormal];
    [vwFooter addSubview:btnNext];
    [btnNext addTarget:self action:@selector(btnNextAction:)forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    /*UIButton *btnskip = [[UIButton alloc]initWithFrame:CGRectMake((tblProfileEdit.frame.size.width/2)-100, vwFooter.frame.size.height -50, 200, 30)];
     
     [btnskip setTitle:NSLocalizedString(@"skip", nil) forState:UIControlStateNormal];
     [btnskip setTintColor:[UIColor blueColor]];
     [btnskip setTitleColor:[UIColor colorWithRed:10.0/255.0 green:90.0/255.0 blue:157.0/255.0 alpha:1.0] forState:UIControlStateNormal];
     btnskip.titleLabel.font = [UIFont boldSystemFontOfSize:14.0];
     [vwFooter addSubview:btnskip];
     [btnskip addTarget:self action:@selector(btnSkipAction:) forControlEvents:UIControlEventTouchUpInside];
     btnskip.hidden=FALSE;*/
    
    
    
    
    return vwFooter;
    
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *vwGrayFooter = [[UIView alloc]initWithFrame:CGRectMake(0, vwHeader.frame.size.height + 40, tblProfileEdit.frame.size.width, 30)];
    [tblProfileEdit addSubview:vwGrayFooter];
    vwGrayFooter.backgroundColor = [UIColor colorWithRed:227.0/255.0 green:230.0/255.0 blue:234.0/255.0 alpha:1.0];
    if (section == 0) {
        vwGrayFooter.hidden = NO;
    }
    else{
        vwGrayFooter.hidden = YES;
    }
    return vwGrayFooter;
}




-(void)addShadowToTheView:(UIView*)vwItem
{
    vwItem.layer.shadowOffset = CGSizeMake(-10.0, 1.0);
    vwItem.layer.shadowColor = [UIColor grayColor].CGColor;
    vwItem.layer.shadowRadius = 3;
    vwItem.layer.shadowOpacity = 0.5;
    vwItem.layer.cornerRadius = 3.0;
}




//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *vwHeaderTitle = [[UIView alloc]initWithFrame:CGRectMake(0, vwHeader.frame.size.height, 50, 50.0)];
//    [tblProfileEdit addSubview:vwHeaderTitle];
//    vwHeaderTitle.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:234.0/255.0 blue:241.0/255.0 alpha:1.0];
//
//
//
//
//
//    UILabel *lblSectionTitle = [[UILabel alloc]initWithFrame:CGRectMake(15, 20, 200, 30.0)];
//    [vwHeaderTitle addSubview:lblSectionTitle];
//    lblSectionTitle.textColor = [UIColor grayColor];
//    lblSectionTitle.font = [UIFont systemFontOfSize:14.0];
//    lblSectionTitle.adjustsFontSizeToFitWidth = YES;
//
//
//    //for grey header Design
//
//
//
//
//    if (section == 0)
//    {
//        // vwGray.hidden = YES;
//        lblSectionTitle.text =NSLocalizedString(@"general_information", nil);
//    }
//    else
//
//    {
//        // vwGray.hidden = NO;
//        lblSectionTitle.text = NSLocalizedString(@"account_information", nil);
//    }
//
//
//
//
//    return vwHeaderTitle;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75.0;
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 60.0;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    return 0.001;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    
    if (section == 0) {
        return  arrGeneralInformation.count;
    }
    else  if (section == 1)
    {
        return arrAccountInformation.count;
    }
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    
    static NSString *CellIdentifier = @"EditUserCell";
    
    EditUserCell *editCell = (EditUserCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    // Configure cell
    if (editCell== nil)
    {
        editCell = [[EditUserCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    /* arrGeneralInformation = [[NSMutableArray alloc]initWithObjects:@"NAME",@"GENDER",@"DATE OF BIRTH",@"QUALIFICATION",@"OCCUPATION",@"STATE/UI",@"DISTRICT",@"ADDRESS", nil];
     arrAccountInformation = [[NSMutableArray alloc]initWithObjects:@"EMAIL ADDRESS",@"ALTERNATE MOBILE NUMBER", nil];*/
    
    editCell.txtNameFields.delegate=self;
    
    if (indexPath.section == 0)
    {
        editCell.lblName.text = [arrGeneralInformation objectAtIndex:indexPath.row];
        
        //[editCell.btnDropDown setImage:[UIImage imageNamed:@"icon_arrow-1"] forState:UIControlStateNormal];
        // [editCell.btnDropDown setTitle:@"" forState:UIControlStateNormal];
        
        editCell.btnDropDown.hidden=TRUE;
        [editCell.btnDropDown setTitle:@"" forState:UIControlStateNormal];
        [editCell.btnDropDown setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        
        if (indexPath.row==0)
        {
            editCell.txtNameFields.enabled=TRUE;
            
            editCell.txtNameFields.text=user_name;
            //editCell.txtNameFields.text =   sharedObject.objUserProfile.objAadhar.name;
            editCell.txtNameFields.tag=101;
            editCell.btnDropDown.hidden=TRUE;
            
            editCell.accessoryType = UITableViewCellAccessoryNone;
            
            
        }
        if (indexPath.row==1)
        {
            editCell.txtNameFields.enabled=FALSE;
            
            
            if ([user_gender isEqualToString:@"M"]||[user_gender isEqualToString:@"m"]||[user_gender isEqualToString:NSLocalizedString(@"gender_male", nil)]||[user_gender isEqualToString:@"male"] ||[user_gender isEqualToString:@"MALE"]){
                editCell.txtNameFields.text=NSLocalizedString(@"gender_male", nil);
                singleton.notiTypeGenderSelected=NSLocalizedString(@"gender_male", nil);
                
            }
            else if ([user_gender isEqualToString:@"F"]||[user_gender isEqualToString:@"f"]||[user_gender isEqualToString:NSLocalizedString(@"gender_female", nil)]  || [user_gender isEqualToString:@"female"] || [user_gender isEqualToString:@"FEMALE"] ) {
                editCell.txtNameFields.text=NSLocalizedString(@"gender_female", nil);
                singleton.notiTypeGenderSelected=NSLocalizedString(@"gender_female", nil);
                
            }
            
            else if ([user_gender isEqualToString:@"T"]||[user_gender isEqualToString:@"t"]||[user_gender isEqualToString:NSLocalizedString(@"gender_transgender", nil)]  || [user_gender isEqualToString:NSLocalizedString(@"other", nil)]  )
            {
                
                editCell.txtNameFields.text=NSLocalizedString(@"gender_transgender", nil);
                singleton.notiTypeGenderSelected=NSLocalizedString(@"gender_transgender", nil);
                
            }
            else
            {
                editCell.txtNameFields.text=@"";
                singleton.notiTypeGenderSelected=@"";
            }
            
            editCell.btnDropDown.hidden=FALSE;
            editCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
        }
        if (indexPath.row==2)
        {
            editCell.txtNameFields.enabled=FALSE;
            editCell.txtNameFields.text=user_dob;
            
            editCell.btnDropDown.hidden=FALSE;
            editCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
        }
        if (indexPath.row==3)
        {
            editCell.txtNameFields.enabled=FALSE;
            editCell.btnDropDown.hidden=FALSE;
            editCell.txtNameFields.text=user_quali;
            editCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            
        }
        if (indexPath.row==4)
        {
            editCell.txtNameFields.enabled=FALSE;
            editCell.btnDropDown.hidden=FALSE;
            editCell.txtNameFields.text=user_Occup;
            editCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            
        }
        if (indexPath.row==5)
        {
            
            editCell.txtNameFields.enabled=FALSE;
            editCell.btnDropDown.hidden=FALSE;
            editCell.txtNameFields.text=user_state;
            editCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
        }
        if (indexPath.row==6)
        {
            editCell.txtNameFields.enabled=FALSE;
            editCell.btnDropDown.hidden=FALSE;
            editCell.txtNameFields.text=user_dist;
            editCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            
        }
        if (indexPath.row==7)
        {
            editCell.txtNameFields.enabled=TRUE;
            editCell.btnDropDown.hidden=TRUE;
            editCell.txtNameFields.text=user_address;
            editCell.txtNameFields.tag=201;
            
            editCell.accessoryType = UITableViewCellAccessoryNone;
            
        }
        
        
        
    }
    else  if (indexPath.section == 1)
        
    {
        editCell.lblName.text = [arrAccountInformation objectAtIndex:indexPath.row];
        
        [editCell.btnDropDown setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        editCell.accessoryType = UITableViewCellAccessoryNone;
        
        
        if ([[UIScreen mainScreen]bounds].size.height >= 1024)
        {
            editCell.btnDropDown.frame = CGRectMake(self.view.frame.size.width-100, 25, 80, 50);
            
        }
        else
        {
            editCell.btnDropDown.frame = CGRectMake(self.view.frame.size.width-100, 25, 80, 50);
        }
        
        
        if (indexPath.row==0)
        {
            editCell.txtNameFields.enabled=FALSE;
            editCell.btnDropDown.hidden=FALSE;
            //NSLog(@"indexpath row EMAIL=%d",indexPath.row);
            
            editCell.txtNameFields.text=user_email;
            
            
            editCell.txtNameFields.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
            
            CGRect butnFrame =  editCell.btnDropDown.frame;
            butnFrame.origin.x = singleton.isArabicSelected ? 10: CGRectGetWidth(tableView.frame) - 100;
            editCell.btnDropDown.frame = butnFrame;
            
            if (editCell.txtNameFields.text.length>0)
            {
                //[editCell.btnDropDown setTitle:NSLocalizedString(@"update", nil) forState:UIControlStateNormal];
                
                [editCell.btnDropDown setImage:[UIImage imageNamed:@"more_info_New"] forState:UIControlStateNormal];
                
                [editCell.btnDropDown setTitle:@"" forState:UIControlStateNormal];
                
                
            }
            else
            {
                //[editCell.btnDropDown setTitle:NSLocalizedString(@"add", nil) forState:UIControlStateNormal];
                [editCell.btnDropDown setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                
                [editCell.btnDropDown setTitle:NSLocalizedString(@"add", nil) forState:UIControlStateNormal];
            }
            
        }
        if (indexPath.row==1)
        {
            editCell.txtNameFields.enabled=FALSE;
            editCell.btnDropDown.hidden=FALSE;
            //NSLog(@"indexpath row ALTERNATE MOBILE=%d",indexPath.row);
            editCell.txtNameFields.text=user_altermob;
            
            editCell.txtNameFields.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
            
            CGRect butnFrame =  editCell.btnDropDown.frame;
            butnFrame.origin.x = singleton.isArabicSelected ? 10: CGRectGetWidth(tableView.frame) - 100;
            editCell.btnDropDown.frame = butnFrame;
            
            if (editCell.txtNameFields.text.length>0) {
                //  [editCell.btnDropDown setTitle:NSLocalizedString(@"update", nil) forState:UIControlStateNormal];
                [editCell.btnDropDown setImage:[UIImage imageNamed:@"more_info_New"] forState:UIControlStateNormal];
                
                [editCell.btnDropDown setTitle:@"" forState:UIControlStateNormal];
                
            }
            else
            {
                
                [editCell.btnDropDown setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                
                [editCell.btnDropDown setTitle:NSLocalizedString(@"add", nil) forState:UIControlStateNormal];
                // [editCell.btnDropDown setTitle:NSLocalizedString(@"add", nil) forState:UIControlStateNormal];
            }
            
        }
        
        
        
    }
    
    
    
    editCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return editCell;
}




/*
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
 {
 
 
 static NSString *CellIdentifier = @"EditUserCell";
 
 EditUserCell *editCell = (EditUserCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 // Configure cell
 if (editCell== nil)
 {
 editCell = [[EditUserCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
 }
 
 
 
 if (indexPath.section == 0)
 {
 editCell.lblName.text = [arrGeneralInformation objectAtIndex:indexPath.row];
 
 
 editCell.btnDropDown.tag=indexPath.row;
 if (editCell.btnDropDown.tag==0)
 {
 
 
 editCell.txtNameFields.enabled=TRUE;
 editCell.btnDropDown.hidden=TRUE;
 
 }
 if (editCell.btnDropDown.tag==7) {
 editCell.txtNameFields.enabled=TRUE;
 editCell.btnDropDown.hidden=TRUE;
 
 }
 else
 {
 editCell.txtNameFields.enabled=FALSE;
 editCell.btnDropDown.hidden=FALSE;
 
 }
 
 }
 else    if (indexPath.section == 1)
 
 {
 editCell.lblName.text = [arrAccountInformation objectAtIndex:indexPath.row];
 editCell.btnDropDown.tag=indexPath.row;
 
 if (editCell.btnDropDown.tag==0) {
 
 [editCell.btnDropDown setTitle:@"Add" forState:UIControlStateNormal];
 if ([editCell.txtNameFields.text length]!=0)
 {
 singleton.profileEmailSelected=[NSString stringWithFormat:@"%@",editCell.txtNameFields.text];
 [editCell.btnDropDown setTitle:@"Update" forState:UIControlStateNormal];
 
 
 }
 if ([singleton.profileEmailSelected length]!=0) {
 editCell.txtNameFields.text=[NSString stringWithFormat:@"%@",singleton.profileEmailSelected];
 [editCell.btnDropDown setTitle:@"Update" forState:UIControlStateNormal];
 
 
 }
 
 }
 if (editCell.btnDropDown.tag==1) {
 
 [editCell.btnDropDown setTitle:@"Add" forState:UIControlStateNormal];
 if ([editCell.txtNameFields.text length]!=0)
 {
 singleton.profileEmailSelected=[NSString stringWithFormat:@"%@",editCell.txtNameFields.text];
 [editCell.btnDropDown setTitle:@"Update" forState:UIControlStateNormal];
 
 }
 
 if ([singleton.altermobileNumber length]!=0)
 {
 editCell.txtNameFields.text=[NSString stringWithFormat:@"%@",singleton.altermobileNumber];
 [editCell.btnDropDown setTitle:@"Update" forState:UIControlStateNormal];
 }
 
 
 }
 [editCell.btnDropDown setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
 
 editCell.txtNameFields.enabled=FALSE;
 editCell.btnDropDown.hidden=FALSE;
 
 
 }
 
 editCell.selectionStyle = UITableViewCellSelectionStyleNone;
 
 return editCell;
 }
 
 */
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag==101)
    {
        singleton.profileNameSelected=textField.text;
        user_name=singleton.profileNameSelected;
    }
    else if (textField.tag==201)
    {
        singleton.profileUserAddress=textField.text;
        user_address = singleton.profileUserAddress;
        
    }
    
    [self setFontforView:self.view andSubViews:YES];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}


/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

