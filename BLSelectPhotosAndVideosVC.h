//
//  BLSelectPhotosAndVideosVC.h
//  BlendPhotoAndVideo
//
//  Created by Hoang Tran on 12/24/14.
//  Copyright (c) 2014 ILandApp. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "BLCameraVC.h"

//=============

@protocol SelectImageVideoBase64Delegate <NSObject>
- (void)selectimageVideoReturnToDept:(NSDictionary*)parameters;
- (void)selectimageVideoFailCaseToDept:(NSDictionary*)parameters;

@end
//=============


@interface BLSelectPhotosAndVideosVC : UIViewController <UITableViewDataSource, UITableViewDelegate>

{
    IBOutlet UIButton *btnback;
    IBOutlet UIButton *btncancel;

}

//=============
@property (nonatomic, retain) id<SelectImageVideoBase64Delegate> delegate;
//=============

-(IBAction)backBtnAction:(id)sender;
-(IBAction)cancelBtnAction:(id)sender;
@property(nonatomic,retain)NSString*chooseMediaType;
@end
