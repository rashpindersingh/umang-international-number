//
//  AlternateMobileVC.h
//  Umang
//
//  Created by deepak singh rawat on 05/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum
{
    
    ISFROMPROFILEUPDATE= 100,
    ISFROMREGISTRATION,
    ISFROMSETTINGS,
    
    
}TYPE_ALTERNATEMOBILE;

@interface AlternateMobileVC : UIViewController<UIScrollViewDelegate>
{
    
    IBOutlet UIScrollView *scrollView;
    SharedManager*  singleton ;
}
@property (weak, nonatomic) IBOutlet UIView *vwTextBG;
@property (assign,nonatomic )  TYPE_ALTERNATEMOBILE TYPE_ALMNO_CHOOSE;
@property (assign,nonatomic )  NSString *tagtopass;
@property(nonatomic,retain)    NSString *titletopass;
-(IBAction)backbtnAction:(id)sender;

@end
