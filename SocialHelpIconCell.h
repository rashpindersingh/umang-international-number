//
//  SocialHelpIconCell.h
//  Umang
//
//  Created by admin on 02/03/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialHelpIconCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *img_SocialHelp;
@property (weak, nonatomic) IBOutlet UILabel *lblSocialHelp;

@end
