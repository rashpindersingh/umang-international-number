//
//  resultfilterShowMoreVC.h
//  Umang
//
//  Created by admin on 03/02/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterNoResultView.h"
@protocol filterChangeDelegate <NSObject>

- (void)filterChanged:(NSMutableArray*)parameters andCategoryDict:(NSMutableDictionary *)dict;

@end

@interface resultfilterShowMoreVC: UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate>
{
    IBOutlet UIView *vw_noresults;
    IBOutlet UILabel *lb_noresults;
    
    NSMutableArray *table_data;
    SharedManager *singleton;
    FilterNoResultView *noResultsVW;

    
}

@property (nonatomic, weak) id<filterChangeDelegate> delegate;
@property (nonatomic,strong) NSMutableArray *filterBOArray;

@property(nonatomic, retain) IBOutlet UICollectionView *allSer_collectionView;

@property (strong, nonatomic) IBOutlet UITableView *tblSearchFilter;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnFilter;

@property(nonatomic,assign) BOOL isFromHomeFilter;
@property (weak, nonatomic) IBOutlet UIButton *btnSort;
@property (weak, nonatomic) IBOutlet UIButton *btnSettingAgain;
- (IBAction)btnSettingAgainClicked:(id)sender;

@property(nonatomic,strong)NSMutableDictionary *dictFilterParams;

@property(nonatomic,strong)NSMutableArray *arrFilterResponse;


- (IBAction)btnSortActionClicked:(UIButton *)sender;

- (IBAction)btnFilterClicked:(id)sender;

-(IBAction)btnBackClicked:(id)sender;


@property(nonatomic,assign) int isFromIndexFilter;






//--------- Code for Left Menu Open-----------



@end
