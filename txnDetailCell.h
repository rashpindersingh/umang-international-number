//
//  txnDetailCell.h
//  Umang
//
//  Created by admin on 13/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface txnDetailCell : UITableViewCell
@property(nonatomic,retain)IBOutlet UILabel *lbl_title;
@property(nonatomic,retain)IBOutlet UILabel *lbl_desc;


@end
