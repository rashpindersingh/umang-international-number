//
//  CustomPickerCell.h
//  Umang
//
//  Created by spice on 19/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPickerCell : UITableViewCell
@property(nonatomic,retain)IBOutlet UILabel *lbl_title;
@property(nonatomic,retain)IBOutlet UIButton *btn_radio;
@end
