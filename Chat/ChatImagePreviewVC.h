//
//  ChatImagePreviewVC.h
//  Umang
//
//  Created by admin on 17/10/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatImagePreviewVC : UIViewController
{
    IBOutlet UIButton *btnback;
    IBOutlet UIImageView *vw_imgMsg;
}
@property (nonatomic,strong)NSString *imgKindOf;
@property (nonatomic,strong)NSData *sharedImageData;
@property (nonatomic,strong)NSString *imagePath;

-(IBAction)backbtnAction:(id)sender;
@end
