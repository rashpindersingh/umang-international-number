//
//  CustomImageFlowLayout.m
//  AllServiceTabVC
//
//  Created by deepak singh rawat on 29/09/16.
//  Copyright © 2016 deepak singh rawat. All rights reserved.
//

#import "CustomImageFlowLayout.h"

@implementation CustomImageFlowLayout
- (instancetype)init
{
    self = [super init];
    if (self)
    {
        //self.minimumLineSpacing = 0.0;
        //self.minimumInteritemSpacing = 0.0;
       // self.scrollDirection = UICollectionViewScrollDirectionVertical;
        
        
        self.collectionView.backgroundColor =[UIColor redColor];
        //[UIColor colorWithRed:248.0/255.0 green:248.0/255.0 blue:248.0/255.0 alpha:1.0];
        
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        
       // flowLayout.sectionInset = UIEdgeInsetsMake(5, 10, 5, 10);
        
        //flowLayout.itemSize = CGSizeMake(96.0, 140.0);
        [self.collectionView setCollectionViewLayout:flowLayout];
        [self.collectionView setContentOffset:CGPointZero animated:NO];

       
        
    }
    return self;
}

/*
- (CGSize)itemSize
{
    NSInteger numberOfColumns = 3;
    
    CGFloat itemWidth = (CGRectGetWidth(self.collectionView.frame) - (numberOfColumns - 1)) / numberOfColumns;
    return CGSizeMake(itemWidth, itemWidth);
}
*/

- (CGSize)itemSize
{
    //NSInteger numberOfColumns = 3;
    
   // CGFloat itemWidth = (CGRectGetWidth(self.collectionView.frame) - (numberOfColumns - 1)) / numberOfColumns;
   // return CGSizeMake(itemWidth, 137);
    return CGSizeMake(105, 150);
}
@end
