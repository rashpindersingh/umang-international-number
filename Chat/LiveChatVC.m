//
//  LiveChatVC.m
//  Umang
//
//  Created by spice_digital on 27/09/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "LiveChatVC.h"
#import "ContentView.h"
#import "ChatTableViewCell.h"
#import "ChatTableViewCellXIB.h"
#import "ChatCellSettings.h"


#import "UMAPIManager.h"
#import "MBProgressHUD.h"
#import "RunOnMainThread.h"

#import "Base64.h"
#import "ChatImagePreviewVC.h"


#define kSubtitleJobs @"Jobs"
#define kSubtitleWoz @"Sanjay Chauhan"
#define kSubtitleCook @"Mr. Alex"

#define NOTIFY_AND_LEAVE(X) {[self cleanup:X]; return;}
#define DATA(X)    [X dataUsingEncoding:NSUTF8StringEncoding]

// Posting constants
#define IMAGE_CONTENT @"Content-Disposition: form-data; name=\"%@\"; filename=\"%@.jpeg\"\r\nContent-Type: image/jpeg\r\n\r\n"
#define BOUNDARY @"------------0x0x0x0x0x0x0x0x"

@interface iMessage: NSObject

-(id) initIMessageWithName:(NSString *)name
                   message:(NSString *)message
                      time:(NSString *)time
                      type:(NSString *)type ImgUrl:(NSString*)url
           msgcontainImage:(NSData*)imageData;


@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) NSString *userMessage;
@property (strong, nonatomic) NSString *userTime;
@property (strong, nonatomic) NSString *messageType;
@property (strong, nonatomic) NSString *user_ImgUrl;//add by me
@property (strong, nonatomic) NSData *imgData;

@end

@implementation iMessage

-(id) initIMessageWithName:(NSString *)name
                   message:(NSString *)message
                      time:(NSString *)time
                      type:(NSString *)type
                    ImgUrl:(NSString*)url
           msgcontainImage:(NSData*)imageData

{
    self = [super init];
    if(self)
    {
        self.userName = name;
        self.userMessage = message;
        self.userTime = time;
        self.messageType = type;
        self.user_ImgUrl = url;
        self.imgData=imageData;
    }
    
    return self;
}

@end

@interface LiveChatVC ()
@property (weak, nonatomic) IBOutlet UITableView *chatTable;
@property (weak, nonatomic) IBOutlet ContentView *contentView;
@property (weak, nonatomic) IBOutlet UITextView *chatTextView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chatTextViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottomConstraint;

/*Uncomment second line and comment first to use XIB instead of code*/
//@property (strong,nonatomic) ChatTableViewCell *chatCell;
@property (strong,nonatomic) ChatTableViewCellXIB *chatCell;
@property (strong,nonatomic) ChatTableViewCellXIB *chatImageCell;


@property (strong,nonatomic) ContentView *handler;

@property (strong,nonatomic) NSString *requestTime;

@property (nonatomic,strong) NSData *theImage;

@end

@implementation LiveChatVC

{
    NSMutableArray *currentMessages;
    ChatCellSettings *chatCellSettings;
    
    
    NSString *userJIDString;
    NSArray *sectionsArray;
    NSMutableArray *xmppMessagesArray;
    NSMutableArray *historyMessagesArray;
    MBProgressHUD *hud;
    UIActivityIndicatorView *newsSpinner;
    NSString *messageString;
    
    
    NSMutableArray *chatArray;
    // BOOL appDelegate.isHistoryLoaded;
    
    AppDelegate *appDelegate ;
    BOOL connectionError;
    UIButton *btnLoadingChat;
    
    BOOL isTyping;
    
    NSString *uploadImageName;
    
    BOOL isOpenImage;
    
}

@synthesize chatCell;
@synthesize chatImageCell;

/*- (UIStatusBarStyle)preferredStatusBarStyle
 {
 return UIStatusBarStyleDefault;
 }*/
- (void)dealloc
{
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ISTYPINGIDENTIFIER" object:nil];
    
}

-(void)checkAgentTyping
{
    
    if ([[[AppDelegate sharedDelegate] xmppHandler] isAgentTyping] == YES)
    {
        self.typingLabelHeightConstraint.constant = 15;
        self.chatTitleTopConstraint.constant      = 6;
    }
    else
    {
        self.typingLabelHeightConstraint.constant = 0;
        self.chatTitleTopConstraint.constant      = 13;
    }
}
- (void)viewDidLoad
{
    isOpenImage=TRUE;
    
    [super viewDidLoad];
    self.typingLabelHeightConstraint.constant = 0;
    self.chatTitleTopConstraint.constant      = 13;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkAgentTyping)
                                                 name:@"ISTYPINGIDENTIFIER"
                                               object:nil];
    self.typingLabel.text = NSLocalizedString(@"typing", nil);
    newsSpinner = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    CGFloat yAxis=self.chatTable.frame.origin.y+10;
    CGFloat xAxis=self.chatTable.frame.origin.x+self.chatTable.frame.size.width/2-40;
    
    newsSpinner.frame=CGRectMake(xAxis, yAxis, 40, 40);
    [self.view addSubview: newsSpinner];
    
    
    self.chatTextView.layer.borderColor = [UIColor colorWithRed:181.0/255.0f green:181.0/255.0f blue:181.0/255.0f alpha:1.0f].CGColor;
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    //appDelegate.isHistoryLoaded=NO; //default set No
    
    singleton = [SharedManager sharedSingleton];
    
    [btn_back setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btn_back.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btn_back.frame.origin.x, btn_back.frame.origin.y, btn_back.frame.size.width, btn_back.frame.size.height);
        
        [btn_back setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btn_back.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    [btn_endchat setTitle:NSLocalizedString(@"end_chat", nil) forState:UIControlStateNormal];
    chattitle.text = NSLocalizedString(@"live_chat", nil);
    
    chatArray = [[NSMutableArray alloc] init];
    
    // [self setNeedsStatusBarAppearanceUpdate];
    
    
    self.view.backgroundColor=[UIColor colorWithRed:250/255.0 green:250/255.0 blue:250/255.0 alpha:1.0];
    
    currentMessages = [[NSMutableArray alloc] init];
    chatCellSettings = [ChatCellSettings getInstance];
    
    [chatCellSettings setSenderBubbleColorHex:@"007AFF"];
    [chatCellSettings setReceiverBubbleColorHex:@"DFDEE5"];
    [chatCellSettings setSenderBubbleNameTextColorHex:@"FFFFFF"];
    [chatCellSettings setReceiverBubbleNameTextColorHex:@"000000"];
    [chatCellSettings setSenderBubbleMessageTextColorHex:@"FFFFFF"];
    [chatCellSettings setReceiverBubbleMessageTextColorHex:@"000000"];
    [chatCellSettings setSenderBubbleTimeTextColorHex:@"FFFFFF"];
    [chatCellSettings setReceiverBubbleTimeTextColorHex:@"000000"];
    
    /*
     [chatCellSettings setSenderBubbleFontWithSizeForName:[UIFont boldSystemFontOfSize:11]];
     [chatCellSettings setReceiverBubbleFontWithSizeForName:[UIFont boldSystemFontOfSize:11]];
     [chatCellSettings setSenderBubbleFontWithSizeForMessage:[UIFont systemFontOfSize:14]];
     [chatCellSettings setReceiverBubbleFontWithSizeForMessage:[UIFont systemFontOfSize:14]];
     [chatCellSettings setSenderBubbleFontWithSizeForTime:[UIFont systemFontOfSize:11]];
     [chatCellSettings setReceiverBubbleFontWithSizeForTime:[UIFont systemFontOfSize:11]];
     */
    
    [chatCellSettings senderBubbleTailRequired:YES];
    [chatCellSettings receiverBubbleTailRequired:YES];
    
    //  self.navigationItem.title = @"iMessageBubble Demo";
    
    [[self chatTable] setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    UINib *nib = [UINib nibWithNibName:@"ChatSendCell" bundle:nil];
    
    [[self chatTable] registerNib:nib forCellReuseIdentifier:@"chatSend"];
    
    nib = [UINib nibWithNibName:@"ChatReceiveCell" bundle:nil];
    
    [[self chatTable] registerNib:nib forCellReuseIdentifier:@"chatReceive"];
    
    
    
    
    nib = [UINib nibWithNibName:@"ChatImageRecieveCell" bundle:nil];
    
    [[self chatTable] registerNib:nib forCellReuseIdentifier:@"ChatImageRecieveCell"];
    
    
    nib = [UINib nibWithNibName:@"ChatImageSendCell" bundle:nil];
    
    [[self chatTable] registerNib:nib forCellReuseIdentifier:@"ChatImageSendCell"];
    
    
    
    
    
    
    
    
    //Instantiating custom view that adjusts itself to keyboard show/hide
    self.handler = [[ContentView alloc] initWithTextView:self.chatTextView ChatTextViewHeightConstraint:self.chatTextViewHeightConstraint contentView:self.contentView ContentViewHeightConstraint:self.contentViewHeightConstraint andContentViewBottomConstraint:self.contentViewBottomConstraint andContentSendButton:self.sendButton];
    
    //Setting the minimum and maximum number of lines for the textview vertical expansion
    [self.handler updateMinimumNumberOfLines:1 andMaximumNumberOfLine:3];
    
    //Tap gesture on table view so that when someone taps on it, the keyboard is hidden
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    // [self.chatTable addGestureRecognizer:gestureRecognizer];
    self.chatTable.backgroundColor=[UIColor clearColor];
    
    
    
    //[self loadHistroy];
    
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
    if ([[UIScreen mainScreen]bounds].size.height == 812)
    {
        self.backButtonTopConstraint.constant = -15.0f;
        self.endButtonTopConstraint.constant  = -15.0f;
    }
    
    
    
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,fDeviceWidth,40)];
    headerView.backgroundColor=[UIColor clearColor];
    if (appDelegate.isHistoryLoaded==YES)
    {
        [btnLoadingChat setTitle:@"" forState: UIControlStateNormal];
        [btnLoadingChat setTitle:@"" forState: UIControlStateSelected];
        
        
        CGRect frame = CGRectZero;
        headerView.frame=frame;
        return headerView;
        
    }
    else
    {
        
        //  headerView.backgroundColor=[UIColor clearColor];
        UILabel *label = [[UILabel alloc] init];
        label.frame = CGRectMake(0,5,fDeviceWidth, 30);
        label.textColor = [UIColor colorWithRed:0.0/255.0f green:89.0/255.0f blue:157.0/255.0f alpha:1.0];
        //label.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
        label.font = [UIFont boldSystemFontOfSize:16];
        label.text = [self tableView:tableView titleForHeaderInSection:section];
        label.textAlignment=NSTextAlignmentCenter;
        [headerView addSubview:label];
        
        
        btnLoadingChat = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnLoadingChat setTitle:NSLocalizedString(@"view_history", nil) forState: UIControlStateNormal];
        //
        [btnLoadingChat setTitle:NSLocalizedString(@"view_history", nil) forState: UIControlStateSelected];
        
        [btnLoadingChat setTitleColor:[UIColor colorWithRed:0.0/255.0f green:89.0/255.0f blue:157.0/255.0f alpha:1.0] forState:UIControlStateNormal];
        [btnLoadingChat setTitleColor:[UIColor colorWithRed:0.0/255.0f green:89.0/255.0f blue:157.0/255.0f alpha:1.0] forState:UIControlStateSelected];
        
        btnLoadingChat.titleLabel.font = [UIFont boldSystemFontOfSize:16];
        
        [btnLoadingChat addTarget:self
                           action:@selector(refreshTable)forControlEvents:UIControlEventTouchDown];
        
        btnLoadingChat.frame = label.frame;
        [headerView addSubview:btnLoadingChat];
        
        
        
        return headerView;
    }
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    //For each section, you must return here it's label
    if (appDelegate.isHistoryLoaded==YES) {
        
        return @"";
        
    }
    else
        return @"";//NSLocalizedString(@"view_history", nil)
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (appDelegate.isHistoryLoaded==YES) {
        return 0.00001;
        
    }
    else
        return 40.0;
    
}


- (void)refreshTable
{
    
    [self loadhistoryfromServer];
    
}


-(void)loadhistoryfromServer
{
    [self hitAPItoGetMessageHistory];
    // [refreshControl endRefreshing];
    
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dismissKeyboard
{
    //_chatTextView.text = @"Type Here";
    _chatTextView.textColor = [UIColor lightGrayColor];
    
    [self.chatTextView resignFirstResponder];
}

/*- (IBAction)sendMessage:(id)sender
 {
 if([self.chatTextView.text length]!=0)
 {
 
 // NSString *textPass=[NSString stringWithCString:[self.chatTextView.text UTF8String] encoding:NSUTF8StringEncoding];
 
 
 // NSData *data = [textPass dataUsingEncoding:NSNonLossyASCIIStringEncoding];
 NSString *goodValue = self.chatTextView.text;
 
 
 
 if ([[[AppDelegate sharedDelegate] xmppHandler] xmppStream] == nil)
 {
 
 messageString = goodValue;
 
 NSString *jidString = [[NSUserDefaults standardUserDefaults] objectForKey:@"XXMPPmyJID"];
 NSString *passwordString = [[NSUserDefaults standardUserDefaults] objectForKey:@"XXMPPmyPassword"];
 
 userJIDString = jidString;
 
 [[AppDelegate sharedDelegate] configureXMPPWithJid:jidString andPassword:passwordString andChatSession:NO];
 
 [[[AppDelegate sharedDelegate]xmppHandler]connect];
 
 NSString *message = [NSString stringWithFormat:@"%@~~~%@~~~abc",goodValue,singleton.user_tkn];
 [self performSelector:@selector(sendMessageAfterDelay:) withObject:message afterDelay:0.3];
 
 }
 else
 {
 
 messageString = goodValue;
 
 NSString *jidString = [[NSUserDefaults standardUserDefaults] objectForKey:@"XXMPPmyJID"];
 NSString *passwordString = [[NSUserDefaults standardUserDefaults] objectForKey:@"XXMPPmyPassword"];
 
 userJIDString = jidString;
 
 [[AppDelegate sharedDelegate] configureXMPPWithJid:jidString andPassword:passwordString andChatSession:NO];
 
 [[[AppDelegate sharedDelegate]xmppHandler]connect];
 
 NSString *message = [NSString stringWithFormat:@"%@~~~%@~~~abc",goodValue,singleton.user_tkn];
 [self performSelector:@selector(sendMessageAfterDelay:) withObject:message afterDelay:0.3];
 }
 
 
 }
 }
 */



- (IBAction)sendMessage:(id)sender
{
    if([self.chatTextView.text length]!=0)
    {
        
        // NSString *textPass=[NSString stringWithCString:[self.chatTextView.text UTF8String] encoding:NSUTF8StringEncoding];
        
        
        // NSData *data = [textPass dataUsingEncoding:NSNonLossyASCIIStringEncoding];
        NSString *goodValue = self.chatTextView.text;
        
        
        
        if ([[[AppDelegate sharedDelegate] xmppHandler] xmppStream] == nil)
        {
            
            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ROFL"
            //                                                            message:@"Dee dee doo doo."
            //                                                           delegate:self
            //                                                  cancelButtonTitle:@"OK"
            //                                             //     otherButtonTitles:nil];
            //   [alert show];
            
            messageString = goodValue;
            
            NSString *jidString = [[NSUserDefaults standardUserDefaults] objectForKey:@"XXMPPmyJID"];
            NSString *passwordString = [[NSUserDefaults standardUserDefaults] objectForKey:@"XXMPPmyPassword"];
            
            userJIDString = jidString;
            
            [[AppDelegate sharedDelegate] configureXMPPWithJid:jidString andPassword:passwordString andChatSession:NO];
            
            [[[AppDelegate sharedDelegate]xmppHandler]connect];
            
            NSString *message = [NSString stringWithFormat:@"%@~~~%@~~~abc",goodValue,singleton.user_tkn];
            [self performSelector:@selector(sendMessageAfterDelay:) withObject:message afterDelay:0.3];
            
        }
        else
        {
            
            
            NSString *jidString = [[NSUserDefaults standardUserDefaults] objectForKey:@"XXMPPmyJID"];
            //NSString *passwordString = [[NSUserDefaults standardUserDefaults] objectForKey:@"XXMPPmyPassword"];
            
            userJIDString = jidString;
            
            //[[AppDelegate sharedDelegate] configureXMPPWithJid:jidString andPassword:passwordString andChatSession:NO];
            
            //[[[AppDelegate sharedDelegate]xmppHandler]connect];
            
            NSString *message = [NSString stringWithFormat:@"%@~~~%@~~~abc",goodValue,singleton.user_tkn];
            [self performSelector:@selector(sendMessageAfterDelay:) withObject:message afterDelay:0.3];
        }
        
        
        /* iMessage *sendMessage;
         
         
         
         NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
         [dateFormatter setLocale: [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] ];
         dateFormatter.dateFormat = @"dd/MM/yyyy";
         
         NSDateFormatter * dateFormatter1 = [[NSDateFormatter alloc] init];
         [dateFormatter1 setDateFormat: @"MMM dd, hh:mm"];
         NSString *senddateString = [dateFormatter1 stringFromDate: [NSDate date]]; //Put whatever date you want
         
         //  NSString *senderImgURL=[NSString stringWithFormat:@"https://kmp.spicedigital.in/photo/%@",@"CH-E00981"];
         
         sendMessage = [[iMessage alloc] initIMessageWithName:@"Parampreet Dhatt" message:self.chatTextView.text time:senddateString type:@"self" ImgUrl:nil];
         
         
         
         [self updateTableView:sendMessage];*/
    }
}



- (IBAction)receiveMessage:(id)sender
{
    
    iMessage *receiveMessage;
    
    
    receiveMessage = [[iMessage alloc] initIMessageWithName:@"Customer Support" message:self.chatTextView.text time:@"Sept 27, 3:52" type:@"other" ImgUrl:nil msgcontainImage:nil];
    
    [self updateTableView:receiveMessage];
    
    
}

-(void)scolltodown
{
    if([self.chatTable numberOfRowsInSection:0]!=0)
    {
        NSIndexPath* ip = [NSIndexPath indexPathForRow:[self.chatTable numberOfRowsInSection:0]-1 inSection:0];
        [self.chatTable scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionBottom animated:UITableViewRowAnimationLeft];
    }
    
}

-(void) updateTableView:(iMessage *)msg
{
    //[self.chatTextView setText:@""];
    [self.handler textViewDidChange:self.chatTextView];
    
    [self.chatTable beginUpdates];
    
    NSIndexPath *row1 = [NSIndexPath indexPathForRow:currentMessages.count inSection:0];
    
    [currentMessages insertObject:msg atIndex:currentMessages.count];
    
    [self.chatTable insertRowsAtIndexPaths:[NSArray arrayWithObjects:row1, nil] withRowAnimation:UITableViewRowAnimationBottom];
    
    [self.chatTable endUpdates];
    
    
    //Always scroll the chat table when the user sends the message
    [self  scolltodown];
    
}

#pragma mark - Attach Method

- (IBAction)attachButtonAction:(UIButton *)sender
{
    [self btnOpenSheet];
}

- (void)btnOpenSheet

{
    NSString *camera=[NSLocalizedString(@"camera", nil) capitalizedString];
    NSString *gallery=[NSLocalizedString(@"gallery", nil) capitalizedString];
    NSString *cancel=NSLocalizedString(@"cancel", nil);
    
    
    UIActionSheet *  sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                         delegate:self
                                                cancelButtonTitle:cancel
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:camera,gallery, nil];
    
    // Show the sheet
    [sheet showInView:self.view];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            [self openCamera];
        }
            break;
        case 1:
        {
            [self openGallary];
        }
            break;
        case 2:
        {
            
        }
            break;
        default:
            break;
    }
}

-(void)openCamera
{
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }
    else
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = NO;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        dispatch_async(dispatch_get_main_queue(), ^ {
            
            
            
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
            {
                UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:picker];
                
                [popover presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
            }
            else
            {
                [self presentViewController:picker animated:NO completion:nil];
            }
            
        });
    }
}


-(void)openGallary
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    picker.navigationBar.barTintColor = [UIColor whiteColor];
    picker.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:14.0/255.0f green:122.0/255.0 blue:254.0/255.0f alpha:1.0]};
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    isOpenImage = false;
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
            
            UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:picker];
            
            [popover presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
        }
        else
        {
            [self presentViewController:picker animated:YES completion:nil];
        }
        
    });
    
}

//=====================================================================
//==================         Camera Delegates       ===================
//=====================================================================


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *beforeCrop = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/ChatImageFolder"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    
    NSString *fileName = [NSString stringWithFormat:@"%@/Image-%@.jpeg",dataPath,dateString];
    
    NSData *imageData = UIImageJPEGRepresentation(beforeCrop, 0.5);
    
    
    /*NSString *base64String = [imageData base64Encoding]; //my base64Encoding function
     
     __block NSData *base64data = [[NSData alloc] initWithData:[NSData dataWithBase64EncodedString:base64String]];*/
    
    
    [imageData writeToFile:fileName atomically:YES];
    
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        NSString *imageName = [[fileName componentsSeparatedByString:@"/"] lastObject];
        uploadImageName = [NSString stringWithFormat:@"/%@",imageName];
        
        
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            [self syncUpload:imageData];
            
            [RunOnMainThread runBlockInMainQueueIfNecessary:^{
                
                [self imageMessageCellUpdatesWithData:imageData];
            }];
        });
        
        
        
    }];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Image Uploading Task Methods

- (void) syncUpload:(NSData *) uploadImage
{
    self.theImage = uploadImage;
    [self main];
}

- (void) cleanup: (NSString *) output
{
    self.theImage = nil;
    //    NSLog(@"******** %@", output);
}

- (NSMutableData*)generateFormDataFromPostDictionary:(NSDictionary*)dict
{
    
    id boundary = BOUNDARY;
    NSArray* keys = [dict allKeys];
    NSMutableData* result = [NSMutableData data];
    
    for (int i = 0; i < [keys count]; i++)
    {
        id value = [dict valueForKey: [keys objectAtIndex:i]];
        [result appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        if ([value isKindOfClass:[NSData class]])
        {
            // handle image data
            NSString *formstring = [NSString stringWithFormat:IMAGE_CONTENT, [keys objectAtIndex:i],uploadImageName];
            [result appendData: DATA(formstring)];
            [result appendData:value];
        }
        else
        {
            NSString *formstring = [NSString stringWithFormat:@"Content-Disposition: form-data; name=\"token\"\r\n\r\n%@", value];
            [result appendData:DATA(formstring)];
        }
        
        NSString *formstring = @"\r\n";
        [result appendData:DATA(formstring)];
    }
    
    NSString *formstring =[NSString stringWithFormat:@"--%@--\r\n", boundary];
    [result appendData:DATA(formstring)];
    return result;
}


// NSOperationQueueによる非同期化を見据えてmainにしています。
- (void) main
{
    if (!self.theImage)
    {
        NOTIFY_AND_LEAVE(@"Please set image before uploading.");
    }
    NSString* MultiPartContentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BOUNDARY];
    
    NSMutableDictionary* post_dict = [[NSMutableDictionary alloc] init];
    [post_dict setObject:singleton.user_tkn forKey:@"token"];
    [post_dict setObject:self.theImage forKey:@"file"];
    
    NSMutableData *postData = [self generateFormDataFromPostDictionary:post_dict];
    //[postData appendData:[[NSString stringWithFormat:@"token=%@",singleton.user_tkn]dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSString *baseurl = [NSString stringWithFormat:@"%@%@",singleton.apiMode.ChatBaseUrl,UM_API_CHAT_IMAGE];
    NSURL *url = [NSURL URLWithString:baseurl];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    if (!urlRequest) NOTIFY_AND_LEAVE(@"Error creating the URL Request");
    
    [urlRequest setHTTPMethod: @"POST"];
    [urlRequest setValue:MultiPartContentType forHTTPHeaderField: @"Content-Type"];
    [urlRequest setHTTPBody:postData];
    
    NSError *error;
    NSURLResponse *response;
    NSData* result = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
    //NSLog(@"***** result =%@", result);
    
    if (!result)
    {
        [self cleanup:[NSString stringWithFormat:@"upload error: %@", [error localizedDescription]]];
        return;
    }
    
    
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
    
    if (httpResponse.statusCode == 200)
    {
        NSLog(@"Success");
        
        [RunOnMainThread runBlockInMainQueueIfNecessary:^{
            
            UIImage *img = [UIImage imageWithData:self.theImage];
            [self sendImageMessage:img];
        }];
        
    }
    else
    {
        NSLog(@"Failure");
        
        NSString *outstring = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
        
        [RunOnMainThread runBlockInMainQueueIfNecessary:^{
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:outstring preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *alertActionConfirm = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                                 {
                                                     
                                                     
                                                 }];
            
            [alertController addAction:alertActionConfirm];
            
            [self presentViewController:alertController animated:YES completion:^{
            }];
        }];
        
        
        
    }
    
    
    // Return results
    NSString *outstring = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    NSLog(@"***** outstring =%@", outstring);
    [self cleanup: outstring];
}

#pragma mark - UITableViewDatasource methods

/*- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
 {
 return 1;
 }*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if (currentMessages) {
        
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        return 1;
        
    } else {
        
        // Display a message when the table is empty
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        
        messageLabel.text = NSLocalizedString(@"no_result_found", nil);
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
        [messageLabel sizeToFit];
        tableView.backgroundView = messageLabel;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    return 0;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return currentMessages.count;
}

/*- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
 {
 iMessage *message = [currentMessages objectAtIndex:indexPath.row];
 
 if([message.messageType isEqualToString:@"self"])
 {
 //Uncomment second line and comment first to use XIB instead of code
 // chatCell = (ChatTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"chatSend"];
 chatCell = (ChatTableViewCellXIB *)[tableView dequeueReusableCellWithIdentifier:@"chatSend"];
 
 chatCell.chatMessageLabel.text = message.userMessage;
 
 chatCell.chatNameLabel.text = message.userName;
 
 chatCell.chatTimeLabel.text = message.userTime;
 
 chatCell.chatUserImage.image = [UIImage imageNamed:@"profile_img"];
 
 
 //Comment this line is you are using XIB
 // chatCell.authorType = iMessageBubbleTableViewCellAuthorTypeSender;
 }
 else
 {
 //Uncomment second line and comment first to use XIB instead of code
 //chatCell = (ChatTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"chatReceive"];
 chatCell = (ChatTableViewCellXIB *)[tableView dequeueReusableCellWithIdentifier:@"chatReceive"];
 
 chatCell.chatMessageLabel.text = message.userMessage;
 
 chatCell.chatNameLabel.text = message.userName;
 
 chatCell.chatTimeLabel.text = message.userTime;
 
 chatCell.chatUserImage.image = [UIImage imageNamed:@"icon_user"];
 
 
 
 if ([message.userMessage isEqualToString:@"session timeout"])
 {
 
 UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"session_expire_msg", nil) preferredStyle:UIAlertControllerStyleAlert];
 
 UIAlertAction *alertActionConfirm = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
 
 {
 
 //===========set time to blank=================
 [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
 [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"chatRequestTime"];
 [[NSUserDefaults standardUserDefaults] synchronize];
 
 
 [appDelegate.chatHistoryLoad  removeAllObjects];
 [appDelegate.chatBotLoad  removeAllObjects];
 [currentMessages removeAllObjects];
 appDelegate.isHistoryLoaded=NO;
 //===========set time to blank=================
 
 
 [[[AppDelegate sharedDelegate]xmppHandler] sendMessage:[NSString stringWithFormat:@"userdisconnected~~~%@~~~exit",singleton.user_tkn] toAdress:@"bot@reporting.umang.gov.in" withType:@"chat"];
 
 
 //[self deleteChatHistoryAnother];
 
 [self performSelector:@selector(moveToViewAfterDeletingMessages) withObject:nil afterDelay:1.0];
 
 
 }];
 
 [alertController addAction:alertActionConfirm];
 
 
 if ([self.comingFromString isEqualToString:@"Department"])
 {
 id rootViewController = [UIApplication sharedApplication].delegate.window.rootViewController;
 
 if([rootViewController isKindOfClass:[UITabBarController class]])
 {
 [self presentViewController:alertController animated:YES completion:^{
 }];
 }
 
 }
 else
 {
 id rootViewController = self.navigationController.topViewController;
 
 if ([rootViewController isKindOfClass:[LiveChatVC class]])
 {
 [rootViewController presentViewController:alertController animated:YES completion:nil];
 }
 }
 
 }
 
 //Comment this line is you are using XIB
 //chatCell.authorType = iMessageBubbleTableViewCellAuthorTypeReceiver;
 }
 chatCell.backgroundColor=[UIColor clearColor];
 
 chatCell.selectionStyle = UITableViewCellSelectionStyleNone;
 
 return chatCell;
 
 }*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    iMessage *message = [currentMessages objectAtIndex:indexPath.row];
    
    if (message.imgData != nil)//contain image url
    {
        
        isOpenImage=FALSE;
        
        ChatImagePreviewVC *vc = [[ChatImagePreviewVC alloc] initWithNibName:@"ChatImagePreviewVC" bundle:nil];
        vc.imgKindOf=@"Data";
        vc.sharedImageData = message.imgData;
        
        [self presentViewController:vc animated:YES completion:nil];
        
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    iMessage *message = [currentMessages objectAtIndex:indexPath.row];
    
    if([message.messageType isEqualToString:@"self"])
    {
        
        /*Uncomment second line and comment first to use XIB instead of code*/
        // chatCell = (ChatTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"chatSend"];
        //ChatImageRecieveCell
        //ChatImageSendCell
        
        
        if (message.imgData != nil)//contain image url
        {
            if (message.imgData)
            {
                chatImageCell= (ChatTableViewCellXIB *)[tableView dequeueReusableCellWithIdentifier:@"ChatImageSendCell"];
                chatImageCell.chatMessageLabel.text = message.userMessage;
                chatImageCell.chatNameLabel.text = message.userName;
                chatImageCell.chatTimeLabel.text = message.userTime;
                chatImageCell.chatUserImage.image = [UIImage imageNamed:@"profile_img"];
                chatImageCell.msgImage.image = [UIImage imageWithData:message.imgData];
                chatImageCell.msgImage.contentMode = UIViewContentModeScaleAspectFill;
                chatImageCell.msgImage.layer.cornerRadius = 5;
                chatImageCell.msgImage.layer.masksToBounds = 5 > 0;
                
                [chatImageCell.msgImage.layer setBorderColor: [[UIColor lightGrayColor] CGColor]];
                [chatImageCell.msgImage.layer setBorderWidth: 1.0];
                chatImageCell.backgroundColor=[UIColor clearColor];
                
                chatImageCell.userInteractionEnabled=YES;
                
                chatImageCell.selectionStyle = UITableViewCellSelectionStyleNone;
                chatImageCell.contentView.userInteractionEnabled = YES;
                
                return chatImageCell;
                
            }
        }
        else
        {
            
            chatCell = (ChatTableViewCellXIB *)[tableView dequeueReusableCellWithIdentifier:@"chatSend"];
            chatCell.chatMessageLabel.text = message.userMessage;
            chatCell.chatNameLabel.text = message.userName;
            chatCell.chatTimeLabel.text = message.userTime;
            chatCell.chatUserImage.image = [UIImage imageNamed:@"profile_img"];
            
            chatCell.backgroundColor=[UIColor clearColor];
            
            chatCell.selectionStyle = UITableViewCellSelectionStyleNone;
            chatCell.userInteractionEnabled=YES;
            chatCell.contentView.userInteractionEnabled = YES;
            
            return chatCell;
            
        }
        
    }
    else
    {
        
        chatImageCell= (ChatTableViewCellXIB *)[tableView dequeueReusableCellWithIdentifier:@"ChatImageRecieveCell"];
        
        
        if (message.imgData!= nil)//contain image url
        {
            
            if (message.imgData)
            {
                chatImageCell.chatMessageLabel.text = message.userMessage;
                chatImageCell.chatNameLabel.text = message.userName;
                chatImageCell.chatTimeLabel.text = message.userTime;
                chatImageCell.chatUserImage.image = [UIImage imageNamed:@"profile_img"];
                chatImageCell.msgImage.image = [UIImage imageWithData:message.imgData];
                chatImageCell.msgImage.contentMode=UIViewContentModeScaleAspectFit;
                
                // UIColor *borderColor = [UIColor colorWithRed:182 green:10 blue:96 alpha:1.0];
                
                chatImageCell.msgImage.layer.cornerRadius = 5;
                chatImageCell.msgImage.layer.masksToBounds = 5 > 0;
                
                [chatImageCell.msgImage.layer setBorderColor: [[UIColor lightGrayColor] CGColor]];
                [chatImageCell.msgImage.layer setBorderWidth: 4.0];
                chatImageCell.backgroundColor=[UIColor clearColor];
                
                
                chatImageCell.userInteractionEnabled=YES;
                
                chatImageCell.selectionStyle = UITableViewCellSelectionStyleNone;
                chatImageCell.contentView.userInteractionEnabled = YES;
                
                return chatImageCell;
            }
        }
        else
        {
            chatCell = (ChatTableViewCellXIB *)[tableView dequeueReusableCellWithIdentifier:@"chatReceive"];
            chatCell.chatMessageLabel.text = message.userMessage;
            chatCell.chatNameLabel.text = message.userName;
            chatCell.chatTimeLabel.text = message.userTime;
            chatCell.chatUserImage.image = [UIImage imageNamed:@"icon_user"];
            
            
            chatCell.backgroundColor=[UIColor clearColor];
            
            chatCell.selectionStyle = UITableViewCellSelectionStyleNone;
            chatCell.userInteractionEnabled=YES;
            chatCell.contentView.userInteractionEnabled = YES;
            
            return chatCell;
            
        }
        
        if ([message.userMessage isEqualToString:@"session timeout"])
        {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"session_expire_msg", nil) preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *alertActionConfirm = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                                 
                                                 {
                                                     
                                                     //===========set time to blank=================
                                                     [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                                                     [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"chatRequestTime"];
                                                     [[NSUserDefaults standardUserDefaults] synchronize];
                                                     
                                                     
                                                     [appDelegate.chatHistoryLoad  removeAllObjects];
                                                     [appDelegate.chatBotLoad  removeAllObjects];
                                                     [currentMessages removeAllObjects];
                                                     appDelegate.isHistoryLoaded=NO;
                                                     //===========set time to blank=================
                                                     
                                                     
                                                     [[[AppDelegate sharedDelegate]xmppHandler] sendMessage:[NSString stringWithFormat:@"userdisconnected~~~%@~~~exit",singleton.user_tkn] toAdress:@"bot@reporting.umang.gov.in" withType:@"chat"];
                                                     
                                                     
                                                     //[self deleteChatHistoryAnother];
                                                     
                                                     [self performSelector:@selector(moveToViewAfterDeletingMessages) withObject:nil afterDelay:1.0];
                                                     
                                                     
                                                 }];
            
            [alertController addAction:alertActionConfirm];
            
            
            if ([self.comingFromString isEqualToString:@"Department"])
            {
                id rootViewController = [UIApplication sharedApplication].delegate.window.rootViewController;
                
                if([rootViewController isKindOfClass:[UITabBarController class]])
                {
                    [self presentViewController:alertController animated:YES completion:^{
                    }];
                }
                
            }
            else
            {
                id rootViewController = self.navigationController.topViewController;
                
                if ([rootViewController isKindOfClass:[LiveChatVC class]])
                {
                    [rootViewController presentViewController:alertController animated:YES completion:nil];
                }
            }
            
        }
        
        /*Comment this line is you are using XIB*/
        //chatCell.authorType = iMessageBubbleTableViewCellAuthorTypeReceiver;
    }
    
    
    chatCell.backgroundColor=[UIColor clearColor];
    chatCell.selectionStyle = UITableViewCellSelectionStyleNone;
    chatCell.userInteractionEnabled=YES;
    chatCell.contentView.userInteractionEnabled = YES;
    
    return chatCell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    iMessage *message = [currentMessages objectAtIndex:indexPath.row];
    
    
    CGSize size;
    
    CGSize Messagesize;
    
    NSArray *fontArray;
    
    
    if (message.imgData != nil)//contain image url then image shows
    {
        
        size.height = 140 + 10.0f;
        
    }
    else
        
    {
        
        
        //Get the chal cell font settings. This is to correctly find out the height of each of the cell according to the text written in those cells which change according to their fonts and sizes.
        //If you want to keep the same font sizes for both sender and receiver cells then remove this code and manually enter the font name with size in Namesize, Messagesize and Timesize.
        if([message.messageType isEqualToString:@"self"])
        {
            fontArray = chatCellSettings.getSenderBubbleFontWithSize;
        }
        else
        {
            fontArray = chatCellSettings.getReceiverBubbleFontWithSize;
        }
        
        NSLog(@"Message Is %@",message.userMessage);
        
        if (singleton.fontSizeSelectedIndex == 2)
        {
            Messagesize = [message.userMessage boundingRectWithSize:CGSizeMake(200.0f, CGFLOAT_MAX)
                                                            options:NSStringDrawingUsesLineFragmentOrigin
                                                         attributes:@{NSFontAttributeName:fontArray[1]}
                                                            context:nil].size;
            
            size.height = Messagesize.height + 70.0f;
        }
        else if (singleton.fontSizeSelectedIndex == 1)
        {
            Messagesize = [message.userMessage boundingRectWithSize:CGSizeMake(200.0f, CGFLOAT_MAX)
                                                            options:NSStringDrawingUsesLineFragmentOrigin
                                                         attributes:@{NSFontAttributeName:fontArray[1]}
                                                            context:nil].size;
            
            if (Messagesize.height > 200)
            {
                size.height = Messagesize.height ;
            }
            else
            {
                size.height = Messagesize.height + 54.0f;
                
            }
            
        }
        else
        {
            Messagesize = [message.userMessage boundingRectWithSize:CGSizeMake(200.0f, CGFLOAT_MAX)
                                                            options:NSStringDrawingUsesLineFragmentOrigin
                                                         attributes:@{NSFontAttributeName:fontArray[1]}
                                                            context:nil].size;
            
            size.height = Messagesize.height + 48.0f;
        }
        
    }
    
    return size.height;
}


-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    // _chatTextView.text = @"";
    if ([_chatTextView.text isEqualToString:NSLocalizedString(@"type_your_message", nil)])
    {
        _chatTextView.text = @"";
    }
    _chatTextView.textColor = [UIColor blackColor];
    return YES;
}


-(void)textViewDidChange:(UITextView *)textView
{
    
    if(_chatTextView.text.length == 0)
    {
        _chatTextView.textColor = [UIColor lightGrayColor];
        _chatTextView.text = NSLocalizedString(@"type_your_message", nil);//
        [_chatTextView resignFirstResponder];
        
        
        [self.sendButton setTitleColor:[UIColor colorWithRed:114.0/255.0f green:114.0/255.0f blue:114.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    }
    else
    {
        [self.sendButton setTitleColor:[UIColor colorWithRed:53.0/255.0f green:184.0/255.0f blue:104.0/255.0f alpha:1.0] forState:UIControlStateNormal];
    }
}


-(void)viewWillAppear:(BOOL)animated
{
    
    singleton = [SharedManager sharedSingleton];
    
    isOpenImage = true ;
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    
    
    NSLog(appDelegate.isHistoryLoaded?@"appDelegate.isHistoryLoaded true":@"appDelegate.isHistoryLoaded false");
    
    NSLog(@"ChatSession  %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"ChatSession"]);
    
    sectionsArray = [NSArray new];
    xmppMessagesArray = [NSMutableArray new];
    chatArray= [NSMutableArray new];
    
    self.tabBarController.tabBar.hidden = YES;
    self.view.backgroundColor=[UIColor whiteColor];
    
    connectionError = NO;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"ChatSession"] == nil || [[[NSUserDefaults standardUserDefaults] objectForKey:@"ChatSession"] isEqualToString:@"N"] || [[[AppDelegate sharedDelegate] xmppHandler] xmppStream] == nil)
    {
        
        [appDelegate.chatHistoryLoad  removeAllObjects];
        [appDelegate.chatBotLoad  removeAllObjects];
        [currentMessages removeAllObjects];
        appDelegate.isHistoryLoaded=NO;
        [_chatTable reloadData];
        
        [self hitApiOpenChatCondition];
    }
    else
    {
        
        
        NSLog(@"Stream Object %@",[[[AppDelegate sharedDelegate] xmppHandler] xmppStream]);
        
        //NSString *jidString = [[NSUserDefaults standardUserDefaults] objectForKey:@"XXMPPmyJID"];
        //NSString *passwordString = [[NSUserDefaults standardUserDefaults] objectForKey:@"XXMPPmyPassword"];
        
        [[AppDelegate sharedDelegate]setChatDelegate:self];
        
        [[NSUserDefaults standardUserDefaults] setValue:@"Y" forKey:@"ChatSession"];
        
        self.userJID = [XMPPJID jidWithString:@"bot@reporting.umang.gov.in"];
        
        self.fetchedResultController = nil;
        sectionsArray = [[self fetchedResultsController] sections];
        
        //  if (appDelegate.isHistoryLoaded==YES) {
        // chatHistoryLoad
        
        if ([appDelegate.chatHistoryLoad count]>0)
        {
            chatArray=[appDelegate.chatHistoryLoad mutableCopy];
            [appDelegate.chatHistoryLoad removeAllObjects];
            [self sortBotArray:chatArray];
        }
        // }
        
    }
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    singleton.chatBadgeCount = @"0";
    
    [super viewWillAppear:NO];
}

//code to sort array in order to date order
-(void)sortBotArray:(NSArray*)histroyarr
{
    //[appDelegate.chatBotLoad removeAllObjects];
    
    NSMutableArray *chatContentArr=[histroyarr mutableCopy];
    [currentMessages removeAllObjects];
    [chatArray removeAllObjects];
    [chatArray addObjectsFromArray:chatContentArr];
    // [_chatTable reloadData];
    [self loadBotHistroy:chatArray];
}


-(void)loadBotHistroy:(NSArray*)SortArray
{
    for (int i=0; i<[SortArray count]; i++)
    {
        
        iMessage *receiveMessage;
        NSString *agentId=[[SortArray objectAtIndex:i] valueForKey:@"agentId"];
        NSString *msg=[[SortArray objectAtIndex:i] valueForKey:@"msg"];
        NSString *time=[[SortArray objectAtIndex:i] valueForKey:@"time"];
        
        NSString *fetchDatetime=@"";
        
        
        
        
        NSLog(@"string does not contain bla");
        
        
        NSDateFormatter * df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"HH:mm:ss"];
        NSDate *gethistorydate = [df dateFromString:time]; // here you can fetch date from string with define format
        
        if (gethistorydate ==nil)
        {
            fetchDatetime=time;
        }
        else
        {
            NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat: @"hh:mm a"];
            fetchDatetime = [dateFormatter
                             stringFromDate:gethistorydate]; //Put whatever date you want
            
        }
        
        NSData *imageData = [[SortArray objectAtIndex:i] valueForKey:@"containImage"];
        
        if (agentId != nil)
        {
            
            
            if (imageData != nil)
            {
                receiveMessage = [[iMessage alloc] initIMessageWithName:@"" message:msg time:fetchDatetime type:@"other" ImgUrl:nil msgcontainImage:imageData];
            }
            else
            {
                receiveMessage = [[iMessage alloc] initIMessageWithName:@"" message:msg time:fetchDatetime type:@"other" ImgUrl:nil msgcontainImage:nil];
            }
            
        }
        else
        {
            
            if (imageData != nil)
            {
                receiveMessage = [[iMessage alloc] initIMessageWithName:@"" message:msg time:fetchDatetime type:@"self" ImgUrl:nil  msgcontainImage:imageData];
            }
            else
            {
                receiveMessage = [[iMessage alloc] initIMessageWithName:@"" message:msg time:fetchDatetime type:@"self" ImgUrl:nil  msgcontainImage:nil];
            }
            
        }
        
        //  [currentMessages insertObject:msg atIndex:currentMessages.count];
        [currentMessages addObject:receiveMessage];
    }
    
    
    [_chatTable reloadData];
    [self  scolltodown];
    
    _chatTextView.text = NSLocalizedString(@"type_your_message", nil);//
    
    _chatTextView.textColor = [UIColor lightGrayColor];
    
    
}



-(void)viewWillDisappear:(BOOL)animated
{
    
    [appDelegate.chatHistoryLoad  removeAllObjects];
    
    if (appDelegate.isHistoryLoaded==YES)
    {
        // chatHistoryLoad
        
        if ([chatArray count]>0)
        {
            [chatArray addObjectsFromArray:appDelegate.chatBotLoad];
            [appDelegate.chatBotLoad removeAllObjects];
            
            appDelegate.chatHistoryLoad =[chatArray mutableCopy];
            [chatArray removeAllObjects];
            
            // [self sortArray:chatArray];
        }
    }
    else
    {
        [appDelegate.chatHistoryLoad addObjectsFromArray:appDelegate.chatBotLoad];
    }
    
    
    
    //[appDelegate.chatBotLoad removeAllObjects];
    //[appDelegate.chatBotLoad removeAllObjects];
    
    NSLog(@"chatHistoryLoad=%@",appDelegate.chatHistoryLoad);
    NSLog(@"appDelegate.chatBotLoad=%@",appDelegate.chatBotLoad);
    
    self.fetchedResultController = nil;
    
    
    //    NSInteger fontIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_FONTSIZE_INDEX"];
    //
    //    if (fontIndex==0)
    //    {
    //        CGFloat fontsize=14.0;
    //        [[UILabel appearance] setFont:[UIFont systemFontOfSize:fontsize]];
    //
    //
    //    }
    //    if (fontIndex==1)
    //    {
    //        CGFloat fontsize=16.0;
    //        [[UILabel appearance] setFont:[UIFont systemFontOfSize:fontsize]];
    //
    //    }
    //    if (fontIndex==2)
    //    {
    //        CGFloat fontsize=18.0;
    //        [[UILabel appearance] setFont:[UIFont systemFontOfSize:fontsize]];
    //
    //    }
    
    //[UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    singleton.chatBadgeCount = @"0";
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CHAT_MESSAGE object:nil];
    [super viewWillDisappear:NO];
}
//================ Create USER and CHAT SESSION START==========

-(void)hitApiOpenChatCondition
{
    
    NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if (selectedLanguage == nil)
    {
        selectedLanguage = @"en";
    }
    else{
        NSArray *arrTemp = [selectedLanguage componentsSeparatedByString:@"-"];
        selectedLanguage = [arrTemp firstObject];
    }
    
    
    NSString *node = [[NSUserDefaults standardUserDefaults] objectForKey:@"NODE_KEY"];
    
    if (node == nil)
    {
        node =@"";
    }
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.label.text = NSLocalizedString(@"connection_please_wait", nil);
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *mapData = [NSMutableDictionary new];
    
    
    double CurrentTime = CACurrentMediaTime();
    
    NSString * timeInMS = [NSString stringWithFormat:@"%f", CurrentTime];
    
    timeInMS=  [timeInMS stringByReplacingOccurrencesOfString:@"." withString:@""];
    
    
    [mapData setObject:timeInMS forKey:@"requestId"];
    [mapData setObject:singleton.appUniqueID forKey:@"did"];
    [mapData setObject:@"" forKey:@"imei"];
    [mapData setObject:@"" forKey:@"imsi"];
    [mapData setObject:@"Apple" forKey:@"hmk"];
    [mapData setObject:[[UIDevice currentDevice] model] forKey:@"hmd"];
    [mapData setObject:@"ios" forKey:@"os"];
    [mapData setObject:singleton.apiMode.PACKAGE_NAME forKey:@"pkg"];
    [mapData setObject:@"1" forKey:@"ver"];
    [mapData setObject:singleton.user_tkn forKey:@"tkn"];
    [mapData setObject:[self isJailbroken]?@"yes":@"no" forKey:@"rot"];
    [mapData setObject:@"app" forKey:@"mod"];
    [mapData setObject:@"404" forKey:@"mcc"];
    [mapData setObject:@"02" forKey:@"mnc"];
    [mapData setObject:@"96" forKey:@"lac"];
    [mapData setObject:@"611346" forKey:@"clid"];
    [mapData setObject:@"" forKey:@"acc"];
    [mapData setObject:@"" forKey:@"lat"];
    [mapData setObject:@"" forKey:@"lon"];
    [mapData setObject:@"" forKey:@"peml"];
    [mapData setObject:selectedLanguage forKey:@"lang"];
    [mapData setObject:@"" forKey:@"aadhr"];
    [mapData setObject:@"" forKey:@"mno"];
    [mapData setObject:@"" forKey:@"st"];
    [mapData setObject:node forKey:@"node"];
    
    
    [objRequest hitXMPPChatWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_CHAT withBody:mapData andTag:TAG_REQUEST_CHAT completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
     {
         
         if (error == nil)
         {
             
             @try
             {
                 if (![response isKindOfClass:[NSNull class]] )
                 {
                     
                     NSString *jidString = [NSString stringWithFormat:@"%@@reporting.umang.gov.in",[response valueForKey:@"id"]];
                     NSString *passowrdString = [NSString stringWithFormat:@"%@",[response valueForKey:@"accessSp"]];
                     
                     userJIDString = jidString;
                     
                     connectionError = YES;
                     
                     [self performSelector:@selector(showConnectionErrorAlert) withObject:nil afterDelay:30];
                     
                     [[AppDelegate sharedDelegate] configureXMPPWithJid:jidString andPassword:passowrdString andChatSession:NO];
                     
                     [[AppDelegate sharedDelegate]setChatDelegate:self];
                     [[[AppDelegate sharedDelegate]xmppHandler]connect];
                     
                 }
                 
             } @catch (NSException *exception)
             {
                 
             } @finally
             {
                 
             }
             
         }
         else
         {
             
             if ([response valueForKey:@"accessSp"] == nil || [response valueForKey:@"accessSp"] == [NSNull null])
             {
                 connectionError = YES;
                 [self performSelector:@selector(showConnectionErrorAlert) withObject:nil afterDelay:30];
             }
             else
             {
                 [hud hideAnimated:YES];
                 
                 
                 NSData *data = [[response valueForKey:@"accessSp"] dataUsingEncoding:NSNonLossyASCIIStringEncoding];
                 NSString *valueUnicode = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                 
                 
                 NSData *dataa = [valueUnicode dataUsingEncoding:NSUTF8StringEncoding];
                 NSString *valueEmoj = [[NSString alloc] initWithData:dataa encoding:NSNonLossyASCIIStringEncoding];
                 
                 UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:valueEmoj preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction *alertActionConfirm = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                                      {
                                                          [self backBtnAction:nil];
                                                          
                                                      }];
                 
                 [alertController addAction:alertActionConfirm];
                 
                 [self presentViewController:alertController animated:YES completion:^{
                 }];
             }
             
             
         }
         
     }];
    
}

-(BOOL)isJailbroken
{
    NSURL* url = [NSURL URLWithString:@"cydia://package/com.example.package"];
    return [[UIApplication sharedApplication] canOpenURL:url];
}

/*- (BOOL)isJailbroken
 {
 BOOL jailbroken = NO;
 NSArray *jailbrokenPath = [NSArray arrayWithObjects:@"/Applications/Cydia.app",  @"/Applications/RockApp.app",  @"/Applications/Icy.app",  @"/usr/sbin/sshd",  @"/usr/bin/sshd",  @"/usr/libexec/sftp-server",  @"/Applications/WinterBoard.app",  @"/Applications/SBSettings.app",  @"/Applications/MxTube.app",  @"/Applications/IntelliScreen.app",  @"/Library/MobileSubstrate/DynamicLibraries/Veency.plist",  @"/Applications/FakeCarrier.app",  @"/Library/MobileSubstrate/DynamicLibraries/LiveClock.plist",  @"/private/var/lib/apt",  @"/Applications/blackra1n.app",  @"/private/var/stash",  @"/private/var/mobile/Library/SBSettings/Themes",  @"/System/Library/LaunchDaemons/com.ikey.bbot.plist",  @"/System/Library/LaunchDaemons/com.saurik.Cydia.Startup.plist",  @"/private/var/tmp/cydia.log",  @"/private/var/lib/cydia", nil];for(NSString *string in jailbrokenPath)
 {
 if ([[NSFileManager defaultManager] fileExistsAtPath:string]){
 jailbroken = YES;
 break;}
 }
 return jailbroken;
 }
 */

-(void)showConnectionErrorAlert
{
    
    [hud hideAnimated:YES];
    
    
    if (connectionError == YES)
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"network_not_available_txt", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *alertActionConfirm = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                             {
                                                 [self backBtnAction:nil];
                                                 
                                             }];
        
        [alertController addAction:alertActionConfirm];
        
        [self presentViewController:alertController animated:YES completion:^{
        }];
    }
}


-(void)hitAPItoGetMessageHistory
{
    [newsSpinner startAnimating];
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *mapData = [NSMutableDictionary new];
    
    
    double CurrentTime = CACurrentMediaTime();
    
    
    self.requestTime = [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"chatRequestTime"];
    
    if ([self.requestTime length]==0||self.requestTime==nil||[self.requestTime isEqualToString:@""])
    {
        self.requestTime=@"";
        
        NSString * timeInMS = [NSString stringWithFormat:@"%f", CurrentTime];
        
        self.requestTime=  [timeInMS stringByReplacingOccurrencesOfString:@"." withString:@""];
        
    }
    
    
    
    
    [mapData setObject:self.requestTime forKey:@"requestId"];
    [mapData setObject:@"87de96a7be0f13e7" forKey:@"did"];
    [mapData setObject:@"" forKey:@"imei"];
    [mapData setObject:@"" forKey:@"imsi"];
    [mapData setObject:@"Apple" forKey:@"hmk"];
    [mapData setObject:[[UIDevice currentDevice] model] forKey:@"hmd"];
    [mapData setObject:@"ios" forKey:@"os"];
    [mapData setObject:singleton.apiMode.PACKAGE_NAME forKey:@"pkg"];
    [mapData setObject:@"1" forKey:@"ver"];
    [mapData setObject:singleton.user_tkn forKey:@"tkn"];
    [mapData setObject:[self isJailbroken]?@"yes":@"no" forKey:@"rot"];
    [mapData setObject:@"app" forKey:@"mod"];
    [mapData setObject:@"404" forKey:@"mcc"];
    [mapData setObject:@"02" forKey:@"mnc"];
    [mapData setObject:@"96" forKey:@"lac"];
    [mapData setObject:@"611346" forKey:@"clid"];
    [mapData setObject:@"" forKey:@"acc"];
    [mapData setObject:@"" forKey:@"lat"];
    [mapData setObject:@"" forKey:@"lon"];
    [mapData setObject:@"" forKey:@"peml"];
    [mapData setObject:@"en" forKey:@"lang"];
    [mapData setObject:@"" forKey:@"aadhr"];
    [mapData setObject:@"" forKey:@"mno"];
    [mapData setObject:@"" forKey:@"st"];
    [mapData setObject:@"" forKey:@"node"];
    [mapData setObject:singleton.mobileNumber forKey:@"msisdn"];
    
    [btnLoadingChat setTitle:@"" forState: UIControlStateNormal];
    [btnLoadingChat setTitle:@"" forState: UIControlStateSelected];
    NSLog(@"mapData=%@",mapData);
    
    [objRequest hitXMPPChatWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_CHAT_HISTORY withBody:mapData andTag:TAG_REQUEST_CHAT completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
     {
         [newsSpinner stopAnimating];
         newsSpinner.hidden = YES;
         
         if (error == nil) {
             
             @try {
                 
                 
                 if (![response isKindOfClass:[NSNull class]] )
                 {
                     NSLog(@"HISTORY -> %@",[response valueForKey:@"history"]);
                     
                     
                     historyMessagesArray = [NSMutableArray new];
                     
                     [[response valueForKey:@"history"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
                      {
                          
                          if ([obj valueForKey:@"user"] != nil)
                          {
                              [[obj valueForKey:@"user"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
                               {
                                   [historyMessagesArray addObject:obj];
                               }];
                          }
                          
                          if ([obj valueForKey:@"agent"] != nil)
                          {
                              
                              [[obj valueForKey:@"agent"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
                               {
                                   [historyMessagesArray addObject:obj];
                               }];
                              
                          }
                          
                          
                      }];
                     
                     
                     /* for (int i=0; i<[historyMessagesArray count]; i++)
                      {
                      [chatArray addObject:[historyMessagesArray objectAtIndex:i]];
                      }
                      NSLog(@"chatArray - %@",chatArray);
                      
                      */
                     
                     if ([response valueForKey:@"requestId"] != nil)
                     {
                         self.requestTime=[NSString stringWithFormat:@"%@",[response valueForKey:@"requestId"]];
                         [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                         [[NSUserDefaults standardUserDefaults] encryptValue:self.requestTime withKey:@"chatRequestTime"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                     }
                     
                     
                     
                     //NSLog(@"lastTime=%@ %lu",lastTime,(unsigned long)[lastTime length]);
                     
                     appDelegate.isHistoryLoaded=YES; //default set YES after load complete
                     [appDelegate.chatBotLoad removeAllObjects];
                     
                     
                     [self sortArray:historyMessagesArray];
                 }
                 
             } @catch (NSException *exception) {
                 
             } @finally {
                 
             }
             
         }
         else
         {
             [btnLoadingChat setTitle:NSLocalizedString(@"view_history", nil) forState: UIControlStateNormal];
             [btnLoadingChat setTitle:NSLocalizedString(@"view_history", nil) forState: UIControlStateSelected];
         }
         
         
         
     }];
}

//code to sort array in order to date order
-(void)sortArray:(NSArray*)histroyarr
{
    //[appDelegate.chatBotLoad removeAllObjects];
    
    NSMutableArray *chatContentArr=[histroyarr mutableCopy];
    
    /* [chatContentArr addObjectsFromArray:chatArray];
     
     
     NSArray *arrSortedItems = nil;
     arrSortedItems = [chatContentArr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
     
     NSString *ldate1 = [obj1 valueForKey:@"time"];
     
     
     NSString *ldate2 = [obj2 valueForKey:@"time"];
     
     
     NSLog(@"Date 1= %@ and Date2 = %@",ldate1,ldate2);
     NSDateFormatter *df = [[NSDateFormatter alloc] init];
     // [df setDateFormat:@"dd-MM-yyyy HH:mm"];
     
     [df setDateFormat:@"HH:mm:ss"];
     NSDate *d1 = [df dateFromString:ldate1];
     NSDate *d2 = [df dateFromString:ldate2];
     //2017-04-11 15:1
     df = nil;
     
     return [d1 compare: d2];
     //return [d2 compare: d1];
     
     }];
     
     
     */
    
    [currentMessages removeAllObjects];
    [chatArray removeAllObjects];
    [chatArray addObjectsFromArray:chatContentArr];
    // [_chatTable reloadData];
    [self loadHistroy:chatArray];
}

-(NSString*)unescapeHtmlCodes:(NSString*)input {
    
    NSRange rangeOfHTMLEntity = [input rangeOfString:@"&#"];
    if( NSNotFound == rangeOfHTMLEntity.location ) {
        return input;
    }
    
    
    NSMutableString* answer = [[NSMutableString alloc] init];
    
    NSScanner* scanner = [NSScanner scannerWithString:input];
    [scanner setCharactersToBeSkipped:nil]; // we want all white-space
    
    while( ![scanner isAtEnd] ) {
        
        NSString* fragment;
        [scanner scanUpToString:@"&#" intoString:&fragment];
        if( nil != fragment ) { // e.g. '&#38; B'
            [answer appendString:fragment];
        }
        
        if( ![scanner isAtEnd] ) { // implicitly we scanned to the next '&#'
            
            int scanLocation = (int)[scanner scanLocation];
            [scanner setScanLocation:scanLocation+2]; // skip over '&#'
            
            int htmlCode;
            if( [scanner scanInt:&htmlCode] ) {
                char c = htmlCode;
                [answer appendFormat:@"%c", c];
                
                scanLocation = (int)[scanner scanLocation];
                [scanner setScanLocation:scanLocation+1]; // skip over ';'
                
            } else {
                // err ?
            }
        }
        
    }
    
    return answer;
    
}

-(void)loadHistroy:(NSArray*)SortArray
{
    
    
    for (int i=0; i<[SortArray count]; i++)
    {
        
        iMessage *receiveMessage;
        NSString *agentId=[[SortArray objectAtIndex:i] valueForKey:@"agentId"];
        NSString *msg=[[SortArray objectAtIndex:i] valueForKey:@"msg"];
        
        msg = [self unescapeHtmlCodes:msg];
        
        NSString *time=[[SortArray objectAtIndex:i] valueForKey:@"time"];
        
        NSDateFormatter * df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"HH:mm:ss"];
        NSDate *gethistorydate = [df dateFromString:time]; // here you can fetch date from string with define format
        
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat: @"hh:mm a"];
        NSString *fetchDatetime = [dateFormatter
                                   stringFromDate:gethistorydate]; //Put whatever date you want
        
        if (agentId != nil)
        {
            receiveMessage = [[iMessage alloc] initIMessageWithName:@"" message:msg time:fetchDatetime type:@"other" ImgUrl:nil  msgcontainImage:nil];
        }
        else
        {
            receiveMessage = [[iMessage alloc] initIMessageWithName:@"" message:msg time:fetchDatetime type:@"self" ImgUrl:nil  msgcontainImage:nil];
        }
        
        //  [currentMessages insertObject:msg atIndex:currentMessages.count];
        [currentMessages addObject:receiveMessage];
    }
    
    
    [_chatTable reloadData];
    [self  scolltodown];
    
    _chatTextView.text = NSLocalizedString(@"type_your_message", nil);//
    
    _chatTextView.textColor = [UIColor lightGrayColor];
    
    
}
-(IBAction)backBtnAction:(id)sender
{
    // [self dismissViewControllerAnimated:NO completion:nil];
    //[self.navigationController popViewControllerAnimated:YES];
    if ([self.comingFromString isEqualToString:@"Help"] || [self.comingFromString isEqualToString:@"MoreHelp"])
    {
        [self.navigationController.tabBarController.tabBar setHidden:YES];
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else if ([self.comingFromString isEqualToString:@"Department"])
    {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    else
    {
        [self.navigationController.tabBarController.tabBar setHidden:NO];
        
        UITabBarController *tabBar = (UITabBarController *) self.navigationController.tabBarController;
        tabBar.selectedIndex = 0;
        tabBar.delegate = self;
        
        [self.navigationController.tabBarController.tabBar setHidden:NO];
    }
    
    
}

-(void)getXMPPConnectionStatus:(BOOL)status
{
    [hud hideAnimated:YES];
    
    if (status)
    {
        NSLog(@"User Logged in successfully");
        
        connectionError = NO;
        
        self.userJID = [XMPPJID jidWithString:@"bot@reporting.umang.gov.in"];
        
        
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"ChatSession"]);
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"ChatSession"] == nil || [[[NSUserDefaults standardUserDefaults] objectForKey:@"ChatSession"] isEqualToString:@"N"])
        {
            NSLog(@"%@",singleton.user_tkn);
            
            [[NSUserDefaults standardUserDefaults] setValue:@"Y" forKey:@"ChatSession"];
            
            [[[AppDelegate sharedDelegate]xmppHandler] sendMessage:[NSString stringWithFormat:@"initconversation~~~%@~~~abc",singleton.user_tkn] toAdress:@"bot@reporting.umang.gov.in" withType:@"chat"];
        }
        
        
        self.fetchedResultController = nil;
        sectionsArray = [[self fetchedResultsController] sections];
        
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"session_expire_msg", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction *alertActionConfirm = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                             
                                             {
                                                 [[[AppDelegate sharedDelegate]xmppHandler] sendMessage:[NSString stringWithFormat:@"userdisconnected~~~%@~~~exit",singleton.user_tkn] toAdress:@"bot@reporting.umang.gov.in" withType:@"chat"];
                                                 
                                                 
                                                 [self deleteChatHistoryAnother];
                                                 
                                                 
                                                 self.fetchedResultController = nil;
                                                 
                                                 [[[AppDelegate sharedDelegate] xmppHandler] teardownStream];
                                                 
                                                 
                                                 [[NSUserDefaults standardUserDefaults] setObject:@"N" forKey:@"ChatSession"];
                                                 [self performSelector:@selector(moveToViewAfterDeletingMessages) withObject:nil afterDelay:0.0];
                                                 
                                             }];
        
        [alertController addAction:alertActionConfirm];
        [self presentViewController:alertController animated:YES completion:^{
        }];
    }
    
}

-(void)getchatSessionExpiredWithStatus:(BOOL)status
{
    if (status == NO)
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"session_expire_msg", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *alertActionConfirm = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                             
                                             {
                                                 
                                                 //===========set time to blank=================
                                                 [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                                                 [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"chatRequestTime"];
                                                 [[NSUserDefaults standardUserDefaults] synchronize];
                                                 
                                                 
                                                 [appDelegate.chatHistoryLoad  removeAllObjects];
                                                 [appDelegate.chatBotLoad  removeAllObjects];
                                                 [currentMessages removeAllObjects];
                                                 appDelegate.isHistoryLoaded=NO;
                                                 //===========set time to blank=================
                                                 
                                                 
                                                 [[[AppDelegate sharedDelegate]xmppHandler] sendMessage:[NSString stringWithFormat:@"userdisconnected~~~%@~~~exit",singleton.user_tkn] toAdress:@"bot@reporting.umang.gov.in" withType:@"chat"];
                                                 
                                                 
                                                 //[self deleteChatHistoryAnother];
                                                 
                                                 [self performSelector:@selector(moveToViewAfterDeletingMessages) withObject:nil afterDelay:1.0];
                                                 
                                                 
                                             }];
        
        [alertController addAction:alertActionConfirm];
        
        
        if ([self.comingFromString isEqualToString:@"Department"])
        {
            id rootViewController = [UIApplication sharedApplication].delegate.window.rootViewController;
            
            if([rootViewController isKindOfClass:[UITabBarController class]])
            {
                [self presentViewController:alertController animated:YES completion:^{
                }];
            }
            
        }
        else
        {
            id rootViewController = self.navigationController.topViewController;
            
            if ([rootViewController isKindOfClass:[LiveChatVC class]])
            {
                [rootViewController presentViewController:alertController animated:YES completion:nil];
            }
        }
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark NSFetchedResultsController
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSFetchedResultsController *)fetchedResultsController
{
    //messageStr
    if (self.fetchedResultController == nil)
    {
        XMPPMessageArchivingCoreDataStorage *storage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
        NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject"
                                                             inManagedObjectContext:moc];
        
        NSSortDescriptor *sd1 = [[NSSortDescriptor alloc] initWithKey:@"timestamp" ascending:YES];
        //NSSortDescriptor *sd2 = [[NSSortDescriptor alloc] initWithKey:@"displayName" ascending:YES];
        
        NSArray *sortDescriptors = [NSArray arrayWithObjects:sd1, nil];
        
        NSFetchRequest *request = [[NSFetchRequest alloc]init];
        XMPPJID *Jid= [XMPPJID jidWithString:[[NSUserDefaults standardUserDefaults] objectForKey:@"XXMPPmyJID"]];//[[[AppDelegate sharedDelegate]xmppHandler]getMyJid];
        //@"bot@itx1spip-momt1"
        
        self.userJID = [XMPPJID jidWithString:@"bot@reporting.umang.gov.in"];
        
        NSString *bareJid = [Jid bare];
        
        NSString *predicateFrmt = @"bareJidStr like %@ AND streamBareJidStr == %@ AND NOT(messageStr CONTAINS[c] %@) AND NOT(messageStr CONTAINS[c] %@)";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, [self.userJID full],bareJid,@"initconversation",@"userdisconnected"];
        request.predicate = predicate;
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults] stringForKey:@"kXMPPmyJID"]);
        [request setEntity:entityDescription];
        
        [request setSortDescriptors:sortDescriptors];
        //[request setFetchBatchSize:10];
        
        self.fetchedResultController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                           managedObjectContext:moc
                                                                             sectionNameKeyPath:nil
                                                                                      cacheName:nil];
        [self.fetchedResultController setDelegate:self];
        NSLog(@"data is %@",self.fetchedResultController);
        
        NSError *error = nil;
        if (![self.fetchedResultController performFetch:&error])
        {
            //DDLogError(@"Error performing fetch: %@", error);
        }
        
    }
    
    return self.fetchedResultController;
}


-(void)deleteChatHistoryAnother
{
    //[self.userJID full],bareJid
    
    
    XMPPJID *Jid= [XMPPJID jidWithString:[[NSUserDefaults standardUserDefaults] objectForKey:@"XXMPPmyJID"]];
    
    
    self.userJID = [XMPPJID jidWithString:@"bot@reporting.umang.gov.in"];
    
    NSString *bareJid = [Jid bare];
    
    NSFetchRequest *messagesCoreD = [[NSFetchRequest alloc] init];
    XMPPMessageArchivingCoreDataStorage *storage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    NSManagedObjectContext *context = [storage mainThreadManagedObjectContext];
    NSEntityDescription *messageEntity = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Contact_CoreDataObject" inManagedObjectContext:context];
    [messagesCoreD setEntity:messageEntity];
    [messagesCoreD setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    NSString *predicateFrmt = @"bareJidStr == %@";
    NSString *predicateFrmt1 = @"streamBareJidStr == %@";
    NSPredicate *predicateName = [NSPredicate predicateWithFormat:predicateFrmt,[self.userJID full]];
    NSPredicate *predicateSSID = [NSPredicate predicateWithFormat:predicateFrmt1,bareJid];
    
    NSArray *subPredicates = [NSArray arrayWithObjects:predicateName, predicateSSID, nil];
    
    NSPredicate *orPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:subPredicates];
    
    messagesCoreD.predicate = orPredicate;
    NSError * error = nil;
    NSArray * messages = [context executeFetchRequest:messagesCoreD error:&error];
    //error handling goes here
    for (NSManagedObject * message in messages)
    {
        [context deleteObject:message];
    }
    NSEntityDescription *messageEntity1 = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:context];
    [messagesCoreD setEntity:messageEntity1];
    [messagesCoreD setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    messagesCoreD.predicate = orPredicate;
    NSArray * messages1 = [context executeFetchRequest:messagesCoreD error:&error];
    //error handling goes here
    for (NSManagedObject * message in messages1)
    {
        [context deleteObject:message];
    }
    
    NSError *saveError = nil;
    [context save:&saveError];
    
    [xmppMessagesArray removeAllObjects];
    [historyMessagesArray removeAllObjects];
    [chatArray removeAllObjects];
    
    // [tblchat reloadData];
}


-(void)sendMessageAfterDelay:(NSString *)message
{
    self.chatTextView.text=@"";
    [[[AppDelegate sharedDelegate]xmppHandler] sendMessage:message toAdress:@"bot@reporting.umang.gov.in" withType:@"chat"];
}

-(void)sendImageMessage:(UIImage *)messageImage
{
    self.chatTextView.text=@"";
    [[[AppDelegate sharedDelegate]xmppHandler] sendImageMessage:messageImage toAdress:@"bot@reporting.umang.gov.in" withType:@"chat"];
}

-(void)viewDidDisappear:(BOOL)animated
{
    
    [super viewDidDisappear:YES];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"Time_Stamp"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"Updated Saved time stamp %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"Time_Stamp"]);
    
    
    
    if ([self.comingFromString isEqualToString:@"Help"])
    {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    }
    
    else if ([self.comingFromString isEqualToString:@"Department"])
    {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    }
    else if ([self.comingFromString isEqualToString:@"MoreHelp"])
    {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    }
    else
    {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    }
    
    
    if (isOpenImage == FALSE)
    {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    }
    
}


- (IBAction)exitButtonPressed:(UIButton *)sender
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"exit_chat_session", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *alertActionConfirm = [UIAlertAction actionWithTitle:[NSLocalizedString(@"yes", nil) capitalizedString] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                         
                                         {
                                             /*   if (loadEarlierMessagesClicked == YES)
                                              {
                                              loadEarlierMessagesClicked = NO;
                                              
                                              sectionsArray = [[self fetchedResultsController] sections];
                                              [_chatTable reloadData];
                                              }
                                              */
                                             
                                             [currentMessages removeAllObjects];
                                             [appDelegate.chatBotLoad  removeAllObjects];
                                             [appDelegate.chatHistoryLoad  removeAllObjects];
                                             appDelegate.isHistoryLoaded=NO;
                                             
                                             
                                             //===========set time to blank=================
                                             [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                                             [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"chatRequestTime"];
                                             [[NSUserDefaults standardUserDefaults] synchronize];
                                             
                                             
                                             
                                             //===========set time to blank=================
                                             
                                             
                                             
                                             
                                             
                                             [[[AppDelegate sharedDelegate]xmppHandler] sendMessage:[NSString stringWithFormat:@"userdisconnected~~~%@~~~exit",singleton.user_tkn] toAdress:@"bot@reporting.umang.gov.in" withType:@"chat"];
                                             
                                             
                                             //[self deleteChatHistoryAnother];
                                             
                                             
                                             
                                             
                                             [self performSelector:@selector(moveToViewAfterDeletingMessages) withObject:nil afterDelay:1.0];
                                             
                                             
                                         }];
    UIAlertAction *alertActionCancel = [UIAlertAction actionWithTitle:[NSLocalizedString(@"no", nil) capitalizedString]  style:UIAlertActionStyleCancel handler:nil];
    
    [alertController addAction:alertActionConfirm];
    [alertController addAction:alertActionCancel];
    [self presentViewController:alertController animated:YES completion:^{
    }];
    
    
    
}

-(void)moveToViewAfterDeletingMessages
{
    self.fetchedResultController = nil;
    
    [[[AppDelegate sharedDelegate] xmppHandler] teardownStream];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"N" forKey:@"ChatSession"];
    
    [self backBtnAction:nil];
    
}


-(void)imageMessageCellUpdatesWithData:(NSData *)imageData
{
    
    NSDate *today = [NSDate date];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"hh:mm a"]; // 2017-09-28
    NSString *msg_time = [df stringFromDate:today];
    
    NSMutableDictionary *dic=[NSMutableDictionary new];
    
    iMessage *receiveMessage;
    
    
    //NSString *sendImageURL=[NSString stringWithFormat:@"https://web.umang.gov.in/web/resources/images/Digital_india_logo.png"];
    
    [dic setObject:@"" forKey:@"msg"];
    [dic setObject:msg_time forKey:@"time"];
    
    
    if (imageData != nil)
    {
        receiveMessage = [[iMessage alloc] initIMessageWithName:@"" message:@"" time:msg_time type:@"self" ImgUrl:nil  msgcontainImage:imageData];
        
        [dic setObject:imageData forKey:@"containImage"];
    }
    else
    {
        receiveMessage = [[iMessage alloc] initIMessageWithName:@"" message:@"" time:msg_time type:@"self" ImgUrl:nil  msgcontainImage:nil];
        
    }
    
    [appDelegate.chatBotLoad addObject:dic];
    [self updateTableView:receiveMessage];
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    NSArray*changewillArray = [[self fetchedResultsController] sections];
    NSLog(@"changewillArray=%@",changewillArray);
    
    
    // [_chatTable beginUpdates];
}

-(void)getNewMessage:(AppDelegate *)appD Message:(XMPPMessage *)message;
{
    NSLog(@"message=%@",message);
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    // UITableView *tableView = _chatTable;
    UITableView *tableView = self.chatTable;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
        {
            XMPPMessageArchiving_Message_CoreDataObject *user = [[self fetchedResultsController] objectAtIndexPath:newIndexPath];
            
            if(user)
            {
                NSLog(@"body=%@",user.body);
                NSLog(@"outgoing=%@",user.outgoing);
                NSLog(@"timestamp=%@",user.timestamp);
                
                NSString *msgtxt=[NSString stringWithFormat:@"%@",user.body];
                @try {
                    msgtxt = [[msgtxt componentsSeparatedByString:@"~~~"] objectAtIndex:0];
                    msgtxt = [self unescapeHtmlCodes:msgtxt];
                    
                    
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                NSString *msgStatus=[NSString stringWithFormat:@"%@",user.outgoing];
                NSString *msg_time=[NSString stringWithFormat:@"%@",user.timestamp];
                // 2017-06-01 09:20:15 +0000
                NSDateFormatter * df = [[NSDateFormatter alloc] init];
                [df setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
                NSDate *getdate = [df dateFromString:msg_time]; // here you can fetch date from string with define format
                NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat: @"hh:mm a"];
                NSString *fetchtime = [dateFormatter
                                       stringFromDate:getdate]; //Put whatever date you want
                NSMutableDictionary *dic=[NSMutableDictionary new];
                iMessage *receiveMessage;
                
                
                //NSString *sendImageURL=[NSString stringWithFormat:@"https://web.umang.gov.in/web/resources/images/Digital_india_logo.png"];
                
                if ([msgStatus isEqualToString:@"0"])//bot
                {
                    [dic setObject:@"agent" forKey:@"agentId"];
                    [dic setObject:msgtxt forKey:@"msg"];
                    [dic setObject:fetchtime forKey:@"time"];
                    //[dic setObject:@"https://web.umang.gov.in/web/resources/images/Digital_india_logo.png" forKey:@"containImage"];
                    
                    
                    receiveMessage = [[iMessage alloc] initIMessageWithName:@"" message:msgtxt time:fetchtime type:@"other" ImgUrl:nil  msgcontainImage:nil];
                    
                    [appDelegate.chatBotLoad addObject:dic];
                    
                    [self updateTableView:receiveMessage];
                }
                else
                {
                    [dic setObject:msgtxt forKey:@"msg"];
                    [dic setObject:fetchtime forKey:@"time"];
                    
                    
                    if (self.theImage!= nil)
                    {
                        receiveMessage = [[iMessage alloc] initIMessageWithName:@"" message:msgtxt time:fetchtime type:@"self" ImgUrl:nil  msgcontainImage:self.theImage];
                        
                        [dic setObject:self.theImage forKey:@"containImage"];
                        
                    }
                    else
                    {
                        receiveMessage = [[iMessage alloc] initIMessageWithName:@"" message:msgtxt time:fetchtime type:@"self" ImgUrl:nil  msgcontainImage:nil];
                        
                    }
                    
                    [appDelegate.chatBotLoad addObject:dic];
                    [self updateTableView:receiveMessage];
                }
                
                NSLog(@"befor appDelegate.chatBotLoad=%@",appDelegate.chatBotLoad);
                
                NSLog(@"after appDelegate.chatBotLoad=%@",appDelegate.chatBotLoad);
                
                
            }
            
            /*  if (loadEarlierMessagesClicked == NO)
             {
             [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
             }*/
        }
            break;
            
        case NSFetchedResultsChangeDelete:
            //  [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            
            // [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            
            if(![indexPath isEqual:newIndexPath]){
                [tableView moveRowAtIndexPath:indexPath toIndexPath:newIndexPath];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [tableView reloadRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                });
            }else{
                [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
            
            
            
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [_chatTable insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [_chatTable deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    
    NSArray*changeArray = [[self fetchedResultsController] sections];
    NSLog(@"changeArray=%@",changeArray);
    
    // [_chatTable endUpdates];
}


/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/

@end

