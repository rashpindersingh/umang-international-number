//
//  VerifyOTPNewMbVC.h
//  Umang
//
//  Created by admin on 09/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerifyOTPNewMbVC : UIViewController<UIScrollViewDelegate>
{
    IBOutlet UIView *resendOTPview;
    IBOutlet UIScrollView *scrollView;
    
}


@property(nonatomic,retain)NSString *mpinEncrypt;

@property(nonatomic,retain)NSString *newmobile;
@property(nonatomic,retain)NSString *rtry;
@property(assign)int tout;
@property(nonatomic,retain)NSString *TAGFROM;

//--- for handling all resend and call option------


@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@property(nonatomic,retain)IBOutlet UIScrollView *scrollView;

@property(strong,nonatomic) NSString *strAadharNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblScreenTitleName;
@end
