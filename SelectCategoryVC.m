//
//  SelectCategoryVC.m
//  Umang
//
//  Created by admin on 19/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "SelectCategoryVC.h"
#import "NotificationItemBO.h"
#import "UIImageView+WebCache.h"
#import "FilterServicesBO.h"

@interface SelectCategoryVC ()<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource>
{
    SharedManager *singleton;
    __weak IBOutlet UIButton *btnSelectCategoryBack;
    UISearchBar *_searchBar;
    NSMutableArray *arrSearchContent;
    NSMutableArray *arrSelectedItem;
}
@end
@implementation SelectCategoryVC
@synthesize arrPreselectedItem;
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    singleton = [SharedManager sharedSingleton];
    _tblSelectCategory.delegate = self;
    _tblSelectCategory.dataSource = self;
    arrSelectedItem = [[NSMutableArray alloc]init];
    [btnSelectCategoryBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnSelectCategoryBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnSelectCategoryBack.frame.origin.x, btnSelectCategoryBack.frame.origin.y, btnSelectCategoryBack.frame.size.width, btnSelectCategoryBack.frame.size.height);
        
        [btnSelectCategoryBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnSelectCategoryBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    if([arrPreselectedItem count]>0)
    {
        arrSelectedItem=[arrPreselectedItem mutableCopy];
    }
    arrSearchContent = [NSMutableArray new];
    
    [arrSearchContent addObjectsFromArray:self.arrTableContent];
    
    _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 64, CGRectGetWidth(self.view.frame), 44)];
    _searchBar.delegate = self;
    _searchBar.placeholder = NSLocalizedString(@"type_to_search",@"");
    [_searchBar setValue:NSLocalizedString(@"cancel", @"") forKey:@"_cancelButtonText"];
    [self.view addSubview:_searchBar];
    
    if (self.isForCategory)
    {
        self.headerTitle.text = NSLocalizedString(@"filter_categories", nil);
        //  [headerTitle setTitle:NSLocalizedString(@"select_category", nil) forState:UIControlStateNormal];
    }
    else
    {
        self.headerTitle.text = NSLocalizedString(@"filter_services", nil);
    }
    
    
    [_tblSelectCategory reloadData];
    
    
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setViewFont];
}

#pragma mark- Font Set to View
-(void)setViewFont{
    [btnSelectCategoryBack.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    _headerTitle.font = [AppFont semiBoldFont:17];
    
    
    
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}
- (IBAction)btnBackAction:(id)sender
{
    
    [singleton traceEvents:@"Back Button" withAction:@"Clicked" withLabel:@"Transactional Select Category" andValue:0];

    [self.navigationController popViewControllerAnimated:YES];
    
}
#pragma mark- Search Bar Delegates
#pragma mark-

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
// return NO to not become first responder
{
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (self.isForCategory) {
        [self filteredContentWithSearchTextForServices:searchText];
    }
    else{
        [self filteredContentWithSearchTextForCategory:searchText];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    if (self.isForCategory) {
        
        [singleton traceEvents:@"Search Service Button" withAction:@"Clicked" withLabel:@"Transactional Select Category" andValue:0];

        [self filteredContentWithSearchTextForServices:searchBar.text];
    }
    else{
        [singleton traceEvents:@"Search Category Button" withAction:@"Clicked" withLabel:@"Transactional Select Category" andValue:0];

        [self filteredContentWithSearchTextForCategory:searchBar.text];
    }
}

-(void)filteredContentWithSearchTextForCategory:(NSString*)searchText
{
    if([searchText length])
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.titleService contains[c] %@",searchText];
        NSArray *tempArray = [self.arrTableContent filteredArrayUsingPredicate:predicate];
        
        [arrSearchContent removeAllObjects];
        [arrSearchContent addObjectsFromArray:tempArray];
    }
    else{
        [arrSearchContent removeAllObjects];
        [arrSearchContent addObjectsFromArray:self.arrTableContent];
    }
    
    [_tblSelectCategory reloadData];
}

-(void)filteredContentWithSearchTextForServices:(NSString*)searchText
{
    if([searchText length])
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.serviceName contains[c] %@",searchText];
        NSArray *tempArray = [self.arrTableContent filteredArrayUsingPredicate:predicate];
        
        [arrSearchContent removeAllObjects];
        [arrSearchContent addObjectsFromArray:tempArray];
    }
    else{
        [arrSearchContent removeAllObjects];
        [arrSearchContent addObjectsFromArray:self.arrTableContent];
    }
    
    [_tblSelectCategory reloadData];
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [singleton traceEvents:@"Search Cancel Button" withAction:@"Clicked" withLabel:@"Transactional Select Category" andValue:0];

    [searchBar setShowsCancelButton:NO animated:YES];
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    if (self.isForCategory) {
        [self filteredContentWithSearchTextForCategory:searchBar.text];
    }
    else{
        [self filteredContentWithSearchTextForServices:searchBar.text];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegates

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.isForCategory)
    {
        // Remove seperator inset
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsMake(0, 50, 0, 0)];
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
            [cell setPreservesSuperviewLayoutMargins:NO];
        }
        
        // Explictly set your cell's layout margins
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 50, 0, 0)];
        }
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.001;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return nil;
}// custom view for header. will be adjusted to default or specified header height
- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return nil;
}// custom view for footer. will be adjusted to default or specified footer height



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 50.0;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return arrSearchContent.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        tableView.separatorColor = [UIColor lightGrayColor];
    }
    
    
    
    id objTemp = arrSearchContent[indexPath.row];
    if ([objTemp isKindOfClass:[NotificationItemBO class]])
    {
        // This is for service
        NotificationItemBO *objNoti = (NotificationItemBO*)objTemp;
        for(UIView *view in cell.contentView.subviews){
            if ([view isKindOfClass:[UILabel class]]) {
                [view removeFromSuperview];
            }
            if ([view isKindOfClass:[UIImageView class]]) {
                [view removeFromSuperview];
            }
        }
        
        UILabel *lbl =[[UILabel alloc]initWithFrame:CGRectMake(48, 7,cell.contentView.frame.size.width, 30)];
        lbl.text = objNoti.titleService;
        // lbl.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
        UIImageView *img= [[UIImageView alloc]initWithFrame:CGRectMake(10,7, 30, 30)];
        
        
        [cell.contentView addSubview:lbl];
        [cell.contentView addSubview:img];
        
        if ([objNoti.imgService length]!=0 ) {
            NSURL *url = [NSURL URLWithString:objNoti.imgService];
            [img sd_setImageWithURL:url
                   placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
            //cell.imageView.wid
        }
        
        if (singleton.isArabicSelected==TRUE)
        {
            cell.transform = CGAffineTransformMakeScale(-1.0, 1.0);
            img.transform= CGAffineTransformMakeScale(-1.0, 1.0);
            lbl.transform= CGAffineTransformMakeScale(-1.0, 1.0);
            lbl.textAlignment=NSTextAlignmentRight;
        }
        lbl.font = [AppFont regularFont:14];

    }
    else if ([objTemp isKindOfClass:[FilterServicesBO class]])
    {
        
        FilterServicesBO *objNoti = (FilterServicesBO*)objTemp;
        cell.textLabel.text = objNoti.serviceName;
        if (singleton.isArabicSelected==TRUE)
        {
            cell.transform = CGAffineTransformMakeScale(-1.0, 1.0);
            cell.textLabel.transform= CGAffineTransformMakeScale(-1.0, 1.0);
            cell.textLabel.textAlignment=NSTextAlignmentRight;
        }
        
        
    }
    
    
    
    if ([arrSelectedItem containsObject:objTemp])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        
    }
    else{
        cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    if (singleton.isArabicSelected==TRUE)
    {
        UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,18,18)];
        CGAffineTransform rotationTransform = CGAffineTransformIdentity;
        rotationTransform = CGAffineTransformMakeScale(-1.0, 1.0);
        iv.transform = rotationTransform;
        
        if ([arrSelectedItem containsObject:objTemp])
        {
            //cell.accessoryType = UITableViewCellAccessoryCheckmark;
            iv.image = [UIImage imageNamed:@"img_feedbackcheck"];
            cell.accessoryView = iv;
        }
        else
        {
            iv.image =nil;
            cell.accessoryView = iv;
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    cell.textLabel.font = [AppFont regularFont:17];

    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    id obj = arrSearchContent[indexPath.row];
    // cell.accessoryType = UITableViewCellAccessoryCheckmark;
    if ([arrSelectedItem containsObject:obj])
    {
        [arrSelectedItem removeObject:obj];
    }
    else
    {
        [arrSelectedItem addObject:obj];
    }
    
    if (self.callBack != nil && [self.callBack respondsToSelector:@selector(updateFilterValue:)])
    {
        [self.callBack performSelectorOnMainThread:@selector(updateFilterValue:) withObject:arrSelectedItem waitUntilDone:YES];
    }
    
    [tableView reloadData];
    //   [self.navigationController popViewControllerAnimated:YES];
}


-(void)updateFilterValue:(id)obj
{
    
    
}


#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*
 #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }
 */

@end
