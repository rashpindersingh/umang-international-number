//
//  UpdMpinVC.m
//  Umang
//
//  Created by deepak singh rawat on 28/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "UpdMpinVC.h"

#define kOFFSET_FOR_KEYBOARD 80.0
#import "JKLLockScreenViewController.h"
#import "MyTextField.h"
#import "MBProgressHUD.h"
#import "UMAPIManager.h"
//#import "LoginViewController.h"
#import "LoginAppVC.h"
#import "ShowUserProfileVC.h"

#import "UserProfileVC.h"
#import "SocialAuthentication.h"
#import "SocialMediaViewController.h"

#import "AadharCardViewCon.h"
#import "MoreTabVC.h"
#import "SecuritySettingVC.h"

@interface UpdMpinVC ()<MyTextFieldDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate,JKLLockScreenViewControllerDataSource, JKLLockScreenViewControllerDelegate>
{
    MBProgressHUD *hud ;
    SharedManager *singleton;
    IBOutlet UIView *vw_base;
    NSString *typeString;
    JKLLockScreenViewController *viewController;
}
@property (nonatomic, strong) NSString * enteredPincode;

@property (weak, nonatomic) IBOutlet UITextField *txt_pin1;
@property (weak, nonatomic) IBOutlet UITextField *txt_pin2;
@property (weak, nonatomic) IBOutlet UITextField *txt_pin3;
@property (weak, nonatomic) IBOutlet UITextField *txt_pin4;
@property (weak, nonatomic) IBOutlet UIView *vw_txtcontain;

@end

@implementation UpdMpinVC
@synthesize dic_info;
@synthesize user_img;
@synthesize socialLinkType;
@synthesize TAG_FROM;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField setFont:[UIFont systemFontOfSize:24]];
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField.textAlignment = NSTextAlignmentCenter;
    
    [self setFontforView:self.view andSubViews:YES];
    [textField resignFirstResponder];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField setFont:[UIFont systemFontOfSize:24]];
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField.textAlignment = NSTextAlignmentCenter;
    
    [textField resignFirstResponder];
    
}


-(void)setFontforView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       if ([view isKindOfClass:[UITextField class]])
                       {
                           
                           UITextField *txtfield = (UITextField *)view;
                           NSString *fonttxtFieldName = txtfield.font.fontName;
                           CGFloat fonttxtsize =txtfield.font.pointSize;
                           txtfield.font = nil;
                           
                           txtfield.font = [UIFont fontWithName:fonttxtFieldName size:fonttxtsize];
                           
                           [txtfield layoutIfNeeded]; //Fixes iOS 9 text bounce glitch
                       }
                       
                       
                   });
    
    if ([view isKindOfClass:[UITextView class]])
    {
        
        UITextView *txtview = (UITextView *)view;
        NSString *fonttxtviewName = txtview.font.fontName;
        CGFloat fontbtnsize =txtview.font.pointSize;
        
        txtview.font = [UIFont fontWithName:fonttxtviewName size:fontbtnsize];
        
    }
    
    
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        NSString *fontName = lbl.font.fontName;
        CGFloat fontSize = lbl.font.pointSize;
        
        lbl.font = [UIFont fontWithName:fontName size:fontSize];
    }
    
    
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *button = (UIButton *)view;
        NSString *fontbtnName = button.titleLabel.font.fontName;
        CGFloat fontbtnsize = button.titleLabel.font.pointSize;
        
        [button.titleLabel setFont: [UIFont fontWithName:fontbtnName size:fontbtnsize]];
    }
    
    if (isSubViews)
    {
        
        for (UIView *sview in view.subviews)
        {
            [self setFontforView:sview andSubViews:YES];
        }
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self setFontforView:self.view andSubViews:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self callMpinVCWithType:0];
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: UMANG_UPD_MPIN_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    // self.view.hidden = true ;
    singleton = [SharedManager sharedSingleton];
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    
    
    
    
    NSLog(@"allviewControllers=%@",allViewControllers);
    
    //_txt_pin1.userInteractionEnabled=NO;
    //_txt_pin2.userInteractionEnabled=NO;
    //_txt_pin3.userInteractionEnabled=NO;
    //_txt_pin4.userInteractionEnabled=NO;
    
    
    /*
     [_txt_pin1.layer setCornerRadius:CGRectGetWidth(_txt_pin1.frame)/2];
     [_txt_pin1.layer setMasksToBounds:YES];
     _txt_pin1.layer.borderWidth=0.5;
     _txt_pin1.layer.borderColor=[UIColor greenColor].CGColor;
     
     
     [_txt_pin2.layer setCornerRadius:CGRectGetWidth(_txt_pin2.frame)/2];
     [_txt_pin2.layer setMasksToBounds:YES];
     _txt_pin2.layer.borderWidth=0.5;
     _txt_pin2.layer.borderColor=[UIColor greenColor].CGColor;
     
     
     [_txt_pin3.layer setCornerRadius:CGRectGetWidth(_txt_pin3.frame)/2];
     [_txt_pin3.layer setMasksToBounds:YES];
     _txt_pin3.layer.borderWidth=0.5;
     _txt_pin3.layer.borderColor=[UIColor greenColor].CGColor;
     
     
     [_txt_pin4.layer setCornerRadius:CGRectGetWidth(_txt_pin4.frame)/2];
     [_txt_pin4.layer setMasksToBounds:YES];
     _txt_pin4.layer.borderWidth=0.5;
     _txt_pin4.layer.borderColor=[UIColor greenColor].CGColor;
     */
    _txt_pin1.delegate=self;
    _txt_pin2.delegate=self;
    _txt_pin3.delegate=self;
    _txt_pin4.delegate=self;
    
    
    
    typeString=@"";
    
    // self.btn_next.enabled=NO;
    self.view.userInteractionEnabled = YES;
    
    self.view.backgroundColor = [UIColor whiteColor] ;
    
    [self addShadowToTheView:self.view];
    [self addShadowToTheView:vw_base];
    
    
    /* if (!UIAccessibilityIsReduceTransparencyEnabled()) {
     self.view.backgroundColor = [UIColor clearColor];
     
     UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
     UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
     blurEffectView.frame = self.view.bounds;
     blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
     
     [self.view addSubview:blurEffectView];
     } else {
     self.view.backgroundColor = [UIColor grayColor];
     }
     */
    
    // self.view.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.8];
    [_txt_pin1 setFont:[UIFont systemFontOfSize:24]];
    [_txt_pin2 setFont:[UIFont systemFontOfSize:24]];
    [_txt_pin3 setFont:[UIFont systemFontOfSize:24]];
    [_txt_pin4 setFont:[UIFont systemFontOfSize:24]];
    //  [[UITextField appearance] setFont:[UIFont systemFontOfSize:24]];
    /*
     _txt_pin1.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
     _txt_pin1.textAlignment = NSTextAlignmentCenter;
     
     _txt_pin2.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
     _txt_pin2.textAlignment = NSTextAlignmentCenter;
     
     _txt_pin3.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
     _txt_pin3.textAlignment = NSTextAlignmentCenter;
     
     _txt_pin4.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
     _txt_pin4.textAlignment = NSTextAlignmentCenter;
     
     */
    
    __weak id weakSelf = self;
    
    
    //    self.enterMpinLabel.text = NSLocalizedString(@"enter_mpin_verify", nil);
    //    [self.cancelButton setTitle:NSLocalizedString(@"cancel", nil) forState:UIControlStateNormal];
    //    [self.deleteButton setTitle:NSLocalizedString(@"delete", nil) forState:UIControlStateNormal];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    
}
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}


-(void)addShadowToTheView:(UIView*)vwItem
{
    vwItem.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    vwItem.layer.shadowColor = [UIColor brownColor].CGColor;
    vwItem.layer.shadowRadius = 3;
    vwItem.layer.shadowOpacity = 0.5;
    vwItem.layer.cornerRadius = 3.0;
}


-(IBAction)keyPress:(UIButton *)sender
{
    int tag=(int)[sender tag];
    
    
    
    
    
    
    if ([typeString length]<4 || tag==111)
    {
        
        
        switch (tag) {
            case 101:
            {
                typeString=[NSString stringWithFormat:@"%@1",typeString];
                
            }
                break;
            case 102:
            {
                typeString=[NSString stringWithFormat:@"%@2",typeString];
                
            }
                break;
            case 103:
            {
                typeString=[NSString stringWithFormat:@"%@3",typeString];
                
            }
                break;
            case 104:
            {
                typeString=[NSString stringWithFormat:@"%@4",typeString];
                
            }
                break;
            case 105:
            {
                typeString=[NSString stringWithFormat:@"%@5",typeString];
                
            }
                break;
            case 106:
            {
                typeString=[NSString stringWithFormat:@"%@6",typeString];
                
            }
                break;
            case 107:
            {
                typeString=[NSString stringWithFormat:@"%@7",typeString];
                
            }
                break;
            case 108:
            {
                typeString=[NSString stringWithFormat:@"%@8",typeString];
                
            }
                break;
            case 109:
            {
                typeString=[NSString stringWithFormat:@"%@9",typeString];
                
            }
                break;
            case 110:
            {
                typeString=[NSString stringWithFormat:@"%@0",typeString];
                
            }
                break;
            case 111:
            {
                [self deleteDigit];
            }
                break;
                
                
            default:
                break;
        }
        
        
        //NSLog(@"value =%@",typeString);
        [self setNumber:typeString];
    }
    else
    {
        
        //  [self shake:_vw_txtcontain];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (!textField.secureTextEntry) {
        return YES;
    }
    
    textField.text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (textField.text.length > 0) {
        textField.font = [UIFont systemFontOfSize:24.0f];
    } else {
        textField.font = [UIFont systemFontOfSize:24.0f];
    }
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField.textAlignment = NSTextAlignmentCenter;
    
    return NO;
}

-(void)setNumber:(NSString*)typString
{
    
    
    
    int length=(int)[typeString length];
    
    switch (length) {
        case 1:
        {
            [_txt_pin1 becomeFirstResponder];
            _txt_pin1.text = [NSString stringWithFormat:@"%c",[typeString characterAtIndex:0]];
            [_txt_pin1 setFont:[UIFont systemFontOfSize:24]];
            [_txt_pin1 resignFirstResponder];
        }
            break;
        case 2:
        {
            [_txt_pin2 becomeFirstResponder];
            _txt_pin2.text = [NSString stringWithFormat:@"%c",[typeString characterAtIndex:1]];
            [_txt_pin2 setFont:[UIFont systemFontOfSize:24]];
            [_txt_pin2 resignFirstResponder];
        }
            break;
            
        case 3:
        {
            [_txt_pin3 becomeFirstResponder];
            _txt_pin3.text = [NSString stringWithFormat:@"%c",[typeString characterAtIndex:2]];
            [_txt_pin3 setFont:[UIFont systemFontOfSize:24]];
            [_txt_pin3 resignFirstResponder];
        }
            break;
            
        case 4:
        {
            [_txt_pin4 becomeFirstResponder];
            _txt_pin4.text = [NSString stringWithFormat:@"%c",[typeString characterAtIndex:3]];
            [_txt_pin4 setFont:[UIFont systemFontOfSize:24]];
            [_txt_pin4 resignFirstResponder];
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               [self checkValidation];
                           });
        }
            break;
        default:
            break;
    }
    
    [self deleteback:length];
    
    
}


-(void)shake:(UIView*)view
{
    typeString=@"";
    if (viewController != nil) {
        NSUInteger x = 0;
        for (x = 0; x<4; x++) {
            [viewController.pincodeView removeLastPincode];
        }
    }
    //    _txt_pin1.text=@"";
    //    _txt_pin2.text=@"";
    //    _txt_pin3.text=@"";
    //    _txt_pin4.text=@"";
    
    view.transform = CGAffineTransformMakeTranslation(20, 0);
    [UIView animateWithDuration:0.4 delay:0.0 usingSpringWithDamping:0.2 initialSpringVelocity:1.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        view.transform = CGAffineTransformIdentity;
        
        
    } completion:nil];
}

-(void)deleteback:(int)length
{
    if (length==0)
    {
        _txt_pin1.text=@"";
        _txt_pin2.text=@"";
        _txt_pin3.text=@"";
        _txt_pin4.text=@"";
    }
    if (length==1)
    {
        _txt_pin2.text=@"";
        _txt_pin3.text=@"";
        _txt_pin4.text=@"";
    }
    if (length==2)
    {
        _txt_pin3.text=@"";
        _txt_pin4.text=@"";
    }
    if (length==3)
    {
        _txt_pin4.text=@"";
    }
    
}



- (void)deleteDigit
{
    if ([typeString length]<=4 &&[typeString length]>0)
    {
        NSString * current = typeString;
        NSString * new = [current substringToIndex:[current length] - 1];
        if ([new length] > 0)
        {
            typeString = new;
        }
        else
        {
            typeString = @"";
        }
        
    }
    NSLog(@"after delete value =%@",typeString);
    [self setNumber:typeString];
    
}




-(void)checkValidation

{
    if ([typeString length]<4) {
    }
    else
    {
        
        
        if ([[dic_info valueForKey:@"Tag"] isEqualToString:@"UPDATE"]) {
            
            //hit update
            [self hitUpdateProfileAPI];
            
        }
        else if ([[dic_info valueForKey:@"Tag"] isEqualToString:@"DELETE"]) {
            
            //hit update
            [self hitDeleteProfileAPI];
            
        }
        
        else if ([TAG_FROM isEqualToString:@"UNLINK"]) {
            
            //hit update
            [self hitUnlinkProfileAPI];
            
        }
        
        else  if ([TAG_FROM isEqualToString:@"UNLINKAADHAAR"])
        {
            //hit update
            [self hitUnlinkAadhaarProfileAPI];
            
        }
        else if ([TAG_FROM isEqualToString:@"UPDATEQUESTION"])
        {
            [self hitUpdateQuestionAPI];
        }
        else if ([TAG_FROM isEqualToString:@"TOUCHID"])
        {
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }
        else
        {
            //do nothing
        }
        
        
    }
    
    
    
    
    
    
}


-(void)hitUpdateQuestionAPI
{
    NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",typeString,SaltMPIN];
    NSString *mpinStrEncrypted=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
    hud = [MBProgressHUD showHUDAddedTo:viewController.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    NSMutableDictionary *postDict = [NSMutableDictionary new];
    
    [postDict setObject:singleton.user_tkn forKey:@"tkn"]; //tkn number
    [postDict setObject:mpinStrEncrypted forKey:@"mpin"];  //MPIN
    [postDict setObject:@"" forKey:@"peml"];
    [postDict setObject:@"" forKey:@"aadhr"];
    [postDict setObject:self.quesAnswArray forKey:@"quesAnsList"];
    
    NSLog(@"dic_info=%@",dic_info);
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_UPDATEQUESTION withBody:postDict andTag:TAG_REQUEST_UPDATE_QUESTION completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                
                
                UIAlertController *okAlertCntrlr = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"security_question", nil) message:[response objectForKey:@"rd"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAlert = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    
                    
                    
                    [self.navigationController.viewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        
                        UIViewController *controller = obj;
                        
                        if ([obj isKindOfClass:[SecuritySettingVC class]]) {
                            
                            [self.navigationController popToViewController:controller
                                                                  animated:YES];
                            
                            *stop = YES;
                        }
                        
                    }];
                    
                }];
                [okAlertCntrlr addAction:okAlert];
                [self presentViewController:okAlertCntrlr animated:YES completion:nil];
                
                //[self closeEditProfile];
                ;
                
            }
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            typeString=@"";
            
            _txt_pin1.text=@"";
            _txt_pin2.text=@"";
            _txt_pin3.text=@"";
            _txt_pin4.text=@"";
            [self shake:_vw_txtcontain];
            
            
        }
        
    }];
}

-(void)hitUnlinkAadhaarProfileAPI
{
    
    NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",typeString,SaltMPIN];
    NSString *mpinStrEncrypted=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
    hud = [MBProgressHUD showHUDAddedTo:viewController.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    
    [dic_info setObject:mpinStrEncrypted forKey:@"mpin"];
    
    NSLog(@"dic_info=%@",dic_info);
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_UN_LINK_WITH_SOCIAL withBody:dic_info andTag:TAG_REQUEST_UNLINK_WITH_SOCIAL completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                
                [self closeEditProfile];
                
            }
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            typeString=@"";
            
            _txt_pin1.text=@"";
            _txt_pin2.text=@"";
            _txt_pin3.text=@"";
            _txt_pin4.text=@"";
            [self shake:_vw_txtcontain];
            
            
        }
        
    }];
    
    
}





-(void)hitUnlinkProfileAPI

{
    NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",typeString,SaltMPIN];
    NSString *mpinStrEncrypted=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
    hud = [MBProgressHUD showHUDAddedTo:viewController.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    
    [dic_info setObject:mpinStrEncrypted forKey:@"mpin"];
    
    NSLog(@"dic_info=%@",dic_info);
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_UN_LINK_WITH_SOCIAL withBody:dic_info andTag:TAG_REQUEST_UNLINK_WITH_SOCIAL completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                if ([socialLinkType isEqualToString:@"facebook"]) {
                    singleton.objUserProfile.objSocial.fb_id = @"";
                    singleton.objUserProfile.objSocial.fb_name = @"";
                    singleton.objUserProfile.objSocial.fb_image = @"";
                    
                    [[FBSDKLoginManager new] logOut];
                }
                else if ([socialLinkType isEqualToString:@"twitter"]) {
                    singleton.objUserProfile.objSocial.twitter_id = @"";
                    singleton.objUserProfile.objSocial.twitter_name = @"";
                    singleton.objUserProfile.objSocial.twitter_image = @"";
                    
                    TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
                    NSString *userID = store.session.userID;
                    [store logOutUserID:userID];
                    
                    
                }
                
                else if ([socialLinkType isEqualToString:@"google"]) {
                    singleton.objUserProfile.objSocial.google_id = @"";
                    singleton.objUserProfile.objSocial.google_name = @"";
                    singleton.objUserProfile.objSocial.google_image = @"";
                    
                    [[GIDSignIn sharedInstance] signOut];
                    
                }
                
                [self closeEditProfile];
                
            }
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            typeString=@"";
            
            _txt_pin1.text=@"";
            _txt_pin2.text=@"";
            _txt_pin3.text=@"";
            _txt_pin4.text=@"";
            [self shake:_vw_txtcontain];
            
            
        }
        
    }];
    
    
}
-(void)closeEditProfile
{
    if ([[dic_info valueForKey:@"Tag"] isEqualToString:@"UPDATE"])
    {
        
        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
        
        
        for (UIViewController *aViewController in allViewControllers)
        {
            if ([aViewController isKindOfClass:[ShowUserProfileVC class]])
            {
                // [aViewController removeFromParentViewController];
                [self.navigationController popToViewController:aViewController
                                                      animated:YES];
                
                // [self closeView];
                
            }
            
            
        }
        
    }
    
    else  if ([TAG_FROM isEqualToString:@"UNLINK"])
    {
        
        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
        
        
        for (UIViewController *aViewController in allViewControllers)
        {
            if ([aViewController isKindOfClass:[SocialMediaViewController class]])
            {
                // [aViewController removeFromParentViewController];
                [self.navigationController popToViewController:aViewController
                                                      animated:YES];
                [self closeView];
                
                [self startCollectionNotifier];
                
            }
        }
        
    }
    
    else if ([TAG_FROM isEqualToString:@"UNLINKAADHAAR"])
    {
        
        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
        
        
        for (UIViewController *aViewController in allViewControllers)
        {
            
            
            if ([aViewController isKindOfClass:[AadharCardViewCon class]])
            {
                singleton.objUserProfile = nil;
                
                // [aViewController removeFromParentViewController];
                [self.navigationController popToViewController:aViewController
                                                      animated:YES];
                [self closeView];
                
                
            }
            
            
            else if ([aViewController isKindOfClass:[MoreTabVC class]])
            {
                singleton.objUserProfile = nil;
                
                // [aViewController removeFromParentViewController];
                [self.navigationController popToViewController:aViewController
                                                      animated:YES];
                [self closeView];
                
                
            }
            
            
            
            
            
            
        }
        
    }else {
        [self closeView];
    }
    [viewController.view removeFromSuperview];
    [viewController removeFromParentViewController];
    
    
    
}

- (void) startCollectionNotifier
{
    // All instances of TestClass will be notified
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADSOCIALVIEW" object:nil];
    
    
    
}

- (IBAction)btn_close_Action:(id)sender {
    
    
    if ([[dic_info valueForKey:@"Tag"] isEqualToString:@"UPDATE"])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self closeView];
        
    }
}


-(void)closeView
{
    
    if ([self.TAG_FROM isEqualToString:@"UPDATEQUESTION"])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [UIView animateWithDuration:0.4 animations:^{
            self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        } completion:^(BOOL finished) {
            [self willMoveToParentViewController:nil];
            [self.view removeFromSuperview];
            [self removeFromParentViewController];
        }];
    }
    
    
    
}

-(IBAction)doneClicked:(id)sender
{
    //    [self checkValidation];
    
}


//----- hitAPI for IVR OTP call Type registration ------
-(void)hitDeleteProfileAPI
{
    
    
    
    
    //NSLog(@"dic_info=%@",dic_info);
    
    hud = [MBProgressHUD showHUDAddedTo:viewController.view animated:YES];
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    
    NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",typeString,SaltMPIN];
    NSString *mpinStrEncrypted=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
    
    
    
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"]; //tkn number
    [dictBody setObject:mpinStrEncrypted forKey:@"mpin"];  //MPIN
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    
    
    /*
     "status": "",                                    //Compulsory in case of delete profile= deleted
     "type": "",
     */
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:YES webServiceURL:UM_API_DELETE_PROFILE withBody:dictBody andTag:TAG_REQUEST_DELETE_PROFILE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        
        if (error == nil) {
            //NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            //            NSString *rc=[response valueForKey:@"rc"];
            //            NSString *rs=[response valueForKey:@"rs"];
            //            NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
            
            //NSLog(@"value of rc =%@ \n value of rs=%@ \n value of tkn=%@ ",rc,rs,tkn);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                singleton.notiTypeGenderSelected=@"";
                singleton.profileNameSelected =@"";
                singleton.profilestateSelected=@"";
                singleton.notiTypeCitySelected=@"";
                singleton.notiTypDistricteSelected=@"";
                singleton.profileUserAddress=@"";
                singleton.profileDOBSelected=@"";
                singleton.altermobileNumber=@"";
                singleton.user_Qualification=@"";
                singleton.user_Occupation=@"";
                singleton.user_profile_URL=@"";
                singleton.profileEmailSelected=@"";
                singleton.mobileNumber=@"";
                
                singleton.arr_initResponse = nil;
                //------------------------- Encrypt Value------------------------
                [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                // Encrypt
                [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"lastFetchDate"];
                [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"lastFetchV1"];
                [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"USER_ID"];
                [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"TOKEN_KEY"];
                [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"USER_PIC"];
                [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"PROFILE_COMPELTE_KEY"];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                //------------------------- Encrypt Value------------------------
                NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
                NSHTTPCookie *cookie;
                for (cookie in [storage cookies]) {
                    
                    [storage deleteCookie:cookie];
                    
                }
                NSMutableArray *cookieArray = [[NSMutableArray alloc] init];
                [[NSUserDefaults standardUserDefaults] setValue:cookieArray forKey:@"cookieArray"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                // Logout from social frameworks as well.
                SocialAuthentication *objSocial = [[SocialAuthentication alloc] init];
                [objSocial logoutFromAllSocialFramewors];
                objSocial = nil;
                
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"InitAPIResponse"];
                
                //------------------------- Encrypt Value------------------------
                [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                // Encrypt
                [[NSUserDefaults standardUserDefaults] encryptValue:@"NO" withKey:@"LINKDIGILOCKERSTATUS"];
                [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"digilocker_username"];
                [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"digilocker_password"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                //------------------------- Encrypt Value------------------------
                
                //——Remove Sharding Value——
                [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"NODE_KEY"];
                [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"lastDirectoryFetchDate"];
                //delete service directory
                [singleton.dbManager deleteServicesDirectory];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                //----- remove sharding--------
                
                @try {
                    [singleton.arr_recent_service removeAllObjects];
                    
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                // [self removePrefrence];
                // [singleton.dbManager  deleteAllNotifications];
                [singleton.dbManager deleteBannerHomeData];
                [singleton.dbManager  deleteAllServices];
                [singleton.dbManager  deleteSectionData];
                [singleton.dbManager deleteBannerStateData];
                
                [singleton setStateId:@""];
                singleton.stateSelected = @"";
                
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ABBR_KEY"];
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"EMB_STR"];
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"lastFetchV1"];
                [[NSUserDefaults standardUserDefaults] setObject:@"N" forKey:@"ChatSession"];
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"AllTabState"];
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Enable_ServiceDir"];
                [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SELECTED_TAB_INDEX"];
                [self alertwithMsg:rd];
            }
            
        }
        else{
            //NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
            typeString=@"";
            _txt_pin1.text=@"";
            _txt_pin2.text=@"";
            _txt_pin3.text=@"";
            _txt_pin4.text=@"";
            [self shake:_vw_txtcontain];
            
            
            
        }
        
    }];
    
}


-(void)removePrefrence
{
    
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        [defs removeObjectForKey:key];
    }
    [defs synchronize];
    
    
    /* [defaults setValue:singleton.objUserProfile.objAadhar.aadhar_number forKey:@"ADHAR_ADHARNUM_KEY"];
     [defaults setValue:singleton.objUserProfile.objAadhar.aadhar_image_url forKey:@"ADHAR_IMAGE_URL_KEY"];
     [defaults setValue:singleton.objUserProfile.objAadhar.pincode forKey:@"ADHAR_PINCODE_KEY"];
     [defaults setValue:singleton.objUserProfile.objAadhar.dob forKey:@"ADHAR_DOB_KEY"];
     [defaults setValue:singleton.objUserProfile.objAadhar.email forKey:@"ADHAR_EMAIL_KEY"];
     [defaults setValue:singleton.objUserProfile.objAadhar.gender forKey:@"ADHAR_GENDER_KEY"];
     [defaults setValue:singleton.objUserProfile.objAadhar.mobile_number forKey:@"ADHAR_MOB_KEY"];
     [defaults setValue:singleton.objUserProfile.objAadhar.name forKey:@"ADHAR_NAME_KEY"];
     [defaults setValue:singleton.objUserProfile.objAadhar.district forKey:@"ADHAR_DISTRICT_KEY"];
     [defaults setValue:singleton.objUserProfile.objAadhar.father_name forKey:@"ADHAR_FATHERNAME_KEY"];
     [defaults setValue:singleton.objUserProfile.objAadhar.state forKey:@"ADHAR_STATE_KEY"];
     [defaults setValue:singleton.objUserProfile.objAadhar.street forKey:@"ADHAR_STREET_KEY"];
     [defaults setValue:singleton.objUserProfile.objAadhar.subdistrict forKey:@"ADHAR_SUBDISTRICT_KEY"];
     [defaults setValue:singleton.objUserProfile.objAadhar.subdistrict forKey:@"ADHAR_SUBDISTRICT_KEY"];
     [defaults setValue:singleton.objUserProfile.objAadhar.vtc forKey:@"ADHAR_VTC_KEY"];
     [defaults setValue:singleton.objUserProfile.objAadhar.vtc_code forKey:@"ADHAR_VTCCODE_KEY"];
     
     
     
     ///---------- Mobile Credential----------------------------------
     [defaults setValue:singleton.notiTypeCitySelected forKey:@"CITY_KEY"];
     [defaults setValue:singleton.altermobileNumber forKey:@"ALTERMB_KEY"];
     [defaults setValue:singleton.user_Qualification forKey:@"QUALI_KEY"];
     [defaults setValue:singleton.user_Occupation forKey:@"OCCUP_KEY"];
     [defaults setValue:singleton.profileEmailSelected forKey:@"EMAIL_KEY"];
     [defaults setValue:singleton.user_profile_URL forKey:@"URLPROFILE_KEY"];
     [defaults setValue:singleton.user_tkn forKey:@"TOKEN_KEY"];
     [defaults setValue:singleton.mobileNumber forKey:@"MOBILE_KEY"];
     [defaults setValue:singleton.profileNameSelected forKey:@"NAME_KEY"];
     [defaults setValue:singleton.notiTypeGenderSelected forKey:@"GENDER_KEY"];
     [defaults setValue:singleton.profilestateSelected forKey:@"STATE_KEY"];
     [defaults setValue:singleton.notiTypDistricteSelected forKey:@"DISTRICT_KEY"];
     [defaults setValue:singleton.profileDOBSelected forKey:@"DOB_KEY"];
     */
    
}

- (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}


//----- hitAPI for IVR OTP call Type registration ------
-(void)hitUpdateProfileAPI
{
    // //NSLog(@"dic_info=%@",dic_info);
    hud = [MBProgressHUD showHUDAddedTo:viewController.view animated:YES];
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    
    //    dispatch_async(dispatch_get_main_queue(), ^
    //                   {
    
    
    @try {
        
        
        NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",typeString,SaltMPIN];
        NSString *mpinStrEncrypted=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
        NSLog(@"dic_info = %@",dic_info);
        
        NSString *name=[dic_info valueForKey:@"name"];
        NSString *gender=[dic_info valueForKey:@"gender"];
        NSString *state_id=[dic_info valueForKey:@"state_id"];
        NSString *city_id=[dic_info valueForKey:@"city_id"];
        NSString *district_id=[dic_info valueForKey:@"district_id"];
        NSString *DOB=[dic_info valueForKey:@"DOB"];
        NSString *ambo=[dic_info valueForKey:@"amno"];
        NSString *email=[dic_info valueForKey:@"email"];
        NSString *qual=[dic_info valueForKey:@"txt_qualification"];
        NSString *occup=[dic_info valueForKey:@"txt_occupation"];
        NSString *addr=[dic_info valueForKey:@"txt_address"];
        NSString *removePicStatus=[dic_info valueForKey:@"removePicStatus"];
        NSString *picChangeStatus=[dic_info valueForKey:@"picChangeStatus"];
        
        UIImage *img = [UIImage imageWithData:self.user_img];
        
        CGSize destinationSize = CGSizeMake(200, 200);
        UIGraphicsBeginImageContext(destinationSize);
        [img drawInRect:CGRectMake(0,0,destinationSize.width,destinationSize.height)];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        NSString *base64Image ;
        
        if ([removePicStatus isEqualToString:@"TRUE"])
        {
            base64Image=@"";
            // singleton.imageLocalpath=@"";
        }
        else
        {
            
            if ([singleton.user_profile_URL length]>0) {
                base64Image = [self encodeToBase64String:newImage];
                
            }
            else if ([picChangeStatus isEqualToString:@"TRUE"])
            {
                base64Image = [self encodeToBase64String:newImage];
                
            }
            
            
            else
            {
                base64Image=@"";
                
            }
            
        }
        NSLog(@"base64Image = %@",base64Image);
        
        
        
        // NSString *base64Image = [self encodeToBase64String:self.user_img];
        
        
        
        if ([base64Image length]==0) {
            base64Image=@"";
        }
        
        
        
        
        
        
        UMAPIManager *objRequest = [[UMAPIManager alloc] init];
        
        
        NSMutableDictionary *dictBody = [NSMutableDictionary new];
        [dictBody setObject:name forKey:@"nam"]; //Enter user name
        [dictBody setObject:gender forKey:@"gndr"];//Enter gender of user(m/f/t)
        [dictBody setObject:DOB forKey:@"dob"]; //Enter DOB of User
        [dictBody setObject:city_id forKey:@"cty"]; //Enter city of User
        [dictBody setObject:state_id forKey:@"st"]; //Enter State Of user
        [dictBody setObject:district_id forKey:@"dist"]; //Enter District Of user
        
        [dictBody setObject:ambo forKey:@"amno"];//Enter alternate mobile number of user
        [dictBody setObject:email forKey:@"email"];//Enter email address by user
        [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
        [dictBody setObject:singleton.user_tkn forKey:@"tkn"]; //tkn number
        [dictBody setObject:mpinStrEncrypted forKey:@"mpin"];  //MPIN
        [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
        
        [dictBody setObject:occup forKey:@"occup"];
        [dictBody setObject:addr forKey:@"addr"];
        [dictBody setObject:qual forKey:@"qual"];
        
        // [dictBody setObject:@"URL" forKey:@"flag"];  //PASS URL ONLY WHEN its from social profile
        
        [dictBody setObject:base64Image forKey:@"pic"];  //check for it
        
        NSLog(@"dictBody = %@",dictBody);
        
        [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:YES webServiceURL:UM_API_UPDATE_PROFILE withBody:dictBody andTag:TAG_REQUEST_UPDATE_PROFILE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
            [hud hideAnimated:YES];
            
            if (error == nil) {
                
                /*  {
                 gcmid = "";
                 node = "";
                 ntfp = "";
                 ntft = "";
                 pd =     {
                 pc = 95;
                 pcdtime = 10;
                 pctime = 1440000;
                 };
                 plang = "";
                 rc = 00;
                 rd = "UMANG Profile updated successfully.";
                 rs = S;
                 }
                 
                 
                 */
                
                NSString *profileComplete=[[response valueForKey:@"pd"]valueForKey:@"pc"];
                
                if ([profileComplete length]>0)
                {
                    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                    // Encrypt
                    [[NSUserDefaults standardUserDefaults] encryptValue:profileComplete withKey:@"PROFILE_COMPELTE_KEY"];
                    // [[NSUserDefaults standardUserDefaults] encryptValue:@"YES" withKey:@"SHOW_PROFILEBAR"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                
                
                //NSLog(@"Server Response = %@",response);
                
                //----- below value need to be forword to next view according to requirement after checking Android apk-----
                
                // NSString *rc=[response valueForKey:@"rc"];
                //NSString *rs=[response valueForKey:@"rs"];
                //NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
                
                //NSLog(@"value of rc =%@ \n value of rs=%@ \n value of tkn=%@ ",rc,rs,tkn);
                //----- End value need to be forword to next view according to requirement after checking Android apk-----
                NSString *rd=[response valueForKey:@"rd"];
                
                if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                    
                {
                    //[self alertwithMsg:rd];
                    if ([[dic_info valueForKey:@"Tag"] isEqualToString:@"UPDATE"]) {
                        
                        UIAlertView *alert = [[UIAlertView alloc]
                                              initWithTitle:NSLocalizedString(@"profile_label", nil)
                                              message:rd
                                              delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
                        [alert show];
                        
                        //hit update
                        // [self btn_close_Action:self];
                        
                        [self closeEditProfile];
                        
                    }
                    
                    
                    
                    
                    if ([removePicStatus isEqualToString:@"TRUE"]) {
                        
                        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                             NSUserDomainMask, YES);
                        NSString *documentsDirectory = [paths objectAtIndex:0];
                        singleton.imageLocalpath = [documentsDirectory stringByAppendingPathComponent:
                                                    @"user_image.png" ];
                        
                        
                        NSFileManager *fileManager = [NSFileManager defaultManager];
                        
                        
                        NSString *filePath = singleton.imageLocalpath;
                        NSError *error;
                        BOOL success = [fileManager removeItemAtPath:filePath error:&error];
                        if (success) {
                            singleton.imageLocalpath=@"";
                            singleton.user_profile_URL=@"";
                            
                            //------------------------- Encrypt Value------------------------
                            [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                            // Encrypt
                            [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_profile_URL withKey:@"USER_PIC"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            //------------------------- Encrypt Value------------------------
                            
                            
                            
                            
                            
                        }
                        else
                        {
                            NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
                        }
                        
                    }
                    else
                    {
                        
                        if([picChangeStatus isEqualToString:@"TRUE"])
                        {
                            NSLog(@"Pinchange=%@",picChangeStatus);
                            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
                            NSString *documentsDirectory = [paths objectAtIndex:0];
                            singleton.imageLocalpath = [documentsDirectory stringByAppendingPathComponent:
                                                        @"user_image.png" ];
                            NSData* data = user_img;
                            [data writeToFile:singleton.imageLocalpath atomically:YES];
                        }
                        
                        
                        
                        
                    }
                }
                
                [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                [[NSUserDefaults standardUserDefaults] encryptValue:@"YES" withKey:@"SHOW_PROFILEBAR"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
            }
            else{
                //NSLog(@"Error Occured = %@",error.localizedDescription);
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                                message:error.localizedDescription
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                      otherButtonTitles:nil];
                [alert show];
                
                typeString=@"";
                _txt_pin1.text=@"";
                _txt_pin2.text=@"";
                _txt_pin3.text=@"";
                _txt_pin4.text=@"";
                
                [self shake:_vw_txtcontain];
                
                
            }
            
        }];
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    // });
    
    
}


-(void)alertwithMsg:(NSString*)msg
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"profile_label", nil) message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                   {
                                       // singleton.user_mpin=@"";
                                       /* if ([[dic_info valueForKey:@"Tag"] isEqualToString:@"UPDATE"]) {
                                        
                                        //hit update
                                        // [self btn_close_Action:self];
                                        
                                        [self closeEditProfile];
                                        
                                        }*/
                                       if ([[dic_info valueForKey:@"Tag"] isEqualToString:@"DELETE"]) {
                                           
                                           //hit update
                                           [self openLoginview];
                                           
                                       }
                                       
                                       
                                   }];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)openLoginview
{
    @try {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        singleton.imageLocalpath = [documentsDirectory stringByAppendingPathComponent:
                                    @"user_image.png" ];
        
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        
        NSString *filePath = singleton.imageLocalpath;
        NSError *error;
        BOOL success = [fileManager removeItemAtPath:filePath error:&error];
        if (success) {
            singleton.imageLocalpath=@"";
        }
        else
        {
            NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
        }
        
        
    } @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    
    [singleton.reach stopNotifier];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:NO forKey:@"LOGIN_KEY"];
    //[defaults setValue:@"" forKey:@"TOKEN_KEY"];
    [defaults synchronize];
    
    
    //------------------------- Encrypt Value------------------------
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    // Encrypt
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"TOKEN_KEY"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"USER_PIC"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"PROFILE_COMPELTE_KEY"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    //------------------------- Encrypt Value------------------------
    
    //——Remove Sharding Value——
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"NODE_KEY"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //----- remove sharding--------
    
    
    singleton.imageLocalpath=@"";
    singleton.notiTypeGenderSelected=@"";
    singleton.profileNameSelected =@"";
    singleton.profilestateSelected=@"";
    singleton.notiTypeCitySelected=@"";
    singleton.notiTypDistricteSelected=@"";
    
    singleton.profileUserAddress = @"";
    singleton.profileDOBSelected=@"";
    singleton.altermobileNumber=@"";
    singleton.user_Qualification=@"";
    singleton.user_Occupation=@"";
    singleton.user_profile_URL=@"";
    singleton.profileEmailSelected=@"";
    singleton.mobileNumber=@"";
    singleton.user_id=@"";
    singleton.user_tkn=@"";
    singleton.user_profile_URL=@"";
    singleton.user_mpin=@"";
    singleton.user_aadhar_number=@"";
    singleton.objUserProfile = nil;
    
    
    
    
    @try {
        [singleton.arr_recent_service removeAllObjects];
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    //------------------------- Encrypt Value------------------------
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    // Encrypt
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"lastFetchDate"];
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"lastFetchV1"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"USER_ID"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"TOKEN_KEY"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"USER_PIC"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"PROFILE_COMPELTE_KEY"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    //------------------------- Encrypt Value------------------------
    
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSHTTPCookie *cookie;
    for (cookie in [storage cookies]) {
        
        [storage deleteCookie:cookie];
        
    }
    NSMutableArray *cookieArray = [[NSMutableArray alloc] init];
    [[NSUserDefaults standardUserDefaults] setValue:cookieArray forKey:@"cookieArray"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    //——Remove Sharding Value——
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"NODE_KEY"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //----- remove sharding--------
    
    
    
    @try {
        [singleton.arr_recent_service removeAllObjects];
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
    //[singleton.dbManager  deleteAllNotifications];
    [singleton.dbManager deleteBannerHomeData];
    [singleton.dbManager  deleteAllServices];
    [singleton.dbManager  deleteSectionData];
    
    
    
    
    /* UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     LoginViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
     [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
     [self presentViewController:vc animated:YES completion:nil];
     
     */
    
    //----later add
    [[NSUserDefaults standardUserDefaults] setInteger:kLoginScreenCase forKey:kInitiateScreenKey];
    [[NSUserDefaults standardUserDefaults] setBool:false forKey:kKeepMeLoggedIn];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    singleton.dbManager = [[UMSqliteManager alloc] initWithDatabaseFilename:@"UMANG_DATABASE.db"];
    
    [singleton.dbManager createUmangDB];
    
    //UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"LoginStoryboard" bundle:nil];
    //UIStoryboard *storyboard = [self grabStoryboard];
    
    LoginAppVC *vc;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginAppVC"];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        vc = [[LoginAppVC alloc] initWithNibName:@"LoginAppVC_iPad" bundle:nil];
    }
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.window.rootViewController = vc;
    
    [self presentViewController:vc animated:YES completion:nil];
    
    
    
}


-(void)callMpinVCWithType:(NSUInteger)type
{
    
    viewController = [[UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil] instantiateViewControllerWithIdentifier:@"JKLLockScreenViewController"] ;
    [viewController setLockScreenMode:type];    // enum { LockScreenModeNormal, LockScreenModeNew, LockScreenModeChange }
    [viewController setDelegate:self];
    [viewController setDataSource:self];
    [viewController setTintColor:[UIColor whiteColor]];
    // [self presentViewController:viewController animated:NO completion:NULL];
    [self addChildViewController:viewController];
    [self.view addSubview:viewController.view];
    [self.view bringSubviewToFront:viewController.view];
    [viewController didMoveToParentViewController:self];
    
}
#pragma mark -
#pragma mark YMDLockScreenViewControllerDelegate
- (void)unlockWasCancelledLockScreenViewController:(JKLLockScreenViewController *)lockScreenViewController {
    [self closeEditProfile];
    [viewController.view removeFromSuperview];
    [viewController removeFromParentViewController];
    NSLog(@"LockScreenViewController dismiss because of cancel");
}

- (void)unlockWasSuccessfulLockScreenViewController:(JKLLockScreenViewController *)lockScreenViewController pincode:(NSString *)pincode {
    
    typeString = pincode;
    [self checkValidation];
}

#pragma mark -
#pragma mark YMDLockScreenViewControllerDataSource
- (BOOL)lockScreenViewController:(JKLLockScreenViewController *)lockScreenViewController pincode:(NSString *)pincode {
    
#ifdef DEBUG
    NSLog(@"Entered Pincode : %@", self.enteredPincode);
#endif
    
    return [self.enteredPincode isEqualToString:pincode];
}

- (BOOL)allowTouchIDLockScreenViewController:(JKLLockScreenViewController *)lockScreenViewController {
    
    return NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*
 #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }
 */
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

