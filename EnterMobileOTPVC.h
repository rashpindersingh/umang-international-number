//
//  EnterMobileOTPVC.h
//  RegistrationProcess
//
//  Created by admin on 07/01/17.
//  Copyright © 2017 SpiceLabs. All rights reserved.
//

#import <UIKit/UIKit.h>




@interface EnterMobileOTPVC : UIViewController<UIScrollViewDelegate>
{
    IBOutlet UIView *resendOTPview;
    IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UIView *vwLineUnderResend;
    
    __weak IBOutlet UILabel *lblMaskedMNumber;
}
typedef enum
{
    
    ISFROMFORGOTMPIN= 100,
    ISFROMLOGINWITHOTP,
    IS_FROM_MOBILE_NUMBER_REGISTRATION,
    IS_FROM_AADHAR_LOGIN
}TYPE_LOGINSCREEN;



@property (assign,nonatomic )  TYPE_LOGINSCREEN TYPE_LOGIN_CHOOSEN;

@property(nonatomic,retain)NSString *altmobile;
@property(nonatomic,retain)NSString *maskedMobileNumber;
@property(nonatomic,retain)NSString *rtry;
@property(assign)int tout;
@property(nonatomic,retain)NSString *TAGFROM;

//--- for handling all resend and call option------


@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@property(nonatomic,retain)IBOutlet UIScrollView *scrollView;

@property(strong,nonatomic) NSString *strAadharNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblScreenTitleName;


@end
