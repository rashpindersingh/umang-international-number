//
//  DigiLockVerifyStatusVC.h
//  Umang
//
//  Created by admin on 06/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DigiLockVerifyStatusVC : UIViewController

{
    __weak IBOutlet UIButton *backButton;
    
    __weak IBOutlet UIButton *clickHereButton;
    __weak IBOutlet UILabel *accountCreatedLabel;
}
@end
