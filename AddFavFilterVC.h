//
//  AddFavFilterVC.h
//  Umang
//
//  Created by admin on 19/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddFavFilterVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tblFilter;
@property (nonatomic, assign) BOOL isRegionalSelected;
@property (weak, nonatomic) IBOutlet UIButton *btnApply;
@property (weak, nonatomic) IBOutlet UIButton *btnBackHome;
@property (weak, nonatomic) IBOutlet UILabel *lblFilter;
@property (weak, nonatomic) IBOutlet UIButton *btnReset;


- (IBAction)btnAlbhabaticClicked:(id)sender;
- (IBAction)btnTopRatedClicked:(id)sender;
- (IBAction)btnNearByClicked:(id)sender;
- (IBAction)btnbackClicked:(id)sender;
- (IBAction)btnApplyClicked:(id)sender;
- (IBAction)btnResetClicked:(id)sender;




@end
