//
//  HelpSettingVC.h
//  Umang
//
//  Created by deepak singh rawat on 15/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpSettingVC : UIViewController
-(IBAction)backbtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tbl_helpSetting;

@end
