//
//  DepartmentListingViewController.m
//  Umang
//
//  Created by Lokesh Jain on 11/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "DepartmentListingViewController.h"
#import "SharedManager.h"
#import "SDWebImageManager.h"
#import "EmailSupportViewController.h"

@interface DepartmentListingViewController ()
{
    NSMutableArray *deptListingArray;
    
    NSArray *filteredArray;
    IBOutlet UIButton *backbtn;
    IBOutlet UILabel *lbl_title;
    SharedManager *singleton;
}

@end

@implementation DepartmentListingViewController

- (void)viewDidLoad
{
    
    singleton = [SharedManager sharedSingleton];
    
    [backbtn setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    lbl_title.text=NSLocalizedString(@"choose_department", nil);
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:backbtn.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(backbtn.frame.origin.x, backbtn.frame.origin.y, backbtn.frame.size.width, backbtn.frame.size.height);
        
        [backbtn setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        backbtn.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setPlaceholder:NSLocalizedString(@"search_withoutDot", nil)];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    deptListingArray = [NSMutableArray new];
    
    filteredArray = [NSMutableArray new];
    
    deptListingArray = [[singleton.dbManager loadDataServiceData] mutableCopy];
    
    filteredArray = [deptListingArray mutableCopy];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    [super viewWillAppear:NO];
}


#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}


- (IBAction)backButtonPressed:(UIButton *)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([searchText isEqualToString:@""])
    {
        filteredArray = [deptListingArray mutableCopy];
        [self.deptTableView reloadData];
    }
    else
    {
        filteredArray = [deptListingArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SERVICE_NAME CONTAINS[c] %@", searchText]];
        
        [self.deptTableView reloadData];
    }
    
}

#pragma mark - TableView Delegate and Datasource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return filteredArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 50.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DepartmentCell *cell = (DepartmentCell *)[tableView dequeueReusableCellWithIdentifier:@"departmentCellIdentifier"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSString *deptText = [[filteredArray objectAtIndex:indexPath.row] valueForKey:@"SERVICE_NAME"] != nil ? [[filteredArray objectAtIndex:indexPath.row] valueForKey:@"SERVICE_NAME"] : @"";
    
    cell.departmentNameLabel.text = [NSString stringWithFormat:@"%@",deptText] ;
    
    if ([[filteredArray objectAtIndex:indexPath.row] valueForKey:@"SERVICE_IMAGE"] != nil)
    {
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:[NSURL URLWithString:[[filteredArray objectAtIndex:indexPath.row] valueForKey:@"SERVICE_IMAGE"]]
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    cell.deptImage.image = image;
                                    // do something with image
                                }
                                else
                                {
                                    cell.deptImage.image = nil;
                                }
                            }] ;
    }
    else
    {
        cell.deptImage = nil;
    }
    
    
    if (singleton.isArabicSelected==TRUE)
    {
        cell.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        cell.departmentNameLabel.transform= CGAffineTransformMakeScale(-1.0, 1.0);
        cell.deptImage.transform= CGAffineTransformMakeScale(-1.0, 1.0);
        cell.departmentNameLabel.textAlignment=NSTextAlignmentRight;
        
        /* UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,18,18)];
         CGAffineTransform rotationTransform = CGAffineTransformIdentity;
         rotationTransform = CGAffineTransformMakeScale(-1.0, 1.0);
         iv.transform = rotationTransform;
         
         
         if(indexPath == selectedIndexPath)
         {
         //cell.accessoryType = UITableViewCellAccessoryCheckmark;
         iv.image = [UIImage imageNamed:@"img_feedbackcheck"];
         cell.accessoryView = iv;
         }
         else
         {
         iv.image =nil;
         cell.accessoryView = iv;
         cell.accessoryType = UITableViewCellAccessoryNone;
         }*/
        
    }
    
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    
    
    [self.navigationController.viewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if ([obj isKindOfClass:[EmailSupportViewController class]])
        {
            EmailSupportViewController *objEmailSupport = obj;
            objEmailSupport.deptDict = [filteredArray objectAtIndex:indexPath.row];
            objEmailSupport.comingFrom = self.viewComingFrom;
            
            [self.navigationController popToViewController:objEmailSupport animated:YES];
        }
        
    }];
    
}

#pragma mark -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

#pragma mark - Department Cell Interface and Implementation

@interface DepartmentCell()
@end

@implementation DepartmentCell

@end
