//
//  BLVideo.m
//  BlendPhotoAndVideo
//
//  Created by Hoang Tran on 12/26/14.
//  Copyright (c) 2014 ILandApp. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "BLVideo.h"
#import "UIImage+FX.h"

@interface BLVideo ()

@property (nonatomic, strong) UIImage *thumbnailImage;

@end

@implementation BLVideo

-(UIImage*)thumbnail
{
    if (self.videoURL) {
        //if (!_thumbnailImage) {
            _thumbnailImage = [self loadThumbnailFromVideoURL:self.videoURL];
       // }
        return _thumbnailImage;
    } else {
        return [UIImage imageWithCGImage:[self.asset thumbnail]];
    }
}

- (UIImage*)loadThumbnailFromVideoURL:(NSURL*)vidURL {
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:vidURL options:nil];
    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 60);
    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
    
    UIImage *img = [UIImage imageWithCGImage:imgRef scale:1 orientation:UIImageOrientationUp];
    CGImageRelease(imgRef);
    return [img imageScaledToSize:[self getThumbnailSizeFromSize:img.size]];
}

-(UIImage*)fullSizeImage
{
    if (self.videoURL) {
        return [self loadThumbnailFromVideoURL:self.videoURL];
    } else {
        return [UIImage imageWithCGImage:[[self.asset defaultRepresentation] fullScreenImage]];
    }
}

-(BOOL)isVideo
{
    return YES;
}

@end
