//
//  Common.m
//

#import "Common.h"
#include <sys/xattr.h>
#import <MessageUI/MessageUI.h>
#import <sys/utsname.h>
#import "AppConstants.h"

BOOL isIPad()
{
  UIDevice *device = [UIDevice currentDevice];
  
  if ([device respondsToSelector:@selector(userInterfaceIdiom)])
  {
    UIUserInterfaceIdiom idiom = [device userInterfaceIdiom];
    switch (idiom) {
      case UIUserInterfaceIdiomPhone:
        return NO;  ///< iPhone
      case UIUserInterfaceIdiomPad:
        return YES;  ///< iPad
      default:
        return NO;  ///< Unknown idiom
    }
  }
  /// old device
  return NO;
}


BOOL isRetina()
{
  if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] 
      && [[UIScreen mainScreen] scale] == 2)
    return YES;  ///< iPhone 4
  /// other devices
  return NO;
}

BOOL addSkipBackupAttributeToFile(NSString *filePath)
{
  const char *path = [filePath fileSystemRepresentation];
  const char *attrName = "com.apple.MobileBackup";
  u_int8_t attrValue = 1;
  
  int result = setxattr(path, attrName, &attrValue, sizeof(attrValue), 0, 0);
  return result == 0;
}

NSString *criticalDataPath()
{
  NSString *path = [LibraryPath 
                    stringByAppendingPathComponent:@"Private Documents/CriticalData"];
  
  if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
    NSError *error = nil;
    [[NSFileManager defaultManager] createDirectoryAtPath:path 
                              withIntermediateDirectories:YES 
                                               attributes:nil 
                                                    error:&error];
    if (error) {
      NSLog(@"%@", [error localizedDescription]);
      return nil;
    }
  }
  return path;
}

NSString *offlineDataPath()
{
  NSString *path = [LibraryPath stringByAppendingPathComponent:
                    @"Private Documents/OfflineData"];
  
  if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
    NSError *error = nil;
    [[NSFileManager defaultManager] createDirectoryAtPath:path 
                              withIntermediateDirectories:YES 
                                               attributes:nil 
                                                    error:&error];
    if (error) {
      NSLog(@"%@", [error localizedDescription]);
      return nil;
    }
    if (!addSkipBackupAttributeToFile(path))
      return nil;
  }
  return path;
}

id loadNib(Class aClass, NSString *nibName, id owner)
{
  NSArray *niblets = [[NSBundle mainBundle] loadNibNamed:nibName 
                                                   owner:owner 
                                                 options:NULL];
  
  for (id niblet in niblets)
    if ([niblet isKindOfClass:aClass])
      return niblet;
  
  return nil;
}

CGSize CGSizeScaledToFitSize(CGSize size1, CGSize size2)
{
  float w, h;
  
  float k1 = size1.height / size1.width;
  float k2 = size2.height / size2.width;
  
  if (k1 > k2) {
    w = roundf(size1.width * size2.height / size1.height);
    h = size2.height;
  } else {
    w = size2.width;
    h = roundf(size1.height * size2.width / size1.width);
  }
  return CGSizeMake(roundf(w), roundf(h));
}

CGSize CGSizeScaledToFillSize(CGSize size1, CGSize size2)
{
  float w, h;
  
  float k1 = size1.height / size1.width;
  float k2 = size2.height / size2.width;
  
  if (k1 > k2) {
    w = size2.width;
    h = roundf(size1.height * size2.width / size1.width);
  } else {
    w = roundf(size1.width * size2.height / size1.height);
    h = size2.height;
  }
  return CGSizeMake(roundf(w), roundf(h));
}

CGRect CGRectWithSize(CGSize size) {
  CGRect rect = CGRectZero;
  rect.size = size;
  return rect;
}

CGRect CGRectFillRect(CGRect rect1, CGRect rect2) {
  CGRect rect;
  rect.size = CGSizeScaledToFillSize(rect1.size, rect2.size);
  rect.origin.x = roundf(rect2.origin.x + 0.5 * (rect2.size.width - rect.size.width));
  rect.origin.y = roundf(rect2.origin.y + 0.5 * (rect2.size.height - rect.size.height));
  return rect;
}

CGRect CGRectExpandToLabel(UILabel *label)
{
  float result = 1.;
  
  float width = label.frame.size.width;
  
  if (label.text)
  {
    CGSize textSize = CGSizeMake(width, 1000000.);
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = label.lineBreakMode;
    NSDictionary *attributes = @{NSFontAttributeName: label.font, NSParagraphStyleAttributeName: paragraphStyle};
    CGSize size = [label.text boundingRectWithSize:textSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
    result = MAX(size.height, result);
  }
  
  return CGRectMake(label.frame.origin.x, label.frame.origin.y, width, result);
}

void CustomizeNavBar()
{
  UIImage *image = nil;
	if (IsIOS7)
		image = [UIImage imageNamed:@"header@2x"];
  else
    image = [UIImage imageNamed:@"header"];
  [[UINavigationBar appearance] setBackgroundImage:image
                                     forBarMetrics:UIBarMetricsDefault];
  
	if (IsIOS7)
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
  else
    [[UINavigationBar appearance] setTintColor:UIColorFromHex(0x00a6eb)];
  
	[[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
  
}

UIColor *UIColorFromHex(int hexColor)
{
	UIColor* colorResult = [UIColor colorWithRed:(hexColor>>16&0xFF)/255.
                                         green:(hexColor>>8&0xFF)/255.
                                          blue:(hexColor>>0&0xFF)/255.
                                         alpha:1];
	return colorResult;
}

CGSize getBlendSize()
{
    float scale = 0.4f;
    if (IS_IPHONE_5) {
        scale = 1.0f;
    } else if (IS_IPHONE_6) {
        scale = 1.2f;
        NSLog(@"%f",scale);

    } else if (IS_IPHONE_6P) {
        scale = 1.2f;
    }
    
    if (AppDel.isFrameMode) {
        float w = 640.0f;
        
        if (IS_IPHONE_6) {
            w= 828;
        } else if (IS_IPHONE_6P) {
            w =828;
        }

        float h = w / SCALE_3_4_WIDTH;
        if (IS_IPHONE_4 || IS_IPAD) {
            h *= SCALE_3_4_HEIGHT;
        } else {
            h *= (SCALE_3_4_HEIGHT + HEIGHT_DIFF);
        }
        
        return CGSizeMake(roundf(w * scale), roundf(h * scale));
    }
    return CGSizeMake(roundf(720 * scale), roundf(720 * scale));
}

CGSize getPreviewSize ()
{
    float scale = 1.0f;
    if (IS_IPHONE_5) {
        scale = 1.0f;
    } else if (IS_IPHONE_6) {
        scale = 1.2f;
    } else if (IS_IPHONE_6P) {
        scale = 1.5f;
    }
    
    if (AppDel.isFrameMode) {
        if (IS_IPHONE_4 || IS_IPAD) {
            return CGSizeMake(roundf(SCALE_3_4_WIDTH * scale), roundf(SCALE_3_4_HEIGHT * scale));
        }  else {
            return CGSizeMake(roundf(SCALE_3_4_WIDTH * scale), roundf((SCALE_3_4_HEIGHT + HEIGHT_DIFF) * scale));
        }
    }
    return CGSizeMake(roundf(SCALE_1_1_WIDTH * scale), roundf(SCALE_1_1_WIDTH * scale));
}

BOOL isiPhone4Not4s()
{
    static BOOL result = NO;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        struct utsname systemInfo;
        
        uname(&systemInfo);
        
        NSString *modelName = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
        
        if([modelName isEqualToString:@"iPhone3,1"] || [modelName isEqualToString:@"iPod4,1"]) {
            result = YES;
        }
    });
    
    return result;
}
