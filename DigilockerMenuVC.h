//
//  DigilockerMenuVC.h
//  Umang
//
//  Created by admin on 06/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DigilockerMenuVC : UIViewController
{
    IBOutlet UIButton *btn_back;
    IBOutlet UIButton *btn_help;
    IBOutlet UIButton *btn_login;
    IBOutlet UIButton *btn_createAcct;

    
}



-(IBAction)backbtnAction:(id)sender;
-(IBAction)helpAction:(id)sender;
-(IBAction)loginAction:(id)sender;
-(IBAction)createAccountAction:(id)sender;


@end
