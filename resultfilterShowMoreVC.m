//
//  resultfilterShowMoreVC.m
//  Umang
//
//  Created by admin on 03/02/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "resultfilterShowMoreVC.h"
#import "SearchCell.h"
#import "CZPickerView.h"
#import "SDCapsuleButton.h"
#import "FilterServicesBO.h"
#import "HomeCardBO.h"
#import "NotificationItemBO.h"
#import "UIImageView+WebCache.h"
#import "HomeDetailVC.h"
#import "AllServiceCVCellForiPad.h"



#import "HCSStarRatingView.h"
#import "AllServiceCVCell.h"
#import "CustomImageFlowLayout.h"
#import "DetailServiceNewVC.h"

#import "ShowMoreServiceVC.h"


@interface resultfilterShowMoreVC () <CZPickerViewDelegate,CZPickerViewDataSource,UIActionSheetDelegate>
{
    NSMutableArray *arrMainData;
    NSMutableArray *arrTableData;
    
    NSArray *arrSortOptions;
    NSArray *arrFilterOptions;
}



@property (nonatomic, strong) HCSStarRatingView *starRatingView;
@property(nonatomic,retain)NSArray *sampleData ;
@property(nonatomic,retain)NSMutableArray *tableData ;
@property(nonatomic,retain)NSDictionary *cellData;
@property(nonatomic,retain)NSMutableDictionary * cellDataOfmore;
@end

@implementation resultfilterShowMoreVC

@synthesize btnSort,btnFilter;
@synthesize allSer_collectionView;
@synthesize cellData;
@synthesize cellDataOfmore;


- (void)viewDidLoad
{
    vw_noresults.hidden=TRUE;
    singleton = [SharedManager sharedSingleton];
    
    noResultsVW = [[FilterNoResultView alloc] initWithFrame:CGRectMake(0, 65, fDeviceWidth, fDeviceHeight - 65)];
    [noResultsVW setFilterBtnTitle];
    [self.view addSubview:noResultsVW];
    [noResultsVW.btnEditFilter addTarget:self action:@selector(btnSettingAgainClicked:) forControlEvents:UIControlEventTouchUpInside];
    noResultsVW.hidden = true;
    [vw_noresults removeFromSuperview];
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    //    [self prepareTempData];
    
    NSLog(@"XXXXXXX--------> value of isFromIndexFilter selected=%d",self.isFromIndexFilter);
    
    
    
    
    // Add Filter Capsules Button
    [self addFilterOptionsAtNavigationBar];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
    // Do any additional setup after loading the view.
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}

-(void)addFilterOptionsAtNavigationBar
{
    
    //Remove existing button
    
    [[self.scrollView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    // First fetch State Name
    
    
    NSMutableArray *arrStates = [self.dictFilterParams objectForKey:@"state_name"];
    
    CGFloat xCord = 10;
    CGFloat yCord = 0;
    CGFloat padding = 5;
    CGFloat itemHeight = 30.0;
    
    for (int i = 0; i < arrStates.count; i++)
    {
        
        NSString *stateName = arrStates[i];
        
        
        CGRect frame = [self rectForText:stateName usingFont:TITLE_FONT boundedBySize:CGSizeMake(1000.0, itemHeight)];
        frame.size.width = frame.size.width + 30;
        
        SDCapsuleButton *btn = [[SDCapsuleButton alloc] initWithFrameWithFullBorder:CGRectMake(xCord, yCord, frame.size.width, itemHeight)];
        [btn setBtnTitle:stateName];
        // [btn set]
        [btn addTarget:self action:@selector(btnCapsuleStateClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn.btnCross addTarget:self action:@selector(btnCrossClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([stateName isEqualToString:NSLocalizedString(@"all", nil)])
        {
            btn.btnCross.hidden = YES;
            btn.userInteractionEnabled = NO;
        }
        else
        {
            btn.btnCross.hidden = NO;
        }
        
        
        
        //  btn.btnCross.
        btn.tag = 2300+i;
        [self.scrollView addSubview:btn];
        xCord+=frame.size.width + padding;
    }
    
    
    // Now fetch Categories
    
    
    NSMutableArray *arrCategories = [self.dictFilterParams objectForKey:@"category_type"];
    
    for (int i = 0; i<arrCategories.count; i++) {
        NSString *serviceName = arrCategories[i];
        
        
        CGRect frame = [self rectForText:serviceName usingFont:TITLE_FONT boundedBySize:CGSizeMake(1000.0, itemHeight)];
        frame.size.width = frame.size.width + 30;
        
        SDCapsuleButton *btn = [[SDCapsuleButton alloc] initWithFrameWithFullBorder:CGRectMake(xCord, yCord, frame.size.width, itemHeight)];
        [btn setBtnTitle:serviceName];
        [btn addTarget:self action:@selector(btnCapsuleCategoryClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn.btnCross addTarget:self action:@selector(btnCrossClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        btn.tag = 2300+i;
        [self.scrollView addSubview:btn];
        xCord+=frame.size.width + padding;
    }
    
    
    NSString *_selectedNotificationType = [self.dictFilterParams objectForKey:@"service_type"];
    if (_selectedNotificationType.length) {
        
        
        CGRect frame = [self rectForText:_selectedNotificationType usingFont:TITLE_FONT boundedBySize:CGSizeMake(1000.0, itemHeight)];
        frame.size.width = frame.size.width + 30;
        
        SDCapsuleButton *btn = [[SDCapsuleButton alloc] initWithFrameWithFullBorder:CGRectMake(xCord, yCord, frame.size.width, itemHeight)];
        
        [btn setBtnTitle:_selectedNotificationType];
        [btn addTarget:self action:@selector(btnCapsuleServiceTypeClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn.btnCross addTarget:self action:@selector(btnCrossClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([_selectedNotificationType isEqualToString:NSLocalizedString(@"all", nil)])
        {
            btn.btnCross.hidden = YES;
            btn.userInteractionEnabled = NO;
        }
        else
        {
            btn.btnCross.hidden = NO;
        }
        
        
        [self.scrollView addSubview:btn];
        xCord+=frame.size.width + padding;
    }
    
    
    [self.scrollView setContentSize:CGSizeMake(xCord, self.scrollView.frame.size.height)];
    
    
    if (self.isFromHomeFilter) {
        [self getFilterDataForHomeScreen];
    }
    
}

-(void)btnCapsuleServiceTypeClicked:(SDCapsuleButton*)btnSender{
    //added here
    //added here
    //added here
    if ([[btnSender btnTitle] isEqualToString:NSLocalizedString(@"regional", nil)])
    {
        
        [self.dictFilterParams removeObjectForKey:@"service_type"];
        [self.dictFilterParams removeObjectForKey:@"state_name"];
        
    }
    else
    {
        [self.dictFilterParams removeObjectForKey:@"service_type"];
        
    }
    //added here
    //added here
    //added here
    [self addFilterOptionsAtNavigationBar];
}

-(void)btnCapsuleStateClicked:(SDCapsuleButton*)btnSender{
    
    NSLog(@"self.dictFilterParams=%@",self.dictFilterParams);
    NSMutableArray *arrStates = [self.dictFilterParams objectForKey:@"state_name"];
    [arrStates removeObject:[btnSender btnTitle]];
    //added here
    //added here
    //added here
    
    if ([arrStates count]==0) {
        [self.dictFilterParams removeObjectForKey:@"service_type"];
    }
    //added here
    //added here
    //added here
    [self.dictFilterParams setObject:arrStates forKey:@"state_name"];
    [self addFilterOptionsAtNavigationBar];
    
}

-(void)btnCapsuleCategoryClicked:(SDCapsuleButton*)btnSender{
    
    NSMutableArray *arrCategories = [self.dictFilterParams objectForKey:@"category_type"];
    
    [arrCategories removeObject:[btnSender btnTitle]];
    
    [self.dictFilterParams setObject:arrCategories forKey:@"category_type"];
    [self addFilterOptionsAtNavigationBar];
    
}
/*
 -(void)getFilterDataForHomeScreen{
 if (arrTableData == nil) {
 arrTableData = [[NSMutableArray alloc] init];
 }
 else{
 [arrTableData removeAllObjects];
 }
 NSString *sortBy = [self.dictFilterParams objectForKey:@"sort_by"];
 NSString *serviceType = [self.dictFilterParams objectForKey:@"service_type"];
 NSArray *_arrStates = [self.dictFilterParams objectForKey:@"state_name"];
 NSMutableArray *arrSelectedCategories = [self.dictFilterParams objectForKey:@"category_type"];
 NSArray *arrFilterResponse =  [singleton.dbManager getFilteredServiceData:sortBy serviceType:serviceType stateIdAlist:_arrStates categoryList:arrSelectedCategories];
 [arrTableData addObjectsFromArray:arrFilterResponse];
 
 if ([arrTableData count]>0) {
 _tblSearchFilter.hidden=FALSE;
 vw_noresults.hidden=TRUE;
 
 }
 else
 {
 _tblSearchFilter.hidden=TRUE;
 vw_noresults.hidden=false;
 
 }
 [_tblSearchFilter reloadData];
 }
 */
-(void)getFilterDataForHomeScreen
{
    if (arrTableData == nil) {
        arrTableData = [[NSMutableArray alloc] init];
    }
    else{
        [arrTableData removeAllObjects];
    }
    
    NSString *sortBy = [self.dictFilterParams objectForKey:@"sort_by"];
    NSString *serviceType = [self.dictFilterParams objectForKey:@"service_type"];
    NSArray *_arrStates = [self.dictFilterParams objectForKey:@"state_name"];
    
    //added here
    //added here
    //added here
    
   /* if ([_arrStates count]==0) {
        serviceType=@"";
    }*/
    //added here
    //added here
    //added here
    
    NSMutableArray *arrSelectedCategories = [self.dictFilterParams objectForKey:@"category_type"];
    NSArray *arrFilterResponse =  [singleton.dbManager getFilteredServiceData:sortBy serviceType:serviceType stateIdAlist:_arrStates categoryList:arrSelectedCategories];
    
    //-------------
    
    
    NSMutableArray *arr_filterCategory=[[NSMutableArray alloc]init];
    
    arr_filterCategory=[arrFilterResponse valueForKey:@"SERVICE_ID"];
    
    
    
    NSLog(@"arr_filterCategory=%@",arr_filterCategory);
    
    
    int indexpass=0;
    if (self.isFromIndexFilter ==0) {
        self.isFromIndexFilter=1;
    }
    else
    {
        indexpass =self.isFromIndexFilter -1;
    }
    
    // NSArray *arrServiceSection=[singleton.dbManager loadDataServiceSection];
    
    NSArray *arrServiceSection=[[singleton.dbManager loadDataServiceSection]objectAtIndex:indexpass];
    
    NSLog(@"arrServiceSection=%@",arrServiceSection);
    NSArray *arrServiceData=[singleton.dbManager loadDataServiceData];
    NSLog(@"arrServiceData=%@",arrServiceData);
    
    
    
    arrTableData=[[NSMutableArray alloc]init];
    
    @try {
        //do old stuff
        
        NSArray *itemsService = [[arrServiceSection valueForKey:@"SECTION_SERVICES"] componentsSeparatedByString:@","];
        NSLog(@"itemsService=%@",itemsService);
        
        // NSMutableArray *section_service=[[NSMutableArray alloc]init];
        
        for (int j=0; j<[itemsService count]; j++)
        {
            
            NSString *checkId=[NSString stringWithFormat:@"%@",[itemsService objectAtIndex:j]];
            NSLog(@"checkId=%@",checkId);
            
            if ([arr_filterCategory containsObject:checkId]) // YES
            {
                // Do something
                NSArray *sectionserviceitem=[singleton.dbManager getServiceData:[itemsService objectAtIndex:j]];
                NSLog(@"sectionserviceitem=%@",sectionserviceitem);
                if ([sectionserviceitem count]!=0) {
                    [arrTableData addObject:[sectionserviceitem objectAtIndex:0]];
                    
                }
                
            }
            
            else
            {
                //do nothing
            }
            
            
            
            
        }
        
        
        
        
        NSLog(@"arrTableData=%@",arrTableData);
        
        if ([arrTableData count]!=0) {
            
            
            if ([sortBy isEqualToString:NSLocalizedString(@"alphabetic", nil)])
            {
                NSSortDescriptor *aphabeticDescriptor = [[NSSortDescriptor alloc] initWithKey:@"SERVICE_NAME" ascending:YES selector:@selector(caseInsensitiveCompare:)];
                NSArray *sortDescriptors = [NSArray arrayWithObject:aphabeticDescriptor];
                arrTableData = [[arrTableData sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
            }
            
            if ([sortBy isEqualToString:NSLocalizedString(@"most_popular", nil)])
            {
                NSSortDescriptor *aphabeticDescriptor = [[NSSortDescriptor alloc] initWithKey:@"SERVICE_POPULARITY" ascending:NO] ;
                NSArray *sortDescriptors = [NSArray arrayWithObject:aphabeticDescriptor];
                arrTableData = [[arrTableData sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
            }
            
            
            if ([sortBy isEqualToString:NSLocalizedString(@"top_rated", nil)])
            {
                NSSortDescriptor *aphabeticDescriptor = [[NSSortDescriptor alloc] initWithKey:@"SERVICE_RATING" ascending:NO];
                NSArray *sortDescriptors = [NSArray arrayWithObject:aphabeticDescriptor];
                arrTableData = [[arrTableData sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
            }
            
        }
        
        //------------------------------
        //[self.sampleData addObjectsFromArray:arrFilterResponse];
        
        if ([arrTableData count]>0) {
            _tblSearchFilter.hidden=FALSE;
            // vw_noresults.hidden=TRUE;
            noResultsVW.hidden = true;
            
            
        }
        else
        {
            _tblSearchFilter.hidden=TRUE;
            //  vw_noresults.hidden=false;
            noResultsVW.hidden = false;
            
            
        }
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           [allSer_collectionView reloadData];
                           
                       });
        
        
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
}

/*
 
 Handle condition to jump here from
 call method name getFilteredFavouriteServiceData for favourite filters
 
 typedef enum{
 FILTER_HOME =108,
 FILTER_FAVOURITE,
 FILTER_ALLSERVICE,
 FILTER_NEARME,
 }FILTER_TYPE;
 
 
 */




-(void)btnCrossClicked:(SDCapsuleButton*)btnSender{
    
}

-(void)prepareTempData
{
    arrSortOptions = @[@"Alphabetic",@"Near By",@"Top Rated"];
    
    arrFilterOptions = @[@"Banking & Finance",@"Identification",@"Electricity",@"Property & Real Estate",@"Health care",@"Education",@"Traffic"];
}

/*
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
 {
 
 static NSString *CellIdentifier = @"searchFilterCell";
 SearchCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 cell.selectionStyle = UITableViewCellSelectionStyleNone;
 
 
 if (self.isFromHomeFilter) {
 NSDictionary *dict = [arrTableData objectAtIndex: indexPath.row];
 
 NSString *imageURLString = [dict objectForKey:@"SERVICE_IMAGE"];
 if (imageURLString) {
 NSURL *imgURL = [NSURL URLWithString:imageURLString];
 [cell.imgSearchCell sd_setImageWithURL:imgURL
 placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
 }
 
 
 cell.lblHeader.text = [dict objectForKey:@"SERVICE_NAME"] ;
 cell.lblDescription.text = [dict objectForKey:@"SERVICE_DESC"] ;
 }
 else{
 NotificationItemBO *object = [arrTableData objectAtIndex: indexPath.row];
 
 cell.imgSearchCell.image = [UIImage imageNamed:object.imgName];
 cell.lblHeader.text = NSLocalizedString(object.headerTitle, nil) ;
 cell.lblDescription.text = NSLocalizedString(object.headerDesc, nil) ;
 }
 cell.layoutMargins = UIEdgeInsetsZero;
 return cell;
 
 }
 
 - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
 {
 
 NSDictionary *cellData = [arrTableData objectAtIndex:indexPath.row];
 if (cellData)
 {
 
 HomeDetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
 [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
 vc.dic_serviceInfo=cellData;
 vc.tagComeFrom=@"OTHERS";
 
 [self presentViewController:vc animated:NO completion:nil];
 
 
 }
 }
 */

-(IBAction)btnBackClicked:(id)sender
{
    NSLog(@"inside back");
    //
    
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    
    
    for (UIViewController *aViewController in allViewControllers)
    {
        if ([aViewController isKindOfClass:[ShowMoreServiceVC class]])
        {
            // [aViewController removeFromParentViewController];
            [self.navigationController popToViewController:aViewController
                                                  animated:YES];
            
        }
        
        
    }
    
    
    
    //  [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    
    self.view.backgroundColor = [UIColor colorWithRed:248.0/255.0f green:246.0/255.0f blue:247.0/255.0f alpha:1.0f];
    //tableView.backgroundColor = [UIColor clearColor];
    
    allSer_collectionView.delegate = self;
    allSer_collectionView.dataSource = self;
    allSer_collectionView.backgroundColor = [UIColor clearColor];
    allSer_collectionView.collectionViewLayout = [[CustomImageFlowLayout alloc] init];
    
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    // [allSer_collectionView.collectionViewLayout setItemSize:CGSizeMake(200, 200)];
    
    
    /*UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(btnBackClicked:)];
     [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
     [self.navigationController.view addGestureRecognizer:gestureRecognizer];
     */
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    
    
    [allSer_collectionView reloadData];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [self setViewFont];
    
    [super viewWillAppear:NO];
}

#pragma mark- Font Set to View
-(void)setViewFont
{
    lb_noresults.font = [AppFont regularFont:14.0];
}
#pragma mark
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnSortActionClicked:(UIButton *)sender
{
    btnFilter.selected = NO;
    btnSort.selected = YES;
    [self showSortOptions];
}

- (IBAction)btnFilterClicked:(UIButton*)sender
{
    btnFilter.selected = YES;
    btnSort.selected = NO;
    
    [self showFilterOptions];
}

#pragma mark- Sort Picker

-(void)showSortOptions
{
    
    CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:NSLocalizedString(@"sort", nil) cancelButtonTitle:nil confirmButtonTitle:NSLocalizedString(@"Apply", nil)];
    picker.delegate = self;
    picker.dataSource = self;
    picker.allowMultipleSelection = YES;
    picker.allowRadioButtons = YES;
    picker.isClearOptionRequired = NO;
    [picker show];
    
}

-(void)showFilterOptions
{
    CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:NSLocalizedString(@"filter_caps", nil) cancelButtonTitle:nil confirmButtonTitle:NSLocalizedString(@"Apply", nil)];
    picker.delegate = self;
    picker.dataSource = self;
    picker.allowMultipleSelection = YES;
    picker.isClearOptionRequired = NO;
    picker.allowRadioButtons = YES;
    [picker show];
    
}




- (NSAttributedString *)czpickerView:(CZPickerView *)pickerView
               attributedTitleForRow:(NSInteger)row
{
    NSAttributedString *att;
    
    if ([btnSort isSelected])
    {
        att = [[NSAttributedString alloc]
               initWithString:NSLocalizedString(arrSortOptions[row], nil)
               attributes:@{
                            NSFontAttributeName:[UIFont fontWithName:@"Avenir-Light" size:18.0]
                            }];
        
        
    }
    else
    {
        att = [[NSAttributedString alloc]
               initWithString:NSLocalizedString(arrFilterOptions[row], nil)
               attributes:@{
                            NSFontAttributeName:[UIFont fontWithName:@"Avenir-Light" size:18.0]
                            }];
    }
    
    return att;
}

- (NSString *)czpickerView:(CZPickerView *)pickerView
               titleForRow:(NSInteger)row
{
    if ([btnFilter isSelected])
    {
        return arrFilterOptions[row];
    }
    else
        
        return arrSortOptions[row];
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView
{
    if ([btnFilter isSelected])
        return arrFilterOptions.count;
    
    
    else
    {
        return arrSortOptions.count;
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 135 ;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        
        height = 220;
        
    }else {
        if (fDeviceWidth == 320)
        {
            height = 135;
        }
        else if (fDeviceWidth == 375)
        {
            height = 145;
        }
        else if (fDeviceWidth > 375)
        {
            height = 160;
        }
        
    }
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return CGSizeMake((fDeviceWidth - 75 ) / 4.0, height);
    }
    return  CGSizeMake((fDeviceWidth - 60 ) / 3.0, height);
    /*
     if (UIInterfaceOrientationIsLandscape(UIApplic
     ation.sharedApplication.statusBarOrientation))
     {
     
     NSLog(@"UIInterfaceOrientationIsLandscape xxxxxxx value=%f",[[ UIScreen mainScreen] bounds].size.height);
     // 375.000000
     
     if ([[ UIScreen mainScreen] bounds].size.height >= 768)
     {
     return CGSizeMake(150, 220);
     }
     else if ([[UIScreen mainScreen] bounds].size.height <= 320)
     {
     return CGSizeMake(90, 135);
     }
     else if ([[UIScreen mainScreen] bounds].size.height <= 375)
     { return CGSizeMake(105, 150);}
     else
     return CGSizeMake(105, 150);
     
     
     
     }
     else
     {
     //667
     
     NSLog(@"UIInterfaceOrientationIsLandscape NOT xxxxxxx value=%f",[[ UIScreen mainScreen] bounds].size.height);
     if ([[ UIScreen mainScreen] bounds].size.height >= 1024)
     {
     return CGSizeMake(150, 220);
     }
     else if ([[UIScreen mainScreen] bounds].size.height <= 568)
     {
     return CGSizeMake(90, 135);
     }
     else if ([[UIScreen mainScreen] bounds].size.height <= 667)
     { return CGSizeMake(105, 150);}
     else
     return CGSizeMake(105, 150);
     
     
     
     
     }
     
     return CGSizeMake(105, 150);*/
    
    
}

/* picker item image for each row */
//- (UIImage *)czpickerView:(CZPickerView *)pickerView imageForRow:(NSInteger)row
//{
//    if ([btnFilter isSelected])
//    {
//        UIImage *imageIcon = nil;
//        switch (row) {
//            case 0:
//                imageIcon = [UIImage imageNamed:@"icon_alphabatic-1"];
//                break;
//            case 1:
//                imageIcon = [UIImage imageNamed:@"icon_nearby"];
//
//                break;
//            case 2:
//                imageIcon = [UIImage imageNamed:@"icon_top_rated"];
//
//                break;
//
//            default:
//                break;
//
//    }
//    }
//    else
//    {
//
//    }
//


//  return imageIcon;
//}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HomeDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    vc.dic_serviceInfo=[arrTableData objectAtIndex:indexPath.row];
    vc.tagComeFrom=@"OTHERS";
    
    [self presentViewController:vc animated:NO completion:nil];
    
    
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *) collectionView
                        layout:(UICollectionViewLayout *) collectionViewLayout
        insetForSectionAtIndex:(NSInteger) section {
    
    return UIEdgeInsetsMake(0,13,0, 16); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *) collectionView
                   layout:(UICollectionViewLayout *) collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger) section {
    return 5.0;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 15;
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return arrTableData.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    if ([[ UIScreen mainScreen] bounds].size.height >= 1024)
    {
        static NSString *identifier = @"AllServiceCVCellForiPad";
        AllServiceCVCellForiPad *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        
        cell.serviceTitle.text =[[arrTableData valueForKey:@"SERVICE_NAME"]objectAtIndex:indexPath.row];
        
        cell.serviceTitle.font = [UIFont systemFontOfSize:18];
        
        NSURL *url=[NSURL URLWithString:[[arrTableData valueForKey:@"SERVICE_IMAGE"]objectAtIndex:indexPath.row]];
        
        [cell.serviceImage sd_setImageWithURL:url
                             placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
        
        
        
        int tagvalue= (int)(indexPath.row);
        cell.serviceFav.tag=tagvalue;
        
        
        //        UIImage * btnImage1 = [UIImage imageNamed:@"icon_favourite"];
        //        UIImage * btnImage2 = [UIImage imageNamed:@"icon_favourite_red"];
        //
        //        [cell.serviceFav setImage:btnImage1 forState:UIControlStateNormal];
        //        [cell.serviceFav setImage:btnImage2 forState:UIControlStateSelected];
        
        
        
        NSString *serviceid=[NSString stringWithFormat:@"%@",[[arrTableData valueForKey:@"SERVICE_ID"] objectAtIndex:indexPath.row]];
        
        NSString *serviceFav=[singleton.dbManager getServiceFavStatus:serviceid];//get service status from db
        
        if ([serviceFav isEqualToString:@"true"]) {
            cell.serviceFav .selected=YES;
        }else{
            cell.serviceFav .selected=NO;
        }
        
        
        cellData = (NSDictionary*)[arrTableData objectAtIndex:[indexPath row]];
        
        cell.serviceInfo.celldata=[cellData mutableCopy];
        
        
        [cell.serviceInfo  addTarget:self action:@selector(moreInfo:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.serviceFav.usdata=[cellData objectForKey:@"SERVICE_NAME"];
        [cell.serviceFav  addTarget:self action:@selector(fav_action:) forControlEvents:UIControlEventTouchUpInside];
        
        
        NSString *rating=[cellData objectForKey:@"SERVICE_RATING"];
        
        float fCost = [rating floatValue];
        cell.starRatingView.value = fCost;
        
        // [cell.starRatingView setScore:fCost*2 withAnimation:NO];
        
        
        
        cell.serviceTitle.font = [AppFont regularFont:14.0];
        
        return cell;
        
        
        
        
        
        
    }
    else
    {
        static NSString *identifier = @"AllServiceCVCell";
        
        
        
        AllServiceCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        cell.serviceTitle.text =[[arrTableData valueForKey:@"SERVICE_NAME"]objectAtIndex:indexPath.row];
        
        
        NSURL *url=[NSURL URLWithString:[[arrTableData valueForKey:@"SERVICE_IMAGE"]objectAtIndex:indexPath.row]];
        
        [cell.serviceImage sd_setImageWithURL:url
                             placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
        
        
        
        int tagvalue= (int)(indexPath.row);
        cell.serviceFav.tag=tagvalue;
        
        
        //        UIImage * btnImage1 = [UIImage imageNamed:@"icon_favourite"];
        //        UIImage * btnImage2 = [UIImage imageNamed:@"icon_favourite_red"];
        //
        //        [cell.serviceFav setImage:btnImage1 forState:UIControlStateNormal];
        //        [cell.serviceFav setImage:btnImage2 forState:UIControlStateSelected];
        
        
        
        NSString *serviceid=[NSString stringWithFormat:@"%@",[[arrTableData valueForKey:@"SERVICE_ID"] objectAtIndex:indexPath.row]];
        
        NSString *serviceFav=[singleton.dbManager getServiceFavStatus:serviceid];//get service status from db
        
        if ([serviceFav isEqualToString:@"true"]) {
            cell.serviceFav .selected=YES;
        }else{
            cell.serviceFav .selected=NO;
        }
        
        
        cellData = (NSDictionary*)[arrTableData objectAtIndex:[indexPath row]];
        
        cell.serviceInfo.celldata=[cellData mutableCopy];
        
        
        [cell.serviceInfo  addTarget:self action:@selector(moreInfo:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.serviceFav.usdata=[cellData objectForKey:@"SERVICE_NAME"];
        [cell.serviceFav  addTarget:self action:@selector(fav_action:) forControlEvents:UIControlEventTouchUpInside];
        
        
        NSString *rating=[cellData objectForKey:@"SERVICE_RATING"];
        
        float fCost = [rating floatValue];
        
        cell.starRatingView.value = fCost;
        
        //[cell.starRatingView setScore:fCost*2 withAnimation:NO];
        
        
        
        cell.serviceTitle.font = [AppFont regularFont:14.0];
        
        return cell;
    }
    
    
    
    
    
    
}

//==================================
//       CUSTOM PICKER STARTS
//==================================
- (void)btnOpenSheet

{
    
    NSString *information=[NSString stringWithFormat:@"%@",NSLocalizedString(@"information", nil)];
    
    NSString *viewMap=NSLocalizedString(@"view_on_map", nil);
    NSString *cancelinfo=NSLocalizedString(@"cancel", nil);
    
    
    UIActionSheet *  sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                         delegate:self
                                                cancelButtonTitle:cancelinfo
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:information,viewMap, nil];
    
    //  [[[sheet valueForKey:@"_buttons"] objectAtIndex:0] setImage:[UIImage imageNamed:@"serviceinfo"] forState:UIControlStateNormal];
    
    //  [[[sheet valueForKey:@"_buttons"] objectAtIndex:1] setImage:[UIImage imageNamed:@"serivemap"] forState:UIControlStateNormal];
    
    UIViewController *vc=[self topMostController];
    // Show the sheet
    [sheet showInView:vc.view];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            [self callDetailServiceVC];
        }
            break;
        case 1:
        {
            NSLog(@"VISIT MAP LONG=%@", [NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]]);
            
            NSString *latitude=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LATITUDE"]];
            
            NSString *longitute=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]];
            
            NSString *deptName=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_NAME"]];
            deptName = [deptName stringByReplacingOccurrencesOfString:@" " withString:@"+"];
            
            if ([[UIApplication sharedApplication] canOpenURL:
                 [NSURL URLWithString:@"comgooglemaps://"]])
                
            {
                NSLog(@"Map App Found");
                
                NSString* googleMapsURLString = [NSString stringWithFormat:@"comgooglemaps://?q=%.6f,%.6f(%@)&center=%.6f,%.6f&zoom=15&views=traffic",[latitude doubleValue], [longitute doubleValue],deptName,[latitude doubleValue], [longitute doubleValue]];
                
                // googleMapsURLString = [googleMapsURLString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]; //IOS 9 and above use this line
                
                NSURL *mapURL=[NSURL URLWithString:[googleMapsURLString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
                NSLog(@"mapURL= %@",mapURL);
                
                [[UIApplication sharedApplication] openURL:mapURL];
                
            } else
            {
                NSString* googleMapsURLString = [NSString stringWithFormat:@"https://maps.google.com/maps?&z=15&q=%.6f+%.6f&ll=%.6f+%.6f",[latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
                
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:googleMapsURLString]];
                
            }
            
        }
            break;
        case 2:
        {
            
        }
            break;
        default:
            break;
    }
}

//==================================
//          CUSTOM PICKER ENDS
//==================================



//------------ Show more info-------------
-(IBAction)moreInfo:(MyButton*)sender
{
    NSLog(@"Data 1 = %@",sender.celldata);
    
    self.cellDataOfmore=[[NSMutableDictionary alloc]init];
    
    self.cellDataOfmore=(NSMutableDictionary*)sender.celldata;
    
    [self btnOpenSheet];
    
    
    
    /* NSString *titlename=[NSString stringWithFormat:@"%@",[cellDataOfmore objectForKey:@"SERVICE_NAME"]];
     
     //NSString *titleDesc=[NSString stringWithFormat:@"%@",[cellDataOfmore objectForKey:@"SERVICE_DESC"]];
     
     // NSString *imageName=[NSString stringWithFormat:@"%@",[cellDataOfmore objectForKey:@"SERVICE_IMAGE"]];
     
     //NSString *Type=[NSString stringWithFormat:@"%@",[cellDataOfmore objectForKey:@"SERVICE_CATEGORY"]];
     
     
     // [self displayContentController:[self getHomeDetailLayerLeftController]];
     
     
     UIAlertController * alert=   [UIAlertController
     alertControllerWithTitle:titlename
     message:nil
     preferredStyle:UIAlertControllerStyleAlert];
     
     UIAlertAction* info = [UIAlertAction
     actionWithTitle:NSLocalizedString(@"information", nil)
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     // [self displayContentController:[self getHomeDetailLayerLeftController]];
     
     [self callDetailServiceVC];
     [alert dismissViewControllerAnimated:YES completion:nil];
     
     }];
     UIAlertAction* map = [UIAlertAction
     actionWithTitle:NSLocalizedString(@"view_on_map", nil)
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     
     NSLog(@"VISIT MAP LONG=%@", [NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]]);
     
     NSString *latitude=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LATITUDE"]];
     
     NSString *longitute=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]];
     
     
     // NSString* googleMapsURLString = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@,%@",latitude, longitute];
     
     
     
     //  [[UIApplication sharedApplication] openURL: [NSURL URLWithString: googleMapsURLString]];
     
     
     if ([[UIApplication sharedApplication] canOpenURL:
     [NSURL URLWithString:@"comgooglemaps://"]])
     
     {
     NSLog(@"Map App Found");
     
     NSString* googleMapsURLString = [NSString stringWithFormat:@"comgooglemaps://?q=%.6f,%.6f&center=%.6f,%.6f&zoom=15&views=traffic", [latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
     
     
     
     
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsURLString]];
     
     
     } else
     {
     NSString* googleMapsURLString = [NSString stringWithFormat:@"https://maps.google.com/maps?&z=15&q=%.6f+%.6f&ll=%.6f+%.6f",[latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
     
     [[UIApplication sharedApplication]openURL:[NSURL URLWithString:googleMapsURLString]];
     
     }
     
     
     [alert dismissViewControllerAnimated:YES completion:nil];
     
     }];
     UIAlertAction* cancel = [UIAlertAction
     actionWithTitle:NSLocalizedString(@"cancel", nil)
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     [alert dismissViewControllerAnimated:YES completion:nil];
     
     }];
     
     
     [alert addAction:info];
     [alert addAction:map];
     
     [alert addAction:cancel];
     
     
     UIViewController *vc=[self topMostController];
     [vc presentViewController:alert animated:NO completion:nil];
     */
}

-(void)callDetailServiceVC
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName: kDetailServiceStoryBoard bundle:nil];
    // UIStoryboard *storyboard = [self grabStoryboard];
    //#import "DetailServiceNewVC.h"
    DetailServiceNewVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DetailServiceNewVC"];
    
    vc.dic_serviceInfo=self.cellDataOfmore;//change it to URL on demand
    
    UIViewController *topvc=[self topMostController];
    //[topvc.navigationController pushViewController:vc animated:YES];
    
    [topvc presentViewController:vc animated:NO completion:nil];
    
}


//----------- END OF MORE INFO POP UP VIEW---------------

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}



-(IBAction)fav_action:(MyFavButton*)sender
{
    MyFavButton *button = (MyFavButton *)sender; //instance of UIButton
    int indexOfTheRow=(int)button.tag;   //tag of the button
    NSLog(@"tabledata=%@",[arrTableData objectAtIndex:indexOfTheRow]);
    
    // Add image to button for pressed state
    //    UIImage * btnImage1 = [UIImage imageNamed:@"icon_favourite"];
    //    UIImage * btnImage2 = [UIImage imageNamed:@"icon_favourite_red"];
    //
    //    [button setImage:btnImage1 forState:UIControlStateNormal];
    //    [button setImage:btnImage2 forState:UIControlStateSelected];
    
    NSString *serviceId=[[arrTableData objectAtIndex:indexOfTheRow] valueForKey:@"SERVICE_ID"];
    
    NSString *serviceFav=[singleton.dbManager getServiceFavStatus:serviceId];
    
    if ([serviceFav isEqualToString:@"true"])// Is selected?
    {
        [singleton.dbManager updateServiceIsFav:serviceId serviceIsFav:@"false" hitAPI:@"Yes"];
        button.selected=FALSE;
    }
    else
    {
        [singleton.dbManager updateServiceIsFav:serviceId serviceIsFav:@"true" hitAPI:@"Yes"];
        button.selected=true;
    }
    
    
    
}









- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemsAtRows:(NSArray *)rows
{
    
    
    
}

- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView {
    NSLog(@"Canceled.");
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation duration:(NSTimeInterval)duration
{
    [allSer_collectionView reloadData];
}


- (IBAction)btnSettingAgainClicked:(id)sender
{
    
    if ([self.delegate respondsToSelector:@selector(filterChanged:andCategoryDict:)])
    {
        
        for (int i = 0; i< self.filterBOArray.count; i++)
        {
            FilterServicesBO *objBo = [self.filterBOArray objectAtIndex:i];
            
            if ([[self.dictFilterParams objectForKey:@"category_type"] containsObject:objBo.serviceName])
            {
                objBo.isServiceSelected = YES;
            }
            else
            {
                objBo.isServiceSelected = NO;
            }
        }
        
        
        NSMutableDictionary *categoryDict = [NSMutableDictionary new];
        
        if ([self.dictFilterParams valueForKey:@"state_name"] != nil)
        {
            [categoryDict setValue:[self.dictFilterParams valueForKey:@"state_name"] forKey:@"selectedStates"];
            
        }
        
        if ([self.dictFilterParams valueForKey:@"service_type"] != nil)
        {
            [categoryDict setValue:[self.dictFilterParams valueForKey:@"service_type"] forKey:@"selectedCategory"];
        }
        
        [self.delegate filterChanged:self.filterBOArray andCategoryDict:categoryDict];
    }
    [self.navigationController popViewControllerAnimated:YES];
}



#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*
 #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }
 */
@end

