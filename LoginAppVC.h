//
//  LoginAppVC.h
//  Umang
//
//  Created by deepak singh rawat on 06/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
    LOGIN_OTP = 112,
    LOGIN_FACEBOOK,
    LOGIN_TWITTER,
    LOGIN_GOOGLE,
    LOGIN_MPIN,
    LOGIN_AADHAR,
}LOGIN_TYPE;



@interface LoginAppVC :  UIViewController <UIScrollViewDelegate>
{
    IBOutlet UIView *vw_login;
    BOOL flg_LgType;
    
    IBOutlet UIImageView *imageBackground;
    
    __weak IBOutlet UILabel *lblPrefixMobileNum;
    IBOutlet UILabel *lbl_line1;
    IBOutlet UILabel *lbl_line2;
    IBOutlet UIScrollView *scrollView;
    
}
@property(nonatomic,assign)LOGIN_TYPE USER_LOGIN_TYPE;

//----- added for passing rty and tout-----
@property(nonatomic,retain)NSString *rtry;
@property(assign)int tout;
//----- added for passing rty and tout-----




@property (weak, nonatomic) IBOutlet UITextField *txtAadhar;
@property (weak, nonatomic) IBOutlet UIImageView *img_adhar;
@property (weak, nonatomic) IBOutlet UILabel *lbl_line3;


@property (weak, nonatomic) IBOutlet UIButton *btnFacebook;
@property (weak, nonatomic) IBOutlet UIButton *btnGoogle;




@property (weak, nonatomic) IBOutlet UITextField *txtMPin;

@property (weak, nonatomic) IBOutlet UIButton *btnLoginWithOTP;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPin;
@property (weak, nonatomic) IBOutlet UILabel *orLoginWith;
@property (weak, nonatomic) IBOutlet UILabel *lblDontHaveAccount;

@property (weak, nonatomic) IBOutlet UIButton *btnNewUser;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;

@property(nonatomic,strong)NSString *socialID;

@end
