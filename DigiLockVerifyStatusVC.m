//
//  DigiLockVerifyStatusVC.m
//  Umang
//
//  Created by admin on 06/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "DigiLockVerifyStatusVC.h"

@interface DigiLockVerifyStatusVC ()

@end

@implementation DigiLockVerifyStatusVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [backButton setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    [clickHereButton setTitle:NSLocalizedString(@"click_here_to_login", nil) forState:UIControlStateNormal];
    
    [self setViewFont];

}
#pragma mark- Font Set to View
-(void)setViewFont{
    [backButton.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    accountCreatedLabel.font = [AppFont regularFont:14.0];
    clickHereButton.titleLabel.font = [AppFont regularFont:12.0];
    
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}
- (IBAction)clickHerePressed:(UIButton *)sender
{
    
}
- (IBAction)backButtonPressed:(UIButton *)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
