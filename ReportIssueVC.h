//
//  ReportIssueVC.h
//  Umang
//
//  Created by admin on 15/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportIssueVC: UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tblFeedback;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
- (IBAction)btnSubmitClicked:(id)sender;

@end
