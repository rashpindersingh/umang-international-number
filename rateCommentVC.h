//
//  rateCommentVC.h
//  Umang
//
//  Created by admin on 09/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface rateCommentVC : UIViewController
{
    IBOutlet UIImageView *serviceImg;
    IBOutlet UILabel *lbl_serviceName;
    IBOutlet UIView *vw_popup;
    IBOutlet UIImageView *img_smily;
    IBOutlet UITextField *txt_addcomment;
    IBOutlet UIButton *btn_submit;
}
@property(nonatomic,retain)NSString *comment;
@property(nonatomic,retain)NSString *ratedBy;
@property(nonatomic,retain)NSDictionary *dictionary;
-(IBAction)btnSubmitAction:(id)sender;
@property(nonatomic)void(^ratingUpdate)(NSString *rating);
@end
