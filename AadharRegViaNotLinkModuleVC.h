//
//  AadharRegViaNotLinkModuleVC.h
//  RegistrationProcess
//
//  Created by admin on 07/01/17.
//  Copyright © 2017 SpiceLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AadharRegViaNotLinkModuleVC : UIViewController<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btn_acceptTerm;
@property (weak, nonatomic) IBOutlet UIButton *btnSkip;

@property(nonatomic,retain)NSString *str_aadhaar_Value;
@property(nonatomic,retain)NSString *tagFrom;

//----- added for passing rty and tout-----
@property(nonatomic,retain)NSString *rtry;
@property(assign)int tout;
//----- added for passing rty and tout-----

@end
