//
//  FilterServicesBO.h
//  Umang
//
//  Created by admin on 9/9/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface FilterServicesBO : NSObject

@property(nonatomic,strong) NSString *filterType;
@property(nonatomic,strong) NSString *strHeaderName;
@property(nonatomic,strong) NSString *serviceImage;
@property(nonatomic,strong) NSString *strDescription;
@property(nonatomic,strong) NSString *serviceName;
@property(nonatomic,strong) NSString *strImg;
@property(nonatomic,assign) BOOL isServiceSelected;


@end
