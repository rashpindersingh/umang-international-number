//
//  Constant.h
//  Umang
//
//  Created by spice on 15/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#ifndef Umang_Constant_h
#define Umang_Constant_h

#import "AppDelegate.h"

#define fDeviceWidth ([UIScreen mainScreen].bounds.size.width)
#define fDeviceHeight ([UIScreen mainScreen].bounds.size.height)
#define DEFAULT_BLUE [UIColor colorWithRed:12.0/255.0 green:118.0/255.0 blue:157.0/255.0 alpha:1.0]



#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)



#define KEY_PREFERED_LOCALE                 @"PreferedLocale"
#define TXT_LANGUAGE_ENGLISH                @"en"
//#define TXT_LANGUAGE_HINDI                  @"hi-IN"


#define APP_DELEGATE ((AppDelegate*)[[UIApplication sharedApplication]delegate])
#define kDigiLockerStoryBoard                     @"DigiLocker"
#define kDetailServiceStoryBoard                  @"DetailService"
#define kMainStoryBoard                           @"Main"


//Google Tracking on screens
#define HOME         @"Umang Home"
#define LANGUAGE_SELECTION_SCREEN         @"Umang LanguageSelection"
#define MOBILE_REGISTRATION_SCREEN   @"Umang MobileRegistration"
#define MOBILE_REGISTRATION_OTP_SCREEN    @"Umang MobileRegistrationOTP"
#define MOBILE_REGISTRATION_SET_MPIN   @"Umang MobileRegistrationSetMpin"
#define AADHAR_REGISTRATION_FROM_MOBILE_LOGIN  @"AadharRegistrationFromMobileLogin"
#define AADHAR_OTP_VERIFY_AFTER_MOBILE_LOGIN     @"AadharOtpVerifyAfterMobileLogin"

#define AADHAR_REGISTRATION_SCREEN     @"AadharRegistration"
#define AADHAR_MODULE_OTP_VERIFY     @"AadharModuleOtpVerify"
#define AADHAR_REGISTRATION_SET_MPIN     @"AadharRegistrationSetMpin"


#define AADHAR_REGISTRATION_VIA_NOT_LINKED     @"AadharRegistrationViaNotLinked"
#define AADHAR_OTP_VIA_NOT_LINKED     @"AadharOTPViaNotLinked"


#define ADVANCE_SEARCH_SCREEN     @"Umang AdvanceSearch"
#define ALTERNATE_MOBILE_SCREEN     @"Umang AlternateMobileScreen"
#define ADD_EMAIL_SCREEN      @"Umang AddEmailScreen"


#define EDIT_USER_INFO_SCREEN  @"EditUserInfoScreen"
#define UMANG_PROFILE_SCREEN    @"UmangProfileScreen"
#define UMANG_UPD_MPIN_SCREEN    @"UmangUPDMpinScreen"
#define VERIFY_ALTERNATE_MOBILE_SCREEN    @"VerifyAlternateMobileScreen"
#define SERVICE_DIRECTORY_DETAIL_SCREEN    @"ServiceDirectoryDetailScreen"
#define VERIFY_ALTERNATE_MOBILE_SCREEN    @"VerifyAlternateMobileScreen"
#define EDIT_PROFILE_INFO_SCREEN    @"EditProfileInfoScreen"
#define LOGIN_APP_SCREEN    @"LoginAppScreen"
#define FAQ_WEB_SCREEN    @"FaqWebScreen"
#define HELP_SETTING_SCREEN    @"HelpSettingScreen"
#define PROFILE_FOR_REGISTRATION_WITH_AADHAR  @"ProfileForRegistrationWithAadhar"
#define PROFILE_FROM_MOBILE_REGISTRATION  @"ProfileFromMobileRegistration"
#define TRANSACTIONAL_SEARCH_SCREEN  @"TransactionalSearchScreen"
#define TRANSACTIONAL_FILTER_SCREEN  @"TransactionalFilterScreen"
#define TRANSACTIONAL_HISTORY_SCREEN  @"TransactionalHistoryScreen"

#define ADHAAR_REGISTRATION_SCREEN  @"AadharRegistrationScreen"
#define FEEDBACK_SCREEN  @"FeedbackScreen"
#define SOCIAL_MEDIA_SCREEN  @"SocialMediaScreen"
#define AADHAR_CARD_SCREEN  @"AadharCardScreen"
#define RATE_US_SCREEN  @"RateUsScreen"
#define NOT_LINKED_AADHAR_SCREEN  @"NotLinkedAadharScreen"
#define ALL_SERVICE_TAB_SCREEN  @"AllServiceTabScreen"
#define CHOOSE_REGISTRATION_TYPE_SCREEN  @"ChooseRegistrationTypeScreen"
#define CUSTOM_PICKER_SCREEN  @"CustomPickerScreen"
#define EDIT_PROFILE_SCREEN  @"EditProfileScreen"
#define ENTER_REGISTRATION_FIELD_SCREEN  @"EnterRegistrationFieldScreen"
#define NOTIFICATION_FILTER_SCREEN  @"NotificationFilterScreen"
#define HELP_VIEW_SCREEN  @"HelpViewScreen"
#define HINT_VIEW_SCREEN  @"HintViewScreen"
#define HOME_DETAIL_SCREEN  @"HomeDetailScreen"
#define ITEM_MORE_INFO_SCREEN  @"ItemMoreScreen"
#define MORE_TAB_SCREEN  @"MoreTabScreen"
#define MY_PROFILE_SCREEN  @"MyProfileScreen"
#define NEAR_ME_SCREEN     @"NearMeScreen"
#define FILTER_SCREEN     @"FilterScreen"
#define NOTIFICATION_SEARCH_SCREEN     @"NotificationSearchScreen"
#define REGISTRATION_SCREEN     @"RegistrationScreen"
#define REGISTRATION_OTP_SCREEN     @"RegistrationOtpScreen"
#define REGISTRATION_MPIN_SCREEN     @"RegistrationMpinScreen"
#define REGISTRATION_PROFILE_SCREEN     @"RegistrationProfileScreen"
#define NOTIFICATION_SCREEN     @"NotifiactionScreen"
#define SEARCH_FILTER_SCREEN     @"SearchFilterScreen"
#define SETTING_NOTIFICATION_SCREEN     @"SettingNotificationScreen"
#define SECURITY_SETTING_SCREEN     @"SecuritySettingScreen"
#define SETTING_SCREEN     @"SettingScreen"
#define SHOW_MORE_SERVICE_SCREEN     @"ShowMoreServiceScreen"
#define FAVORITE_TAB_SCREEN     @"FavoriteTabScreen"
#define DETAIL_SERVICE_SCREEN     @"DetailServiceScreen"
#define SERVICE_CALL_SCREEN      @"ServiceCallScreen"
#define TAB_SCREEN      @"TabScreen"
#define LOGIN_WITH_OTP_SCREEN      @"LoginWithOTPScreen"
#define FORGOT_MPIN_SCREEN      @"ForgotMpinScreen"
#define CHANGE_MPIN_SCREEN      @"ChangeMpinScreen"
#define UPDATE_MPIN_SCREEN      @"UpdateMpinScreen"
#define DIGILOCKER_SCREEN      @"DigilockerScreen"
#define DIGILOCKER_WEB_SCREEN      @"DigilockerWebScreen"
#define USER_PROFILE_SCREEN      @"UserProfileScreen"
#define USER_EDIT_PROFILE_SCREEN      @"UserEditProfileScreen"
#define NOTIFICATION_CHAT_MESSAGE     @"NOTIFICATION_CHAT_MESSAGE"



#endif
