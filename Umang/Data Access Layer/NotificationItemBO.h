//
//  NotificationItemBO.h
//  Umang
//
//  Created by admin on 9/26/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum {
 NOTIFICATION_PROMOTIONAL = 100,
 NOTIFICATION_TRANSACTIONAL,
 NOTIFICATION_NONE
}NOTIFICATION_TYPE;


@interface NotificationItemBO : NSObject

@property(nonatomic,strong)NSString *headerTitle;
@property(nonatomic,strong)NSString *headerDesc;
@property(nonatomic,strong)NSString *date;

@property(nonatomic,strong)NSString *currentTime;
@property(nonatomic,strong)NSString *notification_id;
@property(nonatomic,assign)BOOL noti_IsFav;
@property(nonatomic,strong)NSString *noti_ScreenName;
@property(nonatomic,strong)NSString *noti_ScreenState;
@property(nonatomic,strong)NSString *noti_WebPagetitle;
@property(nonatomic,strong)NSString *noti_Service_id;
@property(nonatomic,assign) BOOL isServiceSelected;



@property(nonatomic,strong)NSString *ID;

@property(nonatomic,strong)UIImage  *imgIcon;
@property(nonatomic,strong)NSString *imgURL;
@property(nonatomic,strong)NSString *imgName;


@property(nonatomic,assign)NSString *notificationStatusPT;
@property(nonatomic,assign)NOTIFICATION_TYPE notificationStatus;
@property(nonatomic,assign)NSInteger statusType;
@property(nonatomic,assign)NSString *statusTypeForServices;




//NotificationSetting
@property(nonatomic,strong)NSString *titleService;
@property(nonatomic,strong)NSString *imgService;
@property(nonatomic,strong)NSString *idService;
@property(nonatomic,strong)NSString *btnService;




@end
