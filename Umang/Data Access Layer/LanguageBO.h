//
//  LanguageBO.h
//  Umang
//
//  Created by admin on 19/09/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LanguageBO : NSObject


@property(nonatomic,strong) NSString *languageName;
@property(nonatomic,strong) NSString *localeName;

//@property(nonatomic,strong) NSString *serviceImageURL;
@property(nonatomic,assign) BOOL isLanguageSelected;

@end
