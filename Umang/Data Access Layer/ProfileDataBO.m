//
//  ProfileDataBO.m
//  Umang
//
//  Created by admin on 12/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "ProfileDataBO.h"


@implementation AadharProfileBO

-(AadharProfileBO *)initWithResponse : (id)response
{
    if ([response isKindOfClass:[NSDictionary class]]) {
        
        self.aadhar_number = [self checkNullValue:[response valueForKey:@"aadhr"]];
        self.aadhar_image_url = [self checkNullValue:[response valueForKey:@"consent_approve_pic_path"]];
        self.pincode = [self checkNullValue:[response valueForKey:@"uidai_ldata_pc"]];
        self.dob = [self checkNullValue:[response valueForKey:@"uidai_pia_dob"]];
        self.email = [self checkNullValue:[response valueForKey:@"uidai_pia_email"]];
        self.gender = [self checkNullValue:[response valueForKey:@"uidai_pia_gender"]];
        self.address = [self checkNullValue:[response valueForKey:@"adhraddr"]];

        self.mobile_number = [self checkNullValue:[response valueForKey:@"uidai_pia_phone"]];
        self.name = [self checkNullValue:[response valueForKey:@"uidai_pia_name"]];
        self.father_name = [self checkNullValue:[response valueForKey:@"uidai_poa_co"]];
        self.district = [self checkNullValue:[response valueForKey:@"uidai_poa_dist"]];

        self.state = [self checkNullValue:[response valueForKey:@"uidai_poa_state"]];
        self.street = [self checkNullValue:[response valueForKey:@"uidai_poa_street"]];
        self.subdistrict = [self checkNullValue:[response valueForKey:@"uidai_poa_subdist"]];
        self.vtc = [self checkNullValue:[response valueForKey:@"uidai_poa_vtc"]];
        self.vtc_code = [self checkNullValue:[response valueForKey:@"uidai_poa_vtccode"]];

    }
    return self;
}
-(NSString*)checkNullValue:(id)text
{
    NSString *parsedText = @"";
    if([text isKindOfClass:[NSString class]])
    {
        if([text isEqualToString:@"<null>"] || [text isEqualToString:@"(null)"])
            parsedText = @"";
        else{
            parsedText = text;
        }
    }
    else if ([text isKindOfClass:[NSNumber class]]){
        parsedText = [text stringValue];
    }
    return parsedText;
}


@end


@implementation GeneralProfileBO

-(GeneralProfileBO *)initWithResponse : (id)response
{
    if ([response isKindOfClass:[NSDictionary class]]) {
        self.aadhar_number = [self checkNullValue:[response valueForKey:@"aadhr"]];
        self.image_url = [self checkNullValue:[response valueForKey:@"pic"]];
        self.pincode = [self checkNullValue:[response valueForKey:@"pincode"]];
        self.dob = [self checkNullValue:[response valueForKey:@"dob"]];
        self.email = [self checkNullValue:[response valueForKey:@"email"]];
        self.gender = [self checkNullValue:[response valueForKey:@"gndr"]];
        self.name = [self checkNullValue:[response valueForKey:@"nam"]];
        
        self.district = [self checkNullValue:[response valueForKey:@"dist"]];
        self.state = [self checkNullValue:[response valueForKey:@"st"]];
        self.s_id = [self checkNullValue:[response valueForKey:@"sid"]];
        self.status = [self checkNullValue:[response valueForKey:@"status"]];
        
    }
    return self;
}

-(NSString*)checkNullValue:(id)text
{
    NSString *parsedText = @"";
    if([text isKindOfClass:[NSString class]])
    {
        if([text isEqualToString:@"<null>"])
            parsedText = @"";
        else{
            parsedText = text;
        }
    }
    else if ([text isKindOfClass:[NSNumber class]]){
        parsedText = [text stringValue];
    }
    return parsedText;
}


@end


@implementation SocialProfileBO

-(SocialProfileBO *)initWithResponse : (id)response
{
    if ([response isKindOfClass:[NSDictionary class]]) {
        self.fb_id = [self checkNullValue:[response valueForKey:@"fbid"]];
        self.fb_name = [self checkNullValue:[response valueForKey:@"fbname"]];
        self.fb_image = [self checkNullValue:[response valueForKey:@"fbimg"]];
        self.google_id = [self checkNullValue:[response valueForKey:@"goid"]];
        self.google_name = [self checkNullValue:[response valueForKey:@"goname"]];
        self.google_image = [self checkNullValue:[response valueForKey:@"goimg"]];
        self.twitter_id = [self checkNullValue:[response valueForKey:@"twitid"]];
        
        self.twitter_name = [self checkNullValue:[response valueForKey:@"twitname"]];
        self.twitter_image = [self checkNullValue:[response valueForKey:@"twitimg"]];
    }
    return self;
}


-(NSString*)checkNullValue:(id)text
{
    NSString *parsedText = @"";
    if([text isKindOfClass:[NSString class]])
    {
        if([text isEqualToString:@"<null>"])
            parsedText = @"";
        else{
            parsedText = text;
        }
    }
    else if ([text isKindOfClass:[NSNumber class]]){
        parsedText = [text stringValue];
    }
    return parsedText;
}


@end


@implementation ProfileDataBO

-(ProfileDataBO *)initWithResponse : (id)response
{
    
    
    if ([response isKindOfClass:[NSDictionary class]]) {
        self.objAadhar = [[AadharProfileBO alloc] initWithResponse:[response objectForKey:@"aadharpd"]];
        self.objSocial = [[SocialProfileBO alloc] initWithResponse:[response objectForKey:@"socialpd"]];
        self.objGeneral = [[GeneralProfileBO alloc] initWithResponse:[response objectForKey:@"generalpd"]];
        self.user_token = [self checkNullValue:[response valueForKey:@"tkn"]];
        

        
        
            }
    return self;
}

-(NSString*)checkNullValue:(id)text
{
    NSString *parsedText = @"";
    if([text isKindOfClass:[NSString class]])
    {
        if([text isEqualToString:@"<null>"])
            parsedText = @"";
        else{
            parsedText = text;
        }
    }
    else if ([text isKindOfClass:[NSNumber class]]){
        parsedText = [text stringValue];
    }
    return parsedText;
}

@end
