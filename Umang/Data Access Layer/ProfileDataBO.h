//
//  ProfileDataBO.h
//  Umang
//
//  Created by admin on 12/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AadharProfileBO : NSObject

@property (nonatomic,strong) NSString *aadhar_number;
@property (nonatomic,strong) NSString *aadhar_image_url;
@property (nonatomic,strong) NSString *pincode;
@property (nonatomic,strong) NSString *dob;
@property (nonatomic,strong) NSString *email;
@property (nonatomic,strong) NSString *gender;
@property (nonatomic,strong) NSString *mobile_number;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *district;
@property (nonatomic,strong) NSString *father_name;
@property (nonatomic,strong) NSString *address;
@property (nonatomic,strong) NSString *state;
@property (nonatomic,strong) NSString *street;
@property (nonatomic,strong) NSString *subdistrict;
@property (nonatomic,strong) NSString *vtc;
@property (nonatomic,strong) NSString *vtc_code;


@end

@interface GeneralProfileBO : NSObject

@property (nonatomic,strong) NSString *aadhar_number;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *image_url;
@property (nonatomic,strong) NSString *pincode;
@property (nonatomic,strong) NSString *dob;
@property (nonatomic,strong) NSString *email;
@property (nonatomic,strong) NSString *gender;
@property (nonatomic,strong) NSString *state;
@property (nonatomic,strong) NSString *city;
@property (nonatomic,strong) NSString *district;
@property (nonatomic,strong) NSString *s_id;
@property (nonatomic,strong) NSString *status;


@end

@interface SocialProfileBO : NSObject
@property (nonatomic,strong) NSString *fb_id;
@property (nonatomic,strong) NSString *fb_name;
@property (nonatomic,strong) NSString *fb_image;

@property (nonatomic,strong) NSString *google_id;
@property (nonatomic,strong) NSString *google_name;
@property (nonatomic,strong) NSString *google_image;

@property (nonatomic,strong) NSString *twitter_id;
@property (nonatomic,strong) NSString *twitter_name;
@property (nonatomic,strong) NSString *twitter_image;

@end



@interface ProfileDataBO : NSObject

@property (nonatomic,strong) NSString *user_token;
@property (nonatomic,strong) AadharProfileBO *objAadhar;
@property (nonatomic,strong) GeneralProfileBO *objGeneral;
@property (nonatomic,strong) SocialProfileBO *objSocial;


-(ProfileDataBO *)initWithResponse : (id)response;



@end
