 //
//  UMAPIManager.m
//  Umang
//
//  Created by admin on 14/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//


#import "UMAPIManager.h"
#import "SharedManager.h"
#import "LoginAppVC.h"
#import <Security/Security.h>
#import "SecKeyWrapper.h"
#import "AppConstants.h"
#import <sys/utsname.h>

#define API_Context @"/umang/coreapi/ws2"
//#define API_Context @"/umang/coreapiint/ws1"

@import MobileCoreServices;

@implementation ApiMode
- (instancetype)initCustomize
{
    self = [super init];
    if (self)
    {
     NSString *bundleIdentifier = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
        if([bundleIdentifier isEqualToString:@"in.gov.umang.negd.g2c.staging"]) {
            self.WSO2PathBASE = @"https://stagingapp.umang.gov.in";
            self.UMAuthorization = @"Bearer  9f43b495-8c2d-3b18-b4be-a27badfc0cd3";
            self.PACKAGE_NAME = @"in.gov.umang.negd.g2c.staging";
            self.ChatBaseUrl = @"https://stgreporting.umang.gov.in";
            self.RefreshTokenBearer = @"Bearer f6854d92-da1b-3562-9066-4238aa4f69e7";
            self.DigiLockerBearer = @"Bearer fcb0bef2-5464-315f-a339-e0bae67dec74";
            self.DepartmentBearerToken = @"Bearer c3aefaa0-5a1b-36d4-9e9f-eace669e7925";
            self.GOOGLE_SIGNIN_CLIENT_ID = @"616074837787-vep6mtjtctu3i1pufcndo16u8sv08h0l.apps.googleusercontent.com";
            self.GOOGLE_SCHEME = @"com.googleusercontent.apps.616074837787-vep6mtjtctu3i1pufcndo16u8sv08h0l";
            self.XMPP_SERVER_HOST = @"staging.umang.gov.in";
            
        }
        else if ([bundleIdentifier isEqualToString:@"in.gov.umang.negd.g2c"])
        {
            self.WSO2PathBASE = @"https://app.umang.gov.in";
            self.UMAuthorization = @"Bearer  fdd7e307-a1ed-3337-82a3-48e65eb33bbe";
            self.PACKAGE_NAME = @"in.gov.umang.negd.g2c";
            self.ChatBaseUrl = @"https://reporting.umang.gov.in";
            self.RefreshTokenBearer = @"Bearer 49b08c8b-5ebc-31a0-8cab-b1d211ff4a0c";
            self.DigiLockerBearer = @"Bearer 3a0f8e45-cb57-398c-929a-b748d033e42c";
            self.DepartmentBearerToken = @"Bearer c3aefaa0-5a1b-36d4-9e9f-eace669e7925";
            self.GOOGLE_SIGNIN_CLIENT_ID = @"616074837787-2sk6q2enilnp1gvv77nuh8ffenvrvv28.apps.googleusercontent.com";
             self.GOOGLE_SCHEME = @"com.googleusercontent.apps.616074837787-2sk6q2enilnp1gvv77nuh8ffenvrvv28";
            self.XMPP_SERVER_HOST = @"reporting.umang.gov.in";

        }
    else if ([bundleIdentifier isEqualToString:@"in.gov.umang.negd.g2c.development"])
        {
            self.WSO2PathBASE = @"https://devapp.umang.gov.in";
            self.UMAuthorization = @"Bearer 7a6368ab-2259-3360-8ee0-99019eb3587e";
            self.PACKAGE_NAME = @"in.gov.umang.negd.g2c.development";
            self.ChatBaseUrl = @"https://devreporting.umang.gov.in";
            self.RefreshTokenBearer = @"Bearer b0961437-05ff-33ca-aee9-1a0126b86bd7";
            self.DigiLockerBearer = @"Bearer 33ed7109-9c12-3c84-a02e-e7b26f47a04d";
            self.DepartmentBearerToken = @"Bearer c3aefaa0-5a1b-36d4-9e9f-eace669e7925";
            self.GOOGLE_SIGNIN_CLIENT_ID = @"616074837787-vep6mtjtctu3i1pufcndo16u8sv08h0l.apps.googleusercontent.com";
            self.GOOGLE_SCHEME = @"com.googleusercontent.apps.616074837787-vep6mtjtctu3i1pufcndo16u8sv08h0l";
            self.XMPP_SERVER_HOST = @"devreporting.umang.gov.in";
        }
        
    }
    return self;
}



@end
@interface UMAPIManager()<NSURLSessionDelegate>
{
    NSDateFormatter *_dateFormat;
    SharedManager *  singleton ;
}

@end


@implementation UMAPIManager


/*
 
 API work only if
 
 response jsonstring will be MD5 then its
 header value of
 match with X-REQUEST-VALU
 else its a fake request
 
 jsonEncryptedRequestBody=[NSString stringWithFormat:@"%@",[str MD5]];
 
 
 Dear ALL,
 
 Hope backward response spoofing check has been implemented at APP end(android ios windows)
 
 Response (Generation at api end and validation at app level)
 API will send checksum in header
 X-REQUEST-VALUE <checksum>
 Calculate check in following order using MD5 alog and validate at app end
 <request Json>|<salt>
 
 SAVE SALT VALUE IN .library
 
 
 */

- (NSString*) deviceName
{
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    
    static NSDictionary* deviceNamesByCode = nil;
    
    if (!deviceNamesByCode) {
        
        deviceNamesByCode = @{@"i386"      : @"Simulator",
                              @"x86_64"    : @"Simulator",
                              @"iPod1,1"   : @"iPod Touch",        // (Original)
                              @"iPod2,1"   : @"iPod Touch",        // (Second Generation)
                              @"iPod3,1"   : @"iPod Touch",        // (Third Generation)
                              @"iPod4,1"   : @"iPod Touch",        // (Fourth Generation)
                              @"iPod7,1"   : @"iPod Touch",        // (6th Generation)
                              @"iPhone1,1" : @"iPhone",            // (Original)
                              @"iPhone1,2" : @"iPhone",            // (3G)
                              @"iPhone2,1" : @"iPhone",            // (3GS)
                              @"iPad1,1"   : @"iPad",              // (Original)
                              @"iPad2,1"   : @"iPad 2",            //
                              @"iPad3,1"   : @"iPad",              // (3rd Generation)
                              @"iPhone3,1" : @"iPhone 4",          // (GSM)
                              @"iPhone3,3" : @"iPhone 4",          // (CDMA/Verizon/Sprint)
                              @"iPhone4,1" : @"iPhone 4S",         //
                              @"iPhone5,1" : @"iPhone 5",          // (model A1428, AT&T/Canada)
                              @"iPhone5,2" : @"iPhone 5",          // (model A1429, everything else)
                              @"iPad3,4"   : @"iPad",              // (4th Generation)
                              @"iPad2,5"   : @"iPad Mini",         // (Original)
                              @"iPhone5,3" : @"iPhone 5c",         // (model A1456, A1532 | GSM)
                              @"iPhone5,4" : @"iPhone 5c",         // (model A1507, A1516, A1526 (China), A1529 | Global)
                              @"iPhone6,1" : @"iPhone 5s",         // (model A1433, A1533 | GSM)
                              @"iPhone6,2" : @"iPhone 5s",         // (model A1457, A1518, A1528 (China), A1530 | Global)
                              @"iPhone7,1" : @"iPhone 6 Plus",     //
                              @"iPhone7,2" : @"iPhone 6",          //
                              @"iPhone8,1" : @"iPhone 6S",         //
                              @"iPhone8,2" : @"iPhone 6S Plus",    //
                              @"iPhone8,4" : @"iPhone SE",         //
                              @"iPhone9,1" : @"iPhone 7",          //
                              @"iPhone9,3" : @"iPhone 7",          //
                              @"iPhone9,2" : @"iPhone 7 Plus",     //
                              @"iPhone9,4" : @"iPhone 7 Plus",     //
                              @"iPhone10,1": @"iPhone 8",          // CDMA
                              @"iPhone10,4": @"iPhone 8",          // GSM
                              @"iPhone10,2": @"iPhone 8 Plus",     // CDMA
                              @"iPhone10,5": @"iPhone 8 Plus",     // GSM
                              @"iPhone10,3": @"iPhone X",          // CDMA
                              @"iPhone10,6": @"iPhone X",          // GSM
                              
                              @"iPad4,1"   : @"iPad Air",          // 5th Generation iPad (iPad Air) - Wifi
                              @"iPad4,2"   : @"iPad Air",          // 5th Generation iPad (iPad Air) - Cellular
                              @"iPad4,4"   : @"iPad Mini",         // (2nd Generation iPad Mini - Wifi)
                              @"iPad4,5"   : @"iPad Mini",         // (2nd Generation iPad Mini - Cellular)
                              @"iPad4,7"   : @"iPad Mini",         // (3rd Generation iPad Mini - Wifi (model A1599))
                              @"iPad6,7"   : @"iPad Pro (12.9)", // iPad Pro 12.9 inches - (model A1584)
                              @"iPad6,8"   : @"iPad Pro (12.9)", // iPad Pro 12.9 inches - (model A1652)
                              @"iPad6,3"   : @"iPad Pro (9.7)",  // iPad Pro 9.7 inches - (model A1673)
                              @"iPad6,4"   : @"iPad Pro (9.7n)"   // iPad Pro 9.7 inches - (models A1674 and A1675)
                              };
    }
    
    NSString* deviceName = [deviceNamesByCode objectForKey:code];
    
    if (!deviceName) {
        // Not found on database. At least guess main device type from string contents:
        
        if ([code rangeOfString:@"iPod"].location != NSNotFound) {
            deviceName = @"iPod Touch";
        }
        else if([code rangeOfString:@"iPad"].location != NSNotFound) {
            deviceName = @"iPad";
        }
        else if([code rangeOfString:@"iPhone"].location != NSNotFound){
            deviceName = @"iPhone";
        }
        else {
            deviceName = @"Unknown";
        }
    }
    
    return deviceName;
}

-(void)clearValueOnlogout
{
    singleton = [SharedManager sharedSingleton];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:NO forKey:@"LOGIN_KEY"];
    [defaults synchronize];
    
    //------------------------- Encrypt Value------------------------
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    // Encrypt
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"TOKEN_KEY"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"USER_PIC"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"PROFILE_COMPELTE_KEY"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    //------------------------- Encrypt Value------------------------
    //——Remove Sharding Value——
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"NODE_KEY"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"jsonStringtoSave"];
    
    //----- remove sharding--------
    
    
    
    singleton.profileUserAddress = @"";
    singleton.imageLocalpath=@"";
    singleton.notiTypeGenderSelected=@"";
    singleton.profileNameSelected =@"";
    singleton.profilestateSelected=@"";
    singleton.notiTypeCitySelected=@"";
    singleton.notiTypDistricteSelected=@"";
    singleton.profileDOBSelected=@"";
    singleton.altermobileNumber=@"";
    singleton.user_Qualification=@"";
    singleton.user_Occupation=@"";
    singleton.user_profile_URL=@"";
    singleton.profileEmailSelected=@"";
    singleton.mobileNumber=@"";
    singleton.user_id=@"";
    singleton.user_tkn=@"";
    singleton.user_mpin=@"";
    singleton.user_aadhar_number=@"";
    singleton.objUserProfile = nil;
    singleton.imageLocalpath=@"";
    singleton.arr_initResponse = nil;
    singleton.stateSelected = @"";
    
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"AccessTokenDigi"];
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"RefreshTokenDigi"];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"N" forKey:@"ChatSession"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ABBR_KEY"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"EMB_STR"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"lastFetchV1"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"AllTabState"];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Enable_ServiceDir"];
   

    @try {
        [singleton.arr_recent_service removeAllObjects];
        
    } @catch (NSException *exception)
    {
        
    } @finally {
        
    }
    
    
    [[NSUserDefaults standardUserDefaults] setInteger:kLoginScreenCase forKey:kInitiateScreenKey];
    
    
    
    //------------------------- Encrypt Value------------------------
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    // Encrypt
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"lastFetchDate"];
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"lastFetchV1"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"USER_ID"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"TOKEN_KEY"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"USER_PIC"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"PROFILE_COMPELTE_KEY"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"lastDirectoryFetchDate"];
    //delete service directory
    [singleton.dbManager deleteServicesDirectory];
    //------------------------- Encrypt Value------------------------
    
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSHTTPCookie *cookie;
    for (cookie in [storage cookies])
    {
        
        [storage deleteCookie:cookie];
        
    }
    NSMutableArray *cookieArray = [[NSMutableArray alloc] init];
    [[NSUserDefaults standardUserDefaults] setValue:cookieArray forKey:@"cookieArray"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //——Remove Sharding Value——
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"NODE_KEY"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"InitAPIResponse"];
    
    // Logout from social frameworks as well.
    
    
    //------------------------- Encrypt Value------------------------
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    // Encrypt
    [[NSUserDefaults standardUserDefaults] encryptValue:@"NO" withKey:@"LINKDIGILOCKERSTATUS"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"digilocker_username"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"digilocker_password"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //------------------------- Encrypt Value------------------------
    
    [singleton.dbManager deleteBannerHomeData];
    [singleton.dbManager  deleteAllServices];
    [singleton.dbManager  deleteSectionData];
    [singleton.dbManager deleteBannerStateData];
    
    singleton.imageLocalpath=@"";
    singleton.dbManager = [[UMSqliteManager alloc] initWithDatabaseFilename:@"UMANG_DATABASE.db"];
    [singleton.dbManager createUmangDB];
    // Start the notifier, which will cause the reachability object to retain itself!
    
    //[[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SELECTED_TAB_INDEX"];
    
    [defaults synchronize];
    
    
    
}

-(void)openLoginView
{
    //==== Alertviewcontroller added===========
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"session_expire_msg", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction *alertActionConfirm = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                         
                                         {
                                             
                                             [self openLoginScreen];//login screen open on 403
                                             
                                         }];
    
    [alertController addAction:alertActionConfirm];
    UIViewController *vc1=[self topMostController];

    [vc1 presentViewController:alertController animated:YES completion:nil];
}


    
    


-(void)openLoginScreen
{
    
    singleton = [SharedManager sharedSingleton];
    
    [self  clearValueOnlogout];
    
    // Start the notifier, which will cause the reachability object to retain itself!
    [singleton.reach stopNotifier];
    /*  UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     LoginViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
     [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
     [self presentViewController:vc animated:YES completion:nil];
     */
    
    //----later add
    [[NSUserDefaults standardUserDefaults] setInteger:kLoginScreenCase forKey:kInitiateScreenKey];
    [[NSUserDefaults standardUserDefaults] setBool:false forKey:kKeepMeLoggedIn];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //------
    
    
    UIStoryboard *storyboard =  [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    LoginAppVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginAppVC"];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        vc = [[LoginAppVC alloc] initWithNibName:@"LoginAppVC_iPad" bundle:nil];
    }
    
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    
    
    UIViewController *vc1=[self topMostController];
    
    
    [vc1 presentViewController:vc animated:NO completion:nil];
}


- (UIStoryboard *)grabStoryboard {
    
    // determine screen size
    int screenHeight = [UIScreen mainScreen].bounds.size.height;
    UIStoryboard *storyboard;
    
    switch (screenHeight) {
            
            // iPhone 4s
        case 480:
            storyboard = [UIStoryboard storyboardWithName:@"Main-4s" bundle:nil];
            break;
            
            // iPhone 5s
        case 568:
            storyboard = [UIStoryboard storyboardWithName:@"Main-5" bundle:nil];
            break;
            
            // iPhone 6
        case 667:
            storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            break;
            
            // iPhone 6 Plus
        case 736:
            storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            break;
            
        default:
            // it's an iPad
            storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            break;
    }
    
    return storyboard;
}




//-------- Get top Most view to add network status view-----
- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

-(void)hitWebServiceSearchPostMethod:(BOOL)isPost isAccessTokenRequired:(BOOL)tokenRequired webServiceURL:(NSString *)apiURL withBody:(id)requestBody andTag:(REQUEST_TAG)tag completionHandler:(APICompletionBlock)completionBlock
{
    singleton = [SharedManager sharedSingleton];

    /*
     **********REQUEST BODY**********
     X-REQUEST-VALUE  <request Json>|<salt> MD5    R%d&Wst676#(Na
     */
    NSString *jsonEncryptedRequestBody = @"";
    NSString *jsonInputStringAESEncrypted = @"";
    if (requestBody) {
        
        // Add common params here only
        [requestBody addEntriesFromDictionary:[self getCommonParametersForRequestBody]];
        
        //===========================================
        //Start new added for international parameter
        //===========================================
        [requestBody addEntriesFromDictionary:[self getInternationalparameter:tag]];
        //===========================================
        //End new added for international parameter
        //===========================================
        

        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:requestBody options:0 error:nil];
        NSString *bodyString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        
        //---------------------------------------------------------------
        //  CONVERT INPUT JSON TO AES ENCRYPTION
        //---------------------------------------------------------------
        jsonInputStringAESEncrypted=[self encryptInputJson:bodyString];
        //NSLog(@"Encrypted JsonInputStr=%@",jsonInputStringAESEncrypted);
        
        NSString *strSaltRequestValue = SaltRequestVaue;//[self getKeyWithTag:KEYCHAIN_SaltRequestVaue];

        NSString *str = [NSString stringWithFormat:@"%@|%@",bodyString,strSaltRequestValue];
        // Convert and print the MD5 value to the console
        jsonEncryptedRequestBody=[NSString stringWithFormat:@"%@",[str MD5]];
        
        
    }
    
    
    /*
     ********** X-REQUEST-TSTAMP  format yyyyMMddHHmmss**********
     //  X-REQUEST-CONTROL  | X-REQUEST-TSTAMP|salt|  SHA256  R%d&Wst676#(Na
     
     */
    NSString *timeStamp = [self getTimeStamp];
    NSString *saltRequestControl =  SaltRequestControl;//[self getKeyWithTag:KEYCHAIN_SaltRequestControl];

    NSString *encrytSHA256=[NSString stringWithFormat:@"|%@|%@|",timeStamp,saltRequestControl];
    NSString *timeStampEncrypted=[encrytSHA256 sha256HashFor:encrytSHA256];
    
    
    NSString *authToken = @"";
    // Append auth token with Bearer
    if (tokenRequired)
    {
        // Digilocker authorirzation
        authToken = @"Bearer 33ed7109-9c12-3c84-a02e-e7b26f47a04d";
    }
    //---fix lint ----
    NSLog(@"authToken=%@",authToken);
    NSString *requestURL=[NSString stringWithFormat:@"%@%@%@",singleton.apiMode.WSO2PathBASE,API_Context,apiURL];
    
    
    //  NSString *requestURL=[NSString stringWithFormat:@"%@:%@%@",WSO2PathBASE,WSO2_Port,apiURL];
    //NSLog(@" requestURL =%@",requestURL);
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestURL]  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    
    NSData *requestData=[jsonInputStringAESEncrypted dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPMethod:isPost?@"POST":@"GET"];
    [request setHTTPBody: requestData];
    [request setValue:@"text/plain" forHTTPHeaderField:API_CONTENT_TYPE];
    [request setValue:timeStampEncrypted forHTTPHeaderField:API_REQUEST_CONTROL];
    [request setValue:jsonEncryptedRequestBody forHTTPHeaderField:API_REQUEST_VALUE];
    [request setValue:timeStamp forHTTPHeaderField:API_REQUEST_TIME_STAMP];
    [request setValue:singleton.apiMode.UMAuthorization forHTTPHeaderField:API_REQUEST_AUTHORIZATION];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString*  responseString;
            
            if (!error) {
                //NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                //NSLog(@" headers =%@",headers);
                
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                //NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
                
                if([httpResponse statusCode]==403)
                    
                {
                    
                    [self openLoginView];
                    return ;
                }
                
                
                
                responseString=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                
                
                //NSLog(@" response =%@",responseString);
                
                
                
                NSError *errorResponse;
                
                if([responseString length]==0)
                {
                    NSError* error = nil;
                    
                    NSMutableDictionary* details = [NSMutableDictionary dictionary];
                    [details setValue:@"Invalid Response Value,please retry" forKey:NSLocalizedDescriptionKey];
                    // populate the error object with the details
                    error = [NSError errorWithDomain:@"Umang" code:200 userInfo:details];
                    
                    //invalid  not equal
                    completionBlock(nil,error,tag);
                }
                NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
                id jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                  options:kNilOptions
                                                                    error:&errorResponse];
                
                //NSLog(@" jsonResponse =%@",jsonResponse);
                
                if (errorResponse == nil) {
                    // Success Case
                    if ([[jsonResponse objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[jsonResponse objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                    {
                        completionBlock(jsonResponse,errorResponse,tag);
                    }
                    else
                    {
                        NSString *errorString = [jsonResponse objectForKey:@"rd"];
                        if ([errorString isKindOfClass:[NSNull class]] || errorString.length == 0 || [errorString isEqualToString:@"null"]) {
                            errorString = @"Unexpected error occured. Please try after some time.";
                        }
                        
                        NSMutableDictionary* details = [NSMutableDictionary dictionary];
                        [details setValue:errorString forKey:NSLocalizedDescriptionKey];
                        
                        NSError *serverError = [NSError errorWithDomain:@"UMANG SERVER ERROR" code:200 userInfo:details];
                        
                        completionBlock(nil,serverError,tag);
                    }
                    
                }
                else{
                    
                    
                    completionBlock(nil,errorResponse,tag);
                }
                
            }//close else
            else{
                completionBlock(nil,error,tag);
            }
            
        });
    }];
    
    [postDataTask resume];
}


////////////////////====================================================>>>>>>>


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:@"star_umang_gov_in" ofType:@"cer"];
    NSData *localCertData = [NSData dataWithContentsOfFile:cerPath];
    
    
    if ([remoteCertificateData isEqualToData:localCertData]) {
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
    }
    else {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
    }
    
    
}


- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler
{
    NSString *authMethod = [[challenge protectionSpace] authenticationMethod];
    
    if ([authMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
        completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
    } else {
        SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
        SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
        NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
        
        NSString *cerPath = [[NSBundle mainBundle] pathForResource:@"star_umang_gov_in" ofType:@"cer"];
        NSData *localCertData = [NSData dataWithContentsOfFile:cerPath];
        NSURLCredential *credential;
        
        if ([remoteCertificateData isEqualToData:localCertData]) {
            credential = [NSURLCredential credentialForTrust:serverTrust];
            [[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
        }
        else {
            [[challenge sender] cancelAuthenticationChallenge:challenge];
        }
        
        
        
        completionHandler(NSURLSessionAuthChallengeUseCredential, credential);
        //NSLog(@"Finished Challenge");
    }
}



////////////////////====================================================>>>>>>>


- (void)refreshTokenAuth:(NSString*)apiURL withBody:(NSString*)requestBody andTag:(REQUEST_TAG)tag  withCompletionHandler:(void (^)(NSString *))handler
{
    
    
    singleton = [SharedManager sharedSingleton];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"lang"];//lang is Status of the account
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [dictBody setObject:@"" forKey:@"st"];  //get from mobile default email //not supported iphone
    
    [self hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_TUK withBody:dictBody andTag:TAG_REQUEST_TUK completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        // [hud hideAnimated:YES];
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            // NSString *accessToken = [response valueForKey:@"access_token"];
            //completionAuthHandler(accessToken);
            NSString *accessToken =[NSString stringWithFormat:@"Bearer  %@",[response valueForKey:@"access_token"]];
            
            //NSString *responsereturn=[self hitAPIRefreshBearerToken];
            //------------------------- Encrypt Value------------------------
            [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
            // Encrypt
            [[NSUserDefaults standardUserDefaults] encryptValue:accessToken withKey:@"BEARERTOKEN"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            //------------------------- Encrypt Value------------------------
            handler(accessToken);
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            
        }
        
    }];
    
    
}



-(void)hitWebServiceAPIWithPostMethod:(BOOL)isPost isAccessTokenRequired:(BOOL)tokenRequired webServiceURL:(NSString *)apiURL withBody:(id)requestBody andTag:(REQUEST_TAG)tag completionHandler:(APICompletionBlock)completionBlock
{
    //[self hitAPIRefreshBearerToken];
    /*
     **********REQUEST BODY**********
     X-REQUEST-VALUE  <request Json>|<salt> MD5    R%d&Wst676#(Na
     */
    singleton = [SharedManager sharedSingleton];

    NSString *jsonEncryptedRequestBody = @"";
    NSString *jsonInputStringAESEncrypted = @"";
    if (requestBody) {
        
        // Add common params here only
        [requestBody addEntriesFromDictionary:[self getCommonParametersForRequestBody]];
        
        //===========================================
        //Start new added for international parameter
        //===========================================
        [requestBody addEntriesFromDictionary:[self getInternationalparameter:tag]];
        //===========================================
        //End new added for international parameter
        //===========================================
        
        

        
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:requestBody options:0 error:nil];
        NSString *bodyString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        
        //---------------------------------------------------------------
        //  CONVERT INPUT JSON TO AES ENCRYPTION
        //---------------------------------------------------------------
        jsonInputStringAESEncrypted=[self encryptInputJson:bodyString];
        //NSLog(@"Encrypted JsonInputStr=%@",jsonInputStringAESEncrypted);
        
        NSString *strSaltRequestValue = SaltRequestVaue;//[self getKeyWithTag:KEYCHAIN_SaltRequestVaue];

        NSString *str = [NSString stringWithFormat:@"%@|%@",bodyString,strSaltRequestValue];
        // Convert and print the MD5 value to the console
        jsonEncryptedRequestBody=[NSString stringWithFormat:@"%@",[str MD5]];
        
        
    }
    
    
    /*
     ********** X-REQUEST-TSTAMP  format yyyyMMddHHmmss**********
     //  X-REQUEST-CONTROL  | X-REQUEST-TSTAMP|salt|  SHA256  R%d&Wst676#(Na
     
     */
    NSString *timeStamp = [self getTimeStamp];
    NSString *saltRequestControl = SaltRequestControl;//[self getKeyWithTag:KEYCHAIN_SaltRequestControl];

    NSString *encrytSHA256=[NSString stringWithFormat:@"|%@|%@|",timeStamp,saltRequestControl];
    NSString *timeStampEncrypted=[encrytSHA256 sha256HashFor:encrytSHA256];
    
    tokenRequired=YES;
    
    NSString *authToken = @"";
    // Append auth token with Bearer
    if (tokenRequired)
    {
        
        authToken = [[NSUserDefaults standardUserDefaults]
                     decryptedValueForKey:@"BEARERTOKEN"];;
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    NSLog(@"authToken=%@",authToken);
    
    
    NSString *requestURL=[NSString stringWithFormat:@"%@%@%@",singleton.apiMode.WSO2PathBASE,API_Context,apiURL];
    
    
    //  NSString *requestURL=[NSString stringWithFormat:@"%@:%@%@",WSO2PathBASE,WSO2_Port,apiURL];
    NSLog(@" requestURL =%@",requestURL);
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestURL]  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    
    NSData *requestData=[jsonInputStringAESEncrypted dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPMethod:isPost?@"POST":@"GET"];
    [request setHTTPBody: requestData];
    [request setValue:@"text/plain" forHTTPHeaderField:API_CONTENT_TYPE];
    [request setValue:timeStampEncrypted forHTTPHeaderField:API_REQUEST_CONTROL];
    [request setValue:jsonEncryptedRequestBody forHTTPHeaderField:API_REQUEST_VALUE];
    [request setValue:timeStamp forHTTPHeaderField:API_REQUEST_TIME_STAMP];
    [request setValue:singleton.apiMode.UMAuthorization forHTTPHeaderField:API_REQUEST_AUTHORIZATION];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString*  responseString;
            //            NSString *JsonOutStr=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            //
            //            responseString=[self decryptionAction:JsonOutStr];
            
            NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
            //NSLog(@" headers =%@",headers);
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            //NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
            
            if([httpResponse statusCode]==403)
                
            {
                singleton.nccode=@""; //blank in case of 403
                // start code to handle error of 403 for tour
               if(tag==TAG_REQUEST_NEWTOUR_IMAGE)
               {
                   NSInteger errorCode=[httpResponse statusCode];
                NSMutableDictionary* details = [NSMutableDictionary dictionary];
                [details setValue:NSLocalizedString(@"network_error_txt",nil)  forKey:NSLocalizedDescriptionKey];
                NSError * serverError = [NSError errorWithDomain:NSLocalizedString(@"network_error_txt",nil) code:errorCode userInfo:details];
                completionBlock(nil,serverError,tag);

               }
                // END code to handle error of 403 for tour //remove else condition from here for old check

                else
                {
                NSLog(@"API tag =%u",tag);
                [self openLoginView];
                }
                return ;
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            if([httpResponse statusCode]==401)
                
            {
                [self refreshTokenAuth:apiURL withBody:requestBody andTag:tag withCompletionHandler:^(NSString* returnString)
                 {
                     
                     NSLog(@"Received access token: %@", returnString);
                     
                     
                     [self hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:YES webServiceURL:apiURL withBody:requestBody andTag:tag completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
                      {
                          
                          NSLog(@"======>>><<<====== Response = %@",response);
                          
                      }];
                     
                     
                 }];
                
                
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            else  if([httpResponse statusCode]==200)
            {
                
                if (!error) {
                    
                    
                    NSString *JsonOutStr=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    
                    NSLog(@"JsonOutStr is empty bahubali");
                    responseString=[self decryptionAction:JsonOutStr];
                    
                    NSString *strSaltRequestValue = SaltRequestVaue;//[self getKeyWithTag:KEYCHAIN_SaltRequestVaue];

                    NSString *jsonResponsewithSalt = [NSString stringWithFormat:@"%@|%@",responseString,strSaltRequestValue];
                    // Convert and print the MD5 value to the console
                    NSString* jsonResponsewithSaltMD5=[NSString stringWithFormat:@"%@",[jsonResponsewithSalt MD5]];
                    
                    //NSLog(@" jsonResponsewithSaltMD5 =%@",jsonResponsewithSaltMD5);
                    //NSLog(@" X-REQUEST-VALUE Response =%@",[headers valueForKey:@"X-REQUEST-VALUE"]);
                    
                    // jsonResponsewithSaltMD5=@"7c01cb28b17ef1a20d610f3a764c0dd7";//test case to be check
                    if (![jsonResponsewithSaltMD5 isEqualToString:[headers valueForKey:@"X-REQUEST-VALUE"]])
                    {
                        
                        NSError* error = nil;
                        
                        NSMutableDictionary* details = [NSMutableDictionary dictionary];
                        //[details setValue:@"Invalid Request Value" forKey:NSLocalizedDescriptionKey];
                        [details setValue:NSLocalizedString(@"network_error_txt",nil) forKey:NSLocalizedDescriptionKey];
                        
                        // populate the error object with the details
                        error = [NSError errorWithDomain:@"Umang" code:200 userInfo:details];
                        
                        //invalid  not equal
                        completionBlock(nil,error,tag);
                        
                    }
                    else
                        
                    {
                        //valid
                        
                        
                        
                        //NSLog(@" response =%@",responseString);
                        
                        
                        NSInteger errorCode=[httpResponse statusCode];
                        NSError *errorResponse;
                        
                        if([responseString length]==0)
                        {
                            NSError* error = nil;
                            
                            NSMutableDictionary* details = [NSMutableDictionary dictionary];
                            // [details setValue:@"Invalid Response Value,please retry" forKey:NSLocalizedDescriptionKey];
                            
                            [details setValue:NSLocalizedString(@"network_error_txt",nil) forKey:NSLocalizedDescriptionKey];
                            
                            
                            
                            // populate the error object with the details
                            error = [NSError errorWithDomain:@"Umang" code:errorCode userInfo:details];
                            
                            //invalid  not equal
                            completionBlock(nil,error,tag);
                        }
                        NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
                        id jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                          options:kNilOptions
                                                                            error:&errorResponse];
                        
                        //NSLog(@" jsonResponse =%@",jsonResponse);
                        
                        if (errorResponse == nil) {
                            // Success Case
                            if ([[jsonResponse objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[jsonResponse objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                            {
                                completionBlock(jsonResponse,errorResponse,tag);
                            }
                            else
                            {
                                NSString *errorString = [jsonResponse objectForKey:@"rd"];
                                if ([errorString isKindOfClass:[NSNull class]] || errorString.length == 0 || [errorString isEqualToString:@"null"]) {
                                    // errorString = @"Unexpected error occured. Please try after some time.";
                                    errorString = NSLocalizedString(@"network_error_txt",nil);
                                    
                                    
                                    
                                }
                                
                                NSMutableDictionary* details = [NSMutableDictionary dictionary];
                                [details setValue:errorString forKey:NSLocalizedDescriptionKey];
                                
                                //NSError *serverError = [NSError errorWithDomain:@"Unexpected error occured. Please try after some time" code:errorCode userInfo:details];
                                
                                NSError *serverError = [NSError errorWithDomain: NSLocalizedString(@"network_error_txt",nil) code:errorCode userInfo:details];
                                
                                
                                
                                completionBlock(jsonResponse,serverError,tag);
                                //completionBlock(jsonResponse,errorResponse,tag);
                                
                            }
                            
                        }
                        else{
                            
                            
                            completionBlock(nil,errorResponse,tag);
                        }
                    }
                }//close else
                
                else{
                    completionBlock(nil,error,tag);
                }
                
            }
            else //all other codes
            {
                NSLog(@"Error Occured = %@",error.localizedDescription);
                /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                 message:error.localizedDescription
                 delegate:self
                 cancelButtonTitle:NSLocalizedString(@"ok", nil)
                 otherButtonTitles:nil];
                 [alert show];
                 
                 */
                NSError *serverError=error;
                
                
                NSString *string =[NSString stringWithFormat:@"%@",error.localizedDescription];
                if ([string rangeOfString:@"The request timed out"].location == NSNotFound  )
                    //||[string rangeOfString:@"Could not connect to the server"].location == NSNotFound
                {
                    NSLog(@"string dont contains!");
                    
                } else
                {
                    NSInteger errorCode=[httpResponse statusCode];
                    
                    NSMutableDictionary* details = [NSMutableDictionary dictionary];
                    [details setValue:NSLocalizedString(@"network_error_txt",nil)  forKey:NSLocalizedDescriptionKey];
                    
                    serverError = [NSError errorWithDomain:NSLocalizedString(@"network_error_txt",nil) code:errorCode userInfo:details];
                    
                    
                    //completionBlock(nil,error,tag);
                    
                }
                
                
                
                completionBlock(nil,serverError,tag);
                
                //completionBlock(nil,error,tag);
            }
        });
    }];
    
    [postDataTask resume];
}

#pragma mark - Department API

-(void)hitWebServiceDeptAPIWithPostMethodForDept:(NSString *)deptName andIsPost:(BOOL)isPost isAccessTokenRequired:(BOOL)tokenRequired webServiceURL:(NSString *)apiURL withBody:(id)requestBody andTag:(REQUEST_TAG)tag completionHandler:(APICompletionBlock)completionBlock
{
    
    NSString *bodyString = @"";
    
    if (requestBody)
    {
        
        //[requestBody addEntriesFromDictionary:[self getCommonParametersForRequestBody]];
        
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:requestBody options:0 error:nil];
        bodyString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    tokenRequired=YES;
    
    NSString *authToken = @"";
    
    if (tokenRequired)
    {
        
        
        if ([deptName isEqualToString:@"epfo"])
        {
            authToken = [[NSUserDefaults standardUserDefaults]
                         valueForKey:@"BEARERDEPTTOKENEPFO"];
        }
        else if ([deptName isEqualToString:@"nps"])
        {
            authToken = [[NSUserDefaults standardUserDefaults]
                         valueForKey:@"BEARERDEPTTOKENNPS"];
        }
        
        if (authToken.length == 0)
        {
            authToken = @"Bearer c3aefaa0-5a1b-36d4-9e9f-eace669e7925";
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    NSLog(@"authToken=%@",authToken);
    
    
    NSString *requestURL=[NSString stringWithFormat:@"%@%@",singleton.apiMode.WSO2PathBASE,apiURL];
    
    
    //  NSString *requestURL=[NSString stringWithFormat:@"%@:%@%@",WSO2PathBASE,WSO2_Port,apiURL];
    NSLog(@" requestURL =%@",requestURL);
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestURL]  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    NSInteger randomID = arc4random() % 9000000000000000 + 1000000000000000;
    
    
    NSData *requestData=[bodyString dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPMethod:isPost?@"POST":@"GET"];
    [request setHTTPBody: requestData];
    [request setValue:@"application/json" forHTTPHeaderField:API_CONTENT_TYPE];
    [request setValue:authToken forHTTPHeaderField:API_REQUEST_AUTHORIZATION];
    [request setValue:[NSString stringWithFormat:@"%ld",randomID] forHTTPHeaderField:@"requestid"];
    
    
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString*  responseString;
            NSString *JsonOutStr=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            responseString=[self decryptionAction:JsonOutStr];
            
            NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
            //NSLog(@" headers =%@",headers);
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            //NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
            
            if([httpResponse statusCode]==403)
            {
                [self openLoginView];
                return ;
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            if([httpResponse statusCode]==401)
                
            {
                
                
                [self refreshTokenDeptAuthForDept:deptName andUrlString:UM_API_REFRESH_DEPT withBody:requestBody andTag:tag withCompletionHandler:^(NSString *returnString)
                 {
                     
                     NSLog(@"Received access token: %@", returnString);
                     
                     //[[NSUserDefaults standardUserDefaults] setValue:returnString forKey:@"BEARERDEPTTOKEN"];
                     
                     
                     NSString *jsonStringtoSave=[[NSUserDefaults standardUserDefaults] objectForKey:@"jsonStringtoSave"];;
                     
                     [[NSUserDefaults standardUserDefaults]  synchronize];
                     
                     NSString *uid = @"";
                     
                     if([jsonStringtoSave length]!=0)
                         
                     {
                         
                         NSData *data = [jsonStringtoSave dataUsingEncoding:NSUTF8StringEncoding];
                         
                         id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                         
                         NSDictionary *epfoData_dic=[json valueForKey:@"epfoData"];
                         NSDictionary *npsData_dic = [json valueForKey:@"npsData"];
                         
                         if (epfoData_dic != nil)
                         {
                             
                             NSDictionary *value_dic=[epfoData_dic valueForKey:@"value"];
                             uid=[value_dic valueForKey:@"uid"];
                         }
                         
                         if (npsData_dic != nil)
                         {
                             
                             NSDictionary *value_dic=[epfoData_dic valueForKey:@"value"];
                             uid=[value_dic valueForKey:@"userid"];
                         }
                         
                     }
                     
                     
                     NSMutableDictionary *dictBody = [NSMutableDictionary new];
                     
                     
                     if ([deptName isEqualToString:@"nps"])
                     {
                         double CurrentTime = CACurrentMediaTime();
                         
                         NSString * timeInMS = [NSString stringWithFormat:@"%f", CurrentTime];
                         
                         timeInMS=  [timeInMS stringByReplacingOccurrencesOfString:@"." withString:@""];
                         
                         [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
                         
                         [dictBody setObject:timeInMS forKey:@"trkr"];
                         [dictBody setObject:@"12" forKey:@"usag"];
                         [dictBody setObject:@"12" forKey:@"usrid"];
                         [dictBody setObject:@"12" forKey:@"srvid"];
                         [dictBody setObject:@"app" forKey:@"pltfrm"];
                         
                         [dictBody setObject:@"2065" forKey:@"lac"];
                         [dictBody setObject:@"" forKey:@"lat"];
                         [dictBody setObject:@"" forKey:@"lon"];
                         [dictBody setObject:@"1234567890" forKey:@"imei"];
                         
                         
                         
                         [dictBody setObject:@"12" forKey:@"did"];
                         [dictBody setObject:@"ios" forKey:@"os"];
                         [dictBody setObject:@"" forKey:@"acesstkn"];
                         [dictBody setObject:uid forKey:@"userid"];
                         [dictBody setObject:@"10.0" forKey:@"maver"];
                         [dictBody setObject:@"10.0" forKey:@"mosver"];
                         [dictBody setObject:singleton.user_id forKey:@"uid"];
                     }
                     else
                     {
                         double CurrentTime = CACurrentMediaTime();
                         
                         NSString * timeInMS = [NSString stringWithFormat:@"%f", CurrentTime];
                         
                         timeInMS=  [timeInMS stringByReplacingOccurrencesOfString:@"." withString:@""];
                         
                         [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
                         
                         [dictBody setObject:timeInMS forKey:@"trkr"];
                         [dictBody setObject:@"12" forKey:@"usag"];
                         [dictBody setObject:@"12" forKey:@"usrid"];
                         [dictBody setObject:@"12" forKey:@"srvid"];
                         [dictBody setObject:@"app" forKey:@"pltfrm"];
                         
                         [dictBody setObject:uid forKey:@"uan"];
                         [dictBody setObject:@"2065" forKey:@"lac"];
                         [dictBody setObject:@"" forKey:@"lat"];
                         [dictBody setObject:@"" forKey:@"lon"];
                         
                         NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
                         if (selectedLanguage == nil)
                         {
                             selectedLanguage = @"en";
                         }
                         else{
                             NSArray *arrTemp = [selectedLanguage componentsSeparatedByString:@"-"];
                             selectedLanguage = [arrTemp firstObject];
                         }
                         
                         
                         [dictBody setObject:selectedLanguage forKey:@"lang"];
                         
                         [dictBody setObject:@"app" forKey:@"mode"];
                     }
                     
                     
                     
                     [self hitWebServiceDeptAPIWithPostMethodForDept:deptName andIsPost:YES isAccessTokenRequired:YES webServiceURL:apiURL withBody:dictBody andTag:tag completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
                      {
                          
                          NSLog(@"======>>><<<====== Response = %@",response);
                          completionBlock(response,nil,tag);
                          
                      }];
                     
                 }];
                
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            else  if([httpResponse statusCode]==200)
            {
                
                if (!error) {
                    
                    
                    NSString *JsonOutStr=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    
                    NSLog(@"JsonOutStr is empty bahubali");
                    responseString=[self decryptionAction:JsonOutStr];
                    
                    NSInteger errorCode=[httpResponse statusCode];
                    NSError *errorResponse;
                    
                    if([responseString length]==0)
                    {
                        NSError* error = nil;
                        
                        NSMutableDictionary* details = [NSMutableDictionary dictionary];
                        // [details setValue:@"Invalid Response Value,please retry" forKey:NSLocalizedDescriptionKey];
                        
                        [details setValue:NSLocalizedString(@"network_error_txt",nil) forKey:NSLocalizedDescriptionKey];
                        
                        
                        
                        // populate the error object with the details
                        error = [NSError errorWithDomain:@"Umang" code:errorCode userInfo:details];
                        
                        //invalid  not equal
                        completionBlock(nil,error,tag);
                    }
                    NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
                    id jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                      options:kNilOptions
                                                                        error:&errorResponse];
                    
                    //NSLog(@" jsonResponse =%@",jsonResponse);
                    
                    if (errorResponse == nil) {
                        // Success Case
                        if ([[jsonResponse objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[jsonResponse objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                        {
                            completionBlock(jsonResponse,errorResponse,tag);
                        }
                        else
                        {
                            NSString *errorString = [jsonResponse objectForKey:@"rd"];
                            if ([errorString isKindOfClass:[NSNull class]] || errorString.length == 0 || [errorString isEqualToString:@"null"]) {
                                // errorString = @"Unexpected error occured. Please try after some time.";
                                errorString = NSLocalizedString(@"network_error_txt",nil);
                                
                                
                                
                            }
                            
                            NSMutableDictionary* details = [NSMutableDictionary dictionary];
                            [details setValue:errorString forKey:NSLocalizedDescriptionKey];
                            
                            //NSError *serverError = [NSError errorWithDomain:@"Unexpected error occured. Please try after some time" code:errorCode userInfo:details];
                            
                            NSError *serverError = [NSError errorWithDomain: NSLocalizedString(@"network_error_txt",nil) code:errorCode userInfo:details];
                            
                            
                            
                            completionBlock(jsonResponse,serverError,tag);
                            //completionBlock(jsonResponse,errorResponse,tag);
                            
                        }
                        
                    }
                    else{
                        
                        
                        completionBlock(nil,errorResponse,tag);
                    }
                    
                }
                
                else{
                    completionBlock(nil,error,tag);
                }
                
            }
            else //all other codes
            {
                NSLog(@"Error Occured = %@",error.localizedDescription);
                /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                 message:error.localizedDescription
                 delegate:self
                 cancelButtonTitle:NSLocalizedString(@"ok", nil)
                 otherButtonTitles:nil];
                 [alert show];
                 
                 */
                NSError *serverError=error;
                
                
                NSString *string =[NSString stringWithFormat:@"%@",error.localizedDescription];
                if ([string rangeOfString:@"The request timed out"].location == NSNotFound  )
                    //||[string rangeOfString:@"Could not connect to the server"].location == NSNotFound
                {
                    NSLog(@"string dont contains!");
                    
                } else
                {
                    NSInteger errorCode=[httpResponse statusCode];
                    
                    NSMutableDictionary* details = [NSMutableDictionary dictionary];
                    [details setValue:NSLocalizedString(@"network_error_txt",nil)  forKey:NSLocalizedDescriptionKey];
                    
                    serverError = [NSError errorWithDomain:NSLocalizedString(@"network_error_txt",nil) code:errorCode userInfo:details];
                    
                    
                    //completionBlock(nil,error,tag);
                    
                }
                
                
                
                completionBlock(nil,serverError,tag);
                
                //completionBlock(nil,error,tag);
            }
        });
    }];
    
    [postDataTask resume];
    
}


- (void)refreshTokenDeptAuthForDept:(NSString *)dept andUrlString:(NSString*)apiURL withBody:(NSString*)requestBody andTag:(REQUEST_TAG)tag  withCompletionHandler:(void (^)(NSString *))handler
{
    
    singleton = [SharedManager sharedSingleton];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"lang"];//lang is Status of the account
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [dictBody setObject:@"" forKey:@"st"];  //get from mobile default email //not supported iphone
    
    [self hitWebServiceAPIForRefreshDeptTokenForDept:dept andIsPost:YES isAccessTokenRequired:NO webServiceURL:apiURL withBody:requestBody andTag:tag completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            // NSString *accessToken = [response valueForKey:@"access_token"];
            //completionAuthHandler(accessToken);
            
            NSString *accessToken =[NSString stringWithFormat:@"Bearer  %@",[response valueForKey:@"access_token"]];
            
            
            if ([dept isEqualToString:@"epfo"])
            {
                //NSString *responsereturn=[self hitAPIRefreshBearerToken];
                //------------------------- Encrypt Value------------------------
                [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                // Encrypt
                [[NSUserDefaults standardUserDefaults] encryptValue:accessToken withKey:@"BEARERDEPTTOKENEPFO"];
                
                [[NSUserDefaults standardUserDefaults]setValue:accessToken forKey:@"BEARERDEPTTOKENEPFO"];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                //------------------------- Encrypt Value------------------------
            }
            else if ([dept isEqualToString:@"nps"])
            {
                //NSString *responsereturn=[self hitAPIRefreshBearerToken];
                //------------------------- Encrypt Value------------------------
                [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                // Encrypt
                
                [[NSUserDefaults standardUserDefaults] encryptValue:accessToken withKey:@"BEARERDEPTTOKENNPS"];
                
                [[NSUserDefaults standardUserDefaults]setValue:accessToken forKey:@"BEARERDEPTTOKENNPS"];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                //------------------------- Encrypt Value------------------------
                
            }
            
            handler(accessToken);
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            
        }
        
    }];
    
}

-(void)hitWebServiceAPIForRefreshDeptTokenForDept:(NSString *)dept andIsPost:(BOOL)isPost isAccessTokenRequired:(BOOL)tokenRequired webServiceURL:(NSString *)apiURL withBody:(id)requestBody andTag:(REQUEST_TAG)tag completionHandler:(APICompletionBlock)completionBlock
{
    NSString *jsonEncryptedRequestBody = @"";
    NSString *jsonInputStringAESEncrypted = @"";
    if (requestBody) {
        
        // Add common params here only
        [requestBody setObject:dept forKey:@"deptname"];
        [requestBody addEntriesFromDictionary:[self getCommonParametersForRequestBody]];
        //===========================================
        //Start new added for international parameter
        //===========================================
        [requestBody addEntriesFromDictionary:[self getInternationalparameter:tag]];
        //===========================================
        //End new added for international parameter
        //===========================================
        

        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:requestBody options:0 error:nil];
        NSString *bodyString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        
        //---------------------------------------------------------------
        //  CONVERT INPUT JSON TO AES ENCRYPTION
        //---------------------------------------------------------------
        jsonInputStringAESEncrypted=[self encryptInputJson:bodyString];
        //NSLog(@"Encrypted JsonInputStr=%@",jsonInputStringAESEncrypted);
        
        NSString *strSaltRequestValue = SaltRequestVaue;//[self getKeyWithTag:KEYCHAIN_SaltRequestVaue];

        NSString *str = [NSString stringWithFormat:@"%@|%@",bodyString,strSaltRequestValue];
        // Convert and print the MD5 value to the console
        jsonEncryptedRequestBody=[NSString stringWithFormat:@"%@",[str MD5]];
        
        
    }
    
    
    /*
     ********** X-REQUEST-TSTAMP  format yyyyMMddHHmmss**********
     //  X-REQUEST-CONTROL  | X-REQUEST-TSTAMP|salt|  SHA256  R%d&Wst676#(Na
     
     */
    NSString *timeStamp = [self getTimeStamp];
    NSString *saltRequestControl =  SaltRequestControl;//[self getKeyWithTag:KEYCHAIN_SaltRequestControl];

    NSString *encrytSHA256=[NSString stringWithFormat:@"|%@|%@|",timeStamp,saltRequestControl];
    NSString *timeStampEncrypted=[encrytSHA256 sha256HashFor:encrytSHA256];
    
    tokenRequired=YES;
    
    NSString *authToken = @"";
    // Append auth token with Bearer
    if (tokenRequired)
    {
        
        authToken = [[NSUserDefaults standardUserDefaults]
                     decryptedValueForKey:@"BEARERDEPTTOKEN"];
        
        if (authToken.length == 0)
        {
            authToken = @"Bearer c3aefaa0-5a1b-36d4-9e9f-eace669e7925";
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    NSLog(@"authToken=%@",authToken);
    
    
    NSInteger randomID = arc4random() % 9000000000000000 + 1000000000000000;
    
    
    NSString *requestURL=[NSString stringWithFormat:@"%@%@",singleton.apiMode.WSO2PathBASE,apiURL];
    
    
    //  NSString *requestURL=[NSString stringWithFormat:@"%@:%@%@",WSO2PathBASE,WSO2_Port,apiURL];
    NSLog(@" requestURL =%@",requestURL);
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestURL]  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    
    NSData *requestData=[jsonInputStringAESEncrypted dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPMethod:isPost?@"POST":@"GET"];
    [request setHTTPBody: requestData];
    [request setValue:@"text/plain" forHTTPHeaderField:API_CONTENT_TYPE];
    [request setValue:timeStampEncrypted forHTTPHeaderField:API_REQUEST_CONTROL];
    [request setValue:jsonEncryptedRequestBody forHTTPHeaderField:API_REQUEST_VALUE];
    [request setValue:timeStamp forHTTPHeaderField:API_REQUEST_TIME_STAMP];
    [request setValue:@"Bearer b0961437-05ff-33ca-aee9-1a0126b86bd7" forHTTPHeaderField:API_REQUEST_AUTHORIZATION];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setValue:[NSString stringWithFormat:@"%ld",randomID] forHTTPHeaderField:@"requestid"];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString*  responseString;
            NSString *JsonOutStr=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            responseString=[self decryptionAction:JsonOutStr];
            
            NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
            //NSLog(@" headers =%@",headers);
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            //NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
            
            if([httpResponse statusCode]==403)
                
            {
                //[self openLoginView];
                //return ;
            }
            else  if([httpResponse statusCode]==200)
            {
                
                if (!error)
                {
                    
                    
                    NSString *JsonOutStr=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    
                    responseString=[self decryptionAction:JsonOutStr];
                    
                    NSString *strSaltRequestValue = SaltRequestVaue;//[self getKeyWithTag:KEYCHAIN_SaltRequestVaue];

                    NSString *jsonResponsewithSalt = [NSString stringWithFormat:@"%@|%@",responseString,strSaltRequestValue];
                    // Convert and print the MD5 value to the console
                    NSString* jsonResponsewithSaltMD5=[NSString stringWithFormat:@"%@",[jsonResponsewithSalt MD5]];
                    
                    //NSLog(@" jsonResponsewithSaltMD5 =%@",jsonResponsewithSaltMD5);
                    //NSLog(@" X-REQUEST-VALUE Response =%@",[headers valueForKey:@"X-REQUEST-VALUE"]);
                    
                    // jsonResponsewithSaltMD5=@"7c01cb28b17ef1a20d610f3a764c0dd7";//test case to be check
                    
                    
                    if (![jsonResponsewithSaltMD5 isEqualToString:[headers valueForKey:@"X-REQUEST-VALUE"]])
                    {
                        
                        NSError* error = nil;
                        
                        NSMutableDictionary* details = [NSMutableDictionary dictionary];
                        //[details setValue:@"Invalid Request Value" forKey:NSLocalizedDescriptionKey];
                        [details setValue:NSLocalizedString(@"network_error_txt",nil) forKey:NSLocalizedDescriptionKey];
                        
                        // populate the error object with the details
                        error = [NSError errorWithDomain:@"Umang" code:200 userInfo:details];
                        
                        //invalid  not equal
                        completionBlock(nil,error,tag);
                        
                    }
                    else
                        
                    {
                        //valid
                        
                        
                        
                        //NSLog(@" response =%@",responseString);
                        
                        
                        NSInteger errorCode=[httpResponse statusCode];
                        NSError *errorResponse;
                        
                        if([responseString length]==0)
                        {
                            NSError* error = nil;
                            
                            NSMutableDictionary* details = [NSMutableDictionary dictionary];
                            // [details setValue:@"Invalid Response Value,please retry" forKey:NSLocalizedDescriptionKey];
                            
                            [details setValue:NSLocalizedString(@"network_error_txt",nil) forKey:NSLocalizedDescriptionKey];
                            
                            
                            
                            // populate the error object with the details
                            error = [NSError errorWithDomain:@"Umang" code:errorCode userInfo:details];
                            
                            //invalid  not equal
                            completionBlock(nil,error,tag);
                        }
                        NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
                        id jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                          options:kNilOptions
                                                                            error:&errorResponse];
                        
                        //NSLog(@" jsonResponse =%@",jsonResponse);
                        
                        if (errorResponse == nil)
                        {
                            // Success Case
                            if ([jsonResponse objectForKey:@"access_token"])
                            {
                                completionBlock(jsonResponse,errorResponse,tag);
                            }
                            else
                            {
                                NSString *errorString = [jsonResponse objectForKey:@"rd"];
                                if ([errorString isKindOfClass:[NSNull class]] || errorString.length == 0 || [errorString isEqualToString:@"null"]) {
                                    // errorString = @"Unexpected error occured. Please try after some time.";
                                    errorString = NSLocalizedString(@"network_error_txt",nil);
                                    
                                    
                                    
                                }
                                
                                NSMutableDictionary* details = [NSMutableDictionary dictionary];
                                [details setValue:errorString forKey:NSLocalizedDescriptionKey];
                                
                                //NSError *serverError = [NSError errorWithDomain:@"Unexpected error occured. Please try after some time" code:errorCode userInfo:details];
                                
                                NSError *serverError = [NSError errorWithDomain: NSLocalizedString(@"network_error_txt",nil) code:errorCode userInfo:details];
                                
                                
                                
                                completionBlock(jsonResponse,serverError,tag);
                                //completionBlock(jsonResponse,errorResponse,tag);
                                
                            }
                            
                        }
                        else{
                            
                            
                            completionBlock(nil,errorResponse,tag);
                        }
                    }
                }//close else
                
                else{
                    completionBlock(nil,error,tag);
                }
                
            }
            else //all other codes
            {
                NSLog(@"Error Occured = %@",error.localizedDescription);
                /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                 message:error.localizedDescription
                 delegate:self
                 cancelButtonTitle:NSLocalizedString(@"ok", nil)
                 otherButtonTitles:nil];
                 [alert show];
                 
                 */
                NSError *serverError=error;
                
                
                NSString *string =[NSString stringWithFormat:@"%@",error.localizedDescription];
                if ([string rangeOfString:@"The request timed out"].location == NSNotFound  )
                    //||[string rangeOfString:@"Could not connect to the server"].location == NSNotFound
                {
                    NSLog(@"string dont contains!");
                    
                } else
                {
                    NSInteger errorCode=[httpResponse statusCode];
                    
                    NSMutableDictionary* details = [NSMutableDictionary dictionary];
                    [details setValue:NSLocalizedString(@"network_error_txt",nil)  forKey:NSLocalizedDescriptionKey];
                    
                    serverError = [NSError errorWithDomain:NSLocalizedString(@"network_error_txt",nil) code:errorCode userInfo:details];
                    
                    
                    //completionBlock(nil,error,tag);
                    
                }
                
                
                
                completionBlock(nil,serverError,tag);
                
                //completionBlock(nil,error,tag);
            }
        });
    }];
    
    [postDataTask resume];
    
    
}



#pragma mark -


#pragma mark - DIGI Locker

-(void)getNewRefreshTokenDigiLocker:(BOOL)isPost isAccessTokenRequired:(BOOL)tokenRequired webServiceURL:(NSString *)apiURL withBody:(id)requestBody andTag:(REQUEST_TAG)tag completionHandler:(APICompletionBlock)completionBlock
{
    singleton = [SharedManager sharedSingleton];
    
    NSString *jsonEncryptedRequestBody = @"";
    NSString *jsonInputStringAESEncrypted = @"";
    if (requestBody) {
        [requestBody addEntriesFromDictionary:[self getCommonParametersForRequestBody]];
        
        //===========================================
        //Start new added for international parameter
        //===========================================
        [requestBody addEntriesFromDictionary:[self getInternationalparameter:tag]];
        //===========================================
        //End new added for international parameter
        //===========================================
        
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:requestBody options:0 error:nil];
        NSString *bodyString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        
        jsonInputStringAESEncrypted=[self encryptInputJsonDigilocker:bodyString];
        
        NSString *str = [NSString stringWithFormat:@"%@|%@",bodyString,@"D@GUM4NG$#4PP"];
        jsonEncryptedRequestBody=[NSString stringWithFormat:@"%@",[str MD5]];
    }
    
    NSString *timeStamp = [self getTimeStamp];
    NSString *saltRequestControl = SaltRequestControl;//[self getKeyWithTag:KEYCHAIN_SaltRequestControl];

    NSString *encrytSHA256=[NSString stringWithFormat:@"|%@|%@|",timeStamp,saltRequestControl];
    NSString *timeStampEncrypted=[encrytSHA256 sha256HashFor:encrytSHA256];
    
    
    NSString *authToken = @"";
    
    // Append auth token with Bearer
    if (tokenRequired)
    {
        // Digilocker authorirzation
        // authToken = @"Bearer 3a0f8e45-cb57-398c-929a-b748d033e4ll";
        
        
        authToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"DIGIAUTHREFRESHTOKEN"];
        if (authToken.length == 0)
        {
            authToken = @"Bearer 3a0f8e45-cb57-398c-929a-b748d033e4ll";
        }
    }
    
    NSInteger randomID = arc4random() % 9000000000000000 + 1000000000000000;
    
    
    NSString *requestURL=[NSString stringWithFormat:@"%@%@",singleton.apiMode.WSO2PathBASE,apiURL];
    
    NSLog(@" requestURL =%@",requestURL);
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestURL]  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    
    NSData *requestData=[jsonInputStringAESEncrypted dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPMethod:isPost?@"POST":@"GET"];
    [request setHTTPBody: requestData];
    [request setValue:@"text/plain" forHTTPHeaderField:API_CONTENT_TYPE];
    [request setValue:timeStampEncrypted forHTTPHeaderField:API_REQUEST_CONTROL];
    [request setValue:jsonEncryptedRequestBody forHTTPHeaderField:API_REQUEST_VALUE];
    [request setValue:timeStamp forHTTPHeaderField:API_REQUEST_TIME_STAMP];
    [request setValue:authToken forHTTPHeaderField:API_REQUEST_AUTHORIZATION];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setValue:[NSString stringWithFormat:@"%ld",randomID] forHTTPHeaderField:@"requestid"];
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            __block NSString*  responseString;
            NSString *JsonOutStr=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            responseString=[self decryptionActionDigiLocker:JsonOutStr];
            
            NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            
            if([httpResponse statusCode]==200)
            {
                
                if (!error)
                {
                    
                    
                    NSError *errorResponse;
                    NSInteger errorCode=[httpResponse statusCode];
                    
                    NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
                    id jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                      options:kNilOptions
                                                                        error:&errorResponse];
                    
                    //NSLog(@" jsonResponse =%@",jsonResponse);
                    
                    if (errorResponse == nil)
                    {
                        // Success Case
                        if ([[jsonResponse objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[jsonResponse objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                        {
                            completionBlock(jsonResponse,errorResponse,tag);
                        }
                        else
                        {
                            NSString *errorString = [jsonResponse objectForKey:@"rd"];
                            if ([errorString isKindOfClass:[NSNull class]] || errorString.length == 0 || [errorString isEqualToString:@"null"])
                            {
                                // errorString = @"Unexpected error occured. Please try after some time.";
                                errorString = NSLocalizedString(@"network_error_txt",nil);
                            }
                            
                            NSMutableDictionary* details = [NSMutableDictionary dictionary];
                            [details setValue:errorString forKey:NSLocalizedDescriptionKey];
                            
                            //NSError *serverError = [NSError errorWithDomain:@"Unexpected error occured. Please try after some time" code:errorCode userInfo:details];
                            
                            NSError *serverError = [NSError errorWithDomain: NSLocalizedString(@"network_error_txt",nil) code:errorCode userInfo:details];
                            
                            
                            
                            completionBlock(jsonResponse,serverError,tag);
                            //completionBlock(jsonResponse,errorResponse,tag);
                            
                        }
                        
                    }
                    else{
                        
                        
                        completionBlock(nil,errorResponse,tag);
                    }
                    
                    
                    
                    NSString *JsonOutStr=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    
                    responseString=[self decryptionAction:JsonOutStr];
                    
                    
                    NSString *strSaltReqDigiLockerValue = SaltRequestVaueDigiLocker;//[self getKeyWithTag:KEYCHAIN_SaltRequestVaueDigiLocker];

                    NSString *jsonResponsewithSalt = [NSString stringWithFormat:@"%@|%@",responseString,strSaltReqDigiLockerValue];
                    // Convert and print the MD5 value to the console
                    NSString* jsonResponsewithSaltMD5=[NSString stringWithFormat:@"%@",[jsonResponsewithSalt MD5]];
                    
                    if (![jsonResponsewithSaltMD5 isEqualToString:[headers valueForKey:@"X-REQUEST-VALUE"]])
                    {
                        
                        NSError* error = nil;
                        
                        NSMutableDictionary* details = [NSMutableDictionary dictionary];
                        //[details setValue:@"Invalid Request Value" forKey:NSLocalizedDescriptionKey];
                        [details setValue:NSLocalizedString(@"network_error_txt",nil) forKey:NSLocalizedDescriptionKey];
                        
                        // populate the error object with the details
                        error = [NSError errorWithDomain:@"Umang" code:200 userInfo:details];
                        
                        //invalid  not equal
                        completionBlock(nil,error,tag);
                        
                    }
                    else
                        
                    {
                        //valid
                        
                        
                        
                        //NSLog(@" response =%@",responseString);
                        
                        
                        
                        if([responseString length]==0)
                        {
                            NSError* error = nil;
                            
                            NSMutableDictionary* details = [NSMutableDictionary dictionary];
                            // [details setValue:@"Invalid Response Value,please retry" forKey:NSLocalizedDescriptionKey];
                            
                            [details setValue:NSLocalizedString(@"network_error_txt",nil) forKey:NSLocalizedDescriptionKey];
                            
                            
                            
                            // populate the error object with the details
                            error = [NSError errorWithDomain:@"Umang" code:errorCode userInfo:details];
                            
                            //invalid  not equal
                            completionBlock(nil,error,tag);
                        }
                    }
                }
                
                else
                {
                    completionBlock(nil,error,tag);
                }
                
            }
            else //all other codes
            {
                NSLog(@"Error Occured = %@",error.localizedDescription);
                
                NSError *serverError=error;
                
                
                NSString *string =[NSString stringWithFormat:@"%@",error.localizedDescription];
                if ([string rangeOfString:@"The request timed out"].location == NSNotFound  )
                    //||[string rangeOfString:@"Could not connect to the server"].location == NSNotFound
                {
                    NSLog(@"string dont contains!");
                    
                } else
                {
                    NSInteger errorCode=[httpResponse statusCode];
                    
                    NSMutableDictionary* details = [NSMutableDictionary dictionary];
                    [details setValue:NSLocalizedString(@"network_error_txt",nil)  forKey:NSLocalizedDescriptionKey];
                    
                    serverError = [NSError errorWithDomain:NSLocalizedString(@"network_error_txt",nil) code:errorCode userInfo:details];
                    
                    
                    //completionBlock(nil,error,tag);
                    
                }
                
                
                
                completionBlock(nil,serverError,tag);
                
                //completionBlock(nil,error,tag);
            }
        });
    }];
    
    [postDataTask resume];
    
    
}


-(void)hitAPIForDigiLockerAuthenticationWithPost:(BOOL)isPost isAccessTokenRequired:(BOOL)tokenRequired webServiceURL:(NSString *)apiURL withBody:(id)requestBody andTag:(REQUEST_TAG)tag completionHandler:(APICompletionBlock)completionBlock
{
    singleton = [SharedManager sharedSingleton];
    
    NSString *jsonEncryptedRequestBody = @"";
    NSString *jsonInputStringAESEncrypted = @"";
    if (requestBody) {
        [requestBody addEntriesFromDictionary:[self getCommonParametersForRequestBody]];
        
        
        //===========================================
        //Start new added for international parameter
        //===========================================
        [requestBody addEntriesFromDictionary:[self getInternationalparameter:tag]];
        //===========================================
        //End new added for international parameter
        //===========================================
        

        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:requestBody options:0 error:nil];
        NSString *bodyString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        
        jsonInputStringAESEncrypted=[self encryptInputJsonDigilocker:bodyString];
        
        NSString *str = [NSString stringWithFormat:@"%@|%@",bodyString,@"D@GUM4NG$#4PP"];
        jsonEncryptedRequestBody=[NSString stringWithFormat:@"%@",[str MD5]];
    }
    
    NSString *timeStamp = [self getTimeStamp];
    NSString *saltRequestControl =  SaltRequestControl;//[self getKeyWithTag:KEYCHAIN_SaltRequestControl];

    NSString *encrytSHA256=[NSString stringWithFormat:@"|%@|%@|",timeStamp,saltRequestControl];
    NSString *timeStampEncrypted=[encrytSHA256 sha256HashFor:encrytSHA256];
    
    
    NSString *authToken = @"";
    
    // Append auth token with Bearer
    if (tokenRequired)
    {
        // Digilocker authorirzation
        // authToken = @"Bearer 3a0f8e45-cb57-398c-929a-b748d033e4ll";
        
        [[NSUserDefaults standardUserDefaults] setValue:singleton.apiMode.DigiLockerBearer forKey:@"DIGIAUTHREFRESHTOKEN"];
        
        authToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"DIGIAUTHREFRESHTOKEN"];
        
        if (authToken.length == 0) {
            authToken = @"Bearer fcb0bef2-5464-315f-a339-e0bae67dec74";
        }
    }
    
    NSInteger randomID = arc4random() % 9000000000000000 + 1000000000000000;
    
    
    NSString *requestURL=[NSString stringWithFormat:@"%@%@",singleton.apiMode.WSO2PathBASE,apiURL];
    
    NSLog(@" requestURL =%@",requestURL);
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestURL]  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    
    NSData *requestData=[jsonInputStringAESEncrypted dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPMethod:isPost?@"POST":@"GET"];
    [request setHTTPBody: requestData];
    [request setValue:@"text/plain" forHTTPHeaderField:API_CONTENT_TYPE];
    [request setValue:timeStampEncrypted forHTTPHeaderField:API_REQUEST_CONTROL];
    [request setValue:jsonEncryptedRequestBody forHTTPHeaderField:API_REQUEST_VALUE];
    [request setValue:timeStamp forHTTPHeaderField:API_REQUEST_TIME_STAMP];
    [request setValue:authToken forHTTPHeaderField:API_REQUEST_AUTHORIZATION];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setValue:[NSString stringWithFormat:@"%ld",randomID] forHTTPHeaderField:@"requestid"];
    
    
    NSLog(@"REQUEST-CONTROL - %@",timeStampEncrypted);
    NSLog(@"REQUEST-VALUE - %@",jsonEncryptedRequestBody);
    NSLog(@"REQUEST-TSTAMP - %@",timeStamp);
    NSLog(@"Authorization - %@",authToken);
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            __block NSString*  responseString;
            NSString *JsonOutStr=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            responseString=[self decryptionActionDigiLocker:JsonOutStr];
            
            NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            //NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
            
            if([httpResponse statusCode]==403)
            {
                [self openLoginView];
                return ;
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            if([httpResponse statusCode]==401)
                
            {
                
                [self refreshBearerAuthDigilocker:apiURL withBody:requestBody andTag:tag withCompletionHandler:^(NSString* returnString)
                 {
                     
                     NSLog(@"Received access token: %@", returnString);
                     NSString *refreshAuthToken = [NSString stringWithFormat:@"Bearer %@",returnString];
                     [[NSUserDefaults standardUserDefaults] setObject:refreshAuthToken forKey:@"DIGIAUTHREFRESHTOKEN"];
                     
                     [self hitAPIForDigiLockerAuthenticationWithPost:YES isAccessTokenRequired:YES webServiceURL:apiURL withBody:requestBody andTag:tag completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
                      {
                          NSLog(@"%@",response);
                          
                          if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                          {
                              completionBlock(response,nil,tag);
                          }
                          else
                          {
                              NSString *errorString = [response objectForKey:@"rd"];
                              if ([errorString isKindOfClass:[NSNull class]] || errorString.length == 0 || [errorString isEqualToString:@"null"])
                              {
                                  // errorString = @"Unexpected error occured. Please try after some time.";
                                  errorString = NSLocalizedString(@"network_error_txt",nil);
                              }
                              
                              NSMutableDictionary* details = [NSMutableDictionary dictionary];
                              [details setValue:errorString forKey:NSLocalizedDescriptionKey];
                              
                              //NSError *serverError = [NSError errorWithDomain:@"Unexpected error occured. Please try after some time" code:errorCode userInfo:details];
                              
                              NSError *serverError = [NSError errorWithDomain: NSLocalizedString(@"network_error_txt",nil) code:200 userInfo:details];
                              
                              
                              completionBlock(response,serverError,tag);
                              //completionBlock(jsonResponse,errorResponse,tag);
                              
                          }
                          
                          
                      }];
                 }];
                
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            else  if([httpResponse statusCode]==200)
            {
                
                if (!error)
                {
                    
                    
                    NSError *errorResponse;
                    NSInteger errorCode=[httpResponse statusCode];
                    
                    NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
                    id jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                      options:kNilOptions
                                                                        error:&errorResponse];
                    
                    //NSLog(@" jsonResponse =%@",jsonResponse);
                    
                    if (errorResponse == nil)
                    {
                        // Success Case
                        if ([[jsonResponse objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[jsonResponse objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                        {
                            completionBlock(jsonResponse,errorResponse,tag);
                        }
                        else if ([[jsonResponse valueForKey:@"rs"] isEqualToString:@"F"] && [[jsonResponse valueForKey:@"rc"] isEqualToString:@"DGL0003"])
                        {
                            
                            if ([[jsonResponse valueForKey:@"rd"] isEqualToString:@"Aadhaar already registered. You can sign in using it."] || [[jsonResponse valueForKey:@"rd"] isEqualToString:@"Aadhaar service temporarily unavailable. Please try again. If problem persists, check back in some time. "] || [[jsonResponse valueForKey:@"rd"] isEqualToString:@"Your Authentication Failed"] || [[jsonResponse valueForKey:@"rd"] isEqualToString:@"Linked mobile number is not verified at Aadhaar."])
                            {
                                completionBlock(jsonResponse,errorResponse,tag);
                            }
                            else
                            {
                                NSMutableDictionary *dictBody = [NSMutableDictionary new];
                                
                                double CurrentTime = CACurrentMediaTime();
                                
                                NSString * timeInMS = [NSString stringWithFormat:@"%f", CurrentTime];
                                
                                timeInMS=  [timeInMS stringByReplacingOccurrencesOfString:@"." withString:@""];
                                
                                
                                
                                
                                NSString *oldRefreshToken = [[NSUserDefaults standardUserDefaults]valueForKey:@"RefreshTokenDigi"];
                                
                                oldRefreshToken = oldRefreshToken.length == 0 ? @"" : oldRefreshToken;
                                
                                [dictBody setObject:timeInMS forKey:@"trkr"];
                                [dictBody setObject:@"rgtadhr" forKey:@"ort"];
                                [dictBody setObject:@"Y" forKey:@"rc"];
                                [dictBody setObject:@"Y" forKey:@"mec"];
                                [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
                                [dictBody setObject:oldRefreshToken forKey:@"rtkn"];
                                
                                NSLog(@"Dictionary is %@",dictBody);
                                
                                [self getNewRefreshTokenDigiLocker:YES isAccessTokenRequired:YES webServiceURL:UM_API_REFRESH_TOKEN withBody:dictBody andTag:TAG_REQUEST_DIGILOCKER_GETREFRESHTOKEN completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
                                 {
                                     
                                     NSLog(@"%@",response);
                                     
                                     NSString *accessToken = [[response valueForKey:@"pd"] valueForKey:@"access_token"];
                                     NSString *refreshToken = [[response valueForKey:@"pd"] valueForKey:@"refresh_token"];
                                     
                                     accessToken = accessToken == (NSString *)[NSNull null] ? @"" : accessToken;
                                     refreshToken = refreshToken == (NSString *)[NSNull null] ? @"" : refreshToken;
                                     
                                     
                                     [[NSUserDefaults standardUserDefaults] setValue:accessToken forKey:@"AccessTokenDigi"];
                                     [[NSUserDefaults standardUserDefaults] setValue:refreshToken forKey:@"RefreshTokenDigi"];
                                     
                                     [requestBody setObject:accessToken forKey:@"utkn"];
                                     
                                     [self hitAPIForDigiLockerAuthenticationWithPost:YES isAccessTokenRequired:YES webServiceURL:apiURL withBody:requestBody andTag:tag completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
                                      {
                                          
                                          if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                                          {
                                              
                                              completionBlock(response,errorResponse,tag);
                                          }
                                          else
                                          {
                                              NSString *errorString = [jsonResponse objectForKey:@"rd"];
                                              if ([errorString isKindOfClass:[NSNull class]] || errorString.length == 0 || [errorString isEqualToString:@"null"])
                                              {
                                                  errorString = NSLocalizedString(@"network_error_txt",nil);
                                              }
                                              
                                              NSMutableDictionary* details = [NSMutableDictionary dictionary];
                                              [details setValue:errorString forKey:NSLocalizedDescriptionKey];
                                              
                                              
                                              NSError *serverError = [NSError errorWithDomain: NSLocalizedString(@"network_error_txt",nil) code:errorCode userInfo:details];
                                              
                                              
                                              
                                              completionBlock(jsonResponse,serverError,tag);
                                          }
                                          
                                          
                                      }];
                                     
                                     
                                 }];
                            }
                            
                            
                            
                        }
                        else
                        {
                            NSString *errorString = [jsonResponse objectForKey:@"rd"];
                            if ([errorString isKindOfClass:[NSNull class]] || errorString.length == 0 || [errorString isEqualToString:@"null"])
                            {
                                // errorString = @"Unexpected error occured. Please try after some time.";
                                errorString = NSLocalizedString(@"network_error_txt",nil);
                            }
                            
                            NSMutableDictionary* details = [NSMutableDictionary dictionary];
                            [details setValue:errorString forKey:NSLocalizedDescriptionKey];
                            
                            //NSError *serverError = [NSError errorWithDomain:@"Unexpected error occured. Please try after some time" code:errorCode userInfo:details];
                            
                            NSError *serverError = [NSError errorWithDomain: NSLocalizedString(@"network_error_txt",nil) code:errorCode userInfo:details];
                            
                            
                            
                            completionBlock(jsonResponse,serverError,tag);
                            //completionBlock(jsonResponse,errorResponse,tag);
                            
                        }
                        
                    }
                    else{
                        
                        
                        completionBlock(nil,errorResponse,tag);
                    }
                    
                    
                    
                    NSString *JsonOutStr=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    
                    responseString=[self decryptionAction:JsonOutStr];
                    
                    
                    NSString *strSaltReqDigiLockerValue = SaltRequestVaueDigiLocker;//[self getKeyWithTag:KEYCHAIN_SaltRequestVaueDigiLocker];

                    NSString *jsonResponsewithSalt = [NSString stringWithFormat:@"%@|%@",responseString,strSaltReqDigiLockerValue];
                    // Convert and print the MD5 value to the console
                    NSString* jsonResponsewithSaltMD5=[NSString stringWithFormat:@"%@",[jsonResponsewithSalt MD5]];
                    
                    if (![jsonResponsewithSaltMD5 isEqualToString:[headers valueForKey:@"X-REQUEST-VALUE"]])
                    {
                        
                        NSError* error = nil;
                        
                        NSMutableDictionary* details = [NSMutableDictionary dictionary];
                        //[details setValue:@"Invalid Request Value" forKey:NSLocalizedDescriptionKey];
                        [details setValue:NSLocalizedString(@"network_error_txt",nil) forKey:NSLocalizedDescriptionKey];
                        
                        // populate the error object with the details
                        error = [NSError errorWithDomain:@"Umang" code:200 userInfo:details];
                        
                        //invalid  not equal
                        completionBlock(nil,error,tag);
                        
                    }
                    else
                        
                    {
                        //valid
                        
                        
                        
                        //NSLog(@" response =%@",responseString);
                        
                        
                        
                        if([responseString length]==0)
                        {
                            NSError* error = nil;
                            
                            NSMutableDictionary* details = [NSMutableDictionary dictionary];
                            // [details setValue:@"Invalid Response Value,please retry" forKey:NSLocalizedDescriptionKey];
                            
                            [details setValue:NSLocalizedString(@"network_error_txt",nil) forKey:NSLocalizedDescriptionKey];
                            
                            
                            
                            // populate the error object with the details
                            error = [NSError errorWithDomain:@"Umang" code:errorCode userInfo:details];
                            
                            //invalid  not equal
                            completionBlock(nil,error,tag);
                        }
                    }
                }
                
                else
                {
                    completionBlock(nil,error,tag);
                }
                
            }
            else //all other codes
            {
                NSLog(@"Error Occured = %@",error.localizedDescription);
                
                NSError *serverError=error;
                
                
                NSString *string =[NSString stringWithFormat:@"%@",error.localizedDescription];
                if ([string rangeOfString:@"The request timed out"].location == NSNotFound  )
                    //||[string rangeOfString:@"Could not connect to the server"].location == NSNotFound
                {
                    NSLog(@"string dont contains!");
                    
                } else
                {
                    NSInteger errorCode=[httpResponse statusCode];
                    
                    NSMutableDictionary* details = [NSMutableDictionary dictionary];
                    [details setValue:NSLocalizedString(@"network_error_txt",nil)  forKey:NSLocalizedDescriptionKey];
                    
                    serverError = [NSError errorWithDomain:NSLocalizedString(@"network_error_txt",nil) code:errorCode userInfo:details];
                    
                    
                    //completionBlock(nil,error,tag);
                    
                }
                
                
                
                completionBlock(nil,serverError,tag);
                
                //completionBlock(nil,error,tag);
            }
        });
    }];
    
    [postDataTask resume];
    
    
}

- (void)refreshBearerAuthDigilocker:(NSString*)apiURL withBody:(NSString*)requestBody andTag:(REQUEST_TAG)tag  withCompletionHandler:(void (^)(NSString *))handler
{
    
    apiURL = UM_API_REFRESH_BEARER_DIGI;
    singleton = [SharedManager sharedSingleton];
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"lang"];//lang is Status of the account
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [dictBody setObject:@"" forKey:@"st"];  //get from mobile default email //not supported iphone
    
    [self hitWebServiceAPIForDigiLockerWithPOST:YES isAccessTokenRequired:NO webServiceURL:apiURL withBody:requestBody andTag:tag completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
     {
         
         NSLog(@"======>>><<<====== Response = %@",response);
         NSString *accessToken = [response valueForKey:@"access_token"];
         handler(accessToken);
         
         //            handler(accessToken);
         
     }];
    
    
}



-(void)hitWebServiceAPIForDigiLockerWithPOST:(BOOL)isPost isAccessTokenRequired:(BOOL)tokenRequired webServiceURL:(NSString *)apiURL withBody:(id)requestBody andTag:(REQUEST_TAG)tag completionHandler:(APICompletionBlock)completionBlock
{
    singleton = [SharedManager sharedSingleton];

    NSString *jsonEncryptedRequestBody = @"";
    NSString *jsonInputStringAESEncrypted = @"";
    if (requestBody) {
        
        //===========================================
        //Start new added for international parameter
        //===========================================
        [requestBody addEntriesFromDictionary:[self getInternationalparameter:tag]];
        //===========================================
        //End new added for international parameter
        //===========================================
        
        
        // Add common params here only
        [requestBody addEntriesFromDictionary:[self getCommonParametersForRequestBody]];
        
        
        
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:requestBody options:0 error:nil];
        NSString *bodyString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        //---------------------------------------------------------------
        //  CONVERT INPUT JSON TO AES ENCRYPTION
        //---------------------------------------------------------------
        jsonInputStringAESEncrypted=[self encryptInputJsonDigilocker:bodyString];
        //NSLog(@"Encrypted JsonInputStr=%@",jsonInputStringAESEncrypted);
        
        NSLog(@"Json Encrypted %@",jsonInputStringAESEncrypted);
        
        NSLog(@"Body ---> %@",bodyString);
        NSLog(@"Salt -----> %@",SaltRequestVaueDigiLocker);
        
        
        NSString *str = [NSString stringWithFormat:@"%@|%@",bodyString,@"D@GUM4NG$#4PP"];
        NSLog(@"Str - %@",str);
        
        // Convert and print the MD5 value to the console
        jsonEncryptedRequestBody=[NSString stringWithFormat:@"%@",[str MD5]];
        
        
    }
    
    
    /*
     ********** X-REQUEST-TSTAMP  format yyyyMMddHHmmss**********
     //  X-REQUEST-CONTROL  | X-REQUEST-TSTAMP|salt|  SHA256  R%d&Wst676#(Na
     
     */
    NSString *timeStamp = [self getTimeStamp];
    NSString *saltRequestControl = SaltRequestControl;//[self getKeyWithTag:KEYCHAIN_SaltRequestControl];

    NSString *encrytSHA256=[NSString stringWithFormat:@"|%@|%@|",timeStamp,saltRequestControl];
    NSString *timeStampEncrypted=[encrytSHA256 sha256HashFor:encrytSHA256];
    
    tokenRequired=YES;
    
    NSString *authToken = @"";
    
    
    // Append auth token with Bearer
    if (tokenRequired)
    {
        // Digilocker authorirzation
        authToken = @"Bearer 3a0f8e45-cb57-398c-929a-b748d033e4ll";
        
        NSString *refreshToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"DIGIAUTHREFRESHTOKEN"];
        if (refreshToken.length) {
            authToken = refreshToken;
        }
        //  [[NSUserDefaults standardUserDefaults] setObject:accessToken forKey :@"DIGIAUTHREFRESHTOKEN"];
        
        //        authToken = [[NSUserDefaults standardUserDefaults]
        //                     decryptedValueForKey:@"BEARERTOKEN"];;
        //        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
    }
    //---fix lint ----
    NSLog(@"authToken=%@",authToken);
    
    
    NSInteger randomID = arc4random() % 9000000000000000 + 1000000000000000;
    
    
    NSString *requestURL=[NSString stringWithFormat:@"%@%@",singleton.apiMode.WSO2PathBASE,apiURL];
    
    
    //  NSString *requestURL=[NSString stringWithFormat:@"%@:%@%@",WSO2PathBASE,WSO2_Port,apiURL];
    NSLog(@" requestURL =%@",requestURL);
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestURL]  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    
    NSData *requestData=[jsonInputStringAESEncrypted dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPMethod:isPost?@"POST":@"GET"];
    [request setHTTPBody: requestData];
    [request setValue:@"text/plain" forHTTPHeaderField:API_CONTENT_TYPE];
    [request setValue:timeStampEncrypted forHTTPHeaderField:API_REQUEST_CONTROL];
    [request setValue:jsonEncryptedRequestBody forHTTPHeaderField:API_REQUEST_VALUE];
    [request setValue:timeStamp forHTTPHeaderField:API_REQUEST_TIME_STAMP];
    //[request setValue:authToken forHTTPHeaderField:API_REQUEST_AUTHORIZATION];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setValue:[NSString stringWithFormat:@"%ld",randomID] forHTTPHeaderField:@"requestid"];
    
    NSLog(@"X-REQUEST-CONTROL - %@",timeStampEncrypted);
    NSLog(@"X-REQUEST-VALUE - %@",jsonEncryptedRequestBody);
    NSLog(@"Authorization  - %@",authToken);
    NSLog(@"X-REQUEST-TSTAMP  - %@",timeStamp);
    
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString*  responseString;
            //            NSString *JsonOutStr=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            //
            //            responseString=[self decryptionActionDigiLocker:JsonOutStr];
            
            NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
            
            //NSLog(@" headers =%@",headers);
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            //NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
            
            if([httpResponse statusCode]==403)
            {
                [self openLoginView];
                return ;
            }
            
            else  if([httpResponse statusCode]==200)
            {
                
                if (!error)
                {
                    
                    
                    NSString *JsonOutStr=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    
                    responseString=[self decryptionActionDigiLocker:JsonOutStr];
                    
                    NSString *strSaltReqDigiLockerValue = SaltRequestVaueDigiLocker ;//[self getKeyWithTag:KEYCHAIN_SaltRequestVaueDigiLocker];

                    NSString *jsonResponsewithSalt = [NSString stringWithFormat:@"%@|%@",responseString,strSaltReqDigiLockerValue];
                    // Convert and print the MD5 value to the console
                    NSString* jsonResponsewithSaltMD5=[NSString stringWithFormat:@"%@",[jsonResponsewithSalt MD5]];
                    
                    //NSLog(@" jsonResponsewithSaltMD5 =%@",jsonResponsewithSaltMD5);
                    //NSLog(@" X-REQUEST-VALUE Response =%@",[headers valueForKey:@"X-REQUEST-VALUE"]);
                    
                    // jsonResponsewithSaltMD5=@"7c01cb28b17ef1a20d610f3a764c0dd7";//test case to be check
                    if (![jsonResponsewithSaltMD5 isEqualToString:[headers valueForKey:@"X-REQUEST-VALUE"]])
                    {
                        
                        NSError* error = nil;
                        
                        NSMutableDictionary* details = [NSMutableDictionary dictionary];
                        //[details setValue:@"Invalid Request Value" forKey:NSLocalizedDescriptionKey];
                        [details setValue:NSLocalizedString(@"network_error_txt",nil) forKey:NSLocalizedDescriptionKey];
                        
                        // populate the error object with the details
                        error = [NSError errorWithDomain:@"Umang" code:200 userInfo:details];
                        
                        
                        NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
                        id jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                          options:kNilOptions
                                                                            error:&error];
                        
                        
                        if ([jsonResponse objectForKey:@"access_token"])
                        {
                            
                            [[NSUserDefaults standardUserDefaults] setObject:[jsonResponse objectForKey:@"access_token"] forKey:@"DIGIAUTHREFRESHTOKEN"];
                            
                            completionBlock(jsonResponse,error,tag);
                            
                        }
                        else
                        {
                            completionBlock(nil,error,tag);
                        }
                        
                        //invalid  not equal
                        
                        
                    }
                    else
                        
                    {
                        //valid
                        
                        //NSLog(@" response =%@",responseString);
                        
                        
                        NSInteger errorCode=[httpResponse statusCode];
                        NSError *errorResponse;
                        
                        if([responseString length]==0)
                        {
                            NSError* error = nil;
                            
                            NSMutableDictionary* details = [NSMutableDictionary dictionary];
                            // [details setValue:@"Invalid Response Value,please retry" forKey:NSLocalizedDescriptionKey];
                            
                            [details setValue:NSLocalizedString(@"network_error_txt",nil) forKey:NSLocalizedDescriptionKey];
                            
                            
                            
                            // populate the error object with the details
                            error = [NSError errorWithDomain:@"Umang" code:errorCode userInfo:details];
                            
                            //invalid  not equal
                            completionBlock(nil,error,tag);
                        }
                        NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
                        id jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                          options:kNilOptions
                                                                            error:&errorResponse];
                        
                        //NSLog(@" jsonResponse =%@",jsonResponse);
                        
                        if (errorResponse == nil)
                        {
                            // Success Case
                            
                            if ([jsonResponse objectForKey:@"access_token"])
                            {
                                
                                [[NSUserDefaults standardUserDefaults] setObject:[jsonResponse objectForKey:@"access_token"] forKey:@"DIGIAUTHREFRESHTOKEN"];
                                
                                completionBlock(jsonResponse,errorResponse,tag);
                                
                            }
                            
                            else if ([[jsonResponse objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[jsonResponse objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                            {
                                completionBlock(jsonResponse,errorResponse,tag);
                            }
                            else
                            {
                                NSString *errorString = [jsonResponse objectForKey:@"rd"];
                                if ([errorString isKindOfClass:[NSNull class]] || errorString.length == 0 || [errorString isEqualToString:@"null"]) {
                                    // errorString = @"Unexpected error occured. Please try after some time.";
                                    errorString = NSLocalizedString(@"network_error_txt",nil);
                                    
                                    
                                    
                                }
                                
                                NSMutableDictionary* details = [NSMutableDictionary dictionary];
                                [details setValue:errorString forKey:NSLocalizedDescriptionKey];
                                
                                //NSError *serverError = [NSError errorWithDomain:@"Unexpected error occured. Please try after some time" code:errorCode userInfo:details];
                                
                                NSError *serverError = [NSError errorWithDomain: NSLocalizedString(@"network_error_txt",nil) code:errorCode userInfo:details];
                                
                                
                                
                                completionBlock(jsonResponse,serverError,tag);
                                //completionBlock(jsonResponse,errorResponse,tag);
                                
                            }
                            
                        }
                        else{
                            
                            
                            completionBlock(nil,errorResponse,tag);
                        }
                    }
                }//close else
                
                else{
                    completionBlock(nil,error,tag);
                }
                
            }
            else //all other codes
            {
                NSLog(@"Error Occured = %@",error.localizedDescription);
                /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                 message:error.localizedDescription
                 delegate:self
                 cancelButtonTitle:NSLocalizedString(@"ok", nil)
                 otherButtonTitles:nil];
                 [alert show];
                 
                 */
                NSError *serverError=error;
                
                
                NSString *string =[NSString stringWithFormat:@"%@",error.localizedDescription];
                if ([string rangeOfString:@"The request timed out"].location == NSNotFound  )
                    //||[string rangeOfString:@"Could not connect to the server"].location == NSNotFound
                {
                    NSLog(@"string dont contains!");
                    
                } else
                {
                    NSInteger errorCode=[httpResponse statusCode];
                    
                    NSMutableDictionary* details = [NSMutableDictionary dictionary];
                    [details setValue:NSLocalizedString(@"network_error_txt",nil)  forKey:NSLocalizedDescriptionKey];
                    
                    serverError = [NSError errorWithDomain:NSLocalizedString(@"network_error_txt",nil) code:errorCode userInfo:details];
                    
                    
                    //completionBlock(nil,error,tag);
                    
                }
                
                
                
                completionBlock(nil,serverError,tag);
                
                //completionBlock(nil,error,tag);
            }
        });
    }];
    
    [postDataTask resume];
}


-(void)hitAPIForDigiLockerDownloadFileWithPost:(BOOL)isPost isAccessTokenRequired:(BOOL)tokenRequired webServiceURL:(NSString *)apiURL withBody:(id)requestBody andTag:(REQUEST_TAG)tag completionHandler:(APICompletionBlock)completionBlock
{
    
    singleton = [SharedManager sharedSingleton];
    
    NSString *authToken = @"";
    
    // Append auth token with Bearer
    if (tokenRequired)
    {
        // Digilocker authorirzation
        // authToken = @"Bearer 3a0f8e45-cb57-398c-929a-b748d033e4ll";
        
        
        authToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"DIGIAUTHREFRESHTOKEN"];
        if (authToken.length == 0) {
            authToken = @"Bearer 3a0f8e45-cb57-398c-929a-b748d033e4ll";
        }
    }
    
    NSInteger randomID = arc4random() % 9000000000000000 + 1000000000000000;
    
    
    NSString *requestURL=[NSString stringWithFormat:@"%@%@",singleton.apiMode.WSO2PathBASE,apiURL];
    
    NSLog(@" requestURL =%@",requestURL);
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestURL]  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    //multipart/form-data
    
    
    
    
    //NSData *requestData=[bodyString dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPMethod:isPost?@"POST":@"GET"];
    
    NSString *query = [self joinQueryWithDictionary:requestBody];
    request.HTTPBody = [query dataUsingEncoding:NSUTF8StringEncoding];
    
    //[request setHTTPBody: requestData];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:API_CONTENT_TYPE];
    [request setValue:authToken forHTTPHeaderField:API_REQUEST_AUTHORIZATION];
    
    [request setValue:[NSString stringWithFormat:@"%ld",(long)randomID] forHTTPHeaderField:@"requestid"];
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            __block NSString*  responseString;
            NSString *JsonOutStr=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            responseString=[self decryptionActionDigiLocker:JsonOutStr];
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            
            
            if([httpResponse statusCode]==403)
            {
                [self openLoginView];
                return ;
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            if([httpResponse statusCode]==401)
                
            {
                
                [self refreshBearerAuthDigilocker:apiURL withBody:requestBody andTag:tag withCompletionHandler:^(NSString* returnString)
                 {
                     
                     NSLog(@"Received access token: %@", returnString);
                     NSString *refreshAuthToken = [NSString stringWithFormat:@"Bearer %@",returnString];
                     [[NSUserDefaults standardUserDefaults] setObject:refreshAuthToken forKey:@"DIGIAUTHREFRESHTOKEN"];
                     
                     [self hitAPIForDigiLockerAuthenticationWithPost:YES isAccessTokenRequired:YES webServiceURL:apiURL withBody:requestBody andTag:tag completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
                      {
                          NSLog(@"%@",response);
                          
                          if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                          {
                              completionBlock(response,nil,tag);
                          }
                          else
                          {
                              NSString *errorString = [response objectForKey:@"rd"];
                              if ([errorString isKindOfClass:[NSNull class]] || errorString.length == 0 || [errorString isEqualToString:@"null"])
                              {
                                  // errorString = @"Unexpected error occured. Please try after some time.";
                                  errorString = NSLocalizedString(@"network_error_txt",nil);
                              }
                              
                              NSMutableDictionary* details = [NSMutableDictionary dictionary];
                              [details setValue:errorString forKey:NSLocalizedDescriptionKey];
                              
                              //NSError *serverError = [NSError errorWithDomain:@"Unexpected error occured. Please try after some time" code:errorCode userInfo:details];
                              
                              NSError *serverError = [NSError errorWithDomain: NSLocalizedString(@"network_error_txt",nil) code:200 userInfo:details];
                              
                              
                              completionBlock(response,serverError,tag);
                              //completionBlock(jsonResponse,errorResponse,tag);
                              
                          }
                          
                          
                      }];
                 }];
                
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            else  if([httpResponse statusCode]==200)
            {
                
                if (!error)
                {
                    
                    completionBlock(data,error,tag);
                }
                
                else
                {
                    completionBlock(nil,error,tag);
                }
                
            }
            else //all other codes
            {
                NSLog(@"Error Occured = %@",error.localizedDescription);
                
                NSError *serverError=error;
                
                
                NSString *string =[NSString stringWithFormat:@"%@",error.localizedDescription];
                if ([string rangeOfString:@"The request timed out"].location == NSNotFound  )
                    //||[string rangeOfString:@"Could not connect to the server"].location == NSNotFound
                {
                    NSLog(@"string dont contains!");
                    
                } else
                {
                    NSInteger errorCode=[httpResponse statusCode];
                    
                    NSMutableDictionary* details = [NSMutableDictionary dictionary];
                    [details setValue:NSLocalizedString(@"network_error_txt",nil)  forKey:NSLocalizedDescriptionKey];
                    
                    serverError = [NSError errorWithDomain:NSLocalizedString(@"network_error_txt",nil) code:errorCode userInfo:details];
                    
                    
                    //completionBlock(nil,error,tag);
                    
                }
                
                
                
                completionBlock(nil,serverError,tag);
                
                //completionBlock(nil,error,tag);
            }
        });
    }];
    
    [postDataTask resume];
    
    
    
}


/// UPLOAD

-(void)hitAPIForDigiLockerUploadFileWithPost:(BOOL)isPost isAccessTokenRequired:(BOOL)tokenRequired webServiceURL:(NSString *)apiURL withBody:(id)requestBody andTag:(REQUEST_TAG)tag andFilePath:(NSString *)filePath completionHandler:(APICompletionBlock)completionBlock
{
    
    NSString *pathString = filePath;
    
    
    singleton = [SharedManager sharedSingleton];
    
    NSString *authToken = @"";
    
    // Append auth token with Bearer
    if (tokenRequired)
    {
        // Digilocker authorirzation
        // authToken = @"Bearer 3a0f8e45-cb57-398c-929a-b748d033e4ll";
        
        
        authToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"DIGIAUTHREFRESHTOKEN"];
        if (authToken.length == 0) {
            authToken = @"Bearer 3a0f8e45-cb57-398c-929a-b748d033e4ll";
        }
    }
    
    NSInteger randomID = arc4random() % 9000000000000000 + 1000000000000000;
    
    
    NSString *requestURL=[NSString stringWithFormat:@"%@%@",singleton.apiMode.WSO2PathBASE,apiURL];
    
    NSLog(@" requestURL =%@",requestURL);
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestURL]  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request setHTTPMethod:isPost?@"POST":@"GET"];
    
    //[request setHTTPBody: requestData];
    
    [request setValue:authToken forHTTPHeaderField:API_REQUEST_AUTHORIZATION];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data"];
    [request setValue:contentType forHTTPHeaderField: API_CONTENT_TYPE];
    
    [request setValue:[NSString stringWithFormat:@"%ld",(long)randomID] forHTTPHeaderField:@"requestid"];
    
    // create body
    NSString *boundary = nil;
    NSData *post = [self multipartDataWithParameters:requestBody boundary:&boundary];
    [request setValue:[@"multipart/form-data; boundary=" stringByAppendingString:boundary] forHTTPHeaderField:@"Content-type"];
    request.HTTPBody = post;
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            __block NSString*  responseString;
            NSString *JsonOutStr=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            responseString=[self decryptionActionDigiLocker:JsonOutStr];
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            
            if([httpResponse statusCode]==403)
            {
                [self openLoginView];
                return ;
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            
            if([httpResponse statusCode]==401)
                
            {
                
                [self refreshBearerAuthDigilocker:apiURL withBody:requestBody andTag:tag withCompletionHandler:^(NSString* returnString)
                 {
                     
                     NSLog(@"Received access token: %@", returnString);
                     NSString *refreshAuthToken = [NSString stringWithFormat:@"Bearer %@",returnString];
                     [[NSUserDefaults standardUserDefaults] setObject:refreshAuthToken forKey:@"DIGIAUTHREFRESHTOKEN"];
                     
                     [self hitAPIForDigiLockerAuthenticationWithPost:YES isAccessTokenRequired:YES webServiceURL:apiURL withBody:requestBody andTag:tag completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
                      {
                          NSLog(@"%@",response);
                          
                          if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                          {
                              completionBlock(response,nil,tag);
                          }
                          else
                          {
                              NSString *errorString = [response objectForKey:@"rd"];
                              if ([errorString isKindOfClass:[NSNull class]] || errorString.length == 0 || [errorString isEqualToString:@"null"])
                              {
                                  // errorString = @"Unexpected error occured. Please try after some time.";
                                  errorString = NSLocalizedString(@"network_error_txt",nil);
                              }
                              
                              NSMutableDictionary* details = [NSMutableDictionary dictionary];
                              [details setValue:errorString forKey:NSLocalizedDescriptionKey];
                              
                              //NSError *serverError = [NSError errorWithDomain:@"Unexpected error occured. Please try after some time" code:errorCode userInfo:details];
                              
                              NSError *serverError = [NSError errorWithDomain: NSLocalizedString(@"network_error_txt",nil) code:200 userInfo:details];
                              
                              
                              completionBlock(response,serverError,tag);
                              //completionBlock(jsonResponse,errorResponse,tag);
                              
                          }
                          
                          
                      }];
                 }];
                
            }
            
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            //-----------------------------------------------------------------------------
            else  if([httpResponse statusCode]==200)
            {
                
                if (!error)
                {
                    
                    
                    NSError *errorResponse;
                    NSInteger errorCode=[httpResponse statusCode];
                    
                    NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
                    id jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                      options:kNilOptions
                                                                        error:&errorResponse];
                    
                    
                    if (errorResponse == nil)
                    {
                        // Success Case
                        if ([[jsonResponse objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[jsonResponse objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                        {
                            completionBlock(jsonResponse,errorResponse,tag);
                        }
                        else if ([[jsonResponse valueForKey:@"rs"] isEqualToString:@"F"] && [[jsonResponse valueForKey:@"rc"] isEqualToString:@"DGL0003"])
                        {
                            
                            NSLog(@"Hit upload Api");
                            
                            
                            
                            NSMutableDictionary *dictBody = [NSMutableDictionary new];
                            
                            double CurrentTime = CACurrentMediaTime();
                            
                            NSString * timeInMS = [NSString stringWithFormat:@"%f", CurrentTime];
                            
                            timeInMS=  [timeInMS stringByReplacingOccurrencesOfString:@"." withString:@""];
                            
                            
                            
                            
                            NSString *oldRefreshToken = [[NSUserDefaults standardUserDefaults]valueForKey:@"RefreshTokenDigi"];
                            
                            oldRefreshToken = oldRefreshToken.length == 0 ? @"" : oldRefreshToken;
                            
                            [dictBody setObject:timeInMS forKey:@"trkr"];
                            [dictBody setObject:@"rgtadhr" forKey:@"ort"];
                            [dictBody setObject:@"Y" forKey:@"rc"];
                            [dictBody setObject:@"Y" forKey:@"mec"];
                            [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
                            [dictBody setObject:oldRefreshToken forKey:@"rtkn"];
                            
                            NSLog(@"Dictionary is %@",dictBody);
                            
                            [self getNewRefreshTokenDigiLocker:YES isAccessTokenRequired:YES webServiceURL:UM_API_REFRESH_TOKEN withBody:dictBody andTag:TAG_REQUEST_DIGILOCKER_GETREFRESHTOKEN completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
                             {
                                 
                                 NSLog(@"%@",response);
                                 
                                 NSString *accessToken = [[response valueForKey:@"pd"] valueForKey:@"access_token"];
                                 NSString *refreshToken = [[response valueForKey:@"pd"] valueForKey:@"refresh_token"];
                                 
                                 accessToken = accessToken.length == 0 ? @"" : accessToken;
                                 
                                 refreshToken = refreshToken.length == 0 ? @"" : refreshToken;
                                 
                                 [[NSUserDefaults standardUserDefaults] setValue:accessToken forKey:@"AccessTokenDigi"];
                                 [[NSUserDefaults standardUserDefaults] setValue:refreshToken forKey:@"RefreshTokenDigi"];
                                 
                                 [requestBody setObject:accessToken forKey:@"utkn"];
                                 
                                 [self hitAPIForDigiLockerUploadFileWithPost:YES isAccessTokenRequired:YES webServiceURL:requestURL withBody:requestBody andTag:tag andFilePath:pathString completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
                                  {
                                      if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                                      {
                                          completionBlock(jsonResponse,error,tag);
                                      }
                                      else
                                      {
                                          completionBlock(nil,error,tag);
                                      }
                                      
                                  }];
                                 
                                 
                             }];
                            
                        }
                        else
                        {
                            NSString *errorString = [jsonResponse objectForKey:@"rd"];
                            if ([errorString isKindOfClass:[NSNull class]] || errorString.length == 0 || [errorString isEqualToString:@"null"])
                            {
                                // errorString = @"Unexpected error occured. Please try after some time.";
                                errorString = NSLocalizedString(@"network_error_txt",nil);
                            }
                            
                            NSMutableDictionary* details = [NSMutableDictionary dictionary];
                            [details setValue:errorString forKey:NSLocalizedDescriptionKey];
                            
                            //NSError *serverError = [NSError errorWithDomain:@"Unexpected error occured. Please try after some time" code:errorCode userInfo:details];
                            
                            NSError *serverError = [NSError errorWithDomain: NSLocalizedString(@"network_error_txt",nil) code:errorCode userInfo:details];
                            
                            
                            
                            completionBlock(jsonResponse,serverError,tag);
                            //completionBlock(jsonResponse,errorResponse,tag);
                            
                        }
                        
                    }
                    else{
                        
                        
                        completionBlock(nil,errorResponse,tag);
                    }
                    
                }
                
                else
                {
                    completionBlock(nil,error,tag);
                }
                
            }
            else //all other codes
            {
                NSLog(@"Error Occured = %@",error.localizedDescription);
                
                NSError *serverError=error;
                
                
                NSString *string =[NSString stringWithFormat:@"%@",error.localizedDescription];
                if ([string rangeOfString:@"The request timed out"].location == NSNotFound  )
                    //||[string rangeOfString:@"Could not connect to the server"].location == NSNotFound
                {
                    NSLog(@"string dont contains!");
                    
                } else
                {
                    NSInteger errorCode=[httpResponse statusCode];
                    
                    NSMutableDictionary* details = [NSMutableDictionary dictionary];
                    [details setValue:NSLocalizedString(@"network_error_txt",nil)  forKey:NSLocalizedDescriptionKey];
                    
                    serverError = [NSError errorWithDomain:NSLocalizedString(@"network_error_txt",nil) code:errorCode userInfo:details];
                    
                    
                    //completionBlock(nil,error,tag);
                    
                }
                
                
                
                completionBlock(nil,serverError,tag);
                
                //completionBlock(nil,error,tag);
            }
        });
    }];
    
    [postDataTask resume];
    
    
}


-(NSData *)multipartDataWithParameters:(NSDictionary *)parameters boundary:(NSString **)boundary
{
    NSMutableData *result = [[NSMutableData alloc] init];
    if (boundary && !*boundary) {
        char buffer[32];
        for (NSUInteger i = 0; i < 32; i++) buffer[i] = "0123456789ABCDEF"[rand() % 16];
        NSString *random = [[NSString alloc] initWithBytes:buffer length:32 encoding:NSASCIIStringEncoding];
        *boundary = [NSString stringWithFormat:@"MyApp--%@", random];
    }
    NSData *newline = [@"\r\n" dataUsingEncoding:NSUTF8StringEncoding];
    NSData *boundaryData = [[NSString stringWithFormat:@"--%@\r\n", boundary ? *boundary : @""] dataUsingEncoding:NSUTF8StringEncoding];
    
    for (NSArray *pair in [self flatten:parameters]) {
        [result appendData:boundaryData];
        [self appendToMultipartData:result key:pair[0] value:pair[1]];
        [result appendData:newline];
    }
    NSString *end = [NSString stringWithFormat:@"--%@--\r\n", boundary ? *boundary : @""];
    [result appendData:[end dataUsingEncoding:NSUTF8StringEncoding]];
    return result;
}

-(void)appendToMultipartData:(NSMutableData *)data key:(NSString *)key value:(id)value
{
    if ([value isKindOfClass:NSData.class]) {
        NSString *name = key;
        if ([key rangeOfString:@"%2F"].length) {
            NSRange r = [name rangeOfString:@"%2F"];
            key = [key substringFromIndex:r.location + r.length];
            name = [name substringToIndex:r.location];
        }
        NSString *string = [NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\nContent-Type: application/octet-stream\r\n\r\n", name, key];
        [data appendData:[string dataUsingEncoding:NSUTF8StringEncoding]];
        [data appendData:value];
    } else {
        NSString *string = [NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@", key, value];
        [data appendData:[string dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

#pragma mark - Methods For Multi Part API Call

- (NSString *)joinQueryWithDictionary:(NSDictionary *)dictionary
{
    NSMutableString *result = [[NSMutableString alloc] init];
    for (NSArray *pair in [self flatten:dictionary]) {
        if (result.length) [result appendString:@"&"];
        [result appendString:pair[0]];
        [result appendString:@"="];
        [result appendString:[self escape:[pair[1] description]]];
    }
    return result;
}
- (NSArray *)flatten:(NSDictionary *)dictionary
{
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:dictionary.count];
    NSArray *keys = [dictionary.allKeys sortedArrayUsingSelector:@selector(compare:)];
    for (NSString *key in keys) {
        id value = [dictionary objectForKey:key];
        if ([value isKindOfClass:NSArray.class] || [value isKindOfClass:NSSet.class]) {
            NSString *k = [[self escape:key] stringByAppendingString:@"[]"];
            for (id v in value) {
                [result addObject:@[k, v]];
            }
        } else if ([value isKindOfClass:NSDictionary.class]) {
            for (NSString *k in value) {
                NSString *kk = [[self escape:key] stringByAppendingFormat:@"[%@]", [self escape:k]];
                [result addObject:@[kk, [value valueForKey:k]]];
            }
        } else {
            [result addObject:@[[self escape:key], value]];
        }
    }
    return result;
}

- (NSString *)escape:(NSString *)string
{
    return CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (__bridge CFStringRef)string, NULL, CFSTR("*'();:@&=+$,/?!%#[]"), kCFStringEncodingUTF8));
}


#pragma mark -
-(NSString*)getTimeStamp{
    if (_dateFormat == nil) {
        _dateFormat = [[NSDateFormatter alloc] init];
    }
    
    [_dateFormat setDateFormat:TIME_STAMP_DATE_FORMAT];
    NSString *timeStamp = [_dateFormat stringFromDate:[NSDate date]];
    return timeStamp;
}




- (NSString*)encryptInputJson:(NSString*)jsonStr
{
    
    //NSString *strSalt = [self getKeyWithTag:KEYCHAIN_SaltAES];
    NSString *value = jsonStr;
    // = [
    // const char *dataKey = [[self getKeyWithTag:KEYCHAIN_KEY_JSON] by];
    //NSString *keyAES = [self getKeyWithTag:KEYCHAIN_KEY_JSON];
    NSString *resultEncryptString=  [value EncryptData:value saltkey:SaltAES];//[value EncryptData:value saltkey:strSalt withKey:keyAES];
    return resultEncryptString;
}

-(void)saveKeyIntoKeyChainWithKey:(NSString*)value withKeyTag:(NSString*)Tag {
    SecKeyWrapper *key = [SecKeyWrapper sharedWrapper];
    NSData *dataKey = [[NSString stringWithFormat:@"%@",value] dataUsingEncoding:NSUTF8StringEncoding];
    [key addKeyToKeychainWith:dataKey tag:Tag];
    
}
-(NSString* _Nullable )getKeyWithTag:( NSString* )Tag {
    SecKeyWrapper *key = [SecKeyWrapper sharedWrapper];
    NSData *dataSalt = [key getKeyDataFromKeyChainWith:Tag];
    if (dataSalt == nil) {
        return nil;
    }
    return [NSString stringWithUTF8String:[dataSalt bytes]];
}


- (NSString*)decryptionAction:(NSString*)jsonOutStr
{
   // NSString *strSalt = [self getKeyWithTag:KEYCHAIN_SaltAES];
    NSString *value = jsonOutStr;
    // = [
    // const char *dataKey = [[self getKeyWithTag:KEYCHAIN_KEY_JSON] by];
    //NSString *keyAES = [self getKeyWithTag:KEYCHAIN_KEY_JSON];
    NSString *resultDecryptString= [value DecryptData:value  saltkey:SaltAES]; //[value DecryptData:value saltkey:strSalt withKey:keyAES];//[value DecryptData:value  saltkey:SaltAES];
    return  resultDecryptString;
}

- (NSString*)encryptInputJsonDigilocker:(NSString*)jsonStr
{
    //NSString *strSalt = [self getKeyWithTag:KEYCHAIN_SaltAESDigilocker];
    
    NSString *value = jsonStr;
    //NSString *keyAES = [self getKeyWithTag:KEYCHAIN_KEY_DIGILOCKER];
    NSString *resultEncryptString= [value EncryptDataDigilocker:value  saltkey:SaltAESDigilocker];//[value EncryptData:value saltkey:strSalt withKey:keyAES]; //[value EncryptDataDigilocker:value  saltkey:SaltAESDigilocker];
    //NSLog(@"%@",resultEncryptString);
    return resultEncryptString;
}

- (NSString*)decryptionActionDigiLocker:(NSString*)jsonOutStr
{
    //NSString *strSalt = [self getKeyWithTag:KEYCHAIN_SaltAESDigilocker];
    
    NSString *value = jsonOutStr;
 //   NSString *keyAES = [self getKeyWithTag:KEYCHAIN_KEY_DIGILOCKER];
    NSString *resultDecryptString= [value DecryptDataDigiLocker:value  saltkey:SaltAESDigilocker];//[value DecryptData:value saltkey:strSalt withKey:keyAES] ;//[value DecryptDataDigiLocker:value  saltkey:SaltAESDigilocker];
    return  resultDecryptString;
}

-(NSMutableDictionary*)getCommonParametersForRequestBody
{
    
    SharedManager *objShared = [SharedManager sharedSingleton];
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    
    //    // Mobile Number
    //    [dict setObject:@"8000000000" forKey:@"mno"];
    
    
    // IMEI
    //[dict setObject:[objShared appUniqueID] forKey:@"imei"];
    [dict setObject:@"" forKey:@"imei"];
    
    // Device ID
    
    
    [dict setObject:[objShared appUniqueID] forKey:@"did"];
    
    // IMSI
    // [dict setObject:[objShared appUniqueID] forKey:@"imsi"];
    [dict setObject:@"" forKey:@"imsi"];
    
    // Handset Make
    [dict setObject:@"Apple" forKey:@"hmk"];
    
    // Handset Model
    
    //[dict setObject:[[UIDevice currentDevice] model] forKey:@"hmd"];
    [dict setObject:[self deviceName] forKey:@"hmd"];
    
    //TODO:// HARD CODED Bundle Idnetifier. It should be dynaimc
    
    
    
    
    
    NSString *bundleIdentifier = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
    // Package Name //in.gov.umang.negd.g2c.sdlinternal
    [dict setObject:bundleIdentifier forKey:@"pkg"];
    
    //[dict setObject:@"in.gov.umang.negd.g2c" forKey:@"pkg"];
    
    // Rooted
  //  [dict setObject:[self isJailbroken]?@"yes":@"no" forKey:@"rot"];
    [dict setObject: [objShared.dbManager isLibertyPatchJailBroken]?@"yes":@"no" forKey:@"rot"];

    // OS
    [dict setObject:@"iOS" forKey:@"os"];
    
    // ver
    
    //CurrentAppVersion
    //[dict setObject:[SharedManager appVersion] forKey:@"ver"];
    
    [dict setObject:[NSString stringWithFormat:@"%d",6] forKey:@"ver"];
    
    // Country Code
    [dict setObject:@"404" forKey:@"mcc"];
    
    // Mobile Network Code
    [dict setObject:@"02" forKey:@"mnc"];
    
    // Lcoation Area Code
    [dict setObject:@"2065" forKey:@"lac"];
    
    // Cell ID
    [dict setObject:@"11413" forKey:@"clid"];
    
    
    // Location Based Params
    // Accuracy
    [dict setObject:@"" forKey:@"acc"];
    
    // Latitude Code
    [dict setObject:@"" forKey:@"lat"];
    
    // Longitude Code
    [dict setObject:@"" forKey:@"lon"];
    
    // State Name
    //[dict setObject:@"" forKey:@"st"];
    
    
    // Selected Language Name
    NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if (selectedLanguage == nil)
    {
        selectedLanguage = @"en";
    }
    else{
        NSArray *arrTemp = [selectedLanguage componentsSeparatedByString:@"-"];
        selectedLanguage = [arrTemp firstObject];
    }
    
    NSLog(@"Applied Selected Language = %@",selectedLanguage);
    
    if ([[selectedLanguage lowercaseString] isEqualToString:@"english"])
    {
        selectedLanguage = @"en";
    }
    
    [dict setObject:selectedLanguage forKey:@"lang"];
    
    // Selected Mode (Domain(Web / App/Mobile Web)
    [dict setObject:@"app" forKey:@"mod"];
    //-------- Add Sharding logic here-------
    NSString *node = [[NSUserDefaults standardUserDefaults] objectForKey:@"NODE_KEY"];
    
    if (node == nil)
    {
        node =@"";
    }
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [dict setObject:node forKey:@"node"];
    //-------- Add Sharding logic here-------
    
    

  
    return dict;
}



-(BOOL)isJailbroken {
    NSURL* url = [NSURL URLWithString:@"cydia://package/com.example.package"];
    return [[UIApplication sharedApplication] canOpenURL:url];
}
/*
 - (BOOL)isJailbroken
 {
 BOOL jailbroken = NO;
 NSArray *jailbrokenPath = [NSArray arrayWithObjects:@"/Applications/Cydia.app",  @"/Applications/RockApp.app",  @"/Applications/Icy.app",  @"/usr/sbin/sshd",  @"/usr/bin/sshd",  @"/usr/libexec/sftp-server",  @"/Applications/WinterBoard.app",  @"/Applications/SBSettings.app",  @"/Applications/MxTube.app",  @"/Applications/IntelliScreen.app",  @"/Library/MobileSubstrate/DynamicLibraries/Veency.plist",  @"/Applications/FakeCarrier.app",  @"/Library/MobileSubstrate/DynamicLibraries/LiveClock.plist",  @"/private/var/lib/apt",  @"/Applications/blackra1n.app",  @"/private/var/stash",  @"/private/var/mobile/Library/SBSettings/Themes",  @"/System/Library/LaunchDaemons/com.ikey.bbot.plist",  @"/System/Library/LaunchDaemons/com.saurik.Cydia.Startup.plist",  @"/private/var/tmp/cydia.log",  @"/private/var/lib/cydia", nil];for(NSString *string in jailbrokenPath)
 {
 if ([[NSFileManager defaultManager] fileExistsAtPath:string]){
 jailbroken = YES;
 break;}
 }
 return jailbroken;
 }
 */


//----------------------------------------------
//----------- Method for chat handling----------
//----------------------------------------------



- (NSString*)encryptInputChatJson:(NSString*)jsonStr
{
    // NSString *strSalt = [self getKeyWithTag:SaltCHAT];
    NSString *value = jsonStr;
 //   NSString *keyAES = [self getKeyWithTag:KEYCHAIN_KEY_CHAT];
    NSString *resultEncryptString= [value EncryptChatData:value  saltkey:SaltCHAT];//[value EncryptData:value saltkey:SaltCHAT withKey:keyAES]; //[value EncryptChatData:value  saltkey:SaltCHAT];
    //NSLog(@"%@",resultEncryptString);
    return resultEncryptString;
}

- (NSString*)decryptionChatAction:(NSString*)jsonOutStr
{
    // NSString *strSalt = [self getKeyWithTag:SaltCHAT];
    NSString *value = jsonOutStr;
   // NSString *keyAES = [self getKeyWithTag:KEYCHAIN_KEY_CHAT];
    // NSString *resultEncryptString= [value EncryptData:value saltkey:SaltCHAT withKey:keyAES];
    NSString *resultDecryptString=  [value DecryptChatData:value  saltkey:SaltCHAT];//[value DecryptData:value saltkey:SaltCHAT withKey:keyAES];//[value DecryptChatData:value  saltkey:SaltCHAT];
    return  resultDecryptString;
}


-(void)hitXMPPChatWebServiceAPIWithPostMethod:(BOOL)isPost isAccessTokenRequired:(BOOL)tokenRequired webServiceURL:(NSString *)apiURL withBody:(id)requestBody andTag:(REQUEST_TAG)tag completionHandler:(APICompletionBlock)completionBlock
{
    singleton = [SharedManager sharedSingleton];

    NSString *jsonInputStringAESEncrypted = @"";
    if (requestBody)
    {
        
        // Add common params here only
        //[requestBody addEntriesFromDictionary:[self getCommonParametersForRequestBody]];
        
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:requestBody options:0 error:nil];
        NSString *bodyString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        //---------------------------------------------------------------
        //  CONVERT INPUT JSON TO AES ENCRYPTION
        //---------------------------------------------------------------
        jsonInputStringAESEncrypted=[self encryptInputChatJson:bodyString];
        
    }
    
    
    NSString *requestURL=[NSString stringWithFormat:@"%@%@",singleton.apiMode.ChatBaseUrl,apiURL];
    NSLog(@" requestURL =%@",requestURL);
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestURL]  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    NSData *requestData=[jsonInputStringAESEncrypted dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPMethod:isPost?@"POST":@"GET"];
    [request setHTTPBody: requestData];
    [request setValue:@"application/json" forHTTPHeaderField:API_CONTENT_TYPE];
    
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString*  responseString;
            //            NSString *JsonOutStr=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            //            responseString=[self decryptionChatAction:JsonOutStr];
            
            //NSLog(@" headers =%@",headers);
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            NSInteger errorCode=[httpResponse statusCode];
            if (!error) {
                
                
                NSString *JsonOutStr=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                responseString=[self decryptionChatAction:JsonOutStr];
                
                
                NSError *errorResponse;
                
                if([responseString length]==0)
                {
                    NSError* error = nil;
                    
                    NSMutableDictionary* details = [NSMutableDictionary dictionary];
                    // [details setValue:@"Invalid Response Value,please retry" forKey:NSLocalizedDescriptionKey];
                    
                    [details setValue:NSLocalizedString(@"network_error_txt",nil) forKey:NSLocalizedDescriptionKey];
                    
                    
                    
                    // populate the error object with the details
                    error = [NSError errorWithDomain:@"Umang" code:errorCode userInfo:details];
                    
                    //invalid  not equal
                    completionBlock(nil,error,tag);
                }
                NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
                id jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                  options:kNilOptions
                                                                    error:&errorResponse];
                
                //NSLog(@" jsonResponse =%@",jsonResponse);
                
                if (errorResponse == nil)
                {
                    // Success Case
                    if ([[jsonResponse objectForKey:@"status"]integerValue] == 0)
                    {
                        completionBlock(jsonResponse,errorResponse,tag);
                    }
                    else
                    {
                        NSString *errorString = [jsonResponse objectForKey:@"rd"];
                        if ([errorString isKindOfClass:[NSNull class]] || errorString.length == 0 || [errorString isEqualToString:@"null"]) {
                            // errorString = @"Unexpected error occured. Please try after some time.";
                            errorString = NSLocalizedString(@"network_error_txt",nil);
                            
                            
                            
                        }
                        
                        NSMutableDictionary* details = [NSMutableDictionary dictionary];
                        [details setValue:errorString forKey:NSLocalizedDescriptionKey];
                        
                        //NSError *serverError = [NSError errorWithDomain:@"Unexpected error occured. Please try after some time" code:errorCode userInfo:details];
                        
                        NSError *serverError = [NSError errorWithDomain: NSLocalizedString(@"network_error_txt",nil) code:errorCode userInfo:details];
                        
                        
                        
                        completionBlock(jsonResponse,serverError,tag);
                        //completionBlock(jsonResponse,errorResponse,tag);
                        
                    }
                    
                }
                else{
                    
                    
                    completionBlock(nil,errorResponse,tag);
                }
            }
            
            
            else //all other codes
            {
                NSLog(@"Error Occured = %@",error.localizedDescription);
                
                NSError *serverError=error;
                
                
                NSString *string =[NSString stringWithFormat:@"%@",error.localizedDescription];
                if ([string rangeOfString:@"The request timed out"].location == NSNotFound  )
                    //||[string rangeOfString:@"Could not connect to the server"].location == NSNotFound
                {
                    NSLog(@"string dont contains!");
                    
                } else
                {
                    NSInteger errorCode=[httpResponse statusCode];
                    
                    NSMutableDictionary* details = [NSMutableDictionary dictionary];
                    [details setValue:NSLocalizedString(@"network_error_txt",nil)  forKey:NSLocalizedDescriptionKey];
                    
                    serverError = [NSError errorWithDomain:NSLocalizedString(@"network_error_txt",nil) code:errorCode userInfo:details];
                    
                    
                    //completionBlock(nil,error,tag);
                    
                }
                
                
                
                completionBlock(nil,serverError,tag);
                
                //completionBlock(nil,error,tag);
            }
        });
    }];
    
    [postDataTask resume];
}


#pragma mark- === Keys Api
-(void)hitKeyEncryptionApi:(NSString*)token withComplition:(void(^)(id response))complition {
    singleton = [SharedManager sharedSingleton];

    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setValue:token forKey:@"itkn"];
    // Add common params here only
    [dict addEntriesFromDictionary:[self getCommonParametersForRequestBody]];
    
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *bodyString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *strKeyHash = @"2ZvQXEHvshw45A1KOen/BVQuuLM=";
    NSString *keyAes = [NSString stringWithFormat:@"%@%@",[strKeyHash substringToIndex:8], [token substringToIndex:8]];
    //NSString *jsonInputStringAESEncrypted = [bodyString AES128EncryptWithKey:keyAes];
    NSString *resultEncryptString=  [bodyString EncryptData:bodyString saltkey:strKeyHash withKey:keyAes];
    NSString *strFinalBody = [NSString stringWithFormat:@"%@,%@",resultEncryptString,token];
    NSLog(@"Encrypted key response -- %@", strFinalBody);
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSString *requestURL=[NSString stringWithFormat:@"%@%@",singleton.apiMode.WSO2PathBASE,UM_API_INSTANCE_ID];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestURL]  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    NSData *requestData=[strFinalBody dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody: requestData];
    [request setValue:@"text/plain" forHTTPHeaderField:API_CONTENT_TYPE];
    [request setValue:@"r" forHTTPHeaderField:API_K_TYPE];
    //[request setValue:jsonEncryptedRequestBody forHTTPHeaderField:API_REQUEST_VALUE];
    [request setValue:singleton.apiMode.UMAuthorization forHTTPHeaderField:API_REQUEST_AUTHORIZATION];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString*  responseString;
            
            if (!error) {
                //NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
                //NSLog(@" headers =%@",headers);
                
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                //NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
                
                responseString=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSString *JsonOutStr=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                NSLog(@"JsonOutStr is empty bahubali");
                responseString = [JsonOutStr DecryptData:JsonOutStr saltkey:strKeyHash withKey:keyAes];
                NSError *errorResponse;
                
                if([responseString length]==0)
                {
                    NSError* error = nil;
                    NSMutableDictionary* details = [NSMutableDictionary dictionary];
                    [details setValue:@"Invalid Response Value,please retry" forKey:NSLocalizedDescriptionKey];
                    // populate the error object with the details
                    error = [NSError errorWithDomain:@"Umang" code:200 userInfo:details];
                    //invalid  not equal
                    //  completionBlock(nil,error,tag);
                }
                NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
                id jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                  options:kNilOptions
                                                                    error:&errorResponse];
                
                //NSLog(@" jsonResponse =%@",jsonResponse);
                
                if (errorResponse == nil) {
                    // Success Case
                    if ([[jsonResponse objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[jsonResponse objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                    {
                        
                        [self processKeyEncryptResponse:jsonResponse];
                        complition(jsonResponse);
                        //completionBlock(jsonResponse,errorResponse,tag);
                    }
                    else
                    {
                        NSString *errorString = [jsonResponse objectForKey:@"rd"];
                        if ([errorString isKindOfClass:[NSNull class]] || errorString.length == 0 || [errorString isEqualToString:@"null"]) {
                            errorString = @"Unexpected error occured. Please try after some time.";
                        }
                        NSMutableDictionary* details = [NSMutableDictionary dictionary];
                        [details setValue:errorString forKey:NSLocalizedDescriptionKey];
                        
                        NSError *serverError = [NSError errorWithDomain:@"UMANG SERVER ERROR" code:200 userInfo:details];
                        //completionBlock(nil,serverError,tag);
                    }
                }
            }//close else
        });
    }];
    [postDataTask resume];
    
    
}
-(void)processKeyEncryptResponse:(id)response {
    
    NSString *saltAes = [response valueForKey:@"rsalt"];
    [self saveKeyIntoKeyChainWithKey:saltAes withKeyTag:KEYCHAIN_SaltAES];
    
    NSString *SaltRequest = [response valueForKey:@"value"];
    [self saveKeyIntoKeyChainWithKey:SaltRequest withKeyTag:KEYCHAIN_SaltRequestVaue];
    
    NSString *ctrl = [response valueForKey:@"ctrl"];
    [self saveKeyIntoKeyChainWithKey:ctrl withKeyTag:KEYCHAIN_SaltRequestControl];
    
    NSString *msalt = [response valueForKey:@"msalt"];
    [self saveKeyIntoKeyChainWithKey:msalt withKeyTag:KEYCHAIN_SaltMPIN];
    
    NSString *rdigi = [response valueForKey:@"rdigi"];
    [self saveKeyIntoKeyChainWithKey:rdigi withKeyTag:KEYCHAIN_SaltAESDigilocker];
    
    NSString *dgvalue = [response valueForKey:@"dgvalue"];
    [self saveKeyIntoKeyChainWithKey:dgvalue withKeyTag:KEYCHAIN_SaltRequestVaueDigiLocker];
    
    NSString *dgctrl = [response valueForKey:@"dgctrl"];
    [self saveKeyIntoKeyChainWithKey:dgctrl withKeyTag:KEYCHAIN_SaltRequestDigiLockerControl];
    
    NSString *kreq = [response valueForKey:@"kreq"];
    [self saveKeyIntoKeyChainWithKey:kreq withKeyTag:KEYCHAIN_KEY_JSON];
    
    NSString *kchat = [response valueForKey:@"kchat"];
    [self saveKeyIntoKeyChainWithKey:kchat withKeyTag:KEYCHAIN_KEY_CHAT];
    NSString *kdigi = [response valueForKey:@"kdigi"];
    [self saveKeyIntoKeyChainWithKey:kdigi withKeyTag:KEYCHAIN_KEY_DIGILOCKER];
    
    //    "rsalt": "@#digitalspice*\u0026", --SaltAES
    //    "value": "R%d\u0026Wst676#(Na",--SaltRequestVaue
    //    "ctrl": "$f%GY#JX^9H@",--SaltRequestControl
    //    "msalt": "56$f@D8H2x^",-SaltMPIN
    //    "rdigi": "@#localedigitalumang*\u0026",--SaltAESDigilocker
    //    "dgvalue": "D@GUM4NG$#4PP",--SaltRequestVaueDigiLocker
    //    "dgctrl": "$f%GY#JX^9H@", ---SaltRequestControlDigiLocker
    //    "kreq": "@#umANG#$%\u0026etKey",--- Key JSON
    //    "kchat": "uMaNgcHat@SpiCey",-- Key Chat
    //    "kdigi": "$%loVHG@\u0026%*eaSey" --- KEY Digilocker
}






//===================================================================================
//=================== NEW PARAMETER ADDED IN DEV MODE ===============================
//===================================================================================
-(NSMutableDictionary*)getInternationalparameter:(REQUEST_TAG)apiTag
{
    NSMutableDictionary *interNationalDict = [NSMutableDictionary new];
   

    NSLog(@"apiTag =%u",apiTag);
    //TAG_REQUEST_UMOBILE
     //(for Update Mobile Numberb in this also send nccode (country code for new mobile number))
    //TAG_REQUEST_UPDATE_PROFILE
    //(for Update Profile in this also send accode (country code for alternate mobile number))
  
    // New mobile number Code for validate
    //================ TAG_REQUEST_UMOBILE ===========================================
    if (apiTag==TAG_REQUEST_UMOBILE)
    {
        // Need ccode
        // need nccode
        // no need accode
        
        [interNationalDict addEntriesFromDictionary:[self need_ccCodeDic]];
        [interNationalDict addEntriesFromDictionary:[self need_nccodeDic]];
        [interNationalDict setObject:@"" forKey:@"accode"];
        
        NSLog(@"TAG_REQUEST_UMOBILE interNationalDict =%@",interNationalDict);
        return interNationalDict;

        
    }
   
    
    
    
    // Update Profile
    //================ TAG_REQUEST_UPDATE_PROFILE ===========================================
    else if (apiTag==TAG_REQUEST_UPDATE_PROFILE)
    {
        // Need ccode
        // no need nccode
        // need accode if available
        
        [interNationalDict addEntriesFromDictionary:[self need_ccCodeDic]];
        [interNationalDict setObject:@"" forKey:@"nccode"];
        [interNationalDict addEntriesFromDictionary:[self need_accCodeDic]];
        NSLog(@"TAG_REQUEST_UPDATE_PROFILE interNationalDict =%@",interNationalDict);

        return interNationalDict;

    }
    
    // Alternate mobile validate
    //================ TAG_REQUEST_ALTERNATE_VALIDATE_OTP ===========================================
    else if (apiTag==TAG_REQUEST_ALTERNATE_VALIDATE_OTP)
    {
        // No Need ccode or // pass accode in ccode
        // No need nccode
        
        [interNationalDict addEntriesFromDictionary:[self accCodeDic_As_ccCodeDic]];
        [interNationalDict setObject:@"" forKey:@"nccode"];
        [interNationalDict setObject:@"" forKey:@"accode"];

        NSLog(@"TAG_REQUEST_ALTERNATE_VALIDATE_OTP interNationalDict =%@",interNationalDict);

        return interNationalDict;

        
    }
    
    //update alternate mobile number
    //================ TAG_REQUEST_UPDATEALTERNATEMOBILE ===========================================
    else if (apiTag==TAG_REQUEST_UPDATEALTERNATEMOBILE)
    {
        // No Need ccode or // pass accode in ccode
        // No need nccode
        
        [interNationalDict addEntriesFromDictionary:[self accCodeDic_As_ccCodeDic]];
        [interNationalDict setObject:@"" forKey:@"nccode"];
        [interNationalDict setObject:@"" forKey:@"accode"];

        NSLog(@"TAG_REQUEST_UPDATEALTERNATEMOBILE interNationalDict =%@",interNationalDict);

        return interNationalDict;

       

    }
    
    //================ TAG_REQUEST_UPDATE_PROFILE ===========================================

   else
   {
       //  Need ccode
       // No need nccode
       // No need accode

       [interNationalDict addEntriesFromDictionary:[self need_ccCodeDic]];
       [interNationalDict setObject:@"" forKey:@"nccode"];
       [interNationalDict setObject:@"" forKey:@"accode"];
       NSLog(@"OTHER API interNationalDict =%@",interNationalDict);

       return interNationalDict;

       
   }

    
    return interNationalDict;
}


/*
 need_ccCodeDic
 need_nccodeDic
 need_accCodeDic
 accCodeDic_As_ccCodeDic
 */


/// METHOD TO USE THEM ALL===========
//need accCode as ccode
-(NSMutableDictionary*)accCodeDic_As_ccCodeDic
{
    SharedManager *singleton = [SharedManager sharedSingleton];

    NSMutableDictionary * accCodeDictAsCcode = [NSMutableDictionary new];
    
    NSString *accode = singleton.accode;
    if (accode == nil)
    {
        accode =@"";
    }
    [accCodeDictAsCcode setObject:accode forKey:@"ccode"];
    
    return accCodeDictAsCcode;
}

//need ccCodeDic


-(NSMutableDictionary*)need_ccCodeDic
{
    SharedManager *singleton = [SharedManager sharedSingleton];

    NSMutableDictionary * ccCodeDict = [NSMutableDictionary new];

    NSString *ccode = singleton.ccode;
    if (ccode == nil)
    {
        ccode =@"";
    }
    [ccCodeDict setObject:ccode forKey:@"ccode"];
    
    return ccCodeDict;
}


//need accCodeDic
-(NSMutableDictionary*)need_accCodeDic
{
    SharedManager *singleton = [SharedManager sharedSingleton];

    NSMutableDictionary * accCodeDict = [NSMutableDictionary new];
    
    NSString *accode = singleton.accode;
    if (accode == nil)
    {
        accode =@"";
    }
    [accCodeDict setObject:accode forKey:@"accode"];
    
    return accCodeDict;
}


//need neednccodeDic
-(NSMutableDictionary*)need_nccodeDic
{
    SharedManager *singleton = [SharedManager sharedSingleton];
    NSMutableDictionary * nccCodeDict = [NSMutableDictionary new];
    NSString *nccode = singleton.nccode;
    if (nccode == nil)
    {
        nccode =@"";
    }
    [nccCodeDict setObject:nccode forKey:@"nccode"];
    return nccCodeDict;
}


//===================================================================================
//================ END NEW PARAMETER ADDED IN DEV MODE ==============================
//===================================================================================




@end



