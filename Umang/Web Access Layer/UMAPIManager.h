//
//  UMAPIManager.h
//  Umang
//
//  Created by admin on 14/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "APIConstant.h"

typedef enum {
    TAG_REQUEST_TOUR_IMAGE = 100,
    TAG_REQUEST_NEWTOUR_IMAGE,
    TAG_REQUEST_INIT_REG,
    TAG_REQUEST_VALIDATE_OTP,
    TAG_REQUEST_UPDATE_QUESTION,
    TAG_REQUEST_SET_MPIN,
    TAG_REQUEST_TRANSACTION_HISTORY,
    TAG_REQUEST_GENERATE_AADHAR_OTP,
    TAG_REQUEST_UPDATE_MPIN,
    TAG_REQUEST_VALID_AADHAR,
    TAG_REQUEST_VALID_DIGILOCKER,
    TAG_REQUEST_LINK_AADHAR,
    TAG_REQUEST_UPDATE_PROFILE,
    TAG_REQUEST_LOGIN,
    TAG_REQUEST_HOME_SCREEN_DATA,
    TAG_REQUEST_HERO_SPACE,
    TAG_REQUEST_IVR_OTP,
    TAG_REQUEST_UNSET_FAVORITE,
    TAG_REQUEST_LOGOUT,
    TAG_REQUEST_FETCH_USER_RATING,
    TAG_REQUEST_API_RATINGS,
    TAG_REQUEST_STATEWISE_SERVICE,
    TAG_REQUEST_VIEW_PROFILE,
    TAG_REQUEST_FETCH_CITY,
    TAG_REQUEST_FETCH_DPT_MSG,
    TAG_REQUEST_CHANGE_MPIN,
    TAG_REQUEST_DELETE_PROFILE,
    TAG_REQUEST_NOTIFICATION_SETTINGS,
    TAG_REQUEST_LOGIN_WITH_OTP,
    TAG_REQUEST_UPDATEALTERNATEMOBILE,
    TAG_REQUEST_RESENDEMAILVERIFY,
    TAG_REQUEST_APPSEARCH,
    TAG_REQUEST_LINK_WITH_SOCIAL,
    TAG_REQUEST_UNLINK_WITH_SOCIAL,
    TAG_REQUEST_INIT,
    TAG_REQUEST_UPDATE_GCM_TOKEN,
    TAG_REQUEST_APPKEYSEARCH,
    TAG_REQUEST_FSTQU,
    TAG_REQUEST_FEEDBACK,
    TAG_REQUEST_FTAL,
    TAG_REQUEST_UMOBILE,
    TAG_REQUEST_VMPIN,
    TAG_REQUEST_APPCATSRCH,
    TAG_REQUEST_TRANSACTION_FILTER,
    TAG_REQUEST_USERSERVICETANSLOGS,
    TAG_REQUEST_FTHDS,
    TAG_REQUEST_TUK,
    TAG_REQUEST_ALTERNATE_VALIDATE_OTP,
    // Digilocker Tags
    TAG_REQUEST_DIGILOCKER_AADHAR_VERIFY,
    TAG_REQUEST_DIGILOCKER_GETTOKEN,
    TAG_REQUEST_DIGILOCKER_GETREFRESHTOKEN,
    TAG_REQUEST_DIGILOCKER_DOWNLOADDOC,
    TAG_REQUEST_DIGILOCKER_UPLOADDOC,
    TAG_REQUEST_VALIDATE_ANSWER,
    TAG_REQUEST_CHAT,
    TAG_REQUEST_SERVICE_DIRECTORY,
    TAG_REQUEST_STATE,
    TAG_REQUEST_DELETE_SESSION
    
}REQUEST_TAG;



// API Completion Block with Response
typedef void(^APICompletionBlock)(id response, NSError *error , REQUEST_TAG tag);

@interface ApiMode : NSObject
@property(strong,nonatomic)NSString *WSO2PathBASE;
@property(strong,nonatomic)NSString *UMAuthorization;
@property(strong,nonatomic)NSString *PACKAGE_NAME;
@property(strong,nonatomic)NSString *ChatBaseUrl;
@property(strong,nonatomic)NSString *RefreshTokenBearer;
@property(strong,nonatomic)NSString *DigiLockerBearer;
@property(strong,nonatomic)NSString *DepartmentBearerToken;
@property(strong,nonatomic)NSString *GOOGLE_SIGNIN_CLIENT_ID;
@property(strong,nonatomic)NSString *XMPP_SERVER_HOST;
@property(strong,nonatomic)NSString *GOOGLE_SCHEME;




- (instancetype)initCustomize;

@end



@interface UMAPIManager : NSObject


-(void)clearValueOnlogout;

-(void)hitWebServiceAPIWithPostMethod:(BOOL)isPost isAccessTokenRequired:(BOOL)tokenRequired webServiceURL:(NSString *)apiURL withBody:(id)requestBody andTag:(REQUEST_TAG)tag completionHandler:(APICompletionBlock)completionBlock;


-(void)hitWebServiceSearchPostMethod:(BOOL)isPost isAccessTokenRequired:(BOOL)tokenRequired webServiceURL:(NSString *)apiURL withBody:(id)requestBody andTag:(REQUEST_TAG)tag completionHandler:(APICompletionBlock)completionBlock;

-(void)hitXMPPChatWebServiceAPIWithPostMethod:(BOOL)isPost isAccessTokenRequired:(BOOL)tokenRequired webServiceURL:(NSString *)apiURL withBody:(id)requestBody andTag:(REQUEST_TAG)tag completionHandler:(APICompletionBlock)completionBlock;

-(void)hitAPIForDigiLockerAuthenticationWithPost:(BOOL)isPost isAccessTokenRequired:(BOOL)tokenRequired webServiceURL:(NSString *)apiURL withBody:(id)requestBody andTag:(REQUEST_TAG)tag completionHandler:(APICompletionBlock)completionBlock;



#pragma mark - Dept API Methods

-(void)hitWebServiceDeptAPIWithPostMethodForDept:(NSString *)deptName andIsPost:(BOOL)isPost isAccessTokenRequired:(BOOL)tokenRequired webServiceURL:(NSString *)apiURL withBody:(id)requestBody andTag:(REQUEST_TAG)tag completionHandler:(APICompletionBlock)completionBlock;

-(void)hitAPIForDigiLockerDownloadFileWithPost:(BOOL)isPost isAccessTokenRequired:(BOOL)tokenRequired webServiceURL:(NSString *)apiURL withBody:(id)requestBody andTag:(REQUEST_TAG)tag completionHandler:(APICompletionBlock)completionBlock;

-(void)hitAPIForDigiLockerUploadFileWithPost:(BOOL)isPost isAccessTokenRequired:(BOOL)tokenRequired webServiceURL:(NSString *)apiURL withBody:(id)requestBody andTag:(REQUEST_TAG)tag andFilePath:(NSString *)filePath completionHandler:(APICompletionBlock)completionBlock;

-(void)hitKeyEncryptionApi:(NSString*)token withComplition:(void(^)(id response))complition;

- (NSString*)joinQueryWithDictionary:(NSDictionary*)dictionary;

-(NSData *)multipartDataWithParameters:(NSDictionary *)parameters boundary:(NSString **)boundary;
-(NSMutableDictionary*)getCommonParametersForRequestBody;
/*
 -(NSArray*)getFilteredServiceData:(NSString*)sortBy serviceType:(NSString*)serviceType stateIdAlist:(NSArray*)stateIdAlist categoryList:(NSArray*)categoryList;
 */
@end
