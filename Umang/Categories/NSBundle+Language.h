//
//  LanguageTableCell.h
//  Umang
//
//  Created by admin on 16/09/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (Language)

+(void)setLanguage:(NSString*)language;

@end
