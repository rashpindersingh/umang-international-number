//
//  ServiceNotAvailableView.h
//  Umang
//
//  Created by admin on 27/09/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterNoResultView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *imgNoFilter;
@property (weak, nonatomic) IBOutlet UIButton *btnEditFilter;
@property (weak, nonatomic) IBOutlet UILabel *lblHeading;
@property (weak, nonatomic) IBOutlet UILabel *lblSubHeading;

-(void)setFilterBtnTitle;


@end
