//
//  MyTextField.h
//  Umang
//
//  Created by deepak singh rawat on 07/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

//create delegate protocol
@protocol MyTextFieldDelegate <NSObject>
@optional
//- (void)textFieldDidDelete;
- (void)textFieldDidDelete:(UITextField *)textField;
- (void)textFieldDidBeginEditing:(UITextField *)textField;
- (void)textFieldDidEndEditing:(UITextField *)textField;

@end

@interface MyTextField : UITextField<UIKeyInput>

//create "myDelegate"
@property (nonatomic, assign) id<MyTextFieldDelegate> myDelegate;

@end
