//
//  OTPEntryView.h
//  Tripmaester
//
//  Created by aditi on 06/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UMUnderLineTextField.h"


@protocol OTPEntryDelegate <NSObject>

@required;
-(void)otpEntryDoneSuccessfully:(id)sender;
-(void)invalidOTPEntry:(id)sender;

@end


@interface OTPEntryView : UIView
{
  UIToolbar *keyboardToolbar;
}

@property(weak,nonatomic) id <OTPEntryDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lblEntryPin;

@property(nonatomic,strong)NSString *headerTitle;
@property(nonatomic,strong)NSString *enterdOTPText;


@property (weak, nonatomic) IBOutlet UMUnderLineTextField *pin1;
@property (weak, nonatomic) IBOutlet UMUnderLineTextField *pin2;
@property (weak, nonatomic) IBOutlet UMUnderLineTextField *pin3;
@property (weak, nonatomic) IBOutlet UMUnderLineTextField *pin4;



@end
