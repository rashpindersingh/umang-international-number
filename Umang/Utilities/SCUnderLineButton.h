//
//  SCUnderLineButton.h
//  SelfcareApp
//
//  Created by admin on 8/29/16.
//  Copyright © 2016 SpiceLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCUnderLineButton : UIControl

@property(nonatomic,strong)UILabel *lblHeader;
@property (nonatomic, strong) CALayer *borderLayer;

- (instancetype)initWithFrame:(CGRect)frame withTitle:(NSString*)title;

-(void)setSelected:(BOOL)isSelected;

@end
