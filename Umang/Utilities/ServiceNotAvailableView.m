//
//  ServiceNotAvailableView.m
//  Umang
//
//  Created by admin on 27/09/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "ServiceNotAvailableView.h"
#import "UIImageView+WebCache.h"
@implementation ServiceNotAvailableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"ServiceNotAvailable" owner:self options:nil];
    self = (ServiceNotAvailableView *)[objects objectAtIndex:0];
    self.frame = frame;
    self.btnSubHeading.titleLabel.numberOfLines = 4.0;
    self.btnSubHeading.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
//    self.backgroundColor = [UIColor clearColor];
//    self.imageBackground.backgroundColor = [UIColor clearColor];
//    self.lblSubHeading.backgroundColor = [UIColor clearColor];

    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)setBtnSubHeadingTitle:(NSString*)stateName
{
    
    //NSString *msg = [NSString stringWithFormat:NSLocalizedString(@"noService_withStateName", nil),stateName];
    
    //"coming_soon"
    
    NSString *msg = [NSString stringWithFormat:NSLocalizedString(@"coming_soon", nil)];
    
    
    NSString *stateSelected = NSLocalizedString(@"select_state", nil);
    if (stateName.length == 0 || [stateName isEqualToString:[stateSelected uppercaseString]]) {
        msg = NSLocalizedString(@"no_services_to_display", nil);
    }
    [self.btnSubHeading setTitle:msg forState:UIControlStateNormal] ;
    
}
-(void)setBackgroundImageWithUrl:(NSString*)url {
    
    NSURL *nsurl=[NSURL URLWithString:url];
        //set your image on main thread.
        __weak typeof(self) weakSelf = self;
    //dispatch_async(dispatch_get_main_queue(),^{
     [weakSelf.imageBackground sd_setImageWithURL:nsurl
                   placeholderImage:[UIImage imageNamed:@"UMANG_noService"]];
      //});
  
}
@end
