//
//  AppFont.h
//  Umang
//
//  Created by admin on 24/10/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppFont : UIFont {
     CGFloat fontScale;
     CGFloat defaultFontSize;
}
+ (UIFont *)regularFont:(CGFloat)size;
+ (UIFont *)boldFont:(CGFloat)size;
+ (UIFont *)lightFont:(CGFloat)size;
+ (UIFont *)heavyFont:(CGFloat)size;
+ (UIFont *)semiBoldFont:(CGFloat)size;
+ (UIFont *)ultraLightFont:(CGFloat)size;
+ (UIFont *)mediumFont:(CGFloat)size;
+(BOOL)isIPad;

@end
