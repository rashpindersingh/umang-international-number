//
//  JPBFloatingTextViewController.m
//  Pods
//
//  Created by Joseph Pintozzi on 8/22/14.
//
//

#import "JPBFloatingTextViewController.h"
#define fDeviceWidth ([UIScreen mainScreen].bounds.size.width)
#define fDeviceHeight ([UIScreen mainScreen].bounds.size.height)
@interface JPBFloatingTextViewController () {
    UILabel *_titleLabel;
    UILabel *_subtitleLabel;
    UILabel *_gendertitleLabel;

    UIView *_labelBackground;
}

@end

@implementation JPBFloatingTextViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];

    _labelBackground = [[UIView alloc] initWithFrame:CGRectMake(0, [self headerHeight] - 60, self.view.frame.size.width, 60)];
    [self addHeaderOverlayView:_labelBackground];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(fDeviceWidth/2-(self.view.frame.size.width - 15 - [self horizontalOffset])/4+10, [self headerHeight] - 75, self.view.frame.size.width - 15 - [self horizontalOffset], 25)];
    [_titleLabel setBackgroundColor:[UIColor clearColor]];
    [_titleLabel setTextColor:[UIColor whiteColor]];
    [_titleLabel setFont:[UIFont boldSystemFontOfSize:17]];
    [_titleLabel setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin];
    [self addHeaderOverlayView:_titleLabel];
    
    
    
    _gendertitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(fDeviceWidth/2-(self.view.frame.size.width - 15 - [self horizontalOffset])/4+50, [self headerHeight] - 40, self.view.frame.size.width - 15 - [self horizontalOffset], 15)];
    [_gendertitleLabel setBackgroundColor:[UIColor clearColor]];
    [_gendertitleLabel setTextColor:[UIColor whiteColor]];
    [_gendertitleLabel setFont:[UIFont systemFontOfSize:12]];
    [_gendertitleLabel setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin];
    [self addHeaderOverlayView:_gendertitleLabel];
    

    _subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(fDeviceWidth/2-(self.view.frame.size.width - 15 - [self horizontalOffset])/4+35, [self headerHeight] - 25, self.view.frame.size.width - 15 - [self horizontalOffset], 15)];
    [_subtitleLabel setBackgroundColor:[UIColor clearColor]];
    [_subtitleLabel setTextColor:[UIColor whiteColor]];
    [_subtitleLabel setFont:[UIFont systemFontOfSize:12]];
    [_subtitleLabel setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin];
    [self addHeaderOverlayView:_subtitleLabel];
    

    
    
    
    
}



- (void)setGendertitleText:(NSString*)text{
    [_gendertitleLabel setText:text];
}


- (CGFloat)horizontalOffset{
    return 15.0f;
}

- (void)setTitleText:(NSString*)text{
    [_titleLabel setText:text];
}

- (void)setSubtitleText:(NSString*)text{
    [_subtitleLabel setText:text];
}

- (void)selLabelBackground:(UIColor*)color{
    _labelBackground.backgroundColor = color;
}


- (void)setTitleFont:(UIFont*)font{
    [_titleLabel setFont:font];

}

- (void)setSubtitleFont:(UIFont*)font{
    [_subtitleLabel setFont:font];
    
}

- (void)setTitleTextColor:(UIColor*)color{
    [_titleLabel setTextColor:color];
    
}


- (void)setSubtitleTextColor:(UIColor*)color{
    [_subtitleLabel setTextColor:color];
    [_gendertitleLabel setTextColor:color];

}

- (void)setLabelBackgroundGradientColor:(UIColor*)bottomColor{
    //build gradient with top clear
    UIColor *topColor = [UIColor clearColor];
    NSArray *gradientColors = [NSArray arrayWithObjects:(id)topColor.CGColor, (id)bottomColor.CGColor, nil];
    NSArray *gradientLocations = [NSArray arrayWithObjects:[NSNumber numberWithInt:0.0],[NSNumber numberWithInt:1.0], nil];
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.colors = gradientColors;
    gradientLayer.locations = gradientLocations;
    gradientLayer.frame = CGRectMake(0, 0, CGRectGetWidth(_labelBackground.frame), CGRectGetHeight(_labelBackground.frame));
    
    [_labelBackground.layer insertSublayer:gradientLayer atIndex:0];
}

@end
