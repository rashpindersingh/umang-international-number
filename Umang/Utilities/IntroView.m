//
//  IntroView.m
//  IntroGuide
//
//  Created by spice on 24/10/16.
//  Copyright (c) 2016 Deepak. All rights reserved.
//
#import "IntroView.h"
 //#define DEFAULT_BLUE [UIColor colorWithRed:60.0/255.0 green:80.0/255.0 blue:146.0/255.0 alpha:1.0]


@implementation IntroView
{
    CAShapeLayer *mask;
    NSArray *introElements;
    NSArray *introTextElements;

    NSUInteger currentIndex;
    UIButton *btn_next;
    UIPageControl *pageControl;
    
    
    UIButton *btn_skip;
    //------- For Arrow Point Start----------
    CGPoint startPoint;
    CGPoint endPoint;
    CGFloat tailWidth;
    CGFloat headWidth;
    CGFloat headLength;
    UIBezierPath *arrowpath;
    //------- For Arrow Point End----------

}





- (id)initWithFrame:(CGRect)frame introElements:(NSArray *)iElements introTextElements:(NSArray *)iTextElements

 {
    self = [super initWithFrame:frame];
    if (self) {
        
        // Initialization code
        
        introElements = iElements;
        introTextElements=iTextElements;
        
        mask = [CAShapeLayer layer];
        [mask setFillColor:[[UIColor blackColor] colorWithAlphaComponent:0.7f].CGColor];
        [mask setFillRule:kCAFillRuleEvenOdd];
        [self.layer addSublayer:mask];

        //---- adding swipe left -----
        UISwipeGestureRecognizer *swipeGestureRecognizerLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(userDidSwipeLeft)];
        swipeGestureRecognizerLeft.direction = UISwipeGestureRecognizerDirectionLeft;
        [self addGestureRecognizer:swipeGestureRecognizerLeft];
        
        
        
        //---- adding swipe Right -----

        UISwipeGestureRecognizer *swipeGestureRecognizerRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(userDidSwipeRight)];
        swipeGestureRecognizerRight.direction = UISwipeGestureRecognizerDirectionRight;
        [self addGestureRecognizer:swipeGestureRecognizerRight];
    }
    
    return self;
}

- (void)userDidSwipeLeft
{
    [self showIntroAtIndex:currentIndex+1];
}


- (void)userDidSwipeRight
{
    if (currentIndex<=0) {
        currentIndex=1;
    }
    [self showIntroAtIndex:currentIndex-1];
}

- (void)showIntro
{
    //Initial index by default
    [self showIntroAtIndex:0];
}

- (void)showIntroAtIndex:(NSUInteger)index
{
    btn_next= [UIButton buttonWithType:UIButtonTypeCustom];

 
    
    
    if (index<[introElements count]-1) {
        [btn_next setTitle:@"NEXT" forState:UIControlStateNormal];
     }
    else
    {
        [btn_next setTitle:@"FINISH" forState:UIControlStateNormal];
        if (index>=[introElements count])
                    {
                        index=index-1;
                }
        // btn_skip.hidden=TRUE;

    }
    NSLog(@"index=%lu",(unsigned long)index);
    
    currentIndex = index;
    
    CGRect rect = [[introElements objectAtIndex:index] CGRectValue];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRect:self.bounds];
   
    CGFloat widthMask=rect.size.width+8;//+8 for increase left space from button
    CGFloat heightMask=rect.size.width+8;//+8 for increase right space from button and both eight width are of size width cause of drawing a perfect Circle

    
    CGFloat xCordinate=rect.origin.x-4; //-4 from left spacing

    CGFloat yCordinate=rect.origin.y-rect.size.width/2+rect.size.height/2-4;//-4 for right space and adding rect.size.height/2  for center of given frame button

    UIBezierPath *elementPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(xCordinate,yCordinate , widthMask, heightMask) ];
 
    [maskPath appendPath:elementPath];
    
    // Animate
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"path"];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    animation.duration = 0.3f;
    animation.fromValue = (__bridge id)(mask.path);
    animation.toValue = (__bridge id)(maskPath.CGPath);
    [mask addAnimation:animation forKey:@"path"];
    mask.path = maskPath.CGPath;
    [self drawLine:yCordinate+heightMask];
    //-------Button Next---------//
    [btn_next addTarget:self
                 action:@selector(btn_nextAction:)
       forControlEvents:UIControlEventTouchUpInside];
    btn_next.titleLabel.textColor=[UIColor whiteColor];
    btn_next.frame = CGRectMake(fDeviceWidth/2-70, fDeviceHeight-120, 140.0, 40.0);
    btn_next.backgroundColor=DEFAULT_BLUE;
    [self addSubview:btn_next];
    //--- add page page control---
    pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(fDeviceWidth/2-25, fDeviceHeight-150, 50.0, 40.0)]; //SET a property of UIPageControl
    pageControl.numberOfPages = [introElements count]; //as we added 3 diff views
    pageControl.currentPage = currentIndex;
    pageControl.pageIndicatorTintColor = [UIColor grayColor];
    pageControl.currentPageIndicatorTintColor = [UIColor whiteColor ];
    [self addSubview:pageControl];

    
    //-------Button Skip---------//
    btn_skip= [UIButton buttonWithType:UIButtonTypeCustom];
    [btn_skip addTarget:self
               action:@selector(btn_skipAction:)
     forControlEvents:UIControlEventTouchUpInside];
    [btn_skip setTitle:@"SKIP" forState:UIControlStateNormal];
    btn_skip.frame = CGRectMake(fDeviceWidth/2-70, fDeviceHeight-80, 140.0, 40.0);
    btn_skip.backgroundColor=[UIColor clearColor];
    btn_skip.tag=7;
    //-------Button Skip label---------//
    UIView *vw_line=[[UIView alloc]initWithFrame:CGRectMake(btn_skip.frame.origin.x+btn_skip.frame.size.width/3, fDeviceHeight-80+30, 50.0, 2.0)];
    vw_line.backgroundColor=[UIColor whiteColor];
    vw_line.tag=7;
    
    if ( [btn_next.titleLabel.text isEqualToString:@"FINISH"]) {
        
        for (UIView *subview in [self subviews]) {
            if (subview.tag == 7) {
                [subview removeFromSuperview];
              //  [vw_line removeFromSuperview];


            }
        }
    }
    else
    {
        [self addSubview:btn_skip];
        [self addSubview:vw_line];

    
    }
    
    
    //[self drawText:yCordinate+heightMask];

}
-(void)btn_nextAction:(id)sender
{
    if ( [btn_next.titleLabel.text isEqualToString:@"FINISH"]) {
       
        [self removeFromSuperview];

    }
    else
    {
    [self showIntroAtIndex:currentIndex+1];
    }
    
}

-(void)btn_skipAction:(id)sender
{
    //[self showIntroAtIndex:currentIndex+1];
    [self removeFromSuperview];
}


-(void)drawLine:(CGFloat)height
{
    

    for (CALayer *layer in [self.layer sublayers]) {
        
        if ([[layer name] isEqualToString:@"lineKey"]) {
            [layer removeFromSuperlayer];
            break;
        }
        
       
    }

     //---- Draw vertical line------
  /*  CGRect rect = [[introElements objectAtIndex:currentIndex] CGRectValue];
    CGFloat xCordinate=rect.origin.x+rect.size.width/2-4;
    CGFloat yCordinate=height;
    CGPoint location = CGPointMake(xCordinate,yCordinate);
    UIBezierPath*  myPath = [UIBezierPath bezierPath];
    [myPath moveToPoint:location];
    CAShapeLayer  *shapeLayer = [[CAShapeLayer alloc] initWithLayer:self.layer];
    shapeLayer.lineWidth = 3.0;
    shapeLayer.strokeColor = [UIColor whiteColor].CGColor;
    shapeLayer.fillColor = [UIColor clearColor].CGColor;
    CGPoint location1 = CGPointMake(xCordinate, yCordinate*2);
    [myPath addLineToPoint:location1];
    shapeLayer.path = myPath.CGPath;
    [shapeLayer setName:@"lineKey"];

    [self.layer addSublayer:shapeLayer];
   
    */
    [self drawText:100];

    
}

- (CGRect)frameForTabInTabBar:(UITabBar*)tabBar withIndex:(NSUInteger)index
{
    NSMutableArray *tabBarItems = [NSMutableArray arrayWithCapacity:[tabBar.items count]];
    for (UIView *view in tabBar.subviews) {
        if ([view isKindOfClass:NSClassFromString(@"UITabBarButton")] && [view respondsToSelector:@selector(frame)]) {
            // check for the selector -frame to prevent crashes in the very unlikely case that in the future
            // objects thar don't implement -frame can be subViews of an UIView
            [tabBarItems addObject:view];
        }
    }
    if ([tabBarItems count] == 0) {
        // no tabBarItems means either no UITabBarButtons were in the subView, or none responded to -frame
        // return CGRectZero to indicate that we couldn't figure out the frame
        return CGRectZero;
    }
    
    // sort by origin.x of the frame because the items are not necessarily in the correct order
    [tabBarItems sortUsingComparator:^NSComparisonResult(UIView *view1, UIView *view2) {
        if (view1.frame.origin.x < view2.frame.origin.x) {
            return NSOrderedAscending;
        }
        if (view1.frame.origin.x > view2.frame.origin.x) {
            return NSOrderedDescending;
        }
        NSAssert(NO, @"%@ and %@ share the same origin.x. This should never happen and indicates a substantial change in the framework that renders this method useless.", view1, view2);
        return NSOrderedSame;
    }];
    
    CGRect frame = CGRectZero;
    if (index < [tabBarItems count]) {
        // viewController is in a regular tab
        UIView *tabView = tabBarItems[index];
        if ([tabView respondsToSelector:@selector(frame)]) {
            frame = tabView.frame;
        }
    }
    else {
        // our target viewController is inside the "more" tab
        UIView *tabView = [tabBarItems lastObject];
        if ([tabView respondsToSelector:@selector(frame)]) {
            frame = tabView.frame;
        }
    }
    return frame;
}
//--- For getting Tab bar Button Frame with button index
-(UIView*)viewForTabBarItemAtIndex:(NSInteger)index withUITabBarController:(UITabBarController*)tabBarController

{
    
    CGRect tabBarRect = tabBarController.tabBar.frame;
    NSInteger buttonCount = tabBarController.tabBar.items.count;
    CGFloat containingWidth = tabBarRect.size.width/buttonCount;
    CGFloat originX = containingWidth * index ;
    CGRect containingRect = CGRectMake( originX, 0, containingWidth,tabBarController.tabBar.frame.size.height );
    CGPoint center = CGPointMake( CGRectGetMidX(containingRect), CGRectGetMidY(containingRect));
    return [tabBarController.tabBar hitTest:center withEvent:nil ];
}

//-------- End of Tab bar Rect Button



-(void)drawText:(CGFloat)height
{
   
    for (CALayer *layer in [self.layer sublayers]) {
        
        if ([[layer name] isEqualToString:@"TextKey"]) {
            [layer removeFromSuperlayer];
            break;
        }
        
        
    }

    //---- Draw Text ------
    
    CATextLayer *label = [[CATextLayer alloc] init];
    [label setFont:@"Helvetica-Bold"];
    [label setFontSize:20];
    [label setFrame:CGRectMake(0, self.frame.size.width/2, fDeviceWidth, 200)];
    [label setString:[introTextElements objectAtIndex:currentIndex]];
    [label setAlignmentMode:kCAAlignmentCenter];
    [label setForegroundColor:[[UIColor whiteColor] CGColor]];
    [label setName:@"TextKey"];
    //[yourString sizeWithFont:fontHere constrainedToSize:CGSizeMake(fixedWidthForLayerTextHere, 999) lineBreakMode:UILineBreakModeWordWrap];
//
    // [label lineBreakMode :UILineBreakModeWordWrap];
   // label.numberOfLines = 2;
    
    //[label setName:@"TextKey"];
    
   // [label setValue:[NSNumber numberWithInt:101] forKey:@"Tag"];

    [self.layer addSublayer:label];
}


@end
