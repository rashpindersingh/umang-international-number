//
//  UIColor.m
//  Umang
//
//  Created by deepak singh rawat on 08/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "UIColor.h"

@implementation CALayer(UIColor)

- (void)setBorderUIColor:(UIColor*)color {
    self.borderColor = color.CGColor;
}

- (UIColor*)borderUIColor {
    return [UIColor colorWithCGColor:self.borderColor];
}

@end