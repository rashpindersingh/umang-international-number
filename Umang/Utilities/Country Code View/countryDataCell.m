//
//  countryDataCell.m
//  CountryPicker
//
//  Created by admin on 17/04/18.
//  Copyright © 2018 admin. All rights reserved.
//

#import "countryDataCell.h"

@implementation countryDataCell
@synthesize lblCountryName;
@synthesize lblCountryDailCode;
@synthesize img_CountryFlag;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
