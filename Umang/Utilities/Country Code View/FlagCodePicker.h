//
//  FlagCodePicker.h
//  CountryPicker
//
//  Created by admin on 17/04/18.
//  Copyright © 2018 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlagCodePicker : UIViewController

typedef void(^CountryCodehandler)(NSString* dialCode,NSString* imageName);
-(void)countryCodeDismiss:(CountryCodehandler)handler;
@property(nonatomic,retain)NSString* commingFrom;
@property(nonatomic,retain)NSString* viewmobileNumber;//used to get current country code
@end
