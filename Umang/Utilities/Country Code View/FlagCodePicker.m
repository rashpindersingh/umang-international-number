//
//  FlagCodePicker.m
//  CountryPicker
//
//  Created by admin on 17/04/18.
//  Copyright © 2018 admin. All rights reserved.
//

#import "FlagCodePicker.h"
#import "countryDataCell.h"
#define fDeviceWidth ([UIScreen mainScreen].bounds.size.width)

@interface FlagCodePicker ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate, UISearchResultsUpdating>
{
    NSString *countryName;
    NSString *imageName ;
    NSString *dailCode ;
    NSMutableArray  *staticCountryArr;
    CountryCodehandler _myHandler;

    IBOutlet UITableView *vw_table;
    __weak IBOutlet UILabel *lbl_title;
    SharedManager *singleton;
    //loadCountryCodeData
}
@property (nonatomic, strong) NSMutableArray *tableData;
@property (nonatomic, strong) NSMutableArray *searchResults;
@property (nonatomic, weak) NSArray * displayedItems;


@property (nonatomic, strong) UISearchController * searchController;

-(IBAction)backButtonAction:(id)sender;
@end

@implementation FlagCodePicker

@synthesize commingFrom;
@synthesize displayedItems;
@synthesize searchController;
@synthesize viewmobileNumber;
- (void)loadCountryCodewithFlag
{
    self.tableData=[NSMutableArray new];
    self.tableData =  [singleton.dbManager loadCountryCodeData];
    self.searchResults = [NSMutableArray arrayWithCapacity:[self.tableData count]];
    
    self.displayedItems = staticCountryArr;

    [vw_table reloadData];
    
}




- (void)viewDidLoad
{
  
    singleton=[SharedManager sharedSingleton];
   
    NSLog(@"viewmobileNumber=%@",viewmobileNumber);
    staticCountryArr=[NSMutableArray new];
    staticCountryArr=  [singleton.dbManager loadStaticCountry];
  
    vw_table.delegate = self;
    vw_table.dataSource = self;
    

    
    [self loadCountryCodewithFlag];
    // Here's where we create our UISearchController
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.searchBar.delegate = self;
    [self.searchController.searchBar sizeToFit];

    self.searchController.dimsBackgroundDuringPresentation = false;
    
  

    //[self.view addSubview:self.searchController.searchBar];
    vw_table.tableHeaderView = self.searchController.searchBar;
    //[vw_table setContentOffset:CGPointMake(0, self.searchController.searchBar.frame.size.height)];

    lbl_title.text=@"Choose Country Code";
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    dispatch_async(dispatch_get_main_queue(), ^{
       // [self.searchController setActive:NO];
       // [self backButtonAction:self];
    });
}

// When the user types in the search bar, this method gets called.
- (void)updateSearchResultsForSearchController:(UISearchController *)aSearchController {
    NSLog(@"updateSearchResultsForSearchController");

    NSString *searchString = aSearchController.searchBar.text;
    NSLog(@"searchString=%@", searchString);
    
    // Check if the user cancelled or deleted the search term so we can display the full list instead.
    if (![searchString isEqualToString:@""]) {
        [self.searchResults removeAllObjects];
        for (NSString *str in staticCountryArr) {
            if ([searchString isEqualToString:@""] || [str localizedCaseInsensitiveContainsString:searchString] == YES) {
                NSLog(@"str=%@", str);
                [self.searchResults addObject:str];
            }
        }
        self.displayedItems = self.searchResults;
    }
    else {
        self.displayedItems = staticCountryArr;
    }
    [vw_table reloadData];
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections
    return 1;
}


- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
    return [self.displayedItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  
    static NSString *MyIdentifier = @"countryDataCell";
    
    countryDataCell *cell = (countryDataCell*)[vw_table dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"countryDataCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    countryName = [self.displayedItems objectAtIndex:indexPath.row];
    NSInteger indexValue = [staticCountryArr indexOfObject:countryName];
    imageName = [[self.tableData objectAtIndex:indexValue] valueForKey:@"alpha2"];
    dailCode = [[self.tableData objectAtIndex:indexValue] valueForKey:@"dial"];
    
    
    NSString *retrievedCountryName =@"";
    NSString *retrievedCountryCode =@"";

    if ([commingFrom isEqualToString:@"ccode"])
    {
        retrievedCountryName = [[NSUserDefaults standardUserDefaults] valueForKey:@"cname_key"];
        retrievedCountryCode = [[NSUserDefaults standardUserDefaults] valueForKey:@"ccode_key"];

    }
    if ([commingFrom isEqualToString:@"altccode"])
    {
        retrievedCountryName = [[NSUserDefaults standardUserDefaults] valueForKey:@"cname_key"];
        retrievedCountryCode = [[NSUserDefaults standardUserDefaults] valueForKey:@"altccode_key"];

    }
    if ([commingFrom isEqualToString:@"newccode"])
    {
        retrievedCountryName = [[NSUserDefaults standardUserDefaults] valueForKey:@"cname_key"];
        retrievedCountryCode = [[NSUserDefaults standardUserDefaults] valueForKey:@"newccode_key"];

    }
    
    NSString *currentViewMobileNumber=[viewmobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    //replace + sign from country code of current view
   // if ([retrievedCountryCode isEqualToString:dailCode])
    if ([currentViewMobileNumber isEqualToString:imageName])
    {
        cell.img_check.hidden=FALSE;
        cell.lblCountryName.textColor= [UIColor colorWithRed:76.0/255.0 green:175.0/255.0 blue:81.0/255.0 alpha:1];
        cell.lblCountryDailCode.textColor= [UIColor colorWithRed:76.0/255.0 green:175.0/255.0 blue:81.0/255.0 alpha:1];
        
    }
    else
    {
        cell.lblCountryName.textColor=[UIColor blackColor];
        cell.lblCountryDailCode.textColor=[UIColor blackColor];
        cell.img_check.hidden=TRUE;
        [cell setTintColor:[UIColor clearColor]];
        
    }
    cell.lblCountryName.text=countryName;
    cell.lblCountryDailCode.text=dailCode;
    cell.img_CountryFlag.image=[UIImage imageNamed:imageName];
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

        NSString* countryName = [self.displayedItems objectAtIndex:indexPath.row];
        NSInteger indexValue = [staticCountryArr indexOfObject:countryName];
        NSString* imageName = [[self.tableData objectAtIndex:indexValue] valueForKey:@"alpha2"];
        NSString* dailCode = [[self.tableData objectAtIndex:indexValue] valueForKey:@"dial"];
    
    //save nsuserdefault values
    if ([commingFrom isEqualToString:@"ccode"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:countryName forKey:@"cname_key"];
        singleton.ccode = dailCode;
        [[NSUserDefaults standardUserDefaults] setObject:dailCode forKey:@"ccode_key"];

    }
    if ([commingFrom isEqualToString:@"altccode"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:countryName forKey:@"cname_key"];
        singleton.accode = dailCode;
        [[NSUserDefaults standardUserDefaults] setObject:dailCode forKey:@"altccode_key"];

    }
    if ([commingFrom isEqualToString:@"newccode"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:countryName forKey:@"cname_key"];
        singleton.nccode = dailCode;
        [[NSUserDefaults standardUserDefaults] setObject:dailCode forKey:@"newccode_key"];

    }
    
    
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self dismissViewControllerAnimated:YES completion:^{
        NSLog(@"Dismiss completed");
        _myHandler(dailCode,imageName);
    }];
    
}


-(IBAction)backButtonAction:(id)sender
{
    // _myHandler(@"",@"");
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)countryCodeDismiss:(CountryCodehandler)handler
{
    _myHandler = handler;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

