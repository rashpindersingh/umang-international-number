//
//  CountryCodeView.m
//  Umang
//
//  Created by Rashpinder on 17/04/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import "CountryCodeView.h"

@implementation CountryCodeView

- (id)initWithWhiteArrowFrame:(CGRect)frame
{
     self = [self initWithFrame:frame];
    self.imageArrow.image = [UIImage imageNamed:@"iconCountryArrow_White"];
    self.bottomLine.backgroundColor = [UIColor whiteColor];
    self.rightBottomLine.backgroundColor = [UIColor whiteColor];
    
    [self.textMobileNumber setTextColor:[UIColor whiteColor]];
    [self.btnCountryCode setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
   
    
    return self;
}
- (id)initWithGrayArrowFrame:(CGRect)frame
{
    self = [self initWithFrame:frame];
    self.imageArrow.image = [UIImage imageNamed:@"iconCountryArrow_Gray"];
    self.bottomLine.backgroundColor = [UIColor darkGrayColor];
    self.rightBottomLine.backgroundColor = [UIColor darkGrayColor];
    
    [self.textMobileNumber setTextColor:[UIColor colorWithRed:132.0/255.0f green:132.0/255.0f blue:132.0/255.0f alpha:1.0f]];
    [self.btnCountryCode setTitleColor:[UIColor colorWithRed:132.0/255.0f green:132.0/255.0f blue:132.0/255.0f alpha:1.0f] forState:UIControlStateNormal];

    return self;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"CountryCodeView" owner:self options:nil];
    self = (CountryCodeView *)[objects objectAtIndex:0];
    self.frame = frame;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapChangeCountry)];
    [self.btnCountryCode addTarget:self action:@selector(didTapChangeCountry) forControlEvents:UIControlEventTouchUpInside];
    [self addGestureRecognizer:tap];
    self.textMobileNumber.keyboardType = UIKeyboardTypeNumberPad;
    
  //  [self performSelector:@selector(delayUpdate) withObject:nil afterDelay:0.0];
    
   // [self delayUpdate];
    
   // @selector(textFieldDidChange:)
    return self;
}
-(void)delayUpdate
{
    [self updateCountryWith:@"91" imageName:@"in"];
}

-(void)updateCountryWith:(NSString *)code imageName:(NSString*)img
{
    
    self.imagFlagName=img;
    self.lastCountryCode = code;
    self.lastCountryImg = img;
    code = [NSString stringWithFormat:@"+%@",code];
    
    [self.btnCountryCode setTitle:code forState:UIControlStateNormal];
    self.imgCountryFlag.image = [UIImage imageNamed:img];
    CGRect size = [self rectForText:code usingFont:self.btnCountryCode.titleLabel.font boundedBySize:CGSizeMake(100, self.frame.size.height)];
    self.countryCodeWidthConstraint.constant = size.size.width + 5;
    [self layoutIfNeeded];
    [self updateConstraintsIfNeeded];
    
}
-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}
-(void)didTapChangeCountry{
    if (self.didTapUpdateCountry != nil ) {
        self.didTapUpdateCountry();
    }
}


@end
