//
//  countryDataCell.h
//  CountryPicker
//
//  Created by admin on 17/04/18.
//  Copyright © 2018 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface countryDataCell  : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblCountryName;
@property (strong, nonatomic) IBOutlet UILabel *lblCountryDailCode;
@property (strong, nonatomic) IBOutlet UIImageView *img_CountryFlag;
@property (strong, nonatomic) IBOutlet UIImageView *img_check;

@end

