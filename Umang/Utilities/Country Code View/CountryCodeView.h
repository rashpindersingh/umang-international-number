//
//  CountryCodeView.h
//  Umang
//
//  Created by Rashpinder on 17/04/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountryCodeView : UIView
@property (strong, nonatomic) IBOutlet UIImageView *imgCountryFlag;
@property (strong, nonatomic) IBOutlet UIButton *btnCountryCode;
@property (strong, nonatomic) IBOutlet UILabel *bottomLine;
@property (strong, nonatomic) IBOutlet UITextField *textMobileNumber;
@property (strong, nonatomic) IBOutlet UILabel *rightBottomLine;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *countryCodeWidthConstraint;

@property (strong, nonatomic) IBOutlet UIImageView *imageArrow;
@property (nonatomic, copy) NSString *lastCountryCode;
@property (nonatomic, copy) NSString *lastCountryImg;

@property (strong, nonatomic)  NSString *imagFlagName;

@property(copy) void(^didTapUpdateCountry)(void);
-(void)updateCountryWith:(NSString *)code imageName:(NSString*)img;
- (id)initWithWhiteArrowFrame:(CGRect)frame;
-(void)delayUpdate;
-(id)initWithGrayArrowFrame:(CGRect)frame;
@end
