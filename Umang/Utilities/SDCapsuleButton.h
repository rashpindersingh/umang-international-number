//
//  FilterServicesBO.h
//  Umang
//
//  Created by admin on 9/9/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "AppFont.h"

// Border Properties
#define BORDER_WIDTH 1.0
#define CORNER_RADIUS 15.0

#define TITLE_COLOR [UIColor colorWithRed:112.0/255.0 green:194.0/255.0 blue:138.0/255.0 alpha:1.0]
#define TITLE_FONT [AppFont regularFont:14.0]
#define BUTTON_BACKGROUND_COLOUR    [UIColor colorWithRed:112.0/255.0 green:194.0/255.0 blue:138.0/255.0 alpha:1.0]

#define NORMAL_STATE_COLOR [UIColor darkGrayColor]
#define SELECTED_STATE_COLOR [UIColor colorWithRed:54.0/255.0 green:170.0/255.0 blue:97.0/255.0 alpha:1.0]

@interface SDCapsuleButton : UIControl
{
    
}


@property(nonatomic,strong) NSString *btnTitle;

@property(nonatomic,strong) UIButton *btnCross;

@property(nonatomic,strong) UIButton *btnMain;



- (instancetype)initWithFrameForFilterButtons:(CGRect)frame withTitle:(NSString*)title withNormalImage:(NSString*)normalImage andSelectedImage:(NSString*)selectedImage;

- (instancetype)initWithFrameWithFullBorder:(CGRect)frame;

@end
