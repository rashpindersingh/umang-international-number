//
//  MyTextField.m
//  Umang
//
//  Created by deepak singh rawat on 07/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "MyTextField.h"

@implementation MyTextField

- (void)deleteBackward {
    [super deleteBackward];
    
//    if ([_myDelegate respondsToSelector:@selector(textFieldDidDelete)]){
//        [_myDelegate textFieldDidDelete];
//    }
    
    if ([_myDelegate respondsToSelector:@selector(textFieldDidDelete:)]){
        [_myDelegate textFieldDidDelete:self];
    }
    
    if ([_myDelegate respondsToSelector:@selector(textFieldDidBeginEditing:)]){
        [_myDelegate textFieldDidBeginEditing:self];
    }
    if ([_myDelegate respondsToSelector:@selector(textFieldDidEndEditing:)]){
        [_myDelegate textFieldDidEndEditing:self];
    }

    

    
}



@end
