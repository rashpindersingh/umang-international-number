//
//  StateList.m
//  Umang
//
//  Created by spice_digital on 23/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "StateList.h"
#import "UMAPIManager.h"

@implementation StateList
-(NSArray*)getStateList
{
    singleton = [SharedManager sharedSingleton];
    
    NSArray *arry=[self getStatefromPlist];
    
    
    return arry;
}


-(NSString*)getStateCode:(NSString*)statename
{
    
    //statename=[statename lowercaseString];
    NSString *state_idcode=[self executeStateQuery:statename];
    
    return state_idcode;
}





-(NSString*)getStateName:(NSString*)state_id
{
    
    NSString *capitalizedString = [state_id capitalizedString]; // capitalizes every word
    
    singleton = [SharedManager sharedSingleton];
    
    NSArray *arry;
    
    if (singleton.statesList.count == 0)
    {
        arry = [[NSUserDefaults standardUserDefaults] objectForKey:@"StateListArray"];
    }
    else
    {
        arry=[NSArray arrayWithArray:singleton.statesList];
    }
    
    // NSArray *resultArray = [arry filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(State_NAME == %@)||(State_Code == %@)", query,query]];
    
    NSArray *resultArray = [arry filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"stateId == %@", capitalizedString]];
    
    if ([resultArray count]==0) {
        return @"ALL";
        
    }
    NSString *state_name=[[resultArray objectAtIndex:0] valueForKey:@"stateName"];
    return state_name;
}




-(NSArray*)getQualiList
{
    singleton = [SharedManager sharedSingleton];
    
    NSString *qualName;
    NSMutableArray *arry=[[NSMutableArray alloc]init];
    for (int i = 0; i < [singleton.qualList count]; i++)
    {
        //stateName ,stateId
        qualName = [[singleton.qualList objectAtIndex:i] valueForKey:@"qualName"];
        qualName = [NSString stringWithFormat:@"%@%@",[[qualName substringToIndex:1] uppercaseString],[qualName substringFromIndex:1] ];
        [arry addObject:qualName];
    }
    return arry ;
}
-(NSString*)getQualiListCode:(NSString*)qual
{
    singleton = [SharedManager sharedSingleton];
    NSArray *arry=[NSArray arrayWithArray:singleton.qualList];
    NSArray *resultArray = [arry filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"qualName == %@", qual]];
    NSString *qual_idcode=@"";
    
    if([resultArray count]>0)
    {
        qual_idcode=[[resultArray objectAtIndex:0] valueForKey:@"qualId"];
        
    }
    return qual_idcode;
}




-(NSArray*)getOccupList
{
    NSString *occuName;
    NSMutableArray *arry=[[NSMutableArray alloc]init];
    for (int i = 0; i < [singleton.occuList count]; i++)
    {
        //stateName ,stateId
        occuName = [[singleton.occuList objectAtIndex:i] valueForKey:@"occuName"];
        occuName = [NSString stringWithFormat:@"%@%@",[[occuName substringToIndex:1] uppercaseString],[occuName substringFromIndex:1] ];
        [arry addObject:occuName];
    }
    return arry ;
}




-(NSString*)getqualName:(NSString*)qualId
{
    singleton = [SharedManager sharedSingleton];
    NSArray *arry=[NSArray arrayWithArray:singleton.qualList];
    NSArray *resultArray = [arry filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"qualId == %@", qualId]];
    
    NSString *qualName=@"";
    if ([resultArray count]>0) {
        qualName=[[resultArray objectAtIndex:0] valueForKey:@"qualName"];
        
    }
    return qualName;
}



-(NSString*)getOccuptname:(NSString*)Occup_id
{
    singleton = [SharedManager sharedSingleton];
    NSArray *arry=[NSArray arrayWithArray:singleton.occuList];
    NSArray *resultArray = [arry filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"occuId == %@", Occup_id]];
    
    NSString *occuName=@"";
    if ([resultArray count]>0) {
        occuName=[[resultArray objectAtIndex:0] valueForKey:@"occuName"];
        
    }
    return occuName;
}


-(NSString*)getOccuptCode:(NSString*)Occup
{
    singleton = [SharedManager sharedSingleton];
    NSArray *arry=[NSArray arrayWithArray:singleton.occuList];
    NSArray *resultArray = [arry filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"occuName == %@", Occup]];
    
    NSString *Occup_idcode=@"";
    if ([resultArray count]>0) {
        Occup_idcode=[[resultArray objectAtIndex:0] valueForKey:@"occuId"];
        
    }
    return Occup_idcode;
}



-(NSArray*)getStatefromPlist
{
    
    NSString *stateName;
    NSMutableArray *arry=[[NSMutableArray alloc]init];
    for (int i = 0; i < [singleton.statesList count]; i++)
    {
        //stateName ,stateId
        stateName = [[singleton.statesList objectAtIndex:i] valueForKey:@"stateName"];
        stateName = [NSString stringWithFormat:@"%@%@",[[stateName substringToIndex:1] uppercaseString],[stateName substringFromIndex:1] ];
        [arry addObject:stateName];
    }
    return arry ;
}

-(NSString*)executeStateQuery:(NSString*)query
{
    
    NSString *capitalizedString = [query capitalizedString]; // capitalizes every word
    
    singleton = [SharedManager sharedSingleton];
    
    NSArray *arry=[NSArray arrayWithArray:singleton.statesList];
    
    // NSArray *resultArray = [arry filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(State_NAME == %@)||(State_Code == %@)", query,query]];
    
    NSArray *resultArray = [arry filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"stateName == %@", capitalizedString]];
    
    if ([resultArray count]==0)
    {
        return @"9999";
        
    }
    NSString *state_id=[[resultArray objectAtIndex:0] valueForKey:@"stateId"];
    return state_id;
}




-(void)hitStateQualifiAPI
{
    
    singleton = [SharedManager sharedSingleton];
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:@"" forKey:@"lang"];//Enter mobile number of user
    
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_FSTQU withBody:dictBody andTag:TAG_REQUEST_UNSET_FAVORITE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        if (error == nil)
        {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            singleton.statesList=[[NSMutableArray alloc]init];
            singleton.qualList=[[NSMutableArray alloc]init];
            singleton.occuList=[[NSMutableArray alloc]init];
            
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                
                singleton.statesList=[[response valueForKey:@"pd"]valueForKey:@"statesList"];
                singleton.qualList=[[response valueForKey:@"pd"]valueForKey:@"qualList"];
                singleton.occuList=[[response valueForKey:@"pd"]valueForKey:@"occuList"];
                
                [[NSUserDefaults standardUserDefaults] setObject:singleton.statesList forKey:@"StateListArray"];
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            
            
        }
        
    }];
}



@end
