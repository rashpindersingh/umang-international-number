//
//  FilterServicesBO.h
//  Umang
//
//  Created by admin on 9/9/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "SDCapsuleButton.h"

#define BORDER_COLOR [UIColor colorWithRed:112.0/255.0 green:194.0/255.0 blue:138.0/255.0 alpha:1.0]


@interface SDCapsuleButton ()

@property(nonatomic,strong) UILabel *lblButtonName;

@end

@implementation SDCapsuleButton


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        // Default Properties
        self.backgroundColor = [UIColor clearColor];
        self.layer.borderWidth = BORDER_WIDTH;
        self.layer.cornerRadius = CORNER_RADIUS;
        self.layer.borderColor = BORDER_COLOR.CGColor;
        self.clipsToBounds = YES;
        
        
        CGFloat xCord = 5;
        CGFloat yCord = 2;
        CGFloat width = self.frame.size.width - 30;
        CGFloat height = self.frame.size.height - 2*yCord;
        
        
        self.lblButtonName = [[UILabel alloc] initWithFrame:CGRectMake(xCord, yCord, width, height)];
        self.lblButtonName.textColor = [UIColor blackColor];
        self.lblButtonName.textAlignment = NSTextAlignmentCenter;
        self.lblButtonName.font = [AppFont regularFont:14.0];
        [self addSubview:self.lblButtonName];
        
        
        xCord+=width + 5;
        
        self.btnCross = [[UIButton alloc] initWithFrame:CGRectMake(xCord, (self.frame.size.height - 20)/2 + 3 , 15, 15)];
        [self.btnCross setImage:[UIImage imageNamed:@"icon_close"] forState:UIControlStateNormal];
        [self addSubview:self.btnCross];
    }
    
    return self;
}


- (instancetype)initWithFrameWithFullBorder:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        // Default Properties
        self.backgroundColor = SELECTED_STATE_COLOR;
        self.layer.borderWidth = 0.0;
        self.layer.cornerRadius = 5.0;
        //        self.layer.borderColor = BORDER_COLOR.CGColor;
        self.clipsToBounds = YES;
        
        
        CGFloat xCord = 5;
        CGFloat yCord = 2;
        CGFloat width = self.frame.size.width - 30;
        CGFloat height = self.frame.size.height - 2*yCord;
        
        
        self.lblButtonName = [[UILabel alloc] initWithFrame:CGRectMake(xCord, yCord, width, height)];
        self.lblButtonName.textColor = [UIColor whiteColor];
        self.lblButtonName.textAlignment = NSTextAlignmentCenter;
      self.lblButtonName.font = [AppFont regularFont:14.0];        [self addSubview:self.lblButtonName];
        
        
        xCord+=width + 5;
        
        self.btnCross = [[UIButton alloc] initWithFrame:CGRectMake(xCord, (self.frame.size.height - 15)/2+ 2, 12, 12)];
        [self.btnCross setImage:[UIImage imageNamed:@"icon_delete"] forState:UIControlStateNormal];
        [self addSubview:self.btnCross];
    }
    
    
    return self;
}

- (void)setBtnTitle:(NSString*)value
{
    
    _btnTitle = value; // Assuming ARC is enabled.
    self.lblButtonName.text = _btnTitle;
    
}


- (instancetype)initWithFrameForFilterButtons:(CGRect)frame withTitle:(NSString*)title withNormalImage:(NSString*)normalImage andSelectedImage:(NSString*)selectedImage
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        // Default Properties
        self.backgroundColor = [UIColor clearColor];
        
        self.layer.borderWidth = 1.0;
        self.layer.cornerRadius = 5.0;
        self.layer.borderColor = BORDER_COLOR.CGColor;
        self.clipsToBounds = YES;
        
        CGFloat xCord = 5;
        CGFloat yCord = 10;
        CGFloat width = self.frame.size.width - xCord;
        CGFloat height = self.frame.size.height - 2*yCord;
        CGFloat tempPadding = 15;
        
        if (frame.size.height == 75) {
            tempPadding = 22;
        }
        self.btnCross = [[UIButton alloc] initWithFrame:CGRectMake(xCord, yCord , width, height)];
        self.btnCross.imageEdgeInsets = UIEdgeInsetsMake(-15,0,0,0);
        [self.btnCross setImage:[UIImage imageNamed:normalImage] forState:UIControlStateNormal];
        [self.btnCross setImage:[UIImage imageNamed:selectedImage] forState:UIControlStateSelected];
        
        [self addSubview:self.btnCross];
        
        self.lblButtonName = [[UILabel alloc] initWithFrame:CGRectMake(xCord, height- tempPadding, width, height)];
        self.lblButtonName.textColor = NORMAL_STATE_COLOR;
        self.lblButtonName.backgroundColor = [UIColor clearColor];
        self.lblButtonName.textAlignment = NSTextAlignmentCenter;
        self.lblButtonName.numberOfLines = 0;
        self.lblButtonName.font = [UIFont systemFontOfSize:12.0];
//       / self.lblButtonName.adjustsFontSizeToFitWidth = YES;
        self.lblButtonName.userInteractionEnabled = YES;
        [self addSubview:self.lblButtonName];
        
        _btnTitle = title;
        self.lblButtonName.text = _btnTitle;
        self.btnMain = [[UIButton alloc] initWithFrame:self.bounds];
        self.btnMain.backgroundColor = [UIColor clearColor];
        [self addSubview:self.btnMain];
    }
    
    return self;
}

-(void)setSelected:(BOOL)selected{
    [super setSelected:selected];
    if (selected) {
        self.btnCross.selected = YES;
        self.lblButtonName.textColor = SELECTED_STATE_COLOR;
        self.layer.borderColor = BORDER_COLOR.CGColor;
    }
    else{
        self.btnCross.selected = NO;
        self.lblButtonName.textColor = NORMAL_STATE_COLOR;
        self.layer.borderColor = [UIColor clearColor].CGColor;
    }
}


/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
