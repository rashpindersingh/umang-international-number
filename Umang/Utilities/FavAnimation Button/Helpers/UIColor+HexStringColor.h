//
//  UIColor+HexStringColor.h
//  Umang
//
//  Created by admin on 21/07/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexStringColor)
+ (UIColor *)colorWithHexString:(NSString *)hexString;
+ (UIColor *)wz_colorWithR:(NSInteger)red g:(NSInteger)green b:(NSInteger)blue;
+ (UIColor *)wz_colorWithR:(NSInteger)red g:(NSInteger)green b:(NSInteger)blue alpha:(CGFloat)alpha;

+ (UIColor *)wz_colorWithRGB:(NSInteger)rgb;
+ (UIColor *)wz_colorWithRGB:(NSInteger)rgb alpha:(CGFloat)alpha;

@end
