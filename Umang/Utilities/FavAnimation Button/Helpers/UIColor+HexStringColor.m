//
//  UIColor+HexStringColor.m
//  Umang
//
//  Created by admin on 21/07/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "UIColor+HexStringColor.h"

@implementation UIColor (HexStringColor)

+ (UIColor *)colorWithHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}
+ (UIColor *)wz_colorWithR:(NSInteger)red g:(NSInteger)green b:(NSInteger)blue
{
    return [self wz_colorWithR:red g:green b:blue alpha:1.0];
}

+ (UIColor *)wz_colorWithR:(NSInteger)red g:(NSInteger)green b:(NSInteger)blue alpha:(CGFloat)alpha
{
    return [UIColor colorWithRed:red / 255.0 green:green / 255.0 blue:blue / 255.0 alpha:alpha];
}

+ (UIColor *)wz_colorWithRGB:(NSInteger)rgb { return [self wz_colorWithRGB:rgb alpha:1.0]; }

+ (UIColor *)wz_colorWithRGB:(NSInteger)rgb alpha:(CGFloat)alpha
{
    return [UIColor colorWithRed:((float)((rgb & 0xFF0000) >> 16)) / 255.0
                           green:((float)((rgb & 0xFF00) >> 8)) / 255.0
                            blue:((float)(rgb & 0xFF)) / 255.0
                           alpha:(alpha)];
}

@end
