//
//  MyButton.h
//  Umang
//
//  Created by spice_digital on 13/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyFavButton : FaveButton <FaveButtonDelegate>

@property (retain,nonatomic) NSString* usdata;
@property (retain,nonatomic) NSDictionary* celldata;

@end
