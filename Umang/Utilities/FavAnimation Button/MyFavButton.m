//
//  MyButton.m
//  Umang
//
//  Created by spice_digital on 13/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "MyFavButton.h"

@implementation MyFavButton


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    
    // Drawing code
    self.normalColor = [UIColor clearColor];
    self.selectedColor = [UIColor redColor];
    
    // doot colors
    self.dotFirstColor = [UIColor redColor];
    self.dotSecondColor = [UIColor yellowColor];

    // circle
    self.circleFromColor = [ UIColor clearColor];
    self.circleToColor = [UIColor clearColor];
   // [self setImage:nil forState:UIControlStateNormal];
   // [self setImage:nil forState:UIControlStateSelected];
    // Add image to button for pressed state
    UIImage * btnImage1 = [UIImage imageNamed:@"icon_favourite"];
    UIImage * btnImage2 = [UIImage imageNamed:@"icon_favourite_red"];
    
    [self setImage:btnImage1 forState:UIControlStateNormal];
    [self setImage:btnImage2 forState:UIControlStateSelected];
    self.contentMode = UIViewContentModeScaleAspectFit;
    
}

- (NSArray<NSArray<UIColor *> *> *)faveButtonColors:(FaveButton *)faveButton
{
    return @[
             @[[UIColor wz_colorWithRGB:0x7dc2f4], [UIColor wz_colorWithRGB:0xe2264d]],
             @[[UIColor wz_colorWithRGB:0xf8cc61], [UIColor wz_colorWithRGB:0x9bdfba]],
             @[[UIColor wz_colorWithRGB:0xaf90f4], [UIColor wz_colorWithRGB:0x90d1f9]],
             @[[UIColor wz_colorWithRGB:0xe9a966], [UIColor wz_colorWithRGB:0xf8c852]],
             @[[UIColor wz_colorWithRGB:0xf68fa7], [UIColor wz_colorWithRGB:0xf6a2b8]]
             ];
}


@end
