//
//  OTPEntryView.m
//  Tripmaester
//
//  Created by aditi on 06/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//



#import "OTPEntryView.h"

@implementation OTPEntryView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)setHeaderTitle:(NSString *)headerTitle{
  _headerTitle = headerTitle;
  self.lblEntryPin.text = _headerTitle;
}


#pragma mark- TextField Delegates
#pragma mark-

- (UIToolbar *)designKeyboardToolbar {
  
  if (keyboardToolbar == nil) {
  keyboardToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 44)];
  keyboardToolbar.barStyle = UIBarStyleDefault;
  keyboardToolbar.items = [NSArray arrayWithObjects:
                         [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                         [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneBtnClick:)],
                         nil];
  
  [keyboardToolbar sizeToFit];
  }
  
  return keyboardToolbar;
}

-(void)doneBtnClick:(UIBarButtonItem*)sender{
  [self endEditing:YES];
  
  // Check all fields entry
  if ([self.pin1 hasText] && [self.pin2 hasText] && [self.pin3 hasText] && [self.pin4 hasText]) {
    
    self.enterdOTPText = [NSString stringWithFormat:@"%@%@%@%@",self.pin1.text,self.pin2.text,self.pin3.text,self.pin4.text];
    
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(otpEntryDoneSuccessfully:)]) {
      [self.delegate performSelector:@selector(otpEntryDoneSuccessfully:) withObject:self];
    }
  }
  else{
    self.enterdOTPText = @"";

    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(invalidOTPEntry:)]) {
      [self.delegate performSelector:@selector(invalidOTPEntry:) withObject:self];
    }
  }
}





- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
{
  textField.inputAccessoryView = [self designKeyboardToolbar];
  return YES;
}


-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
  if (textField == self.pin1)
  {
    [self.pin2 becomeFirstResponder];
  }
  else if (textField == self.pin2)
  {
    [self.pin3 becomeFirstResponder];
  }
  else if (textField == self.pin3)
  {
    [self.pin4 becomeFirstResponder];
  }
  
  return NO;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
  // This allows numeric text only, but also backspace for deletes
  if (string.length > 0 && ![[NSScanner scannerWithString:string] scanInt:NULL])
    return NO;
  
  NSUInteger oldLength = [textField.text length];
  NSUInteger replacementLength = [string length];
  NSUInteger rangeLength = range.length;
  
  NSUInteger newLength = oldLength - rangeLength + replacementLength;
  
  // This 'tabs' to next field when entering digits
  if (newLength == 1) {
    if (textField == self.pin1)
    {
      [self performSelector:@selector(setNextResponder:) withObject:self.pin2 afterDelay:0.1];
    }
    else if (textField == self.pin2)
    {
      [self performSelector:@selector(setNextResponder:) withObject:self.pin3 afterDelay:0.1];
    }
    else if (textField == self.pin3)
    {
      [self performSelector:@selector(setNextResponder:) withObject:self.pin4 afterDelay:0.1];
    }
  }
  //this goes to previous field as you backspace through them, so you don't have to tap into them individually
  else if (oldLength > 0 && newLength == 0) {
    if (textField == self.pin4)
    {
      [self performSelector:@selector(setNextResponder:) withObject:self.pin3 afterDelay:0.1];
    }
    else if (textField == self.pin3)
    {
      [self performSelector:@selector(setNextResponder:) withObject:self.pin2 afterDelay:0.1];
    }
    else if (textField == self.pin2)
    {
      [self performSelector:@selector(setNextResponder:) withObject:self.pin1 afterDelay:0.1];
    }
  }
  
  return newLength <= 1;
}



- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

- (void)setNextResponder:(UITextField *)nextResponder
{
  [nextResponder becomeFirstResponder];
}






@end
