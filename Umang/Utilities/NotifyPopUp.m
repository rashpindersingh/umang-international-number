//
//  NotifyPopUp.m
//  Umang
//
//  Created by spice on 05/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import "NotifyPopUp.h"

@implementation NotifyPopUp
@synthesize dic_profile;



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
     
    }
    return self;
}


 
- (void)closeBtnAction:(id)sender
{
    NSLog(@"inside Close Btn");
    
    [self removeFromSuperview];
}





- (void)updateProgress
{
   // NSDictionary * profileInfo = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:profileComplete, nil] forKeys:[NSArray arrayWithObjects:@"profileComplete", nil]];

    NSString *profilefillrate=[dic_profile valueForKey:@"profileComplete"];
    NSString* str = [NSString stringWithFormat:@"%@", profilefillrate];

    lbl_percentage.text=str;

    CGFloat value = [str floatValue];

    
    NSLog(@"self.timer=%@",self.timer);
    self.progress=value;
   
    [self.progressViews enumerateObjectsUsingBlock:^(THProgressView *progressView, NSUInteger idx, BOOL *stop) {
        [progressView setProgress:self.progress animated:YES];
        //[self.timer invalidate];
    }];
}

 

-(void)clickUpdateAction:(id)sender
{
    //---- Show Custom Progress Profile View--------
    bottomProgressView.borderTintColor = [UIColor clearColor];
    bottomProgressView.progressTintColor = DEFAULT_BLUE;
    bottomProgressView.progressBackgroundColor = [UIColor whiteColor];
    [self addSubview:bottomProgressView];
    self.progressViews = @[bottomProgressView];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateProgress) userInfo:nil repeats:NO];
    NSLog(@"inside click Update Btn");
  
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
