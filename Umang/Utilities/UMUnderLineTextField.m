//
//  UMUnderLineTextField.m
//  Tripmaester
//
//  Created by aditi on 06/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//



#import "UMUnderLineTextField.h"

@implementation UMUnderLineTextField

// Only override drawRect: if you perform custom drawing
// An empty implementation adversely affects performance during animation.
-(void)drawRect:(CGRect)rect {
  // Drawing code
  CALayer *bottomBorder = [CALayer layer];
  bottomBorder.frame = CGRectMake(0.0f, self.frame.size.height - 2, self.frame.size.width, 2.0f);
  bottomBorder.backgroundColor = [UIColor grayColor].CGColor;
  [self.layer addSublayer:bottomBorder];
}


@end
