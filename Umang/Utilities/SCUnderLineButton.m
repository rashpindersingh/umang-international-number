//
//  SCUnderLineButton.m
//  SelfcareApp
//
//  Created by admin on 8/29/16.
//  Copyright © 2016 SpiceLabs. All rights reserved.
//

#import "SCUnderLineButton.h"

#define FONT_MEDIUM(xx) [UIFont fontWithName:@"HelveticaNeue-Medium" size:xx]

#define ACTIVE_TEXT_COLOR [UIColor whiteColor]
#define DEACTIVATE_TEXT_COLOR [UIColor lightGrayColor]

@implementation SCUnderLineButton

- (instancetype)initWithFrame:(CGRect)frame withTitle:(NSString*)title
{
    self = [super initWithFrame:frame];
    if (self) {
        self.lblHeader = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
        self.lblHeader.textColor = [UIColor whiteColor];
        self.lblHeader.textAlignment = NSTextAlignmentCenter;
        self.lblHeader.text = title;
        self.lblHeader.backgroundColor = [UIColor clearColor];
        self.lblHeader.font = FONT_MEDIUM(14.0);
        [self addSubview:self.lblHeader];
        
        self.borderLayer = [CALayer layer];
        CGFloat borderWidth = 3;
        self.borderLayer.borderColor = [UIColor clearColor].CGColor;
        self.borderLayer.frame = CGRectMake(0, self.bounds.size.height - borderWidth, self.bounds.size.width, self.bounds.size.height);
        self.borderLayer.borderWidth = borderWidth;
        [self.layer addSublayer:self.borderLayer];
        self.layer.masksToBounds = YES;
        
    }
    return self;
}


-(void)setSelected:(BOOL)isSelected
{
    if(isSelected){
        self.lblHeader.textColor = ACTIVE_TEXT_COLOR;
        self.borderLayer.borderColor = ACTIVE_TEXT_COLOR.CGColor;
        [self layoutIfNeeded];
        self.layer.masksToBounds = YES;
    }
    else{
        self.lblHeader.textColor = ACTIVE_TEXT_COLOR;
        self.borderLayer.borderColor = [UIColor clearColor].CGColor;
        [self layoutIfNeeded];
        self.layer.masksToBounds = YES;
    }
}


@end
