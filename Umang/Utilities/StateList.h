//
//  StateList.h
//  Umang
//
//  Created by spice_digital on 23/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StateList : NSObject
{
    SharedManager *singleton;
}
-(NSArray*)getStateList;
-(NSString*)getStateCode:(NSString*)statename;
-(NSString*)getStateName:(NSString*)state_id;


-(void)hitStateQualifiAPI;
-(NSArray*)getQualiList;
-(NSString*)getQualiListCode:(NSString*)qual;



-(NSArray*)getOccupList;
-(NSString*)getOccuptCode:(NSString*)qual;
-(NSString*)getqualName:(NSString*)qualId;

-(NSString*)getOccuptname:(NSString*)Occup_id;
@end
