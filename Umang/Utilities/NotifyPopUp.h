//
//  NotifyPopUp.h
//  Umang
//
//  Created by spice on 05/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "THProgressView.h"

@interface NotifyPopUp : UIView
{
  IBOutlet   UIImageView *imgProfile;
  IBOutlet   UILabel     *lbl_profileComplete;
  IBOutlet   UILabel     *lbl_whiteLine;
  IBOutlet   UILabel     *lbl_percentage;
  IBOutlet   UIButton   *btn_close;
  IBOutlet   UIButton   *btn_clickUpdate;

  IBOutlet THProgressView *bottomProgressView;

}
- (IBAction)closeBtnAction:(id)sender;
-(IBAction)clickUpdateAction:(id)sender;


@property(nonatomic,retain)NSDictionary *dic_profile;
@property (nonatomic) CGFloat progress;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) NSArray *progressViews;

@end
