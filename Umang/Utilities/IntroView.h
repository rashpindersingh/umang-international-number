//
//  IntroView.h
//  IntroGuide
//
//  Created by spice on 24/10/16.
//  Copyright (c) 2016 Deepak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroView : UIView
//- (id)initWithFrame:(CGRect)frame introElements:(NSArray *)iElements;

- (id)initWithFrame:(CGRect)frame introElements:(NSArray *)iElements introTextElements:(NSArray *)iTextElements;
-(UIView*)viewForTabBarItemAtIndex:(NSInteger)index withUITabBarController:(UITabBarController*)tabBarController;

- (CGRect)frameForTabInTabBar:(UITabBar*)tabBar withIndex:(NSUInteger)index;

- (void)showIntro;
- (void)showIntroAtIndex:(NSUInteger)index;
@end