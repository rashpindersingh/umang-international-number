//
//  ServiceNotAvailableView.h
//  Umang
//
//  Created by admin on 27/09/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceNotAvailableView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *imageBackground;
@property (weak, nonatomic) IBOutlet UIButton *btnSubHeading;
-(void)setBackgroundImageWithUrl:(NSString*)url;
-(void)setBtnSubHeadingTitle:(NSString*)stateName;
@end
