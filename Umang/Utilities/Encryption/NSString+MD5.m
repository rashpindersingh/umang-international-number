//
//  NSString+MD5.m
//  AES256EnDeCrypt
//
//  Created by spice on 10/11/16.
//  Copyright (c) 2016 deepak singh rawat. All rights reserved.
//

#import "NSString+MD5.h"

#import "NSData+Base64.h"

#import <CommonCrypto/CommonDigest.h>

@implementation NSString (MD5)

- (NSString *)MD5 {
    
    const char * pointer = [self UTF8String];
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    
    CC_MD5(pointer, (CC_LONG)strlen(pointer), md5Buffer);
    
    NSMutableString *string = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [string appendFormat:@"%02x",md5Buffer[i]];
    
    return string;
    
    /*const char* str = [self UTF8String];
    unsigned char result[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(str, strlen(str), result);
    
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH*2];
    for(int i = 0; i<CC_SHA256_DIGEST_LENGTH; i++)
    {
        [ret appendFormat:@"%02x",result[i]];
    }
    return ret;*/
}


- (NSString *)hmacsha1:(NSString *)data secret:(NSString *)key
{
    NSString*tempKey=[key mutableCopy];
    NSString*tempdata=[data mutableCopy];
    const char *cKey  = [tempKey cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [tempdata cStringUsingEncoding:NSASCIIStringEncoding];
    if(cKey == NULL ||[tempKey length]==0)
    {
        cKey  = [tempKey UTF8String];
    }
    if(cData == NULL ||[tempdata length]==0)
    {
        cData  = [tempdata UTF8String];
    }
    unsigned char cHMAC[CC_SHA1_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA1, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    //NSString *hash = [HMAC base64EncodedString];
    NSString *hash = [HMAC base64EncodedStringWithOptions:0];
    return hash;
}

@end
