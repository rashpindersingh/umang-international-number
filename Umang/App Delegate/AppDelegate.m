//
//  AppDelegate.m
//  Umang
//
//  Created by spice_digital on 31/08/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import "AppDelegate.h"
#import "NSBundle+Language.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import <Google/SignIn.h>

#import "NetworkView.h"
#import "UMAPIManager.h"
#import "LoginAppVC.h"
#import "ViewController.h"
#import <Fabric/Fabric.h>
#import <TwitterKit/TwitterKit.h>
#import <Crashlytics/Crashlytics.h>


#import "EmailSupportViewController.h"

//-------- Add by dsrawat4u--------
//#import "HomeTabVC.h"
#import "FavouriteTabVC.h"
#import "MoreTabVC.h"
#import "NearMeTabVC.h"
#import "TabBarVC.h"
#import "LiveChatVC.h"
#import "HelpViewController.h"
#import "HomeDetailVC.h"
#import "NewLanguageSelectVC.h"
//-------- Add by dsrawat4u--------


#import <LocalAuthentication/LocalAuthentication.h>
#import "ChangeRegMobMpinVC.h"
#import "UpdMpinVC.h"
#import "RNCachingURLProtocol.h"
#import "AppConstants.h"

#import "RunOnMainThread.h"

//------- Added for notification handling-------
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@import UserNotifications;
#endif
//@import Firebase;
#import <Firebase/Firebase.h>

@import FirebaseInstanceID;
@import FirebaseMessaging;
// Implement UNUserNotificationCenterDelegate to receive display notification via APNS for devices
// running iOS 10 and above. Implement FIRMessagingDelegate to receive data message via FCM for
// devices running iOS 10 and above.
//#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0

//--------------------------------------------------------------
//-----Code for Spotlight
@import CoreSpotlight;
@import MobileCoreServices;
#import "HomeDetailVC.h"

//#define XMPP_SERVER_HOST @"reporting.umang.gov.in"
//#define XMPP_SERVER_HOST @"staging.umang.gov.in"
//#define XMPP_SERVER_HOST @"devreporting.umang.gov.in"
#import "SocialAuthentication.h"

@interface AppDelegate ()<GIDSignInDelegate,UNUserNotificationCenterDelegate, FIRMessagingDelegate>


{
    SharedManager *singleton;
    UIBackgroundTaskIdentifier backgroundUpdateTask;
    NetworkView *viewFromNib;
    UIViewController *topvc;
    
    
    UINavigationController *navigationController;
    ViewController *viewController;
    
    UpdMpinVC *updMPinVC;
    ChangeRegMobMpinVC *verifyMpinView;
    
}

@property(nonatomic,assign) CGRect frameNetworkview;
@property (nonatomic,strong) UIView *snapshotImageView;
@end

@implementation AppDelegate
@synthesize networkStatus;
@synthesize frameNetworkview;


@synthesize  downloadFilesArry;
@synthesize downloadBookNo;
@synthesize downloadComplete;

@synthesize xmppStream;
@synthesize xmppReconnect;
@synthesize xmppRoster;
@synthesize xmppRosterStorage;
@synthesize xmppvCardTempModule;
@synthesize xmppvCardAvatarModule;
@synthesize xmppCapabilities;
@synthesize xmppCapabilitiesStorage;
@synthesize xmppvCardStorage;


@synthesize xmppMessageArchivingCoreDataStorage;
@synthesize xmppMessageArchivingModule;

@synthesize isHistoryLoaded;
@synthesize chatHistoryLoad;
@synthesize chatBotLoad;
#pragma mark - Shared Delegates

+(AppDelegate*)sharedDelegate{
    return (AppDelegate*)[[UIApplication sharedApplication]delegate];
}

- (XMPPHandler *)getXMPPClient{
    return self.xmppHandler;
}


#pragma mark Core Data
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSManagedObjectContext *)managedObjectContext_roster
{
    return [xmppRosterStorage mainThreadManagedObjectContext];
}

- (NSManagedObjectContext *)managedObjectContext_capabilities
{
    return [xmppCapabilitiesStorage mainThreadManagedObjectContext];
}


#pragma mark --- XMPP HANDLER INITIALIZER--
/**-------------------------------------------------------------------------------------
 IMPLEMENTING XMPP HANDLER
 ---------------------------------------------------------------------------------------*/
-(void) configureXMPPWithJid:(NSString *)jidString andPassword:(NSString *)passwordString andChatSession:(BOOL)chatSession
{
    //@"lokesh@itx1spip-momt1"
    //@"lokesh"
    
    [[NSUserDefaults standardUserDefaults] setValue:jidString forKey:@"XXMPPmyJID"];
    [[NSUserDefaults standardUserDefaults] setValue:passwordString forKey:@"XXMPPmyPassword"];
    
    if (chatSession)
    {
        //// To be Changed
        [[NSUserDefaults standardUserDefaults] setValue:@"Y" forKey:@"ChatSession"];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"N" forKey:@"ChatSession"];
    }
    
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    
    //NSString *domainPath = [@"@" stringByAppendingString:XMPP_SERVER_HOST];
    NSString *animoUser = [[NSUserDefaults standardUserDefaults] objectForKey:@"XXMPPmyJID"];
    NSString *animoPass = [[NSUserDefaults standardUserDefaults] objectForKey:@"XXMPPmyPassword"];
    
    if (!self.xmppHandler) {
        self.xmppHandler = [[XMPPHandler alloc] init];
    }
    
    //[self.xmppHandler clearLocalRosterData];
    singleton = [SharedManager sharedSingleton];
    [self.xmppHandler setHostname:singleton.apiMode.XMPP_SERVER_HOST];
    [self.xmppHandler setUsername:animoUser];
    [self.xmppHandler setPassword:animoPass];
    [self.xmppHandler setDelegate:self];
    [self.xmppHandler setupStream];
    //[self.xmppHandler connect];
}

//////XMPP HANDLER DELEGATE METHODS////////
-(void) setXMPPHandlerConnectionStatus:(BOOL)status
{
    if (status)
    {
        [self.chatDelegate getXMPPConnectionStatus:status];
        NSLog(@"User has been logged in successfully !");
    }
    else
    {
        [self.chatDelegate getXMPPConnectionStatus:status];
    }
}

-(void)chatSessionExpiredWithStatus:(BOOL)status
{
    [self.chatDelegate getchatSessionExpiredWithStatus:NO];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    [NSURLProtocol registerClass:[RNCachingURLProtocol class]];
    // HTTPCookieAcceptPolicyAlways 
    [NSHTTPCookieStorage sharedHTTPCookieStorage].cookieAcceptPolicy = NSHTTPCookieAcceptPolicyAlways;
    self.selectedItems = [[NSMutableArray alloc]init];
    
    // UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Chat" bundle:nil];
    // LiveChatVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LiveChatVC"];
    
    isHistoryLoaded=NO;
    chatHistoryLoad=[NSMutableArray new];
    chatBotLoad=[NSMutableArray new];
    //---------------------
    downloadFilesArry = [[NSMutableArray alloc] init];
    downloadComplete = [[NSMutableArray alloc] init];
    downloadBookNo=0;
    //---------------------
    
    singleton=[SharedManager sharedSingleton];
    singleton.dbManager = [[UMSqliteManager alloc] initWithDatabaseFilename:@"UMANG_DATABASE.db"];
    
    [singleton.dbManager createUmangDB];
    
    
    //------ by default set frame to bottom for no network
    frameNetworkview=CGRectMake(0, fDeviceHeight-50, fDeviceWidth, 50);
    
    
    
    // NSString *bearerToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"BEARERTOKEN"];
    
    
    //------ Encrypt Value--------------------------
    NSString *bearerToken = [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"BEARERTOKEN"];
    NSLog(@"bearerToken=%@",bearerToken);
    if (bearerToken == nil)
    { // There is no language selected. Set English by default
        //[[NSUserDefaults standardUserDefaults] setObject:@"Bearer  9f43b495-8c2d-3b18-b4be-a27badfc0cd3" forKey:@"BEARERTOKEN"];
        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
        // Encrypt
        [[NSUserDefaults standardUserDefaults] encryptValue:@"Bearer  9f43b495-8c2d-3b18-b4be-a27badfc0cd3" withKey:@"BEARERTOKEN"]; //set default token value if token not exists
        
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;
    // Optional: set debug to YES for extra debugging information.
    //  [GAI sharedInstance].debug = YES;
    // Create tracker instance.
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-87973874-2"];
    
    tracker.allowIDFACollection = YES;
    NSDictionary *Dict = [[GAIDictionaryBuilder createEventWithCategory:@"share_extention_tapped" action:@"send_post" label:@"" value:0] build];
    [tracker send:Dict];
    
    
    // grab correct storyboard depending on screen height
    // UIStoryboard *storyboard = [self grabStoryboard];
    
    // display storyboard
    // self.window.rootViewController = [self.storyboard instantiateInitialViewController];
    // [self.window makeKeyAndVisible];
    
    
    
    // For Facebook Login
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    
    // Fot Twitter using Fabric
    //[Fabric with:@[Twitter.self]];
    //[Fabric with:@[[Crashlytics class]]];
    //[Crashlytics sharedInstance].debugMode = YES;
    
    
    // For Google Login
    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    [GIDSignIn sharedInstance].delegate = self;
    
    
    
    // Override point for customization after application launch.
    [[UINavigationBar appearance] setBarStyle:UIBarStyleBlack];
    
    
    
    
    // Check existing language if set
    NSString *existingLanguageSaved = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    
    if (existingLanguageSaved == nil) { // There is no language selected. Set English by default
        [NSBundle setLanguage:TXT_LANGUAGE_ENGLISH];
        [[NSUserDefaults standardUserDefaults] setObject:TXT_LANGUAGE_ENGLISH forKey:KEY_PREFERED_LOCALE];
        
        singleton.languageSelected=NSLocalizedString(@"english", nil);
        
    }
    else // Set already saved language
    {
        [NSBundle setLanguage:existingLanguageSaved];
        if ([existingLanguageSaved containsString:@"en"]) {
            
            singleton.languageSelected= NSLocalizedString(@"english", nil);
            
        }
        
        if ([existingLanguageSaved containsString:@"hi"]) {
            
            singleton.languageSelected=NSLocalizedString(@"hindi", nil);
            
        }
        
        if ([existingLanguageSaved containsString:@"pa"]) {
            
            singleton.languageSelected=NSLocalizedString(@"punjabi", nil);
            
        }
        
        if ([existingLanguageSaved containsString:@"bn"]) {
            
            singleton.languageSelected=NSLocalizedString(@"bengali", nil);
            
        }
        
        if ([existingLanguageSaved containsString:@"as"]) {
            
            singleton.languageSelected=NSLocalizedString(@"assamese", nil);
            
        }
        
        if ([existingLanguageSaved containsString:@"kn"]) {
            
            singleton.languageSelected=NSLocalizedString(@"kannada", nil);
            
        }
        
        if ([existingLanguageSaved containsString:@"gu"]) {
            
            singleton.languageSelected=NSLocalizedString(@"gujarati", nil);
            
        }
        
        if ([existingLanguageSaved containsString:@"ml"]) {
            
            singleton.languageSelected=NSLocalizedString(@"malayalam", nil);
            
        }
        
        if ([existingLanguageSaved containsString:@"mr"]) {
            
            singleton.languageSelected=NSLocalizedString(@"marathi", nil);
            
        }
        if ([existingLanguageSaved containsString:@"or"]) {
            
            singleton.languageSelected=NSLocalizedString(@"oriya", nil);
            
        }
        
        if ([existingLanguageSaved containsString:@"ta"]) {
            
            singleton.languageSelected=NSLocalizedString(@"tamil", nil);
            
        }
        
        if ([existingLanguageSaved containsString:@"te"]) {
            
            singleton.languageSelected=NSLocalizedString(@"telugu", nil);
            
        }
        
        if ([existingLanguageSaved containsString:@"ur"]) {
            
            singleton.languageSelected=NSLocalizedString(@"urdu", nil);
            
        }
        
        
        
        
        
        
        
        
    }
    
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeNewsstandContentAvailability| UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    
    
    [self checkDbVersion];
    
    
    // [self checkNetworkStatus];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"FONTCHANGENOTIFICATION"
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"SPOTLIGHTSEARCH"
                                               object:nil];
    
    //---- Notifier for handling GCM API when API token recieved and User token recieved----
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(GCMHitNotifier:)  name:@"HITGCMAPI"
                                               object:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkNetworkStatusValue:)
                                                 name:@"NETWORKBARCHECK" object:nil];
    
    
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter]  postNotificationName:@"NETWORKBARCHECK" object:userInfo];
    
    //---------------------------------------------------------------------
    //-------------- Start condition for Push Handling using FCM-----------
    //---------------------------------------------------------------------
    
    // Register for remote notifications
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        // iOS 7.1 or earlier. Disable the deprecation warnings.
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        UIRemoteNotificationType allNotificationTypes =
        (UIRemoteNotificationTypeSound |
         UIRemoteNotificationTypeAlert |
         UIRemoteNotificationTypeBadge);
        [application registerForRemoteNotificationTypes:allNotificationTypes];
#pragma clang diagnostic pop
    } else {
        // iOS 8 or later
        // [START register_for_notifications]
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
            UIUserNotificationType allNotificationTypes =
            (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
            UIUserNotificationSettings *settings =
            [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        } else {
            // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
            UNAuthorizationOptions authOptions =
            UNAuthorizationOptionAlert
            | UNAuthorizationOptionSound
            | UNAuthorizationOptionBadge;
            [[UNUserNotificationCenter currentNotificationCenter]
             requestAuthorizationWithOptions:authOptions
             completionHandler:^(BOOL granted, NSError * _Nullable error) {
             }
             ];
            // For iOS 10 display notification (sent via APNS)
            [UNUserNotificationCenter currentNotificationCenter].delegate = self;
            // For iOS 10 data message (sent via FCM)
            [[FIRMessaging messaging] setRemoteMessageDelegate:self];
#endif
        }
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        // [END register_for_notifications]
    }
    // [START configure_firebase]
    [FIRApp configure];
    // [END configure_firebase]
    // Add observer for InstanceID token refresh callback.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:)
                                                 name:kFIRInstanceIDTokenRefreshNotification object:nil];
    //---------------------------------------------------------------------
    //-------------- End condition for Push Handling using FCM-------------
    //---------------------------------------------------------------------
    
    // setup cache
    int cacheSizeMemory = 4*1024*1024; // 4MB
    int cacheSizeDisk = 32*1024*1024; // 32MB
    
    
    NSURLCache *urlCache = [[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"nsurlcache"];
    [NSURLCache setSharedURLCache:urlCache];
    
    /*   BOOL jailbreak=[self isJailbroken];
     
     
     if(jailbreak==true)
     {
     //nslog true
     
     UIAlertView *anAlert = [[UIAlertView alloc] initWithTitle:@"Rooted Mobile" message:@"UMANG don't support in rooted device!" delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
     [anAlert show];
     }
     else
     {
     //nslog false
     
     // Check Loggedin states for user.
     [self initiateController];
     
     }
     
     //[self crashButtonTapped:self];
     
     [self setSettings];
     
     [self loadHTTPCookies];
     */
    [Fabric with:@[[Crashlytics class], [Twitter class]]];
    [self performSelector:@selector(hitInitAPI) withObject:nil afterDelay:0.5];
    
    //----------code Start for handle spotlight search---------------
    if ([CSSearchableItemAttributeSet class])
        [self setUpCoreSpotlight];
    
//    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"DEVICETOKENFIREBASEKEY"];
//    if (token != nil )
//    {
//        UMAPIManager *api = [[UMAPIManager alloc]init];
//        __block  typeof(self) weakSelf = self;
//        [api hitKeyEncryptionApi:token withComplition:^(id response) {
//            [weakSelf hitInitAPI];
//        }];
//    }
    // [[NSUserDefaults standardUserDefaults] setObject:refreshedToken forKey:@"DEVICETOKENFIREBASEKEY"];
   
    
    
    return YES;
}


-(void)checkDbVersion
{
    
    
    singleton.dbManager = [[UMSqliteManager alloc] initWithDatabaseFilename:@"UMANG_DATABASE.db"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    //NSString *currentAppVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    //NSString *previousVersion = [defaults objectForKey:@"appVersion"];
    
    
    
    NSInteger dbVersion = [[defaults objectForKey:@"dbVersion"] integerValue];
    
    if (dbVersion == 0)
    {
        dbVersion = 1;
    }
    
    if (dbVersion < CurrentDBVersion)
    {
        [singleton.dbManager onUpgrade:dbVersion withnewVersion:CurrentDBVersion];
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:CurrentDBVersion] forKey:@"dbVersion"];
    }
    
    
    
    
    
    /*if (!previousVersion)
     {
     // first launch
     //====== Delete database in case of first launch forcefully as testflight code dont have this version save code currently
     // [singleton.dbManager deleteDatabase];
     // [self removeLocalPrefOnlogout];
     
     [singleton.dbManager upgradeDatabaseIfRequired];
     
     
     //Save appversion at first time launch
     NSLog(@"======> Save appversion at first time launch");
     [defaults setObject:currentAppVersion forKey:@"appVersion"];
     [defaults synchronize];
     
     }
     else if ([previousVersion isEqualToString:currentAppVersion])
     {
     // same version
     //do nothing database already created at the end if its not exist.
     NSLog(@"======> Do nothing database already created at the end if its not exist");
     
     }
     else
     {
     // other version
     // So delete database and then create it again as the code is used  at the end
     NSLog(@"======> Other Version So delete database and then create it again as the code is used  at the end");
     // [singleton.dbManager deleteDatabase];
     // [self removeLocalPrefOnlogout];
     
     [singleton.dbManager upgradeDatabaseIfRequired];
     
     
     
     [defaults setObject:currentAppVersion forKey:@"appVersion"];
     [defaults synchronize];
     
     
     }*/
    
    
    
    //------- According to condition create database
    
    
    
    
   // BOOL jailbreak=[self isJailbroken];
    BOOL jailbreak=[singleton.dbManager isLibertyPatchJailBroken];

    
    if(jailbreak==true)
    {
        //nslog true
        
        UIAlertView *anAlert = [[UIAlertView alloc] initWithTitle:@"Rooted Mobile" message:@"UMANG don't support in rooted device!" delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
        [anAlert show];
    }
    else
    {
        //nslog false
        
        // Check Loggedin states for user.
        [self initiateController];
        
    }
    
    //[self crashButtonTapped:self];
    
    [self setSettings];
    
    [self loadHTTPCookies];
    
    
}

-(void)removeLocalPrefOnlogout
{
    singleton=[SharedManager sharedSingleton];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:NO forKey:@"LOGIN_KEY"];
    [[NSUserDefaults standardUserDefaults] setInteger:kLoginScreenCase forKey:kInitiateScreenKey];
    [defaults synchronize];
    
    //------------------------- Encrypt Value------------------------
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    // Encrypt
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"TOKEN_KEY"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"USER_PIC"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"PROFILE_COMPELTE_KEY"];
    [[NSUserDefaults standardUserDefaults] setObject:@"N" forKey:@"ChatSession"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    //------------------------- Encrypt Value------------------------
    //——Remove Sharding Value——
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"NODE_KEY"];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"jsonStringtoSave"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //----- remove sharding--------
    
    
    
    
    singleton.profileUserAddress = @"";
    singleton.stateSelected = @"";
    singleton.imageLocalpath=@"";
    singleton.notiTypeGenderSelected=@"";
    singleton.profileNameSelected =@"";
    singleton.profilestateSelected=@"";
    singleton.notiTypeCitySelected=@"";
    singleton.notiTypDistricteSelected=@"";
    singleton.profileDOBSelected=@"";
    singleton.altermobileNumber=@"";
    singleton.user_Qualification=@"";
    singleton.user_Occupation=@"";
    singleton.user_profile_URL=@"";
    singleton.profileEmailSelected=@"";
    singleton.mobileNumber=@"";
    singleton.user_id=@"";
    singleton.user_tkn=@"";
    singleton.user_mpin=@"";
    singleton.user_aadhar_number=@"";
    singleton.objUserProfile = nil;
    
    singleton.imageLocalpath=@"";
    
    
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"AccessTokenDigi"];
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"RefreshTokenDigi"];
    //[[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"dialogueID"];
    
    [[[AppDelegate sharedDelegate]xmppHandler] sendMessage:[NSString stringWithFormat:@"userdisconnected~~~%@~~~exit",singleton.user_tkn] toAdress:@"bot@reporting.umang.gov.in" withType:@"chat"];
    
    [[[AppDelegate sharedDelegate] xmppHandler] teardownStream];
    
    @try {
        [singleton.arr_recent_service removeAllObjects];
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    //------------------------- Encrypt Value------------------------
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    // Encrypt
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"lastFetchDate"];
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"lastFetchV1"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"USER_ID"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"TOKEN_KEY"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"USER_PIC"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"PROFILE_COMPELTE_KEY"];
    
    //------------------------- Encrypt Value------------------------
    
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSHTTPCookie *cookie;
    for (cookie in [storage cookies])
    {
        
        [storage deleteCookie:cookie];
        
    }
    NSMutableArray *cookieArray = [[NSMutableArray alloc] init];
    [[NSUserDefaults standardUserDefaults] setValue:cookieArray forKey:@"cookieArray"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    //——Remove Sharding Value——
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"NODE_KEY"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //----- remove sharding--------
    
    
    
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    // Logout from social frameworks as well.
    SocialAuthentication *objSocial = [[SocialAuthentication alloc] init];
    [objSocial logoutFromAllSocialFramewors];
    objSocial = nil;
    
    
    
    //------------------------- Encrypt Value------------------------
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    // Encrypt
    [[NSUserDefaults standardUserDefaults] encryptValue:@"NO" withKey:@"LINKDIGILOCKERSTATUS"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"digilocker_username"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"digilocker_password"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //------------------------- Encrypt Value------------------------
    
    
    
    
    
    [self deleteCache];
    [singleton setStateId:@""];
}
-(void)deleteCache {
    NSFileManager *filemgr;
    
    filemgr = [NSFileManager defaultManager];
    
    if ([filemgr removeItemAtPath: [NSHomeDirectory() stringByAppendingString:@"/Documents/OfflineCache"] error: NULL]  == YES)
        NSLog (@"Remove successful");
    else
        NSLog (@"Remove failed");
    
    singleton.imageLocalpath=@"";
    
}


-(void)checkIsAppUpdated
{
    NSString *urlString = @"http://itunes.apple.com/lookup?bundleId=in.gov.umang.negd.g2c&country=in";
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"GET"];
    
    NSError *error = nil;
    NSURLResponse *response = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (!error)
    {
        
        //The response is in data
        //NSLog(@"Success: %@", stringReply);
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        float appStoreVersion=[[[[dictResponse objectForKey:@"results"] firstObject] objectForKey:@"version"] floatValue];
        
        NSLog(@"app stroe version=%f",appStoreVersion);
        
        NSString *strLocalVersion=[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        float localAppVersion=[strLocalVersion floatValue];
        if (localAppVersion!=appStoreVersion)
        {
            
            //delete database here and then create it again
            
            
        }
    }
    
}

- (void) checkNetworkStatusValue:(NSNotification *) notification
{
    UIView *removeView;
    while((removeView = [topvc.view viewWithTag:1779]) != nil) {
        [removeView removeFromSuperview];
    }
    
    if ([[notification name] isEqualToString:@"NETWORKBARCHECK"])
    {
        [singleton.reach stopNotifier];
        
        //hit api for GCM here
        
        NSDictionary *userInfo = notification.userInfo;
        NSLog (@"userInfo=%@",userInfo);
        
        
        if ([[userInfo valueForKey:@"CLASSTYPE"] isEqualToString:@"TABBAR"]) {
            NSLog(@"TABBAR");
            frameNetworkview=CGRectMake(0, fDeviceHeight-100, fDeviceWidth, 50);
            [singleton.reach startNotifier];
            [self checkNetworkStatus];
            
        }
        else if ([[userInfo valueForKey:@"CLASSTYPE"] isEqualToString:@"NOTABBAR"])
        {
            NSLog(@"NOTABBAR");
            //------ by default set frame to bottom for no network
            frameNetworkview=CGRectMake(0, fDeviceHeight-50, fDeviceWidth, 50);
            [singleton.reach startNotifier];
            [self checkNetworkStatus];
            
        }
        
        
    }
}




















- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)activity restorationHandler:(void (^)(NSArray *))restorationHandler
{
    
    NSString * valueCSSearchableItemActionType;
    BOOL wasHandled = YES;
    
    if ([CSSearchableItemAttributeSet class]) //iOS 9
    {
        
        valueCSSearchableItemActionType = CSSearchableItemActionType;
        
    } else { // iOS 8 or earlier
        
        valueCSSearchableItemActionType = @"not supported";
    }
    
    if ([activity.activityType isEqual: valueCSSearchableItemActionType])
        
    {
        
        //…handle the click here, we can assume iOS 9 from now on…
        // NSString * activityIdentifier = [activity.userInfo valueForKey:CSSearchableItemActivityIdentifier];
        
        
        wasHandled = YES;
        NSLog(@"activityIdentifier %@",  activity.userInfo);
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                       ^{
                           
                           NSString *searchSpotlight=[NSString stringWithFormat:@"%@",[activity.userInfo valueForKey:@"kCSSearchableItemActivityIdentifier"]];
                           NSError *jsonError;
                           NSData *objectData = [searchSpotlight dataUsingEncoding:NSUTF8StringEncoding];
                           NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                                                options:NSJSONReadingMutableContainers
                                                                                  error:&jsonError];
                           
                           
                           dispatch_async(dispatch_get_main_queue(),
                                          ^{
                                              UIStoryboard *storyboard  = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                              
                                              
                                              HomeDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
                                              
                                              vc.dic_serviceInfo=json;//change it to URL on demand
                                              vc.tagComeFrom=@"OTHERS";
                                              
                                              //UIViewController *topvc=[self topMostController];
                                              topvc=[self topMostController];
                                              
                                              [topvc presentViewController:vc animated:NO completion:nil];
                                          });
                       });
        
        
        // [self presentViewController:vc animated:NO completion:nil];
        
    }
    else if ([activity.activityType isEqualToString: NSUserActivityTypeBrowsingWeb]) {
        NSURL *url = activity.webpageURL;
        NSURLComponents *urlComponents = [[NSURLComponents alloc] initWithURL:url resolvingAgainstBaseURL:false];
        NSLog(@"URL scheme:%@", [url scheme]);
        NSLog(@"URL query: %@", [url query]);
        NSLog(@"URL urlComponents: %@", urlComponents);
        /// Open home detail VC with url,dept Id and dept Name
        
        if (singleton.user_tkn.length != 0)
        {
            
            //com.spicedigital.umang://?open=https://stgweb.umang.gov.in/pmkvy/api/deptt/pmkvyHtml/&deptId=21&deptName=pmkvy
            UIStoryboard *storyboard  = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
            tbc.selectedIndex = 0; //[singleton getSelectedTabIndex];
            // tbc.selectedIndex=0;
            //self.window.rootViewController = tbc;
            [self.window setRootViewController:tbc];
            
            NSString *argsAsString = (NSString*)[[url query]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
            argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
            argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
            NSMutableDictionary *queryStringDictionary = [[NSMutableDictionary alloc] init];
            NSArray *urlComponents = [argsAsString componentsSeparatedByString:@"&"];
            BOOL isDeptPush = false;
            for (NSString *keyValuePair in urlComponents)
            {
                NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
                NSString *key = [[pairComponents firstObject] stringByRemovingPercentEncoding];
                if ([key isEqualToString:@"deptId"] ||[key isEqualToString:@"dept_Id"]) {
                    key = @"SERVICE_ID";
                    isDeptPush = true;
                }
                else if ([key isEqualToString:@"deptName"]||[key isEqualToString:@"dept_Name"]) {
                    key = @"SERVICE_NAME";
                } else if ([key isEqualToString:@"open"]||[key isEqualToString:@"url"]) {
                    key = @"SERVICE_URL";
                }
                NSString *value = [[pairComponents lastObject] stringByRemovingPercentEncoding];
                [queryStringDictionary setObject:value forKey:key];
            }
            if (isDeptPush) {
                HomeDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
                vc.dic_serviceInfo = queryStringDictionary;
                vc.tagComeFrom=@"OTHERS";
                topvc=[self topMostController];
                [RunOnMainThread runBlockInMainQueueIfNecessary:^{
                    [topvc presentViewController:vc animated:YES completion:nil];
                }];
            }
        }
    }
    return wasHandled;
}

-(void)setUpCoreSpotlight
{
    NSArray *arrServiceData=[singleton.dbManager loadDataServiceData];
    
    //Imaging for demo purposes that these items have been pulled from a server because they have been saved
    //to the user's personal bookmarks at some point -- perhaps on another device.
    
    
    
    
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSMutableArray *indexSearch=[NSMutableArray new];
        for (int i=0; i<[arrServiceData count]; i++)
        {
            
            
            
            NSString *service_id=[[arrServiceData valueForKey:@"SERVICE_ID"] objectAtIndex:i];
            NSString *service_name=[[arrServiceData valueForKey:@"SERVICE_NAME"] objectAtIndex:i];
            // NSString *service_desc=[[arrServiceData valueForKey:@"SERVICE_DESC"] objectAtIndex:i];
            NSString *service_desc=[[arrServiceData valueForKey:@"SERVICE_DEPTDESCRIPTION"] objectAtIndex:i];
            
            NSString *service_imgURL=[[arrServiceData valueForKey:@"SERVICE_IMAGE"] objectAtIndex:i];
            NSString *service_category=[[arrServiceData valueForKey:@"SERVICE_CATEGORY"] objectAtIndex:i];
            //  NSString *service_subcategory=[[arrServiceData valueForKey:@"SERVICE_SUB_CATEGORY"] objectAtIndex:i];
            // NSString *service_rating=[[arrServiceData valueForKey:@"SERVICE_RATING"] objectAtIndex:i];
            //  NSString *service_fav=[[arrServiceData valueForKey:@"SERVICE_IS_FAV"] objectAtIndex:i];
            // NSString *service_hideStatus=[[arrServiceData valueForKey:@"SERVICE_IS_HIDDEN"] objectAtIndex:i];
            // NSString *service_hide=[[arrServiceData valueForKey:@"SERVICE_IS_HIDDEN"] objectAtIndex:i];
            NSString *service_URL=[[arrServiceData valueForKey:@"SERVICE_URL"] objectAtIndex:i];
            //  NSString *service_lat=[[arrServiceData valueForKey:@"SERVICE_LATITUDE"] objectAtIndex:i];
            //  NSString *service_long=[[arrServiceData valueForKey:@"SERVICE_LONGITUDE"] objectAtIndex:i];
            //  NSString *service_Notify=[[arrServiceData valueForKey:@"SERVICE_IS_NOTIF_ENABLED"] objectAtIndex:i];
            // NSString *service_wrkgHour=[[arrServiceData valueForKey:@"SERVICE_WORKINGHOURS"] objectAtIndex:i];
            NSString *service_phoneNo=[[arrServiceData valueForKey:@"SERVICE_PHONE_NUMBER"] objectAtIndex:i];
            NSString *service_website=[[arrServiceData valueForKey:@"SERVICE_WEBSITE"] objectAtIndex:i];
            NSString *service_deptAddress=[[arrServiceData valueForKey:@"SERVICE_DEPTADDRESS"] objectAtIndex:i];
            //  NSString *service_lang=[[arrServiceData valueForKey:@"SERVICE_LANG"] objectAtIndex:i];
            NSString *service_email=[[arrServiceData valueForKey:@"SERVICE_EMAIL"] objectAtIndex:i];
            //   NSString *service_populr=[[arrServiceData valueForKey:@"SERVICE_POPULARITY"] objectAtIndex:i];
            //  NSString *service_catId=[[arrServiceData valueForKey:@"SERVICE_CATEGORY_ID"] objectAtIndex:i];
            
            if ([service_id length]==0) {
                service_id=@"";
            }
            if ([service_name length]==0) {
                service_name=@"";
            }
            if ([service_URL length]==0) {
                service_URL=@"";
            }
            
            
            NSMutableDictionary *serviceDic =[NSMutableDictionary new];
            [serviceDic setValue:service_id forKey:@"SERVICE_ID"];
            [serviceDic setValue:service_name forKey:@"SERVICE_NAME"];
            [serviceDic setValue:service_URL forKey:@"SERVICE_URL"];
            
            
            
            
            
            
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:serviceDic options:NSJSONWritingPrettyPrinted error:&error];
            NSString *resultAsString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            NSLog(@"jsonData as string:\n%@ Error:%@", resultAsString,error);
            
            
            CSSearchableItemAttributeSet * attributeSet = [[CSSearchableItemAttributeSet alloc]
                                                           initWithItemContentType:(NSString *)kUTTypeItem];
            attributeSet.displayName = service_name;
            attributeSet.title = service_name;
            //Sounds similar to displayName but is not displayed to user
            attributeSet.contentDescription =service_desc;
            attributeSet.keywords = @[service_name, service_desc, service_category,service_phoneNo,service_website,service_deptAddress,service_email];
            NSURL *img_url=[NSURL URLWithString:service_imgURL];
            NSData *imageData =[NSData dataWithContentsOfURL:img_url];
            //[NSData dataWithData:UIImagePNGRepresentation(image)];
            attributeSet.thumbnailData = imageData;
            
            
            // NSString *serviceInfo=[NSString stringWithFormat:@"%@",[arrServiceData objectAtIndex:i]];
            CSSearchableItem *item = [[CSSearchableItem alloc]
                                      initWithUniqueIdentifier:resultAsString
                                      domainIdentifier:service_category
                                      attributeSet:attributeSet];
            
            
            [indexSearch addObject:item];
            
            
            
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSArray *itemforSearch=[NSArray arrayWithArray:indexSearch];
            [[CSSearchableIndex defaultSearchableIndex] indexSearchableItems:itemforSearch
                                                           completionHandler: ^(NSError * __nullable error) {
                                                               if (!error)
                                                                   NSLog(@"Search item(s) journaled for indexing.");
                                                           }];
            
        });
    });
    
    
    /*
     CSSearchableItemAttributeSet * attributeSet = [[CSSearchableItemAttributeSet alloc]
     initWithItemContentType:(NSString *)kUTTypeItem];
     
     attributeSet.displayName = @"A Christmas Carol";
     attributeSet.title = @"A Christmas Carol By Charles Dickens";
     //Sounds similar to displayName but is not displayed to user
     attributeSet.contentDescription = @"Who would dare to say “Bah! Humbug" after reading A Christmas Carol? Charles Dickens wrote the novella in just six weeks before it was first published on December 19 1843 but his morality tale about a bitter old miser named Ebenezer Scrooge lives on to this day as a reminder of the importance of the Christmas spirit.";
     attributeSet.keywords = @[@"A Christmas Carol", @"Charles Dickens", @"Victorian Literature"];
     UIImage *image = [UIImage imageNamed:@"CC-Cover"];
     NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(image)];
     attributeSet.thumbnailData = imageData;
     
     CSSearchableItem *item1 = [[CSSearchableItem alloc]
     initWithUniqueIdentifier:@"https://www.notestream.com/streams/564159e4e5c24"
     domainIdentifier:@"notestream.com"
     attributeSet:attributeSet];
     
     
     
     
     attributeSet = [[CSSearchableItemAttributeSet alloc]
     initWithItemContentType:(NSString *)kUTTypeItem];
     
     attributeSet.displayName = @"How Do Enzymes Work?";
     attributeSet.title = @"How Do Enzymes Work? By Joseph Bennington-Castro";
     //Sounds similar to displayName but is not displayed to user
     attributeSet.contentDescription = @"Enzymes are biological molecules (typically proteins) that significantly speed up the rate of virtually all of the chemical reactions that take place within cells.";
     attributeSet.keywords = @[@"Enzymes", @"Science", @"Joseph Bennington-Castro"];
     image = [UIImage imageNamed:@"enzymes-cover"];
     imageData = [NSData dataWithData:UIImagePNGRepresentation(image)];
     attributeSet.thumbnailData = imageData;
     
     
     
     
     
     
     CSSearchableItem *item2 = [[CSSearchableItem alloc]
     initWithUniqueIdentifier:@"https://www.notestream.com/streams/5637b2c2a8f5e"
     domainIdentifier:@"notestream.com"
     attributeSet:attributeSet];
     
     
     
     
     attributeSet = [[CSSearchableItemAttributeSet alloc]
     initWithItemContentType:(NSString *)kUTTypeItem];
     
     attributeSet.displayName = @"Why Does Our Balance Get Worse As We Age?";
     attributeSet.title = @"Why Does Our Balance Get Worse As We Age? By Dawn Skelton";
     //Sounds similar to displayName but is not displayed to user
     attributeSet.contentDescription = @"All of us have taken a tumble at some point in our lives. But as we grow older, the risks associated with falling over become greater: we lose physical strength and bone density, our sense of balance deteriorates and we take longer to recover from a fall. Alarmingly, this process begins around the age of 25. The reasons for this are varied and complex, but by understanding them better, we can find ways to mitigate the effects of old age.";
     attributeSet.keywords = @[@"Balance", @"Aging", @"health", @"Dawn Skelton"];
     image = [UIImage imageNamed:@"balance-cover"];
     imageData = [NSData dataWithData:UIImagePNGRepresentation(image)];
     attributeSet.thumbnailData = imageData;
     
     
     CSSearchableItem *item3 = [[CSSearchableItem alloc]
     initWithUniqueIdentifier:@"https://www.notestream.com/streams/562944bd5a2b3"
     domainIdentifier:@"notestream.com"
     attributeSet:attributeSet];
     
     
     
     
     
     */
    
    
    
    
}

//----------code end for handle spotlight search---------------










- (IBAction)crashButtonTapped:(id)sender {
    [[Crashlytics sharedInstance] crash];
    [[Crashlytics sharedInstance] recordCustomExceptionName:@"HandledException" reason:@"Some reason" frameArray:@[]];
    
}


-(void)hitInitAPI
{
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:@"" forKey:@"lang"];
    
    singleton = [SharedManager sharedSingleton];
    
    NSString *userToken;
    
    if (singleton.user_tkn == nil || singleton.user_tkn.length == 0)
    {
        userToken = @"";
    }
    else
    {
        userToken = singleton.user_tkn;
    }
    
    [dictBody setObject:userToken forKey:@"tkn"];
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_INIT withBody:dictBody andTag:TAG_REQUEST_INIT completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        if (error == nil)
        {
            NSLog(@"Server Response = %@",response);
            
            singleton.arr_initResponse=[[NSMutableArray alloc]init];
            singleton.arr_initResponse=[response valueForKey:@"pd"];
            NSLog(@"singleton.arr_initResponse = %@",singleton.arr_initResponse);
            
            
            if (singleton.arr_initResponse != nil)
            {
                
                NSString*  abbr=[singleton.arr_initResponse valueForKey:@"abbr"];
                
                NSLog(@"value of abbr=%@",abbr);
                
                if ([abbr length]==0)
                {
                    abbr=@"";
                }
                
                singleton.user_StateId = [singleton.arr_initResponse valueForKey:@"ostate"];
                [singleton setStateId:singleton.user_StateId];
                
                NSString *emblemString = [singleton.arr_initResponse valueForKey:@"stemblem"];
                emblemString = emblemString.length == 0 ? @"":emblemString;
                [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
                
                [[NSUserDefaults standardUserDefaults] setObject:abbr forKey:@"ABBR_KEY"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                if (singleton.user_tkn.length != 0)
                {
                    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"TabKey"] isEqualToString:@"YES"])
                    {
                        if (abbr.length != 0)
                        {
                            UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;

                            [[tabBarController.tabBar.items objectAtIndex:3] setTitle:abbr];
                        }
                    }
                }
            
                if (CurrentAppVersion < [[singleton.arr_initResponse valueForKey:@"ver"] integerValue])
                {
                    if ([[singleton.arr_initResponse valueForKey:@"fupd"] boolValue] == TRUE)
                    {
                        
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"account_blocked_desp_txt", nil) preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                                   {
                                                       
                                                       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id1236448857"]];;
                                                       
                                                   }];
                        
                        
                        [alert addAction:okAction];
                        
                        
                        
                        [[self topMostController] presentViewController:alert animated:YES completion:nil];
                    }
                    else
                    {
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"force_update_txt", nil) preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okAction = [UIAlertAction actionWithTitle:[NSLocalizedString(@"ok", nil) capitalizedString] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                   {
                                                       
                                                       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id1236448857"]];;
                                                       
                                                   }];
                        
                        
                        UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil) style:UIAlertActionStyleCancel handler:nil];
                        
                        
                        [alert addAction:cancelAction];
                        
                        [alert addAction:okAction];
                        
                        [[self topMostController] presentViewController:alert animated:YES completion:nil];
                    }
                }
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:singleton.arr_initResponse forKey:@"InitAPIResponse"];
            
            
            /*
             facebooklink
             faq
             forceupdate
             googlepluslink
             opensource
             privacypolicy
             splashScreen
             tabordering
             termsandcondition
             twitterlink
             ver
             vermsg
             */
            
            //------ save value in nsuserdefault for relanch app
            // [[NSUserDefaults standardUserDefaults] setObject:response forKey:@"TOUR_Key"];
            //[[NSUserDefaults standardUserDefaults] synchronize];
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
             message:error.localizedDescription
             delegate:self
             cancelButtonTitle:@"OK"
             otherButtonTitles:nil];
             [alert show];*/
        }
        
    }];
    
}


-(void)setSettings
{
    BOOL selectedBanner;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"SELECTED_BANNER"] == nil)
    {
        selectedBanner = YES;
        [[NSUserDefaults standardUserDefaults] setBool:selectedBanner forKey:@"SELECTED_BANNER"];
    }
    else
    {
        selectedBanner = [[[NSUserDefaults standardUserDefaults] objectForKey:@"SELECTED_BANNER"] boolValue];
    }
    
    if (selectedBanner == YES)
    {
        
        singleton.bannerStatus = YES;
    }
    
    else if (selectedBanner == NO)
    {
        singleton.bannerStatus = NO;
        
    }
    
    
    
    //    NSString *selectedNotification = [[NSUserDefaults standardUserDefaults] objectForKey:@"SELECTED_NOTIFICATION_STATUS"];
    //    [self selectNotificationStatus:selectedNotification];
    //    NSString *selectedFont = [[NSUserDefaults standardUserDefaults] objectForKey:@"SELECTED_FONTSIZE_INDEX"];
    
    BOOL isFirstTimeLaunched = [[NSUserDefaults standardUserDefaults] boolForKey:@"IS_FIRST_TIME_LAUNCHED"];
    if (!isFirstTimeLaunched)
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"IS_FIRST_TIME_LAUNCHED"];
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"SELECTED_FONTSIZE_INDEX"];
    }
    
    NSInteger fontIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_FONTSIZE_INDEX"];
    switch (fontIndex) {
        case 0:
            singleton.fontSizeSelected=NSLocalizedString(@"small", nil);
            singleton.fontSizeSelectedIndex = 0;
            break;
        case 1:
            singleton.fontSizeSelected=NSLocalizedString(@"normal", nil);
            singleton.fontSizeSelectedIndex = 1;
            
            break;
        case 2:
            singleton.fontSizeSelected=NSLocalizedString(@"large", nil);
            singleton.fontSizeSelectedIndex = 2;
            
            break;
            
        default:
            singleton.fontSizeSelected=NSLocalizedString(@"normal", nil);
            singleton.fontSizeSelectedIndex = 1;
            
            break;
    }
    
    
}

/*- (void)application:(UIApplication *)application didChangeStatusBarFrame: (CGRect)newStatusBarFrame
 {
 
 if (newStatusBarFrame.size.height == 40)
 {
 [[NSNotificationCenter defaultCenter] addObserver:self
 selector:@selector(statusBarchange)
 name:@"STATUSBARCHANGE" object:nil];
 
 [[NSNotificationCenter defaultCenter]  postNotificationName:@"STATUSBARCHANGE" object:nil];
 
 }
 else
 {
 //do nothing
 }
 }*/



-(void)setFont:(NSString *)selectedFont
{
    
    float fontsize=15.0;
    
    if ([selectedFont isEqualToString:NSLocalizedString(@"small", nil)])
    {
        fontsize=14.0;
        
        singleton.fontSizeSelected=NSLocalizedString(@"small", nil);
        
    }
    if ([selectedFont isEqualToString:NSLocalizedString(@"normal", nil)])
    {
        singleton.fontSizeSelected=NSLocalizedString(@"normal", nil);
        fontsize=16.0;
        
    }
    if ([selectedFont isEqualToString:NSLocalizedString(@"large", nil)])
    {
        singleton.fontSizeSelected=NSLocalizedString(@"large", nil);
        fontsize=18.0;
        
    }
    [[UILabel appearance] setFont:[UIFont systemFontOfSize:fontsize]];
    
    
}
-(void)selectNotificationStatus:(NSString *)selectedNotification
{
    
    if([selectedNotification isEqualToString:NSLocalizedString(@"enabled", nil)])
    {
        singleton.notificationSelected = NSLocalizedString(@"enabled", nil);
    }
    else
    {
        singleton.notificationSelected=NSLocalizedString(@"disabled", nil);
        
        
    }
    
    
}


-(void)selectedBannerON:(NSString *)selectedBanner
{
    if ([selectedBanner isEqualToString:NSLocalizedString(@"yes", nil)]) {
        
        singleton.bannerStatus = YES;
    }
    else
    {
        singleton.bannerStatus = NO;
        
    }
    
}




-(void)initiateController
{
    NSInteger appCurrentState;
    
    BOOL keepMeLoggedIn = [[NSUserDefaults standardUserDefaults] boolForKey:kKeepMeLoggedIn];
    if (keepMeLoggedIn)
    { // Navigate User directly to the home screen
        // fetch auth token from user defaults and save in appsetting.
        appCurrentState = kDashboardScreenCase;
    }
    else{
        
        BOOL isLanguageScreenShown = [[NSUserDefaults standardUserDefaults]boolForKey:@"languageScreenShown"];
        
        if (isLanguageScreenShown==TRUE)
        {
            appCurrentState =kLoginScreenCase;//
            // appCurrentState =kLanguageScreenCase;//
            
        }
        else
        {
            appCurrentState = kLanguageScreenCase;
        }
    }
    
    switch (appCurrentState)
    {
        case kLanguageScreenCase:
        {
        UIStoryboard *storyboard  = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
        NewLanguageSelectVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"NewLanguageSelectVC"];
            // self.window.rootViewController = vc;

         [self.window setRootViewController:vc];
        [UIView transitionWithView:self.window duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:nil];
        }
            break;
        case kTutorialScreenCase:
        {
            UIStoryboard *storyboard  = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            ViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
            // self.window.rootViewController = vc;
            
            [self.window setRootViewController:vc];
            
            [UIView transitionWithView:self.window duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:nil];
            
        }
            break;
            
            
        case kLoginScreenCase:
            
        {
            NSLog(@"Login Pressed");
            
            
            UIStoryboard *storyboard  = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            LoginAppVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginAppVC"];
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                vc = [[LoginAppVC alloc] initWithNibName:@"LoginAppVC_iPad" bundle:nil];
            }
            
            [self.window setRootViewController:vc];
            
            [UIView transitionWithView:self.window duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:nil];
            
            
        }
            break;
            
        case kDashboardScreenCase:
        {
            UIStoryboard *storyboard  = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
            tbc.selectedIndex=[singleton getSelectedTabIndex];
            // tbc.selectedIndex=0;
            //self.window.rootViewController = tbc;
            [self.window setRootViewController:tbc];
            
            [UIView transitionWithView:self.window duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:nil];
        }
            
            break;
            
            
            
            
        default:
            break;
    }
    
    [self loadSplashScreen];
}
//added new below code
//================================================
//----------  Start OF SPLASH SCREEN-----------------
//================================================


-(void)loadSplashScreen
{
    singleton=[SharedManager sharedSingleton];
    
    @try {
        
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSArray *arrayofInit = [userDefaults objectForKey:@"InitAPIResponse"];
        NSLog(@" arrayofInit=%@",arrayofInit);
        
        
        if([arrayofInit count]>0)
        {
            
            //uncomment this line   for testing purpose and change date and time as per test date range
            // NSString *sscr=@"true|2|http://iphonewalls.net/wp-content/uploads/2014/09/Steve%20Jobs%20Apple%20Products%20Portrait%20iPhone%206%20Wallpaper-320x480.jpg|2017-11-07 05:59:22|2017-11-7 22:04:22";
            
            //comment this line for testing or open this line for live code
            NSString *sscr=[arrayofInit valueForKey:@"sscr"];
            
            NSArray* splashArray = [sscr componentsSeparatedByString:@"|"];
            
            NSString *showhideStatus =[splashArray objectAtIndex:0];
            if ([showhideStatus isEqualToString:@"true"])
            {
                
                //------ Get Display time--------
                NSString *timeDuration =[splashArray objectAtIndex:1];
                int timedisplay=[timeDuration intValue];
                
                
                NSString *URLSplash =[splashArray objectAtIndex:2]; //not nil
                
                if ([URLSplash length]!=0)
                {
                    //then only show
                    
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    
                    
                    NSString *startTimeString =[splashArray objectAtIndex:3]; //not nil
                    
                    NSString *endTimeString =[splashArray objectAtIndex:4]; //not nil
                    
                    NSDate *openingDate = [dateFormatter dateFromString:startTimeString];
                    NSDate *closingDate = [dateFormatter dateFromString:endTimeString];
                    
                    
                    //2017-01-02 22:59:22
                    
                    NSString *nowTimeString = [dateFormatter stringFromDate:[NSDate date]];
                    
                    int startTime   = [self minutesSinceMidnight:openingDate];
                    int endTime  = [self minutesSinceMidnight:closingDate];
                    int nowTime     = [self minutesSinceMidnight:[dateFormatter dateFromString:nowTimeString]];;
                    
                    
                    if (startTime <= nowTime && nowTime <= endTime)
                    {
                        NSLog(@"Time is between");
                        
                        // UIImageView *splashImageView = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
                        
                        UIImageView *splashImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, fDeviceWidth, fDeviceHeight)];
                        
                        //splashImageView.image=[UIImage imageNamed:@"logo"];
                        // [splashImageView sd_setImageWithURL:[NSURL URLWithString:URLSplash]
                        //                   placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
                        
                        splashImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:URLSplash]]];
                        
                        splashImageView.contentMode = UIViewContentModeScaleToFill;
                        
                        
                        [[self findTopViewController].view addSubview:splashImageView];
                        
                        
                        [UIView animateWithDuration:timedisplay
                                              delay:2.0f
                                            options:UIViewAnimationOptionCurveEaseInOut
                                         animations:^{
                                             splashImageView.alpha = .0f;
                                             CGFloat x = -60.0f;
                                             CGFloat y = -120.0f;
                                             splashImageView.frame = CGRectMake(x,
                                                                                y,
                                                                                splashImageView.frame.size.width-2*x,
                                                                                splashImageView.frame.size.height-2*y);
                                         } completion:^(BOOL finished){
                                             if (finished) {
                                                 [splashImageView removeFromSuperview];
                                             }
                                         }];
                        
                        
                        
                    }
                    else {
                        NSLog(@"Time is not between");
                    }
                    
                    
                    
                    
                    
                }
                else
                {
                    
                    //do nothing
                }
                
                
            }
        }
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
    
    
}
- (UIViewController *)findTopViewController {
    return [self topViewControllerFrom:self.window.rootViewController];
}

- (UIViewController *)topViewControllerFrom:(UIViewController *)vc {
    if (vc.navigationController.visibleViewController != nil) {
        return [self topViewControllerFrom:vc.navigationController.visibleViewController];
    }
    if (vc.tabBarController.selectedViewController != nil) {
        return [self topViewControllerFrom:vc.tabBarController.selectedViewController];
    }
    return vc;
}



-(int) minutesSinceMidnight:(NSDate *)date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:date];
    return 60 * (int)[components hour] + (int)[components minute];
}


//================================================
//---------- END OF SPLASH SCREEN-----------------
//================================================


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != 0)  // 0 == the cancel button
    {
        //home button press programmatically
        UIApplication *app = [UIApplication sharedApplication];
        [app performSelector:@selector(suspend)];
        
        //wait 2 seconds while app is going background
        [NSThread sleepForTimeInterval:2.0];
        
        //exit app when app is in background
        exit(0);
    }
}
/*
 -(void)updateAppLanguage
 {
 int defaulttab= [[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_TAB_INDEX"];
 
 [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SELECTED_TAB_INDEX"];
 [[NSUserDefaults standardUserDefaults]  synchronize];
 dispatch_async(dispatch_get_main_queue(), ^{
 //here task #1 that takes 10 seconds to run
 self.window.rootViewController = nil;
 UIStoryboard *storyboard  = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
 UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
 tbc.selectedIndex=[singleton getSelectedTabIndex];
 self.window.rootViewController = tbc;
 [UIView transitionWithView:self.window duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:nil];
 
 NSLog(@"Task #1 finished");
 });
 NSLog(@"Task #1 scheduled");
 
 dispatch_async(dispatch_get_main_queue(), ^{
 
 NSString *defaulttabstr=[NSString stringWithFormat:@"%d",defaulttab];
 [self performSelector:@selector(resetDefaultTab:) withObject:defaulttabstr afterDelay:10];
 //here task #2 that takes 5s to run
 NSLog(@"Task #2 finished");
 
 
 
 
 
 });
 
 
 
 }
 */

/*-(void)updateAppLanguage
 {
 
 // singleton.tabSelectedIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_TAB_INDEX"];
 
 
 
 NSString *selectedTab =[NSString stringWithFormat:@"%ld",(long)[[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_TAB_INDEX"]];
 
 
 dispatch_queue_t serialQueue = dispatch_queue_create("com.changetab.queue", DISPATCH_QUEUE_SERIAL);
 dispatch_async(serialQueue, ^{
 
 [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SELECTED_TAB_INDEX"];
 [[NSUserDefaults standardUserDefaults]  synchronize];
 self.window.rootViewController = nil;
 UIStoryboard *storyboard  = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
 UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
 tbc.selectedIndex=[singleton getSelectedTabIndex];
 self.window.rootViewController = tbc;
 [UIView transitionWithView:self.window duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:nil];
 
 dispatch_async(serialQueue, ^{
 
 
 [self resetDefaultTab:selectedTab];
 
 });
 
 
 
 });
 
 
 //selectedTab
 
 
 }
 */


-(void)updateAppLanguage
{
    int defaulttab= [[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_TAB_INDEX"];
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SELECTED_TAB_INDEX"];
    [[NSUserDefaults standardUserDefaults]  synchronize];
    dispatch_async(dispatch_get_main_queue(), ^{
        //here task #1 that takes 10 seconds to run
        self.window.rootViewController = nil;
        UIStoryboard *storyboard  = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
        tbc.selectedIndex=[singleton getSelectedTabIndex];
        self.window.rootViewController = tbc;
        [UIView transitionWithView:self.window duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:nil];
        
        NSLog(@"Task #1 finished");
    });
    NSLog(@"Task #1 scheduled");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSString *defaulttabstr=[NSString stringWithFormat:@"%d",defaulttab];
        [self performSelector:@selector(resetDefaultTab:) withObject:defaulttabstr afterDelay:4];
        //here task #2 that takes 5s to run
        NSLog(@"Task #2 finished");
        
        
    });
    
    
    
    
    
    
    
    
    
}



-(void)resetDefaultTab:(NSString*)tabstrnumber
{
    
    int tabnumber=[tabstrnumber intValue];
    SharedManager * sharedMySingleton = [SharedManager sharedSingleton];
    
    [[NSUserDefaults standardUserDefaults] setInteger:tabnumber forKey:@"SELECTED_TAB_INDEX"];
    sharedMySingleton.tabSelectedIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_TAB_INDEX"];
    [[NSUserDefaults standardUserDefaults]  synchronize];
    
}
-(BOOL)isJailbroken
{
    NSURL* url = [NSURL URLWithString:@"cydia://package/com.example.package"];
    return [[UIApplication sharedApplication] canOpenURL:url];
}

//------------------ Local Message Default  FOREGROUND--------------------

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    NSLog(@"local Nitification --%@",notification);
    NSLog(@"Filter Pressed");
    [self handleLocalNotificationData:notification];
}
-(void)handleLocalNotificationData:(UILocalNotification *)notification
{
    UINavigationController *navi  = (UINavigationController*)[self topViewController];
    if (([navi isKindOfClass:[TabBarVC class]]))
    {
        UITabBarController *tabBar = (UITabBarController *) navi;
        UINavigationController *selectedNavi = (UINavigationController*)tabBar.selectedViewController;
        if (![singleton.livChat_isTab boolValue]) {
            UINavigationController *naviSettings = (UINavigationController*)tabBar.viewControllers.lastObject;
            if (![naviSettings.visibleViewController isKindOfClass:[LiveChatVC class]])
            {
                __block BOOL check = true;
                [naviSettings.viewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if ([obj isKindOfClass:[HelpViewController class]]) {
                        check = false;
                        stop = !stop;
                        [self pushToLiveChatVC:obj];
                    }
                }];
                if (check) {
                    for (UIViewController *vc in naviSettings.viewControllers) {
                        if ([vc isKindOfClass:[MoreTabVC class]]) {
                            tabBar.selectedIndex = 4;
                            [self pushToLiveChatVC:vc];
                            break;
                        }
                    }
                }
            }
        }
        else if ([selectedNavi.visibleViewController isKindOfClass:[HelpViewController class]]) {
            [self pushToLiveChatVC:selectedNavi.visibleViewController];
        }
        else
        {
            tabBar.selectedIndex = 3;
        }
        [tabBar.tabBarController.tabBar setHidden:YES];
        [UIApplication sharedApplication].applicationIconBadgeNumber -= 1;
    }
    else if (([navi isKindOfClass:[HomeDetailVC class]]))
    {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Chat" bundle:nil];
        LiveChatVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LiveChatVC"];
        vc.hidesBottomBarWhenPushed = YES;
        vc.comingFromString = @"Department";
        //[self.navigationController pushViewController:vc animated:YES];
        [navi presentViewController:vc animated:NO completion:nil];
    }
}
-(void)pushToLiveChatVC:(UIViewController*)vc{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Chat" bundle:nil];
    LiveChatVC *liveVC = [storyboard instantiateViewControllerWithIdentifier:@"LiveChatVC"];
    liveVC.hidesBottomBarWhenPushed = YES;
    liveVC.comingFromString = @"Help";
    [vc.navigationController pushViewController:liveVC animated:YES];
}

/*- (BOOL)isJailbroken
 {
 BOOL jailbroken = NO;
 NSArray *jailbrokenPath = [NSArray arrayWithObjects:@"/Applications/Cydia.app",  @"/Applications/RockApp.app",  @"/Applications/Icy.app",  @"/usr/sbin/sshd",  @"/usr/bin/sshd",  @"/usr/libexec/sftp-server",  @"/Applications/WinterBoard.app",  @"/Applications/SBSettings.app",  @"/Applications/MxTube.app",  @"/Applications/IntelliScreen.app",  @"/Library/MobileSubstrate/DynamicLibraries/Veency.plist",  @"/Applications/FakeCarrier.app",  @"/Library/MobileSubstrate/DynamicLibraries/LiveClock.plist",
 @"/private/var/lib/apt",  @"/Applications/blackra1n.app",  @"/private/var/stash",  @"/private/var/mobile/Library/SBSettings/Themes",  @"/System/Library/LaunchDaemons/com.ikey.bbot.plist",  @"/System/Library/LaunchDaemons/com.saurik.Cydia.Startup.plist",  @"/private/var/tmp/cydia.log",  @"/private/var/lib/cydia", nil];for(NSString *string in jailbrokenPath)
 {
 if ([[NSFileManager defaultManager] fileExistsAtPath:string]){
 jailbroken = YES;
 break;}
 }
 return jailbroken;
 }*/


//---------------------------------------------------------------------
//--------------  Condition for Push Handling using FCM-------------
//---------------------------------------------------------------------
//------------------ Remote Message Default  FOREGROUND--------------------

// [START receive_message for default ]
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    // Print message ID.
    /* if ([userInfo objectForKey:kGCMMessageIDKey]) {
     NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
     }*/
    // Print full message.
    NSLog(@"%@", userInfo);
    NSLog(@"userInfo %@",userInfo);
    for (id key in userInfo) {
        NSLog(@"key: %@, value: %@", key, [userInfo objectForKey:key]);
    }
    @try {
        NSLog(@"Badge %d",[[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] intValue]);
        self.badgeCount=[[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] intValue];
        
        NSString *badge=[NSString stringWithFormat:@"%d",self.badgeCount];
        if ([badge length]>0)
        {
            [[NSUserDefaults standardUserDefaults] setObject:badge  forKey:@"BadgeValue"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [UIApplication sharedApplication].applicationIconBadgeNumber =self.badgeCount ;
        }
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
    
    
    [self notificationHandling:userInfo];
    
}

-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    completionHandler(UIBackgroundFetchResultNewData);

}

//------------------ Remote Message Get HERE ----> --------------------

//Fetch message at background
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    // Print message ID.
    /*if ([userInfo objectForKey:kGCMMessageIDKey]) {
     NSLog(@"Message ID: %@", userI.nfo[kGCMMessageIDKey]);
     }*/
    // Print full message.---------
    
    
    
    NSLog(@"%@", userInfo);
    
    NSLog(@"userInfo %@",userInfo);
    for (id key in userInfo) {
        NSLog(@"key: %@, value: %@", key, [userInfo objectForKey:key]);
    }
    
    @try {
        NSLog(@"Badge %d",[[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] intValue]);
        self.badgeCount=[[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] intValue];
        
        
        NSString *badge=[NSString stringWithFormat:@"%d",self.badgeCount];
        if ([badge length]>0)
        {
            [[NSUserDefaults standardUserDefaults] setObject:badge  forKey:@"BadgeValue"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [UIApplication sharedApplication].applicationIconBadgeNumber =self.badgeCount ;
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
    
    
    [self notificationHandling:userInfo];
    completionHandler(UIBackgroundFetchResultNewData);
    /*  UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Umang Push" message:[userInfo valueForKey:@"msg"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
     [alert show];*/
    
}


//------------------ Remote Message at IOS 10 Devices FOREGROUND --------------------

// [START ios_10_message_handling]
// Receive displayed notifications for iOS 10 devices.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
// Handle incoming notification messages while app is in the foreground.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    // Print message ID.
    NSDictionary *userInfo = notification.request.content.userInfo;
    /*   if ([userInfo objectForKey:kGCMMessageIDKey]) {
     NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
     }*/
    // Print full message.
    NSLog(@"%@", userInfo);
    NSLog(@"userInfo %@",userInfo);
    for (id key in userInfo) {
        NSLog(@"key: %@, value: %@", key, [userInfo objectForKey:key]);
    }
    
    @try {
        NSLog(@"Badge %d",[[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] intValue]);
        self.badgeCount=[[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] intValue];
        
        
        NSString *badge=[NSString stringWithFormat:@"%d",self.badgeCount];
        if ([badge length]>0)
        {
            [[NSUserDefaults standardUserDefaults] setObject:badge  forKey:@"BadgeValue"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [UIApplication sharedApplication].applicationIconBadgeNumber =self.badgeCount ;
        }
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
    
    [self notificationHandling:userInfo];
    @try {
        completionHandler(UNNotificationPresentationOptionAlert);
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
}



//------------------ MESSAGE TAPPED BY USER  IOS 10--------------------


// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)())completionHandler {
    NSString *categoryIdentifier = response.notification.request.content.categoryIdentifier;
    if ([categoryIdentifier isEqualToString:@"chat"]) {
        [self handleLocalNotificationData:response.notification];
        return;
    }
    
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    /* if ([userInfo objectForKey:kGCMMessageIDKey]) {
     NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
     }*/
    // Print full message.
    NSLog(@"%@", userInfo);
    NSLog(@"userInfo %@",userInfo);
    for (id key in userInfo) {
        NSLog(@"key: %@, value: %@", key, [userInfo objectForKey:key]);
    }
    
    @try {
        NSLog(@"Badge %d",[[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] intValue]);
        self.badgeCount=[[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] intValue];
        
        
        NSString *badge=[NSString stringWithFormat:@"%d",self.badgeCount];
        if ([badge length]>0)
        {
            [[NSUserDefaults standardUserDefaults] setObject:badge  forKey:@"BadgeValue"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [UIApplication sharedApplication].applicationIconBadgeNumber =self.badgeCount ;
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
    [self notificationHandling:userInfo];
    
}
#endif
// [END ios_10_message_handling]

//------------------ Remote Message at IOS 10 Devices FOREGROUND --------------------

// [START ios_10_data_message_handling]
//#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
// Receive data message on iOS 10 devices while app is in the foreground.
- (void)applicationReceivedRemoteMessage:(FIRMessagingRemoteMessage *)remoteMessage {
    // Print full message
    NSLog(@"%@", [remoteMessage appData]);
    
    
    
    @try {
        NSLog(@"userInfo %@",[remoteMessage appData]);
        for (id key in [remoteMessage appData]) {
            NSLog(@"key: %@, value: %@", key, [[remoteMessage appData] objectForKey:key]);
        }
        NSLog(@"Badge %d",[[[[remoteMessage appData] objectForKey:@"aps"] objectForKey:@"badge"] intValue]);
        self.badgeCount=[[[[remoteMessage appData] objectForKey:@"aps"] objectForKey:@"badge"] intValue];
        
        NSString *badge=[NSString stringWithFormat:@"%d",self.badgeCount];
        if ([badge length]>0)
        {
            [[NSUserDefaults standardUserDefaults] setObject:badge  forKey:@"BadgeValue"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [UIApplication sharedApplication].applicationIconBadgeNumber =self.badgeCount ;
        }
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
    
    
    [self notificationHandling:[remoteMessage appData]];
    
}


//------------------ tokenRefreshNotification  --------------------

// [START refresh_token]
- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    
    NSLog(@"InstanceID token: %@", refreshedToken);
    
    
    if ([refreshedToken length]!=0)
    {
        //hit api here
        singleton.devicek_tkn=[NSString stringWithFormat:@"%@",refreshedToken];
        NSLog(@"singleton.devicek_tkn: %@",  singleton.devicek_tkn);
//        UMAPIManager *api = [[UMAPIManager alloc]init];
//        __block  typeof(self) weakSelf = self;
//        [api hitKeyEncryptionApi:singleton.devicek_tkn withComplition:^(id response) {
//            [weakSelf hitInitAPI];
//        }];
        [[NSUserDefaults standardUserDefaults] setObject:refreshedToken forKey:@"DEVICETOKENFIREBASEKEY"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [self performSelector:@selector(hitGCMAPINotifier) withObject:nil afterDelay:0.0];
        
        
    }
    // Connect to FCM since connection may have failed when attempted before having a token.
    [self connectToFcm];
    // TODO: If necessary send token to application server.
}


- (void)GCMHitNotifier:(NSNotification *) notification
{
    
    if ([[notification name] isEqualToString:@"HITGCMAPI"])
    {
        
        //hit api for GCM here
        if ([singleton.user_tkn length]!=0)
        {
            NSString * devicetoken = [[NSUserDefaults standardUserDefaults] valueForKey:@"DEVICETOKENFIREBASEKEY"];
            
            
            
            NSLog(@"devicetoken=%@",devicetoken);
            if (devicetoken == nil)
            {
                
            }
            else
            {
                singleton.devicek_tkn=devicetoken;
                if ([singleton.devicek_tkn length]!=0) {
                    [self hitGCMAPI];
                    NSLog (@"Successfully received the test notification!");
                    
                    
                }
            }
        }
    }
}


-(void)hitGCMAPINotifier

{
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:singleton.devicek_tkn forKey:@"USER_DEVICETOKEN"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"HITGCMAPI" object:nil userInfo:userInfo];
}


//----- HIT GCM API ------
-(void)hitGCMAPI
{
    NSLog(@"inside hitGCMAPI");
    
    NSString * devicetoken = [[NSUserDefaults standardUserDefaults] valueForKey:@"DEVICETOKENFIREBASEKEY"];
    
    singleton.devicek_tkn=devicetoken;
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:@"" forKey:@"mno"];
    [dictBody setObject:devicetoken forKey:@"gcmid"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:@"" forKey:@"lang"];
    [dictBody setObject:@"ios" forKey:@"pltfrm"];
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_UPDATE_GCM_TOKEN withBody:dictBody andTag:TAG_REQUEST_UPDATE_GCM_TOKEN completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
     {
         if (error == nil)
         {
             NSLog(@"Server Response = %@",response);
             
             //----- below value need to be forword to next view according to requirement after checking Android apk-----
             
             //NSString *rc=[response valueForKey:@"rc"];
             //NSString *rs=[response valueForKey:@"rs"];
             
             // NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
             
             //NSLog(@"value of rc =%@ \n value of rs=%@ \n value of tkn=%@ ",rc,rs,tkn);
             //----- End value need to be forword to next view according to requirement after checking Android apk-----
             //  NSString *rd=[response valueForKey:@"rd"];
             
             //remove GCMAPI notifier
             /* [[NSNotificationCenter defaultCenter] removeObserver:self
              name:@"HITGCMAPI"
              object:nil];
              
              */
             
             if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                 
             {
                 
                 
                 
             }
             
         }
         else{
             /* NSLog(@"Error Occured = %@",error.localizedDescription);
              UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
              message:error.localizedDescription
              delegate:self
              cancelButtonTitle:@"OK"
              otherButtonTitles:nil];
              [alert show];
              */
         }
         
     }];
    
}



// [END refresh_token]



// [START connect_to_fcm]
- (void)connectToFcm {
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
        }
    }];
}
// [END connect_to_fcm]





- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Unable to register for remote notifications: %@", error);
}



// This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
// If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
// the InstanceID token.
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    
    
    NSString * token = [NSString stringWithFormat:@"%@", deviceToken];
    //Format token as you need:
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    
    
    NSLog(@"deviceToken: %@", token);
    
    // With swizzling disabled you must set the APNs token here.
    //  [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeProd];//need to change here
}





- (void)applicationDidBecomeActive:(UIApplication *)application {
    // [self connectToFcm];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0]; // this one
    
    if(self.snapshotImageView != nil) {
        [self.snapshotImageView removeFromSuperview];
        self.snapshotImageView = nil;
    }
}




// [END disconnect_from_fcm]



//---------------------------------------------------------------
//            REMOTE NOTIFICATION HANDLE
//---------------------------------------------------------------




-(void)notificationHandling:(NSDictionary *)userInfo
{
    
    NSMutableArray *remoteMessage=[userInfo mutableCopy];
    
    if([remoteMessage count]!=0)
    {
        
        NSLog(@"value of received notification=%@",remoteMessage);
        // Check if message contains a data payload.
        
        /*
         {
         db       "type": "promo/trans",
         db       "subtype": "openApp/openAppWithDialog/playstore/webview/browser/openAppWithScreen/openAppWithTab/service/rating/share",
         
         "img": "",  //BIG Image if exists
         
         db       "title": "", //title of service /notificaiton
         db       "msg": "",  //msg to desplay in case of notification send
         "nicon": "", //this is the display image in the notification banner
         
         db       "nimg": "", // YES then show image else don’t show  notification images in the notification view, if no available show default
         
         db       "url": "",  // link with playstore and /webview/browser/service
         db       "screenname": “",//openAppWithScreen//openAppWithTab
         db       "datetime": "", //save for future use
         db       "nid": "",  //nid use for all operations to be used like delete et
         db       "dialogmsg": "", ///link with openAppWithDialog
         db       "serviceid": "", //if notification is of type service then its exists //to open it etc
         db       "state": "" //99 or other case in state id for filter
         db       "webpagetitle":""  //custom webpage title
         }
         
         
         if (openAppWithTab.equalsIgnoreCase("home")) {
         viewPager.setCurrentItem(0);
         } else if (openAppWithTab.equalsIgnoreCase("fav")) {
         viewPager.setCurrentItem(1);
         } else if (openAppWithTab.equalsIgnoreCase("state")) {
         viewPager.setCurrentItem(2);
         } else if (openAppWithTab.equalsIgnoreCase("allservices")) {
         viewPager.setCurrentItem(3);
         } else {
         viewPager.setCurrentItem(0);
         }
         
         
         
         NSLog(@"%@", userInfo);
         
         NSString *aps=[userInfo valueForKey:@"aps"];
         NSString *datetime=[userInfo valueForKey:@"datetime"];
         NSString *dialogmsg=[userInfo valueForKey:@"dialogmsg"];
         NSString *gcm_message_id=[userInfo valueForKey:@"gcm.message_id"];
         NSString *img=[userInfo valueForKey:@"img"];
         NSString *msg=[userInfo valueForKey:@"msg"];
         NSString *nicon=[userInfo valueForKey:@"nicon"];
         NSString *nid=[userInfo valueForKey:@"nid"];
         NSString *nimg=[userInfo valueForKey:@"nimg"];
         NSString *screenname=[userInfo valueForKey:@"screenname"];
         NSString *serviceid=[userInfo valueForKey:@"serviceid"];
         NSString *state=[userInfo valueForKey:@"state"];
         NSString *subtype=[userInfo valueForKey:@"subtype"];
         NSString *title=[userInfo valueForKey:@"title"];
         NSString *type=[userInfo valueForKey:@"type"];
         NSString *url=[userInfo valueForKey:@"url"];
         NSString *webpagetitle=[userInfo valueForKey:@"webpagetitle"];
         
         
         {
         aps =     {
         alert =         {
         body = "This is a test trans NCERT message.";
         title = ORS;
         };
         };
         datetime = "12-17-2016 12:15:37";
         dialogmsg = "";
         "gcm.message_id" = "0:1481957346630358%4a6bd3f64a6bd3f6";
         img = "";
         msg = "This is a test trans NCERT message.";
         nicon = "https://static.umang.gov.in/app/ico/service/ncert.png";
         nid = "0fcf3ba4-0475-46b8-a59a-f087ded22768";
         nimg = "https://static.umang.gov.in/app/ico/service/ncert.png";
         screenname = "";
         serviceid = 9;
         state = 31;
         subtype = service;
         title = ORS;
         type = trans;
         url = "https://web.umang.gov.in/uw/api/deptt/ncert/";
         webpagetitle = "Custom Title";
         }
         */
        
        
        
        NSLog(@"type:%@",[remoteMessage valueForKey:@"type"]);
        NSLog(@"subtype:%@",[remoteMessage valueForKey:@"subtype"]);
        
        NSString *type=[remoteMessage valueForKey:@"type"];
        NSString *subtype=[remoteMessage valueForKey:@"subtype"];
        NSString *img=[remoteMessage valueForKey:@"img"];
        NSString *title=[remoteMessage valueForKey:@"title"];
        NSString *msg=[remoteMessage valueForKey:@"msg"];
        NSString *nicon=[remoteMessage valueForKey:@"nicon"];
        NSString *nImg=[remoteMessage valueForKey:@"nimg"];
        NSString *url = [remoteMessage valueForKey:@"url"];
        
        
        NSString *screenname=[remoteMessage valueForKey:@"screenname"];
        NSString *datetime=[remoteMessage valueForKey:@"datetime"];
        NSString *nId=[remoteMessage valueForKey:@"nid"];
        NSString *dialogmsg=[remoteMessage valueForKey:@"dialogmsg"];
        NSString *serviceId=[remoteMessage valueForKey:@"serviceid"];
        NSString *state=[remoteMessage valueForKey:@"state"];
        NSString *webpagetitle=[remoteMessage valueForKey:@"webpagetitle"];
        
        
        
        //==========================
        NSString *servicename=@"";
        
        if([remoteMessage valueForKey:@"servicename"] != nil)
        {
            // The key existed...
            servicename=[remoteMessage valueForKey:@"servicename"];
        }
        else
        {
            servicename=@"";
        }
        
        NSString *deptname=@"";
        if([remoteMessage valueForKey:@"deptname"] != nil)
        {
            // The key existed...
            deptname=[remoteMessage valueForKey:@"deptname"];
            
        }
        else
        {
            deptname=@"";
        }
        

        if ([servicename length]==0 ||[servicename isEqualToString:@"(null)"])  //new line added
        {
            servicename =@"";
        }
        if ([deptname length]==0 ||[deptname isEqualToString:@"(null)"])  //new line added
        {
            deptname =@"";
        }
        
        //==========================

        
        
        
        //condition for if user only login then he will be able to get notification
        // if (pref.getPref(MyPreferences.PREF_MPIN_SET, "").equalsIgnoreCase("true")) {
        
        /*
         
         "all" =  "All";
         "none" =  "None";
         "promotional_small" =  "Promotional";
         "transactional_small" =  "Transactional";
         
         
         
         */
        
        // BOOL isEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:@"IS_NOTIFICATION_ENABLED"];
        BOOL isEnabled = YES;//NEW CHANGES
        
        if (isEnabled)
        {
            // They're equal
            BOOL showNotif ;
            
            if([serviceId length]!=0)
            {
                //serviceId
                NSLog(@"SHOW NOTIFICATION...............................");
                
                if ([[singleton.notiTypeSelected uppercaseString] isEqualToString:[NSLocalizedString(@"all", nil) uppercaseString]])
                {
                    //showNotif =[self isServiceNotifEnabled:serviceId];
                }
                else  if ([[singleton.notiTypeSelected uppercaseString] isEqualToString:[NSLocalizedString(@"promotional_small", nil) uppercaseString]])
                {
                    //showNotif =[self isServiceNotifEnabled:serviceId];
                }
                else  if ([[singleton.notiTypeSelected uppercaseString] isEqualToString:[NSLocalizedString(@"transactional_small", nil) uppercaseString]])
                {
                    //showNotif =[self isServiceNotifEnabled:serviceId];
                }
            }
            else
            {
                //showNotif=true;
            }
            showNotif=true;//ignore condtion as they will lock notification from server
            
            if (showNotif)
            {
                // NSString *dateStartString =@"31/12/2010 11:04:02";
                
                NSDate *today = [NSDate date];
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
                NSString * dateStr = [dateFormatter stringFromDate:today];
                
                
                
                // datetime break with empty
                
                NSArray *array = [datetime componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                NSString *notifDate=@"";
                NSString *notifTime=@"";
                
                if([array count]!=0)
                {
                    notifDate=[array objectAtIndex:0];
                    notifTime=[array objectAtIndex:1];
                    
                }
                //milliseconds time for delete undo function and sort using it.
                
                NSTimeInterval  TimeMills=[[NSDate date] timeIntervalSince1970];
                NSString *currentTimeMills=[NSString stringWithFormat:@"%f",TimeMills];
                
                
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationRecievedComplete" object:nil];
                
                
                
                
                //----------- Insert data in database--------------------------
               /* [singleton.dbManager insertNotifData:nId notifTitle:title notifImg:nImg notifMsg:msg notifType:type notifDate:notifDate notifTime:notifTime notifState:state notifIsFav:@"false" serviceId:serviceId currentTimeMills:currentTimeMills subType:subtype url:url screenName:screenname receiveDateTime:dateStr dialogMsg:dialogmsg webpageTitle:webpagetitle user_id:singleton.user_id];
                */
                
                  [singleton.dbManager insertNotifData:nId notifTitle:title notifImg:nImg notifMsg:msg notifType:type notifDate:notifDate notifTime:notifTime notifState:state notifIsFav:@"false" serviceId:serviceId currentTimeMills:currentTimeMills subType:subtype url:url screenName:screenname receiveDateTime:dateStr dialogMsg:dialogmsg webpageTitle:webpagetitle servicename:servicename deptname:deptname user_id:singleton.user_id];
                
                
                
                //----------- Send Notificaiton using value recieved--------------------------
                /* [self sendNotification:nId title:title nIcon:nicon nImg:nImg img:img msg:msg type:type dateTime:notifDate timeTime:notifTime state:state serviceId:serviceId subType:subtype url:url screenName:screenname dateStr:dateStr dialogMsg:dialogmsg webpageTitle:webpagetitle];*/

                [self sendNotification:nId title:title nIcon:nicon nImg:nImg img:img msg:msg type:type dateTime:notifDate timeTime:notifTime state:state serviceId:serviceId subType:subtype url:url screenName:screenname dateStr:dateStr dialogMsg:dialogmsg webpageTitle:webpagetitle servicename:servicename deptname:deptname];
                
                
            }
            
        }
    }
    
}


-(BOOL)isServiceNotifEnabled:(NSString*)serviceId
{
    BOOL showNotif = false;
    if(serviceId!=NULL)
    {
        if([serviceId length]!=0)
        {
            NSArray *notifAlist =[singleton.dbManager getServicesDataForNotifSettings];
            
            for (int i = 0; i < [notifAlist count]; i++)
            {
                if([[notifAlist objectAtIndex:i] valueForKey:@"SERVICE_IS_NOTIF_ENABLED"])
                {
                    NSLog(@"value of receive notif true=%@",[[notifAlist objectAtIndex:i] valueForKey:@"SERVICE_IS_NOTIF_ENABLED"]);
                    showNotif = true;
                }
                else
                {
                    NSLog(@"value of receive notif true=%@",[[notifAlist objectAtIndex:i] valueForKey:@"SERVICE_IS_NOTIF_ENABLED"]);
                    showNotif = false;
                }
            }
        }
        else
            showNotif = true;
    }
    else
        showNotif = true;
    return showNotif;
}


//-----------------------

/*-(void)sendNotification:(NSString*)nID title:(NSString *)title  nIcon:(NSString *)nIcon nImg:(NSString *)nImg img:(NSString *)img msg:(NSString *)msg type:(NSString *)type dateTime:(NSString *)dateTime timeTime:(NSString *)timeTime state:(NSString *)state serviceId:(NSString *)serviceId  subType:(NSString *)subType url:(NSString *)url screenName:(NSString *)screenName dateStr:(NSString *)dateStr dialogMsg:(NSString *)dialogMsg  webpageTitle:(NSString *)webpageTitle*/

-(void)sendNotification:(NSString*)nID title:(NSString *)title  nIcon:(NSString *)nIcon nImg:(NSString *)nImg img:(NSString *)img msg:(NSString *)msg type:(NSString *)type dateTime:(NSString *)dateTime timeTime:(NSString *)timeTime state:(NSString *)state serviceId:(NSString *)serviceId  subType:(NSString *)subType url:(NSString *)url screenName:(NSString *)screenName dateStr:(NSString *)dateStr dialogMsg:(NSString *)dialogMsg  webpageTitle:(NSString *)webpageTitle servicename:(NSString*)servicename deptname:(NSString*)deptname


{
   
    
    if ([nID length]==0) {
        nID=@"";
    }
    if ([title length]==0) {
        title=@"";
        
    }
    
    if ([nIcon length]==0) {
        nIcon=@"";
        
    }
    
    if ([nImg length]==0) {
        nImg=@"";
        
    }
    
    if ([img length]==0) {
        img=@"";
        
    }
    
    if ([msg length]==0) {
        msg=@"";
        
    }
    
    if ([type length]==0) {
        type=@"";
        
    }
    
    /*   if ([dateTime length]==0) {
     dateTime=@"";
     
     }
     
     if ([timeTime length]==0) {
     timeTime=@"";
     
     }*/
    
    if ([state length]==0) {
        state=@"";
        
    }
    
    
    
    
    if ([serviceId length]==0) {
        serviceId=@"";
        
    }
    if ([subType length]==0) {
        subType=@"";
        
    }
    
    if ([url length]==0) {
        url=@"";
        
    }
    
    if ([screenName length]==0) {
        screenName=@"";
        
    }
    
    /* if ([dateStr length]==0) {
     dateStr=@"";
     
     }*/
    
    if ([dialogMsg length]==0) {
        dialogMsg=@"";
        
    }
    
    if ([webpageTitle length]==0) {
        webpageTitle=@"";
        
    }
    
    
    if ([servicename length]==0) {
        servicename=@"";
    }
    if ([deptname length]==0) {
        deptname=@"";
    }
    
    
    NSMutableDictionary *notifydata=[NSMutableDictionary new];
    [notifydata setObject:type forKey:@"type"];
    [notifydata setObject:subType forKey:@"subtype"];
    [notifydata setObject:img forKey:@"img"];
    [notifydata setObject:title forKey:@"title"];
    [notifydata setObject:msg forKey:@"msg"];
    [notifydata setObject:nIcon forKey:@"nicon"];
    [notifydata setObject:nImg forKey:@"nimg"];
    [notifydata setObject:url forKey:@"url"];
    [notifydata setObject:screenName forKey:@"screenname"];
    [notifydata setObject:nID forKey:@"nid"];
    [notifydata setObject:dialogMsg forKey:@"dialogmsg"];
    [notifydata setObject:serviceId forKey:@"serviceid"];
    [notifydata setObject:state forKey:@"state"];
    [notifydata setObject:webpageTitle forKey:@"webpagetitle"];
   //add later
    [notifydata setObject:servicename forKey:@"servicename"];
    [notifydata setObject:deptname forKey:@"deptname"];

    
    NSLog(@"notifydata=%@",notifydata);
    
    //NSLog(@"getNotifData =%@",[singleton.dbManager getNotifData]);
    
    //----------Handle case for subType---------------
    
    subType=[subType  lowercaseString];
    // Case to open app  for opening app with notification title/
    if([subType isEqualToString:@"openApp"])
    {
        [self subtypeOpenApp:notifydata];
    }
    // Case to open app  with dialog message dialogmsg/msg
    
    else if([subType isEqualToString:@"openAppWithDialog"])
    {
        [self subtypeopenAppWithDialog:notifydata];
        
    }
    // Case to open  playstore url in external
    
    else if([subType isEqualToString:@"playstore"])
    {
        [self subtypeplaystore:notifydata];
        
        
    }
    // Case to open app  inside webview with custom webview title
    
    else if([subType isEqualToString:@"webview"])
    {
        [self subtypewebview:notifydata];
        
    }
    // Case to open app  in mobile browser
    
    else if([subType isEqualToString:@"browser"])
    {
        [self subtypebrowser:notifydata];
        
    }
    // Case to open app  with Screen Name like profile/settings etc
    
    else if([subType isEqualToString:@"openAppWithScreen"])
    {
        [self subtypeopenAppWithTab:notifydata];
        
    }
    // Case to open app  with tab name
    
    else if([subType isEqualToString:@"openAppWithTab"])
    {
        [self subtypeopenAppWithTab:notifydata];
        
    }
    // Case to open app with service
    
    else  if([subType isEqualToString:@"service"])
    {
        
        [self subtypeservice:notifydata];
        
    }
    // Case to open app  for rating view
    
    else  if([subType isEqualToString:@"rating"])
    {
        [self subtyperating:notifydata];
        
    }
    // Case to open app  for share [sharing message will recieve inside api)
    else  if([subType isEqualToString:@"share"])
    {
        [self subtypeshare:notifydata];
    }
    //Default case
    else
    {
        [self subtypeDefault:notifydata];
    }
    
    UIApplicationState appState = [[UIApplication sharedApplication] applicationState];
    if (appState == UIApplicationStateActive) {
        /*  NSString *cancelTitle = @"Close";
         NSString *showTitle = @"Show";
         NSString *message = [[notifydata valueForKey:@"aps"] valueForKey:@"alert"];
         UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"My App Name"
         message:message
         delegate:self
         cancelButtonTitle:cancelTitle
         otherButtonTitles:showTitle, nil];
         [alertView show];
         
         */
    } else {
        //Do stuff that you would do if the application was not active
    }
    
    
}

// Case to default case

-(void)subtypeDefault:(NSDictionary*)userInfo
{
    
}

// Case to open app  for opening app with notification title/

-(void)subtypeOpenApp:(NSDictionary*)userInfo
{
    
}

// Case to open app  with dialog message dialogmsg/msg
-(void)subtypeopenAppWithDialog:(NSDictionary*)userInfo
{
    
    NSString *title =[userInfo valueForKey:@"title"];
    NSString *dialogMsg =[userInfo valueForKey:@"subtypeopenAppWithDialog"];
    
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:title message:dialogMsg delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
    [alert show];
    
}


// Case to open app  with dialog message dialogmsg/msg
-(void)subtypeplaystore:(NSDictionary*)userInfo
{
    
}

//"subtype": "openApp/openAppWithDialog/playstore/webview/browser/openAppWithScreen/openAppWithTab/service/rating/share"
// Case to open app  webview
-(void)subtypewebview:(NSDictionary*)userInfo
{
    
}

// Case to open app  browser
-(void)subtypebrowser:(NSDictionary*)userInfo
{
    
}


// Case to open app  openAppWithScreen
-(void)subtypeopenAppWithScreen:(NSDictionary*)userInfo
{
    
}



// Case to open app  openAppWithTab
-(void)subtypeopenAppWithTab:(NSDictionary*)userInfo
{
    
}


// Case to open app  service
-(void)subtypeservice:(NSDictionary*)userInfo
{
    
}

// Case to open app  rating
-(void)subtyperating:(NSDictionary*)userInfo
{
    
}

// Case to open app  share
-(void)subtypeshare:(NSDictionary*)userInfo
{
    
}



//---------------------------------------------------------------
//            REMOTE NOTIFICATION HANDLE
//---------------------------------------------------------------





- (UIStoryboard *)grabStoryboard {
    
    // determine screen size
    int screenHeight = [UIScreen mainScreen].bounds.size.height;
    UIStoryboard *storyboard;
    
    switch (screenHeight) {
            
            // iPhone 4s
        case 480:
            storyboard = [UIStoryboard storyboardWithName:@"Main-4s" bundle:nil];
            break;
            
            // iPhone 5s
        case 568:
            storyboard = [UIStoryboard storyboardWithName:@"Main-5" bundle:nil];
            break;
            
            // iPhone 6
        case 667:
            storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            break;
            
            // iPhone 6 Plus
        case 736:
            storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            break;
            
        default:
            // it's an iPad
            storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            break;
    }
    
    return storyboard;
}


- (void) receiveTestNotification:(NSNotification *) notification
{
    
    
    if ([[notification name] isEqualToString:@"FONTCHANGENOTIFICATION"])
    {
        NSLog (@"Successfully received the test notification!");
        
        NSDictionary *userInfo = notification.userInfo;
        //        NSString *fontkey = [userInfo objectForKey:@"FONT_SELECT_KEY"];
        //
        //        float fontsize=15.0;
        //        if ([fontkey isEqualToString:NSLocalizedString(@"small", nil)]) {
        //            fontsize=14.0;
        //        }
        //        if ([fontkey isEqualToString:NSLocalizedString(@"normal", nil)]) {
        //            fontsize=16.0;
        //        }
        //        if ([fontkey isEqualToString:NSLocalizedString(@"large", nil)]) {
        //            fontsize=18.0;
        //        }
        //        [[UILabel appearance] setFont:[UIFont systemFontOfSize:fontsize]];
        
        
        NSInteger fontkeyIndex = [[userInfo objectForKey:@"FONT_SELECT_KEY_INDEX"] integerValue];
        
        float fontsize=15.0;
        
        if (fontkeyIndex == 0) {
            fontsize=14.0;
        }
        else if (fontkeyIndex == 1){
            fontsize=16.0;
            
        }
        else if (fontkeyIndex == 2){
            fontsize=18.0;
        }
        
        [[UILabel appearance] setFont:[UIFont systemFontOfSize:fontsize]];
        
        
        // [[NSNotificationCenter defaultCenter] removeObserver:self name:@"FONTCHANGENOTIFICATION" object:nil]; }
        
    }
    if ([[notification name] isEqualToString:@"SPOTLIGHTSEARCH"])
    {
        
        if ([CSSearchableItemAttributeSet class])
            [self setUpCoreSpotlight];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SPOTLIGHTSEARCH" object:nil];
    }
}

/*
 -(void)keepLogin
 {
 
 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
 
 BOOL myBool = [defaults boolForKey:@"LOGIN_KEY"];
 
 myBool=false;
 
 
 
 if (myBool==true)
 {
 //jump to hometabbar
 
 
 UITabBarController *tbc =[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"TabBarController"];
 
 //[self.storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
 //tbc.selectedIndex=[SharedManager getSelectedTabIndex];
 tbc.selectedIndex=0;
 
 UIViewController *vc=[self topMostController];
 
 [vc presentViewController:tbc animated:NO completion:nil];
 
 
 
 }
 else // Set already saved language
 {
 
 //
 }
 }
 */

//-----------------------------------------------
//      Check Network Status
//-----------------------------------------------


-(void)checkNetworkStatus
{
    
    // Allocate a reachability object
    
    singleton=[SharedManager sharedSingleton];
    
    singleton.reach = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    
    
    
    
    
    
    topvc=[self topMostController];
    
    /*  UIView *removeView;
     while((removeView = [topvc.view viewWithTag:1779]) != nil) {
     [removeView removeFromSuperview];
     }
     */
    
    viewFromNib = [[NSBundle mainBundle] loadNibNamed:@"NetworkView" owner:nil options:nil].firstObject;
    viewFromNib.tag=1779;
    viewFromNib.frame=frameNetworkview;
    __weak typeof(self) weakSelf = self;
    
    
    singleton.reach.reachableBlock = ^(Reachability*reach)
    {
        // keep in mind this is called on a background thread
        // and if you are updating the UI it needs to happen
        // on the main thread, like this:
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"REACHABLE!");
            networkStatus=true;
            
            [viewFromNib removeFromSuperview];
            
        });
    };
    
    singleton.reach.unreachableBlock = ^(Reachability*reach)
    {
        
        
        
        UITabBarController *tabBar = (UITabBarController *) topvc;
        if ([tabBar isKindOfClass:[TabBarVC class]]) {
            UINavigationController *selectedNavi = (UINavigationController *)tabBar.selectedViewController;
            if ([selectedNavi.visibleViewController isKindOfClass:[EmailSupportViewController class]])
            {
                return;
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // code here
            //[topvc.view addSubview:viewFromNib];
            [topvc.view addSubview:viewFromNib];
            
            networkStatus=false;
            
            NSLog(@"UNREACHABLE!");
        });
        
    };
    
    
    
}






- (UIViewController *)topViewController{
    return [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
    
}

- (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil) {
        return rootViewController;
    }
    
    if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        return [self topViewController:lastViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [self topViewController:presentedViewController];
}


//-------- Get top Most view to add network status view-----
- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}


//-------------------------------------------------
//-----------------------------------------------


//-------------------------------------------------
//-----------------------------------------------




// This is the same information which we added in URL Scheme in info.plist
#define FACEBOOK_SCHEME @"fb1480981565262960"
//#define GOOGLE_SCHEME @"com.googleusercontent.apps.207885170031-9u2p59hea1c1je5ljb1hhcoll6brc3sh"

//for production
//#define GOOGLE_SCHEME @"com.googleusercontent.apps.616074837787-2sk6q2enilnp1gvv77nuh8ffenvrvv28"


//for staging
#//define GOOGLE_SCHEME @"com.googleusercontent.apps.616074837787-vep6mtjtctu3i1pufcndo16u8sv08h0l"

#define App_Scheme   @"com.spicedigital.umang"



//com.googleusercontent.apps.207885170031-9u2p59hea1c1je5ljb1hhcoll6brc3sh

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    singleton = [SharedManager sharedSingleton];

    if ([[url scheme] isEqualToString:singleton.apiMode.GOOGLE_SCHEME]){
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:sourceApplication
                                          annotation:annotation];
        
    }
    if ([[url scheme] isEqualToString:FACEBOOK_SCHEME])
    {
        return  [[FBSDKApplicationDelegate sharedInstance] application:application
                                                               openURL:url
                                                     sourceApplication:sourceApplication
                                                            annotation:annotation
                 ];
        
    }
    if ([[url scheme] isEqualToString:App_Scheme])
    {
        NSLog(@"Calling Application Bundle ID: %@", sourceApplication);
        NSLog(@"URL scheme:%@", [url scheme]);
        NSLog(@"URL query: %@", [url query]);
        
        return YES;
    }
    
    
    return NO;
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options {
    singleton = [SharedManager sharedSingleton];

    if ([[url scheme] isEqualToString:singleton.apiMode.GOOGLE_SCHEME])
    {
        
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                          annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
        
    }
    if ([[url scheme] isEqualToString:App_Scheme])
    {
        NSLog(@"URL scheme:%@", [url scheme]);
        NSLog(@"URL query: %@", [url query]);
        
        /// Open home detail VC with url,dept Id and dept Name
        
        if (singleton.user_tkn.length != 0)
        {
            
            //com.spicedigital.umang://?open=https://stgweb.umang.gov.in/pmkvy/api/deptt/pmkvyHtml/&deptId=21&deptName=pmkvy
            
            
            UIStoryboard *storyboard  = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
            tbc.selectedIndex = 0; //[singleton getSelectedTabIndex];
            // tbc.selectedIndex=0;
            //self.window.rootViewController = tbc;
            [self.window setRootViewController:tbc];
            
            HomeDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
            
            NSString *argsAsString = [(NSString*)[url query]stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"/\"" withString:@"\""];
            argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"\"{" withString:@"{"];
            argsAsString= [argsAsString stringByReplacingOccurrencesOfString: @"}\"" withString:@"}"];
            
            NSMutableDictionary *queryStringDictionary = [[NSMutableDictionary alloc] init];
            NSArray *urlComponents = [argsAsString componentsSeparatedByString:@"&"];
            
            for (NSString *keyValuePair in urlComponents)
            {
                NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
                NSString *key = [[pairComponents firstObject] stringByRemovingPercentEncoding];
                NSString *value = [[pairComponents lastObject] stringByRemovingPercentEncoding];
                
                [queryStringDictionary setObject:value forKey:key];
            }
        
            vc.dic_serviceInfo = queryStringDictionary;
            vc.tagComeFrom=@"OTHERS";
            
            topvc=[self topMostController];
            
            [topvc presentViewController:vc animated:YES completion:nil];            
            [UIView transitionWithView:self.window duration:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:nil];
        }
        
        return YES;
    }
    
    return [[FBSDKApplicationDelegate sharedInstance] application:app
                                                          openURL:url
                                                sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                       annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    
}

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    
    if (error) {
        NSLog(@"Error occured %@",error.localizedDescription);
    }
    else{
        NSLog(@"Google Auth Compeleted With userId= %@ && idToken= %@ && fullName= %@ && givenName= %@ && familyName= %@ && email= %@",user.userID,user.authentication.idToken,user.profile.name,user.profile.givenName,user.profile.familyName,user.profile.email);
        
        
        NSLog(@"singleton.SOCIAL_GOOGLE_FROM  %@",singleton.SOCIAL_GOOGLE_FROM );
        
        if ([singleton.SOCIAL_GOOGLE_FROM isEqualToString:@"GOOGLE_LOGIN"])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"GOOGLE_LOGIN" object:user];
            
        }
        else if ([singleton.SOCIAL_GOOGLE_FROM isEqualToString:@"GOOGLE_LOGIN_SOCIAL"])
        {
            
            
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"GOOGLE_LOGIN_SOCIAL" object:user];
            
            
        }
        
        singleton.SOCIAL_GOOGLE_FROM=@"";
        
        
        
        
        
        
        
    }
}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // ...
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    backgroundUpdateTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [self endBackgroundUpdateTask];
    }];
    
    
    
}
- (void) endBackgroundUpdateTask
{
    [[UIApplication sharedApplication] endBackgroundTask:backgroundUpdateTask];
    backgroundUpdateTask = UIBackgroundTaskInvalid;
}


// [START disconnect_from_fcm]
- (void)applicationDidEnterBackground:(UIApplication *)application {
    //[[FIRMessaging messaging] disconnect];
    NSLog(@"Disconnected from FCM");
    
    self.snapshotImageView = [[UIView alloc]initWithFrame:[self.window frame]];
    self.snapshotImageView.backgroundColor = [UIColor lightGrayColor];
    self.snapshotImageView.alpha = 1.0;
    [self.window addSubview:self.snapshotImageView];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NotificationRecievedComplete" object:nil];
    
    self.backgroundedDate = [NSDate date];
    
    [self saveHTTPCookies];
}



- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    
    if (self.backgroundedDate)
    {
        BOOL isTimedout =[self.backgroundedDate timeIntervalSinceNow] <= -(7 * 60);
        
        if (isTimedout)
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"N" forKey:@"ChatSession"];
        }
    }
    
    
    
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [self endBackgroundUpdateTask];
    
    [self loadHTTPCookies];
    
    /*UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     //updMPinVC = [storyboard instantiateViewControllerWithIdentifier:@"UpdMpinVC"];
     
     verifyMpinView = [storyboard instantiateViewControllerWithIdentifier:@"ChangeRegMobMpinVC"];
     verifyMpinView.dic_info = [NSMutableDictionary new];
     [verifyMpinView.dic_info setValue:@"fingerprint" forKey:@"TAG"];
     
     UIViewController *topController = [self topMostController];
     
     [topController presentViewController:verifyMpinView animated:YES completion:^{
     
     [self loginUsingTouchId];
     }];*/
    
    NSMutableDictionary *initArray = [NSMutableDictionary new];
    initArray = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"InitAPIResponse"] mutableCopy];
    

    if (initArray != nil && initArray.count > 0)
    {
        if (CurrentAppVersion < [[initArray valueForKey:@"ver"] integerValue])
        {
            if ([[initArray valueForKey:@"fupd"] boolValue] == TRUE)
            {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"account_blocked_desp_txt", nil) preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                           {
                                               
                                               [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id1236448857"]];;
                                               
                                           }];
                
                
                [alert addAction:okAction];
                
                
                
                [[self topMostController] presentViewController:alert animated:YES completion:nil];
            }
            
            
        }
    }
    
    
    
    
    
    
    
    
}

-(void)loginUsingTouchId
{
    LAContext *myContext = [[LAContext alloc] init];
    NSError *authError = nil;
    NSString *myLocalizedReasonString =  @"Login using Touch Id";
    
    if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]) {
        [myContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                  localizedReason:myLocalizedReasonString
                            reply:^(BOOL success, NSError *error) {
                                if (success) {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        //[self performSegueWithIdentifier:@"Success" sender:nil];
                                        
                                        NSLog(@"Authentication Clear");
                                        
                                        
                                        [verifyMpinView dismissViewControllerAnimated:YES completion:nil];
                                        
                                    });
                                } else
                                {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        
                                        switch (error.code)
                                        {
                                            case LAErrorAuthenticationFailed:
                                                NSLog(@"Authentication Failed");
                                                // Rather than show a UIAlert here, use the error to determine if you should push to a keypad for PIN entry.
                                                
                                                break;
                                                
                                            case LAErrorUserCancel:
                                                NSLog(@"User pressed Cancel button");
                                                break;
                                                
                                            case LAErrorUserFallback:
                                                NSLog(@"User pressed \"Enter Password\"");
                                                break;
                                                
                                            default:
                                                NSLog(@"Touch ID is not configured");
                                                break;
                                        }
                                        NSLog(@"Authentication Fails");
                                        
                                    });
                                }
                            }];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                message:authError.description
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil, nil];
            [alertView show];
            // Rather than show a UIAlert here, use the error to determine if you should push to a keypad for PIN entry.
        });
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    [[NSUserDefaults standardUserDefaults] setObject:@"N" forKey:@"ChatSession"];
    [self saveHTTPCookies];
    singleton.user_tkn=@"";
    
}


-(void)loadHTTPCookies
{
    NSMutableArray* cookieDictionary = [[NSUserDefaults standardUserDefaults] valueForKey:@"cookieArray"];
    
    for (int i=0; i < cookieDictionary.count; i++)
    {
        NSMutableDictionary* cookieDictionary1 = [[NSUserDefaults standardUserDefaults] valueForKey:[cookieDictionary objectAtIndex:i]];
        NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookieDictionary1];
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
    }
}

-(void)saveHTTPCookies
{
    NSMutableArray *cookieArray = [[NSMutableArray alloc] init];
    for (NSHTTPCookie *cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
        [cookieArray addObject:cookie.name];
        NSMutableDictionary *cookieProperties = [NSMutableDictionary dictionary];
        [cookieProperties setObject:cookie.name forKey:NSHTTPCookieName];
        [cookieProperties setObject:cookie.value forKey:NSHTTPCookieValue];
        [cookieProperties setObject:cookie.domain forKey:NSHTTPCookieDomain];
        [cookieProperties setObject:cookie.path forKey:NSHTTPCookiePath];
        [cookieProperties setObject:[NSNumber numberWithUnsignedInteger:cookie.version] forKey:NSHTTPCookieVersion];
        [cookieProperties setObject:[[NSDate date] dateByAddingTimeInterval:2629743] forKey:NSHTTPCookieExpires];
        
        [[NSUserDefaults standardUserDefaults] setValue:cookieProperties forKey:cookie.name];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:cookieArray forKey:@"cookieArray"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (NSUInteger) application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    //    if(self.shouldRotate)
    //    {
    //        //self.shouldRotate = NO;
    //        return UIInterfaceOrientationMaskAll;
    //    }
    
    
    if(self.shouldRotate)
    {
        //self.shouldRotate = NO;
        return UIInterfaceOrientationMaskPortrait;
    }
    
    return UIInterfaceOrientationMaskPortrait;
}

@end
