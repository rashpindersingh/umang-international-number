//
//  AppDelegate.h
//  Umang
//
//  Created by spice_digital on 31/08/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import "GAILogger.h"


/// XMPP related
#import "XMPPFramework.h"
#import "XMPPMessageArchiving.h"
#import "XMPPMessageArchivingCoreDataStorage.h"
#import "XMPPHandler.h"
#import "XMPPvCardCoreDataStorageObject.h"

@protocol ChatDelegate;

@interface AppDelegate : UIResponder <UIApplicationDelegate,XMPPRosterDelegate,XMPPHandlerDelegate>
{
    XMPPStream *xmppStream;
    XMPPReconnect *xmppReconnect;
    XMPPRoster *xmppRoster;
    XMPPRosterCoreDataStorage *xmppRosterStorage;
    XMPPvCardCoreDataStorage *xmppvCardStorage;
    XMPPvCardTempModule *xmppvCardTempModule;
    XMPPvCardAvatarModule *xmppvCardAvatarModule;
    XMPPCapabilities *xmppCapabilities;
    XMPPCapabilitiesCoreDataStorage *xmppCapabilitiesStorage;
    
    NSString *password;
    
    BOOL allowSelfSignedCertificates;
    BOOL allowSSLHostNameMismatch;
    
    BOOL isXmppConnected;
    
    XMPPMessageArchivingCoreDataStorage *xmppMessageArchivingCoreDataStorage;
    XMPPMessageArchiving *xmppMessageArchivingModule;
}
@property(assign,nonatomic)BOOL isHistoryLoaded;
@property(strong,nonatomic)NSMutableArray *chatHistoryLoad;
@property(strong,nonatomic)NSMutableArray *chatBotLoad;

@property (nonatomic, strong) NSMutableArray *selectedItems;
@property (assign) BOOL isFrameMode;


@property (strong, nonatomic) UIWindow *window;
@property(nonatomic,assign)BOOL networkStatus;
@property (assign, nonatomic) BOOL shouldRotate;
@property BOOL  isLandscapePreferred;

@property(assign,nonatomic)int badgeCount;

@property (nonatomic, strong) NSDate *backgroundedDate;

-(void)updateAppLanguage;


//------- Ebook ------------
@property(nonatomic, assign) int downloadBookNo;
@property(nonatomic,retain)NSMutableArray*downloadComplete;
@property(nonatomic,retain)NSMutableArray *downloadFilesArry;
//------- Ebook ------------



/// Related to XMPP

@property (nonatomic, strong, readonly) XMPPStream *xmppStream;
@property (nonatomic, strong, readonly) XMPPReconnect *xmppReconnect;
@property (nonatomic, strong, readonly) XMPPRoster *xmppRoster;
@property (nonatomic, strong, readonly) XMPPRosterCoreDataStorage *xmppRosterStorage;
@property (nonatomic, strong, readonly) XMPPvCardTempModule *xmppvCardTempModule;
@property (nonatomic, strong, readonly) XMPPvCardAvatarModule *xmppvCardAvatarModule;
@property (nonatomic, strong, readonly) XMPPCapabilities *xmppCapabilities;
@property (nonatomic, strong, readonly) XMPPCapabilitiesCoreDataStorage *xmppCapabilitiesStorage;
@property (nonatomic, strong, readonly) XMPPvCardCoreDataStorage *xmppvCardStorage;


@property (nonatomic, strong) XMPPMessageArchivingCoreDataStorage *xmppMessageArchivingCoreDataStorage;
@property (nonatomic, strong) XMPPMessageArchiving *xmppMessageArchivingModule;

@property (nonatomic,strong) XMPPHandler *xmppHandler;
- (NSManagedObjectContext *)managedObjectContext_roster;
- (NSManagedObjectContext *)managedObjectContext_capabilities;

@property (nonatomic,strong) id<ChatDelegate> chatDelegate;

-(void) configureXMPPWithJid:(NSString *)jidString andPassword:(NSString *)passwordString andChatSession:(BOOL)chatSession;

+(AppDelegate*)sharedDelegate;
- (UIViewController*) topMostController;
@end


@protocol ChatDelegate <NSObject>

-(void)friendStatusChange:(AppDelegate *)appD Presence:(XMPPPresence *)presence;
-(void)getNewMessage:(AppDelegate *)appD Message:(XMPPMessage *)message;
-(void)getXMPPConnectionStatus:(BOOL)status;


-(void)getchatSessionExpiredWithStatus:(BOOL)status;
@end
