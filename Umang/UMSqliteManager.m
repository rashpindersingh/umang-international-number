//
//  UMSqliteManager.m
//  Umang
//
//  Created by deepak singh rawat on 20/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "UMSqliteManager.h"
#import <sqlite3.h>
#import "UMAPIManager.h"
#import "StateList.h" //for fetching and getting state list
#import "UIView+Toast.h"
#import "AppDelegate.h"
#import "JBDetect.h"
#import "AppConstants.h"
@interface UMSqliteManager()

{
    SharedManager *singleton;
    StateList *obj;
}
@property(nonatomic,assign)sqlite3  *umangDB;
@property (nonatomic, strong) NSString *documentsDirectory;
@property (nonatomic, strong) NSString *databaseFilename;
@property (nonatomic, strong) NSMutableArray *arrResults;

@property (nonatomic, strong)NSString *databasePath ;


@end

@implementation UMSqliteManager
@synthesize umangDB;
@synthesize databasePath;
#pragma mark - Initialization

-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename{
    self = [super init];
    if (self) {
        // Set the documents directory path to the documentsDirectory property.
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        self.documentsDirectory = [paths objectAtIndex:0];
        
        // Keep the database filename.
        self.databaseFilename = dbFilename;
        NSLog(@"self.documentsDirectory=%@",self.documentsDirectory);
        NSLog(@"dbFilename=%@",dbFilename);
        
        // Copy the database file into the documents directory if necessary.
        // [self copyDatabaseIntoDocumentsDirectory];
    }
    return self;
}


+(NSString* )getDatabasePath
{
    NSString *docsDir;
    NSArray *dirPaths;
    //sqlite3 *DB;
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    // Build the path to the database file
    // NSString*  databasePath = [docsDir stringByAppendingPathComponent:@"UMANG_DATABASE.db"];
    NSString*  databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"UMANG_DATABASE.db"]];
    /*
     NSFileManager *filemgr = [[NSFileManager alloc]init];
     if ([filemgr fileExistsAtPath:databasePath]==NO) {
     const char *dbpath = [databasePath UTF8String];
     if (sqlite3_open(dbpath,&DB)==SQLITE_OK) {
     char *errorMessage;
     const char *sql_statement = "CREATE TABLE IF NOT EXISTS users(ID INTEGER PRIMARY KEY AUTOINCREMENT,FIRSTNAME TEXT,LASTNAME TEXT,EMAILID TEXT,PASSWORD TEXT,BIRTHDATE DATE)";
     if (sqlite3_exec(DB,sql_statement,NULL,NULL,&errorMessage)!=SQLITE_OK) {
     NSLog(@"Failed to create the table");
     }
     sqlite3_close(DB);
     }
     else{
     NSLog(@"Failded to open/create the table");
     }
     }
     // sqlite3_close(DB); //added late
     */
    NSLog(@"database path xxx=%@",databasePath);
    return databasePath;
}


+(NSString*)encodedString:(const unsigned char *)ch
{
    NSString *retStr;
    if(ch == nil)
        retStr = @"";
    else
        retStr = [NSString stringWithCString:(char*)ch encoding:NSUTF8StringEncoding];
    return retStr;
}
/*+(BOOL)executeScalarQuery:(NSString*)str
 {
 
 sqlite3_stmt *statement= nil;
 sqlite3 *database;
 NSLog(@"executeScalarQuery is called =%@",str);
 @try {
 
 BOOL fRet = NO;
 NSString *strPath =[NSString stringWithFormat:@"%@", [self getDatabasePath]];
 if (sqlite3_open([strPath UTF8String],&database) == SQLITE_OK)
 {
 if (sqlite3_prepare_v2(database, [str UTF8String], -1, &statement, NULL) == SQLITE_OK)
 {
 if (sqlite3_step(statement) == SQLITE_DONE)
 {
 
 fRet =YES;
 }
 else
 {
 NSLog(@"FAIL QUERY=%@ strPath=%@ ",str,strPath);
 NSLog(@"error: %s", sqlite3_errmsg(database));
 [UMSqliteManager executeScalarQuery:str];
 
 }
 sqlite3_finalize(statement);
 
 
 
 }
 else
 {
 NSLog(@"FAIL QUERY =====>> %@ strPath=%@ ",str,strPath);
 NSLog(@"error: %s", sqlite3_errmsg(database));
 NSLog(@"FAIL QUERY Problem with prepare statement: %s", sqlite3_errmsg(database));
 [UMSqliteManager executeScalarQuery:str];
 
 }
 sqlite3_close(database);
 
 //sqlite3_finalize(statement);
 }
 else
 {
 NSLog(@"FAIL QUERY An error has occured: %s",sqlite3_errmsg(database));
 [UMSqliteManager executeScalarQuery:str];
 
 }
 
 // sqlite3_finalize(statement);
 sqlite3_close(database);
 
 return fRet;
 
 } @catch (NSException *exception) {
 NSLog(@"FAIL QUERY exception=%@",exception);
 //sqlite3_finalize(statement);
 sqlite3_close(database);
 
 // NSLog(@"executeScalarQuery exception (%s)", sqlite3_errmsg(database));
 
 
 } @finally {
 
 sqlite3_close(database);       // NSLog(@"executeScalarQuery finally (%s)", sqlite3_errmsg(database));
 
 }
 }*/

/*+(BOOL)executeScalarQuery:(NSString*)str
 {
 sqlite3_stmt *statement= nil;
 sqlite3 *database;
 
 NSLog(@"executeScalarQuery is called =%@",str);
 
 
 // NSString *strPath =[NSString stringWithFormat:@"%@", [self getDatabasePath]]; // close on 12 sept and load exact path
 @synchronized(self)
 {
 @try {
 
 BOOL fRet = NO;
 
 NSString *docsDir;
 NSArray *dirPaths;
 //sqlite3 *DB;
 dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 docsDir = [dirPaths objectAtIndex:0];
 NSString*  strPath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"UMANG_DATABASE.db"]];
 
 
 
 const char *fileName = [strPath UTF8String];
 NSLog(@"path 00%s", fileName);
 
 
 
 if (sqlite3_open(fileName,&database) == SQLITE_OK)
 {
 
 if (sqlite3_prepare_v2(database, [str UTF8String], -1, &statement, NULL) == SQLITE_OK)
 {
 
 if (sqlite3_step(statement) == SQLITE_DONE)
 {
 fRet =YES;
 }
 else
 {
 
 NSLog(@"FAIL QUERY=%@ strPath=%@ ",str,strPath);
 
 NSLog(@"error: %s", sqlite3_errmsg(database));
 
 [UMSqliteManager executeScalarQuery:str];
 
 }
 
 sqlite3_finalize(statement);
 
 }
 else
 {
 
 NSLog(@"FAIL QUERY Problem with prepare statement: %s", sqlite3_errmsg(database));
 [UMSqliteManager executeScalarQuery:str];
 }
 
 }
 else
 {
 NSLog(@"FAIL QUERY An error has occured: %s",sqlite3_errmsg(database));
 [UMSqliteManager executeScalarQuery:str];
 }
 
 
 
 // sqlite3_finalize(statement);
 
 sqlite3_close(database);
 
 
 return fRet;
 
 
 
 } @catch (NSException *exception)
 {
 NSLog(@"FAIL QUERY exception=%@",exception);
 
 //sqlite3_finalize(statement);
 
 sqlite3_close(database);
 
 // NSLog(@"executeScalarQuery exception (%s)", sqlite3_errmsg(database));
 
 } @finally
 {
 //sqlite3_close(database);//later close it july 14
 
 NSLog(@"executeScalarQuery finally (%s)", sqlite3_errmsg(database));
 
 }
 
 
 }
 //===== start load local path====
 
 //===== end load local path====
 
 
 }*/ // Commented On 13 nov

+(BOOL)executeScalarQuery:(NSString*)str
{
    
    sqlite3_stmt *statement= nil;
    sqlite3 *database;
    
    NSLog(@"executeScalarQuery is called =%@",str);
    
    @synchronized(self)
    {
        @try
        {
            
            BOOL fRet = NO;
            
            NSString *docsDir;
            
            NSArray *dirPaths;
            
            //sqlite3 *DB;
            
            dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            
            docsDir = [dirPaths objectAtIndex:0];
            
            NSString*  strPath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"UMANG_DATABASE.db"]];
            
            const char *fileName = [strPath UTF8String];
            
            NSLog(@"path 00%s", fileName);
            
            
            if (sqlite3_open(fileName,&database) == SQLITE_OK)
            {
                
                if (sqlite3_prepare_v2(database, [str UTF8String], -1, &statement, NULL) == SQLITE_OK)
                {
                    
                    if (sqlite3_step(statement) == SQLITE_DONE)
                    {
                        fRet =YES;
                    }
                    else
                    {
                        NSLog(@"FAIL QUERY=%@ strPath=%@ ",str,strPath);
                        NSLog(@"error: %s", sqlite3_errmsg(database));
                        
                        [UMSqliteManager executeScalarQuery:str];
                        
                    }
                    sqlite3_finalize(statement);
                }
                else
                {
                    NSLog(@"FAIL QUERY Problem with prepare statement: %s", sqlite3_errmsg(database));
                    
                    [UMSqliteManager executeScalarQuery:str];
                }
            }
            else
            {
                
                NSLog(@"FAIL QUERY An error has occured: %s",sqlite3_errmsg(database));
                
                [UMSqliteManager executeScalarQuery:str];
                
            }
            
            // sqlite3_finalize(statement);
            
            sqlite3_close(database);
            
            return fRet;
            
        } @catch (NSException *exception)
        {
            NSLog(@"FAIL QUERY exception=%@",exception);
            
            sqlite3_close(database);
            
        } @finally
        {
            NSLog(@"executeScalarQuery finally (%s)", sqlite3_errmsg(database));
        }
        
    }
    
}

+(NSMutableArray *)executeQuery:(NSString*)str{
    
    @synchronized(self)
    {
        sqlite3_stmt *statement= nil; // fetch data from table
        sqlite3 *database;
        
        //NSString *strPath = [self getDatabasePath];//close by me 12 sept
        
        
        
        //===== start load local path====
        NSString *docsDir;
        NSArray *dirPaths;
        //sqlite3 *DB;
        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        docsDir = [dirPaths objectAtIndex:0];
        NSString*  strPath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"UMANG_DATABASE.db"]];
        
        //===== end load local path====
        
        
        
        NSMutableArray *allDataArray = [[NSMutableArray alloc] init];
        
        @try {
            if (sqlite3_open([strPath UTF8String],&database) == SQLITE_OK) {
                if (sqlite3_prepare_v2(database, [str UTF8String], -1, &statement, NULL) == SQLITE_OK) {
                    
                    
                    while (sqlite3_step(statement) == SQLITE_ROW) {
                        NSInteger i = 0;
                        NSInteger iColumnCount = sqlite3_column_count(statement);
                        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                        while (i< iColumnCount) {
                            NSString *str = [self encodedString:(const unsigned char*)sqlite3_column_text(statement, (int)i)];
                            
                            
                            NSString *strFieldName = [self encodedString:(const unsigned char*)sqlite3_column_name(statement, (int)i)];
                            
                            [dict setObject:str forKey:strFieldName];
                            i++;
                        }
                        
                        [allDataArray addObject:dict];
                    }
                }
                else
                {
                    
                    NSLog(@"inside FAILED (%s)", sqlite3_errmsg(database));
                }
                
                sqlite3_finalize(statement);
            }
            else
            {
                
                NSLog(@"Statement FAILED (%s)", sqlite3_errmsg(database));
            }
            sqlite3_close(database);
            
            return allDataArray;
        } @catch (NSException *exception) {
            
            //NSLog(@"executeQuery exception (%s)", sqlite3_errmsg(database));
            sqlite3_close(database);
            
        } @finally {
            
            // NSLog(@"executeQuery @finally (%s)", sqlite3_errmsg(database));
            sqlite3_close(database);
            
        }
        
    }
}








//-(NSString*)getNotifyFavStatus:(NSString*)notifyId
-(NSString*)getNotifyFavStatus:(NSString*)notifyId withUser_id:(NSString*)user_id

{
    NSString *query;
    query = [NSString stringWithFormat:@"SELECT NOTIF_IS_FAV FROM TABLE_NOTIFICATIONS where NOTIF_ID='%@' AND USER_ID='%@'",notifyId, user_id];
    NSArray *arry=[UMSqliteManager executeQuery:query];
    
    
    if ([arry count]>0)
    {
        return [[arry objectAtIndex:0]valueForKey:@"NOTIF_IS_FAV"];
        
    }
    
    return @"false";
    
    
}



//----------------------------------------------------------
//          insertNotifData TABLE_NOTIFICATIONS
//----------------------------------------------------------
/*
 
 -(void)insertNotifData:(NSString*)notifId
 notifTitle:(NSString*)notifTitle
 notifImg:(NSString*)notifImg
 notifMsg:(NSString*)notifMsg
 notifType:(NSString*)notifType
 notifDate:(NSString*)notifDate
 notifTime:(NSString*)notifTime
 notifState:(NSString*)notifState
 notifIsFav:(NSString*)notifIsFav
 serviceId:(NSString*)serviceId
 currentTimeMills:(NSString*)currentTimeMills
 subType:(NSString*)subType
 url:(NSString*)url
 screenName:(NSString*)screenName
 receiveDateTime:(NSString*)receiveDateTime
 dialogMsg:(NSString*)dialogMsg
 webpageTitle:(NSString*)webpageTitle
 user_id:(NSString*)user_id
 
 {
 // Prepare the query string.
 NSString *query;
 query = [NSString stringWithFormat:@"insert into TABLE_NOTIFICATIONS values(null, '%@', '%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')", notifId,notifTitle,notifImg, notifMsg,notifType,notifDate,notifTime,notifState,notifIsFav,serviceId,currentTimeMills,subType,url,screenName, receiveDateTime,dialogMsg,webpageTitle,user_id];
 
 if ([UMSqliteManager executeScalarQuery:query]==YES)
 {
 
 NSLog(@"Data  inserted successfully");
 
 }else{
 NSLog(@"Data not inserted successfully");
 
 }
 
 
 
 }
 
 */

-(void)insertNotifData:(NSString*)notifId
            notifTitle:(NSString*)notifTitle
              notifImg:(NSString*)notifImg
              notifMsg:(NSString*)notifMsg
             notifType:(NSString*)notifType
             notifDate:(NSString*)notifDate
             notifTime:(NSString*)notifTime
            notifState:(NSString*)notifState
            notifIsFav:(NSString*)notifIsFav
             serviceId:(NSString*)serviceId
      currentTimeMills:(NSString*)currentTimeMills
               subType:(NSString*)subType
                   url:(NSString*)url
            screenName:(NSString*)screenName
       receiveDateTime:(NSString*)receiveDateTime
             dialogMsg:(NSString*)dialogMsg
          webpageTitle:(NSString*)webpageTitle
           servicename:(NSString*)servicename
              deptname:(NSString*)deptname
               user_id:(NSString*)user_id
{
    
    if (servicename.length==0|| servicename==nil ||[servicename isEqualToString:@"(null)"])
    {
        servicename=@"";
    }
    
    if (deptname.length==0|| deptname==nil ||[deptname isEqualToString:@"(null)"])
    {
        deptname=@"";
    }
    
    // Prepare the query string.
    //    NSString *query;
    //    query = [NSString stringWithFormat:@"insert into TABLE_NOTIFICATIONS values(null, '%@', '%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')", notifId,notifTitle,notifImg, notifMsg,notifType,notifDate,notifTime,notifState,notifIsFav,serviceId,currentTimeMills,subType,url,screenName, receiveDateTime,dialogMsg,webpageTitle,servicename,deptname,user_id];
    
    
    NSString *query;
    query = [NSString stringWithFormat:@"insert into TABLE_NOTIFICATIONS values(null, '%@', '%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')", notifId,notifTitle,notifImg, notifMsg,notifType,notifDate,notifTime,notifState,notifIsFav,serviceId,currentTimeMills,subType,url,screenName, receiveDateTime,dialogMsg,webpageTitle,user_id,servicename,deptname];
    
    if ([UMSqliteManager executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
        
    }
    
    
}


//----------------------------------------------------------
//          insertServiceSections TABLE_SERVICE_SECTIONS
//----------------------------------------------------------

-(void)insertServiceSections:(NSString*)sectionName
                  sectionImg:(NSString*)sectionImg
             sectionServices:(NSString*)sectionServices
{
    @try {
        if(sectionName.length>0)
        {
            sectionName = [NSString stringWithFormat:@"%@%@",[[sectionName substringToIndex:1] uppercaseString],[sectionName substringFromIndex:1] ];
        }
        
        
        // Prepare the query string.
        NSString *query;
        query = [NSString stringWithFormat:@"insert into TABLE_SERVICE_SECTIONS values(null, '%@', '%@','%@')", sectionName,sectionImg,sectionServices];
        if ([UMSqliteManager executeScalarQuery:query]==YES)
        {
            
            NSLog(@"insertServiceSections Data  inserted successfully");
            
        }else{
            NSLog(@"insertServiceSections Data not inserted successfully");
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
}



//----------------------------------------------------------
//          updateNotifIsFav TABLE_NOTIFICATIONS
//----------------------------------------------------------


//-(void)updateNotifIsFav:(NSString*)notifId notifIsFav:(NSString*)notifIsFav
-(void)updateNotifIsFav:(NSString*)notifId notifIsFav:(NSString*)notifIsFav withUser_id:(NSString*)user_id
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"update TABLE_NOTIFICATIONS set NOTIF_IS_FAV='%@' where NOTIF_ID='%@' AND USER_ID='%@'", notifIsFav, notifId,user_id];
    if ([UMSqliteManager executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
}






//query = [NSString stringWithFormat:@"update peopleInfo set firstname='%@', lastname='%@', age=%d where peopleInfoID=%d", self.txtFirstname.text, self.txtLastname.text, self.txtAge.text.intValue, self.recordIDToEdit];









//----------------------------------------------------------
//          getServiceLanguage TABLE_SERVICES_DATA
//----------------------------------------------------------

//13

-(BOOL)getServiceLanguage:(NSString*)serviceId withDeviceLang:(NSString*)langDevice
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"SELECT SERVICE_LANG FROM TABLE_SERVICES_DATA where SERVICE_ID='%@'", serviceId];
    NSArray *resultarry=[UMSqliteManager executeQuery:query];
    NSString *langString=[NSString stringWithFormat:@"%@",[[resultarry objectAtIndex:0]valueForKey:@"SERVICE_LANG"]];
    NSArray *arry = [langString componentsSeparatedByString:@","];
    
    BOOL langContain;
    
    // NSLog(@"langdevice=%@",langDevice);
    if ([arry containsObject:langDevice]) {
        // do something
        langContain=TRUE;
        //NSLog(@"contain");
        
    }
    else
    {
        langContain =FALSE;
        // NSLog(@"not contain");
        
    }
    
    return langContain;
}


//----------------------------------------------------------
//          getServiceData TABLE_SERVICES_DATA
//----------------------------------------------------------



-(NSArray*)getServiceData:(NSString*)serviceId
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"SELECT * FROM TABLE_SERVICES_DATA where SERVICE_ID='%@'", serviceId];
    NSArray *arry=[UMSqliteManager executeQuery:query];
    
    return arry;
}



//----------------------------------------------------------
//          getServiceFavStatis TABLE_SERVICES_DATA
//----------------------------------------------------------

-(NSString*)getServiceFavStatus:(NSString*)serviceId
{
    NSString *query;
    query = [NSString stringWithFormat:@"SELECT SERVICE_IS_FAV FROM TABLE_SERVICES_DATA where SERVICE_ID='%@'", serviceId];
    NSArray *arry=[UMSqliteManager executeQuery:query];
    NSLog(@"kill before");
    
    @try {
        if ([arry count]>0)
        {
            return [[arry objectAtIndex:0]valueForKey:@"SERVICE_IS_FAV"];
            
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    NSLog(@"kill after");
    
    return @"false";
    
    
}

//----------------------------------------------------------
//          updateServicePopularity TABLE_SERVICES_DATA
//----------------------------------------------------------


-(void)updateServiceMostPopular:(NSString*)serviceId withRating:(NSString*)MpRating

{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"update TABLE_SERVICES_DATA  set SERVICE_POPULARITY='%@' where SERVICE_ID='%@'",MpRating, serviceId];
    if ([UMSqliteManager executeScalarQuery:query]==YES)
    {
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
}


//----------------------------------------------------------
//          updateServiceIsFav TABLE_SERVICES_DATA
//----------------------------------------------------------


-(void)updateServiceIsFav:(NSString*)serviceId
             serviceIsFav:(NSString*)serviceIsFav hitAPI:(NSString*)hitStatus
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"update TABLE_SERVICES_DATA  set SERVICE_IS_FAV='%@' where SERVICE_ID='%@'",serviceIsFav, serviceId];
    if ([UMSqliteManager executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
    if ([hitStatus isEqualToString:@"Yes"])
    {
        
        dispatch_queue_t queue = dispatch_queue_create("com.yourdomain.yourappname", NULL);
        dispatch_async(queue, ^{
            NSLog(@"deleteBannerHomeData");
            
            //code to be executed in the background
            [self hitAPI:serviceId favUnfav:serviceIsFav];
            
            //            dispatch_async(dispatch_get_main_queue(), ^{
            //                //code to be executed on the main thread when background task is finished
            //
            //            });
        });
        
    }
}



-(void)showToast :(NSString *)strMessage
{
    [APP_DELEGATE.window makeToast:strMessage duration:1.0 position:CSToastPositionBottom];
}


//----- hitAPI for IVR OTP call Type registration ------
-(void)hitAPI:(NSString*)serviceID favUnfav:(NSString*)favStatus
{
    singleton = [SharedManager sharedSingleton];
    
    
    if ([favStatus isEqualToString:@"true"])
    {
        favStatus=@"y";
    }
    else
    {
        favStatus=@"n";
        
    }
    
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:serviceID forKey:@"sid"];//Enter mobile number of user
    
    [dictBody setObject:favStatus forKey:@"isfav"];//Enter mobile number of user
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_UNSET_FAVORITE withBody:dictBody andTag:TAG_REQUEST_UNSET_FAVORITE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n value of tkn=%@ ",rc,rs,tkn);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                
                
                @try {
                    NSArray *arr=[singleton.dbManager getServiceData:serviceID];
                    NSString *serviceName=@"";
                    if ([arr count]>0) {
                        serviceName=[NSString stringWithFormat:@"%@",[[arr objectAtIndex:0] valueForKey:@"SERVICE_NAME"]];
                        
                    }
                    
                    // NSString *correctString = [[NSString alloc] initWithUTF8String:serviceName];
                    
                    // SERVICE_NAME
                    if ([favStatus isEqualToString:@"y"]) {
                        
                        NSString *texttoshow=[NSString stringWithFormat:@"%@ %@",serviceName,NSLocalizedString(@"added_to_fav", nil)];
                        
                        [self showToast:texttoshow];
                        
                        
                    }
                    else
                        
                    {
                        NSString *texttoshow=[NSString stringWithFormat:@"%@ %@",serviceName,NSLocalizedString(@"removed_from_fav", nil)];
                        
                        [self showToast:texttoshow];
                        
                        
                    }
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                //[self checkstatusFavouriteSuccessFail:serviceID];
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            /*  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
             message:error.localizedDescription
             delegate:self
             cancelButtonTitle:@"OK"
             otherButtonTitles:nil];
             [alert show];*/
            [self checkstatusFavouriteSuccessFail:serviceID];
            
            
        }
        
    }];
    
    
    
}

//---- Handle condition for if Service is fail to update -----------
-(void)checkstatusFavouriteSuccessFail:(NSString*)serviceId
{
    NSString *serviceFav=[self getServiceFavStatus:serviceId];//get current status and reverse it
    
    if ([serviceFav isEqualToString:@"true"])// Is selected?
    {
        [self updateServiceIsFav:serviceId serviceIsFav:@"false" hitAPI:@"No"];
    }
    else
    {
        [self updateServiceIsFav:serviceId serviceIsFav:@"true" hitAPI:@"No"];
    }
    [self startCollectionNotifier];
    
}

- (void) startCollectionNotifier
{
    // All instances of TestClass will be notified
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADCOLLECTIONVIEW" object:nil];
    
    
    
}
//----------------------------------------------------------
//          updateServiceIsNotifEnabled TABLE_SERVICES_DATA
//----------------------------------------------------------


-(void)updateServiceIsNotifEnabled:(NSString*)serviceId
                    notifIsEnabled:(id)notifIsEnabled
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"update TABLE_SERVICES_DATA  set SERVICE_IS_NOTIF_ENABLED='%@' where SERVICE_ID='%@'",notifIsEnabled, serviceId];
    if ([UMSqliteManager executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
}





//----------------------------------------------------------
//          deleteNotification TABLE_NOTIFICATIONS
//----------------------------------------------------------

//-(void)deleteNotification:(NSString*)notifId
-(void)deleteNotification:(NSString*)notifId withUser_id:(NSString*)user_id
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"DELETE FROM TABLE_NOTIFICATIONS where NOTIF_ID='%@' AND USER_ID='%@'", notifId,user_id];
    // Execute the query.
    if ([UMSqliteManager executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
}


//----------------------------------------------------------
//          deleteAllNotifications TABLE_NOTIFICATIONS
//----------------------------------------------------------


//-(void)deleteAllNotifications
-(void)deleteAllNotifications:(NSString*)user_id
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"DELETE FROM TABLE_NOTIFICATIONS where USER_ID='%@'",user_id];
    if ([UMSqliteManager executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
}

//----------------------------------------------------------
//          deleteServiceData TABLE_SERVICES_DATA
//----------------------------------------------------------


-(void)deleteServiceData:(NSString*)serviceId
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"DELETE FROM TABLE_SERVICES_DATA where SERVICE_ID='%@'", serviceId];
    if ([UMSqliteManager executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
}

//----------------------------------------------------------
//          deleteBannerHomeData TABLE_BANNER_HOME
//----------------------------------------------------------


-(void)deleteBannerHomeData
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"DELETE FROM TABLE_BANNER_HOME"];
    if ([UMSqliteManager executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  Delete successfully");
        
    }else{
        NSLog(@"Data not Deleted ");
    }
    
}

//----------------------------------------------------------
//          deleteBannerStateData TABLE_BANNER_HOME
//----------------------------------------------------------


-(void)deleteBannerStateData
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"DELETE FROM TABLE_BANNER_STATE"];
    if ([UMSqliteManager executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
}








//----------------------------------------------------------
//          deleteAllServices TABLE_SERVICES_DATA
//----------------------------------------------------------


-(void)deleteAllServices
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"DELETE FROM TABLE_SERVICES_DATA"];
    if ([UMSqliteManager executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
}




//----------------------------------------------------------
//          deleteSectionData TABLE_SERVICE_SECTIONS
//----------------------------------------------------------


-(void)deleteSectionData
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"DELETE FROM TABLE_SERVICE_SECTIONS"];
    if ([UMSqliteManager executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
}

//----------------------------------------------------------
//          Create UMANG DATABASE TABLES
//----------------------------------------------------------



-(void)createUmangDB
{
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"UMANG_DATABASE.db"]];
    
    
    NSLog(@"databasePath=%@",databasePath);
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        const char *dbpath = [databasePath UTF8String];
        // int version = db.getVersion();
        
        
        if (sqlite3_open(dbpath, &umangDB) == SQLITE_OK)
        {
            char *errMsg;
            //close to create accroding to user id
            /* const char *sql_stmt = "CREATE TABLE IF NOT EXISTS TABLE_NOTIFICATIONS(ID INTEGER PRIMARY KEY AUTOINCREMENT,NOTIF_ID TEXT,NOTIF_TITLE TEXT,NOTIF_IMG TEXT,NOTIF_MSG TEXT,NOTIF_TYPE TEXT,NOTIF_DATE TEXT,NOTIF_TIME TEXT,NOTIF_STATE TEXT,NOTIF_IS_FAV TEXT,SERVICE_ID TEXT,CURRENT_TIME_MILLS TEXT,NOTIF_SUB_TYPE TEXT,NOTIF_URL TEXT,NOTIF_SCREEN_NAME TEXT,NOTIF_RECEIVE_DATE_TIME TEXT,NOTIF_DIALOG_MSG TEXT,NOTIF_WEBPAGE_TITLE TEXT);"*/
            //TABLE_NOTIFICATIONS
            const char *sql_stmt = "CREATE TABLE IF NOT EXISTS TABLE_NOTIFICATIONS(ID INTEGER PRIMARY KEY AUTOINCREMENT,NOTIF_ID TEXT,NOTIF_TITLE TEXT,NOTIF_IMG TEXT,NOTIF_MSG TEXT,NOTIF_TYPE TEXT,NOTIF_DATE TEXT,NOTIF_TIME TEXT,NOTIF_STATE TEXT,NOTIF_IS_FAV TEXT,SERVICE_ID TEXT,CURRENT_TIME_MILLS TEXT,NOTIF_SUB_TYPE TEXT,NOTIF_URL TEXT,NOTIF_SCREEN_NAME TEXT,NOTIF_RECEIVE_DATE_TIME TEXT,NOTIF_DIALOG_MSG TEXT,NOTIF_WEBPAGE_TITLE TEXT,USER_ID TEXT);"//TABLE_NOTIFICATIONS
            
            
            
            
            /*  "CREATE TABLE IF NOT EXISTS TABLE_SERVICES_DATA(ID INTEGER PRIMARY KEY AUTOINCREMENT,SERVICE_ID TEXT,SERVICE_NAME TEXT,SERVICE_DESC TEXT,SERVICE_IMAGE TEXT,SERVICE_CATEGORY TEXT,SERVICE_SUB_CATEGORY TEXT,SERVICE_RATING TEXT,SERVICE_URL TEXT,SERVICE_STATE TEXT,SERVICE_LATITUDE TEXT,SERVICE_LONGITUDE TEXT,SERVICE_IS_FAV TEXT,SERVICE_IS_HIDDEN TEXT,SERVICE_PHONE_NUMBER TEXT,SERVICE_IS_NOTIF_ENABLED TEXT,SERVICE_WEBSITE TEXT, SERVICE_DEPTADDRESS TEXT,SERVICE_WORKINGHOURS TEXT,SERVICE_DEPTDESCRIPTION TEXT,SERVICE_LANG  TEXT,SERVICE_EMAIL  TEXT,SERVICE_POPULARITY TEXT,SERVICE_CATEGORY_ID TEXT);"//TABLE_SERVICES_DATA
             */
            
            "CREATE TABLE IF NOT EXISTS TABLE_SERVICES_DATA(ID INTEGER PRIMARY KEY AUTOINCREMENT,SERVICE_ID TEXT,SERVICE_NAME TEXT,SERVICE_DESC TEXT,SERVICE_IMAGE TEXT,SERVICE_CATEGORY TEXT,SERVICE_SUB_CATEGORY TEXT,SERVICE_RATING TEXT,SERVICE_URL TEXT,SERVICE_STATE TEXT,SERVICE_LATITUDE TEXT,SERVICE_LONGITUDE TEXT,SERVICE_IS_FAV TEXT,SERVICE_IS_HIDDEN TEXT,SERVICE_PHONE_NUMBER TEXT,SERVICE_IS_NOTIF_ENABLED TEXT,SERVICE_WEBSITE TEXT, SERVICE_DEPTADDRESS TEXT,SERVICE_WORKINGHOURS TEXT,SERVICE_DEPTDESCRIPTION TEXT,SERVICE_LANG  TEXT,SERVICE_EMAIL  TEXT,SERVICE_POPULARITY TEXT,SERVICE_CATEGORY_ID TEXT,SERVICE_OTHER_STATE TEXT);"//TABLE_SERVICES_DATA
            
            "CREATE TABLE IF NOT EXISTS TABLE_SERVICE_SECTIONS(ID INTEGER PRIMARY KEY AUTOINCREMENT,SECTION_NAME TEXT,SECTION_IMAGE TEXT,SECTION_SERVICES TEXT);"//TABLE_SERVICE_SECTIONS
            
            "CREATE TABLE IF NOT EXISTS TABLE_BANNER_HOME (ID INTEGER PRIMARY KEY AUTOINCREMENT,BANNER_IMAGE_URL TEXT,BANNER_ACTION_TYPE TEXT,BANNER_ACTION_URL TEXT,BANNER_DESC TEXT);"//TABLE_BANNER_HOME
            
            
            
            "CREATE TABLE IF NOT EXISTS TABLE_BANNER_STATE(ID INTEGER PRIMARY KEY AUTOINCREMENT,BANNER_IMAGE_URL TEXT,BANNER_ACTION_TYPE TEXT,BANNER_ACTION_URL TEXT,BANNER_DESC TEXT);"//TABLE_BANNER_STATE
            
            
            "CREATE TABLE IF NOT EXISTS TABLE_SERVICES_DIRECTORY(ID INTEGER PRIMARY KEY AUTOINCREMENT,SERVICE_ID TEXT,SERVICE_NAME TEXT,SERVICE_DESC TEXT,SERVICE_IMAGE TEXT,SERVICE_LATITUDE TEXT, SERVICE_LONGITUDE TEXT,SERVICE_PHONE_NUMBER TEXT,SERVICE_WEBSITE TEXT, SERVICE_EMAIL TEXT,SERVICE_ADDRESS TEXT,SERVICE_WORKING_HOURS TEXT,SERVICE_OTHER_INFO TEXT,SERVICE_NATIVE_APP TEXT,SERVICE_IS_AVAILABLE TEXT,SERVICE_NATIVE_APPNAME TEXT);"//service directory
            
            
            
            "CREATE TABLE IF NOT EXISTS TABLE_BOOKS_DATA(ID INTEGER PRIMARY KEY AUTOINCREMENT,USER_ID TEXT,BOOK_ID TEXT,BOOK_CLASS TEXT,BOOK_IMAGE TEXT,BOOK_LANGUAGE TEXT ,BOOK_NAME TEXT ,BOOK_SUBJECT TEXT,CHAPTER_ID TEXT,CHAPTER_CLASS_BOOK TEXT,CHAPTER_EPUB_LINK TEXT,CHAPTER_TITLE TEXT,CHAPTER_NO TEXT,CHAPTER_ENM_LAYOUT TEXT,CHAPTER_ALL_DATA TEXT,CHAPTER_ENM_TYPE TEXT,CHAPTER_HASH_KEY TEXT,CHAPTER_PATH TEXT)";//TABLE_BOOKS_DATA
            
            
            
            
            
            
            
            if (sqlite3_exec(umangDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                NSLog(@"Failed to create tables");
            }
            sqlite3_close(umangDB);
            NSLog(@"DataBase created at: %@",databasePath);
        } else {
            NSLog(@"Failed to open/create database");
        }
    }
    
    
}


-(NSArray*)loadDataServiceSection
{
    // Prepare the query string.
    NSString *query;
    // query = [NSString stringWithFormat:@"select * from TABLE_SERVICE_SECTIONS"];
    
    
    query = [NSString stringWithFormat:@"select DISTINCT SECTION_NAME,SECTION_IMAGE,SECTION_SERVICES from TABLE_SERVICE_SECTIONS"];
    //   query = [NSString stringWithFormat:@"SELECT DISTINCT SERVICE_CATEGORY from TABLE_SERVICES_DATA ORDER BY SERVICE_CATEGORY ASC"];
    
    NSArray *arry=[UMSqliteManager executeQuery:query];
    
    return arry;
}



-(NSArray*)loadDataServiceData
{
    // Prepare the query string.
    NSString *query;
    // query = [NSString stringWithFormat:@"select * from TABLE_SERVICES_DATA"];
    
    /*  query = [NSString stringWithFormat:@"SELECT DISTINCT SERVICE_ID ,SERVICE_NAME ,SERVICE_DESC ,SERVICE_IMAGE ,SERVICE_CATEGORY , SERVICE_SUB_CATEGORY ,SERVICE_RATING ,SERVICE_URL ,SERVICE_STATE , SERVICE_LATITUDE ,SERVICE_LONGITUDE ,SERVICE_IS_FAV ,SERVICE_IS_HIDDEN ,SERVICE_PHONE_NUMBER ,SERVICE_IS_NOTIF_ENABLED ,SERVICE_WEBSITE,SERVICE_DEPTADDRESS , SERVICE_WORKINGHOURS, SERVICE_DEPTDESCRIPTION, SERVICE_LANG , SERVICE_EMAIL ,SERVICE_POPULARITY,SERVICE_CATEGORY_ID from TABLE_SERVICES_DATA ORDER BY SERVICE_NAME ASC"];
     */
    
    /*  query = [NSString stringWithFormat:@"SELECT DISTINCT (SERVICE_ID),SERVICE_NAME ,SERVICE_DESC ,SERVICE_IMAGE ,SERVICE_CATEGORY , SERVICE_SUB_CATEGORY ,SERVICE_RATING ,SERVICE_URL ,SERVICE_STATE , SERVICE_LATITUDE ,SERVICE_LONGITUDE ,SERVICE_IS_FAV ,SERVICE_IS_HIDDEN ,SERVICE_PHONE_NUMBER ,SERVICE_IS_NOTIF_ENABLED ,SERVICE_WEBSITE,SERVICE_DEPTADDRESS , SERVICE_WORKINGHOURS, SERVICE_DEPTDESCRIPTION, SERVICE_LANG , SERVICE_EMAIL ,SERVICE_POPULARITY,SERVICE_CATEGORY_ID from TABLE_SERVICES_DATA ORDER BY SERVICE_NAME ASC"];
     
     */
    
    query = [NSString stringWithFormat:@"SELECT * FROM TABLE_SERVICES_DATA GROUP BY SERVICE_ID ORDER BY SERVICE_NAME ASC"];
    
    
    
    
    NSArray *arry=[UMSqliteManager executeQuery:query];
    
    return arry;
}



-(NSArray*)loadServiceCategory
{
    // Prepare the query string.
    NSString *query;
    
    query = [NSString stringWithFormat:@"SELECT DISTINCT SERVICE_CATEGORY from TABLE_SERVICES_DATA ORDER BY SERVICE_CATEGORY ASC"];
    
    
    
    NSArray *arry=[UMSqliteManager executeQuery:query];
    
    return arry;
}




-(NSString*)getServiceId:(NSString*)serviceName
{
    
    // Prepare the query string.
    NSString *query;
    
    query = [NSString stringWithFormat:@"SELECT SERVICE_ID FROM TABLE_SERVICES_DATA where SERVICE_NAME='%@'",serviceName];
    
    // query = [NSString stringWithFormat:@"SELECT DISTINCT SERVICE_CATEGORY from TABLE_SERVICES_DATA"];
    NSArray *arry=[UMSqliteManager executeQuery:query];
    NSString*string=@"";
    if ([arry count]>0) {
        string=[[arry objectAtIndex:0]valueForKey:@"SERVICE_ID"];
    }
    return string;
}

/*
 
 query = [NSString stringWithFormat:@"update TABLE_SERVICES_DATA  set SERVICE_NAME='%@', SERVICE_DESC='%@', SERVICE_IMAGE='%@', SERVICE_CATEGORY='%@', SERVICE_SUB_CATEGORY='%@', SERVICE_RATING='%@', SERVICE_URL='%@', SERVICE_STATE='%@', SERVICE_LATITUDE='%@', SERVICE_LONGITUDE='%@', SERVICE_IS_FAV='%@', SERVICE_IS_HIDDEN='%@', SERVICE_PHONE_NUMBER='%@',SERVICE_IS_NOTIF_ENABLED='%@',
 */
-(NSString*)getServiceCategoryId:(NSString*)catName
{
    // Prepare the query string.
    NSString *query;
    
    query = [NSString stringWithFormat:@"SELECT SERVICE_CATEGORY_ID FROM TABLE_SERVICES_DATA where SERVICE_CATEGORY='%@'",catName];
    
    // query = [NSString stringWithFormat:@"SELECT DISTINCT SERVICE_CATEGORY from TABLE_SERVICES_DATA"];
    NSArray *arry=[UMSqliteManager executeQuery:query];
    NSString*string=@"";
    if ([arry count]>0) {
        string=[[arry objectAtIndex:0]valueForKey:@"SERVICE_CATEGORY_ID"];
    }
    return string;
}

//-----------------------------------------------------------------------------------
//------------- Method to get Service URL from  ServiceID----------------------------
//-----------------------------------------------------------------------------------
-(NSString*)getServiceURL:(NSString*) serviceId
{
    
    NSString *query;
    query = [NSString stringWithFormat:@"SELECT SERVICE_URL FROM TABLE_SERVICES_DATA where SERVICE_ID='%@'", serviceId];
    NSArray *arry=[UMSqliteManager executeQuery:query];
    NSString*string=@"";
    if ([arry count]>0) {
        string=[[arry objectAtIndex:0]valueForKey:@"SERVICE_URL"];
    }
    return string;
}



/*
 synchronized public void insertBooksData(
 '%@','%@', '%@','%@', '%@', '%@','%@','%@','%@','%@','%@','%@', '%@', '%@','%@', '%@','%@'
 */

//-----------------------------------------------------------------------------------
//----------------------------- ePub Book handing Code-------------------------------
//-----------------------------------------------------------------------------------
/*-(void)insertBooksData:(NSString*) userId
 bookId:(NSString*) bookId
 bookClass:(NSString*) bookClass
 bookImage:(NSString*) bookImage
 bookLang:(NSString*) bookLang
 bookName:(NSString*) bookName
 bookSub:(NSString*) bookSub
 cId:(NSString*) cId
 cClassBook:(NSString*) cClassBook
 cEpubLink:(NSString*) cEpubLink
 cTitle:(NSString*) cTitle
 cNo:(NSString*) cNo
 cEnmLay:(NSString*) cEnmLay
 cAllData:(NSString*) cAllData
 cEnmType:(NSString*) cEnmType
 cHashKey:(NSString*) cHashKey
 cPath:(NSString*) cPath
 
 {
 
 
 // Prepare the query string.
 NSString *query;
 query = [NSString stringWithFormat:@"insert into TABLE_BOOKS_DATA values(null, '%@','%@', '%@','%@', '%@', '%@','%@','%@','%@','%@','%@','%@', '%@', '%@','%@', '%@','%@')",userId,bookId, bookClass,bookImage, bookLang, bookName,bookSub,cId,cClassBook,cEpubLink,cTitle, cNo, cEnmLay, cAllData,cEnmType, cHashKey,cPath];
 
 if ([UMSqliteManager executeScalarQuery:query]==YES)
 {
 
 NSLog(@"Data  inserted successfully");
 
 }else{
 NSLog(@"Data not inserted successfully");
 }
 
 
 
 }
 */

/*
 -(void)insertBooksData:(NSString*) userId
 bookId:(NSString*) bookId
 bookClass:(NSString*) bookClass
 bookImage:(NSString*) bookImage
 bookLang:(NSString*) bookLang
 bookName:(NSString*) bookName
 bookSub:(NSString*) bookSub
 cId:(NSString*) cId
 cClassBook:(NSString*) cClassBook
 cEpubLink:(NSString*) cEpubLink
 cTitle:(NSString*) cTitle
 cNo:(NSString*) cNo
 cEnmLay:(NSString*) cEnmLay
 cAllData:(NSString*) cAllData
 cEnmType:(NSString*) cEnmType
 cHashKey:(NSString*) cHashKey
 cPath:(NSString*) cPath
 cbookCategory:(NSString*)cbookCategory
 
 
 {
 
 
 // Prepare the query string.
 NSString *query;
 query = [NSString stringWithFormat:@"insert into TABLE_BOOKS_DATA values(null, '%@','%@', '%@','%@', '%@', '%@','%@','%@','%@','%@','%@','%@', '%@', '%@','%@', '%@','%@','%@')",userId,bookId, bookClass,bookImage, bookLang, bookName,bookSub,cId,cClassBook,cEpubLink,cTitle, cNo, cEnmLay, cAllData,cEnmType, cHashKey,cPath, cbookCategory];
 
 if ([UMSqliteManager executeScalarQuery:query]==YES)
 {
 
 NSLog(@"Data  inserted successfully");
 
 }else{
 NSLog(@"Data not inserted successfully");
 }
 
 
 
 }
 */

-(void)insertBooksData:(NSString*) userId
                bookId:(NSString*) bookId
             bookClass:(NSString*) bookClass
             bookImage:(NSString*) bookImage
              bookLang:(NSString*) bookLang
              bookName:(NSString*) bookName
               bookSub:(NSString*) bookSub
                   cId:(NSString*) cId
            cClassBook:(NSString*) cClassBook
             cEpubLink:(NSString*) cEpubLink
                cTitle:(NSString*) cTitle
                   cNo:(NSString*) cNo
               cEnmLay:(NSString*) cEnmLay
              cAllData:(NSString*) cAllData
              cEnmType:(NSString*) cEnmType
              cHashKey:(NSString*) cHashKey
                 cPath:(NSString*) cPath
         cbookCategory:(NSString*)cbookCategory
              category:(NSString*)Category


{
    
    
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"insert into TABLE_BOOKS_DATA values(null, '%@','%@', '%@','%@', '%@', '%@','%@','%@','%@','%@','%@','%@', '%@', '%@','%@', '%@','%@','%@','%@')",userId,bookId, bookClass,bookImage, bookLang, bookName,bookSub,cId,cClassBook,cEpubLink,cTitle, cNo, cEnmLay, cAllData,cEnmType, cHashKey,cPath, cbookCategory,Category];
    
    if ([UMSqliteManager executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
    
    
}




//----------------------------------------------------------
//          deleteChapter TABLE_BOOKS_DATA
//----------------------------------------------------------


-(NSString*)deleteChapter:(NSString*)userId withChapterid:(NSString*)chapterId
{
    
    NSString *delStatus=@"FAIL";
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"DELETE FROM TABLE_BOOKS_DATA where USER_ID='%@' AND CHAPTER_ID ='%@' ", userId,chapterId];
    if ([UMSqliteManager executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        delStatus=@"SUCCESS";
    }else{
        NSLog(@"Data not inserted successfully");
        delStatus=@"FAIL";
        
    }
    return  delStatus;
    
}


//----------------------------------------------------------
//          getAllBooksData TABLE_BOOKS_DATA  [unused]
//----------------------------------------------------------

-(NSArray*)getAllBooksData:(NSString*)userId
{
    
    
    
    
    
    NSString *query;
    // query = [NSString stringWithFormat:@"SELECT * FROM TABLE_BOOKS_DATA where USER_ID='%@'", userId];
    
    query = [NSString stringWithFormat:@"SELECT * from TABLE_BOOKS_DATA where USER_ID='%@' group by BOOK_ID", userId];
    
    
    
    NSArray *arry=[UMSqliteManager executeQuery:query];
    
    return arry;
}


//----------------------------------------------------------
//          getUniqueBooksData TABLE_BOOKS_DATA
//----------------------------------------------------------

-(NSArray*)getUniqueBooksData:(NSString*)userId withBookid:(NSString*)bookId
{
    NSString *query;
    
    /*"CREATE TABLE IF NOT EXISTS TABLE_BOOKS_DATA(ID INTEGER PRIMARY KEY AUTOINCREMENT,USER_ID
     
     //----------
     //----Get all book data----
     BOOK_CLASS
     BOOK_ID
     BOOK_IMAGE
     BOOK_LANGUAGE
     BOOK_NAME
     BOOK_SUBJECT
     
     //--------------
     
     
     TEXT,BOOK_ID TEXT,BOOK_CLASS TEXT,BOOK_IMAGE TEXT,BOOK_LANGUAGE TEXT ,BOOK_NAME TEXT ,BOOK_SUBJECT TEXT,CHAPTER_ID TEXT,CHAPTER_CLASS_BOOK TEXT,CHAPTER_EPUB_LINK TEXT,CHAPTER_TITLE TEXT,CHAPTER_NO TEXT,CHAPTER_ENM_LAYOUT TEXT,CHAPTER_ALL_DATA TEXT,CHAPTER_ENM_TYPE TEXT,CHAPTER_HASH_KEY TEXT,CHAPTER_PATH TEXT)";//TABLE_BOOKS_DATA
     
     NSString*query = [NSString stringWithFormat:@"SELECT theDate, customer,code1,code2 FROM summary WHERE key=\"%@\"", customerName];
     
     */
    
    
    query = [NSString stringWithFormat:@"SELECT * FROM TABLE_BOOKS_DATA where USER_ID='%@' AND BOOK_ID='%@'", userId,bookId];
    NSArray *arry=[UMSqliteManager executeQuery:query];
    
    return arry;
}



//----------------------------------------------------------
//          getChaptersDataFromBookId TABLE_BOOKS_DATA
//----------------------------------------------------------

-(NSArray*)getChaptersDataFromBookId:(NSString*)userId withBookId:(NSString*)book_id
{
    NSString *query;
    query = [NSString stringWithFormat:@"SELECT * FROM TABLE_BOOKS_DATA where USER_ID='%@' AND BOOK_ID='%@'", userId,book_id];
    NSArray *arry=[UMSqliteManager executeQuery:query];
    
    return arry;
}

//----------------------------------------------------------
//          getChapterDetailFromChapterId TABLE_BOOKS_DATA
//----------------------------------------------------------

-(NSArray*)getChapterDetailFromChapterId:(NSString*)userId withchapterId:(NSString*)chapterId
{
    NSString *query;
    query = [NSString stringWithFormat:@"SELECT * FROM TABLE_BOOKS_DATA where USER_ID='%@' AND CHAPTER_ID='%@'", userId,chapterId];
    NSArray *arry=[UMSqliteManager executeQuery:query];
    
    return arry;
}

//----------------------------------------------------------
//          getChaptersIdFromBookId TABLE_BOOKS_DATA
//----------------------------------------------------------

-(NSArray*)getChaptersIdFromBookId:(NSString*)userId withbookId:(NSString*)bookId
{
    NSString *query;
    query = [NSString stringWithFormat:@"SELECT CHAPTER_ID FROM TABLE_BOOKS_DATA where USER_ID='%@' AND BOOK_ID='%@'", userId,bookId];
    NSArray *arry=[UMSqliteManager executeQuery:query];
    
    return arry;
}
















//----------------------------------------------------------
//          insertBannerHomeData TABLE_BANNER_HOME
//----------------------------------------------------------


-(void)insertBannerHomeData:(NSString*)bannerImgUrl
           bannerActionType:(NSString*)bannerActionType
            bannerActionUrl:(NSString*)bannerActionUrl
                 bannerDesc:(NSString*)bannerDesc

{
    
    
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"insert into TABLE_BANNER_HOME values(null, '%@', '%@','%@','%@')", bannerImgUrl,bannerActionType,bannerActionUrl, bannerDesc];
    
    if ([UMSqliteManager executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
    
    
}

//----------------------------------------------------------
//          insertBannerStateData TABLE_BANNER_STATE
//----------------------------------------------------------


-(void)insertBannerStateData:(NSString*)bannerImgUrl
            bannerActionType:(NSString*)bannerActionType
             bannerActionUrl:(NSString*)bannerActionUrl
                  bannerDesc:(NSString*)bannerDesc

{
    
    
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"insert into TABLE_BANNER_STATE values(null, '%@', '%@','%@','%@')", bannerImgUrl,bannerActionType,bannerActionUrl, bannerDesc];
    
    if ([UMSqliteManager executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
    
    
}



-(NSArray*)getBannerStateData
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"select * from TABLE_BANNER_STATE"];
    NSArray *arry=[UMSqliteManager executeQuery:query];
    
    return arry;
}


-(NSArray*)getBannerHomeData
{
    // Prepare the query string.
    NSString *query;
    // query = [NSString stringWithFormat:@"select * from TABLE_BANNER_HOME"];
    
    
    query = [NSString stringWithFormat:@"select DISTINCT   BANNER_IMAGE_URL,BANNER_ACTION_TYPE,BANNER_ACTION_URL,BANNER_DESC from TABLE_BANNER_HOME"];
    
    NSArray *arry=[UMSqliteManager executeQuery:query];
    
    return arry;
}











-(NSArray*)getServicesDataForNotifSettings
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"select * from TABLE_SERVICES_DATA"];
    NSArray *arry=[UMSqliteManager executeQuery:query];
    
    return arry;
}







-(NSArray*)getTrendingServiceData
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"select * from TABLE_SERVICE_SECTIONS where  SECTION_NAME = 'Trending'"];
    NSArray *arry=[UMSqliteManager executeQuery:query];
    ///need below implement
    return arry;
}


//-(NSArray*)getNotifDatafavourite
-(NSArray*)getNotifDatafavourite:(NSString*)user_id

{
    NSString *query;
    NSString *finalQuery;
    NSString *notifTypeQuery;
    query = [NSString stringWithFormat:@"select * from TABLE_NOTIFICATIONS where"];
    // notifTypeQuery=[NSString stringWithFormat:@"NOTIF_IS_FAV=%@",@"'true'"];
    notifTypeQuery=[NSString stringWithFormat:@"NOTIF_IS_FAV=%@ AND USER_ID='%@'",@"'true'",user_id];
    finalQuery =[NSString stringWithFormat:@"%@ %@",query,notifTypeQuery];
    NSArray *arry=[UMSqliteManager executeQuery:finalQuery];
    ///need below implement
    return arry;
    
}



-(NSArray*)getCentralServiceData

{
    NSString *query;
    query = [NSString stringWithFormat:@"select * from TABLE_SERVICES_DATA WHERE SERVICE_STATE IN ('99')  GROUP BY SERVICE_ID  ORDER BY SERVICE_NAME ASC"];
    
    
    NSArray *arry=[UMSqliteManager executeQuery:query];
    return arry;
    
}



-(NSArray*)getAllServiceDataNotCentral
{
    NSString *query;
    query = [NSString stringWithFormat:@"select * from TABLE_SERVICES_DATA WHERE SERVICE_STATE NOT IN ('99') OR SERVICE_OTHER_STATE !='%@' GROUP BY SERVICE_ID  ORDER BY SERVICE_NAME ASC",@""];
    
    NSArray *arry=[UMSqliteManager executeQuery:query];
    return arry;
    
}

//-(NSArray*)getNotifDatapromo
-(NSArray*)getNotifDatapromo:(NSString*)user_id
{
    NSString *query;
    NSString *finalQuery;
    NSString *notifTypeQuery;
    query = [NSString stringWithFormat:@"select * from TABLE_NOTIFICATIONS where"];
    //notifTypeQuery=[NSString stringWithFormat:@"NOTIF_TYPE=%@",@"'promo'"];
    notifTypeQuery=[NSString stringWithFormat:@"NOTIF_TYPE=%@ AND USER_ID='%@'",@"'promo'",user_id];
    
    finalQuery =[NSString stringWithFormat:@"%@ %@",query,notifTypeQuery];
    NSArray *arry=[UMSqliteManager executeQuery:finalQuery];
    ///need below implement
    return arry;
    
}

//-(NSArray*)getNotifDatatrans
-(NSArray*)getNotifDatatrans:(NSString*)user_id
{
    NSString *query;
    NSString *finalQuery;
    NSString *notifTypeQuery;
    query = [NSString stringWithFormat:@"select * from TABLE_NOTIFICATIONS where"];
    //notifTypeQuery=[NSString stringWithFormat:@"NOTIF_TYPE=%@",@"'trans'"];
    notifTypeQuery=[NSString stringWithFormat:@"NOTIF_TYPE=%@ AND USER_ID='%@'",@"'trans'",user_id];
    finalQuery =[NSString stringWithFormat:@"%@ %@",query,notifTypeQuery];
    NSArray *arry=[UMSqliteManager executeQuery:finalQuery];
    ///need below implement
    return arry;
    
}



//-(NSArray*)getNotifData
-(NSArray*)getNotifData:(NSString*)user_id
{
    // Prepare the query string.
    NSString *query;
    //query = [NSString stringWithFormat:@"select * from TABLE_NOTIFICATIONS"];
    
    query = [NSString stringWithFormat:@"select * from TABLE_NOTIFICATIONS where USER_ID='%@' ORDER BY NOTIF_RECEIVE_DATE_TIME DESC",user_id];
    
    NSArray *arry=[UMSqliteManager executeQuery:query];
    
    return arry;
}

//sortby Alphabet/MostPopular/TopRated
//serviceType All/CentralGov/Regional
//stateIdAlist its a Comma seprated values eg uttarakhand,delhi,Punjab etc
//categoryList its a Comma seprated values eg Healthcare,Education,Agriculture etc

-(NSArray*)getFilteredServiceData:(NSString*)sortBy serviceType:(NSString*)serviceType stateIdAlist:(NSArray*)stateIdAlist categoryList:(NSArray*)categoryList
{
    NSString *query;
    NSString *lastquery;
    
    NSString *myQuery;
    NSString *statequery;
    NSString *categoryquery;
    
    // query = [NSString stringWithFormat:@"select * from TABLE_SERVICES_DATA  GROUP BY SERVICE_ID"];
    
    query = [NSString stringWithFormat:@"select * from TABLE_SERVICES_DATA"];
    
    /*
     
     select_your_language" =  "अपनी भाषा का चयन करें";
     "detected" =  "(पता चला)";
     "more_services" =  "अधिक सेवा";
     "sort_by" =  "इसके अनुसार क्रमबद्ध करें";
     "alphabetic" =  "वर्णानुक्रमक";
     "rating" =  "रेटिंग";
     "top_rated" =  "टॉप रेटेड";
     
     */
    // if ([sortBy isEqualToString:@"Alphabetic"]) {
    
    if ([sortBy isEqualToString:NSLocalizedString(@"alphabetic", nil)]) {
        //lastquery=[NSString stringWithFormat:@" ORDER BY SERVICE_NAME ASC"];
        
        lastquery=[NSString stringWithFormat:@" GROUP BY SERVICE_ID ORDER BY SERVICE_NAME ASC"];
        
        
        
        
        NSLog(@"Alphabetic Sort query=%@",lastquery);
        
    }
    // if ([sortBy isEqualToString:@"MostPopular"])//do nothing as nothing info provided
    if ([sortBy isEqualToString:NSLocalizedString(@"most_popular", nil)])//do nothing as nothing info provided
        
    {
        //lastquery=[NSString stringWithFormat:@" ORDER BY SERVICE_POPULARITY DESC"];
        lastquery=[NSString stringWithFormat:@" GROUP BY SERVICE_ID ORDER BY SERVICE_POPULARITY DESC"];
        
        
        NSLog(@"MostPopular query=%@",lastquery);
        
    }
    
    //if ([sortBy isEqualToString:@"TopRated"])//Sort as per Rating
    if ([sortBy isEqualToString:NSLocalizedString(@"top_rated", nil)])//Sort as per Rating
        
    {
        //lastquery=[NSString stringWithFormat:@" ORDER BY SERVICE_RATING DESC"];
        lastquery=[NSString stringWithFormat:@" GROUP BY SERVICE_ID ORDER BY SERVICE_RATING DESC"];
        
        
        NSLog(@"TopRated query=%@",lastquery);
        
    }
    
    
    if ([serviceType isEqualToString:NSLocalizedString(@"all", nil)])//
    {
        //do nothing
    }
    
    if ([serviceType isEqualToString:NSLocalizedString(@"centralgovernment", nil)])//
    {
        
        
        
        
        
        statequery=[NSString stringWithFormat:@"WHERE SERVICE_STATE IN ('%@')",@"99"];
        
        //99 in case of central governemnt
        
        
        //For Central Government Services the state id will be 99 and currently there is no option for sorting by Most Popular.
        myQuery=[NSString stringWithFormat:@"%@ %@",query,statequery];
        
    }
    
    if ([serviceType isEqualToString:NSLocalizedString(@"regional", nil)])//
    {
        if ([stateIdAlist count]!=0) {
            
            
            // state_id= [obj getStateCode:str_state];
            
            
            
            NSMutableArray *arr_stateId=[[NSMutableArray alloc]init];
            
            for (int i=0; i<[stateIdAlist count]; i++)
            {
                
                NSString *stateName = [[stateIdAlist objectAtIndex:i] lowercaseString];
                
                obj=[[StateList alloc]init];
                
                NSString* state_id= [obj getStateCode:stateName];
                
                [arr_stateId addObject:state_id];
            }
            NSString *joinedComponents = [arr_stateId componentsJoinedByString:@"','"];
            
            
            NSString *stateComponents=[arr_stateId componentsJoinedByString:@","];
            
            NSString *joinedStateComponents = [stateComponents stringByReplacingOccurrencesOfString:@"," withString:@"|%' OR SERVICE_OTHER_STATE like '%|"];
            joinedStateComponents=[NSString stringWithFormat:@" OR  SERVICE_OTHER_STATE like '%%|%@|%%' ",joinedStateComponents];
            
            NSLog(@"joinedStateComponent After === > %@",joinedStateComponents);
            
            
            
            statequery=[NSString stringWithFormat:@"WHERE (SERVICE_STATE IN ('%@')%@)",joinedComponents,joinedStateComponents];
            
            
            
            // WHERE SERVICE_STATE IN ('9999') OR  SERVICE_OTHER_STATE like '%|9999|%'
            
            if ([statequery rangeOfString:@"9999"].location == NSNotFound) {
                NSLog(@"string does not contain 9999");
                ////// statequery=[NSString stringWithFormat:@"WHERE SERVICE_STATE IN ('%@')",joinedComponents];
                
                statequery=[NSString stringWithFormat:@"WHERE (SERVICE_STATE IN ('%@')%@)",joinedComponents,joinedStateComponents];
                
                
                
            } else {
                NSLog(@"string contains 9999!");
                ////// statequery=[NSString stringWithFormat:@"WHERE SERVICE_STATE NOT IN ('%@')",@"99"];
                //.....  statequery=[NSString stringWithFormat:@"WHERE (SERVICE_STATE NOT IN ('%@')%@)",@"99",joinedStateComponents];
                
                statequery=[NSString stringWithFormat:@"WHERE (SERVICE_STATE NOT IN ('%@')%@)",@"99",@"OR  SERVICE_OTHER_STATE like '%|%|%'"];
                
                
            }
            
            
            
            
            //NSString *getfirstStateID=[NSString stringWithFormat:@"%@",statequery];
            NSString *getfirstStateID=[NSString stringWithFormat:@"%@",[arr_stateId objectAtIndex:0]];// MAJOR CHANGE HERE
            
            
            
            if ([getfirstStateID isEqualToString:@"9999"])
            {
                ///// statequery=[NSString stringWithFormat:@"WHERE SERVICE_STATE NOT IN ('%@')",@"99"];
                
                // statequery=[NSString stringWithFormat:@"WHERE (SERVICE_STATE NOT IN ('%@')%@)",@"99",joinedStateComponents];
                
                statequery=[NSString stringWithFormat:@"WHERE (SERVICE_STATE NOT IN ('%@')%@)",@"99",@"OR  SERVICE_OTHER_STATE like '%|%|%'"];
                
                
            }
            else
            {
                ////// statequery=[NSString stringWithFormat:@"WHERE SERVICE_STATE IN ('%@')",joinedComponents];
                
                statequery=[NSString stringWithFormat:@"WHERE (SERVICE_STATE  IN ('%@')%@)",joinedComponents,joinedStateComponents];
                //WHERE SERVICE_STATE  IN ('9999') OR  SERVICE_OTHER_STATE like '%|9999|%'
                
                
            }
            
            myQuery=[NSString stringWithFormat:@"%@ %@",query,statequery];
            
            // select * from TABLE_SERVICES_DATA WHERE SERVICE_STATE IN ('20','31','37')  GROUP BY SERVICE_ID ORDER BY SERVICE_NAME ASC
            
        }
        
        //query= subquery+subquery1
        
    }
    if ([categoryList count]!=0)
    {
        NSString *joinedComponents = [categoryList componentsJoinedByString:@"','"];
        categoryquery=[NSString stringWithFormat:@"SERVICE_CATEGORY IN ('%@')",joinedComponents];
        
        NSLog(@"categoryquery= %@",categoryquery);
        // AND
        myQuery=[NSString stringWithFormat:@"%@ WHERE %@",query,categoryquery];
        
    }
    
    
    
    if ([categoryquery length]!=0)
    {
        if ([statequery length]!=0)
        {
            
            myQuery=[NSString stringWithFormat:@"%@ %@ AND %@",query,statequery,categoryquery];
        }
        
    }
    
    
    
    if ([myQuery length]!=0) {
        query=[NSString stringWithFormat:@"%@ %@",myQuery,lastquery];
        
    }
    else
        query=[NSString stringWithFormat:@"%@ %@",query,lastquery];
    
    
    
    NSArray *arry=[UMSqliteManager executeQuery:query];
    return arry;
    
}

// NSString *dateStartString = @“31/12/2010 11:04:02”;

//-(NSArray*)getFilteredNotifData:(NSString*)sortBy notifType:(NSString*)notifType serviceIdAlist:(NSArray*)serviceIdAlist stateIdAlist:(NSArray*)stateIdAlist startDate:(NSString*)startDate  endDate:(NSString*)endDate


-(NSArray*)getFilteredNotifData:(NSString*)sortBy notifType:(NSString*)notifType serviceIdAlist:(NSArray*)serviceIdAlist stateIdAlist:(NSArray*)stateIdAlist startDate:(NSString*)startDate  endDate:(NSString*)endDate user_id:(NSString*)user_id
{
    // Prepare the query string.
    NSString *query;
    NSString *notifTypeQuery;
    NSString *statequery;
    NSString *serviceIdquery;
    NSString *lastquery;
    NSString *finalQuery;
    NSString *ExecuteQuery;
    
    query = [NSString stringWithFormat:@"select * from TABLE_NOTIFICATIONS where"];
    
    // key to pass notifType = promo/trans/All
    
    //if ([notifType isEqualToString:@"promo"]) //Promotional case
    //        if ([type isEqualToString:@"promo"]||[type isEqualToString:NSLocalizedString(@"promo", nil)])
    
    
    if ([notifType isEqualToString:NSLocalizedString(@"promotional_small", nil)]||[notifType isEqualToString:NSLocalizedString(@"promo", nil)]||[notifType isEqualToString:@"Promo"]) //Promotional case
        
    {
        //  notifTypeQuery=[NSString stringWithFormat:@"NOTIF_TYPE=%@",@"'Promo'"];//
        notifTypeQuery=[NSString stringWithFormat:@"NOTIF_TYPE=%@",@"'promo'"];//
        
        finalQuery =[NSString stringWithFormat:@"%@ %@",query,notifTypeQuery];
        
        
        
    }
    // if ([notifType isEqualToString:@"trans"]) //Transactional
    if ([notifType isEqualToString:NSLocalizedString(@"transactional_small", nil)])
    {
        notifTypeQuery=[NSString stringWithFormat:@"NOTIF_TYPE=%@",@"'trans'"];//
        
        finalQuery =[NSString stringWithFormat:@"%@ %@",query,notifTypeQuery];
        
        
    }
    if ([notifType isEqualToString:NSLocalizedString(@"all", nil)]) // ALL
    {
        //new change
        // notifTypeQuery=[NSString stringWithFormat:@" NOTIF_TYPE  IN ('Promo', 'trans')"];
        notifTypeQuery=[NSString stringWithFormat:@" NOTIF_TYPE  IN ('promo', 'trans')"];
        
        finalQuery =[NSString stringWithFormat:@"%@ %@",query,notifTypeQuery];
        
    }
    
    
    if ([notifType isEqualToString:NSLocalizedString(@"favourites_small", nil)])
    {
        notifTypeQuery=[NSString stringWithFormat:@"NOTIF_IS_FAV=%@",@"'true'"];
        finalQuery = [NSString stringWithFormat:@"%@ %@",query,notifType];
    }
    
    
    
    // Handle StateID data fetch
    
    
    if ([stateIdAlist count]!=0)
    {
        
        
        
        NSMutableArray *arr_stateId=[[NSMutableArray alloc]init];
        
        for (int i=0; i<[stateIdAlist count]; i++)
        {
            
            NSString *stateName = [[stateIdAlist objectAtIndex:i] lowercaseString];
            obj=[[StateList alloc]init];
            NSString* state_id= [obj getStateCode:stateName];
            [arr_stateId addObject:state_id];
        }
        NSString *joinedComponents = [arr_stateId componentsJoinedByString:@"','"];
        
        statequery=[NSString stringWithFormat:@"AND  NOTIF_STATE IN ('%@')",joinedComponents];
        
        if ([arr_stateId count]==1)
        {
            NSString *getfirstStateID=[NSString stringWithFormat:@"%@",[arr_stateId objectAtIndex:0]];
            if ([getfirstStateID isEqualToString:@"9999"])
            {
                finalQuery=[NSString stringWithFormat:@"%@ %@",query,notifTypeQuery];
                
                
            }
            else
            {
                finalQuery=[NSString stringWithFormat:@"%@ %@ %@",query,notifTypeQuery,statequery];
                
            }
            
        }
        else
        {
            
            finalQuery=[NSString stringWithFormat:@"%@ %@ %@",query,notifTypeQuery,statequery];
            
        }
    }
    
    if ([serviceIdAlist count]!=0)
    {
        
        
        NSString *joinedServiceComponents = [serviceIdAlist componentsJoinedByString:@"','"];
        serviceIdquery=[NSString stringWithFormat:@"AND  SERVICE_ID IN ('%@')",joinedServiceComponents];
        
        
        
        if ([statequery length]!=0) {
            
            // statequery=[NSString stringWithFormat:@"AND  NOTIF_STATE IN ('%@')",joinedComponents];
            
            if ([statequery rangeOfString:@"9999"].location == NSNotFound) {
                NSLog(@"string does not contain 9999");
                finalQuery=[NSString stringWithFormat:@"%@ %@ %@ %@",query,notifTypeQuery,statequery,serviceIdquery];
                
            } else {
                NSLog(@"string contains 9999!");
                finalQuery=[NSString stringWithFormat:@"%@ %@ %@",query,notifTypeQuery,serviceIdquery];
                
            }
            
            //serviceIdquery=[NSString stringWithFormat:@"%@ %@ ",statequery, serviceIdquery];
        }
        else
        {
            // serviceIdquery=[NSString stringWithFormat:@"%@",serviceIdquery];
            finalQuery=[NSString stringWithFormat:@"%@ %@ %@",query,notifTypeQuery,serviceIdquery];
        }
    }
    
    //query= query+myquery+serviceIDquery
    
    
    
    /* if ([sortBy isEqualToString:NSLocalizedString(@"alphabetic", nil)]) {
     lastquery=[NSString stringWithFormat:@" ORDER BY NOTIF_TITLE DESC"];
     NSLog(@"Alphabetic Sort query=%@",lastquery);
     
     }
     if ([sortBy isEqualToString:NSLocalizedString(@"most_popular", nil)])// no details provided
     {
     lastquery=[NSString stringWithFormat:@" ORDER BY NOTIF_TITLE DESC"];
     NSLog(@"MostPopular query=%@",lastquery);
     
     }
     
     if ([sortBy isEqualToString:NSLocalizedString(@"top_rated", nil)])// no details provided
     {
     lastquery=[NSString stringWithFormat:@" ORDER BY NOTIF_TITLE DESC"];
     NSLog(@"TopRated query=%@",lastquery);
     
     }
     */
    
    lastquery=[NSString stringWithFormat:@" ORDER BY NOTIF_TIME DESC"];
    
    //   NSString *ExecuteQuery=[NSString stringWithFormat:@"%@ %@",finalQuery,lastquery];
    
    // NSString *dateStartString = @“31/12/2010 11:04:02”;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    
    
    /* NSDate *dateStart = [[NSDate alloc] init];
     dateStart = [dateFormatter dateFromString:startDate];
     
     
     NSDate *endStart = [[NSDate alloc] init];
     endStart = [dateFormatter dateFromString:endDate];*/
    
    if ([startDate length]!=0 &&[endDate length]!=0)
    {
        NSString *dateQuery=[NSString stringWithFormat:@"AND NOTIF_RECEIVE_DATE_TIME >= '%@' AND NOTIF_RECEIVE_DATE_TIME  <= '%@'",startDate,endDate];
        //ExecuteQuery=[NSString stringWithFormat:@"%@ %@ %@",finalQuery,dateQuery,lastquery];
        ExecuteQuery=[NSString stringWithFormat:@"%@ %@ AND USER_ID='%@' %@",finalQuery,dateQuery,user_id,lastquery];
        
    }
    else
    {
        //ExecuteQuery=[NSString stringWithFormat:@"%@ %@",finalQuery,lastquery];
        ExecuteQuery=[NSString stringWithFormat:@"%@ AND USER_ID='%@' %@",finalQuery,user_id,lastquery];
        
    }
    
    
    
    NSLog(@"ExecuteQuery =%@",ExecuteQuery);
    
    /*
     
     Select * from TABLE_NOTIFICATIONS where NOTIF_TYPE ='promo' AND  NOTIF_STATE IN ('15','9999','9999') AND  SERVICE_ID IN ('1','2','3') AND NOTIF_RECEIVE_DATE_TIME >= '31/12/2010 11:04:02' AND NOTIF_RECEIVE_DATE_TIME  <= '31/12/2010 11:04:02'       ORDER BY NOTIF_TITLE DESC
     
     
     select * from TABLE_NOTIFICATIONS where NOTIF_TYPE='promo' AND  NOTIF_STATE IN ('9999') AND  SERVICE_ID IN ('14','17','30','10','13','24','20','31','25','18','11','15','21','22','16','12','9') AND NOTIF_RECEIVE_DATE_TIME >= '31/12/2016 11:04:02' AND NOTIF_RECEIVE_DATE_TIME  <= '10/03/2017 11:04:02'  ORDER BY NOTIF_TITLE ASC
     
     
     */
    
    
    
    NSArray *arry=[UMSqliteManager executeQuery:ExecuteQuery];
    ///need below implement
    return arry;
}

-(NSArray*)getEducationList
{
    
    NSArray *arr_education=[NSArray arrayWithObjects:NSLocalizedString(@"primary", nil),NSLocalizedString(@"middleschool", nil),NSLocalizedString(@"secondary", nil),NSLocalizedString(@"diploma", nil),NSLocalizedString(@"graduate", nil),NSLocalizedString(@"professionalcacsicwaetc", nil),NSLocalizedString(@"postgraduate", nil),NSLocalizedString(@"doctorate", nil), nil];
    return arr_education;
}


-(NSArray*)getOccupationList
{
    NSArray *arr_Occupation=[NSArray arrayWithObjects:NSLocalizedString(@"accountantgeneral", nil),NSLocalizedString(@"accountsclerk", nil),NSLocalizedString(@"actor", nil),NSLocalizedString(@"airforce", nil),NSLocalizedString(@"analyst", nil),NSLocalizedString(@"animalhusbandry", nil),NSLocalizedString(@"archaeologist", nil),NSLocalizedString(@"architect", nil),NSLocalizedString(@"artist", nil),NSLocalizedString(@"armedforce", nil),NSLocalizedString(@"attendant", nil),NSLocalizedString(@"bankworker", nil),NSLocalizedString(@"barrister", nil),NSLocalizedString(@"biotechnologist", nil),NSLocalizedString(@"botanist", nil),NSLocalizedString(@"carpenter", nil),NSLocalizedString(@"chemist", nil),NSLocalizedString(@"civilservant", nil),NSLocalizedString(@"clerk", nil),NSLocalizedString(@"commoditiestrader", nil),NSLocalizedString(@"consultant", nil),NSLocalizedString(@"contractor", nil),NSLocalizedString(@"cook", nil),NSLocalizedString(@"copywriter", nil),NSLocalizedString(@"dairy", nil),NSLocalizedString(@"dentist", nil),NSLocalizedString(@"designer", nil),NSLocalizedString(@"doctor", nil),NSLocalizedString(@"domesticcleaner", nil),NSLocalizedString(@"driver", nil),NSLocalizedString(@"economist", nil),NSLocalizedString(@"educator", nil),NSLocalizedString(@"electrician", nil),NSLocalizedString(@"engineer", nil),NSLocalizedString(@"entrepreneur", nil),NSLocalizedString(@"eventorganizer", nil),NSLocalizedString(@"farmer", nil),NSLocalizedString(@"fisherman", nil),NSLocalizedString(@"jeweller", nil),NSLocalizedString(@"journalist", nil),NSLocalizedString(@"judge", nil),NSLocalizedString(@"hairdresser", nil),NSLocalizedString(@"laborer", nil),NSLocalizedString(@"laundryworker", nil),NSLocalizedString(@"lecturer", nil),NSLocalizedString(@"legal", nil),NSLocalizedString(@"manager", nil),NSLocalizedString(@"mechanic", nil),NSLocalizedString(@"navy", nil),NSLocalizedString(@"nurse", nil),NSLocalizedString(@"photographer", nil),NSLocalizedString(@"physician", nil),NSLocalizedString(@"plumber", nil),NSLocalizedString(@"policeofficer", nil),NSLocalizedString(@"politician", nil),NSLocalizedString(@"publisher", nil),NSLocalizedString(@"publicsectoremployee", nil),NSLocalizedString(@"privatesectoremployee", nil),NSLocalizedString(@"receptionist", nil),NSLocalizedString(@"repairer", nil),NSLocalizedString(@"salesmarketing", nil),NSLocalizedString(@"securityofficer", nil),NSLocalizedString(@"selfemployed", nil),NSLocalizedString(@"scientist", nil),NSLocalizedString(@"socialworker", nil),NSLocalizedString(@"statistician", nil),NSLocalizedString(@"surgeon", nil),NSLocalizedString(@"teacher", nil),NSLocalizedString(@"technician", nil),NSLocalizedString(@"therapist", nil),NSLocalizedString(@"trainer", nil),NSLocalizedString(@"transporter", nil),NSLocalizedString(@"veterinarian", nil),NSLocalizedString(@"watchman", nil),NSLocalizedString(@"others", nil), nil];
    return arr_Occupation;
}






-(NSArray*)getFilteredFavouriteServiceData:(NSString*)sortBy serviceType:(NSString*)serviceType stateIdAlist:(NSArray*)stateIdAlist categoryList:(NSArray*)categoryList
{
    NSString *query;
    NSString *lastquery;
    
    NSString *myQuery;
    NSString *statequery;
    NSString *categoryquery;
    //GROUP BY SERVICE_ID
    //query = [NSString stringWithFormat:@"select * from TABLE_SERVICES_DATA "];
    //close by me remove double from backend
    query = [NSString stringWithFormat:@"select * from TABLE_SERVICES_DATA"];
    
    
    
    if ([sortBy isEqualToString:NSLocalizedString(@"alphabetic", nil)])
    {
        //lastquery=[NSString stringWithFormat:@" ORDER BY SERVICE_NAME ASC"]; //i close
        lastquery=[NSString stringWithFormat:@" GROUP BY SERVICE_ID ORDER BY SERVICE_NAME ASC"];
        
        NSLog(@"Alphabetic Sort query=%@",lastquery);
        
    }
    if ([sortBy isEqualToString:NSLocalizedString(@"most_popular", nil)])//do nothing as nothing info provided
    {
        // lastquery=[NSString stringWithFormat:@" ORDER BY SERVICE_RATING DESC"];
        // lastquery=[NSString stringWithFormat:@" ORDER BY SERVICE_POPULARITY DESC"];//i close
        lastquery=[NSString stringWithFormat:@" GROUP BY SERVICE_ID ORDER BY SERVICE_POPULARITY DESC"];
        
        NSLog(@"MostPopular query=%@",lastquery);
        
    }
    
    if ([sortBy isEqualToString:NSLocalizedString(@"top_rated", nil)])//Sort as per Rating
    {
        
        lastquery=[NSString stringWithFormat:@" GROUP BY SERVICE_ID ORDER BY SERVICE_RATING DESC"];
        
        // lastquery=[NSString stringWithFormat:@" ORDER BY SERVICE_RATING DESC"];//i close
        NSLog(@"TopRated query=%@",lastquery);
        
    }
    
    
    if ([serviceType isEqualToString:NSLocalizedString(@"all", nil)])//
    {
        //do nothing
    }
    
    if ([serviceType isEqualToString:NSLocalizedString(@"centralgovernment", nil)])//
    {
        //do nothing
        statequery=[NSString stringWithFormat:@"WHERE SERVICE_STATE IN ('%@')",@"99"]; //99 in case of central governemnt
        //For Central Government Services the state id will be 99 and currently there is no option for sorting by Most Popular.
        myQuery=[NSString stringWithFormat:@"%@ %@",query,statequery];
        
    }
    
    /*   if ([serviceType isEqualToString:NSLocalizedString(@"regional", nil)])//
     {
     if ([stateIdAlist count]!=0)
     {
     
     
     // state_id= [obj getStateCode:str_state];
     
     
     
     NSMutableArray *arr_stateId=[[NSMutableArray alloc]init];
     
     for (int i=0; i<[stateIdAlist count]; i++)
     {
     
     NSString *stateName = [[stateIdAlist objectAtIndex:i] lowercaseString];
     
     obj=[[StateList alloc]init];
     
     NSString* state_id= [obj getStateCode:stateName];
     
     [arr_stateId addObject:state_id];
     }
     NSString *joinedComponents = [arr_stateId componentsJoinedByString:@"','"];
     
     
     
     NSString *stateComponents=[arr_stateId componentsJoinedByString:@","];
     
     NSString *joinedStateComponents = [stateComponents stringByReplacingOccurrencesOfString:@"," withString:@"|%' OR SERVICE_OTHER_STATE like '%|"];
     joinedStateComponents=[NSString stringWithFormat:@" OR  SERVICE_OTHER_STATE like '%%|%@|%%' ",joinedStateComponents];
     
     NSLog(@"joinedStateComponent After === > %@",joinedStateComponents);
     
     statequery=[NSString stringWithFormat:@"WHERE SERVICE_STATE IN ('%@')%@",joinedComponents,joinedStateComponents];
     
     
     
     
     ////// statequery=[NSString stringWithFormat:@"WHERE SERVICE_STATE IN ('%@')",joinedComponents];
     
     myQuery=[NSString stringWithFormat:@"%@ %@",query,statequery];
     
     }
     
     //query= subquery+subquery1
     
     }
     */
    
    
    if ([serviceType isEqualToString:NSLocalizedString(@"regional", nil)])//
    {
        if ([stateIdAlist count]!=0) {
            
            
            // state_id= [obj getStateCode:str_state];
            
            
            
            NSMutableArray *arr_stateId=[[NSMutableArray alloc]init];
            
            for (int i=0; i<[stateIdAlist count]; i++)
            {
                
                NSString *stateName = [[stateIdAlist objectAtIndex:i] lowercaseString];
                
                obj=[[StateList alloc]init];
                
                NSString* state_id= [obj getStateCode:stateName];
                
                [arr_stateId addObject:state_id];
            }
            NSString *joinedComponents = [arr_stateId componentsJoinedByString:@"','"];
            
            
            NSString *stateComponents=[arr_stateId componentsJoinedByString:@","];
            
            NSString *joinedStateComponents = [stateComponents stringByReplacingOccurrencesOfString:@"," withString:@"|%' OR SERVICE_OTHER_STATE like '%|"];
            joinedStateComponents=[NSString stringWithFormat:@" OR  SERVICE_OTHER_STATE like '%%|%@|%%' ",joinedStateComponents];
            
            NSLog(@"joinedStateComponent After === > %@",joinedStateComponents);
            
            
            
            statequery=[NSString stringWithFormat:@"WHERE (SERVICE_STATE IN ('%@')%@)",joinedComponents,joinedStateComponents];
            
            
            
            // WHERE SERVICE_STATE IN ('9999') OR  SERVICE_OTHER_STATE like '%|9999|%'
            
            if ([statequery rangeOfString:@"9999"].location == NSNotFound) {
                NSLog(@"string does not contain 9999");
                ////// statequery=[NSString stringWithFormat:@"WHERE SERVICE_STATE IN ('%@')",joinedComponents];
                
                statequery=[NSString stringWithFormat:@"WHERE (SERVICE_STATE IN ('%@')%@)",joinedComponents,joinedStateComponents];
                
                
                
            } else {
                NSLog(@"string contains 9999!");
                ////// statequery=[NSString stringWithFormat:@"WHERE SERVICE_STATE NOT IN ('%@')",@"99"];
                // statequery=[NSString stringWithFormat:@"WHERE (SERVICE_STATE NOT IN ('%@')%@)",@"99",joinedStateComponents];
                
                statequery=[NSString stringWithFormat:@"WHERE (SERVICE_STATE NOT IN ('%@')%@)",@"99",@"OR  SERVICE_OTHER_STATE like '%|%|%'"];
                
                
                
            }
            
            
            
            
            //NSString *getfirstStateID=[NSString stringWithFormat:@"%@",statequery];
            NSString *getfirstStateID=[NSString stringWithFormat:@"%@",[arr_stateId objectAtIndex:0]];// MAJOR CHANGE HERE
            
            
            
            if ([getfirstStateID isEqualToString:@"9999"])
            {
                ///// statequery=[NSString stringWithFormat:@"WHERE SERVICE_STATE NOT IN ('%@')",@"99"];
                
                // statequery=[NSString stringWithFormat:@"WHERE (SERVICE_STATE NOT IN ('%@')%@)",@"99",joinedStateComponents];
                
                statequery=[NSString stringWithFormat:@"WHERE (SERVICE_STATE NOT IN ('%@')%@)",@"99",@"OR  SERVICE_OTHER_STATE like '%|%|%'"];
                
                
                
                
                
            }
            else
            {
                ////// statequery=[NSString stringWithFormat:@"WHERE SERVICE_STATE IN ('%@')",joinedComponents];
                
                statequery=[NSString stringWithFormat:@"WHERE (SERVICE_STATE  IN ('%@')%@)",joinedComponents,joinedStateComponents];
                //WHERE SERVICE_STATE  IN ('9999') OR  SERVICE_OTHER_STATE like '%|9999|%'
                
                
            }
            
            myQuery=[NSString stringWithFormat:@"%@ %@",query,statequery];
            
            // select * from TABLE_SERVICES_DATA WHERE SERVICE_STATE IN ('20','31','37')  GROUP BY SERVICE_ID ORDER BY SERVICE_NAME ASC
            
        }
    }
    
    if ([categoryList count]!=0)
    {
        NSString *joinedComponents = [categoryList componentsJoinedByString:@"','"];
        categoryquery=[NSString stringWithFormat:@"SERVICE_CATEGORY IN ('%@')",joinedComponents];
        
        NSLog(@"categoryquery= %@",categoryquery);
        // AND
        myQuery=[NSString stringWithFormat:@"%@ WHERE %@",query,categoryquery];
        
    }
    
    
    
    if ([categoryquery length]!=0)
    {
        if ([statequery length]!=0)
        {
            
            myQuery=[NSString stringWithFormat:@"%@ %@ AND %@",query,statequery,categoryquery];
        }
        
    }
    
    if ([myQuery length]!=0) {
        query=[NSString stringWithFormat:@"%@ AND SERVICE_IS_FAV ='true' %@",myQuery,lastquery];
        
    }
    else
        query=[NSString stringWithFormat:@"%@ WHERE SERVICE_IS_FAV ='true' %@",query,lastquery];
    
    NSArray *arry=[UMSqliteManager executeQuery:query];
    return arry;
    
}




-(void)saveImagesInLocalDirectory:(NSString*)imgURL
{
    NSString * documentsDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *imgName = @"user_image.png";
    NSString *writablePath = [documentsDirectoryPath stringByAppendingPathComponent:imgName];
    
    // file doesn't exist
    NSLog(@"file doesn't exist");
    //save Image From URL
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString: imgURL]];
    
    NSError *error = nil;
    [data writeToFile:writablePath options:NSAtomicWrite error:&error];
    
    if (error) {
        NSLog(@"Error Writing File : %@",error);
    }else{
        NSLog(@"Image %@ Saved SuccessFully",imgName);
    }
}

-(UIImage*)loadImage
{
    NSString * documentsDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *imgName = @"user_image.png";
    NSString *writablePath = [documentsDirectoryPath stringByAppendingPathComponent:imgName];
    UIImage* image = [UIImage imageWithContentsOfFile:writablePath];
    return image;
}







//----------------------------------------------------------
//          dropTable  USED TO REMOVE DB from the path
//----------------------------------------------------------
/*
 -(void)deleteDatabase
 {
 
 NSString *docsDir;
 NSArray *dirPaths;
 //sqlite3 *DB;
 dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 docsDir = [dirPaths objectAtIndex:0];
 // Build the path to the database file
 // NSString*  databasePath = [docsDir stringByAppendingPathComponent:@"UMANG_DATABASE.db"];
 NSString*  dbPath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"UMANG_DATABASE.db"]];
 
 if([[NSFileManager defaultManager] fileExistsAtPath:dbPath]){
 [[NSFileManager defaultManager] removeItemAtPath:dbPath error:nil];
 }
 
 }
 
 
 -(BOOL)checkColumnExists
 {
 BOOL columnExists = NO;
 
 
 
 NSString *query;
 query = [NSString stringWithFormat:@"select SERVICE_OTHER_STATE from TABLE_SERVICES_DATA"];
 
 //query = [NSString stringWithFormat:@"select SERVICEE from TABLE_SERVICES_DATA"];
 
 if ([UMSqliteManager CheckIfExit:query]==YES)
 {
 
 NSLog(@"columnExists Present");
 columnExists=YES;
 
 }else{
 NSLog(@"columnExists Not present");
 columnExists=NO;
 
 }
 
 
 return columnExists;
 }
 
 
 -(void)upgradeDatabaseIfRequired;
 
 {
 
 BOOL columnServiceExist=[self checkColumnExists];
 if (columnServiceExist==TRUE)
 {
 //do nothing
 }
 else
 {
 NSString *query;
 
 query = [NSString stringWithFormat:@"ALTER TABLE TABLE_SERVICES_DATA ADD COLUMN SERVICE_OTHER_STATE TEXT"];
 // query = [NSString stringWithFormat:@"ALTER TABLE TABLE_SERVICES_DATA ADD COLUMN RAWAT_STATE TEXT"];
 
 if ([UMSqliteManager executeScalarQuery:query]==YES)
 {
 
 NSLog(@"Data  inserted successfully");
 
 }else{
 NSLog(@"Data not inserted successfully");
 }
 }
 
 
 }
 
 
 +(BOOL)CheckIfExit:(NSString*)str
 {
 sqlite3_stmt *statement= nil;
 sqlite3 *database;
 
 
 
 // NSString *strPath =[NSString stringWithFormat:@"%@", [self getDatabasePath]]; // close on 12 sept and load exact path
 
 
 
 BOOL columnExists = NO;
 
 NSString *docsDir;
 NSArray *dirPaths;
 //sqlite3 *DB;
 dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 docsDir = [dirPaths objectAtIndex:0];
 NSString*  strPath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"UMANG_DATABASE.db"]];
 
 
 
 const char *fileName = [strPath UTF8String];
 NSLog(@"path 00%s", fileName);
 
 
 
 if (sqlite3_open(fileName,&database) == SQLITE_OK)
 {
 
 if (sqlite3_prepare_v2(database, [str UTF8String], -1, &statement, NULL) == SQLITE_OK)
 {
 
 columnExists = YES;
 
 }
 else
 {
 
 NSLog(@"FAIL QUERY Problem with prepare statement: %s", sqlite3_errmsg(database));
 columnExists  =NO;
 }
 
 }
 else
 {
 NSLog(@"FAIL QUERY An error has occured: %s",sqlite3_errmsg(database));
 columnExists  =NO;
 }
 
 
 
 // sqlite3_finalize(statement);
 
 sqlite3_close(database);
 
 
 return columnExists;
 
 
 
 
 
 
 }
 
 
 
 
 
 */




-(void)onUpgrade:(NSInteger)oldVersion withnewVersion:(NSInteger)newVersion
{
    NSString *query;
    switch (oldVersion)
    {
        case 1:
        {
            /* query = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS TABLE_SERVICES_DATA(ID INTEGER PRIMARY KEY AUTOINCREMENT,SERVICE_ID TEXT,SERVICE_NAME TEXT,SERVICE_DESC TEXT,SERVICE_IMAGE TEXT,SERVICE_CATEGORY TEXT,SERVICE_SUB_CATEGORY TEXT,SERVICE_RATING TEXT,SERVICE_URL TEXT,SERVICE_STATE TEXT,SERVICE_LATITUDE TEXT,SERVICE_LONGITUDE TEXT,SERVICE_IS_FAV TEXT,SERVICE_IS_HIDDEN TEXT,SERVICE_PHONE_NUMBER TEXT,SERVICE_IS_NOTIF_ENABLED TEXT,SERVICE_WEBSITE TEXT, SERVICE_DEPTADDRESS TEXT,SERVICE_WORKINGHOURS TEXT,SERVICE_DEPTDESCRIPTION TEXT,SERVICE_LANG  TEXT,SERVICE_EMAIL  TEXT,SERVICE_POPULARITY TEXT,SERVICE_CATEGORY_ID TEXT)"];
             [self loadDbQuery:query];*/
        }
            //upgrade logic from version 1 to 2
            //break;
            
        case 2:
        {
            query = [NSString stringWithFormat:@"ALTER TABLE TABLE_SERVICES_DATA ADD COLUMN SERVICE_OTHER_STATE TEXT"];
            [self loadDbQuery:query];
            
            
            
            NSString*  servicequery = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS TABLE_SERVICES_DIRECTORY(ID INTEGER PRIMARY KEY AUTOINCREMENT,SERVICE_ID TEXT,SERVICE_NAME TEXT,SERVICE_DESC TEXT,SERVICE_IMAGE TEXT,SERVICE_LATITUDE TEXT, SERVICE_LONGITUDE TEXT,SERVICE_PHONE_NUMBER TEXT,SERVICE_WEBSITE TEXT, SERVICE_EMAIL TEXT,SERVICE_ADDRESS TEXT,SERVICE_WORKING_HOURS TEXT,SERVICE_OTHER_INFO TEXT,SERVICE_NATIVE_APP TEXT,SERVICE_IS_AVAILABLE TEXT,SERVICE_NATIVE_APPNAME TEXT)"];
            [self loadDbQuery:servicequery];
            
            
            
            
        }
            
            // break;
        case 3:
        {
            query = [NSString stringWithFormat:@"ALTER TABLE TABLE_BOOKS_DATA ADD COLUMN BOOK_CATEGORY TEXT"];
            [self loadDbQuery:query];
            
        }
            //  break;
        case 4:
        {
            // Add query to Add OTHER_WEBSITE IN TABLE_SERVICES_DATA
            query = [NSString stringWithFormat:@"ALTER TABLE TABLE_SERVICES_DATA ADD COLUMN OTHER_WEBSITE TEXT NOT NULL DEFAULT''"];
            [self loadDbQuery:query];
            
            // Add query to Add OTHER_WEBSITE IN TABLE_SERVICES_DIRECTORY
            query = [NSString stringWithFormat:@"ALTER TABLE TABLE_SERVICES_DIRECTORY ADD COLUMN OTHER_WEBSITE TEXT NOT NULL DEFAULT''"];
            [self loadDbQuery:query];
            
            // Add query to Add AND_LINK IN TABLE_SERVICES_DIRECTORY
            query = [NSString stringWithFormat:@"ALTER TABLE TABLE_SERVICES_DIRECTORY ADD COLUMN AND_LINK TEXT NOT NULL DEFAULT''"];
            [self loadDbQuery:query];
            
            
            
            
            // Add query to Add SERVICE_NAME IN TABLE_NOTIFICATIONS
            query = [NSString stringWithFormat:@"ALTER TABLE TABLE_NOTIFICATIONS ADD COLUMN SERVICE_NAME TEXT NOT NULL DEFAULT''"];
            [self loadDbQuery:query];
            
            // Add query to Add SERVICE_NAME IN TABLE_NOTIFICATIONS
            query = [NSString stringWithFormat:@"ALTER TABLE TABLE_NOTIFICATIONS ADD COLUMN DEPT_NAME TEXT NOT NULL DEFAULT''"];
            [self loadDbQuery:query];
            
            // "servicename":"ncert12345aSDFGHJKL",
            // "deptname":"ncert12345"
            
        }
            // add Category In Book
        case 5:
        {
            query = [NSString stringWithFormat:@"ALTER TABLE TABLE_BOOKS_DATA ADD COLUMN CATEGORY TEXT NOT NULL DEFAULT''"];
            
            
            [self loadDbQuery:query];
            
        }
            /*
             adding below parameter
             "categoryid": "13", CATEGORY_ID
             "stateid": "99", STATE_ID
             "otherstate": "", OTHER_STATE
             "categoryname": "Utility" CATEGORY_NAME
             */
        case 6:
        {
            // Add query to Add OTHER_WEBSITE IN TABLE_SERVICES_DIRECTORY
            query = [NSString stringWithFormat:@"ALTER TABLE TABLE_SERVICES_DIRECTORY ADD COLUMN CATEGORY_ID TEXT NOT NULL DEFAULT''"];
            [self loadDbQuery:query];
            
            
            // Add query to Add OTHER_WEBSITE IN TABLE_SERVICES_DIRECTORY
            query = [NSString stringWithFormat:@"ALTER TABLE TABLE_SERVICES_DIRECTORY ADD COLUMN STATE_ID TEXT NOT NULL DEFAULT''"];
            [self loadDbQuery:query];
            
            // Add query to Add OTHER_WEBSITE IN TABLE_SERVICES_DIRECTORY
            query = [NSString stringWithFormat:@"ALTER TABLE TABLE_SERVICES_DIRECTORY ADD COLUMN OTHER_STATE TEXT NOT NULL DEFAULT''"];
            [self loadDbQuery:query];
            
            // Add query to Add OTHER_WEBSITE IN TABLE_SERVICES_DIRECTORY
            query = [NSString stringWithFormat:@"ALTER TABLE TABLE_SERVICES_DIRECTORY ADD COLUMN CATEGORY_NAME TEXT NOT NULL DEFAULT''"];
            [self loadDbQuery:query];
        }
            break;
        default:
            break;
    }
    
    
    
}





-(void)loadDbQuery:(NSString*)query
{
    
    if ([UMSqliteManager updateDBifRequired:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
}


+(BOOL)updateDBifRequired:(NSString*)str
{
    sqlite3_stmt *statement= nil;
    sqlite3 *database;
    
    NSLog(@"executeScalarQuery is called =%@",str);
    
    
    // NSString *strPath =[NSString stringWithFormat:@"%@", [self getDatabasePath]]; // close on 12 sept and load exact path
    @synchronized(self)
    {
        @try {
            
            BOOL fRet = NO;
            
            NSString *docsDir;
            NSArray *dirPaths;
            //sqlite3 *DB;
            dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            docsDir = [dirPaths objectAtIndex:0];
            NSString*  strPath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"UMANG_DATABASE.db"]];
            
            
            
            const char *fileName = [strPath UTF8String];
            NSLog(@"path 00%s", fileName);
            
            
            
            if (sqlite3_open(fileName,&database) == SQLITE_OK)
            {
                
                if (sqlite3_prepare_v2(database, [str UTF8String], -1, &statement, NULL) == SQLITE_OK)
                {
                    
                    if (sqlite3_step(statement) == SQLITE_DONE)
                    {
                        fRet =YES;
                    }
                    else
                    {
                        
                        NSLog(@"FAIL QUERY=%@ strPath=%@ ",str,strPath);
                        
                        NSLog(@"error: %s", sqlite3_errmsg(database));
                        
                        
                    }
                    
                    sqlite3_finalize(statement);
                    
                }
                else
                {
                    
                    NSLog(@"FAIL QUERY Problem with prepare statement: %s", sqlite3_errmsg(database));
                }
                
            }
            else
            {
                NSLog(@"FAIL QUERY An error has occured: %s",sqlite3_errmsg(database));
            }
            
            
            
            // sqlite3_finalize(statement);
            
            sqlite3_close(database);
            
            
            return fRet;
            
            
            
        } @catch (NSException *exception)
        {
            NSLog(@"FAIL QUERY exception=%@",exception);
            
            //sqlite3_finalize(statement);
            
            sqlite3_close(database);
            
            // NSLog(@"executeScalarQuery exception (%s)", sqlite3_errmsg(database));
            
        } @finally
        {
            //sqlite3_close(database);//later close it july 14
            
            NSLog(@"executeScalarQuery finally (%s)", sqlite3_errmsg(database));
            
        }
        
        
    }
    
}




//----------------------------------------------------------
//          deleteServicesDirectory TABLE_SERVICES_DIRECTORY Done
//----------------------------------------------------------


-(void)deleteServicesDirectory
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"DELETE FROM TABLE_SERVICES_DIRECTORY"];
    if ([UMSqliteManager executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
}
//----------------------------------------------------------
//          deleteServiceDirDataWithId TABLE_SERVICES_DIRECTORY Done
//----------------------------------------------------------


-(void)deleteServiceDirDataWithId:(NSString*)serviceId
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"DELETE FROM TABLE_SERVICES_DIRECTORY where SERVICE_ID='%@'", serviceId];
    if ([UMSqliteManager executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
}




//----------------------------------------------------------
//          getAllServicesDirData TABLE_SERVICES_DIRECTORY
//----------------------------------------------------------

-(NSArray*)getAllServicesDirData
{
    
    NSString *query;
    // query = [NSString stringWithFormat:@"SELECT * FROM TABLE_BOOKS_DATA where USER_ID='%@'", userId];
    
    query = [NSString stringWithFormat:@"SELECT * from TABLE_SERVICES_DIRECTORY group by SERVICE_ID"];
    
    
    
    NSArray *arry=[UMSqliteManager executeQuery:query];
    
    return arry;
}





//----------------------------------------------------------
//          GETALLUMANG SERVICE TABLE_SERVICES_DIRECTORY
//----------------------------------------------------------

-(NSArray*)getAllUMANGServicesDirData
{
    
    NSString *query;
    
    /*  query = [NSString stringWithFormat:@"SELECT TABLE_SERVICES_DIRECTORY.SERVICE_ID,TABLE_SERVICES_DIRECTORY.SERVICE_NAME,TABLE_SERVICES_DIRECTORY.SERVICE_DESC,TABLE_SERVICES_DIRECTORY.SERVICE_IMAGE,TABLE_SERVICES_DIRECTORY.SERVICE_LATITUDE , TABLE_SERVICES_DIRECTORY.SERVICE_LONGITUDE ,TABLE_SERVICES_DIRECTORY.SERVICE_PHONE_NUMBER ,TABLE_SERVICES_DIRECTORY.SERVICE_WEBSITE, TABLE_SERVICES_DIRECTORY.SERVICE_EMAIL,TABLE_SERVICES_DIRECTORY.SERVICE_ADDRESS,TABLE_SERVICES_DIRECTORY.SERVICE_WORKING_HOURS,TABLE_SERVICES_DIRECTORY.SERVICE_OTHER_INFO,TABLE_SERVICES_DIRECTORY.SERVICE_NATIVE_APP ,TABLE_SERVICES_DIRECTORY.SERVICE_IS_AVAILABLE,TABLE_SERVICES_DIRECTORY.SERVICE_NATIVE_APPNAME FROM TABLE_SERVICES_DIRECTORY  INNER JOIN TABLE_SERVICES_DATA ON TABLE_SERVICES_DATA.SERVICE_ID = TABLE_SERVICES_DIRECTORY.SERVICE_ID GROUP BY TABLE_SERVICES_DIRECTORY.SERVICE_ID"];
     */
    //close cause of adding new parameter in service directory
    
    /*    query = [NSString stringWithFormat:@"SELECT TABLE_SERVICES_DIRECTORY.SERVICE_ID,TABLE_SERVICES_DIRECTORY.SERVICE_NAME,TABLE_SERVICES_DIRECTORY.SERVICE_DESC,TABLE_SERVICES_DIRECTORY.SERVICE_IMAGE,TABLE_SERVICES_DIRECTORY.SERVICE_LATITUDE , TABLE_SERVICES_DIRECTORY.SERVICE_LONGITUDE ,TABLE_SERVICES_DIRECTORY.SERVICE_PHONE_NUMBER ,TABLE_SERVICES_DIRECTORY.SERVICE_WEBSITE, TABLE_SERVICES_DIRECTORY.SERVICE_EMAIL,TABLE_SERVICES_DIRECTORY.SERVICE_ADDRESS,TABLE_SERVICES_DIRECTORY.SERVICE_WORKING_HOURS,TABLE_SERVICES_DIRECTORY.SERVICE_OTHER_INFO,TABLE_SERVICES_DIRECTORY.SERVICE_NATIVE_APP ,TABLE_SERVICES_DIRECTORY.SERVICE_IS_AVAILABLE,TABLE_SERVICES_DIRECTORY.SERVICE_NATIVE_APPNAME,TABLE_SERVICES_DIRECTORY.OTHER_WEBSITE,TABLE_SERVICES_DIRECTORY.AND_LINK FROM TABLE_SERVICES_DIRECTORY  INNER JOIN TABLE_SERVICES_DATA ON TABLE_SERVICES_DATA.SERVICE_ID = TABLE_SERVICES_DIRECTORY.SERVICE_ID GROUP BY TABLE_SERVICES_DIRECTORY.SERVICE_ID"];
     
     */
    
    query = [NSString stringWithFormat:@"SELECT TABLE_SERVICES_DIRECTORY.SERVICE_ID,TABLE_SERVICES_DIRECTORY.SERVICE_NAME,TABLE_SERVICES_DIRECTORY.SERVICE_DESC,TABLE_SERVICES_DIRECTORY.SERVICE_IMAGE,TABLE_SERVICES_DIRECTORY.SERVICE_LATITUDE , TABLE_SERVICES_DIRECTORY.SERVICE_LONGITUDE ,TABLE_SERVICES_DIRECTORY.SERVICE_PHONE_NUMBER ,TABLE_SERVICES_DIRECTORY.SERVICE_WEBSITE, TABLE_SERVICES_DIRECTORY.SERVICE_EMAIL,TABLE_SERVICES_DIRECTORY.SERVICE_ADDRESS,TABLE_SERVICES_DIRECTORY.SERVICE_WORKING_HOURS,TABLE_SERVICES_DIRECTORY.SERVICE_OTHER_INFO,TABLE_SERVICES_DIRECTORY.SERVICE_NATIVE_APP ,TABLE_SERVICES_DIRECTORY.SERVICE_IS_AVAILABLE,TABLE_SERVICES_DIRECTORY.SERVICE_NATIVE_APPNAME,TABLE_SERVICES_DIRECTORY.OTHER_WEBSITE,TABLE_SERVICES_DIRECTORY.AND_LINK,TABLE_SERVICES_DIRECTORY.CATEGORY_ID,TABLE_SERVICES_DIRECTORY.STATE_ID,TABLE_SERVICES_DIRECTORY.OTHER_STATE,TABLE_SERVICES_DIRECTORY.CATEGORY_NAME FROM TABLE_SERVICES_DIRECTORY  INNER JOIN TABLE_SERVICES_DATA ON TABLE_SERVICES_DATA.SERVICE_ID = TABLE_SERVICES_DIRECTORY.SERVICE_ID GROUP BY TABLE_SERVICES_DIRECTORY.SERVICE_ID"];
    
    
    
    
    NSArray *arry=[UMSqliteManager executeQuery:query];
    
    return arry;
}

//----------------------------------------------------------
//          GETOTHER SERVICES TABLE_SERVICES_DIRECTORY
//----------------------------------------------------------

-(NSArray*)getOTHERServicesDirData
{
    
    NSString *query;
    /* query = [NSString stringWithFormat:@"SELECT TABLE_SERVICES_DIRECTORY.SERVICE_ID,TABLE_SERVICES_DIRECTORY.SERVICE_NAME,TABLE_SERVICES_DIRECTORY.SERVICE_DESC,TABLE_SERVICES_DIRECTORY.SERVICE_IMAGE,TABLE_SERVICES_DIRECTORY.SERVICE_LATITUDE , TABLE_SERVICES_DIRECTORY.SERVICE_LONGITUDE ,TABLE_SERVICES_DIRECTORY.SERVICE_PHONE_NUMBER ,TABLE_SERVICES_DIRECTORY.SERVICE_WEBSITE, TABLE_SERVICES_DIRECTORY.SERVICE_EMAIL,TABLE_SERVICES_DIRECTORY.SERVICE_ADDRESS,TABLE_SERVICES_DIRECTORY.SERVICE_WORKING_HOURS,TABLE_SERVICES_DIRECTORY.SERVICE_OTHER_INFO,TABLE_SERVICES_DIRECTORY.SERVICE_NATIVE_APP ,TABLE_SERVICES_DIRECTORY.SERVICE_IS_AVAILABLE,TABLE_SERVICES_DIRECTORY.SERVICE_NATIVE_APPNAME  FROM TABLE_SERVICES_DIRECTORY LEFT OUTER JOIN TABLE_SERVICES_DATA ON TABLE_SERVICES_DIRECTORY.SERVICE_ID = TABLE_SERVICES_DATA.SERVICE_ID WHERE TABLE_SERVICES_DATA.SERVICE_ID IS null GROUP BY TABLE_SERVICES_DIRECTORY.SERVICE_ID"];
     */
    
    /*  query = [NSString stringWithFormat:@"SELECT TABLE_SERVICES_DIRECTORY.SERVICE_ID,TABLE_SERVICES_DIRECTORY.SERVICE_NAME,TABLE_SERVICES_DIRECTORY.SERVICE_DESC,TABLE_SERVICES_DIRECTORY.SERVICE_IMAGE,TABLE_SERVICES_DIRECTORY.SERVICE_LATITUDE , TABLE_SERVICES_DIRECTORY.SERVICE_LONGITUDE ,TABLE_SERVICES_DIRECTORY.SERVICE_PHONE_NUMBER ,TABLE_SERVICES_DIRECTORY.SERVICE_WEBSITE, TABLE_SERVICES_DIRECTORY.SERVICE_EMAIL,TABLE_SERVICES_DIRECTORY.SERVICE_ADDRESS,TABLE_SERVICES_DIRECTORY.SERVICE_WORKING_HOURS,TABLE_SERVICES_DIRECTORY.SERVICE_OTHER_INFO,TABLE_SERVICES_DIRECTORY.SERVICE_NATIVE_APP ,TABLE_SERVICES_DIRECTORY.SERVICE_IS_AVAILABLE,TABLE_SERVICES_DIRECTORY.SERVICE_NATIVE_APPNAME,TABLE_SERVICES_DIRECTORY.OTHER_WEBSITE,TABLE_SERVICES_DIRECTORY.AND_LINK  FROM TABLE_SERVICES_DIRECTORY LEFT OUTER JOIN TABLE_SERVICES_DATA ON TABLE_SERVICES_DIRECTORY.SERVICE_ID = TABLE_SERVICES_DATA.SERVICE_ID WHERE TABLE_SERVICES_DATA.SERVICE_ID IS null GROUP BY TABLE_SERVICES_DIRECTORY.SERVICE_ID"];
     
     */
    
    
    query = [NSString stringWithFormat:@"SELECT TABLE_SERVICES_DIRECTORY.SERVICE_ID,TABLE_SERVICES_DIRECTORY.SERVICE_NAME,TABLE_SERVICES_DIRECTORY.SERVICE_DESC,TABLE_SERVICES_DIRECTORY.SERVICE_IMAGE,TABLE_SERVICES_DIRECTORY.SERVICE_LATITUDE , TABLE_SERVICES_DIRECTORY.SERVICE_LONGITUDE ,TABLE_SERVICES_DIRECTORY.SERVICE_PHONE_NUMBER ,TABLE_SERVICES_DIRECTORY.SERVICE_WEBSITE, TABLE_SERVICES_DIRECTORY.SERVICE_EMAIL,TABLE_SERVICES_DIRECTORY.SERVICE_ADDRESS,TABLE_SERVICES_DIRECTORY.SERVICE_WORKING_HOURS,TABLE_SERVICES_DIRECTORY.SERVICE_OTHER_INFO,TABLE_SERVICES_DIRECTORY.SERVICE_NATIVE_APP ,TABLE_SERVICES_DIRECTORY.SERVICE_IS_AVAILABLE,TABLE_SERVICES_DIRECTORY.SERVICE_NATIVE_APPNAME,TABLE_SERVICES_DIRECTORY.OTHER_WEBSITE,TABLE_SERVICES_DIRECTORY.AND_LINK,TABLE_SERVICES_DIRECTORY.CATEGORY_ID,TABLE_SERVICES_DIRECTORY.STATE_ID,TABLE_SERVICES_DIRECTORY.OTHER_STATE,TABLE_SERVICES_DIRECTORY.CATEGORY_NAME FROM TABLE_SERVICES_DIRECTORY LEFT OUTER JOIN TABLE_SERVICES_DATA ON TABLE_SERVICES_DIRECTORY.SERVICE_ID = TABLE_SERVICES_DATA.SERVICE_ID WHERE TABLE_SERVICES_DATA.SERVICE_ID IS null GROUP BY TABLE_SERVICES_DIRECTORY.SERVICE_ID"];
    
    
    
    
    NSArray *arry=[UMSqliteManager executeQuery:query];
    
    return arry;
}


//----------------------------------------------------------
//          getServiceStateData Data TABLE_SERVICES_DATA
//----------------------------------------------------------



-(NSArray*)getServiceStateData:(NSString*)stateId
{
    // Prepare the query string.
    NSString *query;
    // query = [NSString stringWithFormat:@"SELECT * FROM TABLE_SERVICES_DATA where SERVICE_STATE='%@' GROUP BY SERVICE_ID", stateId];
    
    // new query added
    NSString *otherState=[NSString stringWithFormat:@"%%|%@|%%",stateId];
    query = [NSString stringWithFormat:@"SELECT * FROM TABLE_SERVICES_DATA where SERVICE_STATE='%@' OR  SERVICE_OTHER_STATE like '%@' GROUP BY SERVICE_ID", stateId,otherState];
    
    // SELECT * FROM TABLE_SERVICES_DATA where SERVICE_STATE='31' OR  SERVICE_OTHER_STATE like '%|31|%' GROUP BY SERVICE_ID
    
    NSArray *arry=[UMSqliteManager executeQuery:query];
    
    return arry;
}

//----------------------------------------------------------
//          GETALLUMANG SERVICE TABLE_SERVICES_DIRECTORY
//----------------------------------------------------------

-(NSArray*)getStateWiseUMANGServicesDirData:(NSString*)state_Id
{
    
    NSString *query;
    
    if ([state_Id isEqualToString:@"9999"] || state_Id.length==0)
    {
        
        
        query = [NSString stringWithFormat:@"SELECT TABLE_SERVICES_DIRECTORY.SERVICE_ID,TABLE_SERVICES_DIRECTORY.SERVICE_NAME,TABLE_SERVICES_DIRECTORY.SERVICE_DESC,TABLE_SERVICES_DIRECTORY.SERVICE_IMAGE,TABLE_SERVICES_DIRECTORY.SERVICE_LATITUDE , TABLE_SERVICES_DIRECTORY.SERVICE_LONGITUDE ,TABLE_SERVICES_DIRECTORY.SERVICE_PHONE_NUMBER ,TABLE_SERVICES_DIRECTORY.SERVICE_WEBSITE, TABLE_SERVICES_DIRECTORY.SERVICE_EMAIL,TABLE_SERVICES_DIRECTORY.SERVICE_ADDRESS,TABLE_SERVICES_DIRECTORY.SERVICE_WORKING_HOURS,TABLE_SERVICES_DIRECTORY.SERVICE_OTHER_INFO,TABLE_SERVICES_DIRECTORY.SERVICE_NATIVE_APP ,TABLE_SERVICES_DIRECTORY.SERVICE_IS_AVAILABLE,TABLE_SERVICES_DIRECTORY.SERVICE_NATIVE_APPNAME,TABLE_SERVICES_DIRECTORY.OTHER_WEBSITE,TABLE_SERVICES_DIRECTORY.AND_LINK,TABLE_SERVICES_DIRECTORY.CATEGORY_ID,TABLE_SERVICES_DIRECTORY.STATE_ID,TABLE_SERVICES_DIRECTORY.OTHER_STATE,TABLE_SERVICES_DIRECTORY.CATEGORY_NAME FROM TABLE_SERVICES_DIRECTORY  INNER JOIN TABLE_SERVICES_DATA ON TABLE_SERVICES_DATA.SERVICE_ID = TABLE_SERVICES_DIRECTORY.SERVICE_ID  GROUP BY TABLE_SERVICES_DIRECTORY.SERVICE_ID"];
        
        
    }
    else
    {
        NSString *otherState=[NSString stringWithFormat:@"%%|%@|%%",state_Id];
        query = [NSString stringWithFormat:@"SELECT TABLE_SERVICES_DIRECTORY.SERVICE_ID,TABLE_SERVICES_DIRECTORY.SERVICE_NAME,TABLE_SERVICES_DIRECTORY.SERVICE_DESC,TABLE_SERVICES_DIRECTORY.SERVICE_IMAGE,TABLE_SERVICES_DIRECTORY.SERVICE_LATITUDE , TABLE_SERVICES_DIRECTORY.SERVICE_LONGITUDE ,TABLE_SERVICES_DIRECTORY.SERVICE_PHONE_NUMBER ,TABLE_SERVICES_DIRECTORY.SERVICE_WEBSITE, TABLE_SERVICES_DIRECTORY.SERVICE_EMAIL,TABLE_SERVICES_DIRECTORY.SERVICE_ADDRESS,TABLE_SERVICES_DIRECTORY.SERVICE_WORKING_HOURS,TABLE_SERVICES_DIRECTORY.SERVICE_OTHER_INFO,TABLE_SERVICES_DIRECTORY.SERVICE_NATIVE_APP ,TABLE_SERVICES_DIRECTORY.SERVICE_IS_AVAILABLE,TABLE_SERVICES_DIRECTORY.SERVICE_NATIVE_APPNAME,TABLE_SERVICES_DIRECTORY.OTHER_WEBSITE,TABLE_SERVICES_DIRECTORY.AND_LINK,TABLE_SERVICES_DIRECTORY.CATEGORY_ID,TABLE_SERVICES_DIRECTORY.STATE_ID,TABLE_SERVICES_DIRECTORY.OTHER_STATE,TABLE_SERVICES_DIRECTORY.CATEGORY_NAME FROM TABLE_SERVICES_DIRECTORY  INNER JOIN TABLE_SERVICES_DATA ON TABLE_SERVICES_DATA.SERVICE_ID = TABLE_SERVICES_DIRECTORY.SERVICE_ID where SERVICE_STATE='%@' OR  SERVICE_OTHER_STATE like '%@' GROUP BY TABLE_SERVICES_DIRECTORY.SERVICE_ID",state_Id,otherState];
        
    }
    
    NSArray *arry=[UMSqliteManager executeQuery:query];
    
    return arry;
}




-(NSString*)getStateCodeEnglish:(NSString*)stateName
{
    
    NSDictionary* dummyDictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"StateData" ofType:@"plist"]];
    
    
    
    NSArray* sortedCategories = [dummyDictionary.allKeys sortedArrayUsingDescriptors:nil];
    
    
    
    
    NSString *categoryName;
    NSArray *currentCategory;
    
    NSMutableArray *  statelocalPlist=[[NSMutableArray alloc]init];
    
    
    for (int i = 0; i < [dummyDictionary.allKeys count]; i++)
    {
        
        categoryName = [sortedCategories objectAtIndex:i];
        currentCategory = [dummyDictionary objectForKey:categoryName];
        [statelocalPlist addObject:currentCategory];
        
        
        
        
    }
    NSLog(@"statelocalPlist=%@",statelocalPlist);
    
    
    NSArray *arry=[NSArray arrayWithArray:statelocalPlist];
    NSArray *resultArray = [arry filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"State_NAME == %@", stateName]];
    NSString *state_code=@"";
    
    if([resultArray count]>0)
    {
        state_code=[[resultArray objectAtIndex:0] valueForKey:@"State_ID"];
        
    }
    if ([state_code length]==0) {
        state_code=@"";
    }
    return state_code;
}








-(NSString*)getStateEnglishName:(NSString*)state_id
{
    
    NSDictionary* dummyDictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"StateData" ofType:@"plist"]];
    
    
    
    NSArray* sortedCategories = [dummyDictionary.allKeys sortedArrayUsingDescriptors:nil];
    
    
    
    
    NSString *categoryName;
    NSArray *currentCategory;
    
    NSMutableArray *  statelocalPlist=[[NSMutableArray alloc]init];
    
    
    for (int i = 0; i < [dummyDictionary.allKeys count]; i++)
    {
        
        categoryName = [sortedCategories objectAtIndex:i];
        currentCategory = [dummyDictionary objectForKey:categoryName];
        [statelocalPlist addObject:currentCategory];
        
        
        
        
    }
    NSLog(@"statelocalPlist=%@",statelocalPlist);
    
    
    NSArray *arry=[NSArray arrayWithArray:statelocalPlist];
    NSArray *resultArray = [arry filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"State_ID == %@", state_id]];
    NSString *stateName=@"";
    
    if([resultArray count]>0)
    {
        stateName=[[resultArray objectAtIndex:0] valueForKey:@"State_NAME"];
        
    }
    if ([stateName length]==0) {
        stateName=@"";
    }
    return stateName;
}




-(NSArray*)getServiceStateAvailableInEnglishOnly
{
    NSMutableArray *stateArray=[NSMutableArray new];
    NSMutableArray *statePresentNameArray=[NSMutableArray new];
    NSArray* arrayOfstateOtherString;
    //get array of other state column
    NSString *stateOtherString=[[self getAllServiceOtherStatePresent] mutableCopy];
    if(stateOtherString.length !=0)
    {
        arrayOfstateOtherString  = [stateOtherString componentsSeparatedByString:@"|"];
    }
    //get array of  state column
    stateArray= [[self getServiceStatePresent] mutableCopy];
    
    //merge both column
    for (int i=0; i<[arrayOfstateOtherString count]; i++) {
        [stateArray addObject:[arrayOfstateOtherString objectAtIndex:i]];
    }
    
    
    NSArray *uniqueStateArray = [[NSSet setWithArray:stateArray] allObjects];
    
    
    for(int i=0;i<[uniqueStateArray count];i++)
    {
        NSString *stateId=[NSString stringWithFormat:@"%@",[uniqueStateArray objectAtIndex:i]];
        obj=[[StateList alloc]init];
        
        if([stateId isEqualToString:@"99"])
        {
            //do nothing its central service
        }
        else
        {
            
            NSString *stateName = [self getStateEnglishName:stateId];
            
            if (stateName.length!=0)
            {
                [statePresentNameArray addObject:stateName];
            }
        }
    }
    
    NSLog(@"statePresentNameArray=%@",statePresentNameArray);
    
    
    return statePresentNameArray;
    
    
}


-(NSArray*)getServiceStateAvailable
{
    NSMutableArray *stateArray=[NSMutableArray new];
    NSMutableArray *statePresentNameArray=[NSMutableArray new];
    NSArray* arrayOfstateOtherString;
    //get array of other state column
    NSString *stateOtherString=[[self getAllServiceOtherStatePresent] mutableCopy];
    if(stateOtherString.length !=0)
    {
        arrayOfstateOtherString  = [stateOtherString componentsSeparatedByString:@"|"];
    }
    //get array of  state column
    stateArray= [[self getServiceStatePresent] mutableCopy];
    
    //merge both column
    for (int i=0; i<[arrayOfstateOtherString count]; i++) {
        [stateArray addObject:[arrayOfstateOtherString objectAtIndex:i]];
    }
    
    
    NSArray *uniqueStateArray = [[NSSet setWithArray:stateArray] allObjects];
    
    
    for(int i=0;i<[uniqueStateArray count];i++)
    {
        NSString *stateId=[NSString stringWithFormat:@"%@",[uniqueStateArray objectAtIndex:i]];
        obj=[[StateList alloc]init];
        
        if([stateId isEqualToString:@"99"])
        {
            //do nothing its central service
        }
        else
        {
            
            NSString *stateName = [obj getStateName:stateId];
            
            if (stateName.length!=0)
            {
                [statePresentNameArray addObject:stateName];
            }
        }
    }
    
    NSLog(@"statePresentNameArray=%@",statePresentNameArray);
    
    
    return statePresentNameArray;
    
    
}
//fetch list of all service other stat its will be pipe seperate array need to make it single
-(NSString*)getAllServiceOtherStatePresent
{
    NSString *trimmedString =@"";
    NSString *query;
    query = [NSString stringWithFormat:@"SELECT DISTINCT SERVICE_OTHER_STATE FROM 'TABLE_SERVICES_DATA' where length(SERVICE_OTHER_STATE)!=0   ORDER BY SERVICE_OTHER_STATE"];
    NSArray *arry=[UMSqliteManager executeQuery:query];
    if ([arry count]!=0) {
        NSMutableArray *tempArray=[NSMutableArray new];
        for (int i=0; i<[arry count]; i++)
        {
            NSString*  stringState=[[arry objectAtIndex:i]valueForKey:@"SERVICE_OTHER_STATE"];
            [tempArray addObject:stringState];
        }
        NSLog(@"value of tempArray=%@",tempArray);
        if ([tempArray count]!=0)
        {
            NSString * tempstr = [tempArray componentsJoinedByString:@""];
            trimmedString = [tempstr stringByReplacingOccurrencesOfString:@"||" withString:@"|"];
            if ([trimmedString length] > 0) {
                trimmedString = [trimmedString substringFromIndex:1];
                trimmedString = [trimmedString substringToIndex:[trimmedString length] - 1];
            } else
            {
                
            }
            NSLog(@"value of trimmedString=%@",trimmedString);
        }
    }
    return trimmedString;
}

-(NSMutableArray*)getServiceStatePresent
{
    NSMutableArray *tempArray=[NSMutableArray new];
    NSString *query;
    query = [NSString stringWithFormat:@"SELECT DISTINCT SERVICE_STATE FROM 'TABLE_SERVICES_DATA'  ORDER BY SERVICE_STATE"];
    NSArray *arry=[UMSqliteManager executeQuery:query];
    
    if ([arry count]!=0) {
        NSMutableArray *tempArray=[NSMutableArray new];
        for (int i=0; i<[arry count]; i++)
        {
            NSString*  stringState=[[arry objectAtIndex:i]valueForKey:@"SERVICE_STATE"];
            [tempArray addObject:stringState];
        }
        NSLog(@"value of tempArray=%@",tempArray);
        return tempArray;
        
    }
    return tempArray;
    
}


//getUpcommingStates remaining States
-(NSArray*)getUpcommingStates
{
    singleton = [SharedManager sharedSingleton];
    NSMutableArray *upcommingState = [NSMutableArray new];
    NSArray *statePresentArray=[singleton.dbManager getServiceStateAvailable];
    NSMutableArray *tempStatePresentArray = [NSMutableArray new];
    
    NSArray *arry_state = [singleton.statesList mutableCopy];
    
    NSMutableArray *allStateArray = [NSMutableArray new];
    
    for (int i=0; i<[arry_state count]; i++)
    {
        NSString *stateId=[NSString stringWithFormat:@"%@",[[arry_state objectAtIndex:i]valueForKey:@"stateId"]];
        
        if (stateId.length!=0 )
        {
            [allStateArray addObject:stateId];
        }
    }
    
    
    for (int i=0; i<[statePresentArray count]; i++)
    {
        NSString *stateName=[NSString stringWithFormat:@"%@",[statePresentArray objectAtIndex:i]];
        
        if (stateName.length!=0 )
        {
            obj=[[StateList alloc]init];
            NSString *getStateId = [obj getStateCode:stateName];
            [tempStatePresentArray addObject:getStateId];
        }
    }
    
    
    
    
    
    
    [allStateArray removeObjectsInArray:tempStatePresentArray];
    
    
    //[tempStatePresentArray removeObjectsInArray:allStateArray];
    NSLog(@"allStateArray=%@",allStateArray);
    
    NSMutableArray *tempStateArray=[allStateArray mutableCopy];
    for (int i=0; i<[tempStateArray count]; i++)
    {
        NSString *stateId=[NSString stringWithFormat:@"%@",[tempStateArray objectAtIndex:i]];
        if([stateId isEqualToString:@"99"])
        {
            //Central Services
        }
        else
        {
            obj=[[StateList alloc]init];
            NSString *stateName = [obj getStateName:stateId];
            
            if (stateName.length!=0)
            {
                [upcommingState addObject:stateName];
            }
        }
    }
    return upcommingState;
}







//----------------------------------------------------------
//          insertServicesData TABLE_SERVICES_DATA
//----------------------------------------------------


/*
 -(void)insertServicesData:(NSString*)serviceId
 serviceName:(NSString*)serviceName
 serviceDesc:(NSString*)serviceDesc
 serviceImg:(NSString*)serviceImg
 serviceCategory:(NSString*)serviceCategory
 serviceSubCat:(NSString*)serviceSubCat
 serviceRating:(NSString*)serviceRating
 serviceUrl:(NSString*)serviceUrl
 serviceState:(NSString*)serviceState
 serviceLat:(NSString*)serviceLat
 serviceLng:(NSString*)serviceLng
 serviceIsFav:(NSString*)serviceIsFav
 serviceIsHidden:(NSString*)serviceIsHidden
 servicePhoneNumber:(NSString*)servicePhoneNumber
 serviceisNotifEnabled:(NSString*)serviceisNotifEnabled
 serviceWebsite:(NSString*)serviceWebsite
 servicelang:(NSString*)servicelang
 servicedeptAddress:(NSString*)servicedeptAddress
 serviceworkingHours:(NSString*)serviceworkingHours
 servicedeptDescription:(NSString*)servicedeptDescription
 serviceemail:(NSString*)serviceemail
 popularity:(NSString*)popularity
 servicecategoryId:(NSString*)servicecategoryId
 serviceOtherState:(NSString*)serviceOtherState
 
 
 {
 @try {
 NSString *word = @"'";
 if ([serviceDesc rangeOfString:word].location != NSNotFound)
 {
 NSLog(@"Yes it does contain ' word");
 NSLog(@"Before serviceDesc=%@",serviceDesc);
 
 serviceDesc = [serviceDesc stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
 NSLog(@"serviceDesc=%@",serviceDesc);
 
 }
 else
 {
 NSLog(@"NO it does contain ' word");
 
 }
 
 // Prepare the query string.
 NSString *query;
 query = [NSString stringWithFormat:@"insert into TABLE_SERVICES_DATA values(null, '%@', '%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')", serviceId,serviceName,serviceDesc, serviceImg,serviceCategory,serviceSubCat,serviceRating,serviceUrl,serviceState,serviceLat,serviceLng,serviceIsFav,serviceIsHidden,servicePhoneNumber, serviceisNotifEnabled,serviceWebsite, servicedeptAddress, serviceworkingHours, servicedeptDescription, servicelang ,serviceemail,popularity,servicecategoryId,serviceOtherState];
 
 
 
 NSLog(@"insert into TABLE_SERVICES_DATA values(null, '%@', '%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')", serviceId,serviceName,serviceDesc, serviceImg,serviceCategory,serviceSubCat,serviceRating,serviceUrl,serviceState,serviceLat,serviceLng,serviceIsFav,serviceIsHidden,servicePhoneNumber, serviceisNotifEnabled,serviceWebsite, servicedeptAddress, serviceworkingHours, servicedeptDescription, servicelang ,serviceemail,popularity,servicecategoryId,serviceOtherState);
 
 // SERVICE_OTHER_STATE
 
 
 if ([UMSqliteManager executeScalarQuery:query]==YES)
 {
 
 NSLog(@"Data  inserted successfully");
 
 }else{
 NSLog(@"Data not inserted successfully");
 }
 
 } @catch (NSException *exception) {
 
 } @finally {
 
 }
 
 }
 */

// Method to check Contain single quote or not and return replace single quote with  double quote to insert in it
-(NSString*)replaceSingleQuote:(NSString*)textToCheck
{
    NSString *word = @"'";
    
    NSString *inputText=[NSString stringWithFormat:@"%@",textToCheck];
    if ([inputText rangeOfString:word].location != NSNotFound)
    {
        NSLog(@"Yes it does contain ' word");
        NSLog(@"Before inputText=%@",inputText);
        inputText = [inputText stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
        NSLog(@"inputText=%@",inputText);
        
    }
    else
    {
        NSLog(@"NO it does contain ' word");
        
    }
    return inputText;
}

-(void)insertServicesData:(NSString*)serviceId
              serviceName:(NSString*)serviceName
              serviceDesc:(NSString*)serviceDesc
               serviceImg:(NSString*)serviceImg
          serviceCategory:(NSString*)serviceCategory
            serviceSubCat:(NSString*)serviceSubCat
            serviceRating:(NSString*)serviceRating
               serviceUrl:(NSString*)serviceUrl
             serviceState:(NSString*)serviceState
               serviceLat:(NSString*)serviceLat
               serviceLng:(NSString*)serviceLng
             serviceIsFav:(NSString*)serviceIsFav
          serviceIsHidden:(NSString*)serviceIsHidden
       servicePhoneNumber:(NSString*)servicePhoneNumber
    serviceisNotifEnabled:(NSString*)serviceisNotifEnabled
           serviceWebsite:(NSString*)serviceWebsite
              servicelang:(NSString*)servicelang
       servicedeptAddress:(NSString*)servicedeptAddress
      serviceworkingHours:(NSString*)serviceworkingHours
   servicedeptDescription:(NSString*)servicedeptDescription
             serviceemail:(NSString*)serviceemail
               popularity:(NSString*)popularity
        servicecategoryId:(NSString*)servicecategoryId
        serviceOtherState:(NSString*)serviceOtherState
             otherwebsite:(NSString*)otherwebsite;


{//serviceName
    @try {
        
        
        
        
        serviceName =[self replaceSingleQuote:serviceName];
        serviceDesc =[self replaceSingleQuote:serviceDesc];
        serviceCategory =[self replaceSingleQuote:serviceCategory];
        serviceSubCat =[self replaceSingleQuote:serviceSubCat];
        servicedeptAddress =[self replaceSingleQuote:servicedeptAddress];
        servicedeptDescription =[self replaceSingleQuote:servicedeptDescription];
        serviceworkingHours =[self replaceSingleQuote:serviceworkingHours];
        
        
        
        /*    NSString *word = @"'";
         if ([serviceDesc rangeOfString:word].location != NSNotFound)
         {
         NSLog(@"Yes it does contain ' word");
         NSLog(@"Before serviceDesc=%@",serviceDesc);
         
         serviceDesc = [serviceDesc stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
         NSLog(@"serviceDesc=%@",serviceDesc);
         
         }
         else
         {
         NSLog(@"NO it does contain ' word");
         NSLog(@"after serviceDesc=%@",serviceDesc);
         
         }
         
         
         
         if ([serviceName rangeOfString:word].location != NSNotFound)
         {
         NSLog(@"Yes it does contain ' word");
         NSLog(@"Before serviceName=%@",serviceName);
         
         serviceName = [serviceName stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
         NSLog(@"serviceName=%@",serviceName);
         
         }
         else
         {
         NSLog(@"NO it does contain ' word");
         NSLog(@"after serviceName=%@",serviceName);
         
         }
         
         */
        
        // Prepare the query string.
        NSString *query;
        query = [NSString stringWithFormat:@"insert into TABLE_SERVICES_DATA values(null, '%@', '%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')", serviceId,serviceName,serviceDesc, serviceImg,serviceCategory,serviceSubCat,serviceRating,serviceUrl,serviceState,serviceLat,serviceLng,serviceIsFav,serviceIsHidden,servicePhoneNumber, serviceisNotifEnabled,serviceWebsite, servicedeptAddress, serviceworkingHours, servicedeptDescription, servicelang ,serviceemail,popularity,servicecategoryId,serviceOtherState,otherwebsite];
        
        
        
        NSLog(@"insert into TABLE_SERVICES_DATA values(null, '%@', '%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')", serviceId,serviceName,serviceDesc, serviceImg,serviceCategory,serviceSubCat,serviceRating,serviceUrl,serviceState,serviceLat,serviceLng,serviceIsFav,serviceIsHidden,servicePhoneNumber, serviceisNotifEnabled,serviceWebsite, servicedeptAddress, serviceworkingHours, servicedeptDescription, servicelang ,serviceemail,popularity,servicecategoryId,serviceOtherState,otherwebsite);
        
        // SERVICE_OTHER_STATE
        
        
        // if ([UMSqliteManager executeScalarQuery:query]==YES)
        if ([UMSqliteManager executeScalarInsertServiceQuery:query]==YES)
        {
            
            NSLog(@"Data  inserted successfully");
            
        }else{
            NSLog(@"Data not inserted successfully");
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
}


//create seprate method for Insert Query
+(BOOL)executeScalarInsertServiceQuery:(NSString*)str
{
    
    sqlite3_stmt *statement= nil;
    sqlite3 *database;
    
    NSLog(@"executeScalarInsertServiceQuery is called =%@",str);
    
    @synchronized(self)
    {
        @try
        {
            
            BOOL fRet = NO;
            
            NSString *docsDir;
            
            NSArray *dirPaths;
            
            //sqlite3 *DB;
            
            dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            
            docsDir = [dirPaths objectAtIndex:0];
            
            NSString*  strPath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"UMANG_DATABASE.db"]];
            
            const char *fileName = [strPath UTF8String];
            
            NSLog(@"path 00%s", fileName);
            
            
            if (sqlite3_open(fileName,&database) == SQLITE_OK)
            {
                
                if (sqlite3_prepare_v2(database, [str UTF8String], -1, &statement, NULL) == SQLITE_OK)
                {
                    
                    if (sqlite3_step(statement) == SQLITE_DONE)
                    {
                        fRet =YES;
                    }
                    else
                    {
                        NSLog(@"VVVVVVVVVVVVVVVVVVV  FAIL QUERY=%@ strPath=%@ ",str,strPath);
                        NSLog(@"VVVVVVVVVVVVVVVVVVV error: %s", sqlite3_errmsg(database));
                        
                        [UMSqliteManager executeScalarQuery:str];
                        
                    }
                    sqlite3_finalize(statement);
                }
                else
                {
                    NSLog(@"VVVVVVVVVVVVVVVVVVV FAIL QUERY Problem with prepare statement: %s", sqlite3_errmsg(database));
                    
                    [UMSqliteManager executeScalarQuery:str];
                }
            }
            else
            {
                
                NSLog(@"VVVVVVVVVVVVVVVVVVV FAIL QUERY An error has occured: %s",sqlite3_errmsg(database));
                
                [UMSqliteManager executeScalarQuery:str];
                
            }
            
            // sqlite3_finalize(statement);
            
            sqlite3_close(database);
            
            return fRet;
            
        } @catch (NSException *exception)
        {
            NSLog(@"VVVVVVVVVVVVVVVVVVV FAIL QUERY exception=%@",exception);
            
            sqlite3_close(database);
            
        } @finally
        {
            NSLog(@"VVVVVVVVVVVVVVVVVVV executeScalarQuery finally (%s)", sqlite3_errmsg(database));
        }
        
    }
    
}



//----------------------------------------------------------
//          updateServicesData TABLE_SERVICES_DATA
//----------------------------------------------------------

/*
 -(void)updateServicesData:(NSString*)serviceId
 serviceName:(NSString*)serviceName
 serviceDesc:(NSString*)serviceDesc
 serviceImg:(NSString*)serviceImg
 serviceCategory:(NSString*)serviceCategory
 serviceSubCat:(NSString*)serviceSubCat
 serviceRating:(NSString*)serviceRating
 serviceUrl:(NSString*)serviceUrl
 serviceState:(NSString*)serviceState
 serviceLat:(NSString*)serviceLat
 serviceLng:(NSString*)serviceLng
 serviceIsFav:(NSString*)serviceIsFav
 serviceIsHidden:(NSString*)serviceIsHidden
 servicePhoneNumber:(NSString*)servicePhoneNumber
 serviceisNotifEnabled:(NSString*)serviceisNotifEnabled
 serviceWebsite:(NSString*)serviceWebsite
 servicelang:(NSString*)servicelang
 servicedeptAddress:(NSString*)servicedeptAddress
 serviceworkingHours:(NSString*)serviceworkingHours
 servicedeptDescription:(NSString*)servicedeptDescription
 serviceemail:(NSString*)serviceemail
 popularity:(NSString*)popularity
 servicecategoryId:(NSString*)servicecategoryId
 serviceOtherState:(NSString*)serviceOtherState
 
 
 {
 
 
 
 
 NSString *word = @"'";
 if ([serviceDesc rangeOfString:word].location != NSNotFound)
 {
 NSLog(@"Yes it does contain ' word");
 NSLog(@"Before serviceDesc=%@",serviceDesc);
 
 serviceDesc = [serviceDesc stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
 NSLog(@"serviceDesc=%@",serviceDesc);
 
 }
 else
 {
 NSLog(@"NO it does contain ' word");
 
 }
 
 
 
 // Prepare the query string.
 NSString *query;
 
 
 
 query = [NSString stringWithFormat:@"update TABLE_SERVICES_DATA  set SERVICE_NAME='%@', SERVICE_DESC='%@', SERVICE_IMAGE='%@', SERVICE_CATEGORY='%@', SERVICE_SUB_CATEGORY='%@', SERVICE_RATING='%@', SERVICE_URL='%@', SERVICE_STATE='%@', SERVICE_LATITUDE='%@', SERVICE_LONGITUDE='%@', SERVICE_IS_FAV='%@', SERVICE_IS_HIDDEN='%@', SERVICE_PHONE_NUMBER='%@',SERVICE_IS_NOTIF_ENABLED='%@', SERVICE_WEBSITE='%@',SERVICE_DEPTADDRESS ='%@', SERVICE_WORKINGHOURS ='%@', SERVICE_DEPTDESCRIPTION ='%@', SERVICE_LANG ='%@', SERVICE_EMAIL ='%@', SERVICE_POPULARITY ='%@',SERVICE_CATEGORY_ID ='%@', SERVICE_OTHER_STATE='%@' where SERVICE_ID='%@'",serviceName,serviceDesc,serviceImg,serviceCategory,serviceSubCat,serviceRating,serviceUrl,serviceState,serviceLat,serviceLng,serviceIsFav,serviceIsHidden,servicePhoneNumber,serviceisNotifEnabled,serviceWebsite, servicedeptAddress, serviceworkingHours, servicedeptDescription, servicelang, serviceemail,popularity,servicecategoryId,serviceOtherState,serviceId];
 
 // SERVICE_OTHER_STATE
 
 
 
 NSLog(@"update TABLE_SERVICES_DATA  set SERVICE_NAME='%@', SERVICE_DESC='%@', SERVICE_IMAGE='%@', SERVICE_CATEGORY='%@', SERVICE_SUB_CATEGORY='%@', SERVICE_RATING='%@', SERVICE_URL='%@', SERVICE_STATE='%@', SERVICE_LATITUDE='%@', SERVICE_LONGITUDE='%@', SERVICE_IS_FAV='%@', SERVICE_IS_HIDDEN='%@', SERVICE_PHONE_NUMBER='%@',SERVICE_IS_NOTIF_ENABLED='%@', SERVICE_WEBSITE='%@',SERVICE_DEPTADDRESS ='%@', SERVICE_WORKINGHOURS ='%@', SERVICE_DEPTDESCRIPTION ='%@', SERVICE_LANG ='%@', SERVICE_EMAIL ='%@', SERVICE_POPULARITY ='%@',SERVICE_CATEGORY_ID ='%@' ,SERVICE_OTHER_STATE='%@' where SERVICE_ID='%@'",serviceName,serviceDesc,serviceImg,serviceCategory,serviceSubCat,serviceRating,serviceUrl,serviceState,serviceLat,serviceLng,serviceIsFav,serviceIsHidden,servicePhoneNumber,serviceisNotifEnabled,serviceWebsite, servicedeptAddress, serviceworkingHours, servicedeptDescription, servicelang, serviceemail,popularity,servicecategoryId,serviceOtherState,serviceId);
 
 
 if ([UMSqliteManager executeScalarQuery:query]==YES)
 {
 
 NSLog(@"Data  inserted successfully");
 
 }else{
 NSLog(@"Data not inserted successfully");
 }
 
 }
 */


-(void)updateServicesData:(NSString*)serviceId
              serviceName:(NSString*)serviceName
              serviceDesc:(NSString*)serviceDesc
               serviceImg:(NSString*)serviceImg
          serviceCategory:(NSString*)serviceCategory
            serviceSubCat:(NSString*)serviceSubCat
            serviceRating:(NSString*)serviceRating
               serviceUrl:(NSString*)serviceUrl
             serviceState:(NSString*)serviceState
               serviceLat:(NSString*)serviceLat
               serviceLng:(NSString*)serviceLng
             serviceIsFav:(NSString*)serviceIsFav
          serviceIsHidden:(NSString*)serviceIsHidden
       servicePhoneNumber:(NSString*)servicePhoneNumber
    serviceisNotifEnabled:(NSString*)serviceisNotifEnabled
           serviceWebsite:(NSString*)serviceWebsite
              servicelang:(NSString*)servicelang
       servicedeptAddress:(NSString*)servicedeptAddress
      serviceworkingHours:(NSString*)serviceworkingHours
   servicedeptDescription:(NSString*)servicedeptDescription
             serviceemail:(NSString*)serviceemail
               popularity:(NSString*)popularity
        servicecategoryId:(NSString*)servicecategoryId
        serviceOtherState:(NSString*)serviceOtherState
             otherwebsite:(NSString*)otherwebsite;


{
    
    serviceName =[self replaceSingleQuote:serviceName];
    serviceDesc =[self replaceSingleQuote:serviceDesc];
    serviceCategory =[self replaceSingleQuote:serviceCategory];
    serviceSubCat =[self replaceSingleQuote:serviceSubCat];
    servicedeptAddress =[self replaceSingleQuote:servicedeptAddress];
    servicedeptDescription =[self replaceSingleQuote:servicedeptDescription];
    serviceworkingHours =[self replaceSingleQuote:serviceworkingHours];
    
    /*
     
     
     NSString *word = @"'";
     if ([serviceDesc rangeOfString:word].location != NSNotFound)
     {
     NSLog(@"Yes it does contain ' word");
     NSLog(@"Before serviceDesc=%@",serviceDesc);
     
     serviceDesc = [serviceDesc stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
     NSLog(@"serviceDesc=%@",serviceDesc);
     
     }
     else
     {
     NSLog(@"NO it does contain ' word");
     
     }
     
     
     if ([serviceName rangeOfString:word].location != NSNotFound)
     {
     NSLog(@"Yes it does contain ' word");
     NSLog(@"Before serviceName=%@",serviceName);
     
     serviceName = [serviceName stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
     NSLog(@"serviceName=%@",serviceName);
     
     }
     else
     {
     NSLog(@"NO it does contain ' word");
     NSLog(@"after serviceName=%@",serviceName);
     
     }
     
     */
    
    // Prepare the query string.
    NSString *query;
    
    
    
    query = [NSString stringWithFormat:@"update TABLE_SERVICES_DATA  set SERVICE_NAME='%@', SERVICE_DESC='%@', SERVICE_IMAGE='%@', SERVICE_CATEGORY='%@', SERVICE_SUB_CATEGORY='%@', SERVICE_RATING='%@', SERVICE_URL='%@', SERVICE_STATE='%@', SERVICE_LATITUDE='%@', SERVICE_LONGITUDE='%@', SERVICE_IS_FAV='%@', SERVICE_IS_HIDDEN='%@', SERVICE_PHONE_NUMBER='%@',SERVICE_IS_NOTIF_ENABLED='%@', SERVICE_WEBSITE='%@',SERVICE_DEPTADDRESS ='%@', SERVICE_WORKINGHOURS ='%@', SERVICE_DEPTDESCRIPTION ='%@', SERVICE_LANG ='%@', SERVICE_EMAIL ='%@', SERVICE_POPULARITY ='%@',SERVICE_CATEGORY_ID ='%@', SERVICE_OTHER_STATE='%@', OTHER_WEBSITE = '%@' where SERVICE_ID='%@' ",serviceName,serviceDesc,serviceImg,serviceCategory,serviceSubCat,serviceRating,serviceUrl,serviceState,serviceLat,serviceLng,serviceIsFav,serviceIsHidden,servicePhoneNumber,serviceisNotifEnabled,serviceWebsite, servicedeptAddress, serviceworkingHours, servicedeptDescription, servicelang, serviceemail,popularity,servicecategoryId,serviceOtherState,otherwebsite,serviceId];
    
    // SERVICE_OTHER_STATE
    
    
    
    NSLog(@"update TABLE_SERVICES_DATA  set SERVICE_NAME='%@', SERVICE_DESC='%@', SERVICE_IMAGE='%@', SERVICE_CATEGORY='%@', SERVICE_SUB_CATEGORY='%@', SERVICE_RATING='%@', SERVICE_URL='%@', SERVICE_STATE='%@', SERVICE_LATITUDE='%@', SERVICE_LONGITUDE='%@', SERVICE_IS_FAV='%@', SERVICE_IS_HIDDEN='%@', SERVICE_PHONE_NUMBER='%@',SERVICE_IS_NOTIF_ENABLED='%@', SERVICE_WEBSITE='%@',SERVICE_DEPTADDRESS ='%@', SERVICE_WORKINGHOURS ='%@', SERVICE_DEPTDESCRIPTION ='%@', SERVICE_LANG ='%@', SERVICE_EMAIL ='%@', SERVICE_POPULARITY ='%@',SERVICE_CATEGORY_ID ='%@' ,SERVICE_OTHER_STATE='%@', OTHER_WEBSITE= '%@' where SERVICE_ID='%@'",serviceName,serviceDesc,serviceImg,serviceCategory,serviceSubCat,serviceRating,serviceUrl,serviceState,serviceLat,serviceLng,serviceIsFav,serviceIsHidden,servicePhoneNumber,serviceisNotifEnabled,serviceWebsite, servicedeptAddress, serviceworkingHours, servicedeptDescription, servicelang, serviceemail,popularity,servicecategoryId,serviceOtherState,serviceId,otherwebsite);
    
    
    if ([UMSqliteManager executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
}


//----------------------------------------------------------
//          insertServiceDir TABLE_SERVICES_DIRECTORY
//----------------------------------------------------------

/*
 -(void)insertServicesDirData:(NSString*)serviceId
 serviceName:(NSString*)serviceName
 serviceDesc:(NSString*)serviceDesc
 serviceImg:(NSString*)serviceImg
 serviceLat:(NSString*)serviceLat
 serviceLong:(NSString*)serviceLong
 servicePhoneno:(NSString*)servicePhoneno
 serviceWebsite:(NSString*)serviceWebsite
 serviceEmail:(NSString*)serviceEmail
 serviceAddress:(NSString*)serviceAddress
 serviceWorkingHour:(NSString*)serviceWorkingHour
 serviceOtherInfo:(NSString*)serviceOtherInfo
 serviceNativeApp:(NSString*)serviceNativeApp
 isavailable:(NSString*)isavailable
 nativeAppName:(NSString*)nativeAppName
 
 
 
 {
 
 
 @try {
 
 NSString *word = @"'";
 if ([serviceDesc rangeOfString:word].location != NSNotFound)
 {
 NSLog(@"Yes it does contain ' word");
 NSLog(@"Before serviceDesc=%@",serviceDesc);
 
 serviceDesc = [serviceDesc stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
 NSLog(@"serviceDesc=%@",serviceDesc);
 
 }
 else
 {
 NSLog(@"NO it does contain ' word");
 
 }
 // BOOL containsString = [serviceDesc containsString:@"'"];
 
 
 
 NSString *query;
 query = [NSString stringWithFormat:@"insert into TABLE_SERVICES_DIRECTORY values(null,'%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')", serviceId,serviceName,serviceDesc,serviceImg,serviceLat,serviceLong,servicePhoneno,serviceWebsite,serviceEmail,serviceAddress, serviceWorkingHour, serviceOtherInfo, serviceNativeApp,isavailable,nativeAppName];
 
 
 //updateDBifRequired
 if ([UMSqliteManager executeScalarQuery:query]==YES)
 {
 
 NSLog(@"Data  inserted successfully");
 
 }else{
 NSLog(@"Data not inserted successfully");
 }
 
 } @catch (NSException *exception) {
 
 } @finally {
 
 }
 
 }
 */
/*
 -(void)insertServicesDirData:(NSString*)serviceId
 serviceName:(NSString*)serviceName
 serviceDesc:(NSString*)serviceDesc
 serviceImg:(NSString*)serviceImg
 serviceLat:(NSString*)serviceLat
 serviceLong:(NSString*)serviceLong
 servicePhoneno:(NSString*)servicePhoneno
 serviceWebsite:(NSString*)serviceWebsite
 serviceEmail:(NSString*)serviceEmail
 serviceAddress:(NSString*)serviceAddress
 serviceWorkingHour:(NSString*)serviceWorkingHour
 serviceOtherInfo:(NSString*)serviceOtherInfo
 serviceNativeApp:(NSString*)serviceNativeApp
 isavailable:(NSString*)isavailable
 nativeAppName:(NSString*)nativeAppName
 otherwebsite:(NSString*)otherwebsite
 andLink:(NSString*)andLink
 
 
 
 {
 
 
 @try {
 
 NSString *word = @"'";
 if ([serviceDesc rangeOfString:word].location != NSNotFound)
 {
 NSLog(@"Yes it does contain ' word");
 NSLog(@"Before serviceDesc=%@",serviceDesc);
 
 serviceDesc = [serviceDesc stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
 NSLog(@"serviceDesc=%@",serviceDesc);
 
 }
 else
 {
 NSLog(@"NO it does contain ' word");
 
 }
 // BOOL containsString = [serviceDesc containsString:@"'"];
 
 
 // OTHER_WEBSITE
 // AND_LINK
 NSString *query;
 query = [NSString stringWithFormat:@"insert into TABLE_SERVICES_DIRECTORY values(null,'%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')", serviceId,serviceName,serviceDesc,serviceImg,serviceLat,serviceLong,servicePhoneno,serviceWebsite,serviceEmail,serviceAddress, serviceWorkingHour, serviceOtherInfo, serviceNativeApp,isavailable,nativeAppName,otherwebsite,andLink];
 
 
 //updateDBifRequired
 if ([UMSqliteManager executeScalarQuery:query]==YES)
 {
 
 NSLog(@"Data  inserted successfully");
 
 }else{
 NSLog(@"Data not inserted successfully");
 }
 
 } @catch (NSException *exception) {
 
 } @finally {
 
 }
 
 }
 */







-(void)insertServicesDirData:(NSString*)serviceId
                 serviceName:(NSString*)serviceName
                 serviceDesc:(NSString*)serviceDesc
                  serviceImg:(NSString*)serviceImg
                  serviceLat:(NSString*)serviceLat
                 serviceLong:(NSString*)serviceLong
              servicePhoneno:(NSString*)servicePhoneno
              serviceWebsite:(NSString*)serviceWebsite
                serviceEmail:(NSString*)serviceEmail
              serviceAddress:(NSString*)serviceAddress
          serviceWorkingHour:(NSString*)serviceWorkingHour
            serviceOtherInfo:(NSString*)serviceOtherInfo
            serviceNativeApp:(NSString*)serviceNativeApp
                 isavailable:(NSString*)isavailable
               nativeAppName:(NSString*)nativeAppName
                otherwebsite:(NSString*)otherwebsite
                     andLink:(NSString*)andLink
                  categoryid:(NSString*)categoryid
                     stateid:(NSString*)stateid
                  otherstate:(NSString*)otherstate
                categoryname:(NSString*)categoryname


/*
 adding below parameter
 "categoryid": "13", CATEGORY_ID
 "stateid": "99", STATE_ID
 "otherstate": "", OTHER_STATE
 "categoryname": "Utility" CATEGORY_NAME
 
 */


{
    
    
    @try {
        
        
        
        serviceName =[self replaceSingleQuote:serviceName];
        serviceDesc =[self replaceSingleQuote:serviceDesc];
        serviceAddress =[self replaceSingleQuote:serviceAddress];
        serviceOtherInfo =[self replaceSingleQuote:serviceOtherInfo];
        serviceNativeApp =[self replaceSingleQuote:serviceNativeApp];
        nativeAppName =[self replaceSingleQuote:nativeAppName];
        categoryname =[self replaceSingleQuote:categoryname];
        
        /*
         NSString *word = @"'";
         if ([serviceDesc rangeOfString:word].location != NSNotFound)
         {
         NSLog(@"Yes it does contain ' word");
         NSLog(@"Before serviceDesc=%@",serviceDesc);
         
         serviceDesc = [serviceDesc stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
         NSLog(@"serviceDesc=%@",serviceDesc);
         
         }
         else
         {
         NSLog(@"NO it does contain ' word");
         
         }*/
        // BOOL containsString = [serviceDesc containsString:@"'"];
        
        
        // OTHER_WEBSITE
        // AND_LINK
        NSString *query;
        query = [NSString stringWithFormat:@"insert into TABLE_SERVICES_DIRECTORY values(null,'%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')", serviceId,serviceName,serviceDesc,serviceImg,serviceLat,serviceLong,servicePhoneno,serviceWebsite,serviceEmail,serviceAddress, serviceWorkingHour, serviceOtherInfo, serviceNativeApp,isavailable,nativeAppName,otherwebsite,andLink, categoryid,stateid,otherstate,categoryname];
        
        
        //updateDBifRequired
        if ([UMSqliteManager executeScalarQuery:query]==YES)
        {
            
            NSLog(@"Data  inserted successfully");
            
        }else{
            NSLog(@"Data not inserted successfully");
        }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
}


//----------------------------------------------------------
//           updateServicesDirData TABLE_SERVICES_DIRECTORY
//----------------------------------------------------------


-(void)updateServicesDirData:(NSString*)serviceId
                 serviceName:(NSString*)serviceName
                 serviceDesc:(NSString*)serviceDesc
                  serviceImg:(NSString*)serviceImg
                  serviceLat:(NSString*)serviceLat
                 serviceLong:(NSString*)serviceLong
              servicePhoneno:(NSString*)servicePhoneno
              serviceWebsite:(NSString*)serviceWebsite
                serviceEmail:(NSString*)serviceEmail
              serviceAddress:(NSString*)serviceAddress
          serviceWorkingHour:(NSString*)serviceWorkingHour
            serviceOtherInfo:(NSString*)serviceOtherInfo
            serviceNativeApp:(NSString*)serviceNativeApp
                 isavailable:(NSString*)isavailable
               nativeAppName:(NSString*)nativeAppName
                otherwebsite:(NSString*)otherwebsite
                     andLink:(NSString*)andLink
                  categoryid:(NSString*)categoryid
                     stateid:(NSString*)stateid
                  otherstate:(NSString*)otherstate
                categoryname:(NSString*)categoryname


{
    
    serviceName =[self replaceSingleQuote:serviceName];
    serviceDesc =[self replaceSingleQuote:serviceDesc];
    serviceAddress =[self replaceSingleQuote:serviceAddress];
    serviceOtherInfo =[self replaceSingleQuote:serviceOtherInfo];
    serviceNativeApp =[self replaceSingleQuote:serviceNativeApp];
    nativeAppName =[self replaceSingleQuote:nativeAppName];
    categoryname =[self replaceSingleQuote:categoryname];
    
    /* NSString *word = @"'";
     if ([serviceDesc rangeOfString:word].location != NSNotFound)
     {
     NSLog(@"Yes it does contain ' word");
     NSLog(@"Before serviceDesc=%@",serviceDesc);
     
     serviceDesc = [serviceDesc stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
     NSLog(@"serviceDesc=%@",serviceDesc);
     
     }
     else
     {
     NSLog(@"NO it does contain ' word");
     
     }
     if ([serviceName rangeOfString:word].location != NSNotFound)
     {
     NSLog(@"Yes it does contain ' word");
     NSLog(@"Before serviceName=%@",serviceName);
     
     serviceName = [serviceName stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
     NSLog(@"serviceName=%@",serviceName);
     
     }
     else
     {
     NSLog(@"NO it does contain ' word");
     NSLog(@"after serviceName=%@",serviceName);
     
     }
     */
    
    
    
    
    
    // Prepare the query string.
    NSString *query;
    
    /*
     adding below parameter
     "categoryid": "13", CATEGORY_ID
     "stateid": "99", STATE_ID
     "otherstate": "", OTHER_STATE
     "categoryname": "Utility" CATEGORY_NAME
     
     */
    
    query = [NSString stringWithFormat:@"update TABLE_SERVICES_DIRECTORY  set SERVICE_NAME='%@', SERVICE_DESC='%@', SERVICE_IMAGE='%@', SERVICE_LATITUDE='%@', SERVICE_LONGITUDE='%@', SERVICE_PHONE_NUMBER='%@', SERVICE_WEBSITE='%@', SERVICE_EMAIL='%@', SERVICE_ADDRESS='%@', SERVICE_WORKING_HOURS='%@', SERVICE_OTHER_INFO='%@', SERVICE_NATIVE_APP='%@', SERVICE_IS_AVAILABLE='%@', SERVICE_NATIVE_APPNAME='%@', OTHER_WEBSITE='%@', AND_LINK='%@', CATEGORY_ID='%@', STATE_ID='%@', OTHER_STATE='%@', CATEGORY_NAME='%@' where SERVICE_ID='%@'",serviceName,serviceDesc,serviceImg,serviceLat,serviceLong,servicePhoneno,serviceWebsite,serviceEmail,serviceAddress, serviceWorkingHour, serviceOtherInfo, serviceNativeApp,isavailable,nativeAppName,otherwebsite,andLink,categoryid,stateid,otherstate,categoryname,serviceId];
    
    
    
    
    if ([UMSqliteManager executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
}




/*
 -(void)updateServicesDirData:(NSString*)serviceId
 serviceName:(NSString*)serviceName
 serviceDesc:(NSString*)serviceDesc
 serviceImg:(NSString*)serviceImg
 serviceLat:(NSString*)serviceLat
 serviceLong:(NSString*)serviceLong
 servicePhoneno:(NSString*)servicePhoneno
 serviceWebsite:(NSString*)serviceWebsite
 serviceEmail:(NSString*)serviceEmail
 serviceAddress:(NSString*)serviceAddress
 serviceWorkingHour:(NSString*)serviceWorkingHour
 serviceOtherInfo:(NSString*)serviceOtherInfo
 serviceNativeApp:(NSString*)serviceNativeApp
 isavailable:(NSString*)isavailable
 nativeAppName:(NSString*)nativeAppName
 otherwebsite:(NSString*)otherwebsite
 andLink:(NSString*)andLink
 
 {
 
 NSString *word = @"'";
 if ([serviceDesc rangeOfString:word].location != NSNotFound)
 {
 NSLog(@"Yes it does contain ' word");
 NSLog(@"Before serviceDesc=%@",serviceDesc);
 
 serviceDesc = [serviceDesc stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
 NSLog(@"serviceDesc=%@",serviceDesc);
 
 }
 else
 {
 NSLog(@"NO it does contain ' word");
 
 }
 
 // Prepare the query string.
 NSString *query;
 
 query = [NSString stringWithFormat:@"update TABLE_SERVICES_DIRECTORY  set SERVICE_NAME='%@', SERVICE_DESC='%@', SERVICE_IMAGE='%@', SERVICE_LATITUDE='%@', SERVICE_LONGITUDE='%@', SERVICE_PHONE_NUMBER='%@', SERVICE_WEBSITE='%@', SERVICE_EMAIL='%@', SERVICE_ADDRESS='%@', SERVICE_WORKING_HOURS='%@', SERVICE_OTHER_INFO='%@', SERVICE_NATIVE_APP='%@', SERVICE_IS_AVAILABLE='%@', SERVICE_NATIVE_APPNAME='%@', OTHER_WEBSITE='%@', AND_LINK='%@' where SERVICE_ID='%@'",serviceName,serviceDesc,serviceImg,serviceLat,serviceLong,servicePhoneno,serviceWebsite,serviceEmail,serviceAddress, serviceWorkingHour, serviceOtherInfo, serviceNativeApp,isavailable,nativeAppName,otherwebsite,andLink,serviceId];
 
 
 
 
 if ([UMSqliteManager executeScalarQuery:query]==YES)
 {
 
 NSLog(@"Data  inserted successfully");
 
 }else{
 NSLog(@"Data not inserted successfully");
 }
 
 }
 */
/*
 -(void)updateServicesDirData:(NSString*)serviceId
 serviceName:(NSString*)serviceName
 serviceDesc:(NSString*)serviceDesc
 serviceImg:(NSString*)serviceImg
 serviceLat:(NSString*)serviceLat
 serviceLong:(NSString*)serviceLong
 servicePhoneno:(NSString*)servicePhoneno
 serviceWebsite:(NSString*)serviceWebsite
 serviceEmail:(NSString*)serviceEmail
 serviceAddress:(NSString*)serviceAddress
 serviceWorkingHour:(NSString*)serviceWorkingHour
 serviceOtherInfo:(NSString*)serviceOtherInfo
 serviceNativeApp:(NSString*)serviceNativeApp
 isavailable:(NSString*)isavailable
 nativeAppName:(NSString*)nativeAppName
 
 {
 
 NSString *word = @"'";
 if ([serviceDesc rangeOfString:word].location != NSNotFound)
 {
 NSLog(@"Yes it does contain ' word");
 NSLog(@"Before serviceDesc=%@",serviceDesc);
 
 serviceDesc = [serviceDesc stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
 NSLog(@"serviceDesc=%@",serviceDesc);
 
 }
 else
 {
 NSLog(@"NO it does contain ' word");
 
 }
 
 // Prepare the query string.
 NSString *query;
 
 query = [NSString stringWithFormat:@"update TABLE_SERVICES_DIRECTORY  set SERVICE_NAME='%@', SERVICE_DESC='%@', SERVICE_IMAGE='%@', SERVICE_LATITUDE='%@', SERVICE_LONGITUDE='%@', SERVICE_PHONE_NUMBER='%@', SERVICE_WEBSITE='%@', SERVICE_EMAIL='%@', SERVICE_ADDRESS='%@', SERVICE_WORKING_HOURS='%@', SERVICE_OTHER_INFO='%@', SERVICE_NATIVE_APP='%@', SERVICE_IS_AVAILABLE='%@', SERVICE_NATIVE_APPNAME='%@' where SERVICE_ID='%@'",serviceName,serviceDesc,serviceImg,serviceLat,serviceLong,servicePhoneno,serviceWebsite,serviceEmail,serviceAddress, serviceWorkingHour, serviceOtherInfo, serviceNativeApp,isavailable,nativeAppName,serviceId];
 
 
 
 
 
 if ([UMSqliteManager executeScalarQuery:query]==YES)
 {
 
 NSLog(@"Data  inserted successfully");
 
 }else{
 NSLog(@"Data not inserted successfully");
 }
 
 }
 
 
 */

-(void)clearSingletonProfileValue
{
    singleton = [SharedManager sharedSingleton];
    singleton.serviceDirstateCurrent = @"";
    singleton.serviceDirstateSelected = @"";
    
    NSUserDefaults *usrdef = [NSUserDefaults standardUserDefaults];
    [usrdef setValue:@"" forKey:SERVICEDIRECTORYSTATE];
    [usrdef synchronize];
    
    singleton.stateCurrent  = @"";
    singleton.user_StateId  = @"";
    singleton.stateSelected = @"";
    singleton.objUserProfile = nil;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setAESKey:@"UMANGIOSAPP"];
    [defaults encryptValue:@"" withKey:@"USER_ID"];
    [defaults encryptValue:@"" withKey:@"ADHAR_ADHARNUM_KEY"];
    [defaults encryptValue:@"" withKey:@"ADHAR_IMAGE_URL_KEY"];
    [defaults encryptValue:@"" withKey:@"ADHAR_PINCODE_KEY"];
    [defaults encryptValue:@"" withKey:@"ADHAR_DOB_KEY"];
    [defaults encryptValue:@"" withKey:@"ADHAR_EMAIL_KEY"];
    [defaults encryptValue:@"" withKey:@"ADHAR_GENDER_KEY"];
    [defaults encryptValue:@"" withKey:@"ADHAR_MOB_KEY"];
    [defaults encryptValue:@"" withKey:@"ADHAR_NAME_KEY"];
    [defaults encryptValue:@"" withKey:@"ADHAR_DISTRICT_KEY"];
    [defaults encryptValue:@"" withKey:@"ADHAR_FATHERNAME_KEY"];
    [defaults encryptValue:@"" withKey:@"ADHAR_STATE_KEY"];
    [defaults encryptValue:@"" withKey:@"ADHAR_STREET_KEY"];
    [defaults encryptValue:@"" withKey:@"ADHAR_SUBDISTRICT_KEY"];
    [defaults encryptValue:@"" withKey:@"ADHAR_VTC_KEY"];
    [defaults encryptValue:@"" withKey:@"ADHAR_VTCCODE_KEY"];
    [defaults encryptValue:@"" withKey:@"ADHAR_ADDRESS_KEY"];
    
    ///---------- Mobile Credential----------------------------------
    [defaults encryptValue:@"" withKey:@"CITY_KEY"];
    [defaults encryptValue:@"" withKey:@"ALTERMB_KEY"];
    [defaults encryptValue:@"" withKey:@"QUALI_KEY"];
    [defaults encryptValue:@"" withKey:@"OCCUP_KEY"];
    [defaults encryptValue:@"" withKey:@"EMAIL_KEY"];
    [defaults encryptValue:@"" withKey:@"URLPROFILE_KEY"];
    [defaults encryptValue:@"" withKey:@"TOKEN_KEY"];
    [defaults encryptValue:@"" withKey:@"MOBILE_KEY"];
    [defaults encryptValue:@"" withKey:@"NAME_KEY"];
    [defaults encryptValue:@"" withKey:@"GENDER_KEY"];
    [defaults encryptValue:@"" withKey:@"STATE_KEY"];
    [defaults encryptValue:@"" withKey:@"DISTRICT_KEY"];
    [defaults encryptValue:@"" withKey:@"DOB_KEY"];
    [defaults synchronize];
}


-(BOOL)isLibertyPatchJailBroken
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    SharedManager *objShared = [SharedManager sharedSingleton];
    
    NSString *stringUDID=[objShared appUniqueID];
    
    NSLog(@"stringUDID=%@",stringUDID);
    
    CFUUIDRef udid = CFUUIDCreate(NULL);
    NSString *udidString = (NSString *) CFBridgingRelease(CFUUIDCreateString(NULL, udid));
    NSLog(@"udidString=%@",udidString);
    
    BOOL isJBFlag= [isJB()? @"YES":@"NO" boolValue];
    NSLog(@"isJBFlag %@", (isJBFlag ? @"YES" : @"NO"));
    
    /*
     // No use of it as isCracked is under research
     
     BOOL isCrackedFlag= [isCracked()? @"YES":@"NO" boolValue];
     NSLog(@" isCracked %@", (isCrackedFlag ? @"YES" : @"NO"));
     
     // No use of it as isAppstoreFlag is not used for staging and Simulator
     
     BOOL isAppStoreFlag= [isAppStore()? @"YES":@"NO" boolValue];
     NSLog(@"  isAppStore%@", (isAppStoreFlag ? @"YES" : @"NO"));
     
     NSString *composedMessage = [NSString stringWithFormat:@"OS : %@\nUDID : \n%@\nIs JailBreak : %@\nIs Cracked : %@\nIs Appstore Version : %@", [[UIDevice currentDevice] systemVersion], udidString, (isJB()? @"YES":@"NO"), (isCracked()? @"YES":@"NO"), (isAppStore()? @"YES":@"NO")];
     NSLog(@"  composedMessage %@", composedMessage);
     
     
     */
    return isJBFlag;
    
    
}


//============= Start Code used to load Country Code data from JSON file=====
-(NSMutableArray*)loadCountryCodeData
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"CountryCode" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    NSMutableArray *countryData=[NSMutableArray new];
    countryData= [dict mutableCopy];
    return countryData;
}
//============= END Code used to load Country Code data from JSON file=====



-(NSMutableArray*)loadStaticCountry
{
    NSMutableArray  *staticCountryArr=[[NSMutableArray alloc]initWithObjects:
                                       NSLocalizedString(@"country1" ,nil),
                                       NSLocalizedString(@"country2" ,nil),
                                       NSLocalizedString(@"country3" ,nil),
                                       NSLocalizedString(@"country4" ,nil),
                                       NSLocalizedString(@"country5" ,nil),
                                       NSLocalizedString(@"country6" ,nil),
                                       NSLocalizedString(@"country7" ,nil),
                                       NSLocalizedString(@"country8" ,nil),
                                       NSLocalizedString(@"country9" ,nil),
                                       NSLocalizedString(@"country10" ,nil),
                                       NSLocalizedString(@"country11" ,nil),
                                       NSLocalizedString(@"country12" ,nil),
                                       NSLocalizedString(@"country13" ,nil),
                                       NSLocalizedString(@"country14" ,nil),
                                       NSLocalizedString(@"country15" ,nil),
                                       NSLocalizedString(@"country16" ,nil),
                                       NSLocalizedString(@"country17" ,nil),
                                       NSLocalizedString(@"country18" ,nil),
                                       NSLocalizedString(@"country19" ,nil),
                                       NSLocalizedString(@"country20" ,nil),
                                       NSLocalizedString(@"country21" ,nil),
                                       NSLocalizedString(@"country22" ,nil),
                                       NSLocalizedString(@"country23" ,nil),
                                       NSLocalizedString(@"country24" ,nil),
                                       NSLocalizedString(@"country25" ,nil),
                                       NSLocalizedString(@"country26" ,nil),
                                       NSLocalizedString(@"country27" ,nil),
                                       NSLocalizedString(@"country28" ,nil),
                                       NSLocalizedString(@"country29" ,nil),
                                       NSLocalizedString(@"country30" ,nil),
                                       NSLocalizedString(@"country31" ,nil),
                                       NSLocalizedString(@"country32" ,nil),
                                       NSLocalizedString(@"country33" ,nil),
                                       NSLocalizedString(@"country34" ,nil),
                                       NSLocalizedString(@"country35" ,nil),
                                       NSLocalizedString(@"country36" ,nil),
                                       NSLocalizedString(@"country37" ,nil),
                                       NSLocalizedString(@"country38" ,nil),
                                       NSLocalizedString(@"country39" ,nil),
                                       NSLocalizedString(@"country40" ,nil),
                                       NSLocalizedString(@"country41" ,nil),
                                       NSLocalizedString(@"country42" ,nil),
                                       NSLocalizedString(@"country43" ,nil),
                                       NSLocalizedString(@"country44" ,nil),
                                       NSLocalizedString(@"country45" ,nil),
                                       NSLocalizedString(@"country46" ,nil),
                                       NSLocalizedString(@"country47" ,nil),
                                       NSLocalizedString(@"country48" ,nil),
                                       NSLocalizedString(@"country49" ,nil),
                                       NSLocalizedString(@"country50" ,nil),
                                       NSLocalizedString(@"country51" ,nil),
                                       NSLocalizedString(@"country52" ,nil),
                                       NSLocalizedString(@"country53" ,nil),
                                       NSLocalizedString(@"country54" ,nil),
                                       NSLocalizedString(@"country55" ,nil),
                                       NSLocalizedString(@"country56" ,nil),
                                       NSLocalizedString(@"country57" ,nil),
                                       NSLocalizedString(@"country58" ,nil),
                                       NSLocalizedString(@"country59" ,nil),
                                       NSLocalizedString(@"country60" ,nil),
                                       NSLocalizedString(@"country61" ,nil),
                                       NSLocalizedString(@"country62" ,nil),
                                       NSLocalizedString(@"country63" ,nil),
                                       NSLocalizedString(@"country64" ,nil),
                                       NSLocalizedString(@"country65" ,nil),
                                       NSLocalizedString(@"country66" ,nil),
                                       NSLocalizedString(@"country67" ,nil),
                                       NSLocalizedString(@"country68" ,nil),
                                       NSLocalizedString(@"country69" ,nil),
                                       NSLocalizedString(@"country70" ,nil),
                                       NSLocalizedString(@"country71" ,nil),
                                       NSLocalizedString(@"country72" ,nil),
                                       NSLocalizedString(@"country73" ,nil),
                                       NSLocalizedString(@"country74" ,nil),
                                       NSLocalizedString(@"country75" ,nil),
                                       NSLocalizedString(@"country76" ,nil),
                                       NSLocalizedString(@"country77" ,nil),
                                       NSLocalizedString(@"country78" ,nil),
                                       NSLocalizedString(@"country79" ,nil),
                                       NSLocalizedString(@"country80" ,nil),
                                       NSLocalizedString(@"country81" ,nil),
                                       NSLocalizedString(@"country82" ,nil),
                                       NSLocalizedString(@"country83" ,nil),
                                       NSLocalizedString(@"country84" ,nil),
                                       NSLocalizedString(@"country85" ,nil),
                                       NSLocalizedString(@"country86" ,nil),
                                       NSLocalizedString(@"country87" ,nil),
                                       NSLocalizedString(@"country88" ,nil),
                                       NSLocalizedString(@"country89" ,nil),
                                       NSLocalizedString(@"country90" ,nil),
                                       NSLocalizedString(@"country91" ,nil),
                                       NSLocalizedString(@"country92" ,nil),
                                       NSLocalizedString(@"country93" ,nil),
                                       NSLocalizedString(@"country94" ,nil),
                                       NSLocalizedString(@"country95" ,nil),
                                       NSLocalizedString(@"country96" ,nil),
                                       NSLocalizedString(@"country97" ,nil),
                                       NSLocalizedString(@"country98" ,nil),
                                       NSLocalizedString(@"country99" ,nil),
                                       NSLocalizedString(@"country100" ,nil),
                                       NSLocalizedString(@"country101" ,nil),
                                       NSLocalizedString(@"country102" ,nil),
                                       NSLocalizedString(@"country103" ,nil),
                                       NSLocalizedString(@"country104" ,nil),
                                       NSLocalizedString(@"country105" ,nil),
                                       NSLocalizedString(@"country106" ,nil),
                                       NSLocalizedString(@"country107" ,nil),
                                       NSLocalizedString(@"country108" ,nil),
                                       NSLocalizedString(@"country109" ,nil),
                                       NSLocalizedString(@"country110" ,nil),
                                       NSLocalizedString(@"country111" ,nil),
                                       NSLocalizedString(@"country112" ,nil),
                                       NSLocalizedString(@"country113" ,nil),
                                       NSLocalizedString(@"country114" ,nil),
                                       NSLocalizedString(@"country115" ,nil),
                                       NSLocalizedString(@"country116" ,nil),
                                       NSLocalizedString(@"country117" ,nil),
                                       NSLocalizedString(@"country118" ,nil),
                                       NSLocalizedString(@"country119" ,nil),
                                       NSLocalizedString(@"country120" ,nil),
                                       NSLocalizedString(@"country121" ,nil),
                                       NSLocalizedString(@"country122" ,nil),
                                       NSLocalizedString(@"country123" ,nil),
                                       NSLocalizedString(@"country124" ,nil),
                                       NSLocalizedString(@"country125" ,nil),
                                       NSLocalizedString(@"country126" ,nil),
                                       NSLocalizedString(@"country127" ,nil),
                                       NSLocalizedString(@"country128" ,nil),
                                       NSLocalizedString(@"country129" ,nil),
                                       NSLocalizedString(@"country130" ,nil),
                                       NSLocalizedString(@"country131" ,nil),
                                       NSLocalizedString(@"country132" ,nil),
                                       NSLocalizedString(@"country133" ,nil),
                                       NSLocalizedString(@"country134" ,nil),
                                       NSLocalizedString(@"country135" ,nil),
                                       NSLocalizedString(@"country136" ,nil),
                                       NSLocalizedString(@"country137" ,nil),
                                       NSLocalizedString(@"country138" ,nil),
                                       NSLocalizedString(@"country139" ,nil),
                                       NSLocalizedString(@"country140" ,nil),
                                       NSLocalizedString(@"country141" ,nil),
                                       NSLocalizedString(@"country142" ,nil),
                                       NSLocalizedString(@"country143" ,nil),
                                       NSLocalizedString(@"country144" ,nil),
                                       NSLocalizedString(@"country145" ,nil),
                                       NSLocalizedString(@"country146" ,nil),
                                       NSLocalizedString(@"country147" ,nil),
                                       NSLocalizedString(@"country148" ,nil),
                                       NSLocalizedString(@"country149" ,nil),
                                       NSLocalizedString(@"country150" ,nil),
                                       NSLocalizedString(@"country151" ,nil),
                                       NSLocalizedString(@"country152" ,nil),
                                       NSLocalizedString(@"country153" ,nil),
                                       NSLocalizedString(@"country154" ,nil),
                                       NSLocalizedString(@"country155" ,nil),
                                       NSLocalizedString(@"country156" ,nil),
                                       NSLocalizedString(@"country157" ,nil),
                                       NSLocalizedString(@"country158" ,nil),
                                       NSLocalizedString(@"country159" ,nil),
                                       NSLocalizedString(@"country160" ,nil),
                                       NSLocalizedString(@"country161" ,nil),
                                       NSLocalizedString(@"country162" ,nil),
                                       NSLocalizedString(@"country163" ,nil),
                                       NSLocalizedString(@"country164" ,nil),
                                       NSLocalizedString(@"country165" ,nil),
                                       NSLocalizedString(@"country166" ,nil),
                                       NSLocalizedString(@"country167" ,nil),
                                       NSLocalizedString(@"country168" ,nil),
                                       NSLocalizedString(@"country169" ,nil),
                                       NSLocalizedString(@"country170" ,nil),
                                       NSLocalizedString(@"country171" ,nil),
                                       NSLocalizedString(@"country172" ,nil),
                                       NSLocalizedString(@"country173" ,nil),
                                       NSLocalizedString(@"country174" ,nil),
                                       NSLocalizedString(@"country175" ,nil),
                                       NSLocalizedString(@"country176" ,nil),
                                       NSLocalizedString(@"country177" ,nil),
                                       NSLocalizedString(@"country178" ,nil),
                                       NSLocalizedString(@"country179" ,nil),
                                       NSLocalizedString(@"country180" ,nil),
                                       NSLocalizedString(@"country181" ,nil),
                                       NSLocalizedString(@"country182" ,nil),
                                       NSLocalizedString(@"country183" ,nil),
                                       NSLocalizedString(@"country184" ,nil),
                                       NSLocalizedString(@"country185" ,nil),
                                       NSLocalizedString(@"country186" ,nil),
                                       NSLocalizedString(@"country187" ,nil),
                                       NSLocalizedString(@"country188" ,nil),
                                       NSLocalizedString(@"country189" ,nil),
                                       NSLocalizedString(@"country190" ,nil),
                                       NSLocalizedString(@"country191" ,nil),
                                       NSLocalizedString(@"country192" ,nil),
                                       NSLocalizedString(@"country193" ,nil),
                                       NSLocalizedString(@"country194" ,nil),
                                       NSLocalizedString(@"country195" ,nil),
                                       NSLocalizedString(@"country196" ,nil),
                                       NSLocalizedString(@"country197" ,nil),
                                       NSLocalizedString(@"country198" ,nil),
                                       NSLocalizedString(@"country199" ,nil),
                                       NSLocalizedString(@"country200" ,nil),
                                       NSLocalizedString(@"country201" ,nil),
                                       NSLocalizedString(@"country202" ,nil),
                                       NSLocalizedString(@"country203" ,nil),
                                       NSLocalizedString(@"country204" ,nil),
                                       NSLocalizedString(@"country205" ,nil),
                                       NSLocalizedString(@"country206" ,nil),
                                       NSLocalizedString(@"country207" ,nil),
                                       NSLocalizedString(@"country208" ,nil),
                                       NSLocalizedString(@"country209" ,nil),
                                       NSLocalizedString(@"country210" ,nil),
                                       NSLocalizedString(@"country211" ,nil),
                                       NSLocalizedString(@"country212" ,nil),
                                       NSLocalizedString(@"country213" ,nil),
                                       NSLocalizedString(@"country214" ,nil),
                                       NSLocalizedString(@"country215" ,nil),
                                       NSLocalizedString(@"country216" ,nil),
                                       NSLocalizedString(@"country217" ,nil),
                                       NSLocalizedString(@"country218" ,nil),
                                       NSLocalizedString(@"country219" ,nil),
                                       NSLocalizedString(@"country220" ,nil),
                                       NSLocalizedString(@"country221" ,nil),
                                       NSLocalizedString(@"country222" ,nil),
                                       NSLocalizedString(@"country223" ,nil),
                                       NSLocalizedString(@"country224" ,nil),
                                       NSLocalizedString(@"country225" ,nil),
                                       NSLocalizedString(@"country226" ,nil),
                                       NSLocalizedString(@"country227" ,nil),
                                       NSLocalizedString(@"country228" ,nil),
                                       NSLocalizedString(@"country229" ,nil),
                                       NSLocalizedString(@"country230" ,nil),
                                       NSLocalizedString(@"country231" ,nil),
                                       NSLocalizedString(@"country232" ,nil),
                                       NSLocalizedString(@"country233" ,nil),
                                       NSLocalizedString(@"country234" ,nil),
                                       NSLocalizedString(@"country235" ,nil),
                                       NSLocalizedString(@"country236" ,nil),
                                       NSLocalizedString(@"country237" ,nil),
                                       NSLocalizedString(@"country238" ,nil),
                                       NSLocalizedString(@"country239" ,nil),
                                       NSLocalizedString(@"country240" ,nil),
                                       NSLocalizedString(@"country241" ,nil),
                                       NSLocalizedString(@"country242" ,nil),
                                       NSLocalizedString(@"country243" ,nil),
                                       NSLocalizedString(@"country244" ,nil),
                                       NSLocalizedString(@"country245" ,nil),
                                       NSLocalizedString(@"country246" ,nil),
                                       NSLocalizedString(@"country247" ,nil),
                                       NSLocalizedString(@"country248" ,nil),
                                       NSLocalizedString(@"country249" ,nil)
                                       ,nil];
    
    
    // NSLog(@"static array= %@",staticCountryArr);
    
    return staticCountryArr;
}



@end
