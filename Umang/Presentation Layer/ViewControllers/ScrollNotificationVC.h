//
//  ScrollNotificationVC.h
//  Umang
//
//  Created by admin on 26/09/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScrollNotificationVC : UIViewController
{
}

@property (weak, nonatomic) IBOutlet UITableView *tblVwNotifications;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentFilter;
- (IBAction)segmentOptionClicked:(UISegmentedControl *)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnClearAll;
@property (weak, nonatomic) IBOutlet UIButton *backBtnHome;

@property (weak, nonatomic) IBOutlet UILabel *lblNotificationHeader;

-(IBAction)backbtnPressed:(id)sender;

@end

