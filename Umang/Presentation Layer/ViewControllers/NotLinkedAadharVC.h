//
//  NotLinkedAadharVC.h
//  Umang
//
//  Created by admin on 09/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotLinkedAadharVC : UIViewController
{
    IBOutlet UIButton *btn_help;
}

- (IBAction)btnMoreClicked:(id)sender;
- (IBAction)btnHelpClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *tblNotLinkAadhar;
@end
