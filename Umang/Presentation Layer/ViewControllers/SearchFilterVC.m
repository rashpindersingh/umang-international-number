//
//  SearchFilterVC.m
//  Umang
//
//  Created by admin on 07/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "SearchFilterVC.h"
#import "SearchCell.h"
#import "CZPickerView.h"
#import "SDCapsuleButton.h"
#import "FilterServicesBO.h"
#import "NotificationItemBO.h"

#import "HomeCardBO.h"
#import "NotificationItemBO.h"
#import "UIImageView+WebCache.h"
#import "HomeDetailVC.h"
#import "HomeTabVC.h"
#import "ScrollNotificationVC.h"

#import "FAQWebVC.h"
#import "RateUsVCViewController.h"

#import "NotLinkedAadharVC.h"

#import "AadharCardViewCon.h"
#import "FeedbackVC.h"

#import "SocialMediaViewController.h"

#import "UserProfileVC.h"
#import "ShowUserProfileVC.h"


#import "SettingsViewController.h"
#import "HelpViewController.h"
#import "SecuritySettingVC.h"
#import "NotificationSearchCell.h"



@interface SearchFilterVC () <CZPickerViewDelegate,CZPickerViewDataSource>
{
    NSMutableArray *arrMainData;
    NSMutableArray *arrTableData;
    
    NSArray *arrSortOptions;
    NSArray *arrFilterOptions;
    SharedManager *singleton;
}

@end

@implementation SearchFilterVC
@synthesize btnSort,btnFilter;

- (void)viewDidLoad
{
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:SEARCH_FILTER_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    if (_isFromHomeFilter == false) {
        [self notiticationCellXib];
    }
    vw_noresults.hidden=TRUE;
    
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    
    self.tblSearchFilter.layoutMargins = UIEdgeInsetsZero;
    UIView *vwLineHide = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 0.5)];
    [self.tblSearchFilter addSubview:vwLineHide];
    vwLineHide.backgroundColor = [UIColor whiteColor];
    
    
    // Add Filter Capsules Button
    [self addFilterOptionsAtNavigationBar];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
    
    // Do any additional setup after loading the view.
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}

-(void)addFilterOptionsAtNavigationBar
{
    
    //Remove existing button
    
    [[self.scrollView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    // First fetch State Name
    
    
    NSMutableArray *arrStates = [self.dictFilterParams objectForKey:@"state_name"];
    
    CGFloat xCord = 10;
    CGFloat yCord = 0;
    CGFloat padding = 5;
    CGFloat itemHeight = 30.0;
    
    
    NSString *str = [self.dictFilterParams objectForKey:@"notification_type"];
    
    if ([str length]==0||str==nil)
    {
        str=NSLocalizedString(@"all", nil);
        //[self.dictFilterParams setObject:NSLocalizedString(@"all", nil) forKey:@"notification_type"];
        
    }
    
    if (arrStates.count ==1)
    {
        
        NSString *stateName = arrStates[0];
        
        if ([stateName isEqualToString:NSLocalizedString(@"all", nil)])
        {
            CGRect frame = [self rectForText:stateName usingFont:TITLE_FONT boundedBySize:CGSizeMake(1000.0, itemHeight)];
            frame.size.width = frame.size.width + 30;
            
            SDCapsuleButton *btn = [[SDCapsuleButton alloc] initWithFrameWithFullBorder:CGRectMake(xCord, yCord, frame.size.width, itemHeight)];
            [btn setBtnTitle:stateName];
            // [btn set]
            [btn addTarget:self action:@selector(btnCapsuleStateClicked:) forControlEvents:UIControlEventTouchUpInside];
            [btn.btnCross addTarget:self action:@selector(btnCrossClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            if ([stateName isEqualToString:NSLocalizedString(@"all", nil)])
            {
                btn.btnCross.hidden = YES;
                btn.userInteractionEnabled = NO;
            }
            else
            {
                btn.btnCross.hidden = NO;
            }
            
            
            
            //  btn.btnCross.
            btn.tag = 2300+0;
            [self.scrollView addSubview:btn];
            xCord+=frame.size.width + padding;
        }
        
    }
    CGRect frame;
    if ([str isEqualToString:NSLocalizedString(@"all", nil)])
    {
        
        xCord+=padding;
    }
    else
    {
        
        frame = [self rectForText:str usingFont:TITLE_FONT boundedBySize:CGSizeMake(1000.0, itemHeight)];
        frame.size.width = frame.size.width + 30;
        // xCord+=frame.size.width + padding;
        
        
        SDCapsuleButton *btn = [[SDCapsuleButton alloc] initWithFrameWithFullBorder:CGRectMake(xCord, yCord, frame.size.width, itemHeight)];
        [btn setBtnTitle:str];
        [btn addTarget:self action:@selector(btnCapsuleNotificationClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn.btnCross addTarget:self action:@selector(btnCrossClicked:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 2299;
        [self.scrollView addSubview:btn];
        
        xCord+=frame.size.width + padding;
    }
    
    
    
    
    for (int i = 0; i < arrStates.count; i++)
    {
        
        NSString *stateName = arrStates[i];
        
        if (![stateName isEqualToString:NSLocalizedString(@"all", nil)])
        {
            
            CGRect frame = [self rectForText:stateName usingFont:TITLE_FONT boundedBySize:CGSizeMake(1000.0, itemHeight)];
            frame.size.width = frame.size.width + 30;
            
            SDCapsuleButton *btn = [[SDCapsuleButton alloc] initWithFrameWithFullBorder:CGRectMake(xCord, yCord, frame.size.width, itemHeight)];
            [btn setBtnTitle:stateName];
            // [btn set]
            [btn addTarget:self action:@selector(btnCapsuleStateClicked:) forControlEvents:UIControlEventTouchUpInside];
            [btn.btnCross addTarget:self action:@selector(btnCrossClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            if ([stateName isEqualToString:NSLocalizedString(@"all", nil)])
            {
                btn.btnCross.hidden = YES;
                btn.userInteractionEnabled = NO;
            }
            else
            {
                btn.btnCross.hidden = NO;
            }
            
            
            
            //  btn.btnCross.
            btn.tag = 2300+i;
            [self.scrollView addSubview:btn];
            xCord+=frame.size.width + padding;
        }
    }
    
    
    // Now fetch Categories
    
    
    NSMutableArray *arrCategories = [self.dictFilterParams objectForKey:@"category_type"];
    
    for (int i = 0; i<arrCategories.count; i++) {
        NSString *serviceName = arrCategories[i];
        
        
        CGRect frame = [self rectForText:serviceName usingFont:TITLE_FONT boundedBySize:CGSizeMake(1000.0, itemHeight)];
        frame.size.width = frame.size.width + 30;
        
        SDCapsuleButton *btn = [[SDCapsuleButton alloc] initWithFrameWithFullBorder:CGRectMake(xCord, yCord, frame.size.width, itemHeight)];
        [btn setBtnTitle:serviceName];
        [btn addTarget:self action:@selector(btnCapsuleCategoryClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn.btnCross addTarget:self action:@selector(btnCrossClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        btn.tag = 2300+i;
        [self.scrollView addSubview:btn];
        xCord+=frame.size.width + padding;
    }
    
    
    NSString *_selectedNotificationType = [self.dictFilterParams objectForKey:@"service_type"];
    if (_selectedNotificationType.length) {
        
        
        CGRect frame = [self rectForText:_selectedNotificationType usingFont:TITLE_FONT boundedBySize:CGSizeMake(1000.0, itemHeight)];
        frame.size.width = frame.size.width + 30;
        
        SDCapsuleButton *btn = [[SDCapsuleButton alloc] initWithFrameWithFullBorder:CGRectMake(xCord, yCord, frame.size.width, itemHeight)];
        
        [btn setBtnTitle:_selectedNotificationType];
        [btn addTarget:self action:@selector(btnCapsuleServiceTypeClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn.btnCross addTarget:self action:@selector(btnCrossClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:btn];
        xCord+=frame.size.width + padding;
    }
    
    
    [self.scrollView setContentSize:CGSizeMake(xCord, self.scrollView.frame.size.height)];
    
    
    if (self.isFromHomeFilter) {
        [self getFilterDataForHomeScreen];
    }
    else{
        [self getFilteredData];
    }
}

-(void)btnCapsuleServiceTypeClicked:(SDCapsuleButton*)btnSender{
    //added here
    //added here
    //added here
    if ([[btnSender btnTitle] isEqualToString:NSLocalizedString(@"regional", nil)])
    {
        
        [self.dictFilterParams removeObjectForKey:@"service_type"];
        [self.dictFilterParams removeObjectForKey:@"state_name"];
        
    }
    else
    {
        [self.dictFilterParams removeObjectForKey:@"service_type"];
        
    }
    //added here
    //added here
    //added here
    [self addFilterOptionsAtNavigationBar];
}

-(void)btnCapsuleStateClicked:(SDCapsuleButton*)btnSender{
    
    NSLog(@"self.dictFilterParams=%@",self.dictFilterParams);
    NSMutableArray *arrStates = [self.dictFilterParams objectForKey:@"state_name"];
    [arrStates removeObject:[btnSender btnTitle]];
    //added here
    //added here
    //added here
    
    if ([arrStates count]==0) {
        [self.dictFilterParams removeObjectForKey:@"service_type"];
    }
    //added here
    //added here
    //added here
    [self.dictFilterParams setObject:arrStates forKey:@"state_name"];
    [self addFilterOptionsAtNavigationBar];
    
}


-(void)btnCapsuleCategoryClicked:(SDCapsuleButton*)btnSender{
    
    NSMutableArray *arrCategories = [self.dictFilterParams objectForKey:@"category_type"];
    
    [arrCategories removeObject:[btnSender btnTitle]];
    
    [self.dictFilterParams setObject:arrCategories forKey:@"category_type"];
    
    
    NSMutableArray *serviceIdarray=[[NSMutableArray alloc]init];
    for (int i = 0; i<arrCategories.count; i++)
        
    {
        NSString *serviceID=[singleton.dbManager getServiceId:[arrCategories objectAtIndex:i]];
        [serviceIdarray addObject:serviceID];
    }
    
    [self.dictFilterParams setObject:serviceIdarray forKey:@"service_list"];
    
    
    [self addFilterOptionsAtNavigationBar];
    
    //service_list
    
}


-(void)btnCapsuleNotificationClicked:(SDCapsuleButton*)btnSender{
    
    
    [self.dictFilterParams removeObjectForKey:@"notification_type"];
    
    
    [self addFilterOptionsAtNavigationBar];
    
}


-(void)getFilterDataForHomeScreen{
    if (arrTableData == nil) {
        arrTableData = [[NSMutableArray alloc] init];
    }
    else{
        [arrTableData removeAllObjects];
    }
    SharedManager *singleton = [SharedManager sharedSingleton];
    NSString *sortBy = [self.dictFilterParams objectForKey:@"sort_by"];
    NSString *serviceType = [self.dictFilterParams objectForKey:@"service_type"];
    NSArray *_arrStates = [self.dictFilterParams objectForKey:@"state_name"];
    
    //added here
    //added here
    //added here
    /*
    if ([_arrStates count]==0) {
        serviceType=@"";
    }*/
    //added here
    //added here
    //added here
    
    NSMutableArray *arrSelectedCategories = [self.dictFilterParams objectForKey:@"category_type"];
    NSArray *arrFilterResponse =  [singleton.dbManager getFilteredServiceData:sortBy serviceType:serviceType stateIdAlist:_arrStates categoryList:arrSelectedCategories];
    [arrTableData addObjectsFromArray:arrFilterResponse];
    
    
    if ([arrTableData count]>0) {
        _tblSearchFilter.hidden=FALSE;
        vw_noresults.hidden=TRUE;
        
    }
    else
    {
        _tblSearchFilter.hidden=TRUE;
        vw_noresults.hidden=false;
        lb_noresults.text=@"No service found";
        
        
    }
    
    
    [_tblSearchFilter reloadData];
}


/*
 
 Handle condition to jump here from
 call method name getFilteredFavouriteServiceData for favourite filters
 
 typedef enum{
 FILTER_HOME =108,
 FILTER_FAVOURITE,
 FILTER_ALLSERVICE,
 FILTER_NEARME,
 }FILTER_TYPE;
 
 
 */

-(void)getFilteredData
{
    if (arrTableData == nil) {
        arrTableData = [[NSMutableArray alloc] init];
    }
    else{
        [arrTableData removeAllObjects];
    }
    
    // NSString *_selectedNotificationType = [self.dictFilterParams objectForKey:@"notification_type"];
    
    NSString *_selectedNotificationType = [self.dictFilterParams objectForKey:@"notification_type"];
    
    if ([_selectedNotificationType length]==0||_selectedNotificationType==nil)
    {
        _selectedNotificationType=NSLocalizedString(@"all", nil);
        //[self.dictFilterParams setObject:NSLocalizedString(@"all", nil) forKey:@"notification_type"];
        
    }
    
    
    if ([_selectedNotificationType isEqualToString:NSLocalizedString(@"all", nil)]) {
        // [arrTableData addObjectsFromArray:[[SharedManager sharedSingleton] prepareStaticDataForNotifications]];
    }
    else if ([_selectedNotificationType isEqualToString:NSLocalizedString(@"promotional_small", nil)])
    {
        
        // NSPredicate *predicateFilter  = [NSPredicate predicateWithFormat:@"SELF.statusType == 100"];
        // [arrTableData addObjectsFromArray:[[[SharedManager sharedSingleton] prepareStaticDataForNotifications] filteredArrayUsingPredicate:predicateFilter]];
        
    }
    else if ([_selectedNotificationType isEqualToString:NSLocalizedString(@"transactional_small", nil)])
    {
        
        //NSPredicate *predicateFilter  = [NSPredicate predicateWithFormat:@"SELF.statusType == 101"];
        // [arrTableData addObjectsFromArray:[[[SharedManager sharedSingleton] prepareStaticDataForNotifications] filteredArrayUsingPredicate:predicateFilter]];
        
    }
    
    
    
    
    SharedManager *singleton = [SharedManager sharedSingleton];
    NSString *sortBy = [self.dictFilterParams objectForKey:@"sort_by"];
    // NSString *serviceType = [self.dictFilterParams objectForKey:@"service_type"];
    //NSString *NotifType = [self.dictFilterParams objectForKey:@"notif_type"];
    NSArray *_arr_serviceList = [self.dictFilterParams objectForKey:@"service_list"];//
    
    //NSString *dateStartString = @“31/12/2010 11:04:02”;
    NSString *dateStartString =[self.dictFilterParams objectForKey:@"start_date"];// @"31/12/2010 11:04:02";
    NSString *dateEndString =[self.dictFilterParams objectForKey:@"end_date"];// @"31/12/2010 11:04:02";
    
    //  NSString *dateStartString =@"31/12/2016 11:04:02";
    //  NSString *dateEndString =  @"10/03/2017 11:04:02";
    
    
    
    NSArray *_arrStates = [self.dictFilterParams objectForKey:@"state_name"];
    // NSMutableArray *arrSelectedCategories = [self.dictFilterParams objectForKey:@"category_type"];
    
    
    /* NSArray *arrFilterResponse =[singleton.dbManager getFilteredNotifData:sortBy notifType:serviceType serviceIdAlist:<#(NSArray *)#> stateIdAlist:<#(NSArray *)#> startDate:<#(NSString *)#> endDate:<#(NSString *)#>]
     
     {
     "end_date" = "";
     "notification_type" = All;
     "start_date" = "";
     "state_name" =     (
     All
     );
     }
     
     
     */
    // NSArray *arrFilterResponse =[singleton.dbManager getFilteredServiceData:sortBy serviceType:serviceType stateIdAlist:_arrStates categoryList:arrSelectedCategories];
    
    /* NSArray *arrFilterResponse =[singleton.dbManager getFilteredNotifData:sortBy notifType:_selectedNotificationType serviceIdAlist:_arr_serviceList stateIdAlist:_arrStates startDate:dateStartString endDate:dateEndString]; */
    
    /*
     
     
     select * from TABLE_NOTIFICATIONS where  NOTIF_TYPE  IN ('promo', 'trans') AND  NOTIF_STATE IN ('9999') AND NOTIF_RECEIVE_DATE_TIME >= '31/12/2016 11:04:02' AND NOTIF_RECEIVE_DATE_TIME  <= '10/03/2017 11:04:02' AND USER_ID='1000000058'  ORDER BY NOTIF_TITLE ASC
     
     */
    
    NSArray *arrFilterResponse =[singleton.dbManager getFilteredNotifData:sortBy notifType:_selectedNotificationType serviceIdAlist:_arr_serviceList stateIdAlist:_arrStates startDate:dateStartString endDate:dateEndString user_id:singleton.user_id];
    
    [arrTableData addObjectsFromArray:arrFilterResponse];
    
    
    if ([arrTableData count]>0) {
        _tblSearchFilter.hidden=FALSE;
        vw_noresults.hidden=TRUE;
        
    }
    else
    {
        _tblSearchFilter.hidden=TRUE;
        vw_noresults.hidden=false;
        lb_noresults.text= NSLocalizedString(@"no_notifications", nil);
        //
    }
    
    
    [_tblSearchFilter reloadData];
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 0.001;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    return nil;
}



-(void)btnCrossClicked:(SDCapsuleButton*)btnSender{
    
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (self.isFromHomeFilter) {
        return 100.0;
    }
    else {
        NSString *strMsg = [[arrTableData objectAtIndex:indexPath.row] valueForKey:@"NOTIF_MSG"];
        
        CGRect size = [self rectForText:strMsg usingFont:[AppFont regularFont:14] boundedBySize:CGSizeMake(fDeviceWidth, 0)];
        
        return size.size.height + 60;
        
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    
    return arrTableData.count;
    
}
-(void)notiticationCellXib

{
    UINib *nib = [UINib nibWithNibName:@"NotificationSearchCellXIB" bundle:nil];
    [[self tblSearchFilter] registerNib:nib forCellReuseIdentifier:@"NotificationSearchCellXIB"];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    static NSString *CellIdentifier = @"searchFilterCell";
    SearchCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (self.isFromHomeFilter) {
        NSDictionary *dict = [arrTableData objectAtIndex: indexPath.row];
        NSString *imageURLString = [dict objectForKey:@"SERVICE_IMAGE"];
        if (imageURLString) {
            NSURL *imgURL = [NSURL URLWithString:imageURLString];
            [cell.imgSearchCell sd_setImageWithURL:imgURL
                                  placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
        }
        cell.lblHeader.text = [dict objectForKey:@"SERVICE_NAME"] ;
        cell.lblDescription.text = [dict objectForKey:@"SERVICE_DEPTDESCRIPTION"] ;
        NSString *type=[dict valueForKey:@"NOTIF_TYPE"];
        if ([type isEqualToString:@"promo"]||[type isEqualToString:NSLocalizedString(@"promo", nil)]||[type isEqualToString:@"Promo"])
        {
            cell.imgCategory.image = [UIImage imageNamed:@"promo"];
        }
        else{
            cell.imgCategory.image = nil;
        }
        
        cell.lblHeader.font = [AppFont semiBoldFont:16.0];
        cell.lblDescription.font = [AppFont regularFont:13.0];
        cell.lblEducation.font = [AppFont semiBoldFont:13.0];
        
        return cell;
    }
    else{
        static NSString *CellIdentifier = @"NotificationSearchCellXIB";
        NotificationSearchCell *cell = (NotificationSearchCell *)[self.tblSearchFilter dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        // Configure the cell...
        if (cell == nil) {
            [self notiticationCellXib];
            cell = (NotificationSearchCell *)[self.tblSearchFilter dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            //cell = [[NotificationSearchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSArray *arrData = arrTableData;
        cell.searchHeader.text =  [[arrData objectAtIndex:indexPath.row] valueForKey:@"NOTIF_TITLE"];
        cell.searchDescription.text = [[arrData objectAtIndex:indexPath.row] valueForKey:@"NOTIF_MSG"];
        
        
        // comment this line and add below code
        // cell.searchDate.text = [[arrData objectAtIndex:indexPath.row] valueForKey:@"NOTIF_RECEIVE_DATE_TIME"];
        
        //============== Start of Change date format==============
        
        NSString *dbDateStr = [NSString stringWithFormat:@"%@",[[arrData objectAtIndex:indexPath.row] valueForKey:@"NOTIF_RECEIVE_DATE_TIME"]];
        
        // Convert string to date with dd/MM/yyyy HH:mm:ss formate
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
        NSDate *Dbdate = [dateFormat dateFromString:dbDateStr];
        NSLog(@"Database date from string %@",Dbdate);
        
        // Convert Dbdate object to desired output format
        [dateFormat setDateFormat:@"dd MMM YYYY HH:mm"];
        NSString* dateStr = [dateFormat stringFromDate:Dbdate];
        NSLog(@"dateStr %@",dateStr);
        cell.searchDate.text=dateStr;
        
        //============== End of Change date format==============
        
        
        
        
        
        
        
        NSURL *url=[NSURL URLWithString:[[arrData objectAtIndex:indexPath.row] valueForKey:@"NOTIF_IMG"]];
        
        // [cell.searchImage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"logo.png"]];
        
        [cell.searchImage sd_setImageWithURL:url
                            placeholderImage:[UIImage imageNamed:@"umang_new_logo"]];
        
        
        
        cell.searchPDImg.backgroundColor = [UIColor clearColor];
        cell.layoutMargins = UIEdgeInsetsZero;
        
        NSString *type=[[arrData objectAtIndex:indexPath.row] valueForKey:@"NOTIF_TYPE"];
        
        if ([type isEqualToString:@"promo"]||[type isEqualToString:NSLocalizedString(@"promo", nil)]||[type isEqualToString:@"Promo"])
        {
            cell.searchPDImg.image = [UIImage imageNamed:@"promo"];
        }
        else{
            cell.searchPDImg.image = nil;
        }
        
        cell.searchHeader.font = [AppFont semiBoldFont:16.0];
        cell.searchDescription.font = [AppFont regularFont:13.0];
        cell.searchDate.font = [AppFont semiBoldFont:13.0];
        
        return cell;
        
    }
    
    
    
    /*
     /* NotificationItemBO *object = [arrTableData objectAtIndex: indexPath.row];
     
     cell.imgSearchCell.image = [UIImage imageNamed:object.imgName];
     cell.lblHeader.text = NSLocalizedString(object.headerTitle, nil) ;
     cell.lblDescription.text = NSLocalizedString(object.headerDesc, nil) ;
     NSDictionary *dict = [arrTableData objectAtIndex: indexPath.row];
     
     NSString *imageURLString = [dict objectForKey:@"NOTIF_IMG"];
     if (imageURLString) {
     NSURL *imgURL = [NSURL URLWithString:imageURLString];
     [cell.imgSearchCell sd_setImageWithURL:imgURL
     placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
     }
     
     
     cell.lblHeader.text = [dict objectForKey:@"NOTIF_TITLE"] ;
     cell.lblDescription.text = [dict objectForKey:@"NOTIF_MSG"] ;
     cell.lblEducation.text = [dict objectForKey:@"NOTIF_RECEIVE_DATE_TIME"] ;
     cell.lblEducation.backgroundColor=[UIColor clearColor];
     cell.lblEducation.textColor=[UIColor blackColor];
     
     NSString *type=[dict valueForKey:@"NOTIF_TYPE"];
     
     if ([type isEqualToString:@"promo"]||[type isEqualToString:NSLocalizedString(@"promo", nil)]||[type isEqualToString:@"Promo"])
     {
     cell.imgCategory.image = [UIImage imageNamed:@"promo"];
     }
     else{
     cell.imgCategory.image = nil;
     }
     
     
     }
     cell.layoutMargins = UIEdgeInsetsZero;
     */
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
     NSDictionary *cellData = [arrTableData objectAtIndex:indexPath.row];
     if (cellData)
     {
     
     HomeDetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
     [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
     vc.dic_serviceInfo=cellData;
     vc.tagComeFrom=@"OTHERS";
     
     [self presentViewController:vc animated:NO completion:nil];
     
     
     }*/
    NSMutableDictionary *notifyData = (NSMutableDictionary *)[arrTableData objectAtIndex:indexPath.row];
    
    [self selectedIndexNotify:notifyData];
    
}

-(void)selectedIndexNotify:(NSDictionary*)notifyData
{
    //----------Handle case for subType---------------
    NSString *subType =[notifyData valueForKey:@"NOTIF_SUB_TYPE"];
    //subType=[subType  lowercaseString];
    // Case to open app  for opening app with notification title/
    if([subType isEqualToString:@"openApp"])
    {
        //handle case in delegate for it do nothing
        
    }
    // Case to open app  with dialog message dialogmsg/msg
    
    else if([subType isEqualToString:@"openAppWithDialog"])
    {
        NSString *title =[notifyData valueForKey:@"NOTIF_TITLE"];
        NSString *dialogMsg =[notifyData valueForKey:@"NOTIF_DIALOG_MSG"];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:title message:dialogMsg delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
        [alert show];
        
    }
    // Case to open  playstore url in external
    
    else if([subType isEqualToString:@"playstore"])
    {
        NSString *url=[NSString stringWithFormat:@"%@",[notifyData valueForKey:@"NOTIF_URL"]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        
        
        
    }
    // Case to open app  inside webview with custom webview title
    
    else if([subType isEqualToString:@"webview"])
    {
        NSString *title =[notifyData valueForKey:@"NOTIF_WEBPAGE_TITLE"];
        NSString *url=[NSString stringWithFormat:@"%@",[notifyData valueForKey:@"NOTIF_URL"]];
        // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        [self openFAQWebVC:url withTitle:title];
        
    }
    // Case to open app  in mobile browser
    
    else if([subType isEqualToString:@"browser"])
    {
        NSString *url=[NSString stringWithFormat:@"%@",[notifyData valueForKey:@"NOTIF_URL"]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        
    }
    // Case to open app  with Screen Name like profile/settings etc
    
    else if([subType isEqualToString:@"openAppWithScreen"])
    {
        NSString *screenName=[NSString stringWithFormat:@"%@",[notifyData valueForKey:@"NOTIF_SCREEN_NAME"]];
        
        screenName=[screenName lowercaseString];
        
        if ([screenName isEqualToString:@"settings"])
        {
            [self mySetting_Action];
            
        }
        if ([screenName isEqualToString:@"help"])
        {
            [self myHelp_Action];
            
        }
        if ([screenName isEqualToString:@"social"])
        {
            [self socialMediaAccount];
            
        }
        if ([screenName isEqualToString:@"aadhaar"])
        {
            if (singleton.objUserProfile.objAadhar.aadhar_number.length)
            {
                
                [self AadharCardViewCon];
                
            }
            else
            {
                [self NotLinkedAadharVC];
                
            }
        }
        if ([screenName isEqualToString:@"feedback"])
        {
            [self FeedbackVC];
            
        }
        if ([screenName isEqualToString:@"accountsettings"])
        {
            [self accountSettingAction];
        }
        if ([screenName isEqualToString:@"myprofile"])
        {
            [self myProfile_Action];
            
        }
        else
        {
            //main
        }
        
        
    }
    
    
    
    // Case to open app  with tab name
    
    else if([subType isEqualToString:@"openAppWithTab"])
    {
        NSString *screenName=[NSString stringWithFormat:@"%@",[notifyData valueForKey:@"NOTIF_SCREEN_NAME"]];
        
        screenName=[screenName lowercaseString];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
        
        
        
        if ([screenName isEqualToString:NSLocalizedString(@"home_small", nil)])
        {
            tbc.selectedIndex=0;
            
        }
        if ([screenName isEqualToString:NSLocalizedString(@"favourites_small", nil)])
        {
            tbc.selectedIndex=1;
            
        }
        if ([screenName isEqualToString:NSLocalizedString(@"states", nil)])
        {
            //tbc.selectedIndex=0;ignore case
            
        }
        if ([screenName isEqualToString:NSLocalizedString(@"all_services_small", nil)])
        {
            tbc.selectedIndex=2;
        }
        
        
        [self presentViewController:tbc animated:NO completion:nil];
        
        
        
    }
    // Case to open app with service
    
    else  if([subType isEqualToString:NSLocalizedString(@"services", nil)])
    {
        
        if (notifyData)
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            HomeDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
            [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            vc.dic_serviceInfo=notifyData;
            vc.tagComeFrom=@"NOTIFICATIONVIEW";
            [self presentViewController:vc animated:NO completion:nil];
            
            
        }
        
    }
    // Case to open app  for rating view
    
    else  if([subType isEqualToString:NSLocalizedString(@"Rating", nil)] || [subType isEqualToString:@"rating"])
    {
        [self rateUsClicked];
        
    }
    // Case to open app  for share [sharing message will recieve inside api)
    else  if([subType isEqualToString:NSLocalizedString(@"share", nil)] || [subType isEqualToString:@"share"])
    {
        [self shareContent];
    }
    //Default case
    else
    {
        
    }
    
    
}

-(void)shareContent
{
    NSString *textToShare =singleton.shareText;
    
    NSArray *objectsToShare = @[textToShare];
    
    [[UIPasteboard generalPasteboard] setString:textToShare];
    
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    //if iPhone
    
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        [self presentViewController:controller animated:YES completion:nil];
        
    }
    
    //if iPad
    
    else {
        
        // Change Rect to position Popover
        
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:controller];
        
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    }
    
}

-(void)openFAQWebVC:(NSString *)url withTitle:(NSString*)title
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    vc.urltoOpen=url;
    vc.titleOpen=title;
    
    //  [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}

-(void)rateUsClicked
{
    NSLog(@"My Help Action");
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RateUsVCViewController  *vc = [storyboard instantiateViewControllerWithIdentifier:@"RateUsVCViewController"];
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}


-(void)NotLinkedAadharVC
{
    // SettingsViewController
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NotLinkedAadharVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"NotLinkedAadharVC"];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}



-(void)AadharCardViewCon
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AadharCardViewCon *vc = [storyboard instantiateViewControllerWithIdentifier:@"AadharCardViewCon"];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}




-(void)FeedbackVC
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FeedbackVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FeedbackVC"];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}



-(void)socialMediaAccount
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SocialMediaViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SocialMediaViewController"];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}
-(void)myProfile_Action
{
    NSLog(@"My Profile Action");
    
    
    
    
    // SettingsViewController
    //    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kMainStoryBoard bundle:nil];
    //
    //    UserProfileVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    
    ShowUserProfileVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ShowUserProfileVC"];
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)mySetting_Action
{
    NSLog(@"My Setting Action");
    
    // SettingsViewController
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SettingsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    
    
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
}
-(void)myHelp_Action
{
    NSLog(@"My Help Action");
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    HelpViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}
-(void)accountSettingAction
{
    NSLog(@"My account setting Action");
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SecuritySettingVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"SecuritySettingVC"];
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}



-(IBAction)btnBackClicked:(id)sender
{
    //    NSLog(@"inside back");
    //
    //    [self.navigationController popViewControllerAnimated:YES];
    
    
    //#import "FavouriteTabVC.h"
    //#import "AllServicesTabVC.h"
    
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    
    
    for (UIViewController *aViewController in allViewControllers)
    {
        
        
        if ([aViewController isKindOfClass:[ScrollNotificationVC class]])
            
        {
            
            [self.navigationController popToViewController:aViewController
                                                  animated:YES];
            
            
        }
        
        
    }
    
}



-(void)viewWillAppear:(BOOL)animated
{
    
    singleton=[SharedManager sharedSingleton];
    
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [super viewWillAppear:NO];
}

-(void)setViewFont{
    lb_noresults.font = [AppFont regularFont:14.0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnSortActionClicked:(UIButton *)sender
{
    btnFilter.selected = NO;
    btnSort.selected = YES;
    [self showSortOptions];
}

- (IBAction)btnFilterClicked:(UIButton*)sender
{
    btnFilter.selected = YES;
    btnSort.selected = NO;
    
    [self showFilterOptions];
}

#pragma mark- Sort Picker

-(void)showSortOptions
{
    
    CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:NSLocalizedString(@"sort", nil) cancelButtonTitle:nil confirmButtonTitle:NSLocalizedString(@"Apply", nil)];
    picker.delegate = self;
    picker.dataSource = self;
    picker.allowMultipleSelection = YES;
    picker.allowRadioButtons = YES;
    picker.isClearOptionRequired = NO;
    [picker show];
    
}

-(void)showFilterOptions
{
    CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:NSLocalizedString(@"filter_caps", nil) cancelButtonTitle:nil confirmButtonTitle:NSLocalizedString(@"Apply", nil)];
    picker.delegate = self;
    picker.dataSource = self;
    picker.allowMultipleSelection = YES;
    picker.isClearOptionRequired = NO;
    picker.allowRadioButtons = YES;
    [picker show];
    
}




- (NSAttributedString *)czpickerView:(CZPickerView *)pickerView
               attributedTitleForRow:(NSInteger)row
{
    NSAttributedString *att;
    
    if ([btnSort isSelected])
    {
        att = [[NSAttributedString alloc]
               initWithString:NSLocalizedString(arrSortOptions[row], nil)
               attributes:@{
                            NSFontAttributeName:[UIFont fontWithName:@"Avenir-Light" size:18.0]
                            }];
        
        
    }
    else
    {
        att = [[NSAttributedString alloc]
               initWithString:NSLocalizedString(arrFilterOptions[row], nil)
               attributes:@{
                            NSFontAttributeName:[UIFont fontWithName:@"Avenir-Light" size:18.0]
                            }];
    }
    
    return att;
}

- (NSString *)czpickerView:(CZPickerView *)pickerView
               titleForRow:(NSInteger)row
{
    if ([btnFilter isSelected])
    {
        return arrFilterOptions[row];
    }
    else
        
        return arrSortOptions[row];
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView
{
    if ([btnFilter isSelected])
        return arrFilterOptions.count;
    
    
    else
    {
        return arrSortOptions.count;
    }
}


/* picker item image for each row */
//- (UIImage *)czpickerView:(CZPickerView *)pickerView imageForRow:(NSInteger)row
//{
//    if ([btnFilter isSelected])
//    {
//        UIImage *imageIcon = nil;
//        switch (row) {
//            case 0:
//                imageIcon = [UIImage imageNamed:@"icon_alphabatic-1"];
//                break;
//            case 1:
//                imageIcon = [UIImage imageNamed:@"icon_nearby"];
//
//                break;
//            case 2:
//                imageIcon = [UIImage imageNamed:@"icon_top_rated"];
//
//                break;
//
//            default:
//                break;
//
//    }
//    }
//    else
//    {
//
//    }
//


//  return imageIcon;
//}


- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemsAtRows:(NSArray *)rows
{
    
    
    
}

- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView {
    NSLog(@"Canceled.");
}


- (IBAction)btnSettingAgainClicked:(id)sender
{
    
    if ([self.delegate respondsToSelector:@selector(filterChanged:andCategoryDict:)])
    {
        
        for (int i = 0; i< self.notificationBOArray.count; i++)
        {
            NotificationItemBO *objBo = [self.notificationBOArray objectAtIndex:i];
            
            if ([[self.dictFilterParams objectForKey:@"category_type"] containsObject:objBo.titleService])
            {
                objBo.isServiceSelected = YES;
            }
            else
            {
                objBo.isServiceSelected = NO;
            }
        }
        
        
        NSMutableDictionary *categoryDict = [NSMutableDictionary new];
        
        if ([self.dictFilterParams valueForKey:@"state_name"] != nil)
        {
            [categoryDict setValue:[self.dictFilterParams valueForKey:@"state_name"] forKey:@"selectedStates"];
            
        }
        
        
        if ([self.dictFilterParams valueForKey:@"notification_type"] != nil)
        {
            [categoryDict setValue:[self.dictFilterParams objectForKey:@"notification_type"] forKey:@"selectedNotification"];
        }
        else
        {
            [categoryDict setValue:NSLocalizedString(@"all", nil) forKey:@"selectedNotification"];
        }
        
        
        [self.delegate filterChanged:self.notificationBOArray andCategoryDict:categoryDict];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation duration:(NSTimeInterval)duration
{
    [_tblSearchFilter reloadData];
}


#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*
 #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }
 */
@end

