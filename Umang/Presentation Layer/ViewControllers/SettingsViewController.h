//
//  SettingsViewController.h
//  Umang
//
//  Created by spice on 16/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SettingsViewController : UIViewController<UINavigationBarDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate>

{
    SharedManager *singleton;
   
    
}
@property (weak, nonatomic) IBOutlet UIButton *btnReset;

@property (weak, nonatomic) IBOutlet UILabel *lblSettings;
@property (weak, nonatomic) IBOutlet UIButton *btnBackMore;
@property(retain,nonatomic)NSString *header_title_pass;
@property(retain,nonatomic)NSMutableArray *arr_table_pass;
@property(retain,nonatomic)NSString *TAG_pass;

-(IBAction)backBtnAction:(id)sender;
-(IBAction)resetBtnAction:(id)sender;

@end