//
//  NewLanguageSelectVC.h
//  Umang
//
//  Created by Rashpinder on 14/02/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#pragma mark - Language Model

@interface LanguageModel:NSObject
@property(strong,nonatomic)NSString *localName;
@property(strong,nonatomic)NSString *name;
@property(strong,nonatomic)NSString *engName;
- (instancetype)initCustomize:(NSString*)localName name:(NSString*)name engName:(NSString*)engName;
@end
#pragma mark - NewLanguageSelectVC 

@interface NewLanguageSelectVC : UIViewController
{
    __weak IBOutlet UIButton *btnBack;
    __weak IBOutlet UITableView *tblLangSelect;
    
    __weak IBOutlet UIButton *btnNext;
    __weak IBOutlet UILabel *lblTitle;
}
@property(strong,nonatomic)NSString *sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tblBottomCons;
@property(copy) void(^LanguageSelect)(LanguageModel *model);

@end
