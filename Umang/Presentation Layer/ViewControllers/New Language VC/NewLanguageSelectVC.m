//
//  NewLanguageSelectVC.m
//  Umang
//
//  Created by Rashpinder on 14/02/18.
//  Copyright © 2018 SpiceDigital. All rights reserved.
//

#import "NewLanguageSelectVC.h"
#import "LanguageTableCell.h"
#import "LanguageBO.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "CZPickerView.h"
#import "NotificationFilterVC.h"
#import "NSBundle+Language.h"
#import "Constant.h"
#import "UmangEulaVC.h"
#import "NSBundle+Language.h"
#import "AppDelegate.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "UIView+Toast.h"
#import "StateList.h"
#import "SharedManager.h"
#import "MBProgressHUD.h"
#import "RunOnMainThread.h"
@implementation LanguageModel
- (instancetype)initCustomize:(NSString *)localName name:(NSString *)name engName:(NSString *)engName
{
    self = [super init];
    if (self)
    {
        self.localName = localName;
        self.name = name;
        self.engName = engName;
    }
    return self;
}

@end

@interface NewLanguageSelectVC ()<UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray* arrLanguageNames;
    NSUInteger selectedIndex;
    
}
@end

@implementation NewLanguageSelectVC

- (void)viewDidLoad {
    [super viewDidLoad];
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:LANGUAGE_SELECTION_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    // Update Key for Language Screen
    
    [self prepareTempDataForLanguageType];
    if ([_sender isEqualToString:@"SettingsViewController"] || [_sender isEqualToString:@"LoginAppVC"]) {
        [btnBack setHidden:false];
        [[self.view viewWithTag:102] setHidden:false];
        [[self.view viewWithTag:101] setHidden:true];
        [btnNext setHidden:true];
        _tblBottomCons.constant = 0.0;
    }else {
        [btnBack setHidden:true];
        [[self.view viewWithTag:102] setHidden:true];
        [[self.view viewWithTag:101] setHidden:false];
        [btnNext setHidden:false];
        
        _tblBottomCons.constant = 45.0;
        
        if (self.view.frame.size.height == 812)
        {
            _tblBottomCons.constant = 80.0;
        }
        
        [[NSUserDefaults standardUserDefaults] setInteger:kLanguageScreenCase forKey:kInitiateScreenKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    [self setViewFont];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSString *existingLanguageSaved = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    __weak __typeof(self) weakSelf = self;

    [arrLanguageNames enumerateObjectsUsingBlock:^(LanguageModel*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.localName isEqualToString:existingLanguageSaved]) {
             selectedIndex = idx;
            [weakSelf updateLanguageSelected];
            *stop = YES;    // Stop enumerating
            return;
         }
    }];
    NSString *selectedLanguageName = [[NSUserDefaults standardUserDefaults] objectForKey:@"KEY_PREFERED_LANGUAGENAME"];
    NSLog(@"========> language selected in language=%@",existingLanguageSaved);
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    [self setNeedsStatusBarAppearanceUpdate];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    [self setLocalization];
    [btnNext setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnNext.layer.cornerRadius = 3.0f;
    btnNext.clipsToBounds = YES;
    [btnNext setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    lblTitle.font = [AppFont semiBoldFont:17];
    btnNext.titleLabel.font = [AppFont mediumFont:17.0];
    UILabel *lbl =(UILabel*)[self.view viewWithTag:101];
    lbl.font = [AppFont semiBoldFont:18];
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}
-(void)setLocalization {
    lblTitle.text = NSLocalizedString(@"language", nil);
    [lblTitle sizeToFit];
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    [btnNext setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    if (btnBack.isHidden) {
        UILabel *lbl =(UILabel*)[self.view viewWithTag:101];
        lbl.text = NSLocalizedString(@"choose_your_language", nil);
        [lbl sizeToFit];
        lbl.textColor = [UIColor colorWithRed:0 green:89.0/255.0 blue:157.0/255.0 alpha:1];
    }
    [self.view layoutIfNeeded];
    [self.view updateConstraintsIfNeeded];
}
-(void)prepareTempDataForLanguageType
{
    
    arrLanguageNames = [NSMutableArray new];
     //*objService = [[LanguageBO alloc] init];
    //objService.languageName = @"English";
    //objService.localeName = @"en";
    [arrLanguageNames addObject:[[LanguageModel alloc] initCustomize:@"en" name:@"English" engName:@"English"]];
   // objService = nil;
    [arrLanguageNames addObject:[[LanguageModel alloc] initCustomize:@"hi-IN" name:@"हिंदी" engName:@"Hindi"]];
    [arrLanguageNames addObject:[[LanguageModel alloc] initCustomize:@"as-IN" name:@"অসমীয়া" engName:@"Assamese"]];
     [arrLanguageNames addObject:[[LanguageModel alloc] initCustomize:@"bn-IN" name:@"বাংলা" engName:@"Bengali"]];
      [arrLanguageNames addObject:[[LanguageModel alloc] initCustomize:@"gu-IN" name:@"ગુજરાતી" engName:@"Gujarati"]];
     [arrLanguageNames addObject:[[LanguageModel alloc] initCustomize:@"kn-IN" name:@"ಕನ್ನಡ" engName:@"Kannada"]];
     [arrLanguageNames addObject:[[LanguageModel alloc] initCustomize:@"ml-IN" name:@"മലയാളം" engName:@"Malayalam"]];
    [arrLanguageNames addObject:[[LanguageModel alloc] initCustomize:@"mr-IN" name:@"मराठी" engName:@"Marathi"]];
    
   [arrLanguageNames addObject:[[LanguageModel alloc] initCustomize:@"or-IN" name:@"ଓଡ଼ିଆ" engName:@"Oriya"]];
  
     [arrLanguageNames addObject:[[LanguageModel alloc] initCustomize:@"pa-IN" name:@"ਪੰਜਾਬੀ" engName:@"Punjabi"]];
   [arrLanguageNames addObject:[[LanguageModel alloc] initCustomize:@"ta-IN" name:@"தமிழ்" engName:@"Tamil"]];
   [arrLanguageNames addObject:[[LanguageModel alloc] initCustomize:@"te-IN" name:@"తెలుగు" engName:@"Telugu"]];
      [arrLanguageNames addObject:[[LanguageModel alloc] initCustomize:@"ur-IN" name:@"اردو" engName:@"Urdu"]];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)didTapBackButtonAction:(UIButton *)sender {
    if ([_sender isEqualToString:@"SettingsViewController"] ) {
        [self.navigationController popViewControllerAnimated:true];
    }
    else if ( [_sender isEqualToString:@"LoginAppVC"])
    {
        [self dismissViewControllerAnimated:true completion:nil];
    }
   // [self.navigationController popViewControllerAnimated:true];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)didTapNextButtonAction:(UIButton *)sender {
    
    [[SharedManager sharedSingleton] traceEvents:@"Next Button" withAction:@"Clicked" withLabel:@"Language Select" andValue:0];
    
    NSString *existingLanguageSaved = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    __block LanguageModel *selectedLanguage = nil ;
    [arrLanguageNames enumerateObjectsUsingBlock:^(LanguageModel*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.localName isEqualToString:existingLanguageSaved]) {
            selectedLanguage  = obj;
            //[self updateLanguageSelected:obj];
            *stop = YES;    // Stop enumerating
            return;
        }
    }];
    
    [[NSUserDefaults standardUserDefaults] setObject:selectedLanguage.localName forKey:KEY_PREFERED_LOCALE];
    NSLog(@"========> selectedLanguage.localeName selected in tour=%@",selectedLanguage.localName);
    [NSBundle setLanguage:selectedLanguage.localName];
    SharedManager *singleton = [SharedManager sharedSingleton];
    if ([selectedLanguage.localName isEqualToString:@"ur-IN"])
    {
        singleton.isArabicSelected = YES;
    }
    else
    {
        singleton.isArabicSelected = NO;
    }
    //added as it is in next button action in eula screen
    [[NSUserDefaults standardUserDefaults] setInteger:kTutorialScreenCase forKey:kInitiateScreenKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:vc animated:NO completion:nil];
}

#pragma mark - Table View data Source ____
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  arrLanguageNames.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellNewLanguage" forIndexPath:indexPath];
    UIImageView *image =(UIImageView*)[cell viewWithTag:101] ;
    UILabel *lblName = (UILabel*)[cell viewWithTag:102];
    LanguageModel *model = [arrLanguageNames objectAtIndex:indexPath.row];
    image.image = nil ;
    NSMutableAttributedString *title = nil;
    
    if (selectedIndex == indexPath.row && ![model.localName isEqualToString:@"en"]) {
        title = [self getLangName:[NSString stringWithFormat:@"%@ \n",model.name] engName:model.engName selected:true];
        image.image = [UIImage imageNamed:@"langTick"];
    }else {
        title = [self getLangName:[NSString stringWithFormat:@"%@ \n",model.name] engName:model.engName selected:false];
        image.image = nil;
    }
    lblName.backgroundColor = [UIColor clearColor];
    lblName.numberOfLines = 3;
    lblName.attributedText = title;
    if ([model.localName isEqualToString:@"en"]) {
        lblName.attributedText = nil;
        lblName.textColor = [UIColor lightGrayColor];
        lblName.font = [AppFont regularFont:16];
        if (selectedIndex == 0) {
             lblName.font = [AppFont mediumFont:16];
            lblName.textColor = [UIColor colorWithRed:0 green:89.0/255.0 blue:157.0/255.0 alpha:1];
             image.image = [UIImage imageNamed:@"langTick"];
        }
        lblName.text = model.name;
    }
    
    return cell;
}
-(NSMutableAttributedString*)getLangName:(NSString*)localizedName engName:(NSString*)engName selected:(BOOL)selected {
    NSMutableAttributedString *lang = [[NSMutableAttributedString alloc] initWithString:localizedName attributes:@{NSFontAttributeName : [AppFont regularFont:16] , NSForegroundColorAttributeName: [UIColor grayColor]}];
    NSMutableAttributedString *eng = [[NSMutableAttributedString alloc] initWithString:engName attributes:@{NSFontAttributeName : [AppFont lightFont:15],NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
    if (selected) {
        lang = [[NSMutableAttributedString alloc] initWithString:localizedName attributes:@{NSFontAttributeName : [AppFont mediumFont:16] , NSForegroundColorAttributeName: [UIColor colorWithRed:0 green:89.0/255.0 blue:157.0/255.0 alpha:1]}];
        eng = [[NSMutableAttributedString alloc] initWithString:engName attributes:@{NSFontAttributeName : [AppFont lightFont:15],NSForegroundColorAttributeName: [UIColor darkTextColor]}];
    }
    
    [lang appendAttributedString:eng];
    return lang;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([AppFont isIPad]) {
        return 60;
    }
    return 50;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([_sender isEqualToString:@"SettingsViewController"] ) {
        __weak __typeof(self) weakSelf = self;
        [RunOnMainThread runBlockInMainQueueIfNecessary:^{
            [weakSelf languageChangedAction:indexPath.row];
        }];
    }
    else if ( [_sender isEqualToString:@"LoginAppVC"])
    {
        selectedIndex = indexPath.row;

        [self updateLanguageSelected];
        self.LanguageSelect([arrLanguageNames objectAtIndex:selectedIndex]);
        [self dismissViewControllerAnimated:true completion:nil];
    }else {
        selectedIndex = indexPath.row;
        [self updateLanguageSelected];
        [tblLangSelect reloadData];
    }
    
}
-(void)updateLanguageSelected {
     LanguageModel *model = [arrLanguageNames objectAtIndex:selectedIndex];
    [[NSUserDefaults standardUserDefaults] setObject:model.localName forKey:KEY_PREFERED_LOCALE];
    [NSBundle setLanguage:model.localName];
    SharedManager *singleton = [SharedManager sharedSingleton];
    singleton.languageSelected = model.name;
    if ([model.localName isEqualToString:@"ur-IN"])
    {
        singleton.isArabicSelected = YES;
    }
    else
    {
        singleton.isArabicSelected = NO;
    }
    [self setLocalization];
}
-(void)languageChangedAction:(NSUInteger)index {
    NSString *titlestr=NSLocalizedString(@"change_language_dialog_msg", nil);
    NSString *msg=NSLocalizedString(@"change_language_dialog_msg2", nil);
    __weak __typeof(self) weakSelf = self;

    UIAlertController *okAlertCntrlr = [UIAlertController alertControllerWithTitle:titlestr message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAlert = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){
        if (![self connected])
        {
            // Not connected
            NSLog(@"Device is not connected to the Internet");
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"network_error_txt",nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok",nil), nil];
            [alert show];
            return ;
        }
        selectedIndex = index;
        [weakSelf updateLanguageSelected];
        [weakSelf hitInitAPI];
//        weakSelf.LanguageSelect([arrLanguageNames objectAtIndex:selectedIndex]);
        }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    [okAlertCntrlr addAction:okAlert];
    [okAlertCntrlr addAction:cancelAction];
    [self presentViewController:okAlertCntrlr animated:YES completion:nil];
}


- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}
-(void)hitInitAPI
{
   MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    __block SharedManager* singleton = [SharedManager sharedSingleton];
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:@"" forKey:@"lang"];
    
    NSString *userToken;
    
    if (singleton.user_tkn == nil || singleton.user_tkn.length == 0)
    {
        userToken = @"";
    }
    else
    {
        userToken = singleton.user_tkn;
    }
    
    [dictBody setObject:userToken forKey:@"tkn"];
    __weak __typeof(self) weakSelf = self;

    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_INIT withBody:dictBody andTag:TAG_REQUEST_INIT completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            singleton.arr_initResponse=[[NSMutableArray alloc]init];
            singleton.arr_initResponse=[response valueForKey:@"pd"];
            NSLog(@"singleton.arr_initResponse = %@",singleton.arr_initResponse);
            
            [[NSUserDefaults standardUserDefaults] setObject:singleton.arr_initResponse forKey:@"InitAPIResponse"];
            
            NSString*  abbr=[singleton.arr_initResponse valueForKey:@"abbr"];
            NSLog(@"value of abbr=%@",abbr);
            
            
            if ([abbr length]==0) {
                
                abbr=@"";
                
            }
            
            singleton.user_StateId = [singleton.arr_initResponse valueForKey:@"ostate"];
            [singleton setStateId:singleton.user_StateId];
            
            NSString *emblemString = [singleton.arr_initResponse valueForKey:@"stemblem"];
            emblemString = emblemString.length == 0 ? @"":emblemString;
            [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
            
            [[NSUserDefaults standardUserDefaults] setObject:abbr forKey:@"ABBR_KEY"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
            //------------------------- Encrypt Value------------------------
            [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
            // Encrypt
            [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"lastFetchDate"];
            [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"lastFetchV1"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            //------------------------- Encrypt Value------------------------
            
            NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
            NSHTTPCookie *cookie;
            for (cookie in [storage cookies]) {
                
                [storage deleteCookie:cookie];
                
            }
            NSMutableArray *cookieArray = [[NSMutableArray alloc] init];
            [[NSUserDefaults standardUserDefaults] setValue:cookieArray forKey:@"cookieArray"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [singleton.dbManager deleteBannerHomeData];
            [singleton.dbManager  deleteAllServices];
            [singleton.dbManager  deleteSectionData];
            [RunOnMainThread runBlockInMainQueueIfNecessary:^{
                [weakSelf homeviewJump];
            }];
           // [self performSelector:@selector(homeviewJump) withObject:nil afterDelay:0.1];
            
            
            
            // jump to home view  tab
            
            /* facebooklink
             faq
             forceupdate
             googlepluslink
             opensource
             privacypolicy
             splashScreen
             tabordering
             termsandcondition
             twitterlink
             ver
             vermsg
             */
            
            //------ save value in nsuserdefault for relanch app
            // [[NSUserDefaults standardUserDefaults] setObject:response forKey:@"TOUR_Key"];
            //[[NSUserDefaults standardUserDefaults] synchronize];
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
             message:error.localizedDescription
             delegate:self
             cancelButtonTitle:@"OK"
             otherButtonTitles:nil];
             [alert show];*/
        }
        
    }];
    
}

-(void)homeviewJump
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    [appDelegate updateAppLanguage];
}
@end
