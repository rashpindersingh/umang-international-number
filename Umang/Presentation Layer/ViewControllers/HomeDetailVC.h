//
//  HomeDetailVC.h
//  Umang
//
//  Created by spice_digital on 29/09/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
@import MediaPlayer;

@interface HomeDetailVC : UIViewController<UIWebViewDelegate,MFMessageComposeViewControllerDelegate>
{
    IBOutlet UIView *vw_support;
    IBOutlet UIView *vw_menu;
    IBOutlet UIView *vw_recent;
    IBOutlet UITableView *tbl_recent;
    
    
    //============ebook download=========
    IBOutlet UILabel *lbl_downloadingtxt;
    IBOutlet UIView *vw_download;
    
    //------------------For downloadViewTable--------------------
    IBOutlet UITableView *tbldonwload;
    IBOutlet UILabel *lbl_download;
    IBOutlet UIView *vw_filedownload;
    IBOutlet UIView *vw_filedownloadManager;
    int tagvw_tbl;
    
    
    //=====================================
}
-(IBAction)closeBtn:(id)sender;
//------------------For downloadViewTable--------------------
@property(nonatomic, retain)IBOutlet UILabel *lbl_downloadcomplete;
@property(nonatomic, retain)IBOutlet UILabel *lbl_downloadbook;


-(IBAction)clickonDownloadview:(id)sender;
@property(nonatomic,retain)NSMutableArray*downloadBookArry;
@property(nonatomic,weak)IBOutlet UIProgressView *vw_progressBar;

//=====================================




@property (nonatomic, retain) IBOutlet UIBarButtonItem *web_back_btn;


@property(nonatomic,retain)NSString *tagComeFrom;


@property(nonatomic,retain)NSString *isStateSelected;
@property(nonatomic,retain)NSString *state_nametopass;
@property(nonatomic,retain)NSString *state_idtopass;



-(IBAction)bgtouch:(id)sender;

-(IBAction)btn_home_vw_recent:(id)sender;

-(IBAction)btn_searchAction:(id)sender;
-(IBAction)btn_information:(id)sender;



-(IBAction)btn_map_vw_menu:(id)sender;
-(IBAction)btn_rate_vw_menu:(id)sender;
-(IBAction)btn_share_vw_menu:(id)sender;
-(IBAction)btn_chat_vwSupport:(id)sender;







-(IBAction)btn_call_vwSupport:(id)sender;


-(IBAction)backBtnAction:(id)sender;
-(IBAction)customerCareBtnAction:(id)sender;
-(IBAction)recentBtnAction:(id)sender;
-(IBAction)moreBtnAction:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lbltitle;
@property (retain, nonatomic) NSString *urlString;
@property (weak, nonatomic) NSString *titleStr;

@property (retain, nonatomic) NSDictionary *dic_serviceInfo;

@property (weak, nonatomic) IBOutlet UIButton *backBtnHome;

@end
