//
//  ProfileForRegisterWithAadhar.m
//  Umang
//
//  Created by admin on 05/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "ProfileForRegisterWithAadhar.h"
#import "ProfileCell.h"
#import "MBProgressHUD.h"


@interface ProfileForRegisterWithAadhar ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *arrGeneralInformation;
    NSMutableArray *arrAccountInformation;
    UIView *vwFooter;
    SharedManager *singleton;
    MBProgressHUD *hud ;
    
    
    //----- local values  to be save and show from API-----------
    NSString *str_name;
    NSString *str_gender;
    NSString *str_dob;
    NSString *str_qualification;
    NSString *str_occupation;
    NSString *str_state;
    NSString *str_district;
    NSString *str_registerMb;
    NSString *str_emailAddress;
    NSString *str_alternateMb;
    NSString *str_emailVerifyStatus;
    NSString *str_amnosVerifyStatus;
    NSString *str_address;
    
    NSString *str_Url_pic;
    
    NSMutableArray *socialpd;
    //-----------------------------------------
    
    
    
    NSMutableArray *arrDataGeneralInformation;
    NSMutableArray *arrDataAccountInformation;
    
    UIImageView *imageUserProfile;
}
@property (weak, nonatomic) IBOutlet UITableView *tblProfileFromAadhar;

@end

@implementation ProfileForRegisterWithAadhar

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: PROFILE_FOR_REGISTRATION_WITH_AADHAR];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];


    
    [_tblProfileFromAadhar reloadData];
    
    
    
    
    
    arrGeneralInformation = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"name_caps", nil),NSLocalizedString(@"gender_caps", nil),NSLocalizedString(@"date_of_birth_caps", nil),NSLocalizedString(@"qualication_caps", nil),NSLocalizedString(@"occupation_caps", nil),NSLocalizedString(@"state_txt_caps", nil),NSLocalizedString(@"district_caps", nil),NSLocalizedString(@"address_caps", nil), nil];
    
    arrAccountInformation = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"email_address", nil),NSLocalizedString(@"alt_mob_num", nil), nil];



}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *vwHeaderTitle = [[UIView alloc]initWithFrame:CGRectMake(0, 64, _tblProfileFromAadhar.frame.size.width, 50.0)];
    [_tblProfileFromAadhar addSubview:vwHeaderTitle];
    vwHeaderTitle.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:234.0/255.0 blue:241.0/255.0 alpha:1.0];
    

    UILabel *lblSectionTitle = [[UILabel alloc]initWithFrame:CGRectMake(15, 20, 200, 30.0)];
    [vwHeaderTitle addSubview:lblSectionTitle];
    lblSectionTitle.textColor = [UIColor grayColor];
    lblSectionTitle.font = [UIFont systemFontOfSize:14.0];
    lblSectionTitle.adjustsFontSizeToFitWidth = YES;
    
    
    //for grey header Design
    
    
        if (section == 0)
        {
            // vwGray.hidden = YES;
            
            lblSectionTitle.text =  NSLocalizedString(@"general_information", nil);
        }
        else
            
        {
            // vwGray.hidden = NO;
            lblSectionTitle.text =  NSLocalizedString(@"account_information", nil);
        }
    
    
    
    
    return vwHeaderTitle;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75.0;
}



-(UIView*)designFooterView
{
    vwFooter = [[UIView alloc]initWithFrame:CGRectMake(0,_tblProfileFromAadhar.frame.size.height-150, _tblProfileFromAadhar.frame.size.width, 150.0)];
    [_tblProfileFromAadhar addSubview:vwFooter];
    vwFooter.backgroundColor = [UIColor whiteColor];
    
    UIButton *btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNext setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [btnNext setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnNext.layer.cornerRadius = 3.0f;
    btnNext.clipsToBounds = YES;
    
    if (self.view.frame.size.width < 500)
    {
        btnNext.frame=CGRectMake(30 ,20, self.view.frame.size.width - 60, 50);
    }
    else
    {
        btnNext.frame=CGRectMake(200 ,20, self.view.frame.size.width - 400, 65);
    }
    
    [btnNext setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];
    [btnNext addTarget:self action:@selector(btnNextAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [vwFooter addSubview:btnNext];
    
    
    
    
    UIButton *btnskip = [[UIButton alloc]initWithFrame:CGRectMake((_tblProfileFromAadhar.frame.size.width/2)-100, vwFooter.frame.size.height -50, 200, 30)];
    
    [btnskip setTitle:NSLocalizedString(@"skip", nil) forState:UIControlStateNormal];
    [btnskip setTintColor:[UIColor blueColor]];
    [btnskip setTitleColor:[UIColor colorWithRed:10.0/255.0 green:90.0/255.0 blue:157.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    btnskip.titleLabel.font = [UIFont boldSystemFontOfSize:13.0];
    [vwFooter addSubview:btnskip];
    [btnskip addTarget:self action:@selector(btnSkipAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return vwFooter;
    
}
- (IBAction)btnSkipAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
     //tbc.selectedIndex=[SharedManager getSelectedTabIndex];
    tbc.selectedIndex=0;

    [self presentViewController:tbc animated:NO completion:nil];
}




- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    return 0.001;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;              // Default is 1 if not implemented
{
    return 2;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    
    if (section == 0) {
        return  arrGeneralInformation.count;
    }
    else{
        return arrAccountInformation.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    
    static NSString *CellIdentifier = @"ProfileCell";
    ProfileCell *profileCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    
    
    if (indexPath.section == 0)
    {
        profileCell.lblName.text = [arrGeneralInformation objectAtIndex:indexPath.row];
        
        profileCell.txtNameFields.text= [arrDataGeneralInformation objectAtIndex:indexPath.row];
        
        profileCell.btn_verify.hidden=TRUE;
        profileCell.btn_verify.enabled=FALSE;
        
        
    }
    else
    {
        profileCell.lblName.text = [arrAccountInformation objectAtIndex:indexPath.row];
        profileCell.txtNameFields.text= [arrDataAccountInformation objectAtIndex:indexPath.row];
        if (indexPath.row==0)
        {
            if ([str_emailVerifyStatus isEqualToString:@"0"])
            {
                
                if ([str_emailAddress length]!=0)
                {
                    
                    profileCell.btn_verify.hidden=FALSE;
                    profileCell.btn_verify.enabled=TRUE;
                }
                else
                {
                    profileCell.btn_verify.hidden=TRUE;
                    profileCell.btn_verify.enabled=FALSE;
                    
                    
                }
                
            }
            else
            {
                profileCell.btn_verify.hidden=FALSE;
                profileCell.btn_verify.enabled=FALSE;
                
                
            }
        }
        else
        {
            profileCell.btn_verify.hidden=TRUE;
            profileCell.btn_verify.enabled=FALSE;
            
            
        }
        
    }
    
    [profileCell.btn_verify addTarget:self action:@selector(resendAction:) forControlEvents:UIControlEventTouchUpInside];
    
    profileCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return profileCell;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
