//
//  RateUsVCViewController.m
//  AES256EnDeCrypt
//
//  Created by spice on 10/11/16.
//  Copyright (c) 2016 deepak singh rawat. All rights reserved.
//

#import "RateUsVCViewController.h"
#import "AMRatingControl.h"
#import "UMAPIManager.h"
#import "ReportIssueVC.h"
#import "FeedbackVC.h"
#import "UIView+Toast.h"

//#define YOUR_APP_STORE_ID @"389801252" //Change this one to your ID
#define YOUR_APP_STORE_ID @"1236448857"
static NSString *const iOS7AppStoreURLFormat = @"itms-apps://itunes.apple.com/app/id%@";
static NSString *const iOSAppStoreURLFormat = @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@";

static NSString *const iOS8AppStoreURLFormat =@"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%d&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software";

@interface RateUsVCViewController ()
{
    //  IBOutlet UIView *vw_RateUs;
    IBOutlet UIImageView *img_Emo;;
    IBOutlet UIButton *btn_submit;
    
    __weak IBOutlet UILabel *lblRateUs;
    NSInteger rateCheck;
    
    
}



@end

@implementation RateUsVCViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (IBAction)btnCrossClicked:(id)sender
{
    [UIView animateWithDuration:0.4 animations:^{
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        // [self.navigationController popViewControllerAnimated:YES];
        
    }];
    
    
    
}

- (void)viewDidLoad
{
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: RATE_US_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    self.vw_RateUs.frame = CGRectMake(0, 0, 300, 175);
    self.vw_RateUs.center = self.view.center;
    
    
    lblRateUs.text = NSLocalizedString(@"rate_us", nil);
    [btn_submit setTitle:NSLocalizedString(@"submit", nil) forState:UIControlStateNormal];
    self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7 ];// - and max rating
    UIImage *dot, *star;
    dot = [UIImage imageNamed:@"star_border"];
    star = [UIImage imageNamed:@"star_filled"];
    
    CGRect ratinFrame = CGRectMake(0,0,(5 * 25),25);
    ratinFrame.origin.x = ((_vw_RateUs.frame.size.width  / 2) - 12.5) - (ratinFrame.size.width / 2) ;
    AMRatingControl *imagesRatingControl = [[AMRatingControl alloc] initWithLocation:CGPointMake(ratinFrame.origin.x,_vw_RateUs.frame.size.height/2.5)emptyImage:dot solidImage:star andMaxRating:5];
   // AMRatingControl *imagesRatingControl = [[AMRatingControl alloc] initWithLocation:CGPointMake(_vw_RateUs.frame.size.width/2+5,_vw_RateUs.frame.size.height/2)emptyImage:dot solidImage:star andMaxRating:5];
    
    
    // imagesRatingControl.frame = CGRectMake(_vw_RateUs.frame.size.width/2, _vw_RateUs.frame.size.height/2, 200, 100);
    
    
    imagesRatingControl.center = CGPointMake(_vw_RateUs.frame.size.width/2+5,_vw_RateUs.frame.size.height/2-20);
    
    
    imagesRatingControl.tag = 8989;
    
    rateCheck=5;
    [imagesRatingControl setRating:rateCheck];
    img_Emo.image=[UIImage imageNamed:@"5_star"] ;
    
    
    imagesRatingControl.editingDidEndBlock = ^(NSUInteger rating)
    {
        NSLog(@"editingDidEndBlock %@",[NSString stringWithFormat:@"%lu_star", (unsigned long)rating]);
        
        if (rating <=5 && rating>0) {
            
            NSString* setimg=  [NSString stringWithFormat:@"%lu_star", (unsigned long)rating];
            rateCheck=rating;
            
            img_Emo.image=[UIImage imageNamed:setimg] ;
        }
        else
        {
            img_Emo.image=[UIImage imageNamed:@"1_star"] ;
            rateCheck=0;
        }
        
    };
    
    
    // Add the control(s) as a subview of your view
    [_vw_RateUs addSubview:imagesRatingControl];
    
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}



-(void)callfeedback
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FeedbackVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FeedbackVC"];
    vc.jumpFromVC=@"AppstoreRating";
    vc.ratingFromFeedback = [NSString stringWithFormat:@"%ld",rateCheck];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (IBAction)btnBackClicked:(id)sender {
    
    // [self.navigationController popViewControllerAnimated:YES];
    [self btnCrossClicked:self];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    //------------- Network View Handle------------
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"TABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    
    
    
    
    
    
    // [self setNeedsStatusBarAppearanceUpdate];
    //  [self performSelector:@selector(setHeightOfTableView) withObject:nil afterDelay:.1];
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(btnCrossClicked:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.navigationController.view addGestureRecognizer:gestureRecognizer];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    [self setViewFont];
    [super viewWillAppear:NO];
}

#pragma mark- Font Set to View
-(void)setViewFont
{
    
    lblRateUs.font = [AppFont mediumFont:23];
    btn_submit.titleLabel.font = [AppFont regularFont:16.0];
    
}
#pragma mark -
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)showToast :(NSString *)toast
{
    [self.view makeToast:toast duration:5.0 position:CSToastPositionBottom];
}


- (IBAction)btnSubmitClicked:(id)sender
{
    if (rateCheck ==0)
    {
        [self showToast:NSLocalizedString(@"please_select_stars_to_rate", nil)];
    }
    else if (rateCheck <=3 && rateCheck>0) {
        
        [self btnCrossClicked:self];
        
        [self callfeedback];
    }
    else
    {
        [self CallWishFeedbackApi];
        //feedback api
        
        /*"thank_you_feedback" = "Thank you for your feedback.";
         "rate_us_appstore"*/
         

        NSString *ratingCombinedText = [NSString stringWithFormat:@"%@\n%@",NSLocalizedString(@"thank_you_feedback", nil),NSLocalizedString(@"rate_us_appstore", nil)];
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:ratingCombinedText preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction* noButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil)  style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                                   {
                                       [UIView animateWithDuration:0.4 animations:^{
                                           self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                                       } completion:^(BOOL finished) {
                                           [self willMoveToParentViewController:nil];
                                           [self.view removeFromSuperview];
                                           [self removeFromParentViewController];
                                           // [self.navigationController popViewControllerAnimated:YES];
                                           
                                       }];
                                   }];
        
        UIAlertAction* yesButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"rate_now", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                    {
                                        //Google Tracking
                                        
                                        id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
                                        [tracker set:kGAIScreenName value: @"RateToAppstore4or5"];
                                        [[GAI sharedInstance].defaultTracker send:
                                         [[GAIDictionaryBuilder createScreenView] build]];
                                        
                                        //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id1236448857"]];;
                                        
                                        //NSString *applink=[NSString stringWithFormat:@"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8",YOUR_APP_STORE_ID];
                                        
                                        
                                        NSString *applink = @"https://itunes.apple.com/in/app/umang/id1236448857?mt=8";
                                        //@"itms-apps://itunes.apple.com/app/id1236448857";
                                        
                                        //https://itunes.apple.com/in/app/umang/id1236448857?mt=8

                                        
                                        [[UIApplication sharedApplication] openURL: [NSURL URLWithString: applink]];
                                        
                                        //itms-apps://itunes.apple.com/app/viewContentsUserReviews?id=AppID //new one
                                        [self.view removeFromSuperview];
                                        
                                    }];
        
        
        [alert addAction:yesButton];
        [alert addAction:noButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
        
        
        
        
        /*
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"rate_us", nil)
         message:@"Do you want to submit your Reviews?"
         delegate:self
         cancelButtonTitle:NSLocalizedString(@"cancel_caps", nil)
         otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
         alert.tag=100;
         [alert show];
         */
        
        
    }
    
}

-(void)CallWishFeedbackApi
{
    SharedManager  *singleton = [SharedManager sharedSingleton];
    if(singleton.user_tkn.length==0)
    {
        singleton.user_tkn=@"";
    }
    NSString *ratingStr=[NSString stringWithFormat:@"%ld",(long)rateCheck];
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:ratingStr forKey:@"feedback"];
    [dictBody setObject:@"appfd" forKey:@"cate"];
    [dictBody setObject:@"" forKey:@"pic"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:@"" forKey:@"email"];
    [dictBody setObject:@"" forKey:@"st"];
    [dictBody setObject:@"" forKey:@"sid"];
    
    [dictBody setObject:@"" forKey:@"serviceID"];
    
    
    [dictBody setObject:@"feedback" forKey:@"catetype"];
    
    
    if (ratingStr.length != 0)
    {
        [dictBody setObject:ratingStr forKey:@"rating"];
    }
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_FEEDBACK withBody:dictBody andTag:TAG_REQUEST_FEEDBACK completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
     {
         
         if (error == nil)
         {
             NSLog(@"Server Response = %@",response);
         }
         else
         {
             NSLog(@"Error Occured = %@",error.localizedDescription);
             
             //NSString *errormsg=[NSString stringWithFormat:@"%@",error.localizedDescription];
             //[self showToast:errormsg];
         }
         
         
         
         
         
     }];
}

-(void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100)
    { // handle the altdev
        
        if (buttonIndex == 1)
        {
            // NSURL*appURL=   [NSURL URLWithString:[NSString stringWithFormat:([[UIDevice currentDevice].systemVersion floatValue] >= 7.0f)? iOS7AppStoreURLFormat: iOSAppStoreURLFormat, YOUR_APP_STORE_ID]]; // Would contain the right link
            
            NSString *applink=[NSString stringWithFormat:@"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8",YOUR_APP_STORE_ID];
            
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString: applink]];
            
            //itms-apps://itunes.apple.com/app/viewContentsUserReviews?id=AppID //new one
            [self.view removeFromSuperview];
        }
        
        else
        {
            [self btnCrossClicked:self];
            
        }
        
    }
    
    
    
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation duration:(NSTimeInterval)duration
{
    self.vw_RateUs.frame = CGRectMake(0, 0, 300, 175);
    self.vw_RateUs.center = self.view.center;
    
    // vw_RateUs.center = self.view.center;
    
    AMRatingControl *imagesRatingControl = (AMRatingControl*)[_vw_RateUs viewWithTag:8989];
    
    //imagesRatingControl.frame = CGRectMake(_vw_RateUs.frame.size.width/2, _vw_RateUs.frame.size.height/2, 200, 100);
    
    imagesRatingControl.center = CGPointMake(_vw_RateUs.frame.size.width/2+5,_vw_RateUs.frame.size.height/2-20);
    
    // self.imagesRatingControl.center =
    
    //    if(UIInterfaceOrientationMaskLandscapeLeft|UIInterfaceOrientationMaskLandscapeRight)
    //      {
    //
    //          vw_RateUs.frame = CGRectMake(300,200, self.view.frame.size.width-600, 200);
    //
    //     }
    //      else
    //      {
    //      }
    
    
    //          AMRatingCaontrol *imagesRatingControl = [[AMRatingControl alloc] initWithLocation:CGPointMake(fDeviceWidth/2+5,fDeviceHeight/2-100)emptyImage:dot solidImage:star andMaxRating:5];
    //          [vw_RateUs addSubview:imagesRatingControl];
    //
    //          rateCheck=5;
    //          [imagesRatingControl setRating:rateCheck];
    //
    
    
    //    img_Emo.frame = CGRectMake(self.view.frame.size.width-125, self.view.frame.size.height-100, 50, 50);
    //    [vw_RateUs addSubview:img_Emo];
    //
    //    lblRateUs.frame = CGRectMake(self.view.frame.size.width-150,50,100, 50);
    // [vw_RateUs addSubview: lblRateUs];
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/

/*
 -(void)hitAPI
 {
 
 UMAPIManager *objRequest = [[UMAPIManager alloc] init];
 
 
 NSMutableDictionary *dictBody = [NSMutableDictionary new];
 
 
 [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_FEEDBACK withBody:dictBody andTag:TAG_REQUEST_FEEDBACK completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
 {
 if (error == nil)
 {
 NSLog(@"Server Response = %@",response);
 //------ save value in nsuserdefault for relanch app
 // [[NSUserDefaults standardUserDefaults] setObject:response forKey:@"TOUR_Key"];
 //[[NSUserDefaults standardUserDefaults] synchronize];
 
 
 }
 else{
 NSLog(@"Error Occured = %@",error.localizedDescription);
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
 message:error.localizedDescription
 delegate:self
 cancelButtonTitle:@"OK"
 otherButtonTitles:nil];
 [alert show];
 }
 
 }];
 
 }
 
 */



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

