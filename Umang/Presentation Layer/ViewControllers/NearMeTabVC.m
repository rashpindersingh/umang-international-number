//
//  AllServicesTabVC.m
//  Umang
//
//  Created by spice on 15/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import "NearMeTabVC.h"
#import "HCSStarRatingView.h"

#import "AllServiceCVCell.h"
#import "CustomImageFlowLayout.h"

#define fDeviceWidth ([UIScreen mainScreen].bounds.size.width)
#define fDeviceHeight ([UIScreen mainScreen].bounds.size.height)

#import "AllServiceCVCellForiPad.h"


#import "RunOnMainThread.h"
#import "AdvertisingColumn.h"
#import "UIImageView+WebCache.h"

#import "StateList.h"
#import "CustomPickerVC.h"
#import "UserProfileVC.h"
#import "ShowUserProfileVC.h"

#import "HomeContainerCell.h"
#import "DetailServiceNewVC.h"
#import "NotificationFilterVC.h"
#import "AdvanceSearchVC.h"
#import "UMAPIManager.h"

#import "ScrollNotificationVC.h"

#import "CustomBadge.h"
#import "BadgeStyle.h"
#import "HomeDetailVC.h"
#import "AddfilterStateVC.h"

#import "ServiceNotAvailableView.h"
#import "MBProgressHUD.h"
static float NV_height= 50;

@interface NearMeTabVC ()<UIActionSheetDelegate>
{
    
    __weak IBOutlet UIView *vwSearchBG;
    
    BOOL flagrotation;
    BOOL flagStatusBar;
    
    IBOutlet UIButton *btn_notification;
    
    NSMutableArray* arrServiceData;
    
    SharedManager *singleton;
    CustomPickerVC *customVC;
    StateList *obj;
    UIActivityIndicatorView *activityView;
    ServiceNotAvailableView *noServiceView;
    dispatch_group_t serviceGroup;
    dispatch_group_t stateApiGroup;
    
    Boolean isViewDidLoad ;
    MBProgressHUD *hud;
    HeaderStateBannerCollReusableView *headerView;
    
}
@property (nonatomic, strong) NSString *profileComplete;
@property (nonatomic, strong)NSString *state_id;
@property (nonatomic, strong)NSString *oldStateId;

@property (nonatomic, strong) HCSStarRatingView *starRatingView;
@property(nonatomic,retain)NSArray *sampleData ;
@property(nonatomic,retain)NSMutableArray *table_data ;
@property(nonatomic,retain)NSDictionary *cellData;
@property(nonatomic,retain)NSMutableDictionary * cellDataOfmore;





@property (nonatomic) CGFloat progress;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) NSArray *progressViews;



@property(nonatomic,retain)UIImageView *imgProfile;
@property(nonatomic,retain)UILabel *lbl_profileComplete;
@property(nonatomic,retain)UILabel *lbl_whiteLine;
@property(nonatomic,retain)UILabel *lbl_percentage;
@property(nonatomic,retain)UIButton *btn_close;
@property(nonatomic,retain)UIButton *btn_clickUpdate;
@property(nonatomic,retain)UIView *NotifycontainerView;
@property (nonatomic, retain)NSString *bannerHide;


@end

@implementation NearMeTabVC
@synthesize allSer_collectionView;
@synthesize cellData;
@synthesize cellDataOfmore;
@synthesize table_data;

@synthesize imgProfile;
@synthesize lbl_profileComplete;
@synthesize lbl_whiteLine;
@synthesize lbl_percentage;
@synthesize btn_close;
@synthesize btn_clickUpdate;
@synthesize NotifycontainerView;
@synthesize profileComplete;


#pragma mark - UIView LifeCycle Methods

-(void)reloadEverything
{
    [self showViewSubview:true];
    [self refreshData];
}

-(void)refreshData
{
    serviceGroup = nil;
    stateApiGroup = nil;
    __weak typeof(self) weakSelf = self;
    [RunOnMainThread runBlockInMainQueueIfNecessary:^{
        [weakSelf startDispatchGroup];
    }];
    //    dispatch_group_notify(stateApiGroup,dispatch_get_main_queue(),^{
    //        // Assess any errors
    //
    //        stateApiGroup = nil ;
    //        // Now call the final completion block
    //    });
    //[self startStateListGroup];
}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

- (void)viewDidLoad
{
    //allSer_collectionView.backgroundColor=[UIColor orangeColor];
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadEverything)
                                                 name:@"ReloadEverything" object:nil];
    
    //====== check for active internet connection to change tabs
        if (![self connected])
        {
            // Not connected
            //------------- Network View Handle------------
            
            NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"TABBAR" forKey:@"CLASSTYPE"];
            [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
            NSLog(@"Device is not connected to the Internet");
        } else
        {
            // Connected. Do some Internet stuff
        }
    
    self.allSer_collectionView.alwaysBounceVertical = YES;
    
    vw_banner.backgroundColor = [UIColor clearColor];
    
    singleton = [SharedManager sharedSingleton];
    [singleton getSelectedTabIndex];
    //    NSInteger selectedTabIndex =  [singleton getSelectedTabIndex];
    isViewDidLoad = false;
    
    
    obj=[[StateList alloc]init];
    
    
    flagStatusBar=FALSE;
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:NEAR_ME_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    self.state_id = @"";
    self.bannerHide = NSLocalizedString(@"yes", nil);
    
    
    vwSearchBG.layer.cornerRadius = 5.0;
    
    
    if (noServiceView == nil)
    {
        noServiceView = [[ServiceNotAvailableView alloc] initWithFrame:vw_noServiceFound.frame];
        [self.view addSubview:noServiceView];
    }
    [noServiceView setBtnSubHeadingTitle:@""];
    
    [noServiceView.btnSubHeading addTarget:self action:@selector(btn_locationAction:) forControlEvents:UIControlEventTouchUpInside];
    
    // noServiceView.lblSubHeading.text = NSLocalizedString(@"no_services_to_display", nil);
    
    NSLog(@"arr init --%@", singleton.arr_initResponse);
    // [noServiceView setBackgroundImageWithUrl:@"https:google.com"];
    
    noServiceView.backgroundColor = vw_noServiceFound.backgroundColor;
    self.view.backgroundColor = vw_noServiceFound.backgroundColor;
    [self.view bringSubviewToFront:noServiceView];
    
    noServiceView.hidden     = YES;
    vw_noServiceFound.hidden = TRUE;
    
    [txt_searchField setValue:[UIColor colorWithRed:171.0/255.0 green:171.0/255.0 blue:171.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    
    self.navigationController.navigationBar.hidden = YES;
    
    //UIEdgeInsets inset = UIEdgeInsetsMake(-20, 0, -50, 0);
    //allSer_collectionView.contentInset = inset;
    
    txt_searchField.placeholder = NSLocalizedString(@"search", @"");
    
    txt_searchField.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    
    //_headerView = [[AdvertisingColumn alloc]init];
    // _headerView.backgroundColor = [UIColor clearColor];
    
    // Register the table cell
    //[allSer_collectionView registerClass:[HomeContainerCell class] forCellReuseIdentifier:@"HomeContainerCell"];
    
    // Add observer that will allow the nested collection cell to trigger the view controller select row at index path
    //  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectItemFromCollectionView:) name:@"didSelectItemFromCollectionView" object:nil];
    
    //btn_location.backgroundColor = [UIColor colorWithRed:250/255.0 green:250/255.0 blue:250/255.0 alpha:1.0];
    
    
    [allSer_collectionView setShowsHorizontalScrollIndicator:NO];
    [allSer_collectionView setShowsVerticalScrollIndicator:NO];
    
    //---------- for getting location-----------
    //locationManager = [[CLLocationManager alloc] init];
    //geocoder = [[CLGeocoder alloc] init];
    
    self.str_city = @"Getting Location...";
    
    self.str_state =@""; //used default gcm id in the login
    
    vw_line.frame=CGRectMake(0, 0, 0, 0.0);
    vw_line.hidden = true;
    // Do any additional setup after loading the view, typically from a nib.
    vwSearchBG.layer.cornerRadius = 5.0;
    [txt_searchField setValue:[UIColor colorWithRed:171.0/255.0 green:171.0/255.0 blue:171.0/255.0 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    allSer_collectionView.delegate = self;
    allSer_collectionView.dataSource = self;
    
    allSer_collectionView.backgroundColor = [UIColor clearColor];
    
    allSer_collectionView.collectionViewLayout = [[CustomImageFlowLayout alloc] init];
    self.navigationController.navigationBar.hidden = YES;
    
    
    vw_line.frame=CGRectMake(0, 0, 0, 0);
    if (!refreshController)
    {
        refreshController = [[UIRefreshControl alloc] init];
        [refreshController addTarget:self action:@selector(getEvents:) forControlEvents:UIControlEventValueChanged];
        [allSer_collectionView addSubview:refreshController];
        // [self getEvents:refreshController];
    }
    [self startStateListGroup];
    
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        [self adjustSearchBarView];
    }
    
    
    // [self startDispatchGroup];
}


#pragma mark - Adjust Search View

-(void)adjustSearchBarView
{
    vwSearchBG.frame = CGRectMake(self.view.frame.size.width/2 - 150, vwSearchBG.frame.origin.y, 300, vwSearchBG.frame.size.height);
    
    searchIconImage.frame = CGRectMake(vwSearchBG.frame.origin.x + 7, searchIconImage.frame.origin.y, searchIconImage.frame.size.width, searchIconImage.frame.size.height);
    
    txt_searchField.frame = CGRectMake(searchIconImage.frame.origin.x + searchIconImage.frame.size.width + 5, txt_searchField.frame.origin.y, 265, txt_searchField.frame.size.height);
    
}

#pragma mark - Dispathc Group All Api Data

-(void)startStateListGroup {
    
    if (stateApiGroup == nil ) {
        stateApiGroup = dispatch_group_create();
    }
    dispatch_group_enter(stateApiGroup);
    [self callStateAPI];
    //isViewDidLoad = false
    __weak typeof(self) weakSelf = self;
    dispatch_group_notify(stateApiGroup,dispatch_get_main_queue(),^{
        // Assess any errors
        [weakSelf startDispatchGroup];
        stateApiGroup = nil ;
        // Now call the final completion block
    });
    return;
    
    
}
-(void)startDispatchGroup
{
    if (serviceGroup == nil )
    {
        serviceGroup = dispatch_group_create();
    }
    // Create the dispatch group
    // Start the first service
    if (![singleton.stateCurrent isEqualToString:singleton.user_StateId])
    {
        if ([self.state_id isEqualToString:@"9999"])
        {
            self.bannerHide=NSLocalizedString(@"no", nil);
        }
        else
        {
            dispatch_group_enter(serviceGroup);
            [self hitFetchHerospaceStateAPI:self.state_id];
        }
    }
    dispatch_group_enter(serviceGroup);
    [self fetchDatafromDB];
    //    [self.preferenceService startWithCompletion:^(PreferenceResponse *results, NSError* error){
    //        // Do something with the results
    //        preferenceError = error;
    //        dispatch_group_leave(serviceGroup);
    //    }];
    __weak typeof(self) weakSelf = self;
    dispatch_group_notify(serviceGroup,dispatch_get_main_queue(),^{
        // Assess any errors
        [weakSelf.allSer_collectionView reloadData];
        [weakSelf showViewSubview:false];
        serviceGroup = nil ;
        // Now call the final completion block
    });
    
}
-(void)setStateID {
    singleton.user_StateId = [singleton getStateId];
    NSLog(@"user_StateId user_StateId%@",singleton.user_StateId);
    if (singleton.user_StateId.length != 0)
    {
        singleton.stateSelected = [obj getStateName:singleton.user_StateId];
    }
    if ([[singleton.stateSelected uppercaseString] isEqualToString:@"ALL"] || singleton.stateSelected == nil || singleton.stateSelected.length == 0)
    {
        if (singleton.profilestateSelected.length != 0)
        {
            singleton.user_StateId = [obj getStateCode:singleton.profilestateSelected];
            
            singleton.stateSelected = [obj getStateName:singleton.user_StateId];
        }
    }
    self.state_id = [obj getStateCode:singleton.stateSelected];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear: YES];
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"TABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    
    __weak typeof(self) weakSelf = self;
    bool checkApi = true;
    [self setStateID];
    
    //[RunOnMainThread runBlockInMainQueueIfNecessary:^{
    if (self.oldStateId != nil && self.oldStateId.length != 0) {
        if ([self.oldStateId isEqualToString:self.state_id]) {
            [self showViewSubview:false];
            [self addNotify];
            checkApi = false;
        }
    }
    if (checkApi) {
        [self showViewSubview:true];
        if (isViewDidLoad) {
            [self startDispatchGroup];
        }
    }
    //}];
    //}
    
    
    isViewDidLoad = true;
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    flagrotation=TRUE;
    
    [allSer_collectionView setContentOffset:CGPointMake(0,0) animated:YES];
    appDelegate.badgeCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"BadgeValue"]intValue];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"appDelegate.badgeCount=%d",appDelegate.badgeCount);
    
    //-------- Added later-------------
    [self badgeHandling];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(badgeHandling) name:@"NotificationRecievedComplete" object:nil];
    self.tabBarController.tabBar.itemPositioning = UITabBarItemPositioningCentered;
    [self setNeedsStatusBarAppearanceUpdate];
    // singleton.tabSelectedIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_TAB_INDEX"];
    [self stateNameDelay];
    //    NSInteger selectedTabIndex =  [singleton getSelectedTabIndex];
    //
    //    if (selectedTabIndex == 3)
    //    {
    //        [self performSelector:@selector(stateNameDelay) withObject:nil afterDelay:0.5];
    //    }
    //    else
    //    {
    //
    //    }
    
    
    //[self addNotify];//add later

    [self setViewFont];
}

#pragma mark- Font Set to View
-(void)setViewFont
{
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    txt_searchField.font = [AppFont mediumFont:13.0];
}
-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    self.oldStateId =  singleton.user_StateId;
    
}
-(NSString*)stateNameDelay
{
    
    NSString *localA=singleton.stateSelected;
    //labelLocation.font = [UIFont systemFontOfSize:14 weight:UIFontWeightSemibold];
    localA = [localA isEqualToString:@"All"] ? NSLocalizedString(@"all", nil): localA;
    [noServiceView setBtnSubHeadingTitle:[localA capitalizedString]];
    
    if ([[singleton.stateSelected uppercaseString] isEqualToString:@"ALL"] || singleton.stateSelected == nil || singleton.stateSelected.length == 0)
    {
        [noServiceView setBtnSubHeadingTitle:@""];
        localA = NSLocalizedString(@"select_state", nil);
    }
    
    return localA.uppercaseString;
    
}

//-(void)updateBtnLocationFrame:(CGFloat)yValue
//{
//    [btn_location sizeToFit];
//    CGRect btnFrame = btn_location.frame;
//    btnFrame.size.width = btnFrame.size.width + 30;
//    btnFrame.size.height = 30;
//    btnFrame.origin.x = self.view.center.x - (btnFrame.size.width / 2);
//    btnFrame.origin.y = yValue;
//    [btn_location setImageEdgeInsets:UIEdgeInsetsMake(0, btnFrame.size.width - 30, 0, 0)];
//    [btn_location setTitleEdgeInsets:UIEdgeInsetsMake(0,-30, 0, 0)];
//    btn_location.backgroundColor = [UIColor colorWithRed:60.0/225.0 green:172.0/225.0 blue:110.0/225.0 alpha:1];
//    btn_location.frame = btnFrame;
//    [self.view layoutIfNeeded];
//
//}
-(NSString*)getStateName
{
    obj=[[StateList alloc]init];
    return [obj getStateName: self.state_id];
}
-(void)badgeHandling
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    appDelegate.badgeCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"BadgeValue"]intValue];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"appDelegate.badgeCount=%d",appDelegate.badgeCount);
    NSString*badgeValue=[NSString stringWithFormat:@"%d",appDelegate.badgeCount];
    
    
    if (appDelegate.badgeCount>0)
    {
        
        if (appDelegate.badgeCount>=10)      // 3 and 2 digit digit case
        {
            CustomBadge *badge = [CustomBadge customBadgeWithString:badgeValue withScale:.9];
            CGSize size = CGSizeMake(badge.frame.size.width, badge.frame.size.height);
            
            CGPoint point = CGPointMake(btn_notification.frame.size.width+5-badge.frame.size.width, -14);
            CGRect rect = CGRectMake(point.x, point.y, size.width, size.height+8);
            [badge setFrame:rect];
            badge.userInteractionEnabled=NO;
            badge.tag=786;
            
            [btn_notification addSubview:badge];
            
            
            
        }
        else
        {
            CustomBadge * badge = [CustomBadge customBadgeWithString:badgeValue];
            CGPoint point = CGPointMake(btn_notification.frame.size.width/2+8-badge.frame.size.width/2, -8);
            CGRect rect = CGRectMake(point.x, point.y, 21, 21);
            [badge setFrame:rect];
            badge.userInteractionEnabled=NO;
            badge.tag=786;
            [btn_notification addSubview:badge];
            
        }
        
        
    }
    else
    {
        for (UIView *subview in [btn_notification subviews]) {
            if (subview.tag ==786) {
                [subview removeFromSuperview];
            }
            
        }
        
        
    }
    
    
}


#pragma mark -
-(void)loadBannerImageAfterAPI:(NSArray*)imagearray
{
    
    //========= note get banner from api response from Database =======
    //NSArray *imagearray=[singleton.dbManager getBannerStateData];
    if (_headerView != nil ) {
        [_headerView removeFromSuperview];
        _headerView = nil ;
    }
    _headerView = [[AdvertisingColumn alloc]initWithFrame:CGRectMake(0, -5, fDeviceWidth, AD_height+15)];
    if ([[UIScreen mainScreen]bounds].size.height == 736)
    {
        //return 150;
        _headerView.frame = CGRectMake(0, -6, fDeviceWidth, AD_height+25); // = [[AdvertisingColumn alloc]initWithFrame:)];
    }
    if ([[UIScreen mainScreen]bounds].size.height == 812)
    {
        //return 150;
        _headerView.frame = CGRectMake(0, -6, fDeviceWidth, AD_height+25); // = [[AdvertisingColumn alloc]initWithFrame:)];
    }
    if ([[UIScreen mainScreen]bounds].size.height >= 815)
    {
        //return 150;
        _headerView.frame =CGRectMake(0, -5, fDeviceWidth, AD_height+105);
        //_headerView = [[AdvertisingColumn alloc]initWithFrame:];
    }
    _headerView.backgroundColor = [UIColor clearColor];
    [_headerView setArray:imagearray];
    [headerView.vw_banner addSubview:_headerView];
    
   
 
    
}
-(void)setNoServiceFrame{
    NSString* showProfilestatus =  [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"SHOW_PROFILEBAR"];
    CGFloat navHeight = 50;//[showProfilestatus isEqualToString:@"YES"] ? 70 : 50;
    
    CGFloat bannerHeight = 30;//headerView.vw_banner.isHidden ? 25 : 125;
    if (fDeviceHeight < 560) {
        CGFloat check4s = [showProfilestatus isEqualToString:@"YES"] ? 50 : 0;
        CGFloat yBanner = headerView.vw_banner.isHidden ? 70 : 140;
        noServiceView.frame = CGRectMake(allSer_collectionView.frame.origin.x, allSer_collectionView.frame.origin.y + yBanner, allSer_collectionView.frame.size.width, allSer_collectionView.frame.size.height - bannerHeight - navHeight - check4s);
    }else {
        CGFloat yBanner = headerView.vw_banner.isHidden ? 110 : 140;
        noServiceView.frame = CGRectMake(allSer_collectionView.frame.origin.x, allSer_collectionView.frame.origin.y + yBanner, allSer_collectionView.frame.size.width, allSer_collectionView.frame.size.height - bannerHeight - navHeight - 50);
    }
    [self.view layoutIfNeeded];
    [self.view bringSubviewToFront:vw_noServiceFound];
}
- (void)getEvents:(UIRefreshControl *)refresh
{
    static BOOL refreshInProgress = NO;
    
    if (!refreshInProgress)
    {
        refreshInProgress = YES;
        // refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing"]; // let the user know refresh is in progress
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            // get the data here
            if([self.state_id length]!=0)
            {
                [self hitFetchHerospaceStateAPI:self.state_id];//[passcode
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                
                // when done, update the model and the UI here
                
                // refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"]; // reset the message
                [refresh endRefreshing];
                
                refreshInProgress = NO;
            });
        });
    }
}


- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    // [allSer_collectionView.collectionViewLayout invalidateLayout];
    
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation duration:(NSTimeInterval)duration
{
    
}

-(void)addNotify
{
    
    NSArray *imagearray=[singleton.dbManager getBannerStateData];
    NSString* showProfilestatus =  [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"SHOW_PROFILEBAR"];//[NSString stringWithFormat:@"%@", profileComplete];
    
    if ([showProfilestatus isEqualToString:@"YES"])
    {
        // tableView_home.frame=CGRectMake(0,69,377,fDeviceHeight-69-NV_height);
        CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
        if ([imagearray count]==0)
        {
            //[self updateBtnLocationFrame:80];
            allSer_collectionView.frame=CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight-50);
            //self.bannerHide= NSLocalizedString(@"no", nil);
            
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
            {
                allSer_collectionView.frame=CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight-100);
            }
        }
        else
        {
            
            CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
            // [self updateBtnLocationFrame:bannerViewHeight];
            
            allSer_collectionView.frame=CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight-50);
            // self.bannerHide= NSLocalizedString(@"yes", nil);
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
            {
                allSer_collectionView.frame=CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight-100);
            }
            
            
        }
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
            CGRect frameNoService = noServiceView.frame;
            frameNoService.origin.y = frameNoService.origin.y < 150 ? 200 : frameNoService.origin.y;
            frameNoService.size.height =   frameNoService.size.height - 200;
            noServiceView.frame = frameNoService;
        }
        for (UIView *subview in [self.view subviews])
        {
            if (subview.tag == 7) {
                [subview removeFromSuperview];
            }
        }
        
        
        
        // UIEdgeInsets inset = UIEdgeInsetsMake(-20, 0,0, 0);
        // tableView_home.contentInset = inset;
        
        
        //----------- Add later can remove it----
        if ([[UIScreen mainScreen]bounds].size.height == 812)/// Case for iPhone X
        {
            NotifycontainerView = [[UIView alloc]initWithFrame:CGRectMake(0, fDeviceHeight-tabBarHeight-NV_height,fDeviceWidth, NV_height)];
            
            if (flagStatusBar==TRUE)
            {
                NotifycontainerView.frame=CGRectMake(0, fDeviceHeight-1.95*NV_height-20,fDeviceWidth, NV_height);
                
            }
        }
        else
        {
            NotifycontainerView = [[UIView alloc]initWithFrame:CGRectMake(0, fDeviceHeight-1.95*NV_height,fDeviceWidth, NV_height)];
            
            if (flagStatusBar==TRUE)
            {
                NotifycontainerView.frame=CGRectMake(0, fDeviceHeight-1.95*NV_height-20,fDeviceWidth, NV_height);
                
            }
        }
        
        NotifycontainerView.backgroundColor = [UIColor clearColor];
        NotifycontainerView.tag=7;
        [self.view addSubview:NotifycontainerView];
        
        
        //-------- background image view--------------
        UIImageView *bg_img=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,fDeviceWidth, NV_height)];
        bg_img.image=[UIImage imageNamed:@"img_popup_notify.png"];
        //bg_img.tag=7;
        [NotifycontainerView addSubview:bg_img];
        
        
        
        //  dispatch_queue_t queue = dispatch_queue_create("com.yourdomain.yourappname", NULL);
        //  dispatch_async(queue, ^{
        //code to be executed in the background
        
        
        //------- Profile image view-------
        UIImageView *user_img=[[UIImageView alloc]initWithFrame:CGRectMake(10,5,35,35)];
        
        
        
        UIImage *tempImg;
        if ([singleton.notiTypeGenderSelected isEqualToString:NSLocalizedString(@"gender_male", nil)]) {
            tempImg=[UIImage imageNamed:@"male_avatar"];
            
        }
        else if ([singleton.notiTypeGenderSelected isEqualToString:NSLocalizedString(@"gender_female", nil)]) {
            tempImg=[UIImage imageNamed:@"female_avatar"];
            
        }
        else
        {
            tempImg=[UIImage imageNamed:@"user_placeholder"];
            
            
        }
        
        
        
        user_img.image=tempImg;
        
        //dispatch_async(dispatch_queue_create("com.getImage", NULL), ^(void) {
        //NSLog(@"singleton.imageLocalpath=%@",singleton.imageLocalpath);
        
        UIImage *tempImg1 ;
        
        if([[NSFileManager defaultManager] fileExistsAtPath:singleton.imageLocalpath])
        {
            // ur code here
            NSLog(@"file present singleton.imageLocalpath=%@",singleton.imageLocalpath);
            
            tempImg1 = [UIImage imageWithContentsOfFile:singleton.imageLocalpath];
            if (tempImg1==nil) {
                
                tempImg1=tempImg;
                
            }
            user_img.image=tempImg1;
        } else {
            // ur code here**
            NSLog(@"Not present singleton.imageLocalpath=%@",singleton.imageLocalpath);
            tempImg1=tempImg;
            
        }
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        singleton.user_profile_URL=[defaults decryptedValueForKey:@"USER_PIC"];
        NSLog(@"user_profile_URL=%@",singleton.user_profile_URL);
        
        
        [defaults synchronize];
        
        NSURL *url = [NSURL URLWithString:singleton.user_profile_URL];
        
        if(![[url absoluteString] isEqualToString:@""])
        {
            
            
            NSURLRequest* request = [NSURLRequest requestWithURL:url];
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse * response,
                                                       NSData * data,
                                                       NSError * error) {
                                       if (!error){
                                           if(data == nil)
                                           {
                                               user_img.image = tempImg1;
                                               
                                           }
                                           else
                                           {
                                               UIImage* image = [[UIImage alloc] initWithData:data];
                                               
                                               //added later to resolve if server image path is invalid image
                                               if(image == nil)
                                               {
                                                   image = tempImg1;
                                                   
                                               }
                                               //== end of  image nil
                                               
                                               user_img.image = image;
                                           }
                                           // do whatever you want with image
                                       }
                                       else
                                       {
                                           user_img.image = tempImg1;
                                           
                                       }
                                       
                                   }];
            
        }
        else
        {
            user_img.image = tempImg1;
            
        }
        
        // });
        
        user_img.layer.cornerRadius = user_img.frame.size.height /2;
        user_img.layer.masksToBounds = YES;
        user_img.layer.borderWidth = 0.1;
        //user_img.tag=7;
        [NotifycontainerView addSubview:user_img];
        
        // img_blurProfile.image=[self blurredImageWithImage:img_profileView.image];//comment this code
        
        //  });
        
        
        
        
        
        
        //-------Label Complete text----------
        
        CGSize stringcompletesize = [NSLocalizedString(@"profile_completion", nil) sizeWithFont:[UIFont systemFontOfSize:12]];
        //or whatever font you're using
        
        
        
        lbl_profileComplete=[[UILabel alloc]init];
        
        [lbl_profileComplete setFrame:CGRectMake(51,6,stringcompletesize.width, stringcompletesize.height)];
        
        lbl_profileComplete.text= NSLocalizedString(@"profile_completion", nil);
        
        lbl_profileComplete.font=[UIFont systemFontOfSize:12];
        lbl_profileComplete.textColor=[UIColor whiteColor];
        lbl_profileComplete.adjustsFontSizeToFitWidth = YES;
        //lbl_profileComplete.tag=7;
        [NotifycontainerView addSubview:lbl_profileComplete];
        
        
        
        
        
        
        
        //-------Button Click Update ----------
        
        
        btn_clickUpdate = [UIButton buttonWithType:UIButtonTypeCustom];
        
        // btn_clickUpdate.frame = CGRectMake(52,20,290,22);
        
        
        @try {
            btn_clickUpdate.titleLabel.adjustsFontSizeToFitWidth = YES;
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        
        CGSize stringsize = [NSLocalizedString(@"update_profile_txt", nil) sizeWithFont:[UIFont systemFontOfSize:12]];
        //or whatever font you're using
        
        
        
        
        [btn_clickUpdate setFrame:CGRectMake(52,25,stringsize.width, stringsize.height)];
        
        
        [btn_clickUpdate addTarget:self
                            action:@selector(clickUpdateAction:)
                  forControlEvents:UIControlEventTouchUpInside];
        [btn_clickUpdate setTitle:NSLocalizedString(@"update_profile_txt", nil) forState:UIControlStateNormal];
        
        [btn_clickUpdate setTitle:NSLocalizedString(@"update_profile_txt", nil) forState:UIControlStateSelected];
        btn_clickUpdate.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        
        btn_clickUpdate.titleLabel.font=[UIFont systemFontOfSize:12];
        // btn_clickUpdate.tag=7;
        
        [NotifycontainerView addSubview:btn_clickUpdate];
        
        
        //---- Show Custom Progress Profile View--------
        
        
        
        UIProgressView *progressView;
        progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
        progressView.progressTintColor =[UIColor colorWithRed:12.0/255.0 green:118.0/255.0 blue:157.0/255.0 alpha:1.0];
        [[progressView layer]setFrame:CGRectMake(lbl_profileComplete.frame.origin.x+lbl_profileComplete.frame.size.width+5,9,fDeviceWidth-188-50,10)];
        progressView.trackTintColor = [UIColor whiteColor];
        [[progressView layer]setCornerRadius:2];
        progressView.layer.masksToBounds = TRUE;
        progressView.clipsToBounds = TRUE;
        
        
        
        NSString* str =  [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"PROFILE_COMPELTE_KEY"];//[NSString stringWithFormat:@"%@", profileComplete];
        //str=@"90";
        if (str == nil)
        {
            str=@"0";
        }
        if ([str length]==0)
        {
            str=@"0";
            
        }
        
        CGFloat value = [str floatValue]/100;
        lbl_percentage.text=  [NSString stringWithFormat:@"%@%%",str];
        // self.progress=value;
        [progressView setProgress:value];  ///15
        
        
        
        [NotifycontainerView addSubview:progressView];
        
        
        
        
        
        //-------Label Percentage text----------
        lbl_percentage=[[UILabel alloc]initWithFrame:CGRectMake(progressView.frame.origin.x+progressView.frame.size.width+5,2,30,22)];
        
        
        
        if ([str intValue]>=70)
            
        {
            
            [self closeBtnAction:self];
        }
        
        
        
        lbl_percentage.text=[NSString stringWithFormat:@"%@%%",str];
        lbl_percentage.font=[UIFont boldSystemFontOfSize:11];
        lbl_percentage.textColor=[UIColor whiteColor];
        @try {
            lbl_percentage.adjustsFontSizeToFitWidth = YES;
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        //lbl_percentage.tag=7;
        [NotifycontainerView addSubview:lbl_percentage];
        
        
        //-------Button Close ----------
        
        btn_close = [UIButton buttonWithType:UIButtonTypeCustom];
        
        btn_close.frame = CGRectMake(fDeviceWidth-30,NV_height/2-8, 22, 22);
        
        [btn_close addTarget:self
                      action:@selector(closeBtnAction:)
            forControlEvents:UIControlEventTouchUpInside];
        UIImage *buttonImagePressed = [UIImage imageNamed:@"btn_close_notify.png"];
        [btn_close setImage:buttonImagePressed forState:UIControlStateNormal];
        [btn_close setImage:buttonImagePressed forState:UIControlStateSelected];
        
        btn_close.tag=7;
        
        [NotifycontainerView addSubview:btn_close];
        
        
        // [tableView_home reloadData];
    }
    else
    {
        [self closeBtnAction:self];
        
    }
    //    if (fDeviceHeight < 560) {
    //        CGRect allFrame = allSer_collectionView.frame;
    //        allFrame.origin.y = 68;
    //        allSer_collectionView.frame = allFrame;
    //    }
    // [self setNoServiceFrame];
    __weak typeof(self) weakSelf = self;
    [RunOnMainThread runBlockInMainQueueIfNecessary:^{
        [allSer_collectionView reloadData];
        // [weakSelf setNoServiceFrame];
        
    }];
    //    dispatch_async(dispatch_get_main_queue(), ^{
    //
    //    });
    if (serviceGroup != nil) {
        dispatch_group_leave(serviceGroup);
        // serviceGroup = nil ;
    }
    
}

- (void)closeBtnAction:(id)sender
{
    [singleton traceEvents:@"Close Profile Bar Button" withAction:@"Clicked" withLabel:@"State Tab" andValue:0];
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"NO" withKey:@"SHOW_PROFILEBAR"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    NSArray *imagearray=[singleton.dbManager getBannerStateData];
    
    if ([imagearray count]==0)
    {
        
        // btn_location.frame = CGRectMake(0, 80, 50, 30);
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
            allSer_collectionView.frame=CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight-100);
        }
        //self.bannerHide= NSLocalizedString(@"no", nil);
        
    }
    else
    {
        CGFloat tabBarHeight = self.tabBarController.tabBar.frame.size.height;
        // btn_location.frame = CGRectMake(0, bannerViewHeight, 50, 30);
        
        allSer_collectionView.frame=CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight-50);
        // self.bannerHide= NSLocalizedString(@"yes", nil);
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
            allSer_collectionView.frame=CGRectMake(0,74,fDeviceWidth,fDeviceHeight-74-tabBarHeight-100);
        }
        
    }
    noServiceView.frame = allSer_collectionView.frame;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        CGRect frameNoService = noServiceView.frame;
        frameNoService.origin.y = frameNoService.origin.y < 150 ? 200 : frameNoService.origin.y;
        frameNoService.size.height =   frameNoService.size.height - 200;
        noServiceView.frame = frameNoService;
    }
    [allSer_collectionView reloadData];
    [NotifycontainerView removeFromSuperview];
    
}




-(void)showViewSubview:(BOOL)hide {
    
    if (hide) {
        [allSer_collectionView setHidden:hide];
        [noServiceView setHidden:hide];
        [_headerView setHidden:hide];
        [vw_line setHidden:hide];
        [img_location setHidden:hide];
        [lbl_location setHidden:hide];
        activityView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityView.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);
        activityView.tag = 777888;
        [activityView startAnimating];
        activityView.hidesWhenStopped = hide;
        [self.view addSubview:activityView];
        [self.view bringSubviewToFront:activityView];
    }else {
        [activityView removeFromSuperview];
        [activityView startAnimating];
        activityView = nil ;
        //[allSer_collectionView setHidden:hide];
        //[noServiceView setHidden:true];
        if ([table_data count]!=0)
        {
            self.view.backgroundColor = [UIColor colorWithRed:248.0/255.0f green:246.0/255.0f blue:247.0/255.0f alpha:1.0];
            [self.view bringSubviewToFront:allSer_collectionView];
        }
        [RunOnMainThread runBlockInMainQueueIfNecessary:^{
            [_headerView setHidden:_headerView.hidden];
            [vw_line setHidden:hide];
            [img_location setHidden:img_location.hidden];
            [lbl_location setHidden:lbl_location.hidden];
            [allSer_collectionView setHidden:hide];
            [noServiceView setHidden:hide];
            if ([table_data count]!=0)
            {
                [noServiceView setHidden:true];
                
            }
        }];
        
    }
    [self.view layoutIfNeeded];
    [self.view layoutSubviews];
    
}

- (void)updateProgress
{
    
}



-(void)clickUpdateAction:(id)sender
{
    //    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kMainStoryBoard bundle:nil];
    //
    //    UserProfileVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
    //NSDictionary *Dict = [[GAIDictionaryBuilder createEventWithCategory:@"share_extention_tapped" action:@"send_post" label:@"" value:0] build];
    //[ send:Dict];
    [singleton traceEvents:@"Profile Update Button" withAction:@"Clicked" withLabel:@"State Tab" andValue:0];
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    
    ShowUserProfileVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ShowUserProfileVC"];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(IBAction)btn_locationAction:(id)sender
{
    
    [singleton traceEvents:@"State Change Button" withAction:@"Clicked" withLabel:@"State Tab" andValue:0];
    singleton.stateCurrent = self.state_id;
    [self initStateData];
}

-(void)initStateData
{
    
    singleton = [SharedManager sharedSingleton];
    
    // UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    customVC = [storyboard instantiateViewControllerWithIdentifier:@"CustomPickerVC"];
    
    //NSArray *arrState=[obj getStateList];
    NSArray  * servicesArray = [singleton.dbManager getServiceStateAvailable];
    NSArray *upcomingServicesArray = [singleton.dbManager getUpcommingStates];
    
    servicesArray = [servicesArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    upcomingServicesArray = [upcomingServicesArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    
    customVC.delegate=self;
    
    //customVC.get_title_pass = NSLocalizedString(@"select_state", nil);
    customVC.get_title_pass = NSLocalizedString(@"choose_your_state", nil);
    customVC.get_arr_element=[servicesArray mutableCopy];
    customVC.arrUpcomingState = [upcomingServicesArray mutableCopy];
    customVC.get_TAG=TAG_STATE;
    [self.navigationController pushViewController:customVC animated:YES];
    
}

-(void)callStateAPI
{
    //[obj hitStateQualifiAPI];
    [self hitStateQualifiAPI];
}

-(void)hitStateQualifiAPI
{
    
    //singleton = [SharedManager sharedSingleton];
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:@"" forKey:@"lang"];//Enter mobile number of user
    
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    
    __weak typeof(self) weakSelf = self;
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_FSTQU withBody:dictBody andTag:TAG_REQUEST_UNSET_FAVORITE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            singleton.statesList=[[NSMutableArray alloc]init];
            singleton.qualList=[[NSMutableArray alloc]init];
            singleton.occuList=[[NSMutableArray alloc]init];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                singleton.statesList=[[response valueForKey:@"pd"]valueForKey:@"statesList"];
                singleton.qualList=[[response valueForKey:@"pd"]valueForKey:@"qualList"];
                singleton.occuList=[[response valueForKey:@"pd"]valueForKey:@"occuList"];
                
            }
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            
            
        }
        [RunOnMainThread runBlockInMainQueueIfNecessary:^{
            [weakSelf setStateID];
            [weakSelf stateNameDelay];
        }];
        if (stateApiGroup != nil) {
            dispatch_group_leave(stateApiGroup);
            // serviceGroup = nil ;
        }
    }];
}
-(void)fetchDatafromDB
{
    [self reloadAfterDelay];
    //[RunOnMainThread runBlockInMainQueueIfNecessary:^{
    
    //    [self performSelector:@selector(reloadAfterDelay) withObject:nil afterDelay:0.001];
    
    //}];
    
}

-(void)reloadAfterDelay
{
    singleton = [SharedManager sharedSingleton];
    
    arrServiceData=[[singleton.dbManager getServiceStateData:self.state_id] mutableCopy];
    
    
    
    table_data=[[NSMutableArray alloc]init];
    
    NSSortDescriptor *aphabeticDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"SERVICE_NAME" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    NSArray *sortDescriptors = [NSArray arrayWithObject:aphabeticDescriptor];
    table_data = [[arrServiceData sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    NSLog(@"table_data=%@",table_data);
    
    if ([table_data count]==0)
    {
        vw_noServiceFound.hidden=YES;
        // [noServiceView setHidden:NO];
        [RunOnMainThread runBlockInMainQueueIfNecessary:^{
            [noServiceView setBackgroundImageWithUrl:[[NSUserDefaults standardUserDefaults]objectForKey:@"EMB_STR"]];
        }];
        
        
        self.view.backgroundColor = noServiceView.backgroundColor;
    }
    else
    {
        vw_noServiceFound.hidden=TRUE;
        //   [noServiceView setHidden:TRUE];
        self.view.backgroundColor = [UIColor colorWithRed:248.0/255.0f green:246.0/255.0f blue:247.0/255.0f alpha:1.0];
    }
    
    
    [self addNotify];//add later
}
/*
 -(void)setupData
 {
 self.selectedMarks = [[NSMutableArray alloc] init];
 
 SharedManager *singleton=[SharedManager sharedSingleton];
 
 NSArray *fav_Arr=[singleton.fav_Mutable_Arr mutableCopy];
 
 table_data=[[NSMutableArray alloc]init];
 
 for (int i = 1; i < [fav_Arr count]; i++)
 {
 
 
 NSDictionary *cellData1 = [fav_Arr objectAtIndex:i];
 NSArray *AllServicesData = [cellData1 objectForKey:@"AllServices"];
 
 
 NSLog(@"cellData=%@ and AllServicesData=%@",cellData1,AllServicesData);
 
 for (int i=0; i<[AllServicesData count]; i++) {
 
 
 
 
 NSLog(@"Not Selected!");
 [table_data addObject:[AllServicesData objectAtIndex:i]];
 
 
 
 }
 }
 
 NSLog(@"tableData=%@",table_data);
 [allSer_collectionView reloadData];
 
 }*/

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - UICollectionView Delegate Methods

- (UIEdgeInsets)collectionView:(UICollectionView *) collectionView
                        layout:(UICollectionViewLayout *) collectionViewLayout
        insetForSectionAtIndex:(NSInteger) section {
    NSArray *imagearray=[singleton.dbManager getBannerStateData];
    if (imagearray.count != 0) {
        return UIEdgeInsetsMake(4,13,0, 16);
    }
    return UIEdgeInsetsMake(10,13,0, 16); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *) collectionView
                   layout:(UICollectionViewLayout *) collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger) section {
    return 5.0;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 15;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return table_data.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    [singleton traceEvents:@"Select Department " withAction:@"Clicked" withLabel:@"State Tab" andValue:0];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    HomeDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    vc.dic_serviceInfo=(NSDictionary*)[table_data objectAtIndex:indexPath.row];
    vc.tagComeFrom=@"OTHERS";
    
    
    //------------ Handling loading service if user logout -----------------
    
    
    //===================================================================================
    //         START  NEW CHANGES OF ADDING STATE ID AND STATE NAME
    //===================================================================================
    
    
    NSString *strState = [obj getStateName:self.state_id];
    vc.isStateSelected=@"TRUE";// use to pass value in Homedetail
    vc.state_idtopass=self.state_id;
    vc.state_nametopass=strState;
    
    NSLog(@"state ID= %@",self.state_id);
    NSLog(@"state NAME= %@",strState);
    
    
    //===================================================================================
    //         START  NEW CHANGES OF ADDING STATE ID AND STATE NAME
    //===================================================================================
    
    
    
    
    [self presentViewController:vc animated:NO completion:nil];
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGFloat height = 135 ;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        
        height = 220;
        
    }else {
        if (fDeviceWidth == 320)
        {
            height = 135;
        }
        else if (fDeviceWidth == 375)
        {
            height = 145;
        }
        else if (fDeviceWidth > 375)
        {
            height = 160;
        }
        
    }
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return CGSizeMake((fDeviceWidth - 75 ) / 4.0, height);
    }
    return  CGSizeMake((fDeviceWidth - 60 ) / 3.0, height);
    
    
}




- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([[ UIScreen mainScreen] bounds].size.height >= 1024)
    {
        static NSString *identifier = @"AllServiceCVCellForiPad";
        AllServiceCVCellForiPad *allCellipad = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        @try
        {
            
            allCellipad.serviceTitle.text =[NSString stringWithFormat:@"%@",[[table_data valueForKey:@"SERVICE_NAME"]objectAtIndex:indexPath.row]];
            allCellipad.serviceTitle.font = [UIFont systemFontOfSize:18];
            
            
            NSURL *url=[NSURL URLWithString:[[table_data valueForKey:@"SERVICE_IMAGE"]objectAtIndex:indexPath.row]];
            
            [allCellipad.serviceImage sd_setImageWithURL:url
                                        placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
            
            
            
            int tagvalue= (int)(indexPath.row);
            allCellipad.serviceFav.tag=tagvalue;
            
            
            
            
            NSString *serviceid=[NSString stringWithFormat:@"%@",[[table_data valueForKey:@"SERVICE_ID"] objectAtIndex:indexPath.row]];
            
            NSString *serviceFav=[NSString stringWithFormat:@"%@",[singleton.dbManager getServiceFavStatus:serviceid]];//get service status from db
            
            if ([serviceFav isEqualToString:@"true"]) {
                allCellipad.serviceFav .selected=YES;
            }else{
                allCellipad.serviceFav .selected=NO;
            }
            
            
            cellData = (NSDictionary*)[table_data objectAtIndex:[indexPath row]];
            
            allCellipad.serviceInfo.celldata=[cellData mutableCopy];
            
            NSLog(@"value===%@",allCellipad.serviceTitle.text);
            
            [allCellipad.serviceInfo  addTarget:self action:@selector(moreInfo:) forControlEvents:UIControlEventTouchUpInside];
            
            allCellipad.serviceFav.usdata=[NSString stringWithFormat:@"%@",[cellData objectForKey:@"SERVICE_NAME"]];
            [allCellipad.serviceFav  addTarget:self action:@selector(fav_action:) forControlEvents:UIControlEventTouchUpInside];
            
            
            NSString *rating=[NSString stringWithFormat:@"%@",[cellData objectForKey:@"SERVICE_RATING"]];
            
            float fCost = [rating floatValue];
            
            allCellipad.starRatingView.value = fCost;
            
            //[allCellipad.starRatingView setScore:fCost*2 withAnimation:NO];
            
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        allCellipad.serviceTitle.font = [AppFont regularFont:14];
        
        //[allCellipad layoutIfNeeded];
        return allCellipad;
        
        
    }
    else
    {
        
        static NSString *identifier = @"AllServiceCVCell";
        AllServiceCVCell *allCell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        
        @try {
            allCell.serviceTitle.text =[NSString stringWithFormat:@"%@",[[table_data valueForKey:@"SERVICE_NAME"]objectAtIndex:indexPath.row]];
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                allCell.serviceTitle.font = [UIFont systemFontOfSize:18];
            }
            NSURL *url=[NSURL URLWithString:[[table_data valueForKey:@"SERVICE_IMAGE"]objectAtIndex:indexPath.row]];
            
            [allCell.serviceImage sd_setImageWithURL:url
                                    placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
            
            
            
            int tagvalue= (int)(indexPath.row);
            allCell.serviceFav.tag=tagvalue;
            
            
            
            
            NSString *serviceid=[NSString stringWithFormat:@"%@",[[table_data valueForKey:@"SERVICE_ID"] objectAtIndex:indexPath.row]];
            
            NSString *serviceFav=[NSString stringWithFormat:@"%@",[singleton.dbManager getServiceFavStatus:serviceid]];//get service status from db
            
            if ([serviceFav isEqualToString:@"true"]) {
                allCell.serviceFav .selected=YES;
            }else{
                allCell.serviceFav .selected=NO;
            }
            
            
            cellData = [(NSDictionary*)[table_data objectAtIndex:[indexPath row]] mutableCopy];
            
            allCell.serviceInfo.celldata=[cellData mutableCopy];
            
            
            [allCell.serviceInfo  addTarget:self action:@selector(moreInfo:) forControlEvents:UIControlEventTouchUpInside];
            
            allCell.serviceFav.usdata=[NSString stringWithFormat:@"%@",[cellData objectForKey:@"SERVICE_NAME"]];
            [allCell.serviceFav  addTarget:self action:@selector(fav_action:) forControlEvents:UIControlEventTouchUpInside];
            
            
            NSString *rating=[NSString stringWithFormat:@"%@",[cellData objectForKey:@"SERVICE_RATING"]];
            
            float fCost = [rating floatValue];
            
            allCell.starRatingView.value = fCost;
            allCell.serviceTitle.font = [AppFont regularFont:14];
            
            //
            //[allCell.starRatingView setScore:fCost*2 withAnimation:NO];
            
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        //[allCell layoutIfNeeded];
        
        return allCell;
    }
    
    
    
    
    
}
#pragma mark - header View
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        headerView  = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderStateBannerCollReusableView" forIndexPath:indexPath];
        NSArray *imagearray=[singleton.dbManager getBannerStateData];
        CGFloat btnY = 5;
        CGFloat btnMinus = 0;
        
        headerView.vw_banner.hidden = true;
        if (imagearray.count != 0) {
            headerView.vw_banner.hidden = false;
            
            [self loadBannerImageAfterAPI:imagearray];
            if([imagearray count]>1)
            {
                _headerView.pageControl.hidden = false ;
            }
            else
            {
                btnMinus = 4;
                _headerView.pageControl.hidden = true ;
            }
            btnY = 98 - btnMinus;
            if (fDeviceHeight < 560) {
                btnY = 87 - btnMinus;
                CGRect vwFrame = headerView.vw_banner.frame;
                vwFrame.origin.y = -4;
                headerView.vw_banner.frame = vwFrame;
            }
            if (fDeviceHeight == 736 || fDeviceHeight == 667) {
                btnY = 108 - btnMinus;
            }
            
        }
        [self setNoServiceFrame];
        headerView.btn_Location.userInteractionEnabled = YES;
        headerView.btn_Location.titleLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightSemibold];
        [headerView.btn_Location setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [headerView.btn_Location addTarget:self action:@selector(btn_locationAction:)
                          forControlEvents:UIControlEventTouchUpInside];
        [headerView.btn_Location setImage:[UIImage imageNamed:@"dropdown_arrow_Lang"] forState:UIControlStateNormal];
        headerView.btn_Location.layer.cornerRadius = 15;
        headerView.btn_Location.clipsToBounds = YES;
        
        headerView.btn_Location.titleLabel.font = [AppFont regularFont:14];
        
        NSString *strState = [self stateNameDelay] ;
        if ( [[strState uppercaseString] isEqualToString:@"ALL"] || strState == nil || strState.length == 0)
        {
            strState = NSLocalizedString(@"all", nil);
        }
        [headerView.btn_Location setTitle:strState forState:UIControlStateNormal];
        
        [noServiceView.btnSubHeading setFont:[AppFont regularFont:14]];
        
        [noServiceView setBtnSubHeadingTitle:headerView.btn_Location.currentTitle];
        
        [headerView.btn_Location sizeToFit];
        CGRect btnFrame = headerView.btn_Location.frame;
        btnFrame.size.width = btnFrame.size.width + 25;
        btnFrame.size.height = 30;
        btnFrame.origin.x = self.view.center.x - (btnFrame.size.width / 2);
        btnFrame.origin.y = btnY;
        headerView.btn_Location.frame = btnFrame;
        [headerView.btn_Location setImageEdgeInsets:UIEdgeInsetsMake(0, CGRectGetWidth(headerView.btn_Location.frame) - 25, 0, 0)];
        [headerView.btn_Location setTitleEdgeInsets:UIEdgeInsetsMake(0,-40, 0, 0)];
        reusableview = headerView;
    }
    return reusableview;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    NSArray *imagearray=[singleton.dbManager getBannerStateData];
    if (imagearray.count != 0) {
        if (fDeviceHeight < 560) {
            return CGSizeMake(fDeviceWidth, 121);
        }
        if (fDeviceHeight == 736 || fDeviceHeight == 667) {
            return CGSizeMake(fDeviceWidth, 140);
        }
        return CGSizeMake(fDeviceWidth, 132);
    }
    return CGSizeMake(fDeviceWidth, 35);
}
#pragma mark -

//==================================
//       CUSTOM PICKER STARTS
//==================================
- (void)btnOpenSheet

{
    NSString *information=NSLocalizedString(@"information", nil);
    NSString *viewMap=NSLocalizedString(@"view_on_map", nil);
    NSString *cancelinfo=NSLocalizedString(@"cancel", nil);
    
    
    UIActionSheet *  sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                         delegate:self
                                                cancelButtonTitle:cancelinfo
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:information,viewMap, nil];
    
    //  [[[sheet valueForKey:@"_buttons"] objectAtIndex:0] setImage:[UIImage imageNamed:@"serviceinfo"] forState:UIControlStateNormal];
    
    //  [[[sheet valueForKey:@"_buttons"] objectAtIndex:1] setImage:[UIImage imageNamed:@"serivemap"] forState:UIControlStateNormal];
    
    UIViewController *vc=[self topMostController];
    // Show the sheet
    [sheet showInView:vc.view];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            [self callDetailServiceVC];
        }
            break;
        case 1:
        {
            NSLog(@"VISIT MAP LONG=%@", [NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]]);
            
            NSString *latitude=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LATITUDE"]];
            
            NSString *longitute=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]];
            
            NSString *deptName=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_NAME"]];
            deptName = [deptName stringByReplacingOccurrencesOfString:@" " withString:@"+"];
            
            if ([[UIApplication sharedApplication] canOpenURL:
                 [NSURL URLWithString:@"comgooglemaps://"]])
                
            {
                NSLog(@"Map App Found");
                
                NSString* googleMapsURLString = [NSString stringWithFormat:@"comgooglemaps://?q=%.6f,%.6f(%@)&center=%.6f,%.6f&zoom=15&views=traffic",[latitude doubleValue], [longitute doubleValue],deptName,[latitude doubleValue], [longitute doubleValue]];
                
                // googleMapsURLString = [googleMapsURLString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]; //IOS 9 and above use this line
                
                NSURL *mapURL=[NSURL URLWithString:[googleMapsURLString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
                NSLog(@"mapURL= %@",mapURL);
                
                [[UIApplication sharedApplication] openURL:mapURL];
            } else
            {
                NSString* googleMapsURLString = [NSString stringWithFormat:@"https://maps.google.com/maps?&z=15&q=%.6f+%.6f&ll=%.6f+%.6f",[latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
                
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:googleMapsURLString]];
                
            }
            
        }
            break;
        case 2:
        {
            
        }
            break;
        default:
            break;
    }
}

//==================================
//          CUSTOM PICKER ENDS
//==================================

//------------ Show more info-------------
-(IBAction)moreInfo:(MyButton*)sender
{
    NSLog(@"Data 1 = %@",sender.celldata);
    
    self.cellDataOfmore=[[NSMutableDictionary alloc]init];
    
    cellDataOfmore=(NSMutableDictionary*)sender.celldata;
    
    [self btnOpenSheet];
    
}

-(void)callDetailServiceVC
{
    
    [singleton traceEvents:@"Department Information" withAction:@"Clicked" withLabel:@"State Tab" andValue:0];
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName: kDetailServiceStoryBoard bundle:nil];
    // UIStoryboard *storyboard = [self grabStoryboard];
    //#import "DetailServiceNewVC.h"
    DetailServiceNewVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DetailServiceNewVC"];
    
    vc.dic_serviceInfo=self.cellDataOfmore;//change it to URL on demand
    
    UIViewController *topvc=[self topMostController];
    //[topvc.navigationController pushViewController:vc animated:YES];
    
    [topvc presentViewController:vc animated:NO completion:nil];
}





//----------- END OF MORE INFO POP UP VIEW---------------

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}



-(IBAction)fav_action:(MyFavButton*)sender
{
    
    [singleton traceEvents:@"Favourite Button" withAction:@"Clicked" withLabel:@"State Tab" andValue:0];
    MyFavButton *button = (MyFavButton *)sender; //instance of UIButton
    int indexOfTheRow=(int)button.tag;   //tag of the button
    
    
    NSString *serviceId=[NSString stringWithFormat:@"%@",[[table_data objectAtIndex:indexOfTheRow] valueForKey:@"SERVICE_ID"]];
    
    NSString *serviceFav=[NSString stringWithFormat:@"%@",[singleton.dbManager getServiceFavStatus:serviceId]];
    
    if ([serviceFav isEqualToString:@"true"])// Is selected?
    {
        [singleton.dbManager updateServiceIsFav:serviceId serviceIsFav:@"false" hitAPI:@"Yes"];
        button.selected=FALSE;
    }
    else
    {
        [singleton.dbManager updateServiceIsFav:serviceId serviceIsFav:@"true" hitAPI:@"Yes"];
        button.selected=true;
    }
    
}





//-----------for filterview show----------
-(IBAction)btn_filterAction:(id)sender
{
    NSLog(@"Filter Pressed");
    
    
    /*
     UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     
     NotificationFilterVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"NotificationFilterVC"];
     vc.hidesBottomBarWhenPushed = YES;
     
     if ([singleton.stateSelected isEqualToString:@"All"] || [singleton.stateSelected isEqualToString:@""])
     {
     vc.stateSelected = @"";
     }
     else
     {
     vc.stateSelected = singleton.stateSelected;
     }
     
     [self.navigationController pushViewController:vc animated:YES];
     */
    [singleton traceEvents:@"Filter Button" withAction:@"Clicked" withLabel:@"State Tab" andValue:0];
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"StateFilterResult" bundle:nil];
    AddfilterStateVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"AddfilterStateVC"];
    vc.hidesBottomBarWhenPushed = YES;
    
    if ([singleton.stateSelected isEqualToString:@"All"] || [singleton.stateSelected isEqualToString:@""])
    {
        vc.stateSelected = @"";
    }
    else
    {
        vc.stateSelected = singleton.stateSelected;
    }
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
}





//-----------for notification view show---
-(IBAction)btn_noticationAction:(id)sender
{
    
    NSLog(@"Notification Pressed");
    
    [singleton traceEvents:@"Notification Button" withAction:@"Clicked" withLabel:@"State Tab" andValue:0];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    ScrollNotificationVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ScrollNotificationVC"];
    // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    //[self presentViewController:vc animated:NO completion:nil];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if (textField == txt_searchField)
    {
        [self openSearch];
    }
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)openSearch
{
    
    [singleton traceEvents:@"Search Bar" withAction:@"Clicked" withLabel:@"State Tab" andValue:0];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    AdvanceSearchVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"AdvanceSearchVC"];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self presentViewController:vc animated:NO completion:nil];
    
    
}

#pragma mark - API Methods
-(void)hitFetchHerospaceStateAPI:(NSString*)state
{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:state forKey:@"st"];
    
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"]; //tkn number
    [dictBody setObject:@"" forKey:@"peml"];
    // =-----Rashpinder Changes For Banner Size -----
    NSMutableString *size;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        size = (NSMutableString*)[NSString stringWithFormat:@"%f", fDeviceWidth * 2];
    }else {
        size = (NSMutableString*)[NSString stringWithFormat:@"%f", fDeviceWidth <380 ? fDeviceWidth * 2: fDeviceWidth * 3];
    }
    //[dictBody setObject:size forKey:@"size"];
    //get from mobile default email //not supported iphone
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_HERO_SPACE withBody:dictBody andTag:TAG_REQUEST_HERO_SPACE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        //[hud hideAnimated:YES];
        [singleton.dbManager deleteBannerStateData]; //delete old database
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            //  NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            // NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                //                dispatch_queue_t queue = dispatch_queue_create("com.yourdomain.yourappname", NULL);
                //                dispatch_async(queue, ^{
                //code to be executed in the background
                
                // dispatch_async(dispatch_get_main_queue(), ^{
                //code to be executed on the main thread when background task is finished
                [self insertHeroSpaceStateToDBTask:response];
                
                //});
                // });
                
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            /*  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
             message:error.localizedDescription
             delegate:self
             cancelButtonTitle:@"OK"
             otherButtonTitles:nil];
             [alert show];
             */
        }
        
    }];
    
}

-(void)insertHeroSpaceStateToDBTask:(NSDictionary*)response
{
    NSLog(@"response= %@",response);
    
    @try {
        
        
        NSString *  amno=[NSString stringWithFormat:@"%@",[response valueForKey:@"amno"]];
        NSString *  email=[NSString stringWithFormat:@"%@",[response valueForKey:@"email"]];
        NSString *  mno=[NSString stringWithFormat:@"%@",[response valueForKey:@"mno"]];
        NSString *  nam=[NSString stringWithFormat:@"%@",[response valueForKey:@"nam"]];
        
        NSString *  profileComplete=[NSString stringWithFormat:@"%@",[response valueForKey:@"profileComplete"]];
        
        
        
        
        NSLog(@"amno= %@",amno);
        NSLog(@"email= %@",email);
        NSLog(@"mno= %@",mno);
        NSLog(@"nam= %@",nam);
        
        
        
        
        //------- generalpdList-------
        NSMutableArray *generalpdList=[[NSMutableArray alloc]init];
        generalpdList=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] mutableCopy];
        NSLog(@"generalpdList= %@",generalpdList);
        
        //------- listHeroSpace insert it into database-------
        NSMutableArray *listHeroSpace=[[NSMutableArray alloc]init];
        listHeroSpace=[[[response valueForKey:@"pd"] valueForKey:@"listHeroSpace"] mutableCopy];
        
        
        //------- listMessageBoard-------
        NSMutableArray *listMessageBoard=[[NSMutableArray alloc]init];
        listMessageBoard=[[[response valueForKey:@"pd"] valueForKey:@"listMessageBoard"] mutableCopy];
        NSLog(@"listMessageBoard= %@",listMessageBoard);
        
        [RunOnMainThread runBlockInMainQueueIfNecessary:^{
            if(listHeroSpace && [listHeroSpace count]>=1)
            {
                [self addlistHeroSpaceStateToDBTask:listHeroSpace];
            }
            else
            {
                
                if (serviceGroup != nil) {
                    dispatch_group_leave(serviceGroup);
                    // serviceGroup = nil ;
                }
                //[self loadBannerImageAfterAPI];// used to get data from database
                
            }
        }];
        
        //        dispatch_queue_t queue = dispatch_queue_create("com.yourdomain.yourappname", NULL);
        //        dispatch_async(queue, ^{
        //
        //
        //
        //        });
        //
        
    }
    
    
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
        
    }
    
}


-(void)addlistHeroSpaceStateToDBTask:(NSMutableArray*)listHeroSpace
{
    
    for (int i=0; i<[listHeroSpace count]; i++)
    {
        NSString* actionType =[[listHeroSpace objectAtIndex:i]valueForKey:@"actionType"];
        NSString* actionURL =[[listHeroSpace objectAtIndex:i]valueForKey:@"actionURL"];
        NSString* desc =[[listHeroSpace objectAtIndex:i]valueForKey:@"desc"];
        NSString* imageUrl=[[listHeroSpace objectAtIndex:i]valueForKey:@"imageUrl"];
        [singleton.dbManager insertBannerStateData:imageUrl bannerActionType:actionType bannerActionUrl:actionURL bannerDesc:desc];
    }
    
    
    if (serviceGroup != nil) {
        dispatch_group_leave(serviceGroup);
        // serviceGroup = nil ;
    }
    //[self loadBannerImageAfterAPI];// used to get data from database
    
}
/*
 -(void)fetchHeroSpaceStateDatafromDB
 {
 dispatch_queue_t queue = dispatch_queue_create("com.yourdomain.yourappname", NULL);
 dispatch_async(queue, ^{
 
 
 
 NSArray *imagearray=[singleton.dbManager getBannerStateData];
 
 _headerView = [[AdvertisingColumn alloc]initWithFrame:CGRectMake(0, 5, fDeviceWidth, AD_height+15)];
 
 if ([[UIScreen mainScreen]bounds].size.height == 736)
 {
 
 //return 150;
 _headerView = [[AdvertisingColumn alloc]initWithFrame:CGRectMake(0, 5, fDeviceWidth, AD_height+25)];
 
 }
 _headerView.backgroundColor = [UIColor clearColor];
 [_headerView setArray:imagearray];
 
 
 
 dispatch_async(dispatch_get_main_queue(), ^{
 if ([imagearray count]!=0)
 {
 // bannerHide=@"YES";
 self.bannerHide= NSLocalizedString(@"yes", nil);
 
 //[self reloadRow0Section0];//reload banner view
 [allSer_collectionView reloadData];
 //[self fetchDatafromDB];
 
 }
 else
 {
 // bannerHide=@"NO";
 self.bannerHide= NSLocalizedString(@"no", nil);
 
 }
 });
 
 
 
 
 });
 
 
 
 
 
 }
 
 
 */
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
@end

