//
//  SocialMediaViewController.m
//  Umang
//
//  Created by admin on 04/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "SocialMediaViewController.h"
#import "LinkedTableCell.h"
#import "SocialAuthentication.h"
#import "SocialUserBO.h"
#import "UIImageView+WebCache.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"
#import "SocialHelpVC.h"

#import "UpdMpinVC.h"
static int isGoogleLogin = 0;


@interface SocialMediaViewController ()<UIGestureRecognizerDelegate>
{
    LinkedTableCell *tableCell;
    
    SharedManager *singleton;
    
    __weak IBOutlet UIButton *btnHelp;
    __weak IBOutlet UILabel *lblHeaderSocial;
    MBProgressHUD *hud;
    UpdMpinVC *updMPinVC;
    
    __weak IBOutlet UIButton *btnBackMore;
    BOOL _isSocialAccountLinked;
    NSMutableArray *_arrSocialLinked;
    NSMutableArray *_arrSocialNotLinked;
    
    
    
}
@property(nonatomic,retain)NSMutableDictionary *dictBody;
@property(nonatomic,retain)NSMutableArray *table_data;
@property(nonatomic,retain)NSMutableArray *table_imgdata;

@property(nonatomic,retain)NSMutableArray *arr_social;
@property(nonatomic,retain)NSMutableArray *arr_localsocial;



@property(nonatomic,retain)IBOutlet UITableView *tbl_social;
@property (weak, nonatomic) IBOutlet UIButton *btn_more;
@property (weak, nonatomic) IBOutlet UILabel *lbl_headerTitle;
@property(nonatomic,retain)NSString *socialLinkTypepass;
@end

@implementation SocialMediaViewController
@synthesize table_data;
@synthesize table_imgdata;
@synthesize tbl_social;
@synthesize dictBody;

- (void)viewDidLoad {
    
    
    objAuth = [[SocialAuthentication alloc] init];
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: SOCIAL_MEDIA_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    _isSocialAccountLinked = NO;
    
    singleton = [SharedManager sharedSingleton];
    
    
    lblHeaderSocial.text = NSLocalizedString(@"social_media_accounts", "");
    
    [btnBackMore setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBackMore.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBackMore.frame.origin.x, btnBackMore.frame.origin.y, btnBackMore.frame.size.width, btnBackMore.frame.size.height);
        
        [btnBackMore setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBackMore.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    [btnHelp setTitle:NSLocalizedString(@"help", nil) forState:UIControlStateNormal];
    
    
    self.view.backgroundColor= [UIColor colorWithRed:235/255.0 green:234/255.0 blue:241/255.0 alpha:1.0];
    
    
    
    [self prepareTableData];
    
    
    
    
    
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (IBAction)btnHelpClicked:(id)sender
{
    [singleton traceEvents:@"Open Social Help" withAction:@"Clicked" withLabel:@"Social Media" andValue:0];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SocialHelpVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"SocialHelpVC"];
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
}

-(void)prepareTableData
{
    // first fill all non social accounts
    if (_arrSocialNotLinked)
    {
        [_arrSocialNotLinked removeAllObjects];
    }
    else
    {
        _arrSocialNotLinked = [NSMutableArray new];
        
    }
    [_arrSocialNotLinked addObject:@"facebook"];
    [_arrSocialNotLinked addObject:@"google"];
    [_arrSocialNotLinked addObject:@"twitter"];
    
    
    
    // Check your account connectivity with Social API'S
    
    
    if (_arrSocialLinked) {
        [_arrSocialLinked removeAllObjects];
    }else{
        _arrSocialLinked = [NSMutableArray new];
    }
    
    // For facebook
    if (singleton.objUserProfile.objSocial.fb_id.length) {
        // Connected With FB. add in social array
        [_arrSocialLinked addObject:@"facebook"];
        
        // Remove from not linked array.
        [_arrSocialNotLinked removeObject:@"facebook"];
    }
    
    // For Google
    if (singleton.objUserProfile.objSocial.google_id.length) {
        // Connected With Google. add in social array
        [_arrSocialLinked addObject:@"google"];
        
        // Remove from not linked array.
        [_arrSocialNotLinked removeObject:@"google"];
    }
    
    //aditi
    
    
    //dsrawat4u
    NSString *twitterleng=[NSString stringWithFormat:@"%@",singleton.objUserProfile.objSocial.twitter_id];
    // For Twitter
    //if (singleton.objUserProfile.objSocial.twitter_id.length)
    if (twitterleng.length>0 )
    {
        if([twitterleng isEqualToString:@"<null>"] || [twitterleng isEqualToString:@"(null)"] || [twitterleng isEqualToString:@"null"])
        {
            
        }
        else{
            // Connected With Twitter. add in social array
            [_arrSocialLinked addObject:@"twitter"];
            
            // Remove from not linked array.
            [_arrSocialNotLinked removeObject:@"twitter"];
        }
    }
    
    if ([_arrSocialLinked count])
    {
        _isSocialAccountLinked = YES;
    }
    else{
        _isSocialAccountLinked = NO;
    }
    
    [tbl_social reloadData];
}



-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    
    
    //  [self prepareTableData];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    [self setViewFont];
    [super viewWillAppear:YES];
    
}
-(void)setViewFont{
    [btnBackMore.titleLabel setFont:[AppFont regularFont:17.0]];
    self.lbl_headerTitle.font = [AppFont semiBoldFont:17.0];
    btnHelp.titleLabel.font = [AppFont regularFont:17.0];
    
}


- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    
    return YES;
}




- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_isSocialAccountLinked) {
        if (indexPath.section == 0) {
            return 70.0;
        }
        else{
            return 50.0;
        }
    }
    else{
        return 50.0;
    }
    
    
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_isSocialAccountLinked) {
        if (section == 0) {
            return _arrSocialLinked.count;
        }
        else{
            return _arrSocialNotLinked.count;
        }
    }
    else{
        return _arrSocialNotLinked.count;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    /* if (_isSocialAccountLinked)
     {
     return section==0 ? @"LINKED ACCOUNTS" : @"OTHER ACCOUNTS";
     }
     else
     {
     
     return nil;
     
     }*/
    
    if (_isSocialAccountLinked) {
        if (section == 0) {
            if(_arrSocialLinked.count>0)
            {
                return NSLocalizedString(@"linked_accounts", nil);
            }
            else
            {
                return nil;
                
            }
        }
        else{
            if(_arrSocialNotLinked.count>0)
            {
                return NSLocalizedString(@"other_accounts", nil);
            }
            else
            {
                return nil;
                
            }
        }
    }
    else
    {
        return NSLocalizedString(@"other_accounts", nil);
    }
    
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //Here you must return the number of sectiosn you want
    if (_isSocialAccountLinked) {
        return 2;
    }
    else{
        return 1;
    }
}



-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50.0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.001;
}


- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    //check header height is valid
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,fDeviceWidth,50)];
    headerView.backgroundColor=[UIColor clearColor];
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(15,20,fDeviceWidth-30,30);
    label.textAlignment = singleton.isArabicSelected? NSTextAlignmentRight : NSTextAlignmentLeft;
    
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0];
    //label.font = [UIFont systemFontOfSize:14 weight:UIFontWeightMedium];
    
    label.font = [UIFont systemFontOfSize:13];
    
    label.text = [self tableView:tableView titleForHeaderInSection:section];
    [headerView addSubview:label];
    return headerView;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell1 forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell1 isKindOfClass:[LinkedTableCell class]]) {
        
        LinkedTableCell *linkedCell = (LinkedTableCell*)cell1;
        
        //        CGRect lblFrame = morecell.img_adhgarVerified.frame;
        //        lblFrame.origin.x = singleton.isArabicSelected ? 15 : CGRectGetWidth(tableView.frame) - 80;
        //        morecell.img_adhgarVerified.frame = lblFrame;
        
        CGRect btnLink =  linkedCell.btn_link.frame;
        btnLink.origin.x = singleton.isArabicSelected ? 10 : CGRectGetWidth(tbl_social.frame) - 53;
        btnLink.origin.y = (linkedCell.frame.size.height - btnLink.size.height)/2;
        linkedCell.btn_link.frame = btnLink;
        
        
        CGRect imgFrame =  linkedCell.img_socialprofile.frame;
        imgFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tbl_social.frame) - 48 : 10;
        linkedCell.img_socialprofile.frame = imgFrame;
        
        
        linkedCell.lbl_titleName.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
        
        linkedCell.lbl_linkedSite.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
        
        
        
        
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"LinkedTableCell";
    
    LinkedTableCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    
    
    
    if (cell == nil) {
        cell = [[LinkedTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.lbl_titleName.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    
    cell.separatorInset = UIEdgeInsetsMake(0, 55, 0, 0);

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.img_socialprofile.layer.cornerRadius = 0;
    cell.img_socialprofile.clipsToBounds = NO;
    CGRect imgframe=CGRectMake(10, 6, 38, 38);
    
    CGRect imgFrame =  cell.img_socialprofile.frame;
    imgFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tbl_social.frame) - 48 : 10;
    cell.img_socialprofile.frame = imgFrame;
    
    // CGRect linkbtnframe=CGRectMake(fDeviceWidth-45, 20, 29, 30);
    
    if (_isSocialAccountLinked) {
        
        if (indexPath.section == 0) {
            
            cell.img_socialprofile.layer.cornerRadius = cell.img_socialprofile.frame.size.width/2;
            cell.img_socialprofile.clipsToBounds = YES;
            
            
            
            NSString *socialAccount = _arrSocialLinked[indexPath.row];
            NSURL *imgURL = nil;
            NSString *userName = @"";
            
            if ([socialAccount isEqualToString:@"facebook"]) {
                imgURL = [NSURL URLWithString:singleton.objUserProfile.objSocial.fb_image];
                userName = singleton.objUserProfile.objSocial.fb_name;
            }
            else if ([socialAccount isEqualToString:@"google"]) {
                imgURL = [NSURL URLWithString:singleton.objUserProfile.objSocial.google_image];
                userName = singleton.objUserProfile.objSocial.google_name;
            }
            else if ([socialAccount isEqualToString:@"twitter"]) {
                imgURL = [NSURL URLWithString:singleton.objUserProfile.objSocial.twitter_image];
                userName = singleton.objUserProfile.objSocial.twitter_name;
            }
            
            
            // Add user image
            [cell.img_socialprofile sd_setImageWithURL:imgURL
                                      placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
            
            
            
            
            cell.img_socialprofile.frame=CGRectMake(imgframe.origin.x,18, imgframe.size.width, imgframe.size.width);
            
            
            
            // Add User Name
            cell.lbl_titleName.text=userName;
            //  cell.btn_link.frame=linkbtnframe;
            // Add Social Account Name
            cell.lbl_linkedSite.text=NSLocalizedString(socialAccount, @"");
            
            // Add check mark image
            [cell.btn_link setImage:[UIImage imageNamed:@"icon_check"] forState:UIControlStateNormal];
            
            [cell.img_socialprofile setContentMode:UIViewContentModeScaleAspectFit];
            
        }
        else if (indexPath.section == 1){
            [self updateCellComponentForCell:cell forIndexPath:indexPath];
            [cell.btn_link setImage:[UIImage imageNamed:@"icon_add"] forState:UIControlStateNormal];
            
        }
    }
    else{
        [self updateCellComponentForCell:cell forIndexPath:indexPath];
        [cell.btn_link setImage:[UIImage imageNamed:@"icon_add"] forState:UIControlStateNormal];
        
    }
    cell.lbl_titleName.font = [AppFont regularFont:17.0];
    cell.lbl_linkedSite.font = [AppFont lightFont:14.0];
    return cell;
}


-(void)updateCellComponentForCell:(LinkedTableCell*)cell forIndexPath:(NSIndexPath*)indexPath{
    
    CGRect imgframe=CGRectMake(10, 6, 38, 38);
    
    
    //CGRect linkbtnframe=CGRectMake(fDeviceWidth-45, 15, 29, 30);
    
    NSString *socialAccount = _arrSocialNotLinked[indexPath.row];
    UIImage *imgSocial = nil;
    
    if ([socialAccount isEqualToString:@"facebook"]) {
        imgSocial = [UIImage imageNamed:@"fb_logo_New.png"];
    }
    else if ([socialAccount isEqualToString:@"google"]) {
        imgSocial = [UIImage imageNamed:@"google_logo_New.png"];
    }
    else if ([socialAccount isEqualToString:@"twitter"]) {
        imgSocial = [UIImage imageNamed:@"twitter_logo_New.png"];
    }
    
    cell.img_socialprofile.image = imgSocial;
    cell.img_socialprofile.frame=CGRectMake(imgframe.origin.x,6, imgframe.size.width, imgframe.size.width);
    
    CGRect imgFrame =  cell.img_socialprofile.frame;
    imgFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tbl_social.frame) - 48 : 10;
    cell.img_socialprofile.frame = imgFrame;
    
    
    
    //cell.btn_link.frame=linkbtnframe;
    // Add Social Account Name
    cell.lbl_linkedSite.text=@"";
    cell.lbl_titleName.text=NSLocalizedString(socialAccount, @"");
    cell.lbl_titleName.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    
    
    
    // Add check mark image
    [cell.btn_link setImage:[UIImage imageNamed:@"icon_add"] forState:UIControlStateNormal];
    
    //    CGRect btnLink =  cell.btn_link.frame;
    //    btnLink.origin.x = singleton.isArabicSelected ? 15 : CGRectGetWidth(tbl_social.frame) - 53;
    //    cell.btn_link.frame = btnLink;
    
    
}


#pragma mark- Facebook Auth Delegates
#pragma mark-


-(void)logoutSocialWithType:(NSString *)logingType
{
    
    
    if ([logingType isEqualToString:@"facebook"])
    {
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        
        BOOL isInstalled = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://"]];
        if (isInstalled)
        {
            login.loginBehavior = FBSDKLoginBehaviorSystemAccount;
            
        } else {
            login.loginBehavior = FBSDKLoginBehaviorWeb;
            
        }    [FBSDKAccessToken setCurrentAccessToken:nil];
        [FBSDKProfile setCurrentProfile:nil];
        
        [login logOut];
    }
    else
    {
        [[GIDSignIn sharedInstance] signOut];
        [[GIDSignIn sharedInstance]disconnect];
    }
    
}

// For Facebook Authentication
-(void)facebookAuthenticationSuccessful:(SocialUserBO*)object
{
    NSLog(@"Profile Name = %@",object.profile_name);
    
    [self hitAPIToLinkedWithSocial:@"facebook" withSocialBO:object];
    
    
}


-(void)facebookAuthenticationFailed:(NSError*)error{
    
}



-(void)hitAPIToLinkedWithSocial:(NSString*)socialLinkType withSocialBO:(SocialUserBO*)objSocial{
    
    NSString *atyp=socialLinkType;
    NSString *aid=objSocial.social_id;
    NSString *aname=objSocial.profile_name;
    NSString *aimg=objSocial.profile_image_url;
    
    [hud hideAnimated:YES];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    dictBody = [NSMutableDictionary new];
    [dictBody setObject:aid forKey:@"aid"];//
    [dictBody setObject:atyp forKey:@"atyp"];//
    [dictBody setObject:aname forKey:@"aname"];//
    [dictBody setObject:aimg forKey:@"aimg"];//
    
    
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];  //state id Optional
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];  //token
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_LINK_WITH_SOCIAL withBody:dictBody andTag:TAG_REQUEST_LINK_WITH_SOCIAL completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
     {
         
         [hud hideAnimated:YES];
         
         if (error == nil)
         {
             NSLog(@"Server Response = %@",response);
             
             if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
             {
                 if ([socialLinkType isEqualToString:@"facebook"])
                 {
                     singleton.objUserProfile.objSocial.fb_id = aid;
                     singleton.objUserProfile.objSocial.fb_name = aname;
                     singleton.objUserProfile.objSocial.fb_image = aimg;
                 }
                 else if ([socialLinkType isEqualToString:@"twitter"])
                 {
                     singleton.objUserProfile.objSocial.twitter_id = aid;
                     singleton.objUserProfile.objSocial.twitter_name = aname;
                     singleton.objUserProfile.objSocial.twitter_image = aimg;
                 }
                 
                 else if ([socialLinkType isEqualToString:@"google"]) {
                     singleton.objUserProfile.objSocial.google_id = aid;
                     singleton.objUserProfile.objSocial.google_name = aname;
                     singleton.objUserProfile.objSocial.google_image = aimg;
                 }
                 
                 
                 [self prepareTableData];
                 
             }
             
         }
         else
         {
             
             if ([socialLinkType isEqualToString:@"facebook"])
             {
                 [self logoutSocialWithType:@"facebook"];
             }
             else if ([socialLinkType isEqualToString:@"google"])
             {
                 [self logoutSocialWithType:@"google"];
             }
             
             
             [hud hideAnimated:YES];
             
             NSLog(@"Error Occured = %@",error.localizedDescription);
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                             message:error.localizedDescription
                                                            delegate:self
                                                   cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                   otherButtonTitles:nil];
             [alert show];
             
         }
         
     }];
    
}
// For Google Login



-(void)hitAPIToUnLinkedWithSocial:(NSString*)socialLinkType
{
    
    
    NSString *fb_id = @"";
    NSString *goog_id = @"";
    NSString *twit_id = @"";
    
    NSString *message=@"";
    if ([socialLinkType isEqualToString:@"facebook"])
    {
        fb_id    = singleton.objUserProfile.objSocial.fb_id;
        message=NSLocalizedString(@"facebook", nil);
        
    }
    else if ([socialLinkType isEqualToString:@"twitter"]) {
        twit_id  = singleton.objUserProfile.objSocial.twitter_id;
        message=NSLocalizedString(@"twitter", nil);
        
    }
    else if ([socialLinkType isEqualToString:@"google"]) {
        goog_id  = singleton.objUserProfile.objSocial.google_id;
        message=NSLocalizedString(@"google", nil);
        
    }
    
    
    dictBody = [NSMutableDictionary new];
    [dictBody setObject:fb_id forKey:@"fbid"];//
    [dictBody setObject:goog_id forKey:@"goid"];//
    [dictBody setObject:twit_id forKey:@"twitid"];//
    [dictBody setObject:@"social" forKey:@"type"];//
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];  //state id Optional
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];  //token
    [dictBody setObject:@"" forKey:@"peml"];
    
    self.socialLinkTypepass=socialLinkType;
    //get from mobile default e
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadTable:)
                                                 name:@"RELOADSOCIALVIEW"
                                               object:nil];
    
    
    
    
    
    NSString *msg = [NSString stringWithFormat:NSLocalizedString(@"unlink_account_txt", nil),message];
    
    
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@""
                                                   message:msg
                                                  delegate:self
                                         cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                         otherButtonTitles:NSLocalizedString(@"btn_continue", nil), nil];
    alert.tag = 1010;
    [alert show];
}


-(void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1010)
    { // handle the altdev
        [hud hideAnimated:YES];
        if (buttonIndex==1)
        {
            [self openMPINview];
            
        }
    }
}



// For Google Login

- (void)reloadTable:(NSNotification *)notif
{
    [self prepareTableData];
}



-(void)openMPINview
{
    
    [self displayContentController:[self getHomeDetailLayerLeftController]];
    
}



-(UpdMpinVC*)getHomeDetailLayerLeftController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    updMPinVC = [storyboard instantiateViewControllerWithIdentifier:@"UpdMpinVC"];
    
    updMPinVC.dic_info=dictBody;//change it to URL on demand
    updMPinVC.TAG_FROM=@"UNLINK";
    updMPinVC.socialLinkType=self.socialLinkTypepass;
    return updMPinVC;
}



- (void) displayContentController:(UIViewController*)content;
{
    // UIViewController *vc=[self topMostController];
    
    [self addChildViewController:content];
    
    content.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [self.view addSubview:content.view];
    [content didMoveToParentViewController:self];
    
    [self showViewControllerFromLeftSide];
}

-(void)showViewControllerFromLeftSide
{
    // UIViewController *vc=[self topMostController];
    //add animation from left side
    
    [UIView animateWithDuration:0.4 animations:^{
        updMPinVC.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
}

-(void)hideViewController{
    //  UIViewController *vc=[self topMostController];
    //if (itemMoreVC == nil) {
    
    updMPinVC = [self getHomeDetailLayerLeftController];
    
    //add animation from left side
    
    [UIView animateWithDuration:0.4 animations:^{
        updMPinVC.view.frame = CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
    
}



#pragma mark- Google Auth Delegates
#pragma mark-

// For Google Authentication

-(void)googleAuthenticationSuccessful:(SocialUserBO*)object
{
    NSLog(@"Profile NAme = %@",object.profile_name);
    NSLog(@"isGoogleLogin=======>%d",isGoogleLogin);
    
    
    if (isGoogleLogin==1)
    {
        isGoogleLogin=2;
        
        
        
        if (singleton.objUserProfile.objSocial.google_id.length == 0) {

            [self hitAPIToLinkedWithSocial:@"google" withSocialBO:object];
        }
        else{
            NSLog(@"Google Already LoggedIN");
        }
        
    }
}

-(void)googleAuthenticationFailed:(NSError*)error
{
    
    
}


-(void)twitterAuthenticationSuccessful:(SocialUserBO*)object
{
    
    NSLog(@"Profile NAme = %@",object.profile_name);
    [self hitAPIToLinkedWithSocial:@"twitter" withSocialBO:object];
    
}

-(void)removeLoaderSocial
{
    [hud hideAnimated:YES];
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    [hud hideAnimated:YES];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    [self performSelector:@selector(removeLoaderSocial)  withObject:nil afterDelay:1];
    
    
    if (_isSocialAccountLinked)
    {
        if (indexPath.section == 1)
        {
            NSString *socialAccount = _arrSocialNotLinked[indexPath.row];
            [self connectWithSocialFrameworks:socialAccount];
        }
        else
        {
            NSString *socialAccount = _arrSocialLinked[indexPath.row];
            [self disConnectWithSocialFrameworks:socialAccount];
        }
    }
    else
    {
        if (indexPath.section == 0)
        {
            NSString *socialAccount = _arrSocialNotLinked[indexPath.row];
            [self connectWithSocialFrameworks:socialAccount];
        }
    }
    
}

-(void)connectWithSocialFrameworks:(NSString*)socialAccount
{
    
    
    
    if ([socialAccount isEqualToString:@"facebook"])
    {
        [singleton traceEvents:@"Facebook Link" withAction:@"Clicked" withLabel:@"Social Media" andValue:0];

        [objAuth loginWithFacebookFromController:self];
    }
    else if ([socialAccount isEqualToString:@"google"])
    {
        [singleton traceEvents:@"Google Link" withAction:@"Clicked" withLabel:@"Social Media" andValue:0];

        isGoogleLogin=1;

        [objAuth loginWithGoogleFromController:self];
    }
    else if ([socialAccount isEqualToString:@"twitter"])
    {
        [singleton traceEvents:@"Twitter Link" withAction:@"Clicked" withLabel:@"Social Media" andValue:0];

        [objAuth loginWithTwitterFromController:self];
    }
}



-(void)disConnectWithSocialFrameworks:(NSString*)socialAccount{
    if ([socialAccount isEqualToString:@"facebook"])
    {
        
        [[FBSDKLoginManager new] logOut];
        [singleton traceEvents:@"Facebook UnLink" withAction:@"Clicked" withLabel:@"Social Media" andValue:0];

        [self hitAPIToUnLinkedWithSocial:@"facebook"];
    }
    else if ([socialAccount isEqualToString:@"google"]) {
        [singleton traceEvents:@"Google UnLink" withAction:@"Clicked" withLabel:@"Social Media" andValue:0];

        [[GIDSignIn sharedInstance] signOut];
        [self hitAPIToUnLinkedWithSocial:@"google"];
        
    }
    else if ([socialAccount isEqualToString:@"twitter"])
    {
        [singleton traceEvents:@"Twitter UnLink" withAction:@"Clicked" withLabel:@"Social Media" andValue:0];

        TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
        NSString *userID = store.session.userID;
        [store logOutUserID:userID];
        
        [self hitAPIToUnLinkedWithSocial:@"twitter"];
    }
}

- (IBAction)backBtnAction:(id)sender
{
    
    [singleton traceEvents:@"Back Button" withAction:@"Clicked" withLabel:@"Social Media" andValue:0];

    [self.navigationController popViewControllerAnimated:YES];
    
}


#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*
 #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }
 */

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
