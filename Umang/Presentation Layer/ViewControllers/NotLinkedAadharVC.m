//
//  NotLinkedAadharVC.m
//  Umang
//
//  Created by admin on 09/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "NotLinkedAadharVC.h"
#import "NotLinkedTableCell.h"
#import "AadharDataBO.h"
#import "AadharDetailProfileCell.h"
#import "AadharCardViewCon.h"
#import "AadharHelpLinkVC.h"
#import "AadharRegViaNotLinkModuleVC.h"
#import "AadharHelpVC.h"

@interface NotLinkedAadharVC ()<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>
{
    
    __weak IBOutlet UIButton *btnHelp;
    __weak IBOutlet UIButton *btnMoreBack;
    __weak IBOutlet UILabel *lblHeaderAadhar;
    AadharDetailProfileCell  *headerProfileCell;
    
}



@end

@implementation NotLinkedAadharVC
@synthesize tblNotLinkAadhar;



- (IBAction)btnHelpClicked:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    AadharHelpVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"AadharHelpVC"];
    
    [self.navigationController pushViewController:vc animated:YES];
    

    
}

-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.shouldRotate = NO;
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    
    
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    [self setViewFont];
    [super viewWillAppear:YES];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btnMoreBack.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    lblHeaderAadhar.font = [AppFont semiBoldFont:17];
    btnHelp.titleLabel.font = [AppFont regularFont:17.0];
    
    
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: NOT_LINKED_AADHAR_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    lblHeaderAadhar.text = NSLocalizedString(@"aadhaar", nil);
    [btnMoreBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnMoreBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnMoreBack.frame.origin.x, btnMoreBack.frame.origin.y, btnMoreBack.frame.size.width, btnMoreBack.frame.size.height);
        
        [btnMoreBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnMoreBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    [btnHelp setTitle:NSLocalizedString(@"help", nil) forState:UIControlStateNormal];
    tblNotLinkAadhar.tableHeaderView = [self designAadharProfileView];
    
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
    //  [self setNeedsStatusBarAppearanceUpdate];
    
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}




-(AadharDetailProfileCell*)designAadharProfileView

{
    if (headerProfileCell == nil)
        
    {
        headerProfileCell = [[[NSBundle mainBundle] loadNibNamed:@"AadharDetailProfileCell" owner:self options:nil] objectAtIndex:0];
        headerProfileCell.TYPE_AADHAR_CHOOSEN = NOTLINKED;
        [headerProfileCell bindDataForHeaderView];
        
        
    }
    
    
    return headerProfileCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 0.001;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    return 0.001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    return 140.0;
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    static NSString *CellIdentifierNew = @"NotLinkedTableCell";
    NotLinkedTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierNew];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    {
        
        cell.lblLinkAadharID.text = NSLocalizedString(@"link_aadhar_txt", nil);
        [cell.btnLinkAadhar addTarget:self action:@selector(btnLinkAadharClicked) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    
    
    cell.lblLinkAadharID.font = [AppFont lightFont:18.0];

    
    return cell;
    
    
    
}


-(void)btnLinkAadharClicked
{
    
    AadharRegViaNotLinkModuleVC *vc;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        vc = [[AadharRegViaNotLinkModuleVC alloc] initWithNibName:@"AadharRegViaNotLinkModuleVC_iPad" bundle:nil];
       
       [self.navigationController pushViewController:vc animated:YES];
        
    }
    
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

        AadharRegViaNotLinkModuleVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"AadharRegViaNotLinkModuleVC"];
        [self.navigationController pushViewController:vc animated:YES];

    
    }
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}*/

- (IBAction)btnMoreClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
