//
//  NSString+MD5.h
//  AES256EnDeCrypt
//
//  Created by spice on 10/11/16.
//  Copyright (c) 2016 deepak singh rawat. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <CommonCrypto/CommonDigest.h>
#include <CommonCrypto/CommonHMAC.h>

@interface NSString (MD5)
- (NSString *)MD5;
- (NSString *)hmacsha1:(NSString *)data secret:(NSString *)key;

@end
