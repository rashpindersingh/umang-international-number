//
//  DateOptionView.m
//  SampleUmang
//
//  Created by Honey Lakhani on 10/7/16.
//  Copyright © 2016 Honey Lakhani. All rights reserved.
//

#import "DateOptionView.h"

@implementation DateOptionView

- (instancetype)initWithFrame:(CGRect)frame designedForState:(NSString*)designedFor
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        self.backgroundColor = [UIColor clearColor];
        CGFloat xCord = 10;
        CGFloat yCord = 0;
        CGFloat width = frame.size.width - 20;
        
        self.lblDateOptionFor = [[UILabel alloc] initWithFrame:CGRectMake(xCord, yCord, width, 20)];
        self.lblDateOptionFor.textColor = [UIColor blackColor];
        self.lblDateOptionFor.textAlignment = NSTextAlignmentCenter;
        self.lblDateOptionFor.backgroundColor = [UIColor clearColor];
        self.lblDateOptionFor.text = designedFor;
        self.lblDateOptionFor.font = [UIFont systemFontOfSize:13.0];
        [self addSubview:self.lblDateOptionFor];
        
        yCord+=30;
        
        self.lblDate = [[UILabel alloc] initWithFrame:CGRectMake(xCord+5, yCord-4, width/2, 40)];
        self.lblDate.textColor = [UIColor darkGrayColor];
        self.lblDate.font = [UIFont boldSystemFontOfSize:32.0];
        self.lblDate.text = @"26";
        self.lblDate.backgroundColor = [UIColor clearColor];
        self.lblDate.adjustsFontSizeToFitWidth=YES;
        
        [self addSubview:self.lblDate];
        
        xCord+=width/2;
        
        self.lblMonth = [[UILabel alloc] initWithFrame:CGRectMake(xCord+5, yCord, width/2, 20)];
        self.lblMonth.textColor = [UIColor grayColor];
        self.lblMonth.font = [UIFont systemFontOfSize:10.0];
        self.lblMonth.text = @"SEP";
        self.lblMonth.backgroundColor = [UIColor clearColor];
        self.lblMonth.adjustsFontSizeToFitWidth=YES;
        
        [self addSubview:self.lblMonth];
        
        yCord+=15;
        
        self.lblDayName = [[UILabel alloc] initWithFrame:CGRectMake(xCord+5, yCord, width/2, 20)];
        self.lblDayName.textColor = [UIColor grayColor];
        self.lblDayName.font = [UIFont systemFontOfSize:10.0];
        self.lblDayName.text = @"WED";
        self.lblDayName.backgroundColor = [UIColor clearColor];
        self.lblDayName.adjustsFontSizeToFitWidth=YES;
        [self addSubview:self.lblDayName];
        
        
        
        
        
    }
    return self;
}


-(void)updateValues:(NSDate*)time{
    
    
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"hh:mm a"];
    }
    
    NSString *selectedTime = [dateFormatter stringFromDate:time];
    NSLog(@"Current Selected Time = %@",selectedTime);
    
    NSString *strTime = selectedTime;
    self.lblTime.text = strTime;
}

 

-(void)updateContentValues:(NSDate*)dt
{
    
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"EEE,MMM,dd,yyyy"];
    }
    
    selectedDate = [dateFormatter stringFromDate:dt];
    NSLog(@"Current Selected Date = %@",selectedDate);
    
    NSMutableDictionary *DateDic = [[NSMutableDictionary alloc] init];
    [DateDic setObject:selectedDate forKey:@"Date"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SendDate" object:nil userInfo:DateDic];
    NSArray *arrDateComponents = [selectedDate componentsSeparatedByString:@","];
    
    self.lblDayName.text = [arrDateComponents[0] uppercaseString];
    self.lblMonth.text = [arrDateComponents[1] uppercaseString];
    self.lblDate.text = arrDateComponents[2];
}



- (instancetype)initWithFrame:(CGRect)frame designedForTime:(NSString*)designedFor
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        self.backgroundColor = [UIColor clearColor];
        CGFloat xCord = 10;
        CGFloat yCord = 0;
        CGFloat width = frame.size.width - 10;
        
        self.lblTimeOptionFor = [[UILabel alloc] initWithFrame:CGRectMake(xCord, yCord, width, 20)];
        self.lblTimeOptionFor.textColor = [UIColor blackColor];
        self.lblTimeOptionFor.textAlignment = NSTextAlignmentCenter;
        self.lblTimeOptionFor.backgroundColor = [UIColor clearColor];
        self.lblTimeOptionFor.text = designedFor;
        self.lblTimeOptionFor.font = [UIFont systemFontOfSize:13.0];
        [self addSubview:self.lblTimeOptionFor];
        
        yCord+=30;
        
        self.lblTime = [[UILabel alloc] initWithFrame:CGRectMake(xCord+5, yCord-4, width, 40)];
        self.lblTime.textColor = [UIColor darkGrayColor];
        self.lblTime.font = [UIFont boldSystemFontOfSize:30.0];
        self.lblTime.text = @"12:58";
        self.lblTime.backgroundColor = [UIColor clearColor];
        self.lblTime.adjustsFontSizeToFitWidth=YES;
        
        [self addSubview:self.lblTime];
        
        
        
    }
    return self;
}





@end
