//
//  TransactionalSearchVC.h
//  Umang
//
//  Created by admin on 12/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransactionalSearchVC : UIViewController
{
    IBOutlet UIView *vw_searchResults;
    
    IBOutlet UIImageView *img_no_result;
    IBOutlet UILabel *lbl_noresult;
    __weak IBOutlet UIButton *btnFilterSort;

}
@property(nonatomic,retain)    NSMutableArray *arrTransactionSearch;

@end
