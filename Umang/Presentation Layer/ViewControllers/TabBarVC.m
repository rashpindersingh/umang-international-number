//
//  TabBarVC.m
//  Umang
//
//  Created by deepak singh rawat on 04/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "TabBarVC.h"
#import "HomeTabVC.h"
#import "NearMeTabVC.h"
#import "FavouriteTabVC.h"
#import "AllServicesTabVC.h"
#import "MoreTabVC.h"
#import "LiveChatVC.h"
#import "WebTabVC.h"

#import "MBProgressHUD.h"
#import <Crashlytics/Crashlytics.h>



#import "UMAPIManager.h"
#import "RunOnMainThread.h"
#import "StateList.h"
#import "UIView+Toast.h"
@interface TabBarVC ()
{
    MBProgressHUD *hud ;
    SharedManager*singleton;
    NSMutableArray *listHeroSpace;
    
    BOOL flag_chat;
    StateList *obj;
}
@property (strong, nonatomic) UIWindow *window;
//@property (strong, nonatomic) TabBarVC *tabBarController;
@property (strong, nonatomic) TabBarVC *tabBarController;


@property (strong, nonatomic) HomeTabVC *firstViewController;
@property (strong, nonatomic) NearMeTabVC *secondViewController;
@property (strong, nonatomic) FavouriteTabVC *thirdViewController;
@property (strong, nonatomic) AllServicesTabVC *fourthViewController;
@property (strong, nonatomic) MoreTabVC *fifthViewController;
@property (nonatomic, strong) NSString *profileComplete;

@end

@implementation TabBarVC
@synthesize profileComplete;

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self chatBadgeIconReloadData];
    
}
-(void)chatBadgeIconReloadData
{
    NSUInteger tabIndex = [singleton.livChat_isTab  boolValue] ? 3 : 4;
    [[self.tabBar items] objectAtIndex:tabIndex].badgeValue = [singleton.chatBadgeCount integerValue] > 0 ? singleton.chatBadgeCount : nil;
    
}

-(void)callStateAPI
{
    [obj hitStateQualifiAPI];
    
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    flag_chat=TRUE;
    singleton = [SharedManager sharedSingleton];
    singleton.loadServiceStatus=@"FALSE";
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"TabKey"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"value of order tabs=%@",singleton.arr_initResponse);
    //tord = "HOME|home|,FAVOURITES|fav|,ALL SERVICES|allservices|";
    
    /*NSInteger selectedTabIndex = [singleton getSelectedTabIndex];
    
    if (selectedTabIndex == 3)
    {
        obj=[[StateList alloc]init];
        [self callStateAPI];
    }*/
    
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([singleton.arr_initResponse  count]==0)
    {
        NSArray *arrayofInit = [userDefaults objectForKey:@"InitAPIResponse"];
        NSLog(@" arrayofInit=%@",arrayofInit);
        
        if (arrayofInit.count != 0)
        {
            singleton.arr_initResponse=[arrayofInit mutableCopy];
            singleton.user_StateId = [singleton.arr_initResponse valueForKey:@"ostate"];
            [singleton setStateId:singleton.user_StateId];
            NSLog(@"value of order tabs=%@",singleton.arr_initResponse);
        }
        
        
    }
    
    if ([singleton.arr_initResponse  count]!=0)
    {
        
        NSString *savedAbbreviation = [[NSUserDefaults standardUserDefaults] valueForKey:@"ABBR_KEY"];
        NSString *emblemString = [[NSUserDefaults standardUserDefaults] valueForKey:@"EMB_STR"];
        
        
        if ([savedAbbreviation length] == 0 || savedAbbreviation == nil||[savedAbbreviation isEqualToString:@""])
        {
            NSString*  abbr=[singleton.arr_initResponse valueForKey:@"abbr"];
            NSLog(@"value of abbr=%@",abbr);
            if ([abbr length]==0)
            {
                abbr=@"";
                singleton.user_StateId = [singleton.arr_initResponse valueForKey:@"ostate"];
                [singleton setStateId:singleton.user_StateId];
            }
            
            emblemString = [singleton.arr_initResponse valueForKey:@"stemblem"];
            emblemString = emblemString.length == 0 ? @"":emblemString;
            [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
            
            [[NSUserDefaults standardUserDefaults] setObject:abbr forKey:@"ABBR_KEY"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else
        {
            if ([savedAbbreviation length]==0)
            {
                savedAbbreviation=@"";
            }
            
            
            [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
            [[NSUserDefaults standardUserDefaults] setObject:savedAbbreviation forKey:@"ABBR_KEY"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        
        self.tabOrders=[singleton.arr_initResponse  valueForKey:@"tord"];
        
        
        if([self.tabOrders length]>0)
        {
            
            
        }
        else
        {
            self.tabOrders= @"HOME|home|,FAVOURITES|fav|,ALL SERVICES|allservices|";
            
        }
        NSLog(@"value of order tabs=%@",singleton.arr_initResponse);
        
    }
    else
    {
        self.tabOrders= @"HOME|home|,FAVOURITES|fav|,ALL SERVICES|allservices|";
    }
    
    //uncomment anyone self.tabOrders to check dynamic tab action with hard coded tabs
    
    
    
    //self.tabOrders = @"HOME|home|,FAVOURITES|fav|,ALL SERVICES|allservices|,STATE|state|,GOOGLE|Google|https://www.google.com,YAHOO|Yahoo|https://www.yahoo.com";
    
    
    // self.tabOrders = @"ALL SERVICES|allservices|,GOOGLE|Google|https://www.google.com,YAHOO|Yahoo|https://www.yahoo.com";
    
    
    NSMutableArray *tabOrderArray=[[self.tabOrders componentsSeparatedByString:@","] mutableCopy];
    NSLog(@"value of tabOrderArray count=%d",[tabOrderArray count]);
    
    [tabOrderArray addObject:@"LIVE CHAT|livechat"];
    [tabOrderArray addObject:@"MORE|more"];
    
    
    if([tabOrderArray count]<6)
    {
        flag_chat=TRUE;
        singleton.livChat_isTab = @"true";
    }
    else
    {
        flag_chat=FALSE;
        singleton.livChat_isTab = @"false";
    }
    
    
    
    NSMutableArray *tabViewControllers = [[NSMutableArray alloc] init];
    NSMutableArray *webtabViewControllers = [[NSMutableArray alloc] init];
    for (int i=0; i<[tabOrderArray count]; i++)
    {
        NSArray *arr_tab=[[tabOrderArray objectAtIndex:i] componentsSeparatedByString:@"|"];
        NSString *tabAction=[arr_tab objectAtIndex:1];
        
        if ([tabAction isEqualToString:@"home"])
        {
            [tabViewControllers addObject:[tabOrderArray objectAtIndex:i]];
        }
        else if ([tabAction isEqualToString:@"fav"])
        {
            [tabViewControllers addObject:[tabOrderArray objectAtIndex:i]];
            
        }
        else if ([tabAction isEqualToString:@"allservices"])
        {
            [tabViewControllers addObject:[tabOrderArray objectAtIndex:i]];
            
        }
        else if ([tabAction isEqualToString:@"state"])
        {
            [tabViewControllers addObject:[tabOrderArray objectAtIndex:i]];
            
        }
        else if ([tabAction isEqualToString:@"more"])
        {
            [tabViewControllers addObject:[tabOrderArray objectAtIndex:i]];
            
        }
        else if ([tabAction isEqualToString:@"livechat"])
        {
            if(flag_chat==TRUE)
            {
                [tabViewControllers addObject:[tabOrderArray objectAtIndex:i]];
            }
            else
            {
                [webtabViewControllers addObject:[tabOrderArray objectAtIndex:i]];
                
            }
            
        }
        else
        {
            /* if(flag_chat==TRUE)
             {
             [tabViewControllers addObject:[tabOrderArray objectAtIndex:i]];
             
             }
             else
             {
             [webtabViewControllers addObject:[tabOrderArray objectAtIndex:i]];
             
             }*/
            [webtabViewControllers addObject:[tabOrderArray objectAtIndex:i]];
            
            
        }
        
        
    }
    
    
    
    
    NSLog(@"tabViewControllers of  tabs=%@",tabViewControllers);
    NSLog(@"webtabViewControllers of  tabs=%@",webtabViewControllers);
    
    //====================================================================================
    //========================= View controller used to be assign ========================
    //====================================================================================
    NSMutableArray *tabitemsControllers = [[NSMutableArray alloc] init];
    
    singleton.extraTabArray=[NSMutableArray new];
    
    
    for (int i=0; i<[tabViewControllers count]; i++)
    {
        NSArray *arr_tab=[[tabViewControllers objectAtIndex:i] componentsSeparatedByString:@"|"];
        NSString *tabName=[arr_tab objectAtIndex:0]; //get array name from pipe serate
        NSString *tabAction=[arr_tab objectAtIndex:1];
        
        
        NSLog(@"tabNames=%@",tabName);
        
        if ([tabAction isEqualToString:@"home"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            HomeTabVC *hometab = [storyboard instantiateViewControllerWithIdentifier:@"HomeTabVC"];
            if ([tabName length]==0)
            {
                hometab.title =NSLocalizedString(@"home_small", @"");
            }
            else
            {
                hometab.title =tabName.capitalizedString;
            }
            
            
            hometab.tabBarItem.image=[UIImage imageNamed:@"icon_home_32"];
            // [tabViewControllers addObject:hometab];
            
            UINavigationController *hometabNavVC = [[UINavigationController alloc] initWithRootViewController: hometab];
            
            
            [tabitemsControllers addObject:hometabNavVC];
            
        }
        else if ([tabAction isEqualToString:@"fav"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            FavouriteTabVC *favtab = [storyboard instantiateViewControllerWithIdentifier:@"FavouriteTabVC"];
            if ([tabName length]==0)
            {
                favtab.title =NSLocalizedString(@"favourites_small", @"");
            }
            else
            {
                favtab.title =tabName.capitalizedString;
            }
            
            favtab.tabBarItem.image=[UIImage imageNamed:@"icon_favourite32"];
            
            //[tabViewControllers addObject:favtab];
            
            
            UINavigationController *favtabNavVC = [[UINavigationController alloc] initWithRootViewController: favtab];
            [tabitemsControllers addObject:favtabNavVC];
            
        }
        else if ([tabAction isEqualToString:@"allservices"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            AllServicesTabVC *allservicetab = [storyboard instantiateViewControllerWithIdentifier:@"AllServicesTabVC"];
            
            if ([tabName length]==0)
            {
                allservicetab.title =NSLocalizedString(@"all_services_small", @"");
            }
            else
            {
                allservicetab.title =tabName.capitalizedString;
            }
            
            allservicetab.tabBarItem.image=[UIImage imageNamed:@"icon_all_services32"];
            //[tabViewControllers addObject:allservicetab];
            
            
            UINavigationController *allservicetabNavVC = [[UINavigationController alloc] initWithRootViewController: allservicetab];
            [tabitemsControllers addObject:allservicetabNavVC];
            
            
        }
        else if ([tabAction isEqualToString:@"state"])
        {
            
            
            
            
            //
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            NearMeTabVC *neartab = [storyboard instantiateViewControllerWithIdentifier:@"NearMeTabVC"];
            
            
            
            if ([tabName length]==0)
            {
                
            }
            else
            {
                neartab.title =tabName.capitalizedString;
            }
            
            
            NSString*  abbr = [[NSUserDefaults standardUserDefaults] objectForKey:@"ABBR_KEY"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"STATE TAB NAME abbr=%@",abbr);
            
            if (abbr==nil || [abbr length]==0)
            {
                
            }
            
            else
            {
                neartab.title =abbr;
                
            }
            
            
            
            
            
            
            
            //show if only its name exists
            // neartab.title =tabName.capitalizedString;
            neartab.tabBarItem.image=[UIImage imageNamed:@"icon_near_me32"];
            //[tabViewControllers addObject:neartab];
            UINavigationController *neartabNavVC = [[UINavigationController alloc] initWithRootViewController: neartab];
            [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"TabKey"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [tabitemsControllers addObject:neartabNavVC];
            
            
            
        }
        
        else if ([tabAction isEqualToString:@"more"])
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            MoreTabVC *moretab = [storyboard instantiateViewControllerWithIdentifier:@"MoreTabVC"];
            moretab.title =[NSLocalizedString(@"more_Home", @"") capitalizedString];// tabName; //TabTitle
            moretab.tabBarItem.image=[UIImage imageNamed:@"icon_more2_32"];
            
            //[tabViewControllers addObject:moretab];
            
            UINavigationController *moretabNavVC = [[UINavigationController alloc] initWithRootViewController: moretab];
            [tabitemsControllers addObject:moretabNavVC];
            
            
            
        }
        
        else if ([tabAction isEqualToString:@"livechat"])
        {
            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Chat" bundle:nil];
            LiveChatVC *chattab = [storyboard instantiateViewControllerWithIdentifier:@"LiveChatVC"];
            
            chattab.title=NSLocalizedString(@"help_live_chat", @"");//tabName;
            chattab.tabBarItem.image=[UIImage imageNamed:@"icon_chat_32"];
            UINavigationController *chattabNavVC = [[UINavigationController alloc] initWithRootViewController: chattab];
            [tabitemsControllers addObject:chattabNavVC];//show as a tab bar as total tabs are  5
            
            
        }
        else
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            WebTabVC *webtab = [storyboard instantiateViewControllerWithIdentifier:@"WebTabVC"];
            
            webtab.title = tabName; //TabTitle
            webtab.tabBarItem.image=[UIImage imageNamed:@"visit_website"];
            @try {
                NSString *tabURL=[arr_tab objectAtIndex:2];
                
                webtab.webUrlAction=tabURL;
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            //[tabViewControllers addObject:webtab];
            
            UINavigationController *webtabNavVC = [[UINavigationController alloc] initWithRootViewController:webtab];
            [tabitemsControllers addObject:webtabNavVC];
        }
    }
    
    
    
    //===================================
    //do self
    for (int i=0; i<[webtabViewControllers count]; i++)
    {
        NSArray *arr_tab=[[webtabViewControllers objectAtIndex:i] componentsSeparatedByString:@"|"];
        NSString *tabName=[arr_tab objectAtIndex:0]; //get array name from pipe serate
        NSString *tabAction=[arr_tab objectAtIndex:1];
        
        
        
        NSMutableDictionary *dic=[NSMutableDictionary new];
        [dic setObject:tabName forKey:@"TABNAME"];
        
        
        [dic setObject:tabAction forKey:@"tabAction"];
        
        
        if ([tabAction isEqualToString:@"livechat"])
        {
            [dic setObject:@"live_chat" forKey:@"ICON_URL"];
            [singleton.extraTabArray addObject:dic];//show as a tab bar as total tabs are  5
        }
        else
        {
            NSString *tabURLOpen=[arr_tab objectAtIndex:2];
            [dic setObject:tabURLOpen forKey:@"ACTION_URL"];
            [dic setObject:@"icon_language" forKey:@"ICON_URL"];
            [singleton.extraTabArray addObject:dic];//show as a tab bar as total tabs are  5
            
        }
    }
    
    
    /*
     for (int i=0; i<[webtabViewControllers count]; i++) {
     [tabViewControllers addObject:[webtabViewControllers objectAtIndex:i]];
     }
     */
    
    /*
     [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:15/255.0 green:68/255.0 blue:140/255.0 alpha:1.0]];
     
     [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
     
     
     self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:15/255.0 green:68/255.0 blue:140/255.0 alpha:1.0];
     
     self.navigationController.navigationBar.alpha = 0.7f;
     self.navigationController.navigationBar.translucent = YES;
     
     
     UINavigationController* morenav = self.tabBarController.moreNavigationController;
     morenav.navigationBar.tintColor = [UIColor whiteColor];;
     
     
     morenav.navigationBar.barTintColor=[UIColor colorWithRed:15/255.0 green:68/255.0 blue:140/255.0 alpha:1.0];
     [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
     
     [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
     
     [self.tabBarController.tabBar setBarTintColor:[UIColor colorWithRed:248/255.0 green:248/255.0 blue:248/255.0 alpha:1.0]];
     */
    
    
    
    
    [self setViewControllers:tabitemsControllers];
    NSInteger currentIndexOfTab=[singleton getSelectedTabIndex];
    //  [self setSelectedIndex:0];//place selected tab of the controller
    
    [self setSelectedIndex:currentIndexOfTab];//place selected tab of the controller
    
    
    
    //// put below code inside below it use the same
    //========================================================
    int countUnreadMessages=0;
    NSString *badgeValue = [NSString stringWithFormat:@"%lu", (long)countUnreadMessages];
    
    
    NSInteger liveTabIndex=[tabViewControllers indexOfObject:@"LIVE CHAT|livechat"];
    if(NSNotFound == liveTabIndex)
    {
        NSLog(@"not found");
        
        NSInteger moreTabIndex=[tabViewControllers indexOfObject:@"MORE|more"];
        if(NSNotFound == moreTabIndex)
        {
            NSLog(@"More Tab not found");
        }
        else
        {
            // add badge in more tab
            [[[tabitemsControllers objectAtIndex:moreTabIndex] tabBarItem] setBadgeValue:([badgeValue isEqualToString:@"0"]) ? nil : badgeValue];
        }
        
    }
    else
    {
        NSLog(@"found");
        [[[tabitemsControllers objectAtIndex:liveTabIndex] tabBarItem] setBadgeValue:([badgeValue isEqualToString:@"0"]) ? nil : badgeValue];
    }
    
    //// put above code inside below it
    //========================================================
    
    //uncomment this to check with live chat
    /*
     
     if ([[NSUserDefaults standardUserDefaults] objectForKey:@"ChatSession"] == nil || [[[NSUserDefaults standardUserDefaults] objectForKey:@"ChatSession"] isEqualToString:@"N"] || [[[AppDelegate sharedDelegate] xmppHandler] xmppStream] == nil)
     {
     //connection is closed dont add anything
     
     
     
     }
     else
     {
     //connection is open can set badge value here
     
     //add above code here
     
     }
     
     */
    
    
    
    
    
    // NSLog(@"currentIndexOfTab=)
    if (currentIndexOfTab!=0)
    {
        
        NSLog(@"currentIndexOfTab=%ld",(long)currentIndexOfTab);
        NSString *lastTime = [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"lastFetchDate"];
        //NSLog(@"lastTime=%@ %lu",lastTime,(unsigned long)[lastTime length]);
        if ([lastTime length]==0||lastTime==nil||[lastTime isEqualToString:@""]||[lastTime isEqualToString:@"NR"])
        {
            //call at end
            if (![self connected]) {
                // Not connected
                singleton.loadServiceStatus=@"FALSE";
                //add here to
                [[NSNotificationCenter defaultCenter]  postNotificationName:@"CHECKTABLEEMPTYORNOT" object:nil];
                
                
                
            } else {
                // Connected. Do some Internet stuff
                singleton.loadServiceStatus=@"TRUE";
                //add here to
                [[NSNotificationCenter defaultCenter]  postNotificationName:@"CHECKTABLEEMPTYORNOT" object:nil];
                
                [self hitHomeAPI];
                
                
            }
            
            
            
            
            
            
            // add network condtion also
            
        }
        else
        {
            //do nothing
            singleton.loadServiceStatus=@"FALSE";
            [[NSNotificationCenter defaultCenter]  postNotificationName:@"CHECKTABLEEMPTYORNOT" object:nil];
            
            
        }
        
        
    }
    
    
    // Update Key for Language Screen
    [[NSUserDefaults standardUserDefaults] setInteger:kDashboardScreenCase forKey:kInitiateScreenKey];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chatBadgeIconReloadData) name:NOTIFICATION_CHAT_MESSAGE object:nil];
    
    // Do any additional setup after loading the view.
    
    [self hitInitAPIForStateChange];
}

-(void)hitInitAPIForStateChange
{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    singleton = [SharedManager sharedSingleton];
    
    [hud hideAnimated:YES];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:@"" forKey:@"lang"];
    
    NSString *userToken;
    
    if (singleton.user_tkn == nil || singleton.user_tkn.length == 0)
    {
        userToken = @"";
    }
    else
    {
        userToken = singleton.user_tkn;
    }
    
    [dictBody setObject:userToken forKey:@"tkn"];
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_INIT withBody:dictBody andTag:TAG_REQUEST_INIT completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
     {
         [hud hideAnimated:YES];
         
         if (error == nil)
         {
             NSLog(@"Server Response = %@",response);
             
             singleton.arr_initResponse=[[NSMutableArray alloc]init];
             singleton.arr_initResponse=[response valueForKey:@"pd"];
             NSLog(@"singleton.arr_initResponse = %@",singleton.arr_initResponse);
             
             
             [[NSUserDefaults standardUserDefaults] setObject:singleton.arr_initResponse forKey:@"InitAPIResponse"];
             
             
             
             NSString*  abbr=[singleton.arr_initResponse valueForKey:@"abbr"];
             NSLog(@"value of abbr=%@",abbr);
             
             if ([abbr length]==0) {
                 
                 abbr=@"";
                 
             }
             singleton.user_StateId = [singleton.arr_initResponse valueForKey:@"ostate"];
             [singleton setStateId:singleton.user_StateId];
             
             NSString *emblemString = [singleton.arr_initResponse valueForKey:@"stemblem"];
             emblemString = emblemString.length == 0 ? @"":emblemString;
             [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
             
             
             [[NSUserDefaults standardUserDefaults] setObject:abbr forKey:@"ABBR_KEY"];
             
             [[NSUserDefaults standardUserDefaults] synchronize];
             
         }
         else
         {
             NSLog(@"Error Occured = %@",error.localizedDescription);
             
            /* UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"network_error_txt",nil)  delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok",nil) , nil];
             alert.tag=989;
             [alert show];*/
             [self showToast:NSLocalizedString(@"network_error_txt",nil)];
         }
         
     }];
    
}

-(void)showToast :(NSString *)toast
{
    [self.view makeToast:toast duration:5.0 position:CSToastPositionCenter];
}
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}


- (UIStoryboard *)grabStoryboard {
    
    // determine screen size
    int screenHeight = [UIScreen mainScreen].bounds.size.height;
    UIStoryboard *storyboard;
    
    switch (screenHeight) {
            
            // iPhone 4s
        case 480:
            storyboard = [UIStoryboard storyboardWithName:@"Main-4s" bundle:nil];
            break;
            
            // iPhone 5s
        case 568:
            storyboard = [UIStoryboard storyboardWithName:@"Main-5" bundle:nil];
            break;
            
            // iPhone 6
        case 667:
            storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            break;
            
            // iPhone 6 Plus
        case 736:
            storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            break;
            
        default:
            // it's an iPad
            storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            break;
    }
    
    return storyboard;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}








//----- hitAPI for IVR OTP call Type registration ------
-(void)hitHomeAPI
{
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    NSString *lastTime = [[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"lastFetchDate"];
    @try {
        //NSLog(@"lastTime=%@ %lu",lastTime,(unsigned long)[lastTime length]);
        if ([lastTime length]==0||lastTime==nil||[lastTime isEqualToString:@""]) {
            lastTime=@"";
        }
    } @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
    
    NSString *lastV1Home = [[NSUserDefaults standardUserDefaults] valueForKey:@"lastFetchV1"];
    
    if ([lastV1Home length]==0||lastV1Home==nil||[lastV1Home isEqualToString:@""])
    {
        lastTime=@"";
    }
    else
    {
        lastTime = lastV1Home;
    }
    
    singleton.lastFetchDate=lastTime;
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.lastFetchDate forKey:@"ldate"]; //blank first time then it will return a date save it in Shared manager and hit using it//singleton.lastFetchDate
    [dictBody setObject:@"" forKey:@"st"];//Enter mobile number of user
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"]; //tkn number
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    

     [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_HOME_SCREEN_DATA withBody:dictBody andTag:TAG_REQUEST_HOME_SCREEN_DATA completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
     {
         [hud hideAnimated:YES];
         
         [self hitFetchHerospaceAPI];
         
         if ([lastV1Home length]==0||lastV1Home==nil||[lastV1Home isEqualToString:@""])
         {
             if (error == nil)
             {
                 if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                 {
                     
                     BOOL serviceDirectory = [[[response valueForKey:@"pd"] valueForKey:@"dirsershow"] boolValue];
                     
                     [[NSUserDefaults standardUserDefaults] setBool:serviceDirectory forKey:@"Enable_ServiceDir"];
                     
                     [singleton.dbManager deleteAllServices];
                     
                     //[[NSUserDefaults standardUserDefaults] setValue:[[response valueForKey:@"pd"] valueForKey:@"lastFetchDate"] forKey:@"lastFetchV1"];
                     
                     
                     dispatch_queue_t serialQueue = dispatch_queue_create("com.unique.name.queue", DISPATCH_QUEUE_SERIAL);
                     dispatch_async(serialQueue, ^{
                         
                         [self insertToDBTask:response]; //insert into DB
                     });
                 }
             }
             else
             {
                 
             }
             
         }
         else
         {
             if (error == nil)
             {
                 
                 NSString *forceUpdate=[[response valueForKey:@"pd"] valueForKey:@"forceUpdate"];
                 
                 if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                 {
                     
                     //[[NSUserDefaults standardUserDefaults] setValue:[[response valueForKey:@"pd"] valueForKey:@"lastFetchDate"] forKey:@"lastFetchV1"];
                     
                     if ([[forceUpdate uppercaseString] isEqualToString:@"Y"])
                     {
                         [singleton.dbManager deleteAllServices]; //delete all service data
                     }
                     
                     BOOL serviceDirectory = [[[response valueForKey:@"pd"] valueForKey:@"dirsershow"] boolValue];
                     
                     [[NSUserDefaults standardUserDefaults] setBool:serviceDirectory forKey:@"Enable_ServiceDir"];
                     
                     dispatch_queue_t serialQueue = dispatch_queue_create("com.unique.name.queue", DISPATCH_QUEUE_SERIAL);
                     
                     dispatch_async(serialQueue, ^{
                         [self insertToDBTask:response]; //insert into DB
                     });
                 }
                 
             }
             else
             {
                 
             }
         }
         
         
     }];
    
}


//----------------------------------------------
//
// Code to update popularity of service
//----------------------------------------------

-(void)UpdateMostPopularToDBTask:(NSMutableArray*)mostPopularList
{
    
    @try {
        
        
        for (int i=0; i<[mostPopularList count]; i++)
        {
            NSString* serviceId =[[mostPopularList objectAtIndex:i]valueForKey:@"serviceId"];
            NSString* viewcnt =[[mostPopularList objectAtIndex:i]valueForKey:@"viewcnt"];
            
            
            if (serviceId == (NSString *)[NSNull null]||[serviceId length]==0) {
                serviceId=@"";
            }
            //code to be executed in the background
            [singleton.dbManager updateServiceMostPopular:serviceId withRating:viewcnt];
        }
        
    }
    
    
    @catch (NSException *exception)
    {
        [[Crashlytics sharedInstance] recordCustomExceptionName:@"HandledException" reason:@"Some reason" frameArray:@[exception]];
        
    }
    @finally
    {
        
    }
}


-(void)insertToDBTask:(NSDictionary*)response
{
    //singleton.dbManager  = [[DBManager alloc] initWithDatabaseFilename:@"UMANG_DATABASE.db"];
    
    @try
    {
        //  [singleton.dbManager open];
        NSMutableArray *mostPopularList=[[NSMutableArray alloc]init];
        mostPopularList=[[[response valueForKey:@"pd"] valueForKey:@"mostPopularList"] mutableCopy];
        
        
        
        //------- Code to insert data in database with service list-------
        NSMutableArray *addServiceList=[[NSMutableArray alloc]init];
        addServiceList=[[[response valueForKey:@"pd"] valueForKey:@"addServiceList"] mutableCopy];
        
        NSLog(@"response =%@",response);
        
        if([addServiceList count]<13)
        {
            NSLog(@"break down Response of incomplete Array =%@",response);
        }
        
        //  [singleton.dbManager close];
        
        //----------- Update service list--------
        
        NSMutableArray *updateServiceList=[[NSMutableArray alloc]init];
        updateServiceList=[[[response valueForKey:@"pd"] valueForKey:@"updateServiceList"] mutableCopy];
        
        //  [singleton.dbManager open];
        //  [singleton.dbManager close];
        
        //----------- deleteServiceList--------
        
        NSMutableArray *deleteServiceList=[[NSMutableArray alloc]init];
        deleteServiceList=[[[response valueForKey:@"pd"] valueForKey:@"deleteServiceList"] mutableCopy];
        //  [singleton.dbManager open];
        //  [singleton.dbManager close];
        //----------- favouriteServiceList--------
        
        
        NSMutableArray *favouriteServiceList=[[NSMutableArray alloc]init];
        favouriteServiceList=[[[response valueForKey:@"pd"] valueForKey:@"favouriteServiceList"] mutableCopy];
        
        //NSLog(@"favouriteServiceList===>%@",favouriteServiceList);
        //[singleton.dbManager open];
        //[singleton.dbManager close];
        //[singleton.dbManager open];
        // ----------- serviceCardList--------
        NSMutableArray *serviceCardList=[[NSMutableArray alloc]init];
        serviceCardList=[[[response valueForKey:@"pd"] valueForKey:@"serviceCardList"] mutableCopy];
        
        //NSLog(@"deleteSectionData");
        
        //[singleton.dbManager close];
        
        
        singleton.shareText=[[response valueForKey:@"pd"] valueForKey:@"shareText"];
        //NSLog(@"shareText=%@",singleton.shareText);
        
        
        singleton.phoneSupport=[[response valueForKey:@"pd"] valueForKey:@"phoneSupport"];
        //NSLog(@"shareText=%@",singleton.phoneSupport);
        
        
        singleton.emailSupport=[[response valueForKey:@"pd"] valueForKey:@"emailSupport"];
        //NSLog(@"emailSupport=%@",singleton.emailSupport);
        
        singleton.lastFetchDate=[NSString stringWithFormat:@"%@",[[response valueForKey:@"pd"] valueForKey:@"lastFetchDate"]];
        
        //------------------------- Encrypt Value------------------------
        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
        // Encrypt
        [[NSUserDefaults standardUserDefaults] encryptValue:singleton.lastFetchDate withKey:@"lastFetchDate"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //------------------------- Encrypt Value------------------------
        
        //---------------------------------------------------------------------------
        //---------------------------------------------------------------------------
        //---------------------------------------------------------------------------
        
        
        
        
        
        
        dispatch_queue_t serialQueue = dispatch_queue_create("com.unique.name.queue", DISPATCH_QUEUE_SERIAL);
        dispatch_async(serialQueue, ^{
            if(addServiceList && [addServiceList count]>=1)
            {
                [self addServiceToDBTask:addServiceList]; //insert
                
            }
            
            dispatch_async(serialQueue, ^{
                
                
                if(updateServiceList && [updateServiceList count]>=1)
                {
                    [self updateServiceToDBTask:updateServiceList]; //update
                    
                }   dispatch_async(serialQueue, ^{
                    
                    if(deleteServiceList && [deleteServiceList count]>=1)
                    {
                        [self deleteServiceToDBTask:deleteServiceList];
                        
                    }                    dispatch_async(serialQueue, ^{
                        if(favouriteServiceList && [favouriteServiceList count]>=1)
                        {
                            [self favServiceToDBTask:favouriteServiceList];
                            
                        }
                        dispatch_async(serialQueue, ^{
                            
                            // [[NSNotificationCenter defaultCenter]  postNotificationName:@"FETCHHOMEDATA" object:nil];
                            
                            
                            if(serviceCardList && [serviceCardList count]>=1)
                            {
                                NSLog(@"==============>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>==================Delete section if serviceCardlist is not empty=====>");
                                [singleton.dbManager deleteSectionData];// check
                            }
                            dispatch_async(serialQueue, ^{
                                if(serviceCardList && [serviceCardList count]>=1)
                                {
                                    [self ServiceCardToDBTask:serviceCardList];
                                    
                                }
                                
                                dispatch_async(serialQueue, ^{
                                    
                                    if(mostPopularList && [mostPopularList count]>=1)
                                    {
                                        [self UpdateMostPopularToDBTask:mostPopularList];
                                        
                                    }
                                    
                                    
                                    dispatch_async(serialQueue, ^{
                                        
                                        [[NSUserDefaults standardUserDefaults] setValue:[[response valueForKey:@"pd"] valueForKey:@"lastFetchDate"] forKey:@"lastFetchV1"];
                                    });
                                    
                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadEverything"  object:self];
                                    
                                    
                                    
                                    
                                });
                                
                                
                                
                            });
                        });
                    });
                });
            });
        }
                       
                       );
        
        
        
        
        
    }
    
    
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
    
    
    
}

-(void)addServiceToDBTask:(NSMutableArray*)addServiceList
{
    
    
    for (int i=0; i<[addServiceList count]; i++)
    {
        
        
        
        
        @try {
            NSString* serviceId =[[addServiceList objectAtIndex:i]valueForKey:@"serviceId"];
            NSString* serviceName =[[addServiceList objectAtIndex:i]valueForKey:@"serviceName"];
            NSString* description =[[addServiceList objectAtIndex:i]valueForKey:@"description"];
            NSString* image=[[addServiceList objectAtIndex:i]valueForKey:@"image"];
            NSString* categoryName =[[addServiceList objectAtIndex:i]valueForKey:@"categoryName"];
            NSString* subCategoryName =[[addServiceList objectAtIndex:i]valueForKey:@"subCategoryName"];
            NSString* rating=[[addServiceList objectAtIndex:i]valueForKey:@"rating"];
            NSString* url =[[addServiceList objectAtIndex:i]valueForKey:@"url"];
            NSString* state=[[addServiceList objectAtIndex:i]valueForKey:@"state"];
            NSString* lat =[[addServiceList objectAtIndex:i]valueForKey:@"lat"];
            NSString* log=[[addServiceList objectAtIndex:i]valueForKey:@"log"];
            NSString* serviceIsFav =@"false";
            NSString* serviceIsHidden =@"false";
            NSString* contact =[[addServiceList objectAtIndex:i]valueForKey:@"contact"];
            NSString* serviceisNotifEnabled =@"true";
            NSString* website =[[addServiceList objectAtIndex:i]valueForKey:@"website"];
            
            //------- NEW fields added to be shown--------------
            NSString* deptAddress =[[addServiceList objectAtIndex:i]valueForKey:@"deptAddress"];
            NSString* workingHours =[[addServiceList objectAtIndex:i]valueForKey:@"workingHours"];
            NSString* deptDescription =[[addServiceList objectAtIndex:i]valueForKey:@"deptDescription"];
            NSString* lang =[[addServiceList objectAtIndex:i]valueForKey:@"lang"];
            NSString* email =[[addServiceList objectAtIndex:i]valueForKey:@"email"];
            NSString* popularity =[[addServiceList objectAtIndex:i]valueForKey:@"popularity"];
            NSString* servicecategoryId =[[addServiceList objectAtIndex:i]valueForKey:@"categoryId"];
            
            
            NSString* serviceOtherState =[[addServiceList objectAtIndex:i]valueForKey:@"otherState"];
            
            if ([serviceOtherState length]!=0)
            {
                serviceOtherState=[NSString stringWithFormat:@"|%@|",serviceOtherState];
            }
            
            NSLog(@"Other State=%@",serviceOtherState);
            
            if (serviceOtherState == (NSString *)[NSNull null]||[serviceOtherState length]==0) {
                serviceOtherState=@"";
            }
            
            
            
            
            
            
            
            
            //NSLog(@"insertServicesData");
            //code to be executed in the background
            
            
            
            
            if (deptAddress == (NSString *)[NSNull null]||[deptAddress length]==0) {
                deptAddress=@"";
            }
            if (workingHours == (NSString *)[NSNull null]||[workingHours length]==0) {
                workingHours=@"";
            }
            if (deptDescription == (NSString *)[NSNull null]||[deptDescription length]==0) {
                deptDescription=@"";
            }
            if (lang == (NSString *)[NSNull null]||[lang length]==0) {
                lang=@"";
            }
            if (email == (NSString *)[NSNull null]||[email length]==0) {
                email=@"";
            }
            
            //-------  fields added to be shown--------------
            
            
            if (serviceId == (NSString *)[NSNull null]||[serviceId length]==0) {
                serviceId=@"";
            }
            
            if (serviceName == (NSString *)[NSNull null]||[serviceName length]==0) {
                serviceName=@"";
            }
            if (description == (NSString *)[NSNull null]||[description length]==0) {
                description=@"";
            }
            if (image == (NSString *)[NSNull null]||[image length]==0) {
                image=@"";
            }
            if (categoryName == (NSString *)[NSNull null]||[categoryName length]==0) {
                categoryName=@"";
            }
            if (subCategoryName == (NSString *)[NSNull null]||[subCategoryName length]==0) {
                subCategoryName=@"";
            }
            if (rating == (NSString *)[NSNull null]||[rating length]==0) {
                rating=@"";
            }
            if (url == (NSString *)[NSNull null]||[url length]==0) {
                url=@"";
            }
            if (state == (NSString *)[NSNull null]||[state length]==0) {
                state=@"";
            }
            if (lat == (NSString *)[NSNull null]||[lat length]==0) {
                lat=@"";
            }
            if (log == (NSString *)[NSNull null]||[log length]==0) {
                log=@"";
            }
            if (serviceIsFav == (NSString *)[NSNull null]||[serviceIsFav length]==0) {
                serviceIsFav=@"";
            }
            if (serviceIsHidden == (NSString *)[NSNull null]||[serviceIsHidden length]==0) {
                serviceIsHidden=@"";
            }
            if (contact == (NSString *)[NSNull null]||[contact length]==0) {
                contact=@"";
            }
            if (serviceisNotifEnabled == (NSString *)[NSNull null]||[serviceisNotifEnabled length]==0) {
                serviceisNotifEnabled=@"";
            }
            
            if (website == (NSString *)[NSNull null]||[website length]==0) {
                website=@"";
            }
            
            
            
            
            if (servicecategoryId == (NSString *)[NSNull null]||[servicecategoryId length]==0) {
                servicecategoryId=@"";
            }
            
            
            
            NSString* otherwebsite =[[addServiceList objectAtIndex:i]valueForKey:@"otherwebsite"];

            
            if (otherwebsite == (NSString *)[NSNull null]||[otherwebsite length]==0) {
                otherwebsite=@"";
            }
            
            NSLog(@"otherwebsite=%@",otherwebsite);

            /*
             [singleton.dbManager insertServicesData:serviceId serviceName:serviceName serviceDesc:description serviceImg:image serviceCategory:categoryName serviceSubCat:subCategoryName serviceRating:rating serviceUrl:url serviceState:state serviceLat:lat serviceLng:log serviceIsFav:serviceIsFav serviceIsHidden:serviceIsHidden servicePhoneNumber:contact serviceisNotifEnabled:serviceisNotifEnabled serviceWebsite:website servicelang:lang servicedeptAddress:deptAddress
             serviceworkingHours:workingHours servicedeptDescription:deptDescription  serviceemail:email popularity:popularity servicecategoryId:servicecategoryId serviceOtherState:serviceOtherState];
             
             */
            [singleton.dbManager insertServicesData:serviceId serviceName:serviceName serviceDesc:description serviceImg:image serviceCategory:categoryName serviceSubCat:subCategoryName serviceRating:rating serviceUrl:url serviceState:state serviceLat:lat serviceLng:log serviceIsFav:serviceIsFav serviceIsHidden:serviceIsHidden servicePhoneNumber:contact serviceisNotifEnabled:serviceisNotifEnabled serviceWebsite:website servicelang:lang servicedeptAddress:deptAddress
                                serviceworkingHours:workingHours servicedeptDescription:deptDescription  serviceemail:email popularity:popularity servicecategoryId:servicecategoryId serviceOtherState:serviceOtherState otherwebsite:otherwebsite];
            
            
            
            
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        
        
        
    }
    
}

//---- [singleton.dbManager open];
-(void)updateServiceToDBTask:(NSMutableArray*)updateServiceList
{
    
    for (int i=0; i<[updateServiceList count]; i++)
    {
        @try {
            
            
            NSString* serviceId =[[updateServiceList objectAtIndex:i]valueForKey:@"serviceId"];
            NSString* serviceName =[[updateServiceList objectAtIndex:i]valueForKey:@"serviceName"];
            NSString* description =[[updateServiceList objectAtIndex:i]valueForKey:@"description"];
            NSString* image=[[updateServiceList objectAtIndex:i]valueForKey:@"image"];
            NSString* categoryName =[[updateServiceList objectAtIndex:i]valueForKey:@"categoryName"];
            NSString* subCategoryName =[[updateServiceList objectAtIndex:i]valueForKey:@"subCategoryName"];
            NSString* rating=[[updateServiceList objectAtIndex:i]valueForKey:@"rating"];
            NSString* url =[[updateServiceList objectAtIndex:i]valueForKey:@"url"];
            NSString* state=[[updateServiceList objectAtIndex:i]valueForKey:@"state"];
            NSString* lat =[[updateServiceList objectAtIndex:i]valueForKey:@"lat"];
            NSString* log=[[updateServiceList objectAtIndex:i]valueForKey:@"log"];
            NSString* serviceIsFav =@"false";
            NSString* serviceIsHidden =@"false";
            NSString* contact =[[updateServiceList objectAtIndex:i]valueForKey:@"contact"];
            NSString* serviceisNotifEnabled =@"true";
            NSString* website =[[updateServiceList objectAtIndex:i]valueForKey:@"website"];
            
            //------- NEW fields added to be shown--------------
            NSString* servicedeptAddress =[[updateServiceList objectAtIndex:i]valueForKey:@"deptAddress"];
            NSString* serviceworkingHours =[[updateServiceList objectAtIndex:i]valueForKey:@"workingHours"];
            NSString* servicedeptDescription =[[updateServiceList objectAtIndex:i]valueForKey:@"deptDescription"];
            NSString* servicelang =[[updateServiceList objectAtIndex:i]valueForKey:@"lang"];
            NSString* serviceemail =[[updateServiceList objectAtIndex:i]valueForKey:@"email"];
            NSString* popularity =[[updateServiceList objectAtIndex:i]valueForKey:@"popularity"];
            NSString* servicecategoryId =[[updateServiceList objectAtIndex:i]valueForKey:@"categoryId"];
            
            
            NSString* serviceOtherState =[[updateServiceList objectAtIndex:i]valueForKey:@"otherState"];
            
            if ([serviceOtherState length]!=0)
            {
                serviceOtherState=[NSString stringWithFormat:@"|%@|",serviceOtherState];
            }
            
            NSLog(@"Other State=%@",serviceOtherState);
            NSLog(@"Other State=%@",serviceOtherState);
            
            if (serviceOtherState == (NSString *)[NSNull null]||[serviceOtherState length]==0) {
                serviceOtherState=@"";
            }
            
            
            
            
            
            //NSLog(@"insertServicesData");
            //code to be executed in the background
            
            if (servicedeptAddress == (NSString *)[NSNull null]||[servicedeptAddress length]==0) {
                
                servicedeptAddress=@"";
            }
            if (serviceworkingHours == (NSString *)[NSNull null]||[serviceworkingHours length]==0) {
                serviceworkingHours=@"";
            }
            if (servicedeptDescription == (NSString *)[NSNull null]||[servicedeptDescription length]==0) {
                servicedeptDescription=@"";
            }
            if (servicelang == (NSString *)[NSNull null]||[servicelang length]==0) {
                servicelang=@"";
            }
            if (serviceemail == (NSString *)[NSNull null]||[serviceemail length]==0) {
                serviceemail=@"";
            }
            
            //-------  fields added to be shown--------------
            
            
            
            
            if (serviceId == (NSString *)[NSNull null]||[serviceId length]==0) {
                serviceId=@"";
            }
            
            if (serviceName == (NSString *)[NSNull null]||[serviceName length]==0) {
                serviceName=@"";
            }
            if (description == (NSString *)[NSNull null]||[description length]==0) {
                description=@"";
            }
            if (image == (NSString *)[NSNull null]||[image length]==0) {
                image=@"";
            }
            if (categoryName == (NSString *)[NSNull null]||[categoryName length]==0) {
                categoryName=@"";
            }
            if (subCategoryName == (NSString *)[NSNull null]||[subCategoryName length]==0) {
                subCategoryName=@"";
            }
            if (rating == (NSString *)[NSNull null]||[rating length]==0) {
                rating=@"";
            }
            if (url == (NSString *)[NSNull null]||[url length]==0) {
                url=@"";
            }
            if (state == (NSString *)[NSNull null]||[state length]==0) {
                state=@"";
            }
            if (lat == (NSString *)[NSNull null]||[lat length]==0) {
                lat=@"";
            }
            if (log == (NSString *)[NSNull null]||[log length]==0) {
                log=@"";
            }
            if (serviceIsFav == (NSString *)[NSNull null]||[serviceIsFav length]==0) {
                serviceIsFav=@"";
            }
            if (serviceIsHidden == (NSString *)[NSNull null]||[serviceIsHidden length]==0) {
                serviceIsHidden=@"";
            }
            if (contact == (NSString *)[NSNull null]||[contact length]==0) {
                contact=@"";
            }
            if (serviceisNotifEnabled == (NSString *)[NSNull null]||[serviceisNotifEnabled length]==0) {
                serviceisNotifEnabled=@"";
            }
            
            if (website == (NSString *)[NSNull null]||[website length]==0) {
                website=@"";
            }
            
            if (servicecategoryId == (NSString *)[NSNull null]||[servicecategoryId length]==0) {
                servicecategoryId=@"";
            }
            
            
            
            //NSLog(@"updateServicesData");
            
            //code to be executed in the background
            NSString* otherwebsite =[[updateServiceList objectAtIndex:i]valueForKey:@"otherwebsite"];
            
            if (otherwebsite == (NSString *)[NSNull null]||[otherwebsite length]==0) {
                otherwebsite = @"";
            }
            
            
            NSLog(@"otherwebsite ---------> %@",otherwebsite);
            
            //code to be executed in the background
            /*  [singleton.dbManager updateServicesData:serviceId serviceName:serviceName serviceDesc:description serviceImg:image serviceCategory:categoryName serviceSubCat:subCategoryName serviceRating:rating serviceUrl:url serviceState:state serviceLat:lat serviceLng:log serviceIsFav:serviceIsFav serviceIsHidden:serviceIsHidden servicePhoneNumber:contact serviceisNotifEnabled:serviceisNotifEnabled serviceWebsite:website servicelang:servicelang servicedeptAddress:servicedeptAddress serviceworkingHours:serviceworkingHours servicedeptDescription:servicedeptDescription serviceemail:serviceemail popularity:popularity servicecategoryId:servicecategoryId serviceOtherState:serviceOtherState];*/
            
            [singleton.dbManager updateServicesData:serviceId serviceName:serviceName serviceDesc:description serviceImg:image serviceCategory:categoryName serviceSubCat:subCategoryName serviceRating:rating serviceUrl:url serviceState:state serviceLat:lat serviceLng:log serviceIsFav:serviceIsFav serviceIsHidden:serviceIsHidden servicePhoneNumber:contact serviceisNotifEnabled:serviceisNotifEnabled serviceWebsite:website servicelang:servicelang servicedeptAddress:servicedeptAddress serviceworkingHours:serviceworkingHours servicedeptDescription:servicedeptDescription serviceemail:serviceemail popularity:popularity servicecategoryId:servicecategoryId serviceOtherState:serviceOtherState otherwebsite:otherwebsite];
            
            
            
            
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
    }
}

-(void)deleteServiceToDBTask:(NSMutableArray*)deleteServiceList
{
    
    
    for (int i=0; i<[deleteServiceList count]; i++)
    {
        NSString* serviceId =[[deleteServiceList objectAtIndex:i]valueForKey:@"serviceId"];
        
        
        //NSLog(@"deleteServiceData");
        
        //code to be executed in the background
        [singleton.dbManager deleteServiceData:serviceId];
    }
}
-(void)favServiceToDBTask:(NSMutableArray*)favouriteServiceList
{
    for (int i=0; i<[favouriteServiceList count]; i++)
    {
        NSString* serviceId =[[favouriteServiceList objectAtIndex:i]valueForKey:@"serviceId"];
        NSString* serviceIsFav =@"true";
        //NSLog(@"updateServiceIsFav");
        
        if (serviceId == (NSString *)[NSNull null]||[serviceId length]==0) {
            serviceId=@"";
        }
        
        
        //code to be executed in the background
        [singleton.dbManager updateServiceIsFav:serviceId serviceIsFav:serviceIsFav hitAPI:@"No"];
    }
    
    if (favouriteServiceList>0) {
        singleton.loadServiceStatus=@"TRUE";
    }
    else
    {
        singleton.loadServiceStatus=@"FALSE";
    }
    
}
-(void)ServiceCardToDBTask:(NSMutableArray*)serviceCardList
{
    for (int i=0; i<[serviceCardList count]; i++)
    {
        @try {
            
            
            
            //------- compress code ---------
            NSMutableArray *serviceArr = [[serviceCardList objectAtIndex:i]valueForKey:@"ServiceId"];
            NSString *temp = [serviceArr componentsJoinedByString:@","];
            NSLog(@"temp=%@",temp);
            //-------compress code end---------
            NSString* cardName =[[serviceCardList objectAtIndex:i]valueForKey:@"cardName"];
            NSString* url =[[serviceCardList objectAtIndex:i]valueForKey:@"url"];
            
            if ([url length]==0) {
                url=@"";
            }
            if ([cardName length]==0) {
                cardName=@"";
                
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                //code to be executed on the main thread when background task is finished
                [singleton.dbManager insertServiceSections:cardName sectionImg:url  sectionServices:temp];
                
            });
            
        }
        
        @catch (NSException *exception)
        {
            
        }
        @finally
        {
            
        }
        
    }
    
    
}


//----- hitAPI for hitFetchHerospaceAPI ------
-(void)hitFetchHerospaceAPI
{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    [dictBody setObject:@"" forKey:@"st"]; //(M in case of fetch data related to state)like punjab name
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"]; //tkn number
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    // =-----Rashpinder Changes For Banner Size -----
    NSMutableString *size;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        size = (NSMutableString*)[NSString stringWithFormat:@"%f", fDeviceWidth * 2];
    }else {
        size = (NSMutableString*)[NSString stringWithFormat:@"%f", fDeviceWidth <380 ? fDeviceWidth * 2: fDeviceWidth * 3];
    }
    
    //[dictBody setObject:size forKey:@"size"];
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_HERO_SPACE withBody:dictBody andTag:TAG_REQUEST_HERO_SPACE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        
        if (error == nil) {
            //NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            //  NSString *rc=[response valueForKey:@"rc"];
            //  NSString *rs=[response valueForKey:@"rs"];
            //  NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
            
            //NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            // NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                //code is added by me
                singleton.objUserProfile = nil;
                singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
                
                // code is added by me
                
                //code to be executed in the background
                
                //------- listHeroSpace insert it into database-------
                listHeroSpace=[[NSMutableArray alloc]init];
                listHeroSpace=[[[response valueForKey:@"pd"] valueForKey:@"listHeroSpace"] mutableCopy];
                
                if ([listHeroSpace count]>0)
                {
                    [singleton.dbManager deleteBannerHomeData]; //delete old database
                    
                }
                else
                {
                    //singleton.bannerStatus=NSLocalizedString(@"no", nil);
                    
                }
                
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    
                    [self insertHeroSpaceToDBTask:response];
                    
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        // [[NSNotificationCenter defaultCenter]  postNotificationName:@"FETCHHOMEDATA" object:nil];
                        
                        
                        //call at end
                        
                        [[NSNotificationCenter defaultCenter]  postNotificationName:@"CHECKTABLEEMPTYORNOT" object:nil];
                        
                        
                        //singleton.loadServiceStatus=@"TRUE";
                        // [[NSNotificationCenter defaultCenter]  postNotificationName:@"CHECKTABLEEMPTYORNOT" object:nil];
                        
                        
                        //  [[NSNotificationCenter defaultCenter] removeObserver:self name:@"FETCHHOMEDATA" object:nil];
                        
                    });
                });
                
            }
            
        }
        else{
            
            
        }
        
    }];
    
}



-(void)insertHeroSpaceToDBTask:(NSDictionary*)response
{
    //singleton.dbManager  = [[DBManager alloc] initWithDatabaseFilename:@"UMANG_DATABASE.db"];
    //NSLog(@"response= %@",response);
    
    
    
    @try {
        
        
        
        profileComplete=[NSString stringWithFormat:@"%@",[[response valueForKey:@"pd"] valueForKey:@"profileComplete"]];
        
        
        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
        // Encrypt
        [[NSUserDefaults standardUserDefaults] encryptValue:profileComplete withKey:@"PROFILE_COMPELTE_KEY"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //------------------------- Encrypt Value------------------------
        
        
        //------- generalpdList-------
        NSMutableArray *generalpdList=[[NSMutableArray alloc]init];
        generalpdList=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] mutableCopy];
        //NSLog(@"generalpdList= %@",generalpdList);
        
        //------- listHeroSpace insert it into database-------
        listHeroSpace=[[NSMutableArray alloc]init];
        listHeroSpace=[[[response valueForKey:@"pd"] valueForKey:@"listHeroSpace"] mutableCopy];
        
        
        //------- listMessageBoard-------
        NSMutableArray *listMessageBoard=[[NSMutableArray alloc]init];
        listMessageBoard=[[[response valueForKey:@"pd"] valueForKey:@"listMessageBoard"] mutableCopy];
        //NSLog(@"listMessageBoard= %@",listMessageBoard);
        
        
        
        
        if(listHeroSpace && [listHeroSpace count]>=1)
        {
            [self addlistHeroSpaceToDBTask:listHeroSpace];
        }
        else
        {
            // [self fetchHeroSpaceDatafromDB];// used to get data from database
            
        }
        
        
        //[[NSNotificationCenter defaultCenter]  postNotificationName:@"FETCHHOMEDATA" object:nil];
        //call at end
        //  singleton.loadServiceStatus=@"TRUE";
        //  [[NSNotificationCenter defaultCenter]  postNotificationName:@"CHECKTABLEEMPTYORNOT" object:nil];
        [[NSNotificationCenter defaultCenter]  postNotificationName:@"PROFILECOMPLETE" object:nil];
        
    }
    
    
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
        
    }
    
}


-(void)addlistHeroSpaceToDBTask:(NSMutableArray*)listHeroSpaceArray
{
    
    
    for (int i=0; i<[listHeroSpaceArray count]; i++)
    {
        @try {
            
            
            NSString* actionType =[[listHeroSpaceArray objectAtIndex:i]valueForKey:@"actionType"];
            NSString* actionURL =[[listHeroSpaceArray objectAtIndex:i]valueForKey:@"actionURL"];
            NSString* desc =[[listHeroSpaceArray objectAtIndex:i]valueForKey:@"desc"];
            NSString* imageUrl=[[listHeroSpaceArray objectAtIndex:i]valueForKey:@"imageUrl"];
            //NSLog(@"insertBannerHomeData");
            
            
            
            
            if ([actionType length]==0) {
                actionType=@"";
            }
            if ([actionURL length]==0) {
                actionURL=@"";
            }
            if ([desc length]==0) {
                desc=@"";
            }
            if ([imageUrl length]==0) {
                imageUrl=@"";
            }
            
            
            [singleton.dbManager insertBannerHomeData:imageUrl bannerActionType:actionType bannerActionUrl:actionURL bannerDesc:desc];
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
    }
    [[NSNotificationCenter defaultCenter]  postNotificationName:@"FETCHHOMEDATA" object:nil];
    [[NSNotificationCenter defaultCenter]  postNotificationName:@"HOMEDATALOAD" object:nil];
    
    
    //call at end
    
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

