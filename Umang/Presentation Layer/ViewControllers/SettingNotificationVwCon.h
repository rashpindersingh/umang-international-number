//
//  SettingNotificationVwCon.h
//  Umang
//
//  Created by admin on 19/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingNotificationVwCon : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblNotificationSetting;
@property (weak, nonatomic) IBOutlet UIButton *lblNotificationBack;
@property (weak, nonatomic) IBOutlet UILabel *lblSettingHeader;


@end
