//
//  MoreTabVC.h
//  
//
//  Created by spice_digital on 18/10/16.
//
//

#import <UIKit/UIKit.h>

@interface MoreTabVC : UIViewController <UITableViewDelegate,UITableViewDataSource>
{
    
    IBOutlet UILabel* lbl_name;
    IBOutlet UILabel* lbl_age_gender;
    IBOutlet UIImageView* img_profileView;
    
    IBOutlet UILabel *lbl_Place;
    
    IBOutlet UIImageView *img_blurProfile;
    
}


@end
