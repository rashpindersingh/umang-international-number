//
//  TransactionalHistoryVC.h
//  Umang
//
//  Created by admin on 10/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransactionalHistoryVC : UIViewController
{
    IBOutlet UIView *vw_noresults;
    
    IBOutlet UIImageView *img_no_result;
    IBOutlet UILabel *lbl_noresult;
}
@property (weak, nonatomic) IBOutlet UITableView *tblTransactionHistory;

@end
