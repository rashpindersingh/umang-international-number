//
//  LinkDigilocker.m
//  Umang
//
//  Created by admin on 16/02/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "LinkDigilocker.h"
#import "UIView+Toast.h"
#import "MBProgressHUD.h"
#define kOFFSET_FOR_KEYBOARD 80.0


@interface LinkDigilocker ()<NSURLSessionDelegate>
{
    MBProgressHUD *hud;
    IBOutlet UIButton *btnmore;
}
@end

@implementation LinkDigilocker

- (void)viewDidLoad {
    
    
    //------------------------- Encrypt Value------------------------
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    // Encrypt
    [[NSUserDefaults standardUserDefaults] encryptValue:@"NO" withKey:@"LINKDIGILOCKERSTATUS"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"digilocker_username"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"digilocker_password"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //------------------------- Encrypt Value------------------------
    

    
    txt_username.delegate=self;
    txt_userpass.delegate=self;
    txt_username.placeholder = NSLocalizedString(@"username", nil);
    
    txt_userpass.placeholder = NSLocalizedString(@"password",nil);
    

    [btnmore setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];

    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnmore.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnmore.frame.origin.x, btnmore.frame.origin.y, btnmore.frame.size.width, btnmore.frame.size.height);
        
        [btnmore setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnmore.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    btn_login.layer.cornerRadius = 5.0;
    [btn_login setTitle: NSLocalizedString(@"login", nil) forState:UIControlStateNormal];
    lbl_title.text = NSLocalizedString(@"digi_locker", nil);
    lbl_linkdigilock.text = NSLocalizedString(@"link_digilocker_txt", nil);

    vw_txfield.backgroundColor=[UIColor whiteColor];
    
    [self addShadowToTheView:vw_txfield];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addShadowToTheView:(UIView*)vwItem
{
    vwItem.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    vwItem.layer.shadowColor = [UIColor brownColor].CGColor;
    vwItem.layer.shadowRadius = 3;
    vwItem.layer.shadowOpacity = 0.5;
    vwItem.layer.cornerRadius = 3.0;
}

-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.shouldRotate = NO;
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    [self setViewFont];
    [super viewWillAppear:NO];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btnmore.titleLabel setFont:[AppFont regularFont:17.0]];
    lbl_title.font = [AppFont semiBoldFont:17.0];
    txt_username.font = [AppFont regularFont:14.0];
    txt_userpass.font = [AppFont regularFont:14.0];
    lbl_linkdigilock.font = [AppFont regularFont:14.0];
    btn_login.titleLabel.font = [AppFont mediumFont:16.0];
}
-(IBAction)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];

}
-(IBAction)submitBtnAction:(id)sender
{
    if ([txt_username.text isEqualToString:@""])
    {
        
        [self showToast:NSLocalizedString(@"please_enter_username", nil)];//
        
    }
    else if ([txt_userpass.text isEqualToString:@""])
    {
        [self showToast:NSLocalizedString(@"please_enter_password", nil)];//
    }
    
    else
    {
        //hit api
        
       /* API :
    https://dept.umang.gov.in/digiLockerApi/ws1/fdoc
        
        Params :
        username
        password
        trkr ---- Unique value(timestamp)
        */
        
        [self LoginDigilocker];
        
    }
}

-(void)LoginDigilocker
{
    
    
    /* API :
     https://dept.umang.gov.in/digiLockerApi/ws1/fdoc
     
     Params :
     username
     password
     trkr ---- Unique value(timestamp)
     */
    
    
    
    NSError *error;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:@"https://dept.umang.gov.in/digiLockerApi/ws1/fdoc"];
    
    
    NSDate *currentDate =[NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm:ss"];
    NSString *trkr = [dateFormatter stringFromDate:currentDate];
    trkr = [trkr stringByReplacingOccurrencesOfString:@":" withString:@""];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: txt_username.text, @"username",
                             txt_userpass.text, @"password",trkr,@"trkr",
                             nil];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
    [request setHTTPBody:postData];
    
    
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          
                                          {
                                              //[hud hideAnimated:YES];
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [hud hideAnimated:YES];
                                              });
                                              
                                              
                                              NSHTTPURLResponse *HTTPResponse = (NSHTTPURLResponse *)response;
                                              NSInteger statusCode = [HTTPResponse statusCode];
                                              
                                              if(statusCode==200)
                                              {
                                                  if (!error) {
                                                      /*
                                                       
                                                       {"status":true,"records":[{"id":"256631889","aadhaar":"291917116859","uri":"in.gov.uidai-ADHAR-291917116859","doc_type_id":"ADHAR","issuedOn":"21-01-2017","docName":"Aadhaar Card","orgName":"Unique Identification Authority of India","docDescription":"Aadhaar Card","metadata":"1"},{"id":"255504394","aadhaar":"291917116859","uri":"in.gov.transport-RVCER-PB10CK2140","doc_type_id":"RVCER","issuedOn":"19-01-2017","docName":"Registration of Vehicles","orgName":"Ministry of Road Transport and Highways","docDescription":"Registration of Vehicles","metadata":"1"}]}
                                                       */
                                                      
                                                      NSLog(@"dataAsString %@", [NSString stringWithUTF8String:[data bytes]]);
                                                      
                                                      NSError *error1;
                                                      NSMutableDictionary * innerJson = [NSJSONSerialization
                                                                                         JSONObjectWithData:data
                                                                                         options:kNilOptions
                                                                                         error:&error1];
                                                      
                                                      NSLog(@"innerJson=%@",innerJson);
                                                      
                                                      
                                                      NSString *message=[innerJson valueForKey:@"message"];
                                                      NSString *status=[NSString stringWithFormat:@"%@",[innerJson valueForKey:@"status"]];
                                                      if ([status isEqualToString:@"1"])
                                                      {
                                                          
                                                          UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Your account linked successfully" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                                                          alert.tag=1001;
                                                          [alert show];
                                                          
                                                          
                                                          
                                                          
                                                          //------------------------- Encrypt Value------------------------
                                                          [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                                                          // Encrypt
                                                          [[NSUserDefaults standardUserDefaults] encryptValue:@"YES" withKey:@"LINKDIGILOCKERSTATUS"];
                                                          [[NSUserDefaults standardUserDefaults] synchronize];
                                                          
                                                          [[NSUserDefaults standardUserDefaults] encryptValue:txt_username.text withKey:@"digilocker_username"];
                                                          [[NSUserDefaults standardUserDefaults] encryptValue:txt_userpass.text withKey:@"digilocker_password"];
                                                          
                                                          
                                                          //------------------------- Encrypt Value------------------------
                                                          
                                                          
                                                          
                                                          
                                                          
                                                          
                                                          // [self showToast:status];
                                                      }
                                                      else
                                                      {
                                                          UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                                                          [alert show];
                                                          
                                                          
                                                          //------------------------- Encrypt Value------------------------
                                                          [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                                                          // Encrypt
                                                          [[NSUserDefaults standardUserDefaults] encryptValue:@"NO" withKey:@"LINKDIGILOCKERSTATUS"];
                                                          //------------------------- Encrypt Value------------------------
                                                          
                                                          [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"digilocker_username"];
                                                          [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"digilocker_password"];
                                                          [[NSUserDefaults standardUserDefaults] synchronize];
                                                          
                                                          
                                                      }
                                                      
                                                      
                                                      // [self saveDataToPDF:data];
                                                      //NSLog(@"response=%@",response);
                                                      //NSLog(@"error=%@",error);
                                                  }
                                                  else
                                                  {
                                                      //do nothing
                                                      //[self showToast:@"Please try again"];
                                                      
                                                  }
                                              }
                                              else
                                              {
                                                  
                                                  // [self showToast:NSLocalizedString(@"network_error_txt",nil)];
                                                  UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"network_error_txt",nil) delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                                                  [alert show];
                                                  
                                              }
                                          }];
    
    [postDataTask resume];
    


}

-(void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1001)
    { // handle the altdev
        
        [self.navigationController popViewControllerAnimated:YES];

        
    }
    
}

-(void)showToast :(NSString *)toast
{
    [self.view makeToast:toast duration:5.0 position:CSToastPositionBottom];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (fDeviceHeight<=568) {
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.3];
        [UIView setAnimationBeginsFromCurrentState:TRUE];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -kOFFSET_FOR_KEYBOARD, self.view.frame.size.width, self.view.frame.size.height);
        
        [UIView commitAnimations];
    }
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{    if (fDeviceHeight<=568) {
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +kOFFSET_FOR_KEYBOARD, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (fDeviceHeight<=568) {
        
        UITouch * touch = [touches anyObject];
        if(touch.phase == UITouchPhaseBegan) {
            [self hidekeyboard];
            
        }
    }
}

-(void)hidekeyboard
{
    [txt_username resignFirstResponder];
    [txt_userpass resignFirstResponder];
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
