//
//  ForgotMPINVC.h
//  Umang
//
//  Created by admin on 21/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotMPINVC : UIViewController
{
    SharedManager*  singleton ;
}

//----- added for passing rty and tout-----
@property (weak,nonatomic) NSString *strMobileNumber;
@property(nonatomic,retain)NSString *rtry;
@property(assign)int tout;
//----- added for passing rty and tout-----
@property(nonatomic,retain)NSString *tagFrom;
@end
