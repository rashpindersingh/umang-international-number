//
//  NotificationFilterVC.h
//  Umang
//
//  Created by admin on 12/09/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationFilterVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tblFilter;
@property (nonatomic, assign) BOOL isRegionalSelected;
@property (weak, nonatomic) IBOutlet UIButton *btnApply;
@property (weak, nonatomic) IBOutlet UIButton *btnBackHome;
@property (weak, nonatomic) IBOutlet UILabel *lblFilter;
@property (weak, nonatomic) IBOutlet UIButton *btnReset;
@property(strong,nonatomic)NSString *stateSelected;

- (IBAction)btnAlbhabaticClicked:(id)sender;
- (IBAction)btnTopRatedClicked:(id)sender;
- (IBAction)btnNearByClicked:(id)sender;
- (IBAction)btnbackClicked:(id)sender;
- (IBAction)btnApplyClicked:(id)sender;
- (IBAction)btnResetClicked:(id)sender;




@end
