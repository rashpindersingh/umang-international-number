//
//  DigiLockerVC.h
//  Umang
//
//  Created by deepak singh rawat on 21/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DigiLockerVC : UIViewController
{
    IBOutlet UIImageView *digilocker;
    IBOutlet UIButton *btn_shareDocument;
    IBOutlet UIButton *btn_uploadDocument;
    IBOutlet UIButton *btn_linkdigilocker;
    
    
}
-(IBAction)backbtnAction:(id)sender;
-(IBAction)btn_shareDocument:(id)sender;
-(IBAction)btn_uploadDocument:(id)sender;
-(IBAction)btn_linkdigilocker:(id)sender;

@end
