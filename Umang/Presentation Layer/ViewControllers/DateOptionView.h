//
//  DateOptionView.h
//  SampleUmang
//
//  Created by Honey Lakhani on 10/7/16.
//  Copyright © 2016 Honey Lakhani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DateOptionView : UIControl
{
    NSDateFormatter *dateFormatter;
    NSString *selectedDate;
}

@property(nonatomic,strong)UILabel *lblDateOptionFor;
@property(nonatomic,strong)UILabel *lblDate;
@property(nonatomic,strong)UILabel *lblMonth;
@property(nonatomic,strong)UILabel *lblDayName;

@property(nonatomic,strong)UILabel *lblYear;



//for time
@property(nonatomic,strong)UILabel *lblTimeOptionFor;
@property(nonatomic,strong)UILabel *lblTime;

- (instancetype)initWithFrame:(CGRect)frame designedForState:(NSString*)designedFor;
- (instancetype)initWithFrame:(CGRect)frame designedForTime:(NSString*)designedFor;
-(void)updateContentValues:(NSDate*)dt;
-(void)updateValues:(NSDate*)time;
 
@end
