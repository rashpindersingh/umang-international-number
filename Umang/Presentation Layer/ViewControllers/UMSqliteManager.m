//
//  UMSqliteManager.m
//  Umang
//
//  Created by deepak singh rawat on 20/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "UMSqliteManager.h"
#import <sqlite3.h>
#import "UMAPIManager.h"
#import "StateList.h" //for fetching and getting state list

@interface UMSqliteManager()

{
    NSString *databasePath ;
    SharedManager *singleton;
    StateList *obj;
}
@property(nonatomic,assign)sqlite3  *umangDB;
@property (nonatomic, strong) NSString *documentsDirectory;
@property (nonatomic, strong) NSString *databaseFilename;
@property (nonatomic, strong) NSMutableArray *arrResults;


@end

@implementation UMSqliteManager
@synthesize umangDB;

#pragma mark - Initialization

-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename{
    self = [super init];
    if (self) {
        // Set the documents directory path to the documentsDirectory property.
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        self.documentsDirectory = [paths objectAtIndex:0];
        
        // Keep the database filename.
        self.databaseFilename = dbFilename;
        NSLog(@"self.documentsDirectory=%@",self.documentsDirectory);
        NSLog(@"dbFilename=%@",dbFilename);
        
        // Copy the database file into the documents directory if necessary.
        // [self copyDatabaseIntoDocumentsDirectory];
    }
    return self;
}


-(NSString* )getDatabasePath
{
    NSString *docsDir;
    NSArray *dirPaths;
    sqlite3 *DB;
    
    
    
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    // databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"UMANG_DATABASE.db"]];
    databasePath = [docsDir stringByAppendingPathComponent:@"UMANG_DATABASE.db"];
    
    
    
    
    NSFileManager *filemgr = [[NSFileManager alloc]init];
    
    if ([filemgr fileExistsAtPath:databasePath]==NO) {
        
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath,&DB)==SQLITE_OK) {
            
            char *errorMessage;
            const char *sql_statement = "CREATE TABLE IF NOT EXISTS users(ID INTEGER PRIMARY KEY AUTOINCREMENT,FIRSTNAME TEXT,LASTNAME TEXT,EMAILID TEXT,PASSWORD TEXT,BIRTHDATE DATE)";
            if (sqlite3_exec(DB,sql_statement,NULL,NULL,&errorMessage)!=SQLITE_OK) {
                
                NSLog(@"Failed to create the table");
            }
            sqlite3_close(DB);
        }
        else{
            NSLog(@"Failded to open/create the table");
        }
    }
    NSLog(@"database path=%@",databasePath);
    return databasePath;
}

-(NSString*)encodedString:(const unsigned char *)ch
{
    NSString *retStr;
    if(ch == nil)
        retStr = @"";
    else
        retStr = [NSString stringWithCString:(char*)ch encoding:NSUTF8StringEncoding];
    return retStr;
}
-(BOOL)executeScalarQuery:(NSString*)str{
    
    NSLog(@"executeScalarQuery is called =%@",str);
    @try {
        sqlite3_stmt *statement= nil;
        sqlite3 *database;
        BOOL fRet = NO;
        NSString *strPath = [self getDatabasePath];
        if (sqlite3_open([strPath UTF8String],&database) == SQLITE_OK) {
            if (sqlite3_prepare_v2(database, [str UTF8String], -1, &statement, NULL) == SQLITE_OK) {
                if (sqlite3_step(statement) == SQLITE_DONE)
                    fRet =YES;
            }
            
            sqlite3_finalize(statement);
        }
        sqlite3_close(database);
        return fRet;
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

-(NSMutableArray *)executeQuery:(NSString*)str{
    
    sqlite3_stmt *statement= nil; // fetch data from table
    sqlite3 *database;
    NSString *strPath = [self getDatabasePath];
    NSMutableArray *allDataArray = [[NSMutableArray alloc] init];
    if (sqlite3_open([strPath UTF8String],&database) == SQLITE_OK) {
        if (sqlite3_prepare_v2(database, [str UTF8String], -1, &statement, NULL) == SQLITE_OK) {
            
            
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSInteger i = 0;
                NSInteger iColumnCount = sqlite3_column_count(statement);
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                while (i< iColumnCount) {
                    NSString *str = [self encodedString:(const unsigned char*)sqlite3_column_text(statement, (int)i)];
                    
                    
                    NSString *strFieldName = [self encodedString:(const unsigned char*)sqlite3_column_name(statement, (int)i)];
                    
                    [dict setObject:str forKey:strFieldName];
                    i++;
                }
                
                [allDataArray addObject:dict];
            }
        }
        
        sqlite3_finalize(statement);
    }
    sqlite3_close(database);
    return allDataArray;
}











//----------------------------------------------------------
//          insertNotifData TABLE_NOTIFICATIONS
//----------------------------------------------------------


-(void)insertNotifData:(NSString*)notifId
            notifTitle:(NSString*)notifTitle
              notifImg:(NSString*)notifImg
              notifMsg:(NSString*)notifMsg
             notifType:(NSString*)notifType
             notifDate:(NSString*)notifDate
             notifTime:(NSString*)notifTime
            notifState:(NSString*)notifState
            notifIsFav:(NSString*)notifIsFav
             serviceId:(NSString*)serviceId
      currentTimeMills:(NSString*)currentTimeMills
               subType:(NSString*)subType
                   url:(NSString*)url
            screenName:(NSString*)screenName
       receiveDateTime:(NSString*)receiveDateTime
             dialogMsg:(NSString*)dialogMsg
          webpageTitle:(NSString*)webpageTitle
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"insert into TABLE_NOTIFICATIONS values(null, '%@', '%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')", notifId,notifTitle,notifImg, notifMsg,notifType,notifDate,notifTime,notifState,notifIsFav,serviceId,currentTimeMills,subType,url,screenName, receiveDateTime,dialogMsg,webpageTitle];
    
    if ([self executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
    
    
}



//----------------------------------------------------------
//          insertServicesData TABLE_SERVICES_DATA
//----------------------------------------------------


-(void)insertServicesData:(NSString*)serviceId
              serviceName:(NSString*)serviceName
              serviceDesc:(NSString*)serviceDesc
               serviceImg:(NSString*)serviceImg
          serviceCategory:(NSString*)serviceCategory
            serviceSubCat:(NSString*)serviceSubCat
            serviceRating:(NSString*)serviceRating
               serviceUrl:(NSString*)serviceUrl
             serviceState:(NSString*)serviceState
               serviceLat:(NSString*)serviceLat
               serviceLng:(NSString*)serviceLng
             serviceIsFav:(NSString*)serviceIsFav
          serviceIsHidden:(NSString*)serviceIsHidden
       servicePhoneNumber:(NSString*)servicePhoneNumber
    serviceisNotifEnabled:(NSString*)serviceisNotifEnabled
           serviceWebsite:(NSString*)serviceWebsite
{
    
    if(serviceName.length>0)
    {
        serviceName = [NSString stringWithFormat:@"%@%@",[[serviceName substringToIndex:1] uppercaseString],[serviceName substringFromIndex:1] ];
    }
    
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"insert into TABLE_SERVICES_DATA values(null, '%@', '%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')", serviceId,serviceName,serviceDesc, serviceImg,serviceCategory,serviceSubCat,serviceRating,serviceUrl,serviceState,serviceLat,serviceLng,serviceIsFav,serviceIsHidden,servicePhoneNumber, serviceisNotifEnabled,serviceWebsite];
    if ([self executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
}

//----------------------------------------------------------
//          insertServiceSections TABLE_SERVICE_SECTIONS
//----------------------------------------------------------

-(void)insertServiceSections:(NSString*)sectionName
                  sectionImg:(NSString*)sectionImg
             sectionServices:(NSString*)sectionServices
{
    
    if(sectionName.length>0)
    {
        sectionName = [NSString stringWithFormat:@"%@%@",[[sectionName substringToIndex:1] uppercaseString],[sectionName substringFromIndex:1] ];
    }
    
    
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"insert into TABLE_SERVICE_SECTIONS values(null, '%@', '%@','%@')", sectionName,sectionImg,sectionServices];
    if ([self executeScalarQuery:query]==YES)
    {
        
        NSLog(@"insertServiceSections Data  inserted successfully");
        
    }else{
        NSLog(@"insertServiceSections Data not inserted successfully");
    }
    
}



//----------------------------------------------------------
//          updateNotifIsFav TABLE_NOTIFICATIONS
//----------------------------------------------------------


-(void)updateNotifIsFav:(NSString*)notifId
             notifIsFav:(NSString*)notifIsFav
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"update TABLE_NOTIFICATIONS set NOTIF_IS_FAV='%@' where notifId=%@", notifIsFav, notifId];
    if ([self executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
}

//query = [NSString stringWithFormat:@"update peopleInfo set firstname='%@', lastname='%@', age=%d where peopleInfoID=%d", self.txtFirstname.text, self.txtLastname.text, self.txtAge.text.intValue, self.recordIDToEdit];




//----------------------------------------------------------
//          updateServicesData TABLE_SERVICES_DATA
//----------------------------------------------------------


-(void)updateServicesData:(NSString*)serviceId
              serviceName:(NSString*)serviceName
              serviceDesc:(NSString*)serviceDesc
               serviceImg:(NSString*)serviceImg
          serviceCategory:(NSString*)serviceCategory
            serviceSubCat:(NSString*)serviceSubCat
            serviceRating:(NSString*)serviceRating
               serviceUrl:(NSString*)serviceUrl
             serviceState:(NSString*)serviceState
               serviceLat:(NSString*)serviceLat
               serviceLng:(NSString*)serviceLng
             serviceIsFav:(NSString*)serviceIsFav
          serviceIsHidden:(NSString*)serviceIsHidden
       servicePhoneNumber:(NSString*)servicePhoneNumber
    serviceisNotifEnabled:(NSString*)serviceisNotifEnabled
           serviceWebsite:(NSString*)serviceWebsite

{
    
    
    
    if(serviceName.length>0)
    {
        serviceName = [NSString stringWithFormat:@"%@%@",[[serviceName substringToIndex:1] uppercaseString],[serviceName substringFromIndex:1] ];
    }
    
    
    
    
    
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"update TABLE_SERVICES_DATA  set SERVICE_NAME='%@', SERVICE_DESC='%@', SERVICE_IMAGE='%@', SERVICE_CATEGORY='%@', SERVICE_SUB_CATEGORY='%@', SERVICE_RATING='%@', SERVICE_URL='%@', SERVICE_STATE='%@', SERVICE_LATITUDE='%@', SERVICE_LONGITUDE='%@', SERVICE_IS_FAV='%@', SERVICE_IS_HIDDEN='%@', SERVICE_PHONE_NUMBER='%@',SERVICE_IS_NOTIF_ENABLED='%@', SERVICE_WEBSITE='%@' where SERVICE_ID=%@",serviceName,serviceDesc,serviceImg,serviceCategory,serviceSubCat,serviceRating,serviceUrl,serviceState,serviceLat,serviceLng,serviceIsFav,serviceIsHidden,servicePhoneNumber,serviceisNotifEnabled,serviceWebsite, serviceId];
    if ([self executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
}

//----------------------------------------------------------
//          getServiceData TABLE_SERVICES_DATA
//----------------------------------------------------------



-(NSArray*)getServiceData:(NSString*)serviceId
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"SELECT * FROM TABLE_SERVICES_DATA where SERVICE_ID=%@", serviceId];
    NSArray *arry=[self executeQuery:query];
    
    return arry;
}



//----------------------------------------------------------
//          getServiceFavStatis TABLE_SERVICES_DATA
//----------------------------------------------------------

-(NSString*)getServiceFavStatus:(NSString*)serviceId
{
    NSString *query;
    query = [NSString stringWithFormat:@"SELECT SERVICE_IS_FAV FROM TABLE_SERVICES_DATA where SERVICE_ID=%@", serviceId];
    NSArray *arry=[self executeQuery:query];
    NSLog(@"array=%@",[[arry objectAtIndex:0]valueForKey:@"SERVICE_IS_FAV"]);
    return [[arry objectAtIndex:0]valueForKey:@"SERVICE_IS_FAV"];
    
    
}


//----------------------------------------------------------
//          updateServiceIsFav TABLE_SERVICES_DATA
//----------------------------------------------------------


-(void)updateServiceIsFav:(NSString*)serviceId
             serviceIsFav:(NSString*)serviceIsFav hitAPI:(NSString*)hitStatus
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"update TABLE_SERVICES_DATA  set SERVICE_IS_FAV='%@' where SERVICE_ID=%@",serviceIsFav, serviceId];
    if ([self executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
    if ([hitStatus isEqualToString:@"Yes"])
    {
        [self hitAPI:serviceId favUnfav:serviceIsFav];
        
    }
}




//----- hitAPI for IVR OTP call Type registration ------
-(void)hitAPI:(NSString*)serviceID favUnfav:(NSString*)favStatus
{
    singleton = [SharedManager sharedSingleton];
    
    
    if ([favStatus isEqualToString:@"true"])
    {
        favStatus=@"y";
    }
    else
    {
        favStatus=@"n";
        
    }
    
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:serviceID forKey:@"sid"];//Enter mobile number of user
    
    [dictBody setObject:favStatus forKey:@"isfav"];//Enter mobile number of user
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_UNSET_FAVORITE withBody:dictBody andTag:TAG_REQUEST_UNSET_FAVORITE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n value of tkn=%@ ",rc,rs,tkn);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            /*  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
             message:error.localizedDescription
             delegate:self
             cancelButtonTitle:@"OK"
             otherButtonTitles:nil];
             [alert show];*/
            
        }
        
    }];
    
}
//----------------------------------------------------------
//          updateServiceIsNotifEnabled TABLE_SERVICES_DATA
//----------------------------------------------------------


-(void)updateServiceIsNotifEnabled:(NSString*)serviceId
                    notifIsEnabled:(NSString*)notifIsEnabled
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"update TABLE_SERVICES_DATA  set SERVICE_IS_NOTIF_ENABLED='%@' where SERVICE_ID=%@",notifIsEnabled, serviceId];
    if ([self executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
}


//----------------------------------------------------------
//          getServiceIsNotifEnabled TABLE_SERVICES_DATA
//----------------------------------------------------------




-(NSString*)getServiceIsNotifEnabled:(NSString*)serviceId
{
    NSString *query;
    query = [NSString stringWithFormat:@"SELECT SERVICE_IS_NOTIF_ENABLED FROM TABLE_SERVICES_DATA where SERVICE_ID=%@", serviceId];
    NSArray *arry=[self executeQuery:query];
    NSLog(@"array=%@",[[arry objectAtIndex:0]valueForKey:@"SERVICE_IS_NOTIF_ENABLED"]);
    return [[arry objectAtIndex:0]valueForKey:@"SERVICE_IS_FAV"];
    
    
}




//----------------------------------------------------------
//          deleteNotification TABLE_NOTIFICATIONS
//----------------------------------------------------------


-(void)deleteNotification:(NSString*)notifId
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"DELETE FROM TABLE_NOTIFICATIONS where NOTIF_ID=%@", notifId];
    // Execute the query.
    if ([self executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
}


//----------------------------------------------------------
//          deleteAllNotifications TABLE_NOTIFICATIONS
//----------------------------------------------------------


-(void)deleteAllNotifications
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"DELETE FROM TABLE_NOTIFICATIONS"];
    if ([self executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
}

//----------------------------------------------------------
//          deleteServiceData TABLE_SERVICES_DATA
//----------------------------------------------------------


-(void)deleteServiceData:(NSString*)serviceId
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"DELETE FROM TABLE_SERVICES_DATA where SERVICE_ID=%@", serviceId];
    if ([self executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
}

//----------------------------------------------------------
//          deleteBannerHomeData TABLE_BANNER_HOME
//----------------------------------------------------------


-(void)deleteBannerHomeData
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"DELETE FROM TABLE_BANNER_HOME"];
    if ([self executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  Delete successfully");
        
    }else{
        NSLog(@"Data not Deleted ");
    }
    
}

//----------------------------------------------------------
//          deleteBannerStateData TABLE_BANNER_HOME
//----------------------------------------------------------


-(void)deleteBannerStateData
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"DELETE FROM TABLE_BANNER_STATE"];
    if ([self executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
}






//----------------------------------------------------------
//          deleteAllServices TABLE_SERVICES_DATA
//----------------------------------------------------------


-(void)deleteAllServices
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"DELETE FROM TABLE_SERVICES_DATA"];
    if ([self executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
}




//----------------------------------------------------------
//          deleteSectionData TABLE_SERVICE_SECTIONS
//----------------------------------------------------------


-(void)deleteSectionData
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"DELETE FROM TABLE_SERVICE_SECTIONS"];
    if ([self executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
}

//----------------------------------------------------------
//          Create UMANG DATABASE TABLES
//----------------------------------------------------------



-(void)createUmangDB
{
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"UMANG_DATABASE.db"]];
    
    
    NSLog(@"databasePath=%@",databasePath);
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &umangDB) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt = "CREATE TABLE IF NOT EXISTS TABLE_NOTIFICATIONS(ID INTEGER PRIMARY KEY AUTOINCREMENT,NOTIF_ID TEXT,NOTIF_TITLE TEXT,NOTIF_IMG TEXT,NOTIF_MSG TEXT,NOTIF_TYPE TEXT,NOTIF_DATE TEXT,NOTIF_TIME TEXT,NOTIF_STATE TEXT,NOTIF_IS_FAV TEXT,SERVICE_ID TEXT,CURRENT_TIME_MILLS TEXT,NOTIF_SUB_TYPE TEXT,NOTIF_URL TEXT,NOTIF_SCREEN_NAME TEXT,NOTIF_RECEIVE_DATE_TIME TEXT,NOTIF_DIALOG_MSG TEXT,NOTIF_WEBPAGE_TITLE TEXT);"//TABLE_NOTIFICATIONS
            
            "CREATE TABLE IF NOT EXISTS TABLE_SERVICES_DATA(ID INTEGER PRIMARY KEY AUTOINCREMENT,SERVICE_ID TEXT,SERVICE_NAME TEXT,SERVICE_DESC TEXT,SERVICE_IMAGE TEXT,SERVICE_CATEGORY TEXT, SERVICE_SUB_CATEGORY TEXT,SERVICE_RATING TEXT,SERVICE_URL TEXT,SERVICE_STATE TEXT, SERVICE_LATITUDE TEXT,SERVICE_LONGITUDE TEXT,SERVICE_IS_FAV TEXT,SERVICE_IS_HIDDEN TEXT,SERVICE_PHONE_NUMBER TEXT,SERVICE_IS_NOTIF_ENABLED TEXT,SERVICE_WEBSITE TEXT);"//TABLE_SERVICES_DATA
            
            "CREATE TABLE IF NOT EXISTS TABLE_SERVICE_SECTIONS(ID INTEGER PRIMARY KEY AUTOINCREMENT,SECTION_NAME TEXT,SECTION_IMAGE TEXT,SECTION_SERVICES TEXT);"//TABLE_SERVICE_SECTIONS
            
            "CREATE TABLE IF NOT EXISTS TABLE_BANNER_HOME (ID INTEGER PRIMARY KEY AUTOINCREMENT,BANNER_IMAGE_URL TEXT,BANNER_ACTION_TYPE TEXT,BANNER_ACTION_URL TEXT,BANNER_DESC TEXT);"//TABLE_BANNER_HOME
            
            
            
            "CREATE TABLE IF NOT EXISTS TABLE_BANNER_STATE(ID INTEGER PRIMARY KEY AUTOINCREMENT,BANNER_IMAGE_URL TEXT,BANNER_ACTION_TYPE TEXT,BANNER_ACTION_URL TEXT,BANNER_DESC TEXT)";//TABLE_BANNER_STATE
            
            
            
            
            
            if (sqlite3_exec(umangDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                NSLog(@"Failed to create tables");
            }
            sqlite3_close(umangDB);
            NSLog(@"DataBase created at: %@",databasePath);
        } else {
            NSLog(@"Failed to open/create database");
        }
    }
    
    
}


-(NSArray*)loadDataServiceSection
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"select * from TABLE_SERVICE_SECTIONS"];
    NSArray *arry=[self executeQuery:query];
    
    return arry;
}



-(NSArray*)loadDataServiceData
{
    // Prepare the query string.
    NSString *query;
    // query = [NSString stringWithFormat:@"select * from TABLE_SERVICES_DATA"];
    
    query = [NSString stringWithFormat:@"SELECT DISTINCT SERVICE_ID ,SERVICE_NAME ,SERVICE_DESC ,SERVICE_IMAGE ,SERVICE_CATEGORY , SERVICE_SUB_CATEGORY ,SERVICE_RATING ,SERVICE_URL ,SERVICE_STATE , SERVICE_LATITUDE ,SERVICE_LONGITUDE ,SERVICE_IS_FAV ,SERVICE_IS_HIDDEN ,SERVICE_PHONE_NUMBER ,SERVICE_IS_NOTIF_ENABLED ,SERVICE_WEBSITE  from TABLE_SERVICES_DATA"];
    
    
    
    NSArray *arry=[self executeQuery:query];
    
    return arry;
}



-(NSArray*)loadServiceCategory
{
    // Prepare the query string.
    NSString *query;
    
    query = [NSString stringWithFormat:@"SELECT DISTINCT SERVICE_CATEGORY from TABLE_SERVICES_DATA"];
    
    
    
    NSArray *arry=[self executeQuery:query];
    
    return arry;
}



//----------------------------------------------------------
//          insertBannerHomeData TABLE_BANNER_HOME
//----------------------------------------------------------


-(void)insertBannerHomeData:(NSString*)bannerImgUrl
           bannerActionType:(NSString*)bannerActionType
            bannerActionUrl:(NSString*)bannerActionUrl
                 bannerDesc:(NSString*)bannerDesc

{
    
    
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"insert into TABLE_BANNER_HOME values(null, '%@', '%@','%@','%@')", bannerImgUrl,bannerActionType,bannerActionUrl, bannerDesc];
    
    if ([self executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
    
    
}

//----------------------------------------------------------
//          insertBannerStateData TABLE_BANNER_STATE
//----------------------------------------------------------


-(void)insertBannerStateData:(NSString*)bannerImgUrl
            bannerActionType:(NSString*)bannerActionType
             bannerActionUrl:(NSString*)bannerActionUrl
                  bannerDesc:(NSString*)bannerDesc

{
    
    
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"insert into TABLE_BANNER_STATE values(null, '%@', '%@','%@','%@')", bannerImgUrl,bannerActionType,bannerActionUrl, bannerDesc];
    
    if ([self executeScalarQuery:query]==YES)
    {
        
        NSLog(@"Data  inserted successfully");
        
    }else{
        NSLog(@"Data not inserted successfully");
    }
    
    
    
}



-(NSArray*)getBannerStateData
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"select * from TABLE_BANNER_STATE"];
    NSArray *arry=[self executeQuery:query];
    
    return arry;
}


-(NSArray*)getBannerHomeData
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"select * from TABLE_BANNER_HOME"];
    NSArray *arry=[self executeQuery:query];
    
    return arry;
}














-(NSArray*)getServicesDataForNotifSettings
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"select * from TABLE_SERVICES_DATA"];
    NSArray *arry=[self executeQuery:query];
    
    return arry;
}






-(NSArray*)getNotifData
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"select * from TABLE_NOTIFICATIONS"];
    NSArray *arry=[self executeQuery:query];
    
    return arry;
}



-(NSArray*)getTrendingServiceData
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"select * from TABLE_SERVICE_SECTIONS where  SECTION_NAME = 'Trending'"];
    NSArray *arry=[self executeQuery:query];
    ///need below implement
    return arry;
}






-(NSArray*)getFilteredNotifData
{
    // Prepare the query string.
    NSString *query;
    query = [NSString stringWithFormat:@"select * from TABLE_NOTIFICATIONS"];
    NSArray *arry=[self executeQuery:query];
    ///need below implement
    return arry;
}

//sortby Alphabet/MostPopular/TopRated
//serviceType All/CentralGov/Regional
//stateIdAlist its a Comma seprated values eg uttarakhand,delhi,Punjab etc
//categoryList its a Comma seprated values eg Healthcare,Education,Agriculture etc

-(NSArray*)getFilteredServiceData:(NSString*)sortBy serviceType:(NSString*)serviceType stateIdAlist:(NSArray*)stateIdAlist categoryList:(NSArray*)categoryList
{
    NSString *query;
    NSString *lastquery;
    
    NSString *myQuery;
    NSString *statequery;
    NSString *categoryquery;
    
    query = [NSString stringWithFormat:@"select * from TABLE_SERVICES_DATA"];
    
    if ([sortBy isEqualToString:@"Alphabetic"]) {
        lastquery=[NSString stringWithFormat:@" ORDER BY SERVICE_NAME ASC"];
        NSLog(@"Alphabetic Sort query=%@",lastquery);
        
    }
    if ([sortBy isEqualToString:@"MostPopular"])//do nothing as nothing info provided
    {
        lastquery=[NSString stringWithFormat:@" ORDER BY SERVICE_RATING DESC"];
        NSLog(@"MostPopular query=%@",lastquery);
        
    }
    
    if ([sortBy isEqualToString:@"TopRated"])//Sort as per Rating
    {
        lastquery=[NSString stringWithFormat:@" ORDER BY SERVICE_RATING DESC"];
        NSLog(@"TopRated query=%@",lastquery);
        
    }
    
    
    if ([serviceType isEqualToString:@"All"])//
    {
        //do nothing
    }
    
    if ([serviceType isEqualToString:@"CentralGovt"])//
    {
        //do nothing
        statequery=[NSString stringWithFormat:@"WHERE SERVICE_STATE IN ('%@')",@"99"]; //99 in case of central governemnt
        //For Central Government Services the state id will be 99 and currently there is no option for sorting by Most Popular.
        myQuery=[NSString stringWithFormat:@"%@ %@",query,statequery];
        
    }
    
    if ([serviceType isEqualToString:@"Regional"])//
    {
        if ([stateIdAlist count]!=0) {
            
            
            // state_id= [obj getStateCode:str_state];
            
            
            
            NSMutableArray *arr_stateId=[[NSMutableArray alloc]init];
            
            for (int i=0; i<[stateIdAlist count]; i++)
            {
                
                NSString *stateName = [[stateIdAlist objectAtIndex:i] lowercaseString];
                
                obj=[[StateList alloc]init];
                
                NSString* state_id= [obj getStateCode:stateName];
                
                [arr_stateId addObject:state_id];
            }
            NSString *joinedComponents = [arr_stateId componentsJoinedByString:@"','"];
            statequery=[NSString stringWithFormat:@"WHERE SERVICE_STATE IN ('%@')",joinedComponents];
            
            myQuery=[NSString stringWithFormat:@"%@ %@",query,statequery];
            
        }
        
        //query= subquery+subquery1
        
    }
    if ([categoryList count]!=0)
    {
        NSString *joinedComponents = [categoryList componentsJoinedByString:@"','"];
        categoryquery=[NSString stringWithFormat:@"SERVICE_CATEGORY IN ('%@')",joinedComponents];
        
        NSLog(@"categoryquery= %@",categoryquery);
        // AND
        myQuery=[NSString stringWithFormat:@"%@ WHERE %@",query,categoryquery];
        
    }
    
    
    
    if ([categoryquery length]!=0)
    {
        if ([statequery length]!=0)
        {
            
            myQuery=[NSString stringWithFormat:@"%@ %@ AND %@",query,statequery,categoryquery];
        }
        
    }
    
    
    
    if ([myQuery length]!=0) {
        query=[NSString stringWithFormat:@"%@ %@",myQuery,lastquery];
        
    }
    else
        query=[NSString stringWithFormat:@"%@ %@",query,lastquery];
    
    
    
    NSArray *arry=[self executeQuery:query];
    return arry;
    
}

// NSString *dateStartString = @“31/12/2010 11:04:02”;

-(NSArray*)getFilteredNotifData:(NSString*)sortBy notifType:(NSString*)notifType serviceIdAlist:(NSArray*)serviceIdAlist stateIdAlist:(NSArray*)stateIdAlist startDate:(NSString*)startDate  endDate:(NSString*)endDate
{
    // Prepare the query string.
    NSString *query;
    NSString *notifTypeQuery;
    NSString *statequery;
    NSString *serviceIdquery;
    NSString *lastquery;
    NSString *finalQuery;
    NSString *ExecuteQuery;
    
    query = [NSString stringWithFormat:@"select * from TABLE_NOTIFICATIONS where"];
    
    // key to pass notifType = promo/trans/All
    
    if ([notifType isEqualToString:@"promo"]) //Promotional case
    {
        notifTypeQuery=[NSString stringWithFormat:@"NOTIF_TYPE=%@",@"'promo'"];
        
        finalQuery =[NSString stringWithFormat:@"%@ %@",query,notifTypeQuery];
        
        
        
    }
    if ([notifType isEqualToString:@"trans"]) //Transactional
    {
        notifTypeQuery=[NSString stringWithFormat:@"NOTIF_TYPE=%@",@"'trans'"];
        
        finalQuery =[NSString stringWithFormat:@"%@ %@",query,notifTypeQuery];
        
        
    }
    if ([notifType isEqualToString:@"All"]) // ALL
    {
        notifTypeQuery=[NSString stringWithFormat:@" NOTIF_TYPE  IN ('promo', 'trans')"];
        finalQuery =[NSString stringWithFormat:@"%@ %@",query,notifTypeQuery];
        
    }
    
    
    
    
    
    // Handle StateID data fetch
    
    
    if ([stateIdAlist count]!=0)
    {
        
        NSMutableArray *arr_stateId=[[NSMutableArray alloc]init];
        
        for (int i=0; i<[stateIdAlist count]; i++)
        {
            
            NSString *stateName = [[stateIdAlist objectAtIndex:i] lowercaseString];
            obj=[[StateList alloc]init];
            NSString* state_id= [obj getStateCode:stateName];
            [arr_stateId addObject:state_id];
        }
        NSString *joinedComponents = [arr_stateId componentsJoinedByString:@"','"];
        statequery=[NSString stringWithFormat:@"AND  NOTIF_STATE IN ('%@')",joinedComponents];
        finalQuery=[NSString stringWithFormat:@"%@ %@ %@",query,notifTypeQuery,statequery];
    }
    
    if ([serviceIdAlist count]!=0)
    {
        
        
        NSString *joinedServiceComponents = [serviceIdAlist componentsJoinedByString:@"','"];
        serviceIdquery=[NSString stringWithFormat:@"AND  SERVICE_ID IN ('%@')",joinedServiceComponents];
        
        
        
        if ([statequery length]!=0) {
            //serviceIdquery=[NSString stringWithFormat:@"%@ %@ ",statequery, serviceIdquery];
            finalQuery=[NSString stringWithFormat:@"%@ %@ %@ %@",query,notifTypeQuery,statequery,serviceIdquery];
        }
        else
        {
            // serviceIdquery=[NSString stringWithFormat:@"%@",serviceIdquery];
            finalQuery=[NSString stringWithFormat:@"%@ %@ %@",query,notifTypeQuery,serviceIdquery];
        }
    }
    
    //query= query+myquery+serviceIDquery
    
    
    
    if ([sortBy isEqualToString:@"Alphabetic"]) {
        lastquery=[NSString stringWithFormat:@" ORDER BY NOTIF_TITLE ASC"];
        NSLog(@"Alphabetic Sort query=%@",lastquery);
        
    }
    if ([sortBy isEqualToString:@"MostPopular"])// no details provided
    {
        lastquery=[NSString stringWithFormat:@" ORDER BY NOTIF_TITLE DESC"];
        NSLog(@"MostPopular query=%@",lastquery);
        
    }
    
    if ([sortBy isEqualToString:@"TopRated"])// no details provided
    {
        lastquery=[NSString stringWithFormat:@" ORDER BY NOTIF_TITLE DESC"];
        NSLog(@"TopRated query=%@",lastquery);
        
    }
    
    //   NSString *ExecuteQuery=[NSString stringWithFormat:@"%@ %@",finalQuery,lastquery];
    
    // NSString *dateStartString = @“31/12/2010 11:04:02”;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    
    
    NSDate *dateStart = [[NSDate alloc] init];
    dateStart = [dateFormatter dateFromString:startDate];
    
    
    NSDate *endStart = [[NSDate alloc] init];
    endStart = [dateFormatter dateFromString:endDate];
    
    if ([startDate length]!=0 &&[endDate length]!=0)
    {
        NSString *dateQuery=[NSString stringWithFormat:@"AND NOTIF_RECEIVE_DATE_TIME >= '%@' AND NOTIF_RECEIVE_DATE_TIME  <= '%@'",startDate,endDate];
        ExecuteQuery=[NSString stringWithFormat:@"%@ %@ %@",finalQuery,dateQuery,lastquery];
        
    }
    else
    {
        ExecuteQuery=[NSString stringWithFormat:@"%@ %@",finalQuery,lastquery];
        
    }
    
    
    
    NSLog(@"ExecuteQuery =%@",ExecuteQuery);
    
    /*
     
     Select * from TABLE_NOTIFICATIONS where NOTIF_TYPE ='promo' AND  NOTIF_STATE IN ('15','9999','9999') AND  SERVICE_ID IN ('1','2','3') AND NOTIF_RECEIVE_DATE_TIME >= '31/12/2010 11:04:02' AND NOTIF_RECEIVE_DATE_TIME  <= '31/12/2010 11:04:02'  ORDER BY NOTIF_TITLE DESC
     
     
     */
    
    
    
    NSArray *arry=[self executeQuery:ExecuteQuery];
    ///need below implement
    return arry;
}


/*
 synchronized public ArrayList<NotificationBean> getFilteredNotifData(String notifType, ArrayList<String> serviceIdAlist, ArrayList<String> stateIdAlist, String startDate, String endDate) throws SQLException {
 
 String query = "select * from " + TABLE_NOTIFICATIONS + " where ";
 if (notifType.equalsIgnoreCase("promo")) {
 query = query + NOTIF_TYPE + " = 'promo'";
 } else if (notifType.equalsIgnoreCase("trans")) {
 query = query + NOTIF_TYPE + " = 'trans'";
 }
 
 if (query.contains(NOTIF_TYPE) && serviceIdAlist.size() > 0) {
 query = query + " AND ";
 }
 if (serviceIdAlist.size() > 0) {
 String inClause = serviceIdAlist.toString();
 inClause = inClause.replace("[", "(");
 inClause = inClause.replace("]", ")");
 
 query = query + SERVICE_ID + " IN " + inClause;
 }
 
 if ((query.contains(NOTIF_TYPE) || query.contains(SERVICE_ID)) && stateIdAlist.size() > 0) {
 query = query + " AND ";
 }
 if (stateIdAlist.size() > 0) {
 String inClause = stateIdAlist.toString();
 inClause = inClause.replace("[", "(");
 inClause = inClause.replace("]", ")");
 
 query = query + NOTIF_STATE + " IN " + inClause;
 }
 
 if (query.contains(NOTIF_TYPE) || query.contains(SERVICE_ID) || query.contains(NOTIF_STATE)) {
 query = query + " AND ";
 }
 
 query = query + NOTIF_RECEIVE_DATE_TIME + " >= Datetime('" + startDate + "')" + " AND " + NOTIF_RECEIVE_DATE_TIME + " <= Datetime('" + endDate + "');";
 
 Log.d(TAG, "query........" + query);
 
 ArrayList<NotificationBean> dataAlist = new ArrayList<>();
 openDatabase();
 
 try {
 Cursor c = null;
 c = myDB.rawQuery(query, null);
 if (c != null) {
 if (c.moveToFirst()) {
 do {
 NotificationBean bean = new NotificationBean();
 bean.setNotifId(c.getString(1));
 bean.setTitle(c.getString(2));
 bean.setImg(c.getString(3));
 bean.setMsg(c.getString(4));
 bean.setType(c.getString(5));
 bean.setDate(c.getString(6));
 bean.setTime(c.getString(7));
 bean.setState(c.getString(8));
 bean.setFav(Boolean.parseBoolean(c.getString(9)));
 bean.setServiceId(c.getString(10));
 bean.setCurrentTimeMills(c.getString(11));
 dataAlist.add(bean);
 
 } while (c.moveToNext());
 } else {
 Log.d(TAG, "c empty..............");
 }
 c.close();
 } else {
 Log.d(TAG, "c == null.............");
 }
 } catch (Exception e) {
 e.printStackTrace();
 }
 closeDatabase();
 return dataAlist;
 }
 
 
 
 */
@end
