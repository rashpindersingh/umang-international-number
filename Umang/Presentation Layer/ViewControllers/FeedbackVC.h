//
//  FeedbackVC.h
//  Umang
//
//  Created by admin on 29/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackVC : UIViewController
{
    IBOutlet UILabel *lbl_instruction;
}
@property (weak, nonatomic) IBOutlet UITableView *tblFeedback;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic)NSString *jumpFromVC;

@property (nonatomic,copy) NSString *ratingFromFeedback;
- (IBAction)btnSubmitClicked:(id)sender;

@end


