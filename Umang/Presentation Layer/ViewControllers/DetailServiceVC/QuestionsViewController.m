//
//  QuestionsViewController.m
//  Umang
//
//  Created by Lokesh Jain on 01/12/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "QuestionsViewController.h"

@interface QuestionsViewController ()

@end

@implementation QuestionsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [backButton setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    screenTitleLabel.text = self.screenTitleString;
    
}
- (IBAction)backButtonPressed:(UIButton *)sender
{
    if ([self.tagComingFrom isEqualToString:@"ForgotMPIN"])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if ([self.tagComingFrom isEqualToString:@"Set MPIN"])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if ([self.tagComingFrom isEqualToString:@"Set MPIN Adhaar"])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}

#pragma mark - UITableView DataSource and Delegate Methods


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.questionsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 50.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"questionCell"];
    
    cell.textLabel.text = [[self.questionsArray objectAtIndex:indexPath.row] valueForKey:@"question"];
    
    if (fDeviceWidth == 320)
    {
        cell.textLabel.font = [AppFont regularFont:15.0];
    }
    else
    {
        cell.textLabel.font = [AppFont regularFont:16.0];
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    NSString *questionString = [[self.questionsArray objectAtIndex:indexPath.row] valueForKey:@"question"];
    
    if (questionString != nil && questionString.length != 0)
    {
        self.finishState(questionString);
    }
    
    
    if ([self.tagComingFrom isEqualToString:@"ForgotMPIN"])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if ([self.tagComingFrom isEqualToString:@"Set MPIN"])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if ([self.tagComingFrom isEqualToString:@"Set MPIN Adhaar"])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}

#pragma mark -
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
