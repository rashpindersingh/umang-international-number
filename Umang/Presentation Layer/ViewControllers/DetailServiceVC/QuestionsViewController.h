//
//  QuestionsViewController.h
//  Umang
//
//  Created by Lokesh Jain on 01/12/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionsViewController : UIViewController
{
    
    IBOutlet UILabel *screenTitleLabel;
    IBOutlet UIButton *backButton;
}

@property (strong, nonatomic) IBOutlet UITableView *questionsTableView;
@property (nonatomic,strong) NSMutableArray *questionsArray;
@property (nonatomic,copy) NSString *screenTitleString;
@property (nonatomic,copy)NSString *tagComingFrom;
@property(copy) void(^finishState)(NSString *questionString);

@end
