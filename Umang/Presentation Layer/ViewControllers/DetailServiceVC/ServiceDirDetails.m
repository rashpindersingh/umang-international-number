//
//  DetailServiceNewVC.m
//  Umang
//
//  Created by admin on 17/07/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "ServiceDirDetails.h"
#import "DescriptionCell.h"
#import "AddressCell.h"
#import "WorkingHoursCell.h"
#import "MainDetailCell.h"
#import "ContactDetailCell.h"
#import "SimpleBarChart.h"
#import "AMRatingControl.h"
#import "RatingGraphView.h"
#import "RatingBarCell.h"
#import "UIImageView+WebCache.h"
#import <MessageUI/MessageUI.h>
#import "HomeDetailVC.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"
#import "serviceCallContactVC.h"
#import "DetailParalaxHeaderView.h"
#import "rateCommentVC.h"
#import "DetailParalaxHeaderView.h"
#import "UIView+Toast.h"
#import "DetailCustomTableViewCell.h"
#import "ContactDetailServiceCell.h"
#import "CallBoxTVC.h"
#import "DetailServiceNewVC.h"
#import "RunOnMainThread.h"
#import "StateList.h"

static const NSInteger kStarWidthAndHeight = 25;
#pragma mark - ServicesInfoCell

@implementation ServicesInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end


#pragma mark - ServiceDirDetails

@interface ServiceDirDetails ()<UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate,UIActionSheetDelegate, GSKStretchyHeaderViewStretchDelegate>
{
    __weak IBOutlet DetailParalaxHeaderView *serviceHeader;
    __weak IBOutlet UITableView *tblDetail;
    BOOL isRatingShown;
    BOOL isRatingCellShown;
    
    __weak IBOutlet MyFavButton *btnFavService;
    __weak IBOutlet UIButton *btnBack;
    AddressCell *addressCell;
    ContactDetailServiceCell *contactCell;
    // ContactDetailCell *contactCell;
    
    MainDetailCell *mainCell;
    SharedManager *singleton;
    MBProgressHUD *hud ;
    serviceCallContactVC *serviceVC;
    rateCommentVC *rateVc;
    IBOutlet UIButton *vistiService;
    RatingGraphView *vwRating ;
    
    CGRect framevwRatebtn;    //deepak change
    
    int totalDownload;
    dispatch_group_t serviceGroup;

    CGFloat fontApplied;
    CGFloat headerTitleHeight;
    CGFloat descCellHeight;
    NSMutableArray *arrServicesInfo;
    NSMutableArray *arrSections;
    BOOL networkStatus;
    ServicesInfoCell *servicesCell;
    BOOL isWebCall;
    NSMutableArray *arrLinks;
    // UIBarButtonItem *favBarBtn;
    // UIBarButtonItem *shareBarBtn;
}
@property (weak)IBOutlet UILabel *titleview;

@property(nonatomic,retain)NSString *ratePass;
@property(nonatomic,retain)NSString *commentPass;


@property(nonatomic,retain)NSString *rt1;
@property(nonatomic,retain)NSString *rt2;
@property(nonatomic,retain)NSString *rt3;
@property(nonatomic,retain)NSString *rt4;
@property(nonatomic,retain)NSString *rt5;
@property(nonatomic,retain)NSString *oldRating;
@property(nonatomic,retain)NSArray *contactitems ;
@end

@implementation ServiceDirDetails
@synthesize dic_serviceInfo;
@synthesize commentPass;
@synthesize rt1,rt2,rt3,rt4,rt5,oldRating;


#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *strWebistes = [self.dic_serviceInfo valueForKey:@"OTHER_WEBSITE"];
    if (strWebistes.length != 0)
      {
        arrLinks = [[NSMutableArray alloc] init];
        if ([strWebistes containsString:@"|"]) {
            [arrLinks addObjectsFromArray:[strWebistes componentsSeparatedByString:@"|"]];
        }else {
            [arrLinks addObject:strWebistes];
        }
    }
    NSLog(@"dic_serviceInfo details -----%@",dic_serviceInfo);
    isWebCall = true ;
    arrServicesInfo = [[NSMutableArray alloc] init];
  
    arrSections = [[NSMutableArray alloc] initWithObjects:@"DescriptionCell",@"ContactDetailServiceCell",@"WorkingHoursCell", nil];
    
    NSString *strNative = [dic_serviceInfo valueForKey:@"SERVICE_NATIVE_APP"];
    if (strNative == nil || strNative.length != 0)
     {
         [arrSections addObject:@"cellLinks"];
     }
    if (arrLinks != nil && arrLinks.count != 0) {
        [arrSections addObject:@"CellLinksWebsite"];
    }
    [arrSections addObject:@"ServicesInfoCell"];
    [tblDetail reloadData];
    singleton=[SharedManager sharedSingleton];
    
    fontApplied = 14.0;
    if ([singleton.fontSizeSelected isEqualToString:NSLocalizedString(@"large", nil)])
    {
        fontApplied=18.0;
        
    }
    headerTitleHeight = 50.0;
    descCellHeight = 100;
    NSString *descriptionTxt=[dic_serviceInfo valueForKey:@"SERVICE_DESC"];
    CGRect dynamicHeight = [self rectForText:descriptionTxt usingFont:[UIFont systemFontOfSize:fontApplied] boundedBySize:CGSizeMake(self.view.frame.size.width-40, 1000.0)];
    if (dynamicHeight.size.height <= 65) {
        descCellHeight = 90;
    }
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    
    totalDownload=0;
    NSLog(@"dic_serviceInfo=%@",dic_serviceInfo);
    // [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];

    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [@"Back"/*NSLocalizedString(@"back", nil)*/ sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    [btnBack setTitle:@"Back" /*NSLocalizedString(@"back", nil)*/ forState:UIControlStateNormal];
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:DETAIL_SERVICE_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    //  [contactCell.btnCall setTitle:NSLocalizedString(@"call", nil) forState:UIControlStateNormal];
    
    NSString *strServiceName = [dic_serviceInfo valueForKey:@"SERVICE_NAME"];
    NSString *strOpen = [NSString stringWithFormat:@"Open %@", strServiceName];
    [vistiService setTitle:[strOpen uppercaseString] /*NSLocalizedString(@"visit_service", nil)*/ forState:UIControlStateNormal];
    [vistiService.titleLabel setFont:[AppFont mediumFont:16]];
    vistiService.titleLabel.numberOfLines = 3;
    vistiService.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    addressCell.btn_viewmap.titleLabel.text= @"View on Map";//NSLocalizedString(@"view_on_map", nil);
    
    
    
    //    UIImage *dot, *star;
    //    dot = [UIImage imageNamed:@"star_border"];
    //    star = [UIImage imageNamed:@"star_filled"];
    
    //  //  [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(hitFetchUserRatingAPI)
    //                                                 name:@"RELOADRATING"
    //                                               object:nil];
    
    
    
    
    //mainCell.lbl_titlerateus.text = NSLocalizedString(@"rate_service_on_umang", nil);
    // mainCell.lbl_serviceRating.text= [NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_RATING"]];
    // [mainCell.btnViewDetails setTitle:NSLocalizedString(@"view_ratings", nil) forState:UIControlStateNormal];
    
    // Do any additional setup after loading the view.
    // isRatingShown = YES;
    
    //_titleview.frame=CGRectMake(84, 30, fDeviceWidth-100, 30);
    
    //  _titleview.text= NSLocalizedString(@"back", nil);//[NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_NAME"]];
    
    
    NSLog(@"dic_serviceInfo=%@",dic_serviceInfo);
    [self tableViewHeaderParalxView];
    [tblDetail reloadData];
    
    if ([self.isFrom isEqualToString:@"DEPARTMENT"])
    {
        [self scrollToBottom];
    }
    else if ([self.isFrom isEqualToString:@"not_on_umang"])
    {
        vistiService.hidden = true;
        _heightBtnVisitServiceConstraint.constant = 0.0;
      //  [self.view layoutIfNeeded];
        //[self.view updateConstraintsIfNeeded];
    }
    [self startWebApiCallDispatch];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    
    //    mainCell.lbl_titlerateus.text = NSLocalizedString(@"rate_service_on_umang", nil);
    //    [mainCell.btnViewDetails  setTitle:NSLocalizedString(@"view_ratings", nil) forState:UIControlStateNormal];
    //
    //    mainCell.btnViewDetails.titleLabel.text=NSLocalizedString(@"view_ratings", nil);
    //
    //    if (![self connected]) {
    //        // Not connected
    //
    //    } else {
    //        // Connected. Do some Internet stuff
    //        [self hitFetchUserRatingAPI];
    //
    //    }
    //[tblDetail reloadData];
    
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//
//
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [tblDetail reloadData];
//        });
//    });
    
    [super viewWillAppear:NO];
}

- (void)scrollToBottom
{
    CGFloat yOffset = 0;
    
    if (tblDetail.contentSize.height > tblDetail.bounds.size.height) {
        yOffset = tblDetail.contentSize.height - tblDetail.bounds.size.height;
    }
    
    [tblDetail setContentOffset:CGPointMake(0, yOffset) animated:NO];
}
- (BOOL)connected
{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
    
}
-(IBAction)vistiServiceAction:(id)sender
{
    [singleton traceEvents:@"Open Department" withAction:@"Clicked" withLabel:@"Service Directory Detail" andValue:0];
    
    

    NSString *deptID = [dic_serviceInfo valueForKey:@"SERVICE_ID"] ;
    NSString *url = [singleton.dbManager getServiceURL:deptID];
    [dic_serviceInfo setValue:url forKey:@"SERVICE_URL"];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    HomeDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    vc.dic_serviceInfo=dic_serviceInfo ;
    vc.tagComeFrom=@"OTHERS";
    
    [self presentViewController:vc animated:NO completion:nil];
}


-(void)ratingView
{
    [singleton traceEvents:@"Open Rating View" withAction:@"Clicked" withLabel:@"Service Directory Detail" andValue:0];

    // - and max rating
    [self displayRateUsController:[self getrateCommentVc]];
    
}


-(rateCommentVC*)getrateCommentVc
{
    NSLog(@"value fo rate=%@",self.ratePass);
    NSLog(@"value fo commentPass=%@",self.commentPass);
    if ([self.ratePass length]==0) {
        self.ratePass=@"";
    }
    if ([commentPass length]==0) {
        commentPass=@"";
    }
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    rateVc = [storyboard instantiateViewControllerWithIdentifier:@"rateCommentVC"];
    rateVc.ratedBy=self.ratePass;
    rateVc.comment=commentPass;
    rateVc.dictionary=dic_serviceInfo;
    __weak __typeof(self) weakSelf = self;
    rateVc.ratingUpdate = ^(NSString *rating) {
        if ([rating isEqualToString:@"0"]){
            [weakSelf tblDataReload];
        }else {
            [weakSelf showToast:rating withDuration:2.0];
        }
    };
    
    return rateVc;
}

-(void)tblDataReload {
    [mainCell.imagesRatingControl setRating:[self.oldRating intValue]];
    [tblDetail reloadData];
}
- (void) displayRateUsController: (UIViewController*) content;
{
    [self addChildViewController:content];
    content.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:content.view];
    [content didMoveToParentViewController:self];
    [self showRateUSViewpopUp];
}

-(void)showRateUSViewpopUp{
    
    [UIView animateWithDuration:0.4 animations:^{
        rateVc.view.frame = CGRectMake(0, 0,self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
}





//----------------
-(serviceCallContactVC*)getserviceCallContacts
{
    
    NSString *contacts=[dic_serviceInfo valueForKey:@"SERVICE_PHONE_NUMBER"];
    NSArray *contactitems = [contacts componentsSeparatedByString:@","];
    NSLog(@"contactitems to be shown in pop up =%@",contactitems);
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    serviceVC = [storyboard instantiateViewControllerWithIdentifier:@"serviceCallContactVC"];
    serviceVC.tablearray=contactitems;//change it to URL on demand
    return serviceVC;
}





//----------

- (void) displayContentController: (UIViewController*) content;
{
    UIViewController *vc=[self topMostController];
    
    [vc addChildViewController:content];
    
    
    
    content.view.frame = CGRectMake(0, 0, vc.view.frame.size.width, vc.view.frame.size.height);
    
    [vc.view addSubview:content.view];
    [content didMoveToParentViewController:vc];
    
    [self showViewControllerFromLeftSide];
}

-(void)showViewControllerFromLeftSide{
    
    
    UIViewController *vc=[self topMostController];
    
    //if (itemMoreVC == nil)
    //{
    //itemMoreVC = [self getHomeDetailLayerLeftController];
    //}
    //add animation from left side
    [UIView animateWithDuration:0.4 animations:^{
        serviceVC.view.frame = CGRectMake(0, 0, vc.view.frame.size.width, vc.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
}

-(void)hideViewController{
    UIViewController *vc=[self topMostController];
    
    //if (itemMoreVC == nil) {
    serviceVC = [self getserviceCallContacts];
    //}
    //add animation from left side
    [UIView animateWithDuration:0.4 animations:^{
        serviceVC.view.frame = CGRectMake(vc.view.frame.size.width, 0, vc.view.frame.size.width, vc.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
    
}

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - ===  Table View data Source and Delegate ====

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0 ) {
        return 0;
    }
    return 0.0;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 1 ) {
        return -1;
    }
    return 0;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   
    CGFloat heightRow = 0.0;
    NSString *strSection = [arrSections objectAtIndex:indexPath.section];
    if ([strSection isEqualToString:@"CellLinksWebsite"])
    {
        if (indexPath.section == 0) {
            return 90;
        }
        return 70;
    }
    else if ([strSection isEqualToString:@"DescriptionCell"])
    {
        heightRow = descCellHeight;
    }
    else if ([strSection isEqualToString:@"ContactDetailServiceCell"])
    {
        heightRow =   90 ;

    }else if ([strSection isEqualToString:@"WorkingHoursCell"])
    {
        
    
        NSString *addressData = [dic_serviceInfo valueForKey:@"SERVICE_ADDRESS"];
        CGRect dynamicHeight = [self rectForText:addressData usingFont:[AppFont lightFont:15] boundedBySize:CGSizeMake(self.view.frame.size.width-40, 1000.0)];
        CGFloat textHeight = MAX(25, dynamicHeight.size.height);
        NSString *workingText=[dic_serviceInfo valueForKey:@"SERVICE_WORKING_HOURS"];
        CGRect dynamicHeightHours = [self rectForText:workingText usingFont:[AppFont lightFont:15] boundedBySize:CGSizeMake(self.view.frame.size.width-40, 1000.0)];
        CGFloat textHours = MAX(25, dynamicHeightHours.size.height);
        
        heightRow = textHeight + 120 + textHours;
    }
    else if ([strSection isEqualToString:@"cellLinks"])
    {
        heightRow = 90;
    }
    else if ([strSection isEqualToString:@"ServicesInfoCell"])
    {
        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];

        if (arrServicesInfo.count == 0) {
            if (isWebCall && appDelegate.networkStatus ) {
                return 60;
            }else if (appDelegate.networkStatus == false ){
                return 180;
            }
        }
        NSDictionary *dicService = [arrServicesInfo objectAtIndex:indexPath.row];
        NSString *addressData = [dicService valueForKey:@"desc"];
        NSString *name = [dicService valueForKey:@"name"];
        CGRect dynamicHeight = [self rectForText:addressData usingFont:[AppFont lightFont:14] boundedBySize:CGSizeMake(self.view.frame.size.width-40, 1000.0)];
        CGFloat textHeight = MAX(25, dynamicHeight.size.height);
        CGRect heightName = [self rectForText:name usingFont:[AppFont regularFont:16.0] boundedBySize:CGSizeMake(self.view.frame.size.width-115, 1000.0)];
        CGFloat textNameH = MAX(34, heightName.size.height);
        textNameH = textNameH > 34.0 ? textNameH + 5 : textNameH;
        heightRow = textHeight + 75 + textNameH;
        if (indexPath.row == 0) {
            heightRow = textHeight + 75 + textNameH + 30;
        }
        NSString *strServiceUL =   [dicService valueForKey:@"url"];
        if ([_isFrom isEqualToString:@"not_on_umang"] || strServiceUL == nil || strServiceUL.length == 0) {
            heightRow = heightRow - 30 ;
        }
    }
    
    
    return heightRow;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;              // Default is 1 if not implemented
{
//    NSInteger serSection = arrServicesInfo.count == 0 ? 0 : 1;
//    NSString *strNative = [dic_serviceInfo valueForKey:@"SERVICE_NATIVE_APP"];
//    if (strNative == nil || strNative.length == 0)
//    {
//        return 3 + serSection;
//    }
    return arrSections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    NSString *strSection = [arrSections objectAtIndex:section];
    if ([strSection isEqualToString:@"ServicesInfoCell"]) {
        if (arrServicesInfo.count == 0) {
            return 1;
        }
        return arrServicesInfo.count;
    }
    if ([strSection isEqualToString:@"CellLinksWebsite"]) {
        return arrLinks.count;
    }
//    if (section == 0 ) {
//        return 1;
//    }
//    if (arrServicesInfo.count != 0) {
//      NSString *strNative = [dic_serviceInfo valueForKey:@"SERVICE_NATIVE_APP"];
//        NSInteger sec = 3 ;
//        if (strNative == nil || strNative.length == 0)
//        {
//            sec = 2;
//        }
//      if (section == sec ) {
//            return arrServicesInfo.count;
//       }
//    }
    return 1;
}
-(void)tableViewHeaderParalxView {
    self.automaticallyAdjustsScrollViewInsets = NO;
    //    UIEdgeInsets contentInset = tblDetail.contentInset;
    //    contentInset.top = 100;
    //    tblDetail.contentInset = contentInset;
    CGSize headerSize = CGSizeMake(tblDetail.frame.size.width, 140); // 200 will be the default height
    // tblDetail.contentSize = CGSizeMake(headerSize.width, self.view.frame.size.height + 450);
    
    serviceHeader.frame = CGRectMake(0, 0, headerSize.width, headerSize.height);// this is completely optional
    
    serviceHeader.serviceName.titleLabel.font = [AppFont regularFont:18];
    serviceHeader.lblServiceRating.font = [AppFont lightFont:14];
    serviceHeader.lblCat_Service.font = [AppFont lightFont:14];
    serviceHeader.stateName.font = [AppFont lightFont:14];
    //---- Show service Image-----
    NSString *strUrl = [dic_serviceInfo objectForKey:@"SERVICE_IMAGE"];
    NSString *strServiceName = [dic_serviceInfo valueForKey:@"SERVICE_NAME"];
    UILabel *lblLogo =(UILabel*)[serviceHeader.logoSuperView viewWithTag:8899];
    lblLogo.hidden = YES;
    if (strUrl != nil && strUrl.length != 0) {
        serviceHeader.serviceImageLogo.hidden = NO;
        NSURL *url=[NSURL URLWithString:strUrl];
        [serviceHeader.serviceImageLogo sd_setImageWithURL:url
                                          placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
    }else {
        lblLogo.hidden = NO;
        serviceHeader.serviceImageLogo.hidden = YES;
        unichar name = [strServiceName characterAtIndex:0];
        NSCharacterSet *numericSet = [NSCharacterSet decimalDigitCharacterSet];
        if ([numericSet characterIsMember:name]) {
            name = [@"#" characterAtIndex:0];
        }
       lblLogo.font = [AppFont regularFont:16.0];
        lblLogo.textColor = [UIColor blackColor];
       lblLogo.layer.cornerRadius = lblLogo.frame.size.width /2.0;
       lblLogo.layer.masksToBounds = YES;
       lblLogo.backgroundColor = [UIColor clearColor];
        lblLogo.text =[NSString stringWithCharacters:&name length:1];
    }
    [serviceHeader.serviceName setTitle:strServiceName forState:UIControlStateNormal];
    serviceHeader.serviceName.titleLabel.numberOfLines = 20;
    serviceHeader.serviceName.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self setCat_StateNameToParalaxView:serviceHeader];
    
    serviceHeader.maximumContentHeight = 160;
    serviceHeader.minimumContentHeight = 0.0;
    serviceHeader.contentExpands = YES;
    serviceHeader.contentShrinks = YES;
    serviceHeader.contentAnchor = GSKStretchyHeaderViewContentAnchorTop;
    serviceHeader.logoSuperView.layer.cornerRadius = serviceHeader.logoSuperView.frame.size.width / 2.0;
    serviceHeader.logoSuperView.layer.masksToBounds = true ;
    serviceHeader.logoSuperView.clipsToBounds = true ;
    serviceHeader.serviceName.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    //    serviceHeader.lblCat_Service.textAlignment = NSTextAlignmentCenter;
    //    serviceHeader.lblServiceRating.textAlignment = NSTextAlignmentLeft;
    if (singleton.isArabicSelected) {
        [serviceHeader updateViewServiceDirecoryForRightAlignLanguage];
        serviceHeader.serviceName.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    }
    [tblDetail addSubview:serviceHeader] ;
}
-(void)setCat_StateNameToParalaxView:(DetailParalaxHeaderView*)view
{
    NSString *category = [dic_serviceInfo valueForKey:@"CATEGORY_NAME"];
    NSMutableArray *arrCat = [[NSMutableArray alloc] init];
    /*NSArray *arr = [category componentsSeparatedByString:@","];
     for (NSString *cat in arr) {
     [arrCat addObject:cat];
     }*/
    [arrCat addObject:category];
    //NSString *stateId = [[arr_results  objectAtIndex:indexPath.row] valueForKey:@"STATE"];
    NSString *stateId = [dic_serviceInfo valueForKey:@"STATE_ID"];
    
    //StateList *obj = [[StateList alloc] init];

    NSString *stateName = [singleton.dbManager getStateEnglishName:stateId]; //[obj getStateName:stateId];
    
    if (stateId.length == 0 || [stateId isEqualToString:@"99"])
    {
        stateName =  @"Central";//NSLocalizedString(@"Central", @"");
    }
    NSMutableArray *arrState = [[NSMutableArray alloc] initWithObjects:stateName, nil];
    NSMutableArray *otherStatesNames = [[NSMutableArray alloc] init] ;
    if (stateId.length != 0 && ![stateId isEqualToString:@"99"])
    {
        NSString *otherStateId = [dic_serviceInfo valueForKey:@"OTHER_STATE"];
        if (otherStateId.length != 0) {
            NSString *newString = [otherStateId substringToIndex:[otherStateId length]-1];
            NSString *finalString = [newString substringFromIndex:1];
            if (finalString.length != 0)
            {
                NSArray *otherStates = [finalString componentsSeparatedByString:@"|"];
                NSLog(@"%@",otherStates);
                for (NSString *stateIDS in otherStates)
                {
                    //[otherStatesNames addObject: [obj getStateName:stateIDS]];
                    [otherStatesNames addObject:[singleton.dbManager getStateEnglishName:stateIDS]];
                }
            }
        }
    }
    if ([otherStatesNames containsObject:stateName]) {
        [otherStatesNames removeObject:stateName];
    }
    if (otherStatesNames.count != 0) {
        [arrState addObjectsFromArray:otherStatesNames];
    }
    
    CGFloat fontSize = 12;
    
    if ([self.isFrom isEqualToString:@"from_umang"])
    {
        [view addCategoryStateItemsToScrollViewServiceDetails:arrCat state:arrState withFont:fontSize cell:view];
    }
    
}
-(void)setBorderColorToView:(UIView*)vw{
    vw.layer.borderColor = [UIColor colorWithRed:220.0/255.0 green:219.0/255.0 blue:218.0/255.0 alpha:1].CGColor;//colorWithHexString:@"#DFDDDE"].CGColor;
    vw.layer.borderWidth = 0.5;
    vw.layer.masksToBounds = true;
    vw.layer.shadowColor = [UIColor colorWithRed:220.0/255.0 green:219.0/255.0 blue:218.0/255.0 alpha:1].CGColor;
    vw.layer.shadowOffset = CGSizeMake(0, 2);
    vw.layer.shadowOpacity = 1.0;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strSection = [arrSections objectAtIndex:indexPath.section];
    // ---- Description Cell  -----
    if ([strSection isEqualToString:@"DescriptionCell"] )
    {
        static NSString *cellIdentifier = @"DescriptionCell";
        DescriptionCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[DescriptionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            
        }
        NSString *descriptionTxt=[dic_serviceInfo valueForKey:@"SERVICE_DESC"];
        cell.lbl_desc.text = descriptionTxt;
        cell.lbl_desc.textColor = [UIColor colorWithHexString:@"#787878"];
        cell.lbl_desc.font = [AppFont lightFont:15.0];
        CGRect dynamicHeight = [self rectForText:descriptionTxt usingFont:[AppFont regularFont:16.0] boundedBySize:CGSizeMake(self.view.frame.size.width-40, 1000.0)];
        
        CGFloat textHeight = MAX(70, dynamicHeight.size.height);
        cell.btnNeedMore.hidden = NO;
        cell.btnNeedMore.titleLabel.font = [AppFont mediumFont:15.0];
        if (textHeight <= 70 ) {
            cell.btnNeedMore.hidden = YES;
            CGRect frameTxt = cell.lbl_desc.frame;
            frameTxt.size.width = fDeviceWidth - (8*3);
            frameTxt.size.height = descCellHeight - 20;
            cell.lbl_desc.translatesAutoresizingMaskIntoConstraints = true ;
            cell.lbl_desc.frame = frameTxt;
        }
        if (descCellHeight <= 100 && cell.btnNeedMore.hidden == false) {
            [cell.btnNeedMore  setTitle:@"..." forState:UIControlStateNormal];
            [cell.btnNeedMore.titleLabel setFont:[UIFont systemFontOfSize:14 + 20]];
            
        }else
        {
            cell.btnNeedMore.hidden = YES;
            CGRect frameTxt = cell.lbl_desc.frame;
            frameTxt.size.width = fDeviceWidth - (8*3);
            frameTxt.size.height = descCellHeight - 20;
            cell.lbl_desc.translatesAutoresizingMaskIntoConstraints = true ;
            cell.lbl_desc.frame = frameTxt;
        }
        [cell.btnNeedMore addTarget:self action:@selector(didTapShowMoreDescriptionBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self setBorderColorToView:[cell viewWithTag:999]];
        cell.btnNeedMore.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        
        return  cell;
        
    }
    // ---- Contact  Cell  -----
    else if ([strSection isEqualToString:@"ContactDetailServiceCell"] )
    {
        static NSString *CellIdentifier = @"ContactDetailServiceCell";
        contactCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        NSString *phoneNumber = [dic_serviceInfo valueForKey:@"SERVICE_PHONE_NUMBER"];
        NSMutableArray *arrContact = [[NSMutableArray alloc] init];
        NSString *kCall = @"callIcon_Sevice";
        NSString *kEmail = @"emailSevice";
        NSString *kMap = @"mapIcon_Service";
        NSString *kWebsite = @"website_Service";
        NSString *kContactType = @"kContactType";
        NSString *kImageName = @"kImageName";
        NSString *kAlpha = @"kalpha";
        NSMutableDictionary *dicPhone = [[NSMutableDictionary alloc]init];
        [dicPhone setValue:@"Call" /*NSLocalizedString(@"call", nil)*/ forKey:kContactType];
        [dicPhone setValue:kCall forKey:kImageName];
        [dicPhone setValue:@"0.5" forKey:kAlpha];
        if (phoneNumber.length > 0) {
            [dicPhone setValue:@"1" forKey:kAlpha];
        }
        [arrContact addObject:dicPhone];
        
        NSString *emailID = [dic_serviceInfo valueForKey:@"SERVICE_EMAIL"];
        NSMutableDictionary *dicEmail = [[NSMutableDictionary alloc]init];
        [dicEmail setValue:@"Email" /*NSLocalizedString(@"profile_email_hint", nil)*/forKey:kContactType];
        [dicEmail setValue:kEmail forKey:kImageName];
        [dicEmail setValue:@"0.5" forKey:kAlpha];
        if (emailID.length > 0) {
            [dicEmail setValue:@"1" forKey:kAlpha];
        }
        [arrContact addObject:dicEmail];
        
        NSString *latitude= [dic_serviceInfo valueForKey:@"SERVICE_LATITUDE"];
        NSString *longitute=  [dic_serviceInfo valueForKey:@"SERVICE_LONGITUDE"];
        NSMutableDictionary *dicMap = [[NSMutableDictionary alloc]init];
        [dicMap setValue:@"View on Map"/*NSLocalizedString(@"view_on_map", nil)*/ forKey:kContactType];
        [dicMap setValue:kMap forKey:kImageName];
        [dicMap setValue:@"0.5" forKey:kAlpha];
        if (latitude.length > 0 && longitute.length > 0) {
            [dicMap setValue:@"1" forKey:kAlpha];
        }
        [arrContact addObject:dicMap];
        
        NSString *webSiteLink = [dic_serviceInfo valueForKey:@"SERVICE_WEBSITE"];
        
        NSMutableDictionary *dicWeb = [[NSMutableDictionary alloc]init];
        [dicWeb setValue:@"Visit Website"/*NSLocalizedString(@"visit_website", nil)*/ forKey:kContactType];
        [dicWeb setValue:kWebsite forKey:kImageName];
        [dicWeb setValue:@"0.5" forKey:kAlpha];
        if (webSiteLink.length > 0 ) {
            [dicWeb setValue:@"1" forKey:kAlpha];
        }
        [arrContact addObject:dicWeb];
        
        [contactCell setupDataSource:arrContact ];
        
        __weak __typeof(self) weakSelf = self;
        contactCell.ContactServiceBlock = ^(NSString *contactType) {
            if ([contactType isEqualToString:kCall]) {
                [weakSelf callAction];
            }
            else if ([contactType isEqualToString:kEmail]) {
                [weakSelf emailAction];
            }
            else if ([contactType isEqualToString:kMap]) {
                [weakSelf viewOnMap];
            }
            else if ([contactType isEqualToString:kWebsite]) {
                [weakSelf visitAction];
            }
            
        };
        [self setBorderColorToView:[contactCell viewWithTag:999]];
        
        return contactCell;
        
    }
    // ---- Rating Main  Cell  -----
    /*
     if (indexPath.section == 2) {
     static NSString *CellIdentifier = @"MainDetailCell";
     mainCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
     
     [mainCell.btnViewDetails addTarget:self action:@selector(btnViewDetailsClicked:) forControlEvents:UIControlEventTouchUpInside];
     UIImage *dot, *star;
     dot = [UIImage imageNamed:@"star_border"];
     star = [UIImage imageNamed:@"star_filled"];
     
     __weak typeof(self) weakSelf = self;
     // mainCell.imagesRatingControl.removeFromSuperview;
     if (mainCell.imagesRatingControl == nil ){
     
     CGRect ratinFrame = CGRectMake(0,0,(5 * kStarWidthAndHeight),kStarWidthAndHeight);
     ratinFrame.origin.x = ((fDeviceWidth  / 2) - kStarWidthAndHeight) - (ratinFrame.size.width / 2) ;
     // mainCell.imagesRatingControl.frame = ratinFrame;
     mainCell.imagesRatingControl = [[AMRatingControl alloc] initWithLocation:ratinFrame.origin emptyImage:dot solidImage:star andMaxRating:5];
     mainCell.imagesRatingControl.editingDidEndBlock = ^(NSUInteger rating)
     {
     NSLog(@"editingDidEndBlock %@",[NSString stringWithFormat:@"%lu_star", (unsigned long)rating]);
     weakSelf.ratePass=[NSString stringWithFormat:@"%lu",(unsigned long)rating];
     // [weakSelf hitRatingserviceAPI:ratingStar];
     [weakSelf ratingView];
     };
     //
     // Add the control(s) as a subview of your view
     [mainCell.ratingView addSubview:mainCell.imagesRatingControl];
     }
     [mainCell.imagesRatingControl setRating:[self.oldRating intValue]];
     
     mainCell.contentView.backgroundColor=[UIColor clearColor];
     mainCell.lbl_titlerateus.text = NSLocalizedString(@"rate_service_on_umang", nil);
     
     for(UIView *subview in [mainCell.contentView subviews])
     {
     if(subview.tag==9912)
     {
     [subview removeFromSuperview];
     }
     }
     
     
     UIView *vwRate=[self addRatingGraphView];
     vwRate.tag=9912;
     mainCell.btnViewDetails.backgroundColor = [UIColor clearColor];
     if (isRatingCellShown)
     {
     [mainCell.contentView addSubview:vwRate];
     [mainCell.btnViewDetails  setTitle:NSLocalizedString(@"hide_ratings", nil) forState:UIControlStateNormal];
     
     }
     else
     {
     [vwRate removeFromSuperview];
     [mainCell.btnViewDetails  setTitle:NSLocalizedString(@"view_ratings", nil) forState:UIControlStateNormal];
     
     }
     [mainCell bringSubviewToFront:mainCell.btnViewDetails];
     
     [self setBorderColorToView:[mainCell viewWithTag:999]];
     
     return mainCell;
     
     }
     */
    // ---- Working Hours Cell  -----
    
    else if ([strSection isEqualToString:@"WorkingHoursCell"])
    {
        static NSString *cellIdentifier = @"WorkingHoursCell";
        WorkingHoursCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[WorkingHoursCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            
        }
        
        NSString *workingText=[dic_serviceInfo valueForKey:@"SERVICE_WORKING_HOURS"];
        cell.lbl_workinghours.text = workingText;
        cell.lbl_workinghours.font = [AppFont lightFont:15];
        
        NSString *addressText=[dic_serviceInfo valueForKey:@"SERVICE_ADDRESS"];
        [cell.btn_workinghours setTitle:@"Working Hours"/*NSLocalizedString(@"working_hours", nil)*/ forState:UIControlStateNormal];
        cell.btn_workinghours.titleLabel.font = [AppFont mediumFont:17];
        
        cell.lblAddress.text = @"Address";//NSLocalizedString(@"address", nil);
        cell.lblAddress.font = [AppFont mediumFont:17];
        
        cell.addressOfService.text = addressText;
        cell.addressOfService.font = [AppFont lightFont:15];
        
        
        //NSArray *arrDays = [workingText componentsSeparatedByString:@" "];
        
        //   [cell updateWeekDayWorkingStatus:@"" lastDay:@""];
        [self setBorderColorToView:[cell viewWithTag:999]];
        
        return  cell;
        
    }
    // Native App Links Cell
    else if ([strSection isEqualToString:@"cellLinks"] )
    {
        static NSString *indetifierLink = @"cellLinks";
        UITableViewCell *cellLinks = [tableView dequeueReusableCellWithIdentifier:indetifierLink forIndexPath:indexPath];
        UIView *viewSub = [cellLinks viewWithTag:999];
        [self setBorderColorToView:viewSub];
        UIButton *btnNativeApp = (UIButton*)[viewSub viewWithTag:2325];
        if (btnNativeApp != nil) {
            [btnNativeApp addTarget:self action:@selector(didTapNativeAppBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            btnNativeApp.titleLabel.numberOfLines = 5.0;
            btnNativeApp.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
            [btnNativeApp setTitle:[dic_serviceInfo valueForKey:@"SERVICE_NAME"] forState:UIControlStateNormal];
            
        }
        UILabel *lblLinks = (UILabel*)[cellLinks viewWithTag:2323];
        
        if (singleton.isArabicSelected) {
            UIImageView *phone = (UIImageView*)[cellLinks viewWithTag:2324];
            UIImageView *appStore = (UIImageView*)[cellLinks viewWithTag:2326];
            CGRect subFrme = viewSub.frame;
            CGRect frameLink = lblLinks.frame;
            cellLinks.translatesAutoresizingMaskIntoConstraints = true ;
            lblLinks.textAlignment = NSTextAlignmentRight;
            btnNativeApp.frame = CGRectMake(119,CGRectGetMaxY(frameLink) + 8 ,CGRectGetWidth(subFrme) - (119 + 46 + 16)  , 41);
            phone.frame = CGRectMake(CGRectGetWidth(subFrme) - 46, CGRectGetMidY(btnNativeApp.frame) - 17 , 34, 34);
            appStore.frame = CGRectMake(8, CGRectGetMidY(btnNativeApp.frame) - 16.5 , 107, 33);
            btnNativeApp.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            [cellLinks layoutIfNeeded];
            [cellLinks updateConstraintsIfNeeded];
        }
        return  cellLinks;
        
    }
    // Website Info  Links Cell

    else if ([strSection isEqualToString:@"CellLinksWebsite"])
    {
            NSString *cellIdentifier = singleton.isArabicSelected ?  @"cellLinksWebsite_Arabic" : @"cellLinksWebsite";
            CellLinksWebsite *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                cell = [[CellLinksWebsite alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            }
            UILabel *lblLinks = (UILabel*)[cell viewWithTag:2323];
            lblLinks.font = [AppFont mediumFont:17.0];
            lblLinks.text = NSLocalizedString(@"links", nil);
            if (indexPath.row != 0) {
                lblLinks.hidden = true;
                cell.topLinksLabelConstraint.constant = 10;
            }
            UILabel *links = (UILabel*)[cell viewWithTag:2424];
            links.numberOfLines = 3;
            links.lineBreakMode = NSLineBreakByWordWrapping;
            links.attributedText = [self setLinksAttributed:arrLinks[indexPath.row]];
            UIButton *btnOpen = (UIButton*)[cell viewWithTag:2525];
            btnOpen.tag = 2525 + indexPath.row;
           [btnOpen.titleLabel setFont:[AppFont regularFont:17]];
           [btnOpen setTitle:NSLocalizedString(@"open_text", nil) forState:UIControlStateNormal];
            [btnOpen addTarget:self action:@selector(didTapOpenWebsiteLinks:) forControlEvents:UIControlEventTouchUpInside];
            [self setBorderColorToView:[cell viewWithTag:999]];
            [cell layoutIfNeeded];
            [cell updateConstraintsIfNeeded];
            return  cell;
            
        
    }
    // Service Info  Links Cell

    else if ([strSection isEqualToString:@"ServicesInfoCell"]) {
        NSString *indetifierLink = singleton.isArabicSelected ? @"ServicesInfoCell_Arabic" : @"ServicesInfoCell";
        ServicesInfoCell *cellService = [tableView dequeueReusableCellWithIdentifier:indetifierLink forIndexPath:indexPath];
        cellService.heightServiceLabel.constant = 20.0;
        cellService.topConstraintContentView.constant = 5.0;
        cellService.topServiceNameContraint.constant = 39.0;
        if (indexPath.row != 0) {
            cellService.lblServices.hidden = true;
            cellService.topConstraintContentView.constant = 1.5;
            cellService.heightServiceLabel.constant = 0.0;
            cellService.topServiceNameContraint.constant = 13.0;
        }
        [cellService layoutIfNeeded];
        [cellService updateConstraintsIfNeeded];
        servicesCell = cellService;
        [cellService viewWithTag:888].hidden = YES;

        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
        [cellService viewWithTag:999].hidden = NO;
        [cellService.loaderIndicator stopAnimating];
        cellService.lblServices.text = @"Services";//[NSLocalizedString(@"services", nil) capitalizedString];
        cellService.lblServices.font = [AppFont mediumFont:17.0];
        cellService.lblServices.hidden = false;
        if (arrServicesInfo.count == 0) {
            if (isWebCall && appDelegate.networkStatus) {
                [cellService.loaderIndicator setHidden:NO];
                [cellService.loaderIndicator startAnimating];
                [cellService viewWithTag:999].hidden = YES;
                servicesCell = cellService;
                return cellService;
            }
            else if (appDelegate.networkStatus == false) {
                [cellService.loaderIndicator stopAnimating];
                [cellService viewWithTag:888].hidden = NO;
                [cellService bringSubviewToFront:[cellService viewWithTag:888]];
                [cellService.btnClickHereRetry setTitle:@"Click here to retry"/*NSLocalizedString(@"click_here_to_retry", nil)*/ forState:UIControlStateNormal];
                cellService.btnClickHereRetry.titleLabel.font = [AppFont lightFont:15.0];
                [ cellService.btnClickHereRetry addTarget:self action:@selector(didTapClickHereToRetryServicesAction:) forControlEvents:UIControlEventTouchUpInside];
                cellService.lblCheckInternet.text = @"Please check your Internet Connection and try again";//NSLocalizedString(@"please_check_network_and_try_again", nil);// @"Please check for internet connection"; click_here_to_retry
                servicesCell = cellService;
                cellService.lblCheckInternet.textAlignment = NSTextAlignmentCenter;
                return cellService;
            }
        }
    
        NSDictionary *dicService = [arrServicesInfo objectAtIndex:indexPath.row];
      
     
        NSString *strServiceName = [dicService valueForKey:@"name"];
        CGRect heightName = [self rectForText:strServiceName usingFont:[AppFont regularFont:16.0] boundedBySize:CGSizeMake(self.view.frame.size.width-115, 1000.0)];
        CGFloat textHeight = MAX(34, heightName.size.height);
        textHeight = textHeight > 34.0 ? textHeight + 5 : textHeight;
        cellService.heightServiceNameConstraint.constant = textHeight;
        [cellService.serviceName setTitle:strServiceName forState:UIControlStateNormal];
        cellService.serviceName.titleLabel.font = [AppFont regularFont:16.0];
        cellService.serviceName.titleLabel.numberOfLines = 3.0;
        cellService.serviceName.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cellService.ser_Desc.textColor = [UIColor colorWithHexString:@"#787878"];
        cellService.ser_Desc.font = [AppFont lightFont:15.0];
        cellService.ser_Desc.text = [dicService valueForKey:@"desc"];
        NSString *strImageUrl = [dicService valueForKey:@"img"];
        cellService.ser_Image.backgroundColor = [UIColor clearColor];
        if (strImageUrl != nil && strImageUrl.length != 0 ) {
            cellService.ser_Image.hidden = YES;
            cellService.serviceImageView.hidden = NO;

            NSURL *imageUrl = [NSURL URLWithString:strImageUrl];
           
            [cellService.ser_Image.imageView sd_setImageWithURL:imageUrl completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [RunOnMainThread runBlockInMainQueueIfNecessary:^{
                    cellService.serviceImageView.image = image;
                   // [cellService.ser_Image setImage:image forState:UIControlStateNormal];
                    if (image == nil ) {
                        cellService.ser_Image.hidden = NO;
                        cellService.serviceImageView.hidden = YES;
                        unichar name = [strServiceName characterAtIndex:0];
                        NSCharacterSet *numericSet = [NSCharacterSet decimalDigitCharacterSet];
                        if ([numericSet characterIsMember:name]) {
                            name = [@"#" characterAtIndex:0];
                        }
                        cellService.ser_Image.titleLabel.font = [AppFont regularFont:16.0];
                        cellService.ser_Image.titleLabel.textColor = [UIColor whiteColor];
                        cellService.ser_Image.layer.cornerRadius = cellService.ser_Image.frame.size.width /2.0;
                        cellService.ser_Image.layer.masksToBounds = YES;
                        cellService.ser_Image.backgroundColor = serviceHeader.backgroundColor;
                        [cellService.ser_Image setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        [cellService.ser_Image setTitle:[[NSString stringWithCharacters:&name length:1] uppercaseString] forState:UIControlStateNormal];
                    }
                }];
//
               }];
         }
        else {
            cellService.ser_Image.hidden = NO;
            cellService.serviceImageView.hidden = YES;
            unichar name = [strServiceName characterAtIndex:0];
            NSCharacterSet *numericSet = [NSCharacterSet decimalDigitCharacterSet];
            if ([numericSet characterIsMember:name]) {
                name = [@"#" characterAtIndex:0];
            }
            cellService.ser_Image.titleLabel.font = [AppFont regularFont:16.0];
            cellService.ser_Image.titleLabel.textColor = [UIColor whiteColor];
            cellService.ser_Image.layer.cornerRadius = cellService.ser_Image.frame.size.width /2.0;
            cellService.ser_Image.layer.masksToBounds = YES;
            cellService.ser_Image.backgroundColor = serviceHeader.backgroundColor;
            [cellService.ser_Image setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [cellService.ser_Image setTitle:[NSString stringWithCharacters:&name length:1] forState:UIControlStateNormal];

        }
        //cellService.btnSer_Rating.hidden = YES;
        NSString *strRating = [dicService valueForKey:@"rating"];
        if (strRating == nil || strRating.length == 0 ) {
            strRating = @"0.0";
        }
        [cellService.btnSer_Rating setTitle:strRating forState:UIControlStateNormal];
        cellService.btnSer_Rating.titleLabel.font = [AppFont regularFont:16.0];
        NSString *strServiceUL =   [dicService valueForKey:@"url"];
        if ([_isFrom isEqualToString:@"from_umang"] && strServiceUL != nil && strServiceUL.length != 0) {
            cellService.btnOpenService.hidden = false ;
            cellService.heightOpenServiceConstraint.constant = 30.0;

          [cellService.btnOpenService setTitle:@"OPEN"/*NSLocalizedString(@"open_service", nil)*/ forState:UIControlStateNormal];
            cellService.btnOpenService.titleLabel.font = [AppFont mediumFont:13.0];
            cellService.btnOpenService.tag = indexPath.row;
            cellService.btnOpenService.layer.borderColor = [[UIColor colorWithRed:0.0/255.0f green:89.0/255.0f blue:157.0/255.0f alpha:1.0f] CGColor];
            
            cellService.btnOpenService.layer.borderWidth = 1.0;
            cellService.btnOpenService.layer.cornerRadius = 3.0;
            cellService.btnOpenService.clipsToBounds = YES;
            
            cellService.btnOpenService.backgroundColor = [UIColor clearColor];
            [ cellService.btnOpenService addTarget:self action:@selector(didTapOpenServcesAction:) forControlEvents:UIControlEventTouchUpInside];
        }else {
            cellService.btnOpenService.hidden = true ;
            cellService.heightOpenServiceConstraint.constant = 0.0;
           
        }
       if (singleton.isArabicSelected) {
              cellService.ser_Desc.textAlignment = NSTextAlignmentRight;
              cellService.lblServices.textAlignment = NSTextAlignmentRight;
          cellService.serviceName.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
           }
        [cellService layoutIfNeeded];
        [cellService updateConstraintsIfNeeded];
        return cellService;
    }
    // ---- default Cell  -----
    else  {
        static NSString *cellIdentifier = @"addressCell";
        DetailCustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[DetailCustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            [cell designInterfaceForFrame:tableView.frame withFontSizeApplied:fontApplied withMapOptionRequired:YES];
        }
        // [cell.btnViewOnMap addTarget:self action:@selector(viewOnMap) forControlEvents:UIControlEventTouchUpInside];
        //  cell.btnViewOnMap.titleLabel.textColor = [UIColor colorWithRed:9.0/255.0 green:59.0/255.0 blue:135.0/255.0 alpha:1.0];
        
        return  cell;
    }
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strSection = [arrSections objectAtIndex:indexPath.section];

    if ([strSection isEqualToString:@"cellLinks"])
    {
        [singleton traceEvents:@"Open Native App" withAction:@"Clicked" withLabel:@"Service Directory Detail" andValue:0];

        NSString *strAppID = [dic_serviceInfo valueForKey:@"SERVICE_NATIVE_APP"]; //@"1236448857";
        
        NSString *applink = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@",strAppID];
        
        
        
        //NSString *applink=[NSString stringWithFormat:@"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8",strAppID];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString: applink]]) {
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString: applink]];
        }
    }
    
}
-(NSMutableAttributedString*)setLinksAttributed:(NSString*)strLinks {
    NSArray *arrLinks = [strLinks componentsSeparatedByString:@"#"];
    NSMutableAttributedString *strFinal = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ \n",arrLinks.firstObject] attributes:@{NSFontAttributeName: [AppFont lightFont:15]}];
    NSAttributedString *strLink = [[NSAttributedString alloc] initWithString:arrLinks[1] attributes:@{NSFontAttributeName: [AppFont lightFont:13]}];
    [strFinal appendAttributedString:strLink];
    return strFinal;
}
-(void)visitAction
{
    [singleton traceEvents:@"Visit Website" withAction:@"Clicked" withLabel:@"Service Directory Detail" andValue:0];

    //[dic_serviceInfo valueForKey:@"SERVICE_WEBSITE"];
    NSString* website = [NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_WEBSITE"]];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:website]];
}

-(void)startWebApiCallDispatch{
    if (serviceGroup == nil )
    {
        serviceGroup = dispatch_group_create();
    }
 
    // Create the dispatch group
    // Start the first service
    dispatch_group_enter(serviceGroup);
    [self hitFetchServicesInfoDetailApi];
   // __weak typeof(self) weakSelf = self;
    dispatch_group_notify(serviceGroup,dispatch_get_main_queue(),^{
        // Assess any errors
          AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
        if (arrServicesInfo.count == 0 && appDelegate.networkStatus) {
            [arrSections removeObject:@"ServicesInfoCell"];
            //[arrSections addObject:@"ServicesInfoCell"];
         }
        [tblDetail reloadData];
         serviceGroup  = nil ;
        // Now call the final completion block
    });
    
}


#pragma mark- Web api Methods
//----- hitAPI for IVR OTP call Type registration ------
-(void)hitFetchServicesInfoDetailApi
{
    NSString *service_id=[NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_ID"]];
   isWebCall = true ;
    [tblDetail reloadData];
  //  hud = [MBProgressHUD showHUDAddedTo:servicesCell animated:YES];
    // Set the label text.
   // hud.label.text = NSLocalizedString(@"loading",nil);
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    [dictBody setObject:service_id forKey:@"sid"];//Enter mobile number of user
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"]; //tkn number
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    __weak __typeof(self) weakSelf = self;
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_SERVICES_INFO withBody:dictBody andTag:TAG_REQUEST_FETCH_USER_RATING completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        isWebCall = false ;
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            // NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                arrServicesInfo =[[NSMutableArray alloc] initWithArray:[[response valueForKey:@"pd"]valueForKey:@"services"]] ;
//                if (arrServicesInfo.count > 2) {
//                    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:[arrServicesInfo objectAtIndex:1]] ;
//                    [dic setValue:@"" forKey:@"url" ];
//                    [arrServicesInfo insertObject:dic atIndex:1];
//                }
            }
            
           
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            
            [self showToast:error.localizedDescription withDuration:2.0];
           
        }
        if (serviceGroup != nil) {
            dispatch_group_leave(serviceGroup);
        }
    }];
    
}

//----- hitAPI for IVR OTP call Type registration ------
-(void)hitFetchUserRatingAPI
{
    NSString *service_id=[NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_ID"]];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = @"Loading...";//NSLocalizedString(@"loading",nil);
    
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    [dictBody setObject:service_id forKey:@"sid"];//Enter mobile number of user
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"]; //tkn number
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    __weak __typeof(self) weakSelf = self;
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_FETCH_USER_RATING withBody:dictBody andTag:TAG_REQUEST_FETCH_USER_RATING completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            // NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                commentPass=[[response valueForKey:@"pd"]valueForKey:@"cmt"];
                if ([[[response valueForKey:@"pd"]valueForKey:@"rt1"] length]!=0) {
                    rt1=[[response valueForKey:@"pd"]valueForKey:@"rt1"];
                }
                if ([[[response valueForKey:@"pd"]valueForKey:@"rt2"] length]!=0) {
                    rt2=[[response valueForKey:@"pd"]valueForKey:@"rt2"];
                }
                
                if ([[[response valueForKey:@"pd"]valueForKey:@"rt3"] length]!=0) {
                    rt3=[[response valueForKey:@"pd"]valueForKey:@"rt3"];
                }
                
                if ([[[response valueForKey:@"pd"]valueForKey:@"rt4"] length]!=0) {
                    rt4=[[response valueForKey:@"pd"]valueForKey:@"rt4"];
                }
                if ([[[response valueForKey:@"pd"]valueForKey:@"rt5"] length]!=0) {
                    rt5=[[response valueForKey:@"pd"]valueForKey:@"rt5"];
                }
                
                
                NSString *rating=@"";
                
                if ([[[response valueForKey:@"pd"]valueForKey:@"rating"] isKindOfClass:[NSNull class]])
                {
                    rating=@"0";
                    [mainCell.imagesRatingControl setRating:[rating intValue]];
                    
                }
                
                
                else  if ([[[response valueForKey:@"pd"]valueForKey:@"rating"] length]!=0)
                {
                    
                    rating=[[response valueForKey:@"pd"]valueForKey:@"rating"];
                    
                    [mainCell.imagesRatingControl setRating:[rating intValue]];
                    
                }
                weakSelf.oldRating = rating;
               
                //[tblDetail reloadData];
                
                //[imagesRatingControl setRating:3];
                //----JUST FOR TEST
                
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            
            [self showToast:error.localizedDescription withDuration:2.0];
           
        }
        if (serviceGroup != nil) {
            dispatch_group_leave(serviceGroup);
        }
    }];
    
}


-(void)showToast :(NSString *)toast withDuration:(double)duration
{
    [self.view makeToast:toast duration:5.0 position:CSToastPositionBottom];
}



-(NSString*)getNewlineString:(NSString*)existingStr{
    NSString *mainString = existingStr;
    for (int i = 0; i<10; i++) {
        NSString *tobeReplaced = [NSString stringWithFormat:@" %i.",i];
        NSString *replacedString = [NSString stringWithFormat:@"\n%i.",i];
        mainString = [mainString stringByReplacingOccurrencesOfString:tobeReplaced withString:replacedString];
    }
    return mainString;
}

-(void)hitRatingserviceAPI:(NSString*)rating
{
    
    NSString *service_id=[NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_ID"]];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = @"Loading...";//NSLocalizedString(@"loading",nil);
    
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    [dictBody setObject:rating forKey:@"rating"];//Enter mobile number of user
    [dictBody setObject:service_id forKey:@"sid"];//Enter mobile number of user
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"]; //tkn number
    [dictBody setObject:@"" forKey:@"st"]; //tkn number
    // [dictBody setObject:@"en" forKey:@"lcle"]; //tkn number
    
    
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_RATINGS withBody:dictBody andTag:TAG_REQUEST_API_RATINGS completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            // NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                
                //[imagesRatingControl setRating:3];
                //----JUST FOR TEST
                [tblDetail reloadData];
                
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            
            /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)  message:error.localizedDescription delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];*/
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"  message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alert show];
            
        }
        
    }];
    
}



-(void)emailAction
{
    //  [dic_serviceInfo valueForKey:@"SERVICE_EMAIL"]
    
    [singleton traceEvents:@"Service Email" withAction:@"Clicked" withLabel:@"Service Directory Detail" andValue:0];

    
    NSString *emailIdString = [dic_serviceInfo valueForKey:@"SERVICE_EMAIL"];
    
    if (emailIdString.length>0)
    {
        if ([emailIdString containsString:@"|"] || [emailIdString containsString:@"#"] || [emailIdString containsString:@","])
        {
            CallBoxTVC *callVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CallBoxTVC"];
            callVC.call = emailIdString;
            callVC.comingFrom = @"email";
            [self addChildViewController:callVC];
            [self.view addSubview:callVC.view];
            [callVC didMoveToParentViewController:self];
        }
        else
        {
            MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
            if ([MFMailComposeViewController canSendMail]) {
                
                
                NSString *receipt=[dic_serviceInfo valueForKey:@"SERVICE_EMAIL"];
                // Email Subject
                NSString *emailTitle = @"Umang Services Email";
                // Email Content
                NSString *messageBody = @"Sent by Umang App!";
                // To address
                // To address
                NSArray *toRecipents = [NSArray arrayWithObject:receipt];
                
                picker.mailComposeDelegate = self;
                [picker setSubject:emailTitle];
                [picker setMessageBody:messageBody isHTML:YES];
                [picker setToRecipients:toRecipents];
                
                // Present mail view controller on screen
                [self presentViewController:picker animated:YES completion:NULL];
            }
        }
    }
    
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (void)openContactList
{
    [singleton traceEvents:@"Service Contact List" withAction:@"Clicked" withLabel:@"Service Directory Detail" andValue:0];

    // Create the sheet with only cancel button
    
    /*UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:NSLocalizedString(@"call", nil)
                                  delegate:self
                                  cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:nil];*/
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:@"Call"
                                  delegate:self
                                  cancelButtonTitle:@"Cancel"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:nil];
    
    
    // Add buttons one by one (e.g. in a loop from array etc...)
    for (int i=0; i<[self.contactitems count]; i++)
    {
        NSString *contact=[NSString stringWithFormat:@"%@",[self.contactitems objectAtIndex:i]];
        [actionSheet addButtonWithTitle:contact];
        
    }
    
    
    [actionSheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    if  (actionSheet.cancelButtonIndex == buttonIndex)
    {
        [singleton traceEvents:@"Service Contact Call Cancel" withAction:@"Clicked" withLabel:@"Service Directory Detail" andValue:0];

        return;
    }
    else
    {
        if(buttonIndex>0)
        {
            buttonIndex=buttonIndex-1;
        }
        
        [singleton traceEvents:@"Service Contact Call Dail" withAction:@"Clicked" withLabel:@"Service Directory Detail" andValue:0];

        NSString*  callNumber=[NSString stringWithFormat:@"%@",[self.contactitems objectAtIndex:buttonIndex]];
        callNumber = [callNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *mobileNumber=[NSString stringWithFormat:@"telprompt://%@",callNumber];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mobileNumber]];
    }
}



-(void)callAction
{
    
    NSString *contacts=[dic_serviceInfo valueForKey:@"SERVICE_PHONE_NUMBER"];
    
    if (contacts.length>0)
    {
//        CallBoxTVC *callVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CallBoxTVC"];
//        callVC.call = contacts;
//
//        [self addChildViewController:callVC];
//        [self.view addSubview:callVC.view];
//        [callVC didMoveToParentViewController:self];
        if ([contacts containsString:@"|"] || [contacts containsString:@"#"] || [contacts containsString:@","]) {
            UIStoryboard *story = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
            CallBoxTVC *callVC = [story instantiateViewControllerWithIdentifier:@"CallBoxTVC"];
            callVC.call = contacts;
            callVC.comingFrom = @"call";
            [self addChildViewController:callVC];
            [self.view addSubview:callVC.view];
            [callVC didMoveToParentViewController:self];
        }
        else {
            NSString *mobileNumber=[NSString stringWithFormat:@"telprompt://%@",contacts];
            
            mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mobileNumber]];
        }
        
    }
    else
    {
        //do nothing
        // contactCell.btnCall.hidden = YES;
        
        
        /*UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:NSLocalizedString(@"phone_support", nil)                         message:NSLocalizedString(@"no_result_found", nil)
                              delegate:self
                              cancelButtonTitle:nil
                              otherButtonTitles:NSLocalizedString(@"ok", nil), nil];*/
        
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Phone Support"                         message:@"No result found"
                              delegate:self
                              cancelButtonTitle:nil
                              otherButtonTitles:@"OK", nil];
        
        [alert show];
    }
}
-(void)didTapClickHereToRetryServicesAction:(UIButton*)sender
{
    [self startWebApiCallDispatch];
}

-(void)didTapOpenServcesAction:(UIButton*)sender
{
    [singleton traceEvents:@"Open Department" withAction:@"Clicked" withLabel:@"Service Directory Detail" andValue:0];

    NSDictionary *dicService = [arrServicesInfo objectAtIndex:sender.tag];
    [dic_serviceInfo setValue:[dicService valueForKey:@"url"] forKey:@"SERVICE_URL"];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HomeDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    vc.dic_serviceInfo= dic_serviceInfo;
    vc.tagComeFrom=@"OTHERS";
    
    [self presentViewController:vc animated:NO completion:nil];
}


#pragma mark : --- Show Complete Description Btn Action ----
-(void)didTapNativeAppBtnAction:(UIButton*)sender
{
    [singleton traceEvents:@"Open Service Native App" withAction:@"Clicked" withLabel:@"Service Directory Detail" andValue:0];

    NSString *strAppID = [dic_serviceInfo valueForKey:@"SERVICE_NATIVE_APP"]; //@"1236448857";
    
    NSString *applink = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@",strAppID];
    
    
    
    //NSString *applink=[NSString stringWithFormat:@"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8",strAppID];
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString: applink]]) {
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString: applink]];
    }
}
-(void)didTapShowMoreDescriptionBtnAction:(MyFavButton*)sender
{
    //sender.selected = !sender.isSelected;
    if (descCellHeight <= 100) {
        NSString *serviceData = [dic_serviceInfo valueForKey:@"SERVICE_DESC"];
        CGRect dynamicHeight = [self rectForText:serviceData usingFont:[AppFont regularFont:16.0] boundedBySize:CGSizeMake(self.view.frame.size.width-40, 1000.0)];
        CGFloat textHeight = MAX(30, dynamicHeight.size.height);
        descCellHeight = textHeight + headerTitleHeight;
    }else {
        descCellHeight = 100;
    }
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
    NSArray *arrIndex = [[NSArray alloc] initWithObjects:index, nil];
    [tblDetail reloadRowsAtIndexPaths:arrIndex withRowAnimation:UITableViewRowAnimationAutomatic];
    sender.hidden = true ;
    
}


-(void)viewOnMap
{
    
    [singleton traceEvents:@"Open View Map" withAction:@"Clicked" withLabel:@"Service Directory Detail" andValue:0];

    NSString *latitude= [dic_serviceInfo valueForKey:@"SERVICE_LATITUDE"];
    
    
    NSString *longitute=  [dic_serviceInfo valueForKey:@"SERVICE_LONGITUDE"];
    
    
    // NSString* googleMapsURLString = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@,%@",latitude, longitute];
    
    // [[UIApplication sharedApplication] openURL: [NSURL URLWithString: googleMapsURLString]];
    
    NSString *deptName=[NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_NAME"]];
    deptName = [deptName stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    
    
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://"]])
        
    {
        NSLog(@"Map App Found");
        
        NSString* googleMapsURLString = [NSString stringWithFormat:@"comgooglemaps://?q=%.6f,%.6f(%@)&center=%.6f,%.6f&zoom=15&views=traffic",[latitude doubleValue], [longitute doubleValue],deptName,[latitude doubleValue], [longitute doubleValue]];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsURLString]];
        
        
    } else
    {
        NSString* googleMapsURLString = [NSString stringWithFormat:@"https://maps.google.com/maps?&z=15&q=%.6f+%.6f&ll=%.6f+%.6f",[latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
        
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:googleMapsURLString]];
        
    }
    
    
}
//deepak change below function

-(void)btnViewDetailsClicked :(NSIndexPath*)indexPath
{
    if ( [mainCell.btnViewDetails.titleLabel.text isEqualToString:@"View Ratings"/*NSLocalizedString(@"view_ratings", nil)*/])
    {
        [mainCell.btnViewDetails setTitle:@"Hide Ratings"/*NSLocalizedString(@"hide_ratings", nil)*/ forState:UIControlStateNormal];
        
    }
    else
    {
        [mainCell.btnViewDetails setTitle:@"View Ratings"/*NSLocalizedString(@"view_ratings", nil)*/ forState:UIControlStateNormal];
        
    }
    mainCell.btnViewDetails.backgroundColor = [UIColor clearColor];
    
    
    if (isRatingShown==YES) {
        isRatingCellShown = YES;
        isRatingShown=NO;
    }
    else
    {
        isRatingCellShown = NO;
        isRatingShown=YES;
    }
    
    //[tblDetail reloadData];
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:2];
    NSArray *arrIndex = [[NSArray alloc] initWithObjects:index, nil];
    [tblDetail reloadRowsAtIndexPaths:arrIndex withRowAnimation:UITableViewRowAnimationAutomatic];
    
    
}





-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}

-(UIView *)addRatingGraphView
{
    if ([rt1 length]==0) {
        rt1=@"0";
    }
    if ([rt2 length]==0) {
        rt2=@"0";
        
    }
    if ([rt3 length]==0) {
        rt3=@"0";
        
    }
    if ([rt4 length]==0) {
        rt4=@"0";
        
    }
    if ([rt5 length]==0) {
        rt5=@"0";
        
    }
    int rate1=[rt1 intValue];
    int rate2=[rt2 intValue];
    int rate3=[rt3 intValue];
    int rate4=[rt4 intValue];
    int rate5=[rt5 intValue];
    
    NSNumber *myNum1 = [NSNumber numberWithInt:rate1];
    NSNumber *myNum2 = [NSNumber numberWithInt:rate2];
    NSNumber *myNum3 = [NSNumber numberWithInt:rate3];
    NSNumber *myNum4 = [NSNumber numberWithInt:rate4];
    NSNumber *myNum5 = [NSNumber numberWithInt:rate5];
    //NSArray *valuesTopass = @[myNum1, myNum2,myNum3, myNum4, myNum5];
    NSArray *valuesTopass = @[myNum5, myNum4,myNum3, myNum2, myNum1];
    
    //deepak change
    
    //vwRating = [[RatingGraphView alloc] initWithFrame:CGRectMake(10,framevwRatebtn.origin.y + 30, CGRectGetWidth(self.view.frame) - 20, 160) withXValues:valuesTopass];
    NSString*ratingDept= [NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_RATING"]];
    
    NSString*totalDownloadStr= [NSString stringWithFormat:@"%d",totalDownload];
    
    @try {
        totalDownload=rate1+rate2+rate3+rate4+rate5;
        
        NSLog(@"totalDownload=%d",totalDownload);
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
    // vwRating = [[RatingGraphView alloc] initWithFrame:CGRectMake(10,170 + 30, CGRectGetWidth(self.view.frame) - 20, 160) withXValues:valuesTopass withRating:ratingDept withTotalDownload:totalDownloadStr];
    
    
    vwRating = [[RatingGraphView alloc] initWithFrame:CGRectMake(10,80, CGRectGetWidth(self.view.frame) - 20, 150) withXValues:valuesTopass withRating:ratingDept withTotalDownload:totalDownloadStr];
    
    //vwRating = [[RatingGraphView alloc] initWithFrame:CGRectMake(10,170 + 30, CGRectGetWidth(self.view.frame) - 20, 160) withXValues:valuesTopass withRating:ratingDept withTotalDownload:totalDownloadStr];
    
    vwRating.tag=1099;
    //[self.view addSubview:vwRating];
    return vwRating;
}


-(IBAction)backbtnAction:(id)sender
{
    [singleton traceEvents:@"Back Button" withAction:@"Clicked" withLabel:@"Service Directory Detail" andValue:0];

    [self.navigationController popViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:NO completion:nil];
}
- (IBAction)didTapFavouriteBtnAction:(UIButton *)sender {
    //sender.selected = !sender.isSelected;
    //[sender ]
    
    NSString* serviceId =[self.dic_serviceInfo valueForKey:@"SERVICE_ID"];
    NSString* serviceIsFav = sender.isSelected ?  @"true" : @"false";
    //NSLog(@"updateServiceIsFav");
    
    if (serviceId == (NSString *)[NSNull null]||[serviceId length]==0) {
        serviceId=@"";
    }
    // UIActivityViewController
    //code to be executed in the background
    [singleton.dbManager updateServiceIsFav:serviceId serviceIsFav:serviceIsFav hitAPI:@"Yes"];
}
- (IBAction)didTapShareBtnAction:(UIButton *)sender
{
    
    [singleton traceEvents:@"Share Button" withAction:@"Clicked" withLabel:@"Service Directory Detail" andValue:0];

    NSString *textToShare = singleton.shareText;
    
    NSArray *objectsToShare = @[textToShare];
    
    [[UIPasteboard generalPasteboard] setString:textToShare];
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    //if iPhone
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        [self presentViewController:controller animated:YES completion:nil];
        
    }
    //if iPad
    
    else {
        
        // Change Rect to position Popover
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:controller];
        
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    }
    
}
-(void)didTapOpenWebsiteLinks:(UIButton*)sender
{
    [singleton traceEvents:@"Open Website" withAction:@"Clicked" withLabel:@"Service Directory Detail" andValue:0];

    NSString *strLink = arrLinks[(sender.tag - 2525)];
    NSArray *arrLinks = [strLink componentsSeparatedByString:@"#"];
    NSURL *url = [[NSURL alloc] initWithString:arrLinks[1]];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}


#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
