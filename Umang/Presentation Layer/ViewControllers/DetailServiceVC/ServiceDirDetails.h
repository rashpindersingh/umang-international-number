//
//  DetailServiceNewVC.h
//  Umang
//
//  Created by admin on 17/07/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ServiceDirDetails : UIViewController
@property(nonatomic,retain)NSDictionary *dic_serviceInfo;
@property(nonatomic,retain)NSString *isFrom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightBtnVisitServiceConstraint;

@end

@interface ServicesInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightServiceLabel;
@property (weak, nonatomic) IBOutlet UIButton *ser_Image;
@property (weak, nonatomic) IBOutlet UIButton *btnSer_Rating;
@property (weak, nonatomic) IBOutlet UILabel *lblServices;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraintContentView;
@property (weak, nonatomic) IBOutlet UILabel *lblCheckInternet;
@property (weak, nonatomic) IBOutlet UIButton *btnClickHereRetry;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightOpenServiceConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightServiceNameConstraint;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loaderIndicator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topServiceNameContraint;

@property (weak, nonatomic) IBOutlet UIButton *serviceName;
@property (weak, nonatomic) IBOutlet UILabel *ser_Desc;
@property (weak, nonatomic) IBOutlet UIButton *btnOpenService;
@property (strong, nonatomic) IBOutlet UIImageView *serviceImageView;



@end
