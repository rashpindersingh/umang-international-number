//
//  ShowUserProfileVC.h
//  Umang
//
//  Created by admin on 30/10/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowUserProfileVC : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *headerBGImage;
@property (weak, nonatomic) IBOutlet UIImageView *userProfileImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingUserProConstraint;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIButton *btnEditUserProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblMobile;
@property (strong, nonatomic) IBOutlet UIView *headerViewProfile;
@property (weak, nonatomic) IBOutlet UITableView *tblProfileInfo;

@end
@interface CellShowUserProfile:UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblValue;
@property (weak, nonatomic) IBOutlet UIImageView *imagePlaceholder;
@property (strong, nonatomic) IBOutlet UIImageView *imageRightDetail;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *widthOfRightDetailsImageConstraint;


@property (weak, nonatomic) IBOutlet UILabel *lblPlaceholder;

@end
