//
//  SocialAuthentication.m
//  Umang
//
//  Created by Kuldeep Saini on 11/28/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "SocialAuthentication.h"
#import <TwitterKit/TwitterKit.h>

@implementation SocialAuthentication

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        
        
        
        
    }
    return self;
}
#pragma mark- Facebook Login
#pragma mark-

-(void)loginWithFacebookFromController:(id)vwCont{
    
    self.delegate = vwCont;
    
    if ([FBSDKAccessToken currentAccessToken]) {
        // User is logged in, do work such as go to next view controller.
        
        NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@"id, name, email" forKey:@"fields"];
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                      id result, NSError *error) {
             if (!error) {
                 NSLog(@"fetched user:%@", result);
                 
                 SocialUserBO *objUser = [[SocialUserBO alloc] init];
                 objUser.auth_type = LOGIN_TYPE_FACEBOOK;
                 objUser.social_id = [result objectForKey:@"id"];
                 objUser.profile_email = [result objectForKey:@"email"];
                 objUser.profile_name = [result objectForKey:@"name"];
                 objUser.profile_image_url = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", [result objectForKey:@"id"]];
                 
                 if (self.delegate != nil && [self.delegate respondsToSelector:@selector(facebookAuthenticationSuccessful:)]) {
                     [self.delegate performSelector:@selector(facebookAuthenticationSuccessful:) withObject:objUser];
                 }
             }
             else{ // Error Occured While authenticating facebook
                 if (self.delegate != nil && [self.delegate respondsToSelector:@selector(facebookAuthenticationFailed:)]) {
                     [self.delegate performSelector:@selector(facebookAuthenticationFailed:) withObject:error];
                 }
             }
         }];
    }
    else{
        [self fetchInfoFromFacebook];
    }
}


-(void)fetchInfoFromFacebook{
    
    UIViewController *vwContForCallBack = (UIViewController*)self.delegate;
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    BOOL isInstalled = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://"]];
    if (isInstalled)
    {
        login.loginBehavior = FBSDKLoginBehaviorSystemAccount;
        
    } else {
        login.loginBehavior = FBSDKLoginBehaviorWeb;
        
    }
    
    
    [login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"] fromViewController:vwContForCallBack handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        if (error)
        {
            
            NSLog(@"Unexpected login error: %@", error);
            //      NSString *alertMessage = error.userInfo[FBSDKErrorLocalizedDescriptionKey] ?: @"There was a problem logging in. Please try again later.";
            
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(facebookAuthenticationFailed:)]) {
                [self.delegate performSelector:@selector(facebookAuthenticationFailed:) withObject:error];
            }
        }
        else if (result.isCancelled)
        {
            NSLog(@"Cancell");
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(facebookAuthenticationFailed:)]) {
                [self.delegate performSelector:@selector(facebookAuthenticationFailed:) withObject:error];
            }
            
        }
        else
        {
            [self loginWithFacebookFromController:self.delegate];
            
            //      if ([result.grantedPermissions containsObject:@"email"]) {
            //        // Do work
            //        [self loginWithFacebookFromController:self.delegate];
            //      }
        }
    }];
    
}


#pragma mark- Google Login
#pragma mark-

-(void)loginWithGoogleFromController:(id)vwCont
{
    SharedManager *singleton = [SharedManager sharedSingleton];
    singleton.SOCIAL_GOOGLE_FROM=@"GOOGLE_LOGIN_SOCIAL";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(googleLoginCompletedResult:) name:@"GOOGLE_LOGIN_SOCIAL" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(googleLoginFailed:) name:@"GOOGLE_ERROR" object:nil];
    
    self.delegate = vwCont;
    [GIDSignIn sharedInstance].uiDelegate = self.delegate;
    [[GIDSignIn sharedInstance] signIn];
    
}



-(void)googleLoginCompletedResult:(NSNotification*)notification
{
    
    GIDGoogleUser *user = notification.object;
    
    NSURL *imageURL;
    if (user.profile.hasImage)
    {
        // NSUInteger dimension = round(thumbSize.width * [[UIScreen mainScreen] scale]);
        imageURL = [user.profile imageURLWithDimension:44];
    }
    
    NSLog(@"user =%@",user);
    SocialUserBO *objUser = [[SocialUserBO alloc] init];
    objUser.auth_type = LOGIN_TYPE_GOOGLE;
    objUser.social_id = user.userID;
    objUser.auth_token = user.authentication.idToken;
    objUser.profile_name = user.profile.name;
    objUser.profile_image_url=[NSString stringWithFormat:@"%@",imageURL];
    
    objUser.profile_email = user.profile.email;
    
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(googleAuthenticationSuccessful:)])
    {
        [self.delegate performSelector:@selector(googleAuthenticationSuccessful:) withObject:objUser];
    }
    
    
    [self startCollectionNotifier];
}


- (void) startCollectionNotifier
{
    // All instances of TestClass will be notified
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADSOCIALVIEW" object:nil];
    
    
    
}

-(void)googleLoginFailed:(NSNotification*)notification{
    
    NSError *error = notification.object;
    
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(googleAuthenticationFailed:)]) {
        [self.delegate performSelector:@selector(googleAuthenticationFailed:) withObject:error];
    }
    
}


-(void)loginWithTwitterFromController:(id)vwCont
{
    
    self.delegate = vwCont;
    
    //  [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
    
    
    [[Twitter sharedInstance] logInWithMethods:TWTRLoginMethodWebBasedForceLogin completion:^(TWTRSession *session, NSError *error) {
        
        
        if (session) {
            
            [[[TWTRAPIClient alloc]init] loadUserWithID:[session userID] completion:^(TWTRUser* _Nullable user ,  NSError *_Nullable error){
                TWTRAPIClient *client = [TWTRAPIClient clientWithCurrentUser];
                
                NSURLRequest *request = [client URLRequestWithMethod:@"GET"
                                         
                                                                 URL:@"https://api.twitter.com/1.1/account/verify_credentials.json"
                                         
                                                          parameters:@{@"include_email": @"true", @"skip_status": @"true"}
                                         
                                                               error:nil];
                
                
                
                [client sendTwitterRequest:request completion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                    
                    if (connectionError) {
                        
                        NSLog(@"error: %@",connectionError.localizedDescription);
                        
                    }
                    
                    else{
                        
                        NSLog(@"success");
                        NSMutableDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                                      options:NSJSONReadingMutableContainers
                                                                                        error:nil];
                        NSLog(@"Response: %@", result);
                        
                        SocialUserBO *objUser = [[SocialUserBO alloc] init];
                        objUser.auth_type = LOGIN_TYPE_TWITTER;
                        objUser.social_id = [result objectForKey:@"id"];
                        objUser.profile_email = [result objectForKey:@"email"];
                        objUser.profile_name = [result objectForKey:@"name"];
                        objUser.profile_image_url = [result objectForKey:@"profile_image_url_https"];
                        
                        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(twitterAuthenticationSuccessful:)]) {
                            [self.delegate performSelector:@selector(twitterAuthenticationSuccessful:) withObject:objUser];
                        }
                        
                    }
                    
                }];
            }];
        }
    }];
}


/*  if(error)
 NSLog(@"error occurred... %@",error.localizedDescription);
 else
 {
 NSLog(@"Successfully logged in with session :%@",session);
 [self fetchUserProfile];
 }
 }];
 }
 **/



-(void)fetchUserProfile{
    TWTRAPIClient *client = [TWTRAPIClient clientWithCurrentUser];
    NSURLRequest *request = [client URLRequestWithMethod:@"GET"
                                                     URL:@"https://api.twitter.com/1.1/account/verify_credentials.json"
                                              parameters:@{@"include_email": @"true", @"skip_status": @"true"}
                                                   error:nil];
    
    [client sendTwitterRequest:request completion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        // NSString *strResponse = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        NSMutableDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                      options:NSJSONReadingMutableContainers
                                                                        error:nil];
        NSLog(@"Response: %@", result);
        
        SocialUserBO *objUser = [[SocialUserBO alloc] init];
        objUser.auth_type = LOGIN_TYPE_TWITTER;
        objUser.social_id = [result objectForKey:@"id"];
        objUser.profile_email = [result objectForKey:@"email"];
        objUser.profile_name = [result objectForKey:@"name"];
        objUser.profile_image_url = [result objectForKey:@"profile_image_url_https"];
        
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(twitterAuthenticationSuccessful:)]) {
            [self.delegate performSelector:@selector(twitterAuthenticationSuccessful:) withObject:objUser];
        }
        
    }];
}

-(void)logoutFromAllSocialFramewors
{
    //[[FBSDKLoginManager new] logOut];
    
    //[FBSession.activeSession closeAndClearTokenInformation];
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    BOOL isInstalled = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://"]];
    if (isInstalled)
    {
        login.loginBehavior = FBSDKLoginBehaviorSystemAccount;
        
    } else {
        login.loginBehavior = FBSDKLoginBehaviorWeb;
        
    }    [FBSDKAccessToken setCurrentAccessToken:nil];
    [FBSDKProfile setCurrentProfile:nil];
    
    [login logOut];
    
    
    
    [[GIDSignIn sharedInstance] signOut];
    [[GIDSignIn sharedInstance]disconnect];
    /*  TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
     NSString *userID = store.session.userID;
     [store logOutUserID:userID];
     */
    // [[[Twitter sharedInstance] sessionStore] logOutUserID:userID];
    
    
    
    
    [[NSURLCache sharedURLCache]removeAllCachedResponses];
    
    NSHTTPCookieStorage  *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie *each in cookieStorage.cookies)
    {
        [cookieStorage deleteCookie:each];
    }
    
    
}





-(void)dealloc
{
    NSLog(@"dealloc called");
    
}


#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}


/*
#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}



 */

@end
