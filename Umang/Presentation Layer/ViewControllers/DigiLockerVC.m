//
//  DigiLockerVC.m
//  Umang
//
//  Created by deepak singh rawat on 21/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "DigiLockerVC.h"
#import "DigiLockerWebVC.h"
#import "LinkDigilocker.h"

@interface DigiLockerVC ()
{
    
    __weak IBOutlet UIButton *btnMore;
    __weak IBOutlet UILabel *lblUploadDoc;
    __weak IBOutlet UILabel *lblShareDoc;
    __weak IBOutlet UILabel *lbllinkDigilocker;
    SharedManager *singleton;
    __weak IBOutlet UILabel *lblHeaderDigilocker;
    
    UIImageView   *imglinkDigi;
    UIImageView   *imglinkArrow;
    
    UIImageView   *imgDocument;
    UIImageView *imgArrowDocument;
    
    UIImageView   *imgSharing;
    UIImageView *imgArrow;
    
    
}
@end

@implementation DigiLockerVC



- (void)viewDidLoad
{
     singleton = [SharedManager sharedSingleton];
    
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:DIGILOCKER_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    self.view.backgroundColor= [UIColor colorWithRed:235/255.0 green:234/255.0 blue:241/255.0 alpha:1.0];
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
    
//    if ([[UIScreen mainScreen]bounds].size.height == 1024)
//    {
//        lblShareDoc.font = [UIFont systemFontOfSize:20.0];
//        lblUploadDoc.font = [UIFont systemFontOfSize:20.0];
//        lbllinkDigilocker.font = [UIFont boldSystemFontOfSize:20.0];
//        lbllinkDigilocker.numberOfLines = 2;
//    }

    
    //btn_linkdigilocker
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
}


-(void)loadbuttons
{
    
    
    lblHeaderDigilocker.text = NSLocalizedString(@"digi_locker", nil);
    btn_shareDocument.layer.cornerRadius = 5.0;
    btn_uploadDocument.layer.cornerRadius = 5.0;
    btn_linkdigilocker.layer.cornerRadius=5.0;
    
    imgSharing = [[UIImageView alloc]initWithFrame:CGRectMake(10,( btn_shareDocument.frame.size.height/2)-16, 32, 32)];
    imgSharing.image = [UIImage imageNamed:@"share_documents"];
    [btn_shareDocument setTitle: NSLocalizedString(@"share_documents", nil) forState:UIControlStateNormal];
    btn_shareDocument.titleLabel.font = [UIFont systemFontOfSize:19.0];
    [btn_shareDocument addSubview:imgSharing];
    
    
    imgArrow = [[UIImageView alloc]initWithFrame:CGRectMake(btn_shareDocument.frame.size.width-30,(btn_shareDocument.frame.size.height/2)-12, 20, 25)];
    [btn_shareDocument addSubview:imgArrow];
    imgArrow.image = [UIImage imageNamed:@"arrow_digi.png"];
    
        
    
    
    [btnMore setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnMore.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnMore.frame.origin.x, btnMore.frame.origin.y, btnMore.frame.size.width, btnMore.frame.size.height);
        
        
        [btnMore setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnMore.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    
    imgDocument = [[UIImageView alloc]initWithFrame:CGRectMake(10,(btn_uploadDocument.frame.size.height/2)-16, 32, 32)];
    [btn_uploadDocument addSubview:imgDocument];
    [btn_uploadDocument setTitle: NSLocalizedString(@"upload_documents", nil) forState:UIControlStateNormal];
    btn_uploadDocument.titleLabel.font = [UIFont systemFontOfSize:19.0];
    imgDocument.image = [UIImage imageNamed:@"upload_documents.png"];
    
    imgArrowDocument = [[UIImageView alloc]initWithFrame:CGRectMake(btn_uploadDocument.frame.size.width-30,(btn_uploadDocument.frame.size.height/2)-12, 20, 25)];
    [btn_uploadDocument addSubview:imgArrowDocument];
    imgArrowDocument.image = [UIImage imageNamed:@"arrow_digi.png"];
    
    
    lblShareDoc.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
     lblUploadDoc.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    
    lblShareDoc.text =  NSLocalizedString(@"share_documents_txt", nil);
    
    lblUploadDoc.text = NSLocalizedString(@"upload_documents_txt", nil);
    
    
    
    NSString *checkLinkStatus=[[NSUserDefaults standardUserDefaults] decryptedValueForKey:@"LINKDIGILOCKERSTATUS"];
   
    
    
    if (checkLinkStatus == nil|| [checkLinkStatus isEqualToString:@"NO"])
    {
        //------------------------- Encrypt Value------------------------
        [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
        // Encrypt
        [[NSUserDefaults standardUserDefaults] encryptValue:@"NO" withKey:@"LINKDIGILOCKERSTATUS"];
        [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"digilocker_username"];
        [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"digilocker_password"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        //------------------------- Encrypt Value------------------------
        
        
        
        
        
        lbllinkDigilocker.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
        lbllinkDigilocker.text = NSLocalizedString(@"link_digilocker_txt", nil);
        
        imglinkDigi = [[UIImageView alloc]initWithFrame:CGRectMake(10,( btn_linkdigilocker.frame.size.height/2)-16, 32, 32)];
        imglinkDigi.image = [UIImage imageNamed:@"img_linkdigi"];
        [btn_linkdigilocker setTitle: NSLocalizedString(@"link_digilocker", nil) forState:UIControlStateNormal];
        [btn_linkdigilocker setTitle: NSLocalizedString(@"link_digilocker", nil) forState:UIControlStateSelected];
        btn_linkdigilocker.titleLabel.font = [UIFont systemFontOfSize:19.0];
        [btn_linkdigilocker addSubview:imglinkDigi];
        imglinkArrow = [[UIImageView alloc]initWithFrame:CGRectMake(btn_linkdigilocker.frame.size.width-30,(btn_linkdigilocker.frame.size.height/2)-12, 20, 25)];
        [btn_linkdigilocker addSubview:imgArrow];
        imglinkArrow.image = [UIImage imageNamed:@"arrow_digi.png"];
        
        
        lbllinkDigilocker.hidden=FALSE;
        btn_linkdigilocker.hidden=FALSE;
        imglinkDigi.hidden=FALSE;
        
    }
    else  if ([checkLinkStatus isEqualToString:@"YES"])
    {
        lbllinkDigilocker.hidden=TRUE;
        btn_linkdigilocker.hidden=TRUE;
        imglinkDigi.hidden=TRUE;
    }
    
}


-(void)updateFrame

{
    imglinkDigi.frame = CGRectMake(10,( btn_linkdigilocker.frame.size.height/2)-16, 32, 32);
    imglinkArrow.frame =CGRectMake(btn_linkdigilocker.frame.size.width-30,(btn_linkdigilocker.frame.size.height/2)-12, 20, 25);
    
    
    imgDocument.frame = CGRectMake(10,(btn_uploadDocument.frame.size.height/2)-16, 32, 32);
    imgArrowDocument.frame = CGRectMake(btn_uploadDocument.frame.size.width-30,(btn_uploadDocument.frame.size.height/2)-12, 20, 25);
    
    imgSharing.frame = CGRectMake(10,( btn_shareDocument.frame.size.height/2)-16, 32, 32);
    imgArrow.frame = CGRectMake(btn_shareDocument.frame.size.width-30,(btn_shareDocument.frame.size.height/2)-12, 20, 25);
    
    
}


- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation duration:(NSTimeInterval)duration
{
    
    [self updateFrame];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}




-(void)viewWillAppear:(BOOL)animated
{
     [self updateFrame];
    
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.shouldRotate = NO;
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    
    
    
    
    
    
    
    
    [self loadbuttons];
    // [self setNeedsStatusBarAppearanceUpdate];
    //  [self performSelector:@selector(setHeightOfTableView) withObject:nil afterDelay:.1];
    /*UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(backbtnAction:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.navigationController.view addGestureRecognizer:gestureRecognizer];
    */
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [super viewWillAppear:NO];
}



-(IBAction)backbtnAction:(id)sender
{
    // [self dismissViewControllerAnimated:NO completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    
}


-(void)openDigiLockerWebVC:(NSString *)url withTitle:(NSString*)title
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    DigiLockerWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DigiLockerWebVC"];
    vc.urltoOpen=url;
    vc.titleOpen=title;
    
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    // [self.navigationController pushViewController:vc animated:YES];
    
    [self presentViewController:vc animated:NO completion:nil];
    
}
-(IBAction)btn_shareDocument:(id)sender
{
    
    NSString *titlestr=NSLocalizedString(@"digi_locker", nil);
    NSString *Url=@"https://web.umang.gov.in/Digilocker/digiLocker?src=ios";
    //https://web.umang.gov.in/Digilocker/digiLocker?src=ios
    [self openDigiLockerWebVC:Url withTitle:titlestr];
    
    
}
-(IBAction)btn_uploadDocument:(id)sender
{
    
    NSString *titlestr=NSLocalizedString(@"digi_locker", nil);
    NSString *Url=@"https://web.umang.gov.in/Digilocker/uploadDocs";
    
    [self openDigiLockerWebVC:Url withTitle:titlestr];
    //https://web.umang.gov.in/Digilocker/digiLocker?src=ios
    
}
-(IBAction)btn_linkdigilocker:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    LinkDigilocker *vc = [storyboard instantiateViewControllerWithIdentifier:@"LinkDigilocker"];
   // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self.navigationController pushViewController:vc animated:YES];
    
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}*/



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
