//
//  NearMeFilterResultVC.h
//  Umang
//
//  Created by admin on 29/08/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol filterChangeDelegate <NSObject>

- (void)filterChanged:(NSMutableArray*)parameters andCategoryDict:(NSMutableDictionary *)dict;

@end


@interface NearMeFilterResultVC : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate>
{
    IBOutlet UIView *vw_noresults;
    IBOutlet UILabel *lb_noresults;
    
    NSMutableArray *table_data;
    SharedManager *singleton;
    
    
}

@property (nonatomic, weak) id<filterChangeDelegate> delegate;
@property (nonatomic,strong) NSMutableArray *filterBOArray;

@property(nonatomic, retain) IBOutlet UICollectionView *allSer_collectionView;


@property (weak, nonatomic) IBOutlet UITableView *tblSearchFilter;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnFilter;

@property(nonatomic,assign) BOOL isFromHomeFilter;
@property (weak, nonatomic) IBOutlet UIButton *btnSort;
@property (weak, nonatomic) IBOutlet UIButton *btnSettingAgain;
- (IBAction)btnSettingAgainClicked:(id)sender;

@property(nonatomic,strong)NSMutableDictionary *dictFilterParams;

@property(nonatomic,strong)NSMutableArray *arrFilterResponse;


- (IBAction)btnSortActionClicked:(UIButton *)sender;

- (IBAction)btnFilterClicked:(id)sender;

-(IBAction)btnBackClicked:(id)sender;


@end
