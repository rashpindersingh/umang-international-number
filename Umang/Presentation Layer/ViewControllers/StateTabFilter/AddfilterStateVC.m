//
//  AddfilterStateVC.m
//  Umang
//
//  Created by admin on 29/08/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "AddfilterStateVC.h"
#import "FilterCell.h"
#import "SortFilterCell.h"
#import "FilterServicesBO.h"
#import "AllserviceFilterResultVC.h"
#import "StateCustomCell.h"
#import "SDCapsuleButton.h"
#import "CZPickerView.h"
#import "StateList.h"
#import "NotificationItemBO.h"
#import "NearMeFilterResultVC.h"
#import "FavFilterResultsVC.h"


#define BORDER_COLOR [UIColor colorWithRed:112.0/255.0 green:194.0/255.0 blue:138.0/255.0 alpha:1.0]


@interface AddfilterStateVC ()<CZPickerViewDelegate,CZPickerViewDataSource,filterChangeDelegate>

{
    NSMutableArray *arrNotificationTypes;
    // NSMutableArray *arrStateNames;
    NSString *_selectedNotificationType;
    
    BOOL isRegionalSelected;
    NSArray *arry_state;
    NSMutableArray *arrFirstFilterItems;
    NSMutableArray *arrSecondFilterItems;
    
    NSString *selectedFirstFilter;
    NSString *selectedSecondFilter;
    NSMutableArray *arrStates;
    SharedManager *singleton;
    NSMutableArray *_arrServiceFilter;
    NSMutableArray *_arrStates;
    
    StateList *obj;
    
    NSMutableArray *arrSelectedItemForPicker;
}

@end


@implementation AddfilterStateVC
@synthesize stateSelected;
- (IBAction)btnAlbhabaticClicked:(id)sender
{
    
}
- (IBAction)btnTopRatedClicked:(id)sender
{
    
}
- (IBAction)btnNearByClicked:(id)sender
{
    
}

@synthesize isRegionalSelected,btnApply;


- (void)filterChanged:(NSMutableArray*)parameters andCategoryDict:(NSMutableDictionary *)dict
{
    
    if ([dict valueForKey:@"selectedCategory"] != nil)
    {
        
        NSString * localizedString = NSLocalizedString(@"centralgovernment", nil);
        localizedString = [localizedString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        
        NSString * centralCategoryCheck = [dict valueForKey:@"selectedCategory"];
        centralCategoryCheck = [centralCategoryCheck stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        // if ([[dict valueForKey:@"selectedCategory"] isEqualToString:localizedString])
        if ([centralCategoryCheck isEqualToString:localizedString])

        {
            selectedSecondFilter = NSLocalizedString(@"centralgovernment", nil);
        }
        else if ([[dict valueForKey:@"selectedCategory"] isEqualToString:NSLocalizedString(@"regional", nil)])
        {
            selectedSecondFilter = NSLocalizedString(@"regional", nil);
        }
        else
        {
            selectedSecondFilter = NSLocalizedString(@"all", nil);
        }
    }
    else
    {
        selectedSecondFilter = NSLocalizedString(@"all", nil);
    }
    
    if ([[dict valueForKey:@"selectedStates"] count] > 0)
    {
        isRegionalSelected = YES;
        _arrStates = [dict valueForKey:@"selectedStates"];
        
        if ([_arrStates containsObject:NSLocalizedString(@"all", nil)])
        {
            arrSelectedItemForPicker = [NSMutableArray new];
        }
        else
        {
            [arrSelectedItemForPicker removeAllObjects];
            
            [arry_state enumerateObjectsUsingBlock:^(id  _Nonnull object, NSUInteger idx, BOOL * _Nonnull stop)
             {
                 if ([_arrStates containsObject:object])
                 {
                     [arrSelectedItemForPicker addObject:[NSNumber numberWithInteger:idx]];
                 }
             }];
        }
        
    }
    else
    {
        isRegionalSelected = NO;
        _arrStates = [NSMutableArray new];
        arrSelectedItemForPicker = [NSMutableArray new];
    }
    
    [_tblFilter reloadData];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];
     //——————— Add to handle portrait mode only———
     
     //———— Add to handle network bar of offline——
     NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
     [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];*/
    
    //———— Add to handle network bar of offline——
    
    
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    
    
    [_tblFilter reloadData];
    [_tblFilter setNeedsLayout ];
    [_tblFilter layoutIfNeeded ];
    [_tblFilter reloadData];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    [self setViewFont];
    [super viewWillAppear:NO];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [_btnBackHome.titleLabel setFont:[AppFont regularFont:17.0]];
    [_btnReset.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    _lblFilter.font = [AppFont semiBoldFont:17];
    [btnApply.titleLabel setFont:[AppFont regularFont:20.0]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setViewFont];
    arrSelectedItemForPicker = [NSMutableArray new];
    NSLog(@"inside view did load");
    _arrStates = [NSMutableArray new];
    if (stateSelected.length == 0) {
        //[_arrStates addObject:NSLocalizedString(@"all", nil)];
    }
    
    [btnApply setTitle:[NSLocalizedString(@"apply_label", nil) uppercaseString] forState:UIControlStateNormal];
    
    [_btnReset setTitle:NSLocalizedString(@"reset", nil) forState:UIControlStateNormal];
    
    [_btnBackHome setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    _lblFilter.text = NSLocalizedString(@"filter", nil);
    
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:_btnBackHome.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(_btnBackHome.frame.origin.x, _btnBackHome.frame.origin.y, _btnBackHome.frame.size.width, _btnBackHome.frame.size.height);
        
        
        [_btnBackHome setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        _btnBackHome.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    isRegionalSelected = NO;
    
    [self prepareTempDataForServicesType];
    
    //    [self setUpNavigationProperties];
    // arrStateNames = [NSMutableArray new];
    
    
    [self.tblFilter reloadData];
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
    singleton = [SharedManager sharedSingleton];
    
    [self addFilterServiceData];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        self.tblFilter.frame = CGRectMake(80, self.tblFilter.frame.origin.y, self.view.frame.size.width - 160, self.tblFilter.frame.size.height);
        
        self.tblFilter.backgroundColor = [UIColor colorWithRed:248.0/255.0f green:246.0/255.0f blue:247.0/255.0f alpha:1.0];
        
        self.view.backgroundColor = self.tblFilter.backgroundColor;
    }
    
}

-(void)addFilterServiceData
{
    if (_arrServiceFilter == nil) {
        _arrServiceFilter = [NSMutableArray new];
    }
    else{
        [_arrServiceFilter removeAllObjects];
    }
    
    NSArray *arrService = [singleton.dbManager loadServiceCategory];
    
    for (int i = 0; i< arrService.count; i++)
    {
        NSDictionary *dict = arrService [i];
        FilterServicesBO *objFilter = [[FilterServicesBO alloc]init];
        objFilter.serviceName = [dict valueForKey:@"SERVICE_CATEGORY"];
        [_arrServiceFilter addObject:objFilter];
    }
    
    [_tblFilter reloadData];
}




-(void)setUpNavigationProperties{
    //    [self.navigationController.navigationBar setTranslucent:YES];
    //
    //    [self.navigationController.navigationBar setShadowImage:nil];
    //    // "Pixel" is a solid white 1x1 image.
    //    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.navigationController.navigationBar setTranslucent:NO];
    
    self.navigationController.navigationBar.barTintColor =DEFAULT_NAV_BAR_COLOR;
    self.navigationController.navigationBarHidden = NO;
    self.title = NSLocalizedString(@"filter", nil);
    
    
    [_btnReset setTitle:NSLocalizedString(@"apply_label", nil) forState:UIControlStateNormal];
    
    
    
    // Add Reset Button on right side
    //    UIBarButtonItem *btnReset =    [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"reset", nil) style:UIBarButtonItemStyleDone target:self action:@selector(btnResetClicked)];
    //    self.navigationItem.rightBarButtonItem = btnReset;
    
    
    
}




- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

-(void)addShadowToTheView:(UIView*)vwItem
{
    vwItem.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    vwItem.layer.shadowColor = [UIColor brownColor].CGColor;
    vwItem.layer.shadowRadius = 3;
    vwItem.layer.shadowOpacity = 0.5;
    vwItem.layer.cornerRadius = 3.0;
}

//-(UIView*)designHeaderViewForFilterText{
//
//    static NSString *CellIdentifierNew = @"NotificationHeaderCell";
//    UITableViewCell *headerView = [self.tblFilter dequeueReusableCellWithIdentifier:CellIdentifierNew];
//
//    if (headerView == nil)
//    {
//        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
//    }
//
////    UILabel *lblSectionTitle = (UILabel*)[headerView viewWithTag:122];
////    lblSectionTitle.text = @"FILTERS";
////
////    UIButton *btnClear = (UIButton*)[headerView viewWithTag:123];
////    btnClear.hidden = YES;
//
//    return headerView;
//}


//-(void)prepareTempDataForNoticationType{
//
//    arrNotificationTypes = [NSMutableArray new];
//
//    FilterServicesBO *objService = [[FilterServicesBO alloc] init];
//    objService.serviceName = @"Promotional";
//    [arrNotificationTypes addObject:objService];
//    objService = nil;
//
//    objService = [[FilterServicesBO alloc] init];
//    objService.serviceName = @"Transactional";
//    [arrNotificationTypes addObject:objService];
//    objService = nil;
//
//
//}

-(void)prepareTempDataForServicesType{
    
    // Prepare State Data
    //    arrStates = [[NSMutableArray alloc] initWithObjects:@"Andhra_Pradesh",@"Arunachal_Pradesh",@"Assam",@"Bihar",@"Chhattisgarh",@"Goa",@"Gujarat",@"Haryana",@"Himachal_Pradesh",@"Jammu_and_Kashmir",@"Jharkhand",@"Karnataka",@"Kerala",@"Madhya_Pradesh",@"Maharashtra",@"Manipur",@"Meghalaya",@"Mizoram",@"Nagaland",@"Odisha",@"Punjab",@"Rajasthan",@"Mizoram",@"Sikkim",@"Tamil_Nadu",@"Telangana",@"Tripura",@"Uttar_Pradesh",@"West_Bengal",@"Uttarakhand", nil];
    
    
    // Prepare State Data
    
    obj = [[StateList alloc] init];
    arry_state = [obj getStateList];
    
    arry_state = [arry_state sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    arrFirstFilterItems = [NSMutableArray new];
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:NSLocalizedString(@"alphabetic", nil) forKey:@"Title"];
    [dict setObject:@"icon_alphabatic" forKey:@"NormalState"];
    [dict setObject:@"icon_alphabatic_selected.png" forKey:@"SelectedState"];
    //objNotification.filterType = @"notification_type";
    [arrFirstFilterItems addObject:dict];
    dict = nil;
    
    selectedFirstFilter = NSLocalizedString(@"alphabetic", nil);
    
    dict = [NSMutableDictionary new];
    [dict setObject:NSLocalizedString(@"most_popular", nil) forKey:@"Title"];
    [dict setObject:@"icon_most_popular.png" forKey:@"NormalState"];
    [dict setObject:@"icon_most_popular_select.png" forKey:@"SelectedState"];
    [arrFirstFilterItems addObject:dict];
    dict = nil;
    
    dict = [NSMutableDictionary new];
    [dict setObject:NSLocalizedString(@"top_rated", nil) forKey:@"Title"];
    [dict setObject:@"icon_top_rated.png" forKey:@"NormalState"];
    [dict setObject:@"icon_top_rated_select.png" forKey:@"SelectedState"];
    [arrFirstFilterItems addObject:dict];
    dict = nil;
    
    
    arrSecondFilterItems = [NSMutableArray new];
    dict = [NSMutableDictionary new];
    [dict setObject:NSLocalizedString(@"all", nil) forKey:@"Title"];
    [dict setObject:@"icon_all.png" forKey:@"NormalState"];
    [dict setObject:@"icon_all_select.png" forKey:@"SelectedState"];
    [arrSecondFilterItems addObject:dict];
    dict = nil;
    //selectedSecondFilter = NSLocalizedString(@"all", nil);
    selectedSecondFilter = NSLocalizedString(@"regional", nil);
    if ([selectedSecondFilter isEqualToString:NSLocalizedString(@"regional", nil)])
    {
        isRegionalSelected = YES;
        
        if ([arry_state containsObject:stateSelected]) {
            NSUInteger index = [arry_state indexOfObject:stateSelected];
            [_arrStates addObject:arry_state[index]];
            [arrSelectedItemForPicker addObject:[NSString stringWithFormat:@"%lu",(unsigned long)index]];
        }else {
            [_arrStates addObject:NSLocalizedString(@"all", nil)];
        }
    }
    dict = [NSMutableDictionary new];
    [dict setObject:NSLocalizedString(@"centralgovernment", nil) forKey:@"Title"];
    [dict setObject:@"icon_central-_govt.png" forKey:@"NormalState"];
    [dict setObject:@"icon_central-_govt_select.png" forKey:@"SelectedState"];
    [arrSecondFilterItems addObject:dict];
    dict = nil;
    
    dict = [NSMutableDictionary new];
    [dict setObject:NSLocalizedString(@"regional", nil) forKey:@"Title"];
    [dict setObject:@"icon_regional-1.png" forKey:@"NormalState"];
    [dict setObject:@"icon_regional_selected.png" forKey:@"SelectedState"];
    [arrSecondFilterItems addObject:dict];
    dict = nil;
    
    
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (isRegionalSelected) {
        if (section == 3) {
            if ([_arrServiceFilter count] > 0)
            {
                return 10.0;
            }
            else
            {
                return 10;
            }
        }
        return 50.0;
    }
    else{
        if (section == 0 || section == 1 || section == 4) {
            return 50.0;
        }
        else{
            return 0.001;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    return 0.001;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    static NSString *CellIdentifier = @"NotificationHeaderCell";
    UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (headerView == nil){
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    UILabel *lblSectionTitle = (UILabel*)[headerView viewWithTag:122];
    UIButton *btnClear = (UIButton*)[headerView viewWithTag:123];
    
    
    [btnClear setTitle:NSLocalizedString(@"clear", @"") forState:UIControlStateSelected];
    [btnClear setTitle:NSLocalizedString(@"clear", @"") forState:UIControlStateNormal];
    
    /*
     CGRect buttonClearType =  btnClear.frame;

    buttonClearType.origin.x = singleton.isArabicSelected ? 10 : CGRectGetWidth(tableView.frame) - 70;
    btnClear.frame = buttonClearType;
    
    
    CGRect labelFrame =  lblSectionTitle.frame;
    labelFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tableView.frame) - 100 : 15;
    lblSectionTitle.frame = labelFrame;
    */
    
    CGRect buttonClearType =  btnClear.frame;
    buttonClearType.origin.x = singleton.isArabicSelected ?10 : 250;
    btnClear.frame = CGRectMake(buttonClearType.origin.x , buttonClearType.origin.y, buttonClearType.size.width, buttonClearType.size.height);
    CGRect labelFrame =  lblSectionTitle.frame;
    CGFloat originX=0;
    NSLog(@"fDeviceWidth=%f",fDeviceWidth);
    if (fDeviceWidth==320)
    {
        originX=fDeviceWidth - (lblSectionTitle.frame.size.width+lblSectionTitle.frame.origin.x+10);
    }
    else if (fDeviceWidth>375)
    {
        originX=fDeviceWidth - (lblSectionTitle.frame.size.width+lblSectionTitle.frame.origin.x+100);
    }
    else
    {
        originX=fDeviceWidth - (lblSectionTitle.frame.size.width+lblSectionTitle.frame.origin.x+70);
    }
   
    labelFrame.origin.x = singleton.isArabicSelected ? originX : 15;
    lblSectionTitle.frame = CGRectMake(labelFrame.origin.x, labelFrame.origin.y, labelFrame.size.width, labelFrame.size.height);
   
    lblSectionTitle.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;

    
    if (section == 0) {
        lblSectionTitle.text = NSLocalizedString(@"sort_by", nil);
        btnClear.hidden = YES;
        
    }
    else if (section == 1)
    {
        
        lblSectionTitle.text = NSLocalizedString(@"alll_label", nil);
        btnClear.hidden = YES;
        
    }
    else if (section == 2)
    {
        if (isRegionalSelected) {
            lblSectionTitle.text = NSLocalizedString(@"state_label", nil);
            [btnClear addTarget:self action:@selector(btnClearStateClicked) forControlEvents:UIControlEventTouchUpInside];
            
            
            //if ([_arrServiceFilter count] && ![_arrStates containsObject:NSLocalizedString(@"all", nil)])
            
            if ([_arrStates count] && ![_arrStates containsObject:NSLocalizedString(@"all", nil)])
            {
                btnClear.hidden = NO;
            }
            else{
                btnClear.hidden = YES;
            }
            
        }
        else{
            return nil;
        }
    }
    else if (section == 3)
    {
        if (isRegionalSelected) {
            
            lblSectionTitle.text = @"";
        }
        else{
            return nil;
            
        }
        btnClear.hidden = YES;
        
    }
    
    else
    {
        [btnClear addTarget:self action:@selector(btnClearFilterCategoriesClicked) forControlEvents:UIControlEventTouchUpInside];
        
        lblSectionTitle.text = NSLocalizedString(@"cat_label", nil);
        
        // First Fetch all categories
        NSPredicate *predicateFilter  = [NSPredicate predicateWithFormat:@"SELF.isServiceSelected == YES"];
        NSArray *arrServices = [_arrServiceFilter filteredArrayUsingPredicate:predicateFilter];
        
        if ([arrServices count]) {
            btnClear.hidden = NO;
        }
        else{
            btnClear.hidden = YES;
        }
    }
    headerView.backgroundColor = [UIColor colorWithRed:248.0/255.0 green:246.0/255.0 blue:247.0/255.0 alpha:1.0];
    lblSectionTitle.font = [AppFont regularFont:13.0];
    btnClear.titleLabel.font = [AppFont mediumFont:13.0];
    //changes done
    btnClear.titleLabel.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    return headerView;
}

-(void)btnClearStateClicked
{
    //    [_arrServiceFilter removeAllObjects];
    //    [_tblFilter reloadData];
    
    [_arrStates removeAllObjects];
    [arrSelectedItemForPicker removeAllObjects];
    [_arrStates addObject:NSLocalizedString(@"all", nil)];
    [_tblFilter reloadData];
    
}
-(void)btnClearFilterCategoriesClicked
{
    [_arrServiceFilter setValue:@NO forKey:@"isServiceSelected"];
    [_tblFilter reloadData];
    
}

- ( UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}// custom view for footer. will be adjusted to default or specified footer height



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if ((indexPath.section == 0) || (indexPath.section == 1))
    {
        return  100;
    }
    else if (indexPath.section == 2) {
        return [self heightForDynamicCell];
    }
    else if (indexPath.section == 3)
    {
        return 70.0;
    }
    else
        return 40.0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;              // Default is 1 if not implemented
{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    switch (section)
    {
        case 0:
            return 1;
            break;
            
        case 1:
            return 1;
            break;
            
        case 2:
            return isRegionalSelected?1:0;
            break;
            
        case 3:
            return isRegionalSelected?1:0;
            break;
            
        case 4:
            return _arrServiceFilter.count;
            break;
            
            
        default:
            break;
    }
    
    return 0;
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
}
-(void)btnClickedFromCellForFistSection:(UIButton*)sender{
    SDCapsuleButton *superVw = (SDCapsuleButton*)[sender superview];
    selectedFirstFilter = [superVw btnTitle];
    
    [_tblFilter reloadData];
    
}

-(void)btnClickedFromCellForSecondSection:(UIButton*)sender{
    SDCapsuleButton *superVw = (SDCapsuleButton*)[sender superview];
    selectedSecondFilter = [superVw btnTitle];
    if ([selectedSecondFilter isEqualToString:NSLocalizedString(@"regional", nil)])
    {
        isRegionalSelected = YES;
        
        if (_arrStates.count == 0)
        {
            [_arrStates addObject:NSLocalizedString(@"all", nil)];
        }
    }
    else{
        isRegionalSelected = NO;
    }
    
    [_tblFilter reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    
    if (indexPath.section == 0) {
        static NSString *CellIdentifier = @"SortCellIdentifier";
        SortFilterCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        CGFloat xCord = 10;
        CGFloat yCord = 13;
        
        CGFloat width = 100;
        CGFloat height = 75;
        
        CGFloat screenWidth = self.tblFilter.frame.size.width - 2*xCord;
        CGFloat paddingWidth = (screenWidth/2 - width/2);
        for (int i = 0 ;  i < arrFirstFilterItems.count ; i++) {
            
            NSMutableDictionary *dictItem = arrFirstFilterItems[i];
            SDCapsuleButton *btnFilter = [[SDCapsuleButton alloc] initWithFrameForFilterButtons:CGRectMake(xCord, yCord, width, height) withTitle:[dictItem objectForKey:@"Title"] withNormalImage:[dictItem objectForKey:@"NormalState"] andSelectedImage:[dictItem objectForKey:@"SelectedState"]];
            [cell.contentView addSubview:btnFilter];
            [btnFilter.btnMain addTarget:self action:@selector(btnClickedFromCellForFistSection:) forControlEvents:UIControlEventTouchUpInside];
            
            if ([selectedFirstFilter isEqualToString:[dictItem objectForKey:@"Title"]]) {
                [btnFilter setSelected:YES];
            }
            else{
                [btnFilter setSelected:NO];
            }
            
            if (i==0) {
                xCord = paddingWidth;
            }
            else{
                xCord = screenWidth - width;
            }
        }
        
        
        return cell;
        
    }
    else if (indexPath.section == 1)
    {
        static NSString *CellIdentifierNew = @"SortCellIdentifier";
        SortFilterCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierNew];
        [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        CGFloat xCord = 10;
        CGFloat yCord = 13;
        
        CGFloat width = 100;
        CGFloat height = 75;
        
        CGFloat screenWidth = tableView.frame.size.width - 2*xCord;
        CGFloat paddingWidth = (screenWidth/2 - width/2);
        for (int i = 0 ;  i < arrSecondFilterItems.count ; i++) {
            
            NSMutableDictionary *dictItem = arrSecondFilterItems[i];
            SDCapsuleButton *btnFilter = [[SDCapsuleButton alloc] initWithFrameForFilterButtons:CGRectMake(xCord, yCord, width, height) withTitle:[dictItem objectForKey:@"Title"] withNormalImage:[dictItem objectForKey:@"NormalState"] andSelectedImage:[dictItem objectForKey:@"SelectedState"]];
            [cell.contentView addSubview:btnFilter];
            
            [btnFilter.btnMain addTarget:self action:@selector(btnClickedFromCellForSecondSection:) forControlEvents:UIControlEventTouchUpInside];
            
            if ([selectedSecondFilter isEqualToString:[dictItem objectForKey:@"Title"]]) {
                [btnFilter setSelected:YES];
            }
            else{
                [btnFilter setSelected:NO];
            }
            
            
            if (i==0) {
                xCord = paddingWidth;
            }
            else{
                xCord = screenWidth - width;
            }
        }
        
        
        return cell;
        
    }
    
    else if (indexPath.section == 2 || indexPath.section == 3)
    {
        
        
        
        static NSString *cellIdentifierState = @"StateNameCell";
        StateCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifierState];
        cell.textLabel.text = @"";
        // Remove Previous contents
        [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        if (indexPath.section == 2) {
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            // Add Capsule Buttons for Selected States
            [self designCellContentForStateNamesOnCelView:cell.contentView];
        }
        else{
            
            UIView *tempVw = [[UIView alloc] initWithFrame:CGRectMake(10, 10, CGRectGetWidth(self.tblFilter.frame) - 20, 50)];
            tempVw.layer.borderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.5].CGColor;
            tempVw.layer.borderWidth = 1.0;
            tempVw.layer.cornerRadius  = 2.0;
            tempVw.clipsToBounds = YES;
            tempVw.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:tempVw];
            
            cell.textLabel.backgroundColor = [UIColor clearColor];
            cell.textLabel.text = NSLocalizedString(@"select_state", nil);
            cell.textLabel.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        cell.textLabel.font = [AppFont regularFont:14];

        return cell;
        
    }
    
    else
    {
        
        static NSString *CellIdentifierType = @"TypeCellIdentifier";
        FilterCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierType];
        [cell.btnCheckUncheck addTarget:self action:@selector(btnCheckboxClickedForServices:) forControlEvents:UIControlEventTouchUpInside];
        cell.lblType.font = [AppFont regularFont:15.0];
        /* cell.btnCheckUncheck.tag = 1000+indexPath.row;
         cell.separatorInset = UIEdgeInsetsMake(0, 1000, 0, 0);
         [cell.btnCheckUncheck setImage:[UIImage imageNamed:@"checkbox_marked.png"] forState:UIControlStateNormal];
         
         
         CGRect labelType =  cell.lblType.frame;
         labelType.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tableView.frame) - 100 : 15;
         cell.lblType.frame = labelType;
         
         
         CGRect btnCheck =  cell.btnCheckUncheck.frame;
         btnCheck.origin.x = singleton.isArabicSelected ? 15 : CGRectGetWidth(_tblFilter.frame) - 60;
         cell.btnCheckUncheck.frame = btnCheck;*/
        
        
        return cell;
        
    }
    
}



- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell isKindOfClass:[FilterCell class]]) {
        
        FilterCell *cellFilter = (FilterCell*)cell;
        
        
        FilterServicesBO *objCategory = [_arrServiceFilter objectAtIndex:indexPath.row];
        cellFilter.lblType.text = objCategory.serviceName;
        
        
        cellFilter.btnCheckUncheck.tag = 1000+indexPath.row;
        cellFilter.separatorInset = UIEdgeInsetsMake(0, 1000, 0, 0);
        [cellFilter.btnCheckUncheck setImage:[UIImage imageNamed:@"img_uncheck-1.png"] forState:UIControlStateNormal];
        
        if (objCategory.isServiceSelected)
        {
            [cellFilter.btnCheckUncheck setImage:[UIImage imageNamed:@"checkbox_marked.png"] forState:UIControlStateNormal];
        }
        else
        {
            [cellFilter.btnCheckUncheck setImage:[UIImage imageNamed:@"img_uncheck-1.png"] forState:UIControlStateNormal];
            
        }
        
        
        
        CGRect labelType =  cellFilter.lblType.frame;
        labelType.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tableView.frame) - 100 : 15;
        cellFilter.lblType.frame = labelType;
        
        CGRect btnCheck =  cellFilter.btnCheckUncheck.frame;
        btnCheck.origin.x = singleton.isArabicSelected ? 10 : CGRectGetWidth(_tblFilter.frame) - 35;
        if(singleton.isArabicSelected==TRUE)
        {
            labelType.origin.x = btnCheck.origin.x+btnCheck.size.width+5 ;
            cellFilter.lblType.frame = labelType;
            cellFilter.lblType.textAlignment=NSTextAlignmentRight;
        }
        else
        {
            labelType.origin.x = 15 ;
            cellFilter.lblType.frame = labelType;
            cellFilter.lblType.textAlignment=NSTextAlignmentLeft;
            
        }
        cellFilter.btnCheckUncheck.frame = btnCheck;
    }
}





-(void)btnCheckboxClickedForServices:(UIButton*)btnSender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:btnSender.tag-1000 inSection:4];
    [self checkUncheckFilterOptions:indexPath];
}


//
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
    }
    else if (indexPath.section == 3){
        
        [self showStatePicker];
    }
    else if (indexPath.section == 4){
        
        [self checkUncheckFilterOptions:indexPath];
    }
}



-(void)checkUncheckFilterOptions:(NSIndexPath*)indexPath
{
    FilterServicesBO *objFilter = nil;
    
    if (indexPath.section == 4){
        objFilter = _arrServiceFilter[indexPath.row];
    }
    
    objFilter.isServiceSelected = !objFilter.isServiceSelected;
    
    [self.tblFilter reloadData];
    
    
}

//- (IBAction)btnDoneClicked:(id)sender {
//
//    [self.navigationController popViewControllerAnimated:YES];
//}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}

-(CGFloat)heightForDynamicCell{
    
    CGFloat xCord = 10;
    CGFloat heightPadding = 45;
    CGFloat padding = 10;
    
    if ([_arrStates count] == 0) {
        return 0.0;
    }
    else
    {
        CGFloat height = 60.0;
        for (int i = 0; i<_arrStates.count; i++)
        {
            NSString *stateName = _arrStates[i];
            CGFloat screenWidth = _tblFilter.frame.size.width;
            
            CGRect frame = [self rectForText:stateName usingFont:TITLE_FONT boundedBySize:CGSizeMake(screenWidth, 30.0)];
            frame.size.width = frame.size.width + 40;
            
            CGFloat rightMargin = self.tblFilter.frame.size.width - xCord;
            if (rightMargin < frame.size.width) {
                xCord = 10;
                height+=heightPadding;
                NSLog(@"New Item, Height Increased = %lf",height);
            }
            
            xCord+=frame.size.width + padding;
        }
        
        return height;
    }
    
}

-(void)designCellContentForStateNamesOnCelView:(UIView*)contentView{
    
    
    CGFloat xCord = 10;
    CGFloat yCord = 15;
    CGFloat padding = 10;
    CGFloat heightPadding = 45;
    for (int i = 0; i<_arrStates.count; i++) {
        
        NSString *stateName = _arrStates[i];
        
        CGFloat screenWidth = _tblFilter.frame.size.width;
        
        CGRect frame = [self rectForText:stateName usingFont:TITLE_FONT boundedBySize:CGSizeMake(screenWidth, 30.0)];
        frame.size.width = frame.size.width + 40;
        
        CGFloat rightMargin = self.tblFilter.frame.size.width - xCord;
        if (rightMargin < frame.size.width) {
            xCord = 10;
            yCord+=heightPadding;
        }
        
        SDCapsuleButton *btn = [[SDCapsuleButton alloc] initWithFrame:CGRectMake(xCord, yCord, frame.size.width, 30)];
        [btn setBtnTitle:stateName];
        [btn addTarget:self action:@selector(btnCapsuleStateClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn.btnCross addTarget:self action:@selector(btnCrossClicked:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 2300+i;
        
        
        [contentView addSubview:btn];
        
        if ([stateName isEqualToString:NSLocalizedString(@"all", nil)])
        {
            btn.btnCross.hidden = YES;
        }
        else
        {
            btn.btnCross.hidden = NO;
        }
        
        xCord+=frame.size.width + padding;
    }
}


-(void)btnCrossClicked:(UIButton*)btnCross
{
    SDCapsuleButton *btn = (SDCapsuleButton*)[btnCross superview];
    NSInteger row = btn.tag - 2300;
    if (row < _arrStates.count) {
        [_arrStates removeObjectAtIndex:row];
        [arrSelectedItemForPicker removeObjectAtIndex:row];
        [_tblFilter reloadData];
        
    }
    if (_arrStates.count == 0)
    {
        [_arrStates addObject:NSLocalizedString(@"all", nil)];
        
        [_tblFilter reloadData];
    }
    
}


-(void)btnCapsuleStateClicked:(SDCapsuleButton*)btnState{
    NSInteger row = btnState.tag - 2300;
    NSLog(@"State Clicked : %li",(long)row);
}

- (IBAction)btnAlbhabatic:(id)sender
{
    
}

- (IBAction)btnbackClicked:(id)sender
{
    
    //   [self dismissViewControllerAnimated:NO completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)btnApplyClicked:(id)sender
{
    NSLog(@"Apply Pressed");
    
    // Get Filtered Content Here
    NSMutableDictionary *dictFilterOptions = [NSMutableDictionary new];
    
    // First Fetch all categories
    NSPredicate *predicateFilter  = [NSPredicate predicateWithFormat:@"SELF.isServiceSelected == YES"];
    NSArray *arrServices = [_arrServiceFilter filteredArrayUsingPredicate:predicateFilter];
    
    NSMutableArray *arrSelectedCategories = [NSMutableArray new];
    if ([arrServices count])
    {
        for (int i = 0; i<arrServices.count; i++) {
            FilterServicesBO *objFilterCateogry = arrServices[i];
            [arrSelectedCategories addObject:objFilterCateogry.serviceName];
        }
        [dictFilterOptions setObject:arrSelectedCategories forKey:@"category_type"];
    }
    
    // Now fetch all states
    // Now fetch all states
    // Now fetch all states
    
    //NSString *serviceType = [selectedSecondFilter stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString *serviceType = selectedSecondFilter;
    
    if ([selectedSecondFilter isEqualToString:NSLocalizedString(@"regional", nil)])
    {
        if ([_arrStates count])
        {
            [dictFilterOptions setObject:_arrStates forKey:@"state_name"];
            
            if (serviceType)
            {
                
                [dictFilterOptions setObject:serviceType forKey:@"service_type"];
                
            }
        }
        
    }
    else
    {
        [dictFilterOptions setObject:serviceType forKey:@"service_type"];
        
    }
    
    
    // Now fetch all states
    // Now fetch all states
    // Now fetch all states
    
    // Fetch filter data
    NSString *sortBy =[NSString stringWithFormat:@"%@",selectedFirstFilter ];
    
    // NSString *sortBy = [selectedFirstFilter stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (sortBy) {
        [dictFilterOptions setObject:sortBy forKey:@"sort_by"];
    }
    
    
    
    /*UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"StateFilterResult" bundle:nil];
     
     NearMeFilterResultVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"NearMeFilterResultVC"];
     vc.dictFilterParams = dictFilterOptions;
     vc.delegate = self;
     vc.filterBOArray = [NSMutableArray new];
     
     vc.filterBOArray = _arrServiceFilter;
     vc.isFromHomeFilter = YES;
     vc.hidesBottomBarWhenPushed = YES;
     [self.navigationController pushViewController:vc animated:YES];*/
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    FavFilterResultsVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FavFilterResultsVC"];
    vc.dictFilterParams = dictFilterOptions;
    vc.isFromHomeFilter = YES;
    vc.comingFromFilter = @"fromStateTab";
    vc.delegate = self;
    vc.filterBOArray = [NSMutableArray new];
    
    vc.filterBOArray = _arrServiceFilter;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}


- (IBAction)btnResetClicked:(id)sender
{
    // selectedSecondFilter = NSLocalizedString(@"all", nil);
    selectedSecondFilter = NSLocalizedString(@"regional", nil);
    
    selectedFirstFilter = NSLocalizedString(@"alphabetic", nil);
    
    if ([selectedSecondFilter isEqualToString:NSLocalizedString(@"regional", nil)]) {
        isRegionalSelected = YES;
    }
    else{
        isRegionalSelected = NO;
    }
    [arrStates removeAllObjects];
    
    if ([selectedSecondFilter isEqualToString:NSLocalizedString(@"regional", nil)])
    {
        isRegionalSelected = YES;
        [self btnClearStateClicked];
//        if (_arrStates.count == 0)
//        {
//            [_arrStates addObject:NSLocalizedString(@"all", nil)];
//        }
        
    }
    
    
    
    [_arrServiceFilter setValue:@NO forKey:@"isServiceSelected"];
    [arrStates setValue:@NO forKey:@"isServiceSelected"];
    
    
    
    
    [self.tblFilter reloadData];
}




#pragma mark- State Picker

-(void)showStatePicker
{
    CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:NSLocalizedString(@"profile_state", nil) cancelButtonTitle:nil confirmButtonTitle:NSLocalizedString(@"done", nil)];
    picker.delegate = self;
    picker.dataSource = self;
    picker.allowRadioButtons = YES;
    picker.isClearOptionRequired = NO;
    picker.allowMultipleSelection = YES;
    picker.arrPreviousItemSelected = arrSelectedItemForPicker;
    [picker show];
    
    //  [picker setSelectedRows:arrSelectedItemForPicker];
    
}
- (NSAttributedString *)czpickerView:(CZPickerView *)pickerView
               attributedTitleForRow:(NSInteger)row{
    
    NSAttributedString *att = [[NSAttributedString alloc]
                               initWithString:arry_state[row]
                               attributes:@{
                                            NSFontAttributeName:[UIFont fontWithName:@"Avenir-Light" size:18.0]
                                            }];
    return att;
}

- (NSString *)czpickerView:(CZPickerView *)pickerView
               titleForRow:(NSInteger)row{
    return arry_state[row];
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView {
    return arry_state.count;
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemsAtRows:(NSArray *)rows
{
    [_arrStates removeAllObjects];
    [arrSelectedItemForPicker removeAllObjects];
    
    NSArray * newArray = [[NSOrderedSet orderedSetWithArray:rows] array]; // iOS 5.0 and later**
    [arrSelectedItemForPicker addObjectsFromArray:newArray];
    
    for (NSNumber *n in newArray)
    {
        NSInteger row = [n integerValue];
        NSLog(@"%@ is chosen!", arry_state[row]);
        [_arrStates addObject:arry_state[row]];
    }
    
    if ([_arrStates count] == [arry_state count] || [_arrStates count] == 0)
    {
        [_arrStates removeAllObjects];
        [_arrStates addObject:NSLocalizedString(@"all", nil)];
    }
    [_tblFilter reloadData];
}

- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView {
    NSLog(@"Canceled.");
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation duration:(NSTimeInterval)duration
{
    [_tblFilter reloadData];
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/



@end

