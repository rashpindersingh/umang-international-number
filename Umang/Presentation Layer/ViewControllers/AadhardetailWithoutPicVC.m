//
//  AadhardetailWithoutPicVC.m
//  Umang
//
//  Created by admin on 09/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "AadhardetailWithoutPicVC.h"
#import "AadharDetailProfileCell.h"
#import "AadharDataBO.h"
#import "AadharDetailInfoCell.h"


@interface AadhardetailWithoutPicVC () <UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *arrAadharData;
    AadharDetailProfileCell  *headerProfileCell;
}
@end

@implementation AadhardetailWithoutPicVC
@synthesize tblAadharDetail;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    arrAadharData = [[NSMutableArray alloc]init];
    [self prepareTempDataForAadharCard];
    
    tblAadharDetail.dataSource = self;
    tblAadharDetail.delegate = self;
    
     tblAadharDetail.tableHeaderView = [self designAadharProfileView];
    
    
    self.navigationController.navigationBarHidden=true;
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [self setNeedsStatusBarAppearanceUpdate];
    
}
 
-(AadharDetailProfileCell*)designAadharProfileView

{
    if (headerProfileCell == nil)
        
    {
        headerProfileCell = [[[NSBundle mainBundle] loadNibNamed:@"AadharDetailProfileCell" owner:self options:nil] objectAtIndex:0];
        headerProfileCell.TYPE_AADHAR_CHOOSEN = AADHARWITHOUTPIC;
            [headerProfileCell bindDataForHeaderView];
    }
    
    
    return headerProfileCell;
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}





-(void)prepareTempDataForAadharCard
{
    
    AadharDataBO *objAadharData = [[AadharDataBO alloc] init];
    objAadharData.title =  @"Name";
    objAadharData.titleDescription = @"Parampreet Dhatt";
    [arrAadharData addObject:objAadharData];
    objAadharData = nil;
    
    
    objAadharData = [[AadharDataBO alloc] init];
    objAadharData.title =  @"Date of Birth";
    objAadharData.titleDescription = @"28/12/2000";
    [arrAadharData addObject:objAadharData];
    objAadharData = nil;
    
    
    objAadharData = [[AadharDataBO alloc] init];
    objAadharData.title =  @"Gender";
    objAadharData.titleDescription = @"Male";
    //objNotification.filterType = @"notification_type";
    [arrAadharData addObject:objAadharData];
    objAadharData = nil;
    
    
    
    objAadharData = [[AadharDataBO alloc] init];
    objAadharData.title =  @"Father's Name";
    objAadharData.titleDescription = @"Mohan Singh";
    //objNotification.filterType = @"notification_type";
    [arrAadharData addObject:objAadharData];
    objAadharData = nil;
    
    
    objAadharData = [[AadharDataBO alloc] init];
    objAadharData.title =  @"Address";
    objAadharData.titleDescription = @"1234, sector 68";
    //objNotification.filterType = @"notification_type";
    [arrAadharData addObject:objAadharData];
    objAadharData = nil;
    
    
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 0.001;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    return 0.001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    return 44.0;
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    
    return arrAadharData.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;

{
    
    static NSString *CellIdentifierNew = @"AadharDetailInfoCell";
    AadharDetailInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierNew];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    {

    AadharDataBO  *objAadhar = arrAadharData [indexPath.row];
    cell.lblTitle.text = objAadhar.title;
    cell.lblTitleValue.text = objAadhar.titleDescription;
    
    
    }

    return cell;
    
}


#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}*/



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnMoreClicked:(id)sender
{
     [self.navigationController popViewControllerAnimated:YES];
}
@end
