//
//  SearchFilterVC.h
//  Umang
//
//  Created by admin on 07/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol filterChangeDelegate <NSObject>

- (void)filterChanged:(NSMutableArray*)parameters andCategoryDict:(NSMutableDictionary *)dict;

@end

@interface SearchFilterVC : UIViewController
{
    IBOutlet UIView *vw_noresults;
    IBOutlet UILabel *lb_noresults;

}

@property (nonatomic, weak) id<filterChangeDelegate> delegate;
@property (nonatomic,strong) NSMutableArray *filterBOArray;
@property (nonatomic,strong) NSMutableArray *notificationBOArray;


@property (weak, nonatomic) IBOutlet UITableView *tblSearchFilter;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnFilter;

@property(nonatomic,assign) BOOL isFromHomeFilter;
@property (weak, nonatomic) IBOutlet UIButton *btnSort;
@property (weak, nonatomic) IBOutlet UIButton *btnSettingAgain;
- (IBAction)btnSettingAgainClicked:(id)sender;

@property(nonatomic,strong)NSMutableDictionary *dictFilterParams;

@property(nonatomic,strong)NSMutableArray *arrFilterResponse;


- (IBAction)btnSortActionClicked:(UIButton *)sender;

- (IBAction)btnFilterClicked:(id)sender;

-(IBAction)btnBackClicked:(id)sender;

typedef enum{
    FILTER_HOME =108,
    FILTER_FAVOURITE,
    FILTER_ALLSERVICE,
    FILTER_NEARME,
    FILTER_SHOWMORE,
}FILTER_TYPE;

@property(nonatomic,assign)FILTER_TYPE APPLY_FILTER_TYPE;


@end
