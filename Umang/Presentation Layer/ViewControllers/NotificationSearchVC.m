//
//  NotificationSearchVC.m
//  Umang
//
//  Created by admin on 18/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "NotificationSearchVC.h"
#import "NotificationSearchCell.h"
#import "NotificationItemBO.h"
#import "UIImageView+WebCache.h"



#import "SettingsViewController.h"
#import "HelpViewController.h"
#import "SocialMediaViewController.h"
#import "NotLinkedAadharVC.h"
#import "AadharCardViewCon.h"
#import "FeedbackVC.h"
#import "SecuritySettingVC.h"
#import "UserProfileVC.h"
#import "ShowUserProfileVC.h"

#import "RateUsVCViewController.h"


#import "FAQWebVC.h"
#import "HomeDetailVC.h"


@interface NotificationSearchVC ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
{
    NSMutableArray *arrMainData;
    //NSMutableArray *allItems;
    // NSMutableArray *displayItems;
    
    __weak IBOutlet UISearchBar *searchBar;
    __weak IBOutlet UIButton *btnBack;
    __weak IBOutlet UILabel *lblTitleHeader;
    SharedManager* singleton;
    NSArray *searchResults;
    
}
@end

@implementation NotificationSearchVC
@synthesize vwTextBG;


- (void) showKeyboard
{
    [searchBar becomeFirstResponder];
}


- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    
    //  [self performSelector:@selector(showKeyboard) withObject:nil afterDelay:0.5];
    [self notiticationCellXib];
    singleton=[SharedManager sharedSingleton];
    
    searchBar.placeholder = NSLocalizedString(@"search", @"");
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:NOTIFICATION_SEARCH_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    // Do any additional setup after loading the view.
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    [self prepareTempData];
    self.navigationController.navigationBarHidden=true;
    self.tblSearchNotification.layoutMargins = UIEdgeInsetsZero;
    [self setNeedsStatusBarAppearanceUpdate];
    
    
    vwTextBG.layer.borderWidth = 0.5;
    vwTextBG.layer.cornerRadius = 5.0;
    vwTextBG.layer.borderColor = [UIColor colorWithRed:168.0/255.0 green:168.0/255.0 blue:168.0/255.0 alpha:1.0].CGColor;
    lblTitleHeader.text = NSLocalizedString(@"search_withoutDot", @"");
    
    UIView *vwLineHide = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 0.5)];
    [self.tblSearchNotification addSubview:vwLineHide];
    vwLineHide.backgroundColor = [UIColor whiteColor];
    
    self.tblSearchNotification.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.view bringSubviewToFront:searchBar];
    searchBar.placeholder = NSLocalizedString(@"search", @"");
    // Do any additional setup after loading the view.
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
    [searchBar setShowsCancelButton:YES animated:YES];
    UIButton *cancelButton;
    UIView *topView = searchBar.subviews[0];
    for (UIView *subView in topView.subviews) {
        if ([subView isKindOfClass:NSClassFromString(@"UINavigationButton")]) {
            cancelButton = (UIButton*)subView;
        }
    }
    if (cancelButton) {
        [cancelButton setTitle:NSLocalizedString(@"cancel", @"") forState:UIControlStateNormal];
    }
    
}
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}


- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    if (searchText.length > 0) {
        //  NSPredicate *title = [NSPredicate predicateWithFormat:@"(NOTIF_TITLE contains[c] %@) || (NOTIF_MSG contains[c] %@)", searchText];
        NSPredicate *p1 = [NSPredicate predicateWithFormat:@"NOTIF_TITLE contains[c] %@", searchText];
        NSPredicate *p2 = [NSPredicate predicateWithFormat:@"NOTIF_MSG contains[c] %@", searchText];
        
        //======= new check
        NSPredicate *p3 = [NSPredicate predicateWithFormat:@"SERVICE_NAME contains[c] %@", searchText];

        NSPredicate *p4 = [NSPredicate predicateWithFormat:@"DEPT_NAME contains[c] %@", searchText];

        //NSPredicate *placesPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[p1, p2]];
        
        NSPredicate *placesPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[p1,p2,p3,p4]];

        
        
        //    NSPredicate *msg = [NSPredicate predicateWithFormat:@"(NOTIF_TITLE contains[c] %@) OR (NOTIF_MSG contains[c] %@)", searchText];
        //    NSPredicate *key = [NSPredicate predicateWithFormat:@"(NOTIF_TITLE contains[c] %@) OR (NOTIF_MSG contains[c] %@)", searchText];
        
        //NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"%K CONTAINS[c] %@", searchText];
        searchResults = [arrMainData filteredArrayUsingPredicate:placesPredicate];
    }
    [self.tblSearchNotification reloadData];
}
/*{
 NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"NOTIF_TITLE BEGINSWITH[c] %@", searchText];
 searchResults = [arrMainData filteredArrayUsingPredicate:resultPredicate];
 [self.tblSearchNotification reloadData];
 
 }*/


- (IBAction)btnCancelClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

-(void)prepareTempData
{
    
    //==== fix lint---
    // NSMutableArray * arrTableData = [[NSMutableArray alloc]init];
    // arrTableData  = [[singleton.dbManager getNotifData] mutableCopy];
    // NSMutableArray *arrTableData = (NSMutableArray *)[[singleton.dbManager getNotifData] mutableCopy];
    
    arrMainData= [[NSMutableArray alloc]init];
    //arrMainData= [[singleton.dbManager getNotifData] mutableCopy];
    arrMainData= [[singleton.dbManager getNotifData:singleton.user_id] mutableCopy];
    
    [self.tblSearchNotification reloadData];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [self setViewFont];
    [super viewWillAppear:YES];
    
}

#pragma mark- Font Set to View
-(void)setViewFont
{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    lblTitleHeader.font = [AppFont semiBoldFont:17.0];
}
#pragma mark -
-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *strMsg = @"";
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        strMsg = [[searchResults objectAtIndex:indexPath.row] valueForKey:@"NOTIF_MSG"];
    }else {
        strMsg = [[arrMainData objectAtIndex:indexPath.row] valueForKey:@"NOTIF_MSG"];
    }
    CGRect size = [self rectForText:strMsg usingFont:[UIFont systemFontOfSize:14 weight:UIFontWeightLight] boundedBySize:CGSizeMake(fDeviceWidth, 0)];
    
    return size.size.height + 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.001;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        if (searchResults.count == 0) {
        for (UIView *view in tableView.subviews) {
            if ([view isKindOfClass:[UILabel class]]) {
                ((UILabel *)view).text = NSLocalizedString(@"no_result_found", @"");
                }
            }
        }
        return [searchResults count];
        
    } else {
        return [arrMainData count];
    }
}



-(void)notiticationCellXib

{
    UINib *nib = [UINib nibWithNibName:@"NotificationSearchCellXIB" bundle:nil];
    [[self tblSearchNotification] registerNib:nib forCellReuseIdentifier:@"NotificationSearchCellXIB"];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    static NSString *CellIdentifier = @"NotificationSearchCellXIB";
    NotificationSearchCell *cell = (NotificationSearchCell *)[self.tblSearchNotification dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    if (cell == nil) {
        [self notiticationCellXib];
        cell = (NotificationSearchCell *)[self.tblSearchNotification dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        //cell = [[NotificationSearchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSArray *arrData = tableView == self.searchDisplayController.searchResultsTableView ?  searchResults : arrMainData;
    cell.searchHeader.text =  [[arrData objectAtIndex:indexPath.row] valueForKey:@"NOTIF_TITLE"];
    cell.searchDescription.text = [[arrData objectAtIndex:indexPath.row] valueForKey:@"NOTIF_MSG"];
    
    
    // cell.searchDate.text = [[arrData objectAtIndex:indexPath.row] valueForKey:@"NOTIF_RECEIVE_DATE_TIME"];
    
    NSString *dbDateStr = [NSString stringWithFormat:@"%@",[[arrData objectAtIndex:indexPath.row] valueForKey:@"NOTIF_RECEIVE_DATE_TIME"]];
    
    // Convert string to date with dd/MM/yyyy HH:mm:ss formate
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    NSDate *Dbdate = [dateFormat dateFromString:dbDateStr];
    NSLog(@"Database date from string %@",Dbdate);
    
    // Convert Dbdate object to desired output format
    [dateFormat setDateFormat:@"dd MMM YYYY HH:mm"];
    NSString* dateStr = [dateFormat stringFromDate:Dbdate];
    NSLog(@"dateStr %@",dateStr);
    cell.searchDate.text=dateStr;
    
    
    
    NSURL *url=[NSURL URLWithString:[[arrData objectAtIndex:indexPath.row] valueForKey:@"NOTIF_IMG"]];
    
    // [cell.searchImage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"logo.png"]];
    
    [cell.searchImage sd_setImageWithURL:url
                        placeholderImage:[UIImage imageNamed:@"umang_new_logo"]];
    
    
    cell.searchPDImg.backgroundColor = [UIColor clearColor];
    cell.layoutMargins = UIEdgeInsetsZero;
    
    NSString *type=[[arrData objectAtIndex:indexPath.row] valueForKey:@"NOTIF_TYPE"];
    
    if ([type isEqualToString:@"promo"]||[type isEqualToString:NSLocalizedString(@"promo", nil)]||[type isEqualToString:@"Promo"])
    {
        cell.searchPDImg.image = [UIImage imageNamed:@"promo"];
    }
    else{
        cell.searchPDImg.image = nil;
    }
    
    cell.searchHeader.font = [AppFont regularFont:16.0];
    cell.searchDescription.font = [AppFont lightFont:14.0];
    cell.searchDate.font = [AppFont semiBoldFont:13.0];
    
    // [cell resetFramesForPannedArea];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //--- fix lint----
    // NSMutableDictionary *notifyData=[NSMutableDictionary new];
    NSMutableDictionary *notifyData;//=[NSMutableDictionary new];
    
    if (self.searchDisplayController.active)
    {
        indexPath = [self.searchDisplayController.searchResultsTableView indexPathForSelectedRow];
        // notifyData=[searchResults objectAtIndex:indexPath.row];
        notifyData = (NSMutableDictionary *)[searchResults objectAtIndex:indexPath.row];
        
    } else {
        indexPath = [self.tblSearchNotification indexPathForSelectedRow];
        // notifyData=[arrMainData objectAtIndex:indexPath.row];
        notifyData = (NSMutableDictionary *)[arrMainData objectAtIndex:indexPath.row];
        
    }
    
    
    
    
    
    [self selectedIndexNotify:notifyData];
    
    
}




-(void)selectedIndexNotify:(NSDictionary*)notifyData
{
    //----------Handle case for subType---------------
    NSString *subType =[notifyData valueForKey:@"NOTIF_SUB_TYPE"];
    //subType=[subType  lowercaseString];
    // Case to open app  for opening app with notification title/
    if([subType isEqualToString:@"openApp"])
    {
        //handle case in delegate for it do nothing
        
    }
    // Case to open app  with dialog message dialogmsg/msg
    
    else if([subType isEqualToString:@"openAppWithDialog"])
    {
        NSString *title =[notifyData valueForKey:@"NOTIF_TITLE"];
        NSString *dialogMsg =[notifyData valueForKey:@"NOTIF_DIALOG_MSG"];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:title message:dialogMsg delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
        [alert show];
        
    }
    // Case to open  playstore url in external
    
    else if([subType isEqualToString:@"playstore"])
    {
        NSString *url=[NSString stringWithFormat:@"%@",[notifyData valueForKey:@"NOTIF_URL"]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        
        
        
    }
    // Case to open app  inside webview with custom webview title
    
    else if([subType isEqualToString:@"webview"])
    {
        NSString *title =[notifyData valueForKey:@"NOTIF_WEBPAGE_TITLE"];
        NSString *url=[NSString stringWithFormat:@"%@",[notifyData valueForKey:@"NOTIF_URL"]];
        // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        [self openFAQWebVC:url withTitle:title];
        
    }
    // Case to open app  in mobile browser
    
    else if([subType isEqualToString:@"browser"])
    {
        NSString *url=[NSString stringWithFormat:@"%@",[notifyData valueForKey:@"NOTIF_URL"]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        
    }
    // Case to open app  with Screen Name like profile/settings etc
    
    else if([subType isEqualToString:@"openAppWithScreen"])
    {
        NSString *screenName=[NSString stringWithFormat:@"%@",[notifyData valueForKey:@"NOTIF_SCREEN_NAME"]];
        
        screenName=[screenName lowercaseString];
        
        if ([screenName isEqualToString:@"settings"])
        {
            [self mySetting_Action];
            
        }
        if ([screenName isEqualToString:@"help"])
        {
            [self myHelp_Action];
            
        }
        if ([screenName isEqualToString:@"social"])
        {
            [self socialMediaAccount];
            
        }
        if ([screenName isEqualToString:@"aadhaar"])
        {
            if (singleton.objUserProfile.objAadhar.aadhar_number.length)
            {
                
                [self AadharCardViewCon];
                
            }
            else
            {
                [self NotLinkedAadharVC];
                
            }
        }
        if ([screenName isEqualToString:@"feedback"])
        {
            [self FeedbackVC];
            
        }
        if ([screenName isEqualToString:@"accountsettings"])
        {
            [self accountSettingAction];
        }
        if ([screenName isEqualToString:@"myprofile"])
        {
            [self myProfile_Action];
            
        }
        else
        {
            //main
        }
        
        
    }
    // Case to open app  with tab name
    
    else if([subType isEqualToString:@"openAppWithTab"])
    {
        NSString *screenName=[NSString stringWithFormat:@"%@",[notifyData valueForKey:@"NOTIF_SCREEN_NAME"]];
        
        screenName=[screenName lowercaseString];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
        
        
        if ([screenName isEqualToString:NSLocalizedString(@"home_small", nil)])
        {
            tbc.selectedIndex=0;
            
        }
        if ([screenName isEqualToString:NSLocalizedString(@"favourites_small", nil)])
        {
            tbc.selectedIndex=1;
            
        }
        if ([screenName isEqualToString:NSLocalizedString(@"states", nil)])
        {
            //tbc.selectedIndex=0;ignore case
            
        }
        if ([screenName isEqualToString:NSLocalizedString(@"all_services_small", nil)])
        {
            tbc.selectedIndex=2;
            
        }
        
        
        [self presentViewController:tbc animated:NO completion:nil];
        
        
        
    }
    // Case to open app with service
    
    else  if([subType isEqualToString:@"service"])
    {
        
        if (notifyData)
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            HomeDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
            [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            vc.dic_serviceInfo=notifyData;
            vc.tagComeFrom=@"NOTIFICATIONVIEW";
            [self presentViewController:vc animated:NO completion:nil];
            
            
        }
        
    }
    // Case to open app  for rating view
    
    else  if([subType isEqualToString:@"rating"] || [subType isEqualToString:NSLocalizedString(@"Rating", nil)])
    {
        [self rateUsClicked];
        
    }
    // Case to open app  for share [sharing message will recieve inside api)
    else  if([subType isEqualToString:@"share"] || [subType isEqualToString:NSLocalizedString(@"share", nil)])
    {
        [self shareContent];
    }
    //Default case
    else
    {
        
        
    }
    
    
}

-(void)shareContent
{
    NSString *textToShare =singleton.shareText;
    
    NSArray *objectsToShare = @[textToShare];
    
    [[UIPasteboard generalPasteboard] setString:textToShare];
    
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    //if iPhone
    
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        [self presentViewController:controller animated:YES completion:nil];
        
    }
    
    //if iPad
    
    else {
        
        // Change Rect to position Popover
        
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:controller];
        
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    }
    
}

-(void)openFAQWebVC:(NSString *)url withTitle:(NSString*)title
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    vc.urltoOpen=url;
    vc.titleOpen=title;
    
    //[vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}
-(void)rateUsClicked
{
    NSLog(@"My Help Action");
    // UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    RateUsVCViewController  *vc = [storyboard instantiateViewControllerWithIdentifier:@"RateUsVCViewController"];
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}

-(void)NotLinkedAadharVC
{
    // SettingsViewController
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NotLinkedAadharVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"NotLinkedAadharVC"];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)AadharCardViewCon
{
    // SettingsViewController
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AadharCardViewCon *vc = [storyboard instantiateViewControllerWithIdentifier:@"AadharCardViewCon"];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)FeedbackVC
{
    
    // SettingsViewController
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    FeedbackVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FeedbackVC"];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}
-(void)socialMediaAccount
{
    
    // SettingsViewController
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SocialMediaViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SocialMediaViewController"];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}
-(void)myProfile_Action
{
    NSLog(@"My Profile Action");
    
    
    
    
    // SettingsViewController
    //    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kMainStoryBoard bundle:nil];
    //
    //    UserProfileVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    
    ShowUserProfileVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ShowUserProfileVC"];
    
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)mySetting_Action
{
    NSLog(@"My Setting Action");
    
    // SettingsViewController
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SettingsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    
    
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
}
-(void)myHelp_Action
{
    NSLog(@"My Help Action");
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    HelpViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}
-(void)accountSettingAction
{
    NSLog(@"My account setting Action");
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SecuritySettingVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"SecuritySettingVC"];
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}


#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end

