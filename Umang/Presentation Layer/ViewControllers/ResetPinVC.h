//
//  ResetPinVC.h
//  Umang
//
//  Created by admin on 23/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ResetPinVC : UIViewController

{
    SharedManager *singleton;
}
@property (retain, nonatomic) NSString  *otpstr;

@property (weak, nonatomic) IBOutlet UIImageView *img_error;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UILabel *lbl_error;


@property (nonatomic,strong)NSMutableArray * userQuestionArray;

@property (nonatomic,copy)NSString *comingFrom;

@end
