//

//  TransactionalHistoryVC.m

//  Umang

//

//  Created by admin on 10/01/17.

//  Copyright © 2017 SpiceDigital. All rights reserved.

//


#import "TransactionalSearchVC.h"
#import "TransactionalHistoryVC.h"
#import "TxnHistoryFilterVC.h"
#import "TransactionCell.h"
#import "MBProgressHUD.h"
#import "UMAPIManager.h"
#import "UIImageView+WebCache.h"
#import "TxnDetailVC.h"
#import "AppConstants.h"

@interface TransactionalHistoryVC ()  <UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>

{
    
    
    NSMutableArray *responseTransactionHistory;
    
    __weak IBOutlet UILabel *lblHeaderTransHis;
    
    __weak IBOutlet UIButton *btnMoreBack;
    __weak IBOutlet UILabel *lblNoTransaction;
    MBProgressHUD *hud;
    
    SharedManager *singleton ;
    
}
@property (nonatomic,retain)NSString *pageValue;
@property (nonatomic,strong)NSMutableArray *arrTransactionHistory;

@end



@implementation TransactionalHistoryVC

@synthesize pageValue;

- (void)viewDidLoad
{
    singleton = [SharedManager sharedSingleton];

    [super viewDidLoad];
    
    pageValue=@"1";
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: TRANSACTIONAL_HISTORY_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     
     [[GAIDictionaryBuilder createScreenView] build]];
    
    _tblTransactionHistory.hidden=FALSE;
    vw_noresults.hidden=TRUE;
    lblNoTransaction.text = NSLocalizedString(@"no_transaction_found", nil);
    lblHeaderTransHis.text = NSLocalizedString(@"transaction_history", nil);
    [btnMoreBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnMoreBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnMoreBack.frame.origin.x, btnMoreBack.frame.origin.y, btnMoreBack.frame.size.width, btnMoreBack.frame.size.height);
        
        [btnMoreBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnMoreBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    self.arrTransactionHistory = [[NSMutableArray alloc]init];
    responseTransactionHistory = [[NSMutableArray alloc]init];
    [self hitAPIForTransaction];
    
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
    
    // Do any additional setup after loading the view.
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (IBAction)btnMoreClicked:(id)sender
{
    [singleton traceEvents:@"More Button" withAction:@"Clicked" withLabel:@"Transactional History" andValue:0];

    [self.navigationController popViewControllerAnimated:YES];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.shouldRotate = NO;
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    [self setViewFont];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    NSLog(@"%@",self.arrTransactionHistory);
    
    [super viewWillAppear:YES];
}

#pragma mark- Font Set to View
-(void)setViewFont{
    [btnMoreBack.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    lblHeaderTransHis.font = [AppFont semiBoldFont:17.0];
    lbl_noresult.font = [AppFont regularFont:14.0];
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}


- (IBAction)btnFilterClicked:(id)sender

{
    [singleton traceEvents:@"Filter Button" withAction:@"Clicked" withLabel:@"Transactional History" andValue:0];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    TxnHistoryFilterVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"TxnHistoryFilterVC"];
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}

-(IBAction)btnSearchClicked:(id)sender
{
    [singleton traceEvents:@"Search Button" withAction:@"Clicked" withLabel:@"Transactional History" andValue:0];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    TransactionalSearchVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"TransactionalSearchVC"];
    vc.arrTransactionSearch = [self.arrTransactionHistory mutableCopy];
    [self.navigationController pushViewController:vc animated:YES];
    
}






- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section

{
    
    return 0.001f;
    
}







- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section

{
    
    
    
    return 0.001;
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    //Here, for each section, you must return the number of rows it will contain
    
    
    
    return self.arrTransactionHistory.count;
    
}





- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    return 94;
    
    
    
}







-(void)hitAPIForTransaction

{
    SharedManager *singleton = [SharedManager sharedSingleton];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Set the label text.
    
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    
    [dictBody setObject:@"" forKey:@"source"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:@"" forKey:@"peml"];
    [dictBody setObject:@"" forKey:@"lang"];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];
    [dictBody setObject:pageValue forKey:@"page"];
    [dictBody setObject:@"date_time" forKey:@"sortby"];
    
    /*
     {
     
     "source": "",
     "imsi": "",
     "did": "",
     "imei": "",
     "hmk": "",
     "hmd": "",
     "os": "",
     "pkg": "",
     "ver": "",
     "tkn": "",
     "rot": "",
     "mod": "",
     "mcc": "",
     "mnc": "",
     "lac": "",
     "clid": "",
     "acc": "",
     "lat": "",
     "lon": "",
     "peml": "",
     "lang": "en",
     "mno": ""
     }
     
     }
     
     */
    
    
    
    
    
    
    //Type for which OTP to be intiate eg register,login,forgot mpin
    
    NSLog(@"Dict body is :%@",dictBody);
    
    
    
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_FETCH_TRANSACTION_LOGS withBody:dictBody andTag:TAG_REQUEST_USERSERVICETANSLOGS completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        [hud hideAnimated:YES];
        
        
        
        if (error == nil) {
            
            NSLog(@"Server Response = %@",response);
            
            
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            
            
            //  NSString *rc=[response valueForKey:@"rc"];
            
            //  NSString *rs=[response valueForKey:@"rs"];
            
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            // NSString *rd=[response valueForKey:@"rd"];
            NSString *pages=[[response valueForKey:@"pd"] valueForKey:@"page"];
            @try {
                NSArray *pageArray = [pages componentsSeparatedByString:@"|"];
                
                NSString*  temppage =[pageArray objectAtIndex:0];
                if ([temppage isEqualToString:@"0"])
                {
                    //do nothing
                    pageValue=[pageArray objectAtIndex:0];
                    
                }
                else if ([temppage isEqualToString:@"1"])
                {
                    pageValue=[pageArray objectAtIndex:1];
                    
                    
                }
                
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
            
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
                
                
            {
                
                // arrTransactionHistory=[[response valueForKey:@"pd"] valueForKey:@"fetchUserService"];
                //  arrTransactionHistory=[[response valueForKey:@"pd"] valueForKey:@"fetchUserService"];
                
                // [arrTransactionHistory removeAllObjects];
                
                [responseTransactionHistory removeAllObjects];
                [responseTransactionHistory addObjectsFromArray:[[response valueForKey:@"pd"] valueForKey:@"fetchUserService"]];
                
                
                for (int i=0; i<[responseTransactionHistory count]; i++)
                {
                    [self.arrTransactionHistory addObject:[responseTransactionHistory objectAtIndex:i]];
                }
                
                
                //
                
                //[self sortArrayBasedOnDate];
                
                
                [_tblTransactionHistory reloadData];
                
                //   page = "0|1";
                
                
                //page 0|1
                
                //  [self alertwithMsg:rd];
                
            }
            
            
        }
        
        else{
            
            NSLog(@"Error Occured = %@",error.localizedDescription);
            
            
        }
        
        
        if ([self.arrTransactionHistory count]<=0) {
            _tblTransactionHistory.hidden=TRUE;
            vw_noresults.hidden=FALSE;
        }
        else
        {
            _tblTransactionHistory.hidden=FALSE;
            vw_noresults.hidden=TRUE;
            
        }
        
    }];
    
}

-(void)sortArrayBasedOnDate
{
    
    NSArray *arrSortedItems = nil;
    arrSortedItems = [self.arrTransactionHistory sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        
        NSString *ldate1 = [obj1 valueForKey:@"ldate"];
        if ([ldate1 length]>10) {
            ldate1 = [ldate1 substringToIndex:16];
        }
        
        NSString *ldate2 = [obj2 valueForKey:@"ldate"];
        if ([ldate2 length]>10) {
            ldate2 = [ldate2 substringToIndex:16];
        }
        
        NSLog(@"Date 1= %@ and Date2 = %@",ldate1,ldate2);
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        // [df setDateFormat:@"dd-MM-yyyy HH:mm"];
        
        [df setDateFormat:@"yyyy-MM-dd HH:mm"];
        NSDate *d1 = [df dateFromString:ldate1];
        NSDate *d2 = [df dateFromString:ldate2];
        //2017-04-11 15:1
        df = nil;
        
        // return [d1 compare: d2];
        return [d2 compare: d1];
        
    }];
    
    
    
    
    [self.arrTransactionHistory removeAllObjects];
    [self.arrTransactionHistory addObjectsFromArray:arrSortedItems];
    [_tblTransactionHistory reloadData];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    static NSString *simpleTableIdentifier = @"TransactionCell";
    
    
    
    TransactionCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    
    
    if (cell == nil) {
        
        cell = [[TransactionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    /*
     
     "dept_resp" = "Appointment Booked Successfully";
     deptid = 402;
     deptname = Health;
     event = "Appointment Booked";
     image = "https://static.umang.gov.in/app/ico/service/ors.png";
     ldate = "2017-01-10 20:47:21.299271";
     sdltid = 17011008000100439;
     servicename = ORS;
     srid = 11;
     status = S;
     tdatezone = "2017-01-10 20:47:21.299271";
     uid = 5796;
     },
     {
     "dept_resp" = "Candidate Added Successfully";
     deptid = 407;
     deptname = Skill;
     event = "Candidate Added";
     image = "https://static.umang.gov.in/app/ico/service/pmkvy.png";
     ldate = "2017-01-12 19:58 :15.080129";
     sdltid = 1269;
     servicename = PMKVY;
     srid = 16;
     status = 1;
     tdatezone = "2017-01-12 19:58:15.080129";
     uid = 5796;
     
     */
    
    
    
    cell.headerTransaction.text = [[self.arrTransactionHistory objectAtIndex:indexPath.row] valueForKey:@"servicename"];
    cell.descriptnTransaction.text =  [[self.arrTransactionHistory objectAtIndex:indexPath.row] valueForKey:@"dept_resp"];
    
    cell.lblCategory.text= [[self.arrTransactionHistory objectAtIndex:indexPath.row] valueForKey:@"event"];
    cell.lblCategory.font =[UIFont systemFontOfSize:14.0];
    if (IS_IPAD || IS_IPAD_2 || IS_IPAD_3)
    {
        CGRect lblFrame=cell.lblCategory.frame;
        cell.lblCategory.frame=CGRectMake(lblFrame.origin.x, lblFrame.origin.y, lblFrame.size.width+100, lblFrame.size.height);
    }
    else
    {
        //cell.lblCategory.adjustsFontSizeToFitWidth = YES;
        
        if (self.view.frame.size.width == 414)
        {
            CGRect lblFrame=cell.lblCategory.frame;
            cell.lblCategory.frame=CGRectMake(lblFrame.origin.x, lblFrame.origin.y, lblFrame.size.width+70, lblFrame.size.height);
        }
        
        cell.lblCategory.adjustsFontSizeToFitWidth = NO;

    }
    cell.lblCategory.textAlignment = NSTextAlignmentLeft;
    NSString *timedate=[[self.arrTransactionHistory objectAtIndex:indexPath.row] valueForKey:@"ldate"];
    
    timedate = [timedate substringToIndex:16];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    
    
    
    
    
    
    
    NSDate *d1 = [df dateFromString:timedate];
    NSString *newDate = [df stringFromDate:d1];
    
    
    
    
    NSLog(@"New Date = %@",newDate);
    
    
    NSDate *date = [df dateFromString: newDate]; // here you can fetch date from string with define format
    
    df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd-MM-yyyy HH:mm"];
    NSString *convertedString = [df stringFromDate:date];
    cell.lblDateNtime.text=convertedString;
    cell.lblDateNtime.textAlignment = NSTextAlignmentLeft;
    cell.lblDateNtime.font = [UIFont systemFontOfSize:12.0];
    cell.lblDateNtime.adjustsFontSizeToFitWidth = YES;
    
    df = nil;
    NSURL *url=[NSURL URLWithString:[[self.arrTransactionHistory objectAtIndex:indexPath.row] valueForKey:@"image"]];
    [cell.imgTransaction sd_setImageWithURL:url
                           placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
    
    /*cell.headerTransaction.text=[[arrTransactionHistory objectAtIndex:indexPath.row] valueForKey:@"SERVICE_NAME"];
     
     cell.descriptnTransaction.text=[[arrTransactionHistory objectAtIndex:indexPath.row] valueForKey:@"SERVICE_DESC"];
     NSString *imageName= [[arrTransactionHistory objectAtIndex:indexPath.row] valueForKey:@"SERVICE_IMAGE"];
     cell.lblCategory.text=[[arrTransactionHistory  objectAtIndex:indexPath.row] valueForKey:@"SERVICE_CATEGORY"];
     NSURL *url=[NSURL URLWithString:imageName];*/
    //---------- Set business Unit color here---------------------------------
    cell.headerTransaction.font = [AppFont regularFont:16.0];
    cell.descriptnTransaction.font = [AppFont lightFont:14.0];
    cell.lblCategory.font = [AppFont regularFont:14.0];
    cell.lblDateNtime.font = [AppFont mediumFont:11.0];
    
    CGFloat catwidth=(fDeviceWidth-cell.lblDateNtime.frame.size.width-70);
    cell.lblCategory.frame=CGRectMake(68, 70,catwidth , 20);
    cell.lblCategory.lineBreakMode = NSLineBreakByTruncatingTail;

    cell.lblCategory.backgroundColor=[UIColor clearColor];

    cell.lblDateNtime.backgroundColor=[UIColor whiteColor];

    return cell;
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    //NSInteger result = maximumOffset - currentOffset;
    
    // Change 10.0 to adjust the distance from bottom
    if (maximumOffset - currentOffset <= 10.0)
    {
        if ([pageValue isEqualToString:@"0"])
        {
            //do nothing
        }
        else
        {
            [self hitAPIForTransaction];
        }
        //[self methodThatAddsDataAndReloadsTableView];
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath

{
    
    NSDictionary *celldata=[self.arrTransactionHistory objectAtIndex:indexPath.row];
    
    if (celldata)
    {
        //for open transactional name changes done
        
        [singleton traceEvents:@"Open Transactional Detail" withAction:@"Clicked" withLabel:@"Transactional History" andValue:0];

        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TxnDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"TxnDetailVC"];
        vc.srid=[celldata valueForKey:@"srid"];
        vc.sdltid=[celldata valueForKey:@"sdltid"];
        vc.dic_txnInfo=celldata;
        //[vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self.navigationController pushViewController:vc animated:YES];
        
    }
}





- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
    
}



#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*
 #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }
 */
@end

/*
 
 #pragma mark - Navigation
 
 
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 
 // Get the new view controller using [segue destinationViewController].
 
 // Pass the selected object to the new view controller.
 
 }
 
 */
