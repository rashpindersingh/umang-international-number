//
//  ResetPinVC.m
//  Umang
//
//  Created by admin on 23/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "ResetPinVC.h"
#import "MyTextField.h"
#import "UMAPIManager.h"
#import "EnterMobileOTPVC.h"
#import "ForgotMPINVC.h"

#import "TabBarVC.h"

#define kOFFSET_FOR_KEYBOARD 80.0
#import "LoginAppVC.h"

#define MAX_LENGTH 4

#import "MBProgressHUD.h"


@interface ResetPinVC ()<UITextFieldDelegate,UIScrollViewDelegate>
{
    
    
    __weak IBOutlet UILabel *lblTips;
    __weak IBOutlet UIButton *btnBack;
    NSString *mpinStr;
    NSString *CmpinStr;
    MBProgressHUD *hud ;
    IBOutlet UIScrollView *scrollview;
    
    
}

@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_titleMsg;
@property (weak, nonatomic) IBOutlet UILabel *lbl_enterMpin;

@property (weak, nonatomic) IBOutlet UITextField *txt_mpin1;
@property (weak, nonatomic) IBOutlet UITextField *txt_Cmpin1;


@property (weak, nonatomic) IBOutlet UILabel *lbl_cMpin;

@property (weak, nonatomic) IBOutlet UILabel *vw_line1;
@property (weak, nonatomic) IBOutlet UILabel *vw_line2;




@end

@implementation ResetPinVC
@synthesize txt_mpin1,txt_Cmpin1;

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

-(void)setFontforView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       if ([view isKindOfClass:[UITextField class]])
                       {
                           
                           UITextField *txtfield = (UITextField *)view;
                           NSString *fonttxtFieldName = txtfield.font.fontName;
                           CGFloat fonttxtsize =txtfield.font.pointSize;
                           txtfield.font = nil;
                           
                           txtfield.font = [UIFont fontWithName:fonttxtFieldName size:fonttxtsize];
                           
                           [txtfield layoutIfNeeded]; //Fixes iOS 9 text bounce glitch
                       }
                       
                       
                   });
    
    if ([view isKindOfClass:[UITextView class]])
    {
        
        UITextView *txtview = (UITextView *)view;
        NSString *fonttxtviewName = txtview.font.fontName;
        CGFloat fontbtnsize =txtview.font.pointSize;
        
        txtview.font = [UIFont fontWithName:fonttxtviewName size:fontbtnsize];
        
    }
    
    
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        NSString *fontName = lbl.font.fontName;
        CGFloat fontSize = lbl.font.pointSize;
        
        lbl.font = [UIFont fontWithName:fontName size:fontSize];
    }
    
    
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *button = (UIButton *)view;
        NSString *fontbtnName = button.titleLabel.font.fontName;
        CGFloat fontbtnsize = button.titleLabel.font.pointSize;
        
        [button.titleLabel setFont: [UIFont fontWithName:fontbtnName size:fontbtnsize]];
    }
    
    if (isSubViews)
    {
        
        for (UIView *sview in view.subviews)
        {
            [self setFontforView:sview andSubViews:YES];
        }
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self setViewFont];
    // [self setFontforView:self.view andSubViews:YES];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    [_btnNext.titleLabel setFont:[AppFont mediumFont:19.0]];
    _lbl_title.font = [AppFont semiBoldFont:23.0];
    _lbl_titleMsg.font = [AppFont semiBoldFont:16.0];
    _lbl_enterMpin.font = [AppFont mediumFont:16.0];
    _lbl_cMpin.font = [AppFont mediumFont:16.0];
    lblTips.font = [AppFont mediumFont:13.0];
    _lbl_error.font = [AppFont regularFont:14.0];
    txt_mpin1.font = [AppFont regularFont:21];
    txt_Cmpin1.font = [AppFont regularFont:21];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    singleton = [SharedManager sharedSingleton];
    
    [self.btnNext setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [self.btnNext setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btnNext.layer.cornerRadius = 3.0f;
    self.btnNext.clipsToBounds = YES;
    
    //NSString *str = NSLocalizedString(@"tip", nil);
    lblTips.text = NSLocalizedString(@"set_mpin_sub_heading", nil);
    
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:UPDATE_MPIN_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    _lbl_title.text = NSLocalizedString(@"updatempin_label", nil);
    _lbl_titleMsg.text = NSLocalizedString(@"set_mpin_heading", nil);
    _lbl_cMpin.text = NSLocalizedString(@"confirm_mpin", nil);
    _lbl_enterMpin.text = NSLocalizedString(@"enter_mpin_verify", nil);
    _lbl_error.text = NSLocalizedString(@"mpin_donot_match_txt", nil);
    
    
    [self textfieldInit];
    
    
    //----- Setting delegate for Custom textfield so back space operation work smooth
    txt_mpin1.delegate = self;
    txt_Cmpin1.delegate = self;
    
    
    [self textfieldInit];
    self.lbl_error.hidden=TRUE;
    self.img_error.hidden=TRUE;
    [self enableBtnNext:NO];
    self.btnNext.enabled=NO;
    
    
    
    self.view.userInteractionEnabled = YES;
    
    
    //--------- Code for handling -------------------
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       CGRect contentRect = CGRectZero;
                       for (UIView *view in scrollview.subviews)
                           contentRect = CGRectUnion(contentRect, view.frame);
                       
                       contentRect.size.height=contentRect.size.height+100;
                       scrollview.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
                   });
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
}


-(void)hideKeyboard
{
    [scrollview setContentOffset:
     CGPointMake(0, -scrollview.contentInset.top) animated:YES];
    
    [self.txt_mpin1 resignFirstResponder];
    [self.txt_Cmpin1 resignFirstResponder];
    
}

- (IBAction)btnNextClicked:(id)sender
{
    if (mpinStr.length < 4)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"enter_mpin_to_verify", nil)delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
        [alert show];
        
        
    }
    
    if (CmpinStr.length < 4)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"confirm_mpin", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
        [alert show];
        
        
    }
    if (![CmpinStr isEqualToString:mpinStr])
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"mpin_donot_match_txt", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
        [alert show];
    }
    
    
    
    
    else
    {
        [self hitAPI];
        
    }
    
    
}
- (IBAction)btnBackClicked:(id)sender
{
    BOOL checkDismiss = NO;
    
    if ([self.comingFrom isEqualToString:@"QuestionView"])
    {
        UIViewController *vc = self.presentingViewController;
        while (vc.presentingViewController)
        {
            vc = vc.presentingViewController;
            
            if ([vc isKindOfClass:[ForgotMPINVC class]])
            {
                checkDismiss = YES;
                [vc dismissViewControllerAnimated:YES completion:nil];
            }
        }
                
        if (!checkDismiss)
        {
            UIViewController *vc1 = self.presentingViewController;
            while (vc1.presentingViewController) {
                vc1 = vc1.presentingViewController;
            }
            [vc1 dismissViewControllerAnimated:YES completion:NULL];
        }
    }
    else
    {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    
}




-(void)hitAPI
{
    NSString *strSaltMPIN = SaltMPIN;//[[SharedManager sharedSingleton] getKeyWithTag:KEYCHAIN_SaltMPIN];

    NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",mpinStr,strSaltMPIN];
    NSString *mpinStrEncrypted=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
    
    
    
    
    //    NSString *encrytSHA256=[NSString stringWithFormat:@"|%@|%@|",mpinStr,SaltRequestControl];
    //    NSString *mpinStrEncrypted=[mpinStr sha256HashFor:encrytSHA256];
    //
    singleton.user_mpin=mpinStr; //save value in local db
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [dictBody setObject:mpinStrEncrypted forKey:@"mpin"];  //Type for which OTP to be intiate eg register,login,forgot mpin
    
    [dictBody setObject:self.otpstr forKey:@"otp"];  //otp
    
    [dictBody setObject:self.userQuestionArray forKey:@"quesAnsList"];
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_UPDATE_MPIN withBody:dictBody andTag:TAG_REQUEST_UPDATE_MPIN completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n value of tkn=%@ ",rc,rs,tkn);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                singleton.user_tkn=tkn;
                [self alertwithMsg:rd];
            }
            
        }
        else
        {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"error", nil) message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                           {
                                               UIViewController *vc = self.presentingViewController;
                                               while (vc.presentingViewController) {
                                                   vc = vc.presentingViewController;
                                               }
                                               [vc dismissViewControllerAnimated:YES completion:NULL];
                                           }];
            
            [alert addAction:cancelAction];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
    }];
}

-(void)alertwithMsg:(NSString*)msg
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"changempin_small_label", nil) message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                   {
                                       
                                       [self openNextView];
                                   }];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)openNextView
{
    //----later add
    [[NSUserDefaults standardUserDefaults] setInteger:kLoginScreenCase forKey:kInitiateScreenKey];
    [[NSUserDefaults standardUserDefaults] setBool:false forKey:kKeepMeLoggedIn];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //------
    
    LoginAppVC *vc;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginAppVC"];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        vc = [[LoginAppVC alloc] initWithNibName:@"LoginAppVC_iPad" bundle:nil];
    }
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.window.rootViewController = vc;
    
    [self presentViewController:vc animated:YES completion:nil];
}




-(void)textfieldInit
{
    //-----------add comment dsrawat4u-------
    //[txt_mpin1 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    // [txt_Cmpin1 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    //-----------end add comment dsrawat4u-------
    
    
}



//---Code for hangle jump from one textfield to another while filling values
- (void)textFieldDidChange:(UITextField *)textField
{
    self.lbl_error.hidden=TRUE;
    self.img_error.hidden=TRUE;
    
    [self enableBtnNext:NO];
    self.btnNext.enabled=NO;
    
    
    if (textField.text.length >= MAX_LENGTH)
    {
        
        if ([textField isEqual:self.txt_mpin1]) {
            textField.text = [textField.text substringToIndex:MAX_LENGTH];
            
            [self.txt_Cmpin1 becomeFirstResponder];
            
            [self checkValidation];
            
        }
        else if([textField isEqual:self.txt_Cmpin1]) {
            textField.text = [textField.text substringToIndex:MAX_LENGTH];
            
            [textField resignFirstResponder];
            [self checkValidation];
        }
        
    }
    
    //-----start add later dsrawat4u
    else
    {
        if (textField.text.length == 0)
        {
            
            if ([textField isEqual:self.txt_Cmpin1])
            {
                textField.text = [textField.text substringToIndex:0];
                
                [self.txt_mpin1 becomeFirstResponder];
            }
            if ([textField isEqual:self.txt_mpin1])
            {
                textField.text = [textField.text substringToIndex:0];
                
                [textField resignFirstResponder];
            }
            
            
        }
        
    }
    
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    //Replace the string manually in the textbox
    textField.text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //perform any logic here now that you are sure the textbox text has changed
    [self textFieldDidChange:textField];
    return NO; //this make iOS not to perform any action
}

//-----------end add later dsrawat4u----------


-(void)checkValidation

{
    mpinStr=[NSString stringWithFormat:@"%@",self.txt_mpin1.text];
    CmpinStr=[NSString stringWithFormat:@"%@",self.txt_Cmpin1.text];
    
    CmpinStr=[NSString stringWithFormat:@"%@",self.txt_Cmpin1.text];
    mpinStr=[NSString stringWithFormat:@"%@",self.txt_mpin1.text];
    
    if (CmpinStr.length == 4 && mpinStr.length == 4 )
    {
        
        self.btnNext.enabled=YES;
        [self enableBtnNext:YES];
    }
    else
    {
        self.btnNext.enabled=NO;
        [self enableBtnNext: NO];
    }
    
}




-(void)enableBtnNext:(BOOL)status
{
    if (status ==YES)
    {
        //[self.btnNext setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateNormal];
        //[self.btnNext setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateSelected];
        
        [self.btnNext setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];
    }
    else
    {
        //[self.btnNext setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateNormal];
        //[self.btnNext setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateSelected];
        [self.btnNext setBackgroundColor:[UIColor colorWithRed:176.0/255.0f green:176.0/255.0f blue:176.0/255.0f alpha:1.0f]];
        
    }
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (fDeviceHeight<=568) {
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.3];
        [UIView setAnimationBeginsFromCurrentState:TRUE];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -kOFFSET_FOR_KEYBOARD, self.view.frame.size.width, self.view.frame.size.height);
        
        [UIView commitAnimations];
    }
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (fDeviceHeight<=568) {
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.3];
        [UIView setAnimationBeginsFromCurrentState:TRUE];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +kOFFSET_FOR_KEYBOARD, self.view.frame.size.width, self.view.frame.size.height);
        
        [UIView commitAnimations];
    }
    
    [self setFontforView:self.view andSubViews:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (fDeviceHeight<=568) {
        UITouch * touch = [touches anyObject];
        if(touch.phase == UITouchPhaseBegan)
        {
            [self.txt_mpin1 resignFirstResponder];
            [self.txt_Cmpin1 resignFirstResponder];
            
        }
    }
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*
 #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }
 */
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
