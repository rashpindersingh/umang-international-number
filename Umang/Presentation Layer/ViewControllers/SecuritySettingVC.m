
//
//  SecuritySettingVC.m
//  Umang
//
//  Created by admin on 09/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "SecuritySettingVC.h"
#import "HelpScreenCell.h"
#import "ChangeMPINVC.h"
#import "UpdMpinVC.h"
#import "AlternateMobileVC.h"
#import "ChangeRegMobMpinVC.h"
#import "HelpAccountSetVC.h"
#import "ForgotMPINVC.h"
#import "UpdateQuestionsViewController.h"
#import "LogoutSessionVC.h"

@interface SecuritySettingVC ()<UITableViewDelegate,UITableViewDataSource>
{
    
    __weak IBOutlet UILabel *lblHeadingAccountSeeting;
    
    __weak IBOutlet UIButton *btnHelp;
    
    __weak IBOutlet UIButton *backBtnSetting;
    __weak IBOutlet UILabel *lblAccountSetting;
    UpdMpinVC *updMPinVC;
    SharedManager *singleton;
    
}
@end

@implementation SecuritySettingVC

- (void)viewDidLoad
{
    singleton = [SharedManager sharedSingleton];
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:SECURITY_SETTING_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [btnHelp setTitle:NSLocalizedString(@"help", nil) forState:UIControlStateNormal];
    [backBtnSetting setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    lblHeadingAccountSeeting.text = NSLocalizedString(@"account", nil);
    
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:backBtnSetting.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(backBtnSetting.frame.origin.x, backBtnSetting.frame.origin.y, backBtnSetting.frame.size.width, backBtnSetting.frame.size.height);
        
        [backBtnSetting setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        backBtnSetting.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    self.view.backgroundColor  = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
    _tblSecuritySetting.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
}


-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    
    [_tblSecuritySetting reloadData];
    
    [_tblSecuritySetting reloadData];
    [_tblSecuritySetting setNeedsLayout ];
    [_tblSecuritySetting layoutIfNeeded ];
    [_tblSecuritySetting reloadData];
    [self setViewFont];

    [super viewWillAppear:NO];
    
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [backBtnSetting.titleLabel setFont:[AppFont regularFont:17.0]];
    [btnHelp.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    lblHeadingAccountSeeting.font = [AppFont semiBoldFont:17];
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([cell isKindOfClass:[HelpScreenCell class]])
    {
        
        HelpScreenCell *cellAccount = (HelpScreenCell*)cell;
        
        if (singleton.isArabicSelected==TRUE)
        {
            cellAccount.imgService.transform = CGAffineTransformMakeRotation(-M_PI);
            cellAccount.lblServiceTitle.transform = CGAffineTransformMakeRotation(-M_PI);
            cellAccount.transform = CGAffineTransformMakeRotation(180*0.0174532925);
            
            //                cellAccount.img_adhgarVerified.transform= CGAffineTransformMakeRotation(180*0.0174532925);
            cellAccount.lblServiceTitle.textAlignment=NSTextAlignmentRight;//right here
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.tblSecuritySetting
             setNeedsDisplay];
            
        });
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 50.0;
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    static NSString *CellIdentifierType = @"HelpScreenCell";
    HelpScreenCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierType];
    
    
    
    
    
    switch (indexPath.row) {
        
       
        case 0:
        {
            
            cell.lblServiceTitle.text = NSLocalizedString(@"changereg_mob_label", nil);
            cell.imgService.image = [UIImage imageNamed:@"regstrd_mobile_number_SS.png"];
            cell.btnSwitch.hidden=TRUE;
            //cell.lblServiceDescription.text = NSLocalizedString(@"changemob_small_label_desc", nil);
            cell.lblServiceDescription.font=[AppFont regularFont:14.0];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
          
        }
            break;
        case 1:
        {
            ////2
            
            cell.lblServiceTitle.text =  NSLocalizedString(@"changempin_small_label", nil);
            cell.imgService.image = [UIImage imageNamed:@"change_MPIN_SS.png"];
            cell.btnSwitch.hidden=TRUE;
            //   cell.lblServiceDescription.text = NSLocalizedString(@"change_MPIN_SS", nil);
             cell.lblServiceDescription.font=[AppFont regularFont:14.0];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
            break;
        case 2:
        {
            
            cell.lblServiceTitle.text = NSLocalizedString(@"forgot_mpin", nil);
            cell.imgService.image = [UIImage imageNamed:@"forgot_mpin_SS.png"];
            cell.btnSwitch.hidden=TRUE;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            //cell.lblServiceDescription.text = NSLocalizedString(@"forgot_mpin_desc", nil);
            cell.lblServiceTitle.font=[AppFont regularFont:14.0];

        }
            break;
        case 3:
        {
            
            
            
            cell.lblServiceTitle.text = NSLocalizedString(@"update_security_ques", nil); //NSLocalizedString(@"deleteprofile_label", nil);
            cell.imgService.image = [UIImage imageNamed:@"icon_security_que.png"];
            cell.btnSwitch.hidden=TRUE;
            // cell.lblServiceDescription.text = NSLocalizedString(@"deleteprofile_small_label_desc", nil);
            //   cell.lblServiceDescription.font=[UIFont systemFontOfSize:15];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            //security_que
            
        }
            break;
            
        case 4:
        {
            cell.lblServiceTitle.text = NSLocalizedString(@"deleteprofile_label", nil);
            cell.imgService.image = [UIImage imageNamed:@"delete_profile_SS.png"];
            cell.btnSwitch.hidden=TRUE;
            // cell.lblServiceDescription.text = NSLocalizedString(@"deleteprofile_small_label_desc", nil);
            //   cell.lblServiceDescription.font=[UIFont systemFontOfSize:15];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
            break;

        case 5:
        {
            cell.lblServiceTitle.text = NSLocalizedString(@"logged_in_sessions", nil);//NSLocalizedString(@"deleteprofile_label", nil);
            cell.imgService.image = [UIImage imageNamed:@"icon_logged_in"];
            cell.btnSwitch.hidden=TRUE;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
            
            break;
        default:
            break;
    }
    cell.lblServiceTitle.font=[AppFont regularFont:17.0];

    
    return cell;
}

-(void)btnKeepMeLoginChanged:(UISwitch*)sender{
    
    [[NSUserDefaults standardUserDefaults] setBool:sender.isOn forKey:kKeepMeLoggedIn];
    if (sender.isOn) {
        [[NSUserDefaults standardUserDefaults] setValue:KEEP_LOGGED_IN_TRUE forKey:KEEP_LOGGED_IN_STATE];
    }
    else{
        [[NSUserDefaults standardUserDefaults] setValue:KEEP_LOGGED_IN_FALSE forKey:KEEP_LOGGED_IN_STATE];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)btnMoreClicked:(id)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnHelpClickedd:(id)sender
{

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HelpAccountSetVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HelpAccountSetVC"];
    //[vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self.navigationController pushViewController:vc animated:YES];
    
    //[self.navigationController popViewControllerAnimated:YES];
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //Here you must return the number of sectiosn you want
    return 1;
}


- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    return 0;
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath   *)indexPath
{
    
    switch (indexPath.row) {
        /*case 0:
        {
            
        }
            break;*/
        case 0:
        {
            
            [self openAlternateMobileVC];
            
        }
            break;
        case 1:
        {
            
            [self openChangeMpin];
            
        }
            break;
            
        case 2:
        {
            //cell.lblServiceTitle.text = @"Delete profile";
            
            [self openForgotMPIN];
            
        }
            break;
        case 3:
        {
            //cell.lblServiceTitle.text = @"Delete profile";
            
            [self openUpdateQuestions];
            
            
        }
            break;
            
        case 4:
        {
            //cell.lblServiceTitle.text = @"Delete profile";
            
            [self openDeleteProfile];
            
            
        }
            break;
            
        case 5:
        {
            //cell.lblServiceTitle.text = @"Delete profile";
            
            [self LoggedSessionAction];
            
            
        }
            break;
            
        default:
            break;
    }
    
    
    
}
-(void)openUpdateQuestions
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"DetailService" bundle:nil];
    UpdateQuestionsViewController *updateVC = [storyboard instantiateViewControllerWithIdentifier:@"UpdateQuestionController"];
    
    [self.navigationController pushViewController:updateVC animated:YES];
}

-(void)openForgotMPIN
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ForgotMPINVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ForgotMPINVC"];
    vc.strMobileNumber=singleton.mobileNumber;
    vc.tagFrom=@"INSIDESETTINGS";
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:vc animated:YES completion:nil];
    //[self.navigationController pushViewController:vc animated:YES];
    
    
}
//aditi
//delete_profile_txt2
-(void)openAlternateMobileVC
{
    
    
    UIAlertView *alertview=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"alert", nil)message:NSLocalizedString(@"change_mob_heading", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"cancel", nil),NSLocalizedString(@"btn_continue", nil), nil];
    alertview.tag=101;
    [alertview show];
    
    
}

-(void)LoggedSessionAction
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    LogoutSessionVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LogoutSessionVC"];
    [self.navigationController showViewController:vc sender:nil];
}


-(void)changeRegNumber
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ChangeRegMobMpinVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ChangeRegMobMpinVC"];
    // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self.navigationController pushViewController:vc animated:YES];
    
    
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==101) {
        if (buttonIndex == 1)
        { // handle the donate
            
            [self performSelector:@selector(changeRegNumber) withObject:nil afterDelay:0.3];
            
        }
        else
        {
            //
        }
        
    }
    if (alertView.tag==105) {
        if (buttonIndex == 1)
        { // handle the donate
            // [self displayContentController:[self getHomeDetailLayerLeftController]];
            
            [self performSelector:@selector(openDelPinview) withObject:nil afterDelay:1];
        }
        else
        {
            //
        }
        
    }
}


-(void)openDelPinview
{
    [self displayContentController:[self getHomeDetailLayerLeftController]];
    
}
-(void)openDeleteProfile
{
    
    NSString *strMsg = [NSString stringWithFormat:@"%@\n\n%@",NSLocalizedString(@"delete_profile_txt2", nil),NSLocalizedString(@"delete_profile_txt3", nil)];
    
    UIAlertView *alertview=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"delete_profile_txt1", nil)
                                                     message:strMsg delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"cancel", nil),NSLocalizedString(@"ok", nil), nil];
    alertview.tag=105;
    
    [alertview show];
    
    
}


- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

-(UpdMpinVC*)getHomeDetailLayerLeftController
{
    NSMutableDictionary *user_dic = [[NSMutableDictionary alloc]init];
    [user_dic setValue:@"DELETE" forKey:@"Tag"];
    
    //UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    //if (itemMoreVC == nil) {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    updMPinVC = [storyboard instantiateViewControllerWithIdentifier:@"UpdMpinVC"];
    //}
    
    updMPinVC.dic_info=user_dic;//change it to URL on demand
    
    
    return updMPinVC;
}


- (void) displayContentController: (UIViewController*) content;
{
    UIViewController *vc=[self topMostController];
    
    [vc addChildViewController:content];
    
    
    
    content.view.frame = CGRectMake(0, 0, vc.view.frame.size.width, vc.view.frame.size.height);
    
    [vc.view addSubview:content.view];
    [content didMoveToParentViewController:vc];
    
    [self showViewControllerFromLeftSide];
}

-(void)showViewControllerFromLeftSide{
    UIViewController *vc=[self topMostController];
    
    //add animation from left side
    [UIView animateWithDuration:0.4 animations:^{
        updMPinVC.view.frame = CGRectMake(0, 0, vc.view.frame.size.width, vc.view.frame.size.height);
    } completion:^(BOOL finished)
     {
         
     }];
}

-(void)hideViewController{
    UIViewController *vc=[self topMostController];
    
    //if (itemMoreVC == nil) {
    updMPinVC = [self getHomeDetailLayerLeftController];
    //}
    //add animation from left side
    [UIView animateWithDuration:0.4 animations:^{
        updMPinVC.view.frame = CGRectMake(vc.view.frame.size.width, 0, vc.view.frame.size.width, vc.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
    
}






-(void)openChangeMpin
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ChangeMPINVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ChangeMPINVC"];
    vc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*
 #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
