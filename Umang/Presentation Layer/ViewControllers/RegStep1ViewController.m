//
//  RegStep1ViewController.m
//  SpiceRegistration
//
//  Created by spice_digital on 21/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "RegStep1ViewController.h"
#import "RegStep2ViewController.h"
#import "UMAPIManager.h"

#import "MBProgressHUD.h"

#define MAX_LENGTH 10
#define kOFFSET_FOR_KEYBOARD 80.0

@interface RegStep1ViewController ()
{
    __weak IBOutlet UILabel *txtMsgLogintheApp;
    __weak IBOutlet UILabel *lblheaderDescription;
    MBProgressHUD *hud ;
    __weak IBOutlet UILabel *lblHeaderTitle;
    __weak IBOutlet UIButton *btnBack;
    CountryCodeView *countryView;
    IBOutlet UIScrollView *scrollView;
    
}

@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title_msg;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title_submsg;
@property (weak, nonatomic) IBOutlet UITextField *txt_mobileNo;
@property (weak, nonatomic) IBOutlet UIButton *btn_next;

- (IBAction)btn_nextAction:(id)sender;


@end

@implementation RegStep1ViewController
@synthesize tout,rtry;

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    singleton = [SharedManager sharedSingleton];
    
    [self.btn_next setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [self.btn_next setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btn_next.layer.cornerRadius = 3.0f;
    self.btn_next.clipsToBounds = YES;
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:REGISTRATION_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    txtMsgLogintheApp.text = NSLocalizedString(@"register_intro_sub_heading_hint", nil);
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    _lbl_title.text = NSLocalizedString(@"adhaar_details", nil);
    _lbl_title_msg.text = NSLocalizedString(@"aadhar_intro", nil);
    _lbl_title_submsg.text =NSLocalizedString(@"otp_on_this_number", nil);
   // [_btn_next setTitle:NSLocalizedString(@"next", @"") forState:UIControlStateNormal];
    
    self.btn_next.enabled=NO;
    self.view.userInteractionEnabled = YES;

    [_txt_mobileNo addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

    self.vwTextBG.layer.borderColor = [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0].CGColor;
    self.vwTextBG.layer.borderWidth = 2.0;
    self.vwTextBG.layer.cornerRadius = 4.0;
    
    //------ Add dismiss keyboard while background touch-------
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setViewFont];
}
#pragma mark - Country Picker Action
-(void)addCountryCodeView {
    //.hidden = true;
    NSString *lastcountrycode =  nil;
    NSString *lastcountryImg =  nil;
    
    if (countryView != nil)
    {
        lastcountrycode = countryView.lastCountryCode;
        lastcountryImg = countryView.lastCountryImg;
        [countryView removeFromSuperview];
        countryView = nil;
    }
    
    CGRect lblFrame = _vwTextBG.frame;
    lblFrame.origin.x = 25;
    lblFrame.size.width = fDeviceWidth - 50;
    //lblFrame.size.width = 80;
    countryView = [[CountryCodeView alloc] initWithGrayArrowFrame:lblFrame];
    countryView.didTapUpdateCountry = ^{
        NSLog(@"did tap country change");
    };
    countryView.btnCountryCode.titleLabel.font = [AppFont mediumFont:17.0];
    countryView.btnCountryCode.titleLabel.font = [AppFont mediumFont:22.0];
    countryView.textMobileNumber.delegate = self;
    [countryView.textMobileNumber addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    countryView.textMobileNumber.font = [AppFont mediumFont:22];
    if ( lastcountrycode != nil ) {
        [countryView updateCountryWith:lastcountrycode imageName:lastcountryImg];
    }else {
        [countryView delayUpdate];
    }
    
    [scrollView addSubview:countryView];
    //[_txt_enterId setLeftView:countryView];
    // [_txt_enterId setLeftView:countryView];
    //  [vw_login addSubview:countryView];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    [_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
    _lbl_title.font = [AppFont semiBoldFont:22.0];
    _lbl_title_msg.font = [AppFont semiBoldFont:16.0];
    _lbl_title_submsg.font = [AppFont mediumFont:14.0];
 //   lblCountryCode.font = [AppFont mediumFont:21];
    _txt_mobileNo.font = [AppFont mediumFont:21];
    txtMsgLogintheApp.font = [AppFont regularFont:14.0];
}
-(void)hideKeyboard
{
    [self.txt_mobileNo resignFirstResponder];
}
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}
- (void)textFieldDidChange:(UITextField *)textField
{
    //20 digit mobile validation
    if (textField.text.length >= 19)
    {
        textField.text = [textField.text substringToIndex:19];
    }
//    // International
//    if (textField.text.length >= MAX_LENGTH)
//    {
//        textField.text = [textField.text substringToIndex:MAX_LENGTH];
//        
//        self.btn_next.enabled=YES;
//        [self enableBtnNext:YES];
//        
//       // NSLog(@"got it");
//    }
//    else
//    {
//        self.btn_next.enabled=NO;
//        [self enableBtnNext:NO];
//    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}







/*
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
  {
   NSString* fullString = [textField.text stringByReplacingCharactersInRange:range withString:string];
   NSLog(@"textField.text.length =%lu",textField.text.length );
   NSLog(@"fullString =%@",fullString );
   if (fullString.length >= MAX_LENGTH)
      {
      _txt_mobileNo.text = [fullString substringToIndex:MAX_LENGTH];
       self.btn_next.enabled=YES;
       [self enableBtnNext:YES];
        return NO; // return NO to not change text
      }
   else
      {
        self.btn_next.enabled=NO;
        [self enableBtnNext:NO];
      }
     return YES;
   }


*/
-(void)enableBtnNext:(BOOL)status
{
    if (status ==YES)
    {
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateNormal];
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateSelected];

        [self.btn_next setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];

    }
    else
    {
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateNormal];
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateSelected];
        
        [self.btn_next setBackgroundColor:[UIColor colorWithRed:176.0/255.0f green:176.0/255.0f blue:176.0/255.0f alpha:1.0f]];
    }
    
}
- (BOOL)validatePhone:(NSString *)phoneNumber
{
    //NSString *phoneRegex = @"[789][0-9]{3}([0-9]{6})?";
   // NSString *phoneRegex =@"[6789][0-9]{9}";
   /* NSString *phoneRegex =@"^[0-9]+$";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [test evaluateWithObject:phoneNumber];
    */
    if (phoneNumber.length == 0 || phoneNumber.length > 19 )
    {
        return false;
    }
    return true;
}


- (IBAction)btn_nextAction:(id)sender
{
    //jump to next step here
    if (([self validatePhone:_txt_mobileNo.text]!=TRUE)) {
        
        NSLog(@"Wrong Mobile");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                        message:NSLocalizedString(@"enter_correct_phone_number", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                              otherButtonTitles:nil];
        [alert show];
    }
    else
    {
    [self hitAPI];
    }
}

//----- hitAPI for IVR OTP call Type registration ------
-(void)hitAPI
{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:_txt_mobileNo.text forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"sms" forKey:@"chnl"]; //chnl : type sms for OTP and IVR for call
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile contact //not supported iphone
    [dictBody setObject:@"rgtmob" forKey:@"ort"];  //Type for which OTP to be intiate eg register,login,forgot mpin
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
 hud.label.text = NSLocalizedString(@"loading",nil);    

    singleton.mobileNumber=_txt_mobileNo.text; //save mobile number for future use of user
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_REGISTRATION withBody:dictBody andTag:TAG_REQUEST_INIT_REG completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];

        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            //------ Sharding Logic parsing---------------
            NSString *node=[response valueForKey:@"node"];
            if([node length]>0)
            {
                [[NSUserDefaults standardUserDefaults] setValue:node forKey:@"NODE_KEY"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            

            
            //------ Sharding Logic parsing---------------
            

            //----- below value need to be forword to next view according to requirement after checking Android apk-----
//            NSString *man=[[response valueForKey:@"pd"] valueForKey:@"man"];
//            NSString *tmsg=[[response valueForKey:@"pd"] valueForKey:@"tmsg"];
//            NSString *wmsg=[[response valueForKey:@"pd"] valueForKey:@"wmsg"];
            
            tout=[[[response valueForKey:@"pd"] valueForKey:@"tout"] intValue];
            rtry=[[response valueForKey:@"pd"] valueForKey:@"rtry"];
            
            
            
//            NSString *rc=[response valueForKey:@"rc"];
//            NSString *rd=[response valueForKey:@"rd"];
//            NSString *rs=[response valueForKey:@"rs"];
            
        
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                [self openNextView];
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}
- (IBAction)btnBackClicked:(id)sender {
    
    [self dismissViewControllerAnimated:NO completion:nil];
}


-(void)openNextView
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    RegStep2ViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"RegStep2ViewController"];
    vc.TYPE_LOGIN_CHOOSEN=Is_From_Choose_Registration_With_Mobile;
    vc.tout=tout;
    vc.rtry=rtry;
    vc.TYPE_RESEND_OTP_FROM=ISFROMREGISTRATIONSTEP1VC;
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:vc animated:NO completion:nil];
    
    
}



-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -kOFFSET_FOR_KEYBOARD, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
    
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +kOFFSET_FOR_KEYBOARD, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch * touch = [touches anyObject];
    if(touch.phase == UITouchPhaseBegan) {
        [self.txt_mobileNo resignFirstResponder];
    }
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}
*/
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
