//
//  RegStep2ViewController.m
//  SpiceRegistration
//
//  Created by spice_digital on 21/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "RegStep2ViewController.h"
#import "RegStep3ViewController.h"
#import "TabBarVC.h"
#import "ResetPinVC.h"

#define MAX_LENGTH 1
#define kOFFSET_FOR_KEYBOARD 80.0

#import "MBProgressHUD.h"
#import <QuartzCore/QuartzCore.h>

#import "MyTextField.h"
#import "UMAPIManager.h"
#import "AadharCardViewCon.h"



@interface RegStep2ViewController ()<MyTextFieldDelegate>
{
    SharedManager *singleton;
    MBProgressHUD *hud ;
    RegStep2ViewController *vc;
    
    __weak IBOutlet UILabel *lblWaitingSMS;
    __weak IBOutlet UIButton *btnBack;
    //--- code for otp and resend handling---------
    IBOutlet UIButton *btn_resend;
    IBOutlet UIButton *btn_callme;
    NSString *retryResend;
    NSString *retryCall;
    NSString *retryAdhar;

    __weak IBOutlet UILabel *lbldidntReceiveOTP;
    __weak IBOutlet UILabel *lblEnter6Digit;
    __weak IBOutlet UILabel *lblSubHeaderDescription;
    __weak IBOutlet UILabel *lblHeaderDescription;
    __weak IBOutlet UILabel *lblHeader;
    int count;
    
}
@property (weak, nonatomic) IBOutlet UILabel *lb_rtryResend;
@property (weak, nonatomic) IBOutlet UILabel *lb_rtryCall;
@property(nonatomic,retain)NSString *mno_resend;
@property(nonatomic,retain)NSString *chnl_resend;
@property(nonatomic,retain)NSString *tkn_resend;
@property(nonatomic,retain)NSString *ort_resend;

//--- code for otp and resend handling---------


@property (nonatomic, strong) IBOutlet UIProgressView *progressView;
@property (nonatomic, strong) NSTimer *myTimer;
@property (weak, nonatomic) IBOutlet UILabel *lb_timer;

@property (weak, nonatomic) IBOutlet UIView *vw_line;
@property (weak, nonatomic) IBOutlet MyTextField *txt1;

@property (weak, nonatomic) IBOutlet UILabel *vw_line1;


@end

@implementation RegStep2ViewController
@synthesize isFromForgotMPIN,isFromLoginWithOTP,TYPE_LOGIN_CHOOSEN,strAadharNumber;

//-------- Code for resend otp and IVR---------
@synthesize altmobile;
@synthesize TAGFROM;
@synthesize rtry;
@synthesize tout;
@synthesize scrollView;
@synthesize TYPE_RESEND_OTP_FROM;


@synthesize mno_resend;
@synthesize chnl_resend;
@synthesize tkn_resend;
@synthesize ort_resend;



-(IBAction)btn_resendAction:(id)sender
{
    resendOTPview.hidden=TRUE;

    chnl_resend=@"sms";
    
    [self parameterForApiCall:chnl_resend];
}

-(IBAction)btn_callmeAction:(id)sender
{
    resendOTPview.hidden=TRUE;

    chnl_resend=@"ivr";
    [self parameterForApiCall:chnl_resend];
    
}

- (void)updateUI:(NSTimer *)timer
{
    NSLog(@"count=%d",count);
    
    
    if (count <=0)
    {
        [self.myTimer invalidate];
        self.myTimer = nil;
        resendOTPview.hidden=FALSE;
        
        
        
        
        //----------
        int rtryOtp=[retryResend intValue];
        int rtryCall=[retryCall intValue];
        int rtryAdhar=[retryAdhar intValue];
        
        if (rtryAdhar>0)
        {
            resendOTPview.hidden=FALSE;
            
        }
        else
        {
            if (rtryOtp <=0)
            {
                btn_resend.hidden=TRUE;
                self.lb_rtryResend.hidden=TRUE;
                
            }
            if (rtryCall<=0)
            {
                
                btn_callme.hidden=TRUE;
                self.lb_rtryCall.hidden=TRUE;
                
                
            }
            if (rtryOtp <=0 && rtryCall<=0)
            {
                resendOTPview.hidden=TRUE;
            }
        }
        //----------
        
        
    }
    else
    {
        
        count --;
        
        self.progressView.progress = (float)count/120.0f;
        
        self.lb_timer.text=[self timeFormatted:count];
        
    }
    
    
    
    
}


-(void)parameterForApiCall:(NSString*)chnlresend
{
    //case handle for  // AadharRegistrationVC  ForgotMPINVC  LoginAppVC // RegStep1ViewController
    if (TYPE_RESEND_OTP_FROM ==ISFROMADHARREGISTRATIONVC)
    {
        mno_resend=strAadharNumber;
        tkn_resend=@"";//singleton.user_tkn;
        ort_resend=@"rgtadhr";
        
        btn_callme.hidden=TRUE;
        
        
    }
    if (TYPE_RESEND_OTP_FROM ==ISFROMFORGOTMPINVC)
    {
        mno_resend=singleton.mobileNumber;
        tkn_resend=@"";//singleton.user_tkn;
        ort_resend=@"frgtmpn";
        
        
    }
    if (TYPE_RESEND_OTP_FROM ==ISFROMLOGINWITHOTPVC)
    {
        mno_resend=singleton.mobileNumber;
        tkn_resend=@"";//singleton.user_tkn;
        ort_resend=@"loginmob";
        
        
    }
    if (TYPE_RESEND_OTP_FROM ==ISFROMLOGINAPPVC)
    {
        mno_resend=strAadharNumber;
        tkn_resend=@"";//singleton.user_tkn;
        ort_resend=@"loginadhr";
        
        
    }
    if (TYPE_RESEND_OTP_FROM ==ISFROMREGISTRATIONSTEP1VC)
    {
        mno_resend=singleton.mobileNumber;
        tkn_resend=@"";//singleton.user_tkn;
        ort_resend=@"rgtmob";
        
        
    }
    
    [self hitAPIResendOTPwithIVR:chnlresend];
}



-(void)hitAPIResendOTPwithIVR:(NSString*)channelType
{
    NSLog(@"value of mno=%@ \n value of channel=%@ \n value of token=%@ \n value of ort=%@ \n ",mno_resend,chnl_resend,tkn_resend,ort_resend);
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"Wait...", @"Wait");
    // You can also adjust other label properties if needed.
    // hud.label.font = [UIFont italicSystemFontOfSize:16.f];
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
   // ort_resend=@"loginadhr";

    if ([ort_resend isEqualToString:@"rgtadhr"]||[ort_resend isEqualToString:@"loginadhr"]) {
       
        [dictBody setObject:@"" forKey:@"mno"];//Enter mobile number of user
        [dictBody setObject:mno_resend forKey:@"aadhr"];//Enter mobile number of user

    }
    else
    {
        
        [dictBody setObject:mno_resend forKey:@"mno"];//Enter mobile number of user

        
    }
    [dictBody setObject:chnl_resend forKey:@"chnl"]; //chnl : type sms for OTP and IVR for call
    [dictBody setObject:@"" forKey:@"lang"]; //lang
    [dictBody setObject:tkn_resend forKey:@"tkn"];  //get from mobile contact //not supported iphone
    [dictBody setObject:ort_resend forKey:@"ort"];  //Type for which OTP to be intiate eg register,login,forgot mpin
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];  //Type for which OTP to be intiate eg register,login,forgot mpin //valtmno
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_IVR_OTP withBody:dictBody andTag:TAG_REQUEST_IVR_OTP completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        
        [hud hideAnimated:YES];
        // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
        
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            //[self openNextView];
            //------ Sharding Logic parsing---------------
            NSString *node=[response valueForKey:@"node"];
            if([node length]>0)
            {
                [[NSUserDefaults standardUserDefaults] setValue:node forKey:@"NODE_KEY"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            

            
            //------ Sharding Logic parsing---------------
            
            

            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            //  NSString *rc=[response valueForKey:@"rc"];
            //  NSString *rd=[response valueForKey:@"rd"];
            //  NSString *rs=[response valueForKey:@"rs"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                //[self alertwithMsg:rd];
                // [self hitUpdateAlterMob];
                // man=[[response valueForKey:@"pd"] valueForKey:@"man"];
                rtry=[[response valueForKey:@"pd"] valueForKey:@"rtry"];
                // tmsg=[[response valueForKey:@"pd"] valueForKey:@"tmsg"];
                tout=[[[response valueForKey:@"pd"] valueForKey:@"tout"] intValue];
                NSArray *retryItems = [rtry componentsSeparatedByString:@"|"];
                
                retryResend=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:0]];
                retryCall=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:1]];
                
               // retryAdhar=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:2]];
                @try {
                    retryAdhar=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:2]];
                    
                } @catch (NSException *exception) {
                    
                } @finally {
                    
                }
                btn_resend.enabled=TRUE;
                btn_callme.enabled=TRUE;
                NSString *strRetryValue = NSLocalizedString(@"attempts_left", nil);
                
                //self.lb_rtryResend.text= [NSString stringWithFormat:@"(%@ %@)",retryAdhar,strRetryValue];
                self.lb_rtryResend.text= [NSString stringWithFormat:@"(%@ %@)",retryResend,strRetryValue];
                self.lb_rtryCall.text= [NSString stringWithFormat:@"(%@ %@)",retryCall,strRetryValue];
                
                
                int rtryOtp=[retryResend intValue];
                int rtryCall=[retryCall intValue];
                int rtryAdhar=[retryAdhar intValue];
                
                if (rtryAdhar>0)
                {
                    resendOTPview.hidden=FALSE;
 
                }
                else
                {
                if (rtryOtp <=0)
                {
                    btn_resend.enabled=false;
                }
                if (rtryCall<=0)
                {
                    btn_callme.enabled=false;
                }
                if (rtryOtp <=0 && rtryCall<=0)
                {
                    resendOTPview.hidden=TRUE;
                }
                
                }
                count =tout;// count++;
                
                // wmsg=[[response valueForKey:@"pd"] valueForKey:@"wmsg"];
                
                if ( [self.myTimer isValid]){
                    [self.myTimer invalidate], self.myTimer=nil;
                }
                
                self.myTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateUI:) userInfo:nil repeats:YES];
                
                [[NSRunLoop mainRunLoop] addTimer:_myTimer forMode:NSRunLoopCommonModes];

                
            }
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}






- (void)viewDidLoad {
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:REGISTRATION_OTP_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    [self.btnNext setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [self.btnNext setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btnNext.layer.cornerRadius = 3.0f;
    self.btnNext.clipsToBounds = YES;
    [self enableBtnNext:NO];
    tkn_resend=@"";
    btn_resend.alpha = 1.0;
    btn_callme.alpha = 1.0;
    
    //-------- Code for resend otp and IVR---------
    
    lblWaitingSMS.text = NSLocalizedString(@"waiting_for_sms", nil);

    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    lbldidntReceiveOTP.text = NSLocalizedString(@"didnt_receive_otp", nil);lblEnter6Digit.text  = NSLocalizedString(@"enter_6_digit_otp", nil);
    lblSubHeaderDescription.text = NSLocalizedString(@"register_verify_otp_sub_heading", nil);
    lblHeaderDescription.text = NSLocalizedString(@"register_verify_otp_heading", nil);
    
    lblHeader.text = NSLocalizedString(@"verify_using_otp", nil);
    [btn_resend setTitle:NSLocalizedString(@"resend_otp", nil) forState:UIControlStateNormal];
     [btn_callme setTitle:NSLocalizedString(@"call_me", nil) forState:UIControlStateNormal];
    
    
    singleton = [SharedManager sharedSingleton];
    singleton.user_aadhar_number = self.strAadharNumber;
    
    
    //----- Setting delegate for Custom textfield so back space operation work smooth
    _txt1.myDelegate = self;
  
    
    
    //self.vw_line.frame=CGRectMake(53, 492, 268, 0.5);
    self.view.userInteractionEnabled = YES;
    
    
    
    
    
    //[self textfieldInit];
    
    self.vw_line1.backgroundColor=[UIColor lightGrayColor];
   
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //------ Add dismiss keyboard while background touch-------
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [self.scrollView addGestureRecognizer:tapGesture];
    
    
    
}
-(void)hideKeyboard
{
    [self.view endEditing:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.shouldRotate = NO;
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——

    //----- code for handling ----
    
    resendOTPview.hidden=TRUE;
    NSArray *retryItems = [rtry componentsSeparatedByString:@"|"];
    
    retryResend=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:0]];
    retryCall=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:1]];
    
   // retryAdhar=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:2]];

    @try {
        retryAdhar=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:2]];
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
       
    
    NSString *strRetryValue = NSLocalizedString(@"attempts_left", nil);
    
    self.lb_rtryResend.text= [NSString stringWithFormat:@"(%@ %@)",[retryItems objectAtIndex:0],strRetryValue];
    
    
    self.lb_rtryCall.text= [NSString stringWithFormat:@"(%@ %@)",[retryItems objectAtIndex:1],strRetryValue];
    
    count =tout;// count++;
    if ( [self.myTimer isValid]){
        [self.myTimer invalidate], self.myTimer=nil;
    }
    
    self.myTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateUI:) userInfo:nil repeats:YES];
    
    [[NSRunLoop mainRunLoop] addTimer:_myTimer forMode:NSRunLoopCommonModes];

    //--------- Code for handling -------------------
    CGRect contentRect = CGRectZero;
    for (UIView *view in self.scrollView.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    self.scrollView.delegate=self;
    self.scrollView.contentSize = contentRect.size;
    self.scrollView.delegate=self;
    self.scrollView.contentSize = contentRect.size;
    [self setViewFont];
    [super viewWillAppear:NO];
}

#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    lblHeader.font = [AppFont semiBoldFont:22.0];
    _lblScreenTitleName.font = [AppFont semiBoldFont:22.0];
    lblHeaderDescription.font = [AppFont semiBoldFont:17.0];
    //lblMaskedMNumber.font = [AppFont regularFont:18.0];
    lblSubHeaderDescription.font = [AppFont mediumFont:15.0];
    _txt1.font = [AppFont regularFont:21.0];
    lblEnter6Digit.font = [AppFont mediumFont:14.0];
    _lb_timer.font = [AppFont mediumFont:14];
    lblWaitingSMS.font = [AppFont mediumFont:14];
    lbldidntReceiveOTP.font = [AppFont regularFont:16.0];
    btn_resend.titleLabel.font = [AppFont mediumFont:15.0];
    btn_callme.titleLabel.font = [AppFont mediumFont:15.0];
    _lb_rtryResend.font = [AppFont regularFont:13.0];
    _lb_rtryCall.font = [AppFont regularFont:13.0];
    
    
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}



- (IBAction)btnBackClicked:(id)sender {
    [self.myTimer invalidate];
    self.myTimer = nil;
    
    [self dismissViewControllerAnimated:NO completion:nil];
}


- (void) viewDidDisappear:(BOOL)animated
{
    [self.myTimer invalidate];
    self.myTimer = nil;
    [super viewDidDisappear:NO];
}




- (NSString *)timeFormatted:(int)totalSeconds{
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    // int hours = totalSeconds / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark UITextFieldDelegate methods


-(void)enableBtnNext:(BOOL)status
{
    self.btnNext.userInteractionEnabled = status;
    if (status ==YES)
    {
        [self hideKeyboard];
        [self.btnNext setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];
    }
    else
    {
        [self.btnNext setBackgroundColor:[UIColor colorWithRed:176.0/255.0f green:176.0/255.0f blue:176.0/255.0f alpha:1.0f]];
    }
    
}

- (IBAction)didTapNextBtnAction:(UIButton *)sender {
    if (_txt1.text.length < 6)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"enter_otp_is_not_valid", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
        [alert show];
        
    }
    else
    {
        if (TYPE_LOGIN_CHOOSEN == ISFROMLOGINWITHOTP)
        {
            [self hitloginAPI];
        }
        if (TYPE_LOGIN_CHOOSEN == ISFROMFORGOTMPIN)
        {
            [self hitAPI];
        }
        else if (TYPE_LOGIN_CHOOSEN == IS_FROM_MOBILE_NUMBER_REGISTRATION)
        {
            [self hitAPI];
        }
        
        //case1. get Otp while enter Mobile number
        
        else if (TYPE_LOGIN_CHOOSEN == Is_From_Choose_Registration_With_Mobile)
        {
            [self hitAPI];//
            
        }
        else if (TYPE_LOGIN_CHOOSEN == Link_Aadhar_After_Mobile)
        {
            [self hitAPIForLinkProfile];
            
        }
        
        else if (TYPE_LOGIN_CHOOSEN == IS_FROM_AADHAR_REGISTRATION)
        {
            [self hitAPIForAadharREGISTRATION];
        }
        else if (TYPE_LOGIN_CHOOSEN == IS_FROM_PROFILE_LINK)
        {
            [self hitAPIForLinkProfile];
        }
        
        
        else if (TYPE_LOGIN_CHOOSEN == IS_FROM_AADHAR_LOGIN)
        {
            [self hitLoginAPIForAadharAfterValidationCheck];
            
        }
        
    }
}

-(void)checkValidation
{
    [self enableBtnNext:YES];
    return;
    if (_txt1.text.length < 6)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"enter_otp_is_not_valid", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
        [alert show];
        
    }
    else
    {
        if (TYPE_LOGIN_CHOOSEN == ISFROMLOGINWITHOTP)
        {
            [self hitloginAPI];
        }
        if (TYPE_LOGIN_CHOOSEN == ISFROMFORGOTMPIN)
        {
            [self hitAPI];
        }
        else if (TYPE_LOGIN_CHOOSEN == IS_FROM_MOBILE_NUMBER_REGISTRATION)
        {
            [self hitAPI];
        }
        
        //case1. get Otp while enter Mobile number
        
        else if (TYPE_LOGIN_CHOOSEN == Is_From_Choose_Registration_With_Mobile)
        {
            [self hitAPI];//
            
        }
        else if (TYPE_LOGIN_CHOOSEN == Link_Aadhar_After_Mobile)
        {
            [self hitAPIForLinkProfile];
            
        }
        
        else if (TYPE_LOGIN_CHOOSEN == IS_FROM_AADHAR_REGISTRATION)
        {
            [self hitAPIForAadharREGISTRATION];
        }
        else if (TYPE_LOGIN_CHOOSEN == IS_FROM_PROFILE_LINK)
        {
            [self hitAPIForLinkProfile];
        }
        else if (TYPE_LOGIN_CHOOSEN == IS_FROM_AADHAR_LOGIN)
        {
            [self hitLoginAPIForAadharAfterValidationCheck];
            
        }
        
    }
    
}

-(void)hitAPIForAadharLOGIN
{
    NSString *otpString= _txt1.text;
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Set the label text.
 hud.label.text = NSLocalizedString(@"loading",nil);
    // You can also adjust other label properties if needed.
    // hud.label.font = [UIFont italicSystemFontOfSize:16.f];
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    //  [dictBody setObject:singleton.aadharNumber forKey:@"mno"];//Enter aadhar number of user
    
    
    // [dictBody setObject:@"aadharo" forKey:@"type"];
    NSLog(@"aadhar Number is = %@",self.strAadharNumber);
    [dictBody setObject:self.strAadharNumber forKey:@"aadhr"];
    [dictBody setObject:otpString forKey:@"otp"];
    [dictBody setObject:@"loginadhr" forKey:@"ort"];
    [dictBody setObject:@"Y" forKey:@"rc"];
    [dictBody setObject:@"Y" forKey:@"mec"];
    
    NSLog(@"Dictioary is %@",dictBody);
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_VALID_AADHAAR withBody:dictBody andTag:TAG_REQUEST_VALID_AADHAR completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        
        [hud hideAnimated:YES];
        // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
        
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            //------ Sharding Logic parsing---------------
            NSString *node=[response valueForKey:@"node"];
            if([node length]>0)
            {
                [[NSUserDefaults standardUserDefaults] setValue:node forKey:@"NODE_KEY"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            

            
            //------ Sharding Logic parsing---------------
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
//            NSString *rc=[response valueForKey:@"rc"];
//            NSString *rd=[response valueForKey:@"rd"];
//            NSString *rs=[response valueForKey:@"rs"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                
                [self hitLoginAPIForAadharAfterValidationCheck];
                
                
                
                
            }
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
    return;
    
}


-(void)hitLoginAPIForAadharAfterValidationCheck
{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    NSString *otpString= _txt1.text;
    
    [dictBody setObject:@"aadharo" forKey:@"type"];
    [dictBody setObject:otpString forKey:@"lid"];
    [dictBody setObject:self.strAadharNumber forKey:@"aadhr"];
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_LOGIN withBody:dictBody andTag:TAG_REQUEST_LOGIN completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
     {
         NSLog(@"dictBody is = %@",dictBody);
         
         [hud hideAnimated:YES];
         // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
         
         
         if (error == nil) {
             NSLog(@"Server Response = %@",response);
             
             
            // NSString *rc=[response valueForKey:@"rc"];
             NSString *rd=[response valueForKey:@"rd"];
             
             NSString *rs=[response valueForKey:@"rs"];
             
             NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
             
             if ([rs isEqualToString:@"SU"]||[rs isEqualToString:@"S"])
             {
                 //singleton.mobileNumber= txtAadhar.text;
                 
                 
                 //for login in pd sending General pd
                 
                 
                 singleton.objUserProfile = nil;
                 singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
                 
                 
                 //---fix lint---

               //  NSMutableArray *generalpdList=[NSMutableArray new];
               //  generalpdList=[[response valueForKey:@"pd"]valueForKey:@"aadharpd"];
                 
                // NSMutableArray *generalpdList = (NSMutableArray *)[[response valueForKey:@"pd"]valueForKey:@"aadharpd"];

                 
                 
                 
                 
                 singleton.user_tkn=tkn;
                 
                 [self alertwithMsg:rd];
                 
             }
             
         }
         
         else{
             
         }
         
         
     }];
    return;
}


-(void)hitAPIForLinkProfile
{
    NSString *otpString= _txt1.text;
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
   hud.label.text = NSLocalizedString(@"loading",nil);
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    //  [dictBody setObject:singleton.aadharNumber forKey:@"mno"];//Enter aadhar number of user
    
    
    [dictBody setObject:@"aadhar" forKey:@"type"];
    NSLog(@"aadhar Number is = %@",self.strAadharNumber);
    [dictBody setObject:self.strAadharNumber forKey:@"aadhr"];
    [dictBody setObject:otpString forKey:@"otp"];
    [dictBody setObject:@"rgtadhr" forKey:@"ort"];
    [dictBody setObject:@"Y" forKey:@"rc"];
    [dictBody setObject:@"Y" forKey:@"mec"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    
    NSLog(@"Dictioary is %@",dictBody);
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_LINK_AADHAAR withBody:dictBody andTag:TAG_REQUEST_LINK_AADHAR completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        
        [hud hideAnimated:YES];
        // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
        
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
          //  NSString *rc=[response valueForKey:@"rc"];
            NSString *rd=[response valueForKey:@"rd"];
          //  NSString *rs=[response valueForKey:@"rs"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                [self alertwithMsg:rd];
                
                singleton.objUserProfile = nil;
                singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
                

                
            }
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
    return;
    
}

-(void)hitAPIForAadharREGISTRATION
{
    NSString *otpString= _txt1.text;
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
  hud.label.text = NSLocalizedString(@"loading",nil);
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    //  [dictBody setObject:singleton.aadharNumber forKey:@"mno"];//Enter aadhar number of user
    
    
    [dictBody setObject:@"aadhar" forKey:@"type"];
    NSLog(@"aadhar Number is = %@",self.strAadharNumber);
    [dictBody setObject:self.strAadharNumber forKey:@"aadhr"];
    [dictBody setObject:otpString forKey:@"otp"];
    [dictBody setObject:@"rgtadhr" forKey:@"ort"];
    [dictBody setObject:@"Y" forKey:@"rc"];
    [dictBody setObject:@"Y" forKey:@"mec"];
    
    NSLog(@"Dictioary is %@",dictBody);
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_VALIDATE_AADHAR_OTP withBody:dictBody andTag:TAG_REQUEST_VALID_AADHAR completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        
        [hud hideAnimated:YES];
        // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
        
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
         //   NSString *rc=[response valueForKey:@"rc"];
            NSString *rd=[response valueForKey:@"rd"];
          //  NSString *rs=[response valueForKey:@"rs"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                [self alertwithMsg:rd];
                
                
                
            }
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
    return;
    
}




//----- hitAPI for IVR OTP call Type registration ------
-(void)hitloginAPI
{
    
    NSString *otpString= _txt1.text;
    
    
    NSString *encryptedInputID = @"";
    NSString *loginType = @"";
    NSString *sType = @"";
    NSString *strSaltMPIN = SaltMPIN;//[[SharedManager sharedSingleton] getKeyWithTag:KEYCHAIN_SaltMPIN];

    NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",otpString ,strSaltMPIN];
    encryptedInputID=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
    loginType = @"mobo";
    sType = @"";
    
    //---fix lint---
    NSLog(@"encryptedInputID=%@",encryptedInputID);
    
    
    
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Set the label text.
 hud.label.text = NSLocalizedString(@"loading",nil);    // You can also adjust other label properties if needed.
    // hud.label.font = [UIFont italicSystemFontOfSize:16.f];
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:loginType forKey:@"type"];
    
    
    //for Mobile with Mpin
    //[dictBody setObject:@"OTP" forKey:@"lid"];  //for Mobile with OTP
    //[dictBody setObject:@"Social ID" forKey:@"lid"]; //for Mobile with Social ID
    [dictBody setObject:otpString forKey:@"lid"];
    [dictBody setObject:sType forKey:@"stype"];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_LOGIN withBody:dictBody andTag:TAG_REQUEST_LOGIN completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        
        [hud hideAnimated:YES];
        // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
        
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
         //   NSString *rc=[response valueForKey:@"rc"];
            NSString *rd=[response valueForKey:@"rd"];
            
            NSString *rs=[response valueForKey:@"rs"];
            
            NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
            
            if ([rs isEqualToString:@"SU"]||[rs isEqualToString:@"S"]) {
                //  rd = "OTP Match & session is created successfully";

                //---fix lint---
                // NSMutableArray *generalpdList=[NSMutableArray new];
              //  generalpdList=[[response valueForKey:@"pd"]valueForKey:@"generalpd"];
                NSMutableArray *generalpdList = (NSMutableArray *)[[response valueForKey:@"pd"]valueForKey:@"generalpd"];

              //  singleton.user_id=[[response valueForKey:@"pd"]valueForKey:@"uid"];

                singleton.user_id=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"uid"];
                
                NSString *abbreviation = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"abbr"];
                
                if ([abbreviation length]==0)
                {
                    abbreviation=@"";
                }
                
                NSString *emblemString = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"stemblem"];
                emblemString = emblemString.length == 0 ? @"":emblemString;
                [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
                
                [[NSUserDefaults standardUserDefaults] setObject:abbreviation forKey:@"ABBR_KEY"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                singleton.user_StateId = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ostate"];
                [singleton setStateId:singleton.user_StateId];
                
                
                NSString *altCountryCode = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"accode"];
                altCountryCode = altCountryCode.length == 0 ? @"" : altCountryCode;
                singleton.accode = altCountryCode;
                
                NSString *countryCode = [[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ccode"];
                countryCode = countryCode.length == 0 ? @"" : countryCode;
                singleton.ccode = countryCode;
                
                //-------- Add later----------
                singleton.shared_ntft=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntft"];
                singleton.shared_ntfp=[[[response valueForKey:@"pd"] valueForKey:@"generalpd"] valueForKey:@"ntfp"];
                //-------- Add later----------
                

                
                if ([singleton.user_id length]==0) {
                    singleton.user_id=@"";
                }
                // NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                
                // [defaults setObject:singleton.user_id forKey:@"USER_ID"];
                
                // [defaults synchronize];
                
                
                //------------------------- Encrypt Value------------------------
                [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                // Encrypt
                [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_id withKey:@"USER_ID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                //------------------------- Encrypt Value------------------------
                

                
                NSString* str_name=[generalpdList valueForKey:@"nam"];
                NSString* str_mno =[generalpdList valueForKey:@"mno"];
                NSString* str_amno=[generalpdList valueForKey:@"amno"];
                NSString* str_city=[generalpdList valueForKey:@"cty"];
                NSString* str_state=[generalpdList valueForKey:@"st"];
                NSString* str_district=[generalpdList valueForKey:@"dist"];
                NSString* str_dob=[generalpdList valueForKey:@"dob"];
                NSString* str_gender=[generalpdList valueForKey:@"gndr"];
                NSString* str_pic=[generalpdList valueForKey:@"pic"];
                
                
                NSString* str_occup=[generalpdList valueForKey:@"occup"];
                NSString* str_qual=[generalpdList valueForKey:@"qual"];
                NSString* str_email=[generalpdList valueForKey:@"email"];
                
                
                
                singleton.notiTypeGenderSelected=@"";
                singleton.profileNameSelected =@"";
                singleton.profilestateSelected=@"";
                singleton.notiTypeCitySelected=@"";
                singleton.notiTypDistricteSelected=@"";
                singleton.profileDOBSelected=@"";
                singleton.altermobileNumber=@"";
                singleton.user_Qualification=@"";
                singleton.user_Occupation=@"";
                singleton.user_profile_URL=@"";
                singleton.profileEmailSelected=@"";
                
                
                
                if ([str_occup length]!=0) {
                    singleton.user_Occupation=str_occup;
                }
                
                if ([str_qual length]!=0) {
                    singleton.user_Qualification=str_qual;
                }
                if ([str_email length]!=0) {
                    singleton.profileEmailSelected=str_email;
                }
                
                
                
                
                if ([str_mno length]!=0) {
                    
                    singleton.mobileNumber=str_mno;
                    
                }
                
                
                if ([str_amno length]!=0) {
                    
                    singleton.altermobileNumber=str_amno;
                    
                }
                
                
                
                if ([str_pic length]!=0) {
                    
                    singleton.user_profile_URL=str_pic;
                    
                    //------------------------- Encrypt Value------------------------
                    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
                    // Encrypt
                    [[NSUserDefaults standardUserDefaults] encryptValue:singleton.user_profile_URL withKey:@"USER_PIC"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    //------------------------- Encrypt Value------------------------
                    

                    
                }
                
               
                
                
                if ([str_name length]!=0) {
                    str_name = [str_name stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[str_name substringToIndex:1] uppercaseString]];
                    
                    
                }
                if ([singleton.profileNameSelected length]==0) {
                    if ([str_name length]!=0) {
                        singleton.profileNameSelected=str_name;
                        
                    }
                }
                
                
                
                
                if ([singleton.notiTypeGenderSelected length]==0) {
                    if ([str_gender length]!=0) {
                        str_gender=[str_gender uppercaseString];
                        
                        if ([str_gender isEqualToString:@"M"]||[str_gender isEqualToString:@"MALE"]) {
                            singleton.notiTypeGenderSelected=@"Male";
                            
                        }
                        if ([str_gender isEqualToString:@"F"]||[str_gender isEqualToString:@"FEMALE"]){
                            singleton.notiTypeGenderSelected=@"Female";
                            
                        }
                        if ([str_gender isEqualToString:@"T"]) {
                            singleton.notiTypeGenderSelected=@"Other";
                            
                        }
                        
                        
                        
                    }
                }
                
                if ([singleton.profilestateSelected length]==0) {
                    if ([str_state length]!=0) {
                        singleton.profilestateSelected=str_state;
                        //state_id= [obj getStateCode:singleton.profilestateSelected];
                        
                    }
                }
                if ([singleton.notiTypeCitySelected length]==0) {
                    if ([str_city length]!=0) {
                        singleton.notiTypeCitySelected=str_city;
                        // city_id= [obj getStateCode:singleton.profilestateSelected];
                        
                    }
                }
                if ([singleton.notiTypDistricteSelected length]==0) {
                    if ([str_district length]!=0) {
                        singleton.notiTypDistricteSelected=str_district;
                        //district_id= [obj getStateCode:singleton.profilestateSelected];
                        
                    }
                }
                
                if ([singleton.profileDOBSelected length]==0) {
                    if ([str_dob length]!=0) {
                        singleton.profileDOBSelected=str_dob;
                        
                    }
                }
                
                
                
                singleton.user_tkn=tkn;
                
                
                [self alertwithMsg:rd];
                
            }
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}








//----- hitAPI for IVR OTP call Type registration ------
-(void)hitAPI
{
    
    NSString *otpString= _txt1.text;
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Set the label text.
   hud.label.text = NSLocalizedString(@"loading",nil);
    // You can also adjust other label properties if needed.
    // hud.label.font = [UIFont italicSystemFontOfSize:16.f];
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    
    [dictBody setObject:otpString forKey:@"otp"];//Enter OTP number by user
    
    
    [dictBody setObject:@"sms" forKey:@"chnl"]; //chnl : type sms for OTP and IVR for call
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile contact //not supported iphone
    
    
    if (TYPE_LOGIN_CHOOSEN == ISFROMFORGOTMPIN)
        
    {
        [dictBody setObject:@"frgtmpn" forKey:@"ort"];  //Type for which OTP to be intiate eg register,login,forgot mpin
        
    }
    
   
    
    
    else if (TYPE_LOGIN_CHOOSEN == IS_FROM_MOBILE_NUMBER_REGISTRATION)
    {
        [dictBody setObject:@"rgtmob" forKey:@"ort"];  //Type for which OTP to be intiate eg register,login,forgot mpin
        
    }
    
    else if (TYPE_LOGIN_CHOOSEN == Is_From_Choose_Registration_With_Mobile)
    {
        [dictBody setObject:@"rgtmob" forKey:@"ort"];  //Type for which OTP to be intiate eg register,login,forgot mpin
        
    }
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_VALIDATE_OTP withBody:dictBody andTag:TAG_REQUEST_VALIDATE_OTP completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        
        [hud hideAnimated:YES];
        // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
        
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
           // NSString *rc=[response valueForKey:@"rc"];
            NSString *rd=[response valueForKey:@"rd"];
          //  NSString *rs=[response valueForKey:@"rs"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                [self alertwithMsg:rd];
            }
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}

-(void)alertwithMsg:(NSString*)msg
{
    if (TYPE_LOGIN_CHOOSEN == IS_FROM_AADHAR_LOGIN)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

        UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
         //tbc.selectedIndex=[SharedManager getSelectedTabIndex];
        tbc.selectedIndex=0;

        [self presentViewController:tbc animated:NO completion:nil];
        
    }
    else if (TYPE_LOGIN_CHOOSEN == IS_FROM_AADHAR_REGISTRATION){
        
        [self openNextView];
    }
    
    else if (TYPE_LOGIN_CHOOSEN == Link_Aadhar_After_Mobile)
    {
        //Open New Profile View
        [self openNextView];
    }
    
    else if (TYPE_LOGIN_CHOOSEN == IS_FROM_PROFILE_LINK){
        
        [self openNextView];
    }
    
    else if (TYPE_LOGIN_CHOOSEN == ISFROMFORGOTMPIN)
    {
        [self openResetPinView];
        
    }
    else if (TYPE_LOGIN_CHOOSEN == ISFROMLOGINWITHOTP)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

        UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
        // tbc.selectedIndex=[SharedManager getSelectedTabIndex];
        tbc.selectedIndex=0;

        [self presentViewController:tbc animated:NO completion:nil];
    }
    
    else{
        [self openNextView];
    }
    
    
    /* UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"OTP Verified" message:msg preferredStyle:UIAlertControllerStyleAlert];
     UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
     {
     if (TYPE_LOGIN_CHOOSEN == IS_FROM_AADHAR_LOGIN)
     {
     UITabBarController *tbc = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
     tbc.selectedIndex=0;
     [self presentViewController:tbc animated:NO completion:nil];
     
     }
     else if (TYPE_LOGIN_CHOOSEN == IS_FROM_AADHAR_REGISTRATION){
     
     [self openNextView];
     }
     else if (TYPE_LOGIN_CHOOSEN == ISFROMFORGOTMPIN)
     {
     [self openResetPinView];
     
     }
     else if (TYPE_LOGIN_CHOOSEN == ISFROMLOGINWITHOTP)
     {
     UITabBarController *tbc = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
     tbc.selectedIndex=0;
     [self presentViewController:tbc animated:NO completion:nil];
     }
     
     else{
     [self openNextView];
     }
     
     }];
     [alert addAction:cancelAction];
     [self presentViewController:alert animated:YES completion:nil];*/
}


-(void)openResetPinView
{
    NSString *otpString= _txt1.text;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    ResetPinVC *rpvc = [storyboard instantiateViewControllerWithIdentifier:@"ResetPinVC"];
    rpvc.otpstr=otpString;
    [rpvc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:rpvc animated:NO completion:nil];

}

-(void)openNextView
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if (self.TYPE_LOGIN_CHOOSEN == IS_FROM_AADHAR_REGISTRATION)
    {
        RegStep3ViewController *RS3vc = [storyboard instantiateViewControllerWithIdentifier:@"RegStep3ViewController"];
        RS3vc.isFromAadharRegistration = YES;
        [RS3vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:RS3vc animated:NO completion:nil];
    }
    else if (self.TYPE_LOGIN_CHOOSEN == IS_FROM_PROFILE_LINK)
    {

        AadharCardViewCon *aadharLink = [storyboard instantiateViewControllerWithIdentifier:@"AadharCardViewCon"];
        //   RS3vc.isFromAadharRegistration = YES;
        [aadharLink setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        
        aadharLink.tagFrom=@"RegistrationView";
        [self presentViewController:aadharLink animated:NO completion:nil];
    }
    else
    {
        
        RegStep3ViewController *RS3vc = [storyboard instantiateViewControllerWithIdentifier:@"RegStep3ViewController"];
        RS3vc.isFromAadharRegistration = NO;
        [RS3vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:RS3vc animated:NO completion:nil];
    }
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}
 -(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (_txt1.text.length <= 6)
    {
        if (_txt1.text.length == 6)
        {
           
            [self checkValidation];
        }
        return YES;
    }
    else{
        return NO;
    }
    
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -kOFFSET_FOR_KEYBOARD, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
    
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +kOFFSET_FOR_KEYBOARD, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch * touch = [touches anyObject];
    if(touch.phase == UITouchPhaseBegan) {
        [self.txt1 resignFirstResponder];
       
        
    }
}


#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*
#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}
*/

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
