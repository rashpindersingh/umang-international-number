//
//  ShowMoreServiceVC.h
//  Umang
//
//  Created by spice_digital on 14/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowMoreServiceVC : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate>
{
    IBOutlet UIButton *btn_filter;
    
    IBOutlet UILabel *lbl_title;
    
    NSMutableArray *table_data;
    
    IBOutlet UIView *vw_line;
}
@property(nonatomic, weak) IBOutlet UICollectionView *allSer_collectionView;
@property (nonatomic,weak)IBOutlet UIButton *btn_back;


@property(nonatomic,assign)int indexUsed;

@property(nonatomic,retain)NSString *categ_title;
@property(nonatomic,retain)NSArray *arr_service;
@property(nonatomic,retain)NSString *btn_backTitle;
-(IBAction)btn_filterAction:(id)sender;


-(IBAction)backBtnAction:(id)sender;
@end
