//
//  WKPaymentViewController.h
//  
//
//  Created by Lokesh Jain on 12/02/18.
//

#import <UIKit/UIKit.h>
@import WebKit;

@protocol senddataProtocol <NSObject>

-(void)loadPreviousWebViewWithURLString:(NSString *)redirectingUrl andServicedict:(NSDictionary *)serviceDict;

@end

@interface WKPaymentViewController : UIViewController

@property (strong, nonatomic) WKWebView *webView;
@property (nonatomic,copy) NSString *urlString;
@property (strong, nonatomic) IBOutlet UIButton *backButton;

@property(nonatomic,weak)id delegate;
@property (nonatomic,strong) NSDictionary *homeDict;

@property (nonatomic,copy) NSString *paymentStateName;
@property (nonatomic,copy) NSString *paymentStateId;
@property (nonatomic,copy) NSString *paymentIsStateSelected;
@property (nonatomic,assign) BOOL paymentBool;

@end
