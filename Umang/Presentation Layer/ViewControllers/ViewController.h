//
//  ViewController.h
//  Umang
//
//  Created by spice_digital on 31/08/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UIScrollViewDelegate>
{
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIPageControl *pageControl;
    IBOutlet UIButton *btn_skip;
    IBOutlet UIButton *btn_next;
    
    
    
    IBOutlet UIButton *btn_login;
    IBOutlet UIButton *btn_signUp;
}

-(IBAction)finishPressed:(id)sender;
- (IBAction)skipPressed:(id)sender;



@property(nonatomic,assign) int page;
@property (nonatomic, retain) UIScrollView *scrollView;
@property (nonatomic, retain) UIPageControl *pageControl;

@property(nonatomic) NSInteger numberOfLines;
 
 
 - (IBAction)changePage:(id)sender;



@end
