//
//  WebTabVC.m
//  DynamicTabs
//
//  Created by admin on 06/07/17.
//  Copyright © 2017 admin. All rights reserved.
//

#import "WebTabVC.h"

@interface WebTabVC ()

@end

@implementation WebTabVC

@synthesize webUrlAction;

- (void)viewDidLoad
{
    NSLog(@"webUrlAction=%@",webUrlAction);
    //UINavigationController* morenav = self.tabBarController.moreNavigationController;
    // morenav.navigationBar.barStyle = UIStatusBarStyleLightContent;
   // morenav.navigationBar.tintColor = [UIColor colorWithRed:246/255.0 green:246/255.0 blue:246/255.0 alpha:1.0];
   // morenav.navigationBar.translucent = NO;
    
    self.view.backgroundColor= [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:15/255.0 green:68/255.0 blue:140/255.0 alpha:1.0];
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];;

    [self.tabBarController.tabBar setBarTintColor:[UIColor colorWithRed:248/255.0 green:248/255.0 blue:248/255.0 alpha:1.0]];

    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //Create a URL object.
        NSURL *url = [NSURL URLWithString:webUrlAction];
        
        //URL Requst Object
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        
        //Load the request in the UIWebView.
        [vw_web loadRequest:requestObj];
    
    });
  
    
    [super viewDidLoad];
}
-(IBAction)backBtnAtion:(id)sender
{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
