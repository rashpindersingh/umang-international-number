//
//  HelpViewController.h
//  Umang
//
//  Created by spice on 21/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewController : UIViewController
{
    IBOutlet UITableView *table_helpView;
}
@property (weak, nonatomic) IBOutlet UIButton *btnBackMore;
@property (weak, nonatomic) IBOutlet UILabel *lblHelp;
-(IBAction)backbtnAction:(id)sender;
@end
