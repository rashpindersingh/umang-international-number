//
//  UserEditVC.m
//  Umang
//
//  Created by admin on 30/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "UserEditVC.h"
#import "EditUserCell.h"

#define kOFFSET_FOR_KEYBOARD 80.0

#import "UMAPIManager.h"
#import "MBProgressHUD.h"
#import "StateList.h"

#import "EnterRegFieldVC.h"
#import "AlternateMobileVC.h"
#import "UpdMpinVC.h"

#import "AddEmailVC.h"
#import "UIImageView+WebCache.h"

#import "UIView+Toast.h"



@interface UserEditVC ()<UITextFieldDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate>
{
    NSMutableArray *arrGeneralInformation;
    NSMutableArray *arrAccountInformation;
    UIView *vwHeader;
    UIView *vwFooter;
    
    
    __weak IBOutlet UIButton *btnBack;
    EnterRegFieldVC *customVC;
    MBProgressHUD *hud ;
    
    __weak IBOutlet UILabel *lblHeaderTitle;
    UIButton*  btnDone;
    UIButton*  btncancel;
    UIDatePicker *datepicker;
    UIView*  vw_dateToolbar;
    StateList *obj;
    BOOL flag_apiHitStatus;
    NSDate *pickdate;
    UpdMpinVC *updMPinVC;
    NSString *base64Image;
    NSString *facebookId;
    NSString *googleId;
    NSString *twitterId;
    BOOL flagURL;
    
    
    
    NSString *user_name;
    NSString *user_gender;
    NSString *user_dob;
    NSString *user_quali;
    NSString *user_Occup;
    NSString *user_state;
    NSString *user_dist;
    NSString *user_address;
    NSString *user_email;
    NSString *user_altermob;
    UIImageView *imageUserProfile;
    
    CountryCodeView *countryView;

    BOOL flagRemovePic;
    
    BOOL flagEditCheck;
    UIButton *btnChangeImage;
    
}
@property (weak, nonatomic) IBOutlet UITableView *tblProfileEdit;
@property (weak, nonatomic) IBOutlet UIView *vwProfileEdit;
- (IBAction)bgTouch:(id)sender;
@property(retain,nonatomic)NSString *base64Image;
@property(retain,nonatomic)NSString *header_title_pass;
@property(retain,nonatomic)NSMutableArray *arr_table_pass;
@property(retain,nonatomic)NSString *TAG_pass;
@property(nonatomic,retain)NSMutableArray *citiesList;
@property(nonatomic,retain)NSMutableArray *districtList;
@property (nonatomic, strong)NSString *state_id;
@property (nonatomic, strong)NSString *city_id;
@property (nonatomic, strong)NSString *district_id;
@property (nonatomic, strong)NSString *old_state;
@property (nonatomic, strong)NSString *old_district;

@property (nonatomic, strong)NSString *imgURL_removeStatus;

@property (nonatomic, strong)NSString *picChangeStatus;

@property(nonatomic,retain)NSMutableArray *arr_importtype;


@end

@implementation UserEditVC
@synthesize tblProfileEdit,vwProfileEdit;

@synthesize arr_table_pass;
@synthesize header_title_pass;
@synthesize TAG_pass;

@synthesize citiesList,districtList;
@synthesize state_id,city_id,district_id;
@synthesize dic_info;
@synthesize tagFrom;
@synthesize base64Image;
@synthesize arr_importtype;
@synthesize user_img;
@synthesize picChangeStatus;
@synthesize imgURL_removeStatus;
-(void)loadDataGender
{
    
    TAG_pass=TAG_GENDER;
    header_title_pass= NSLocalizedString(@"profile_gender", nil);
    
    arr_table_pass=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"gender_male", nil),NSLocalizedString(@"gender_female", nil),NSLocalizedString(@"gender_transgender", nil), nil];
    
    [self callCustomPicker];
}


- (IBAction)bgTouch:(id)sender
{
    
    
}
-(void)btnChangeImageClicked
{
    // arr_importtype
    arr_importtype=[[NSMutableArray alloc]init];
    
    [arr_importtype addObject:NSLocalizedString(@"choose_from_gallery", nil)];
    [arr_importtype addObject:NSLocalizedString(@"take_photo", nil)];
    
    
    if ([facebookId length]>1) {
        
        
        [arr_importtype addObject:NSLocalizedString(@"import_from_fb", nil)];
        
    }
    if ([googleId length]>1)
    {
        [arr_importtype addObject:NSLocalizedString(@"import_from_google", nil)];
        
    }
    
    if ([twitterId length]>1)
    {
        [arr_importtype addObject:NSLocalizedString(@"import_from_twitter", nil)];
        
    }
    
    if ([imgURL_removeStatus length]>1) //changes here
    {
        [arr_importtype addObject:NSLocalizedString(@"remove_picture", nil)];
        
    }
    
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"choose_image_from", nil)
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    // ObjC Fast Enumeration
    for (NSString *title in arr_importtype) {
        [actionSheet addButtonWithTitle:title];
    }
    
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:NSLocalizedString(@"cancel", nil)];
    
    
    
    actionSheet.tag = 1;
    [actionSheet showInView:self.view];
    
    
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation duration:(NSTimeInterval)duration
{
    tblProfileEdit.tableHeaderView = nil;
    tblProfileEdit.tableHeaderView =  [self designHeaderView];
    tblProfileEdit.tableFooterView =  [self designFooterView];
    
    
    
    [tblProfileEdit reloadData];
    
    
}



/*- (void)textFieldDidChange:(UITextField *)textField
 {
 if (textField.text.length >= MAX_LENGTH)
 {
 textField.text = [textField.text substringToIndex:MAX_LENGTH];
 
 self.btn_next.enabled=YES;
 [self enableBtnNext:YES];
 
 // NSLog(@"got it");
 }
 else
 {
 self.btn_next.enabled=NO;
 [self enableBtnNext:NO];
 }
 }
 */

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    flagEditCheck=TRUE;
    
    
    
    if (textField.tag == 101)
    {
        // if (textField.text.length >= 50)
        if (textField.text.length >= 50 && range.length == 0)
        {
            // return NO to not change text
            textField.text = [textField.text substringToIndex:50];
            return NO;
        }else {
            NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz. "] invertedSet];
            NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
            return [string isEqualToString:filtered];
        }
        
    }
    else if (textField.tag == 201)
    {
        
        // if (textField.text.length >= 100)
        if (textField.text.length >= 200 && range.length == 0)
            
        {
            textField.text = [textField.text substringToIndex:200];
            
            return NO; // return NO to not change text
        }
        else
        {
            return YES;
        }
        
    }
    
    
    else if (textField.tag == 301)
    {
         // International
//        if (textField.text.length >= 10)
//        {
//            textField.text = [textField.text substringToIndex:10];
//
//            return NO; // return NO to not change text
//        }
        
    }
    return YES;
    
    
}

//------- Add later---------
-(void)removeEmail
{
    flagEditCheck=TRUE;
    NSLog(@"remove email and reload");
    user_email=@"";
    [tblProfileEdit reloadData];
}

-(void)removeAlternateMobile
{
    flagEditCheck=TRUE;
    NSLog(@"remove alter mobile and reload");
    user_altermob=@"";
    [tblProfileEdit reloadData];
}


- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (popup.tag==1010)
    {
        
        switch (buttonIndex)
        {
            case 0:
                [self openAddEmailView];
                break;
            case 1:
                [self removeEmail];
                break;
            default:
                break;
        }
        
    }
    else  if (popup.tag==2020)
    {
        
        switch (buttonIndex)
        {
            case 0:
                [self openAlternateMobileView];
                break;
            case 1:
                [self removeAlternateMobile];
                break;
            default:
                break;
        }
        
    }
    else
    {
        if (buttonIndex == popup.cancelButtonIndex)
        {
            
            return;
            
        }
        NSString *titletocheck=[arr_importtype objectAtIndex:buttonIndex];
        if ([titletocheck isEqualToString:NSLocalizedString(@"choose_from_gallery", nil)])
        {
            [singleton traceEvents:@"Gallary Button" withAction:@"Clicked" withLabel:@"UserProfile" andValue:0];

            [self GalleryOpen];
            flagURL=FALSE;
        }
        if ([titletocheck isEqualToString:NSLocalizedString(@"take_photo", nil)])
        {
            [singleton traceEvents:@"Camera Button" withAction:@"Clicked" withLabel:@"UserProfile" andValue:0];

            [self CameraOpen];
            flagURL=FALSE;
            
            
            
        }
        
        if ([titletocheck isEqualToString:NSLocalizedString(@"import_from_fb", nil)])
        {
            flagURL=TRUE;
            //NSLog(@"facebook=%@",facebookId);
            
            [singleton traceEvents:@"Facebook Import Button" withAction:@"Clicked" withLabel:@"UserProfile" andValue:0];

            NSURL *url = [NSURL URLWithString:facebookId];
            [imageUserProfile sd_setImageWithURL:url
                                placeholderImage:[UIImage imageNamed:@"user_placeholder"]];
            
            
            picChangeStatus=@"TRUE";
            flagEditCheck=TRUE;
            flagRemovePic=FALSE;
            imgURL_removeStatus=[NSString stringWithFormat:@"%@",url];
            
        }
        
        if ([titletocheck isEqualToString:NSLocalizedString(@"import_from_google", nil)])
        {
            [singleton traceEvents:@"Google Import Button" withAction:@"Clicked" withLabel:@"UserProfile" andValue:0];

            flagURL=TRUE;
            
            NSURL *url = [NSURL URLWithString:googleId];
            [imageUserProfile sd_setImageWithURL:url
                                placeholderImage:[UIImage imageNamed:@"user_placeholder"]];
            
            picChangeStatus=@"TRUE";
            flagEditCheck=TRUE;
            flagRemovePic=FALSE;
            imgURL_removeStatus=[NSString stringWithFormat:@"%@",url];
            
        }
        
        if ([titletocheck isEqualToString:NSLocalizedString(@"import_from_twitter", nil)])
        {
            [singleton traceEvents:@"Twitter Import Button" withAction:@"Clicked" withLabel:@"UserProfile" andValue:0];

            flagURL=TRUE;
            
            NSURL *url = [NSURL URLWithString:twitterId];
            [imageUserProfile sd_setImageWithURL:url
                                placeholderImage:[UIImage imageNamed:@"user_placeholder"]];
            
            picChangeStatus=@"TRUE";
            flagEditCheck=TRUE;
            imgURL_removeStatus=[NSString stringWithFormat:@"%@",url];
            
            flagRemovePic=FALSE;
            
            
        }
        
        
        
        if ([titletocheck isEqualToString:NSLocalizedString(@"remove_picture", nil)])
        {
            flagURL=FALSE;
            //NSLog(@"googleId=%@",googleId);
            [singleton traceEvents:@"Remove Profile Picture Button" withAction:@"Clicked" withLabel:@"UserProfile" andValue:0];

            
            if ([singleton.notiTypeGenderSelected isEqualToString:NSLocalizedString(@"gender_male", nil)]) {
                UIImage *image=[UIImage imageNamed:@"male_avatar"];
                
                imageUserProfile.image=image;
            }
            else if ([singleton.notiTypeGenderSelected isEqualToString:NSLocalizedString(@"gender_female", nil)]) {
                UIImage *image=[UIImage imageNamed:@"female_avatar"];
                imageUserProfile.image=image;
            }
            else if ([singleton.notiTypeGenderSelected isEqualToString:NSLocalizedString(@"gender_transgender", nil)]) {
                UIImage *image=[UIImage imageNamed:@"user_placeholder"];
                imageUserProfile.image=image;
            }
            else
            {
                UIImage *image=[UIImage imageNamed:@"user_placeholder"];
                imageUserProfile.image=image;
            }
            imgURL_removeStatus=@"";
            flagRemovePic=TRUE;
            picChangeStatus=@"FALSE";
            
            flagEditCheck=TRUE;
            //UIImage *image=[self makeRoundedImage:imageUserProfile.image];
            //imageUserProfile.image=image;
            NSString *tilte=NSLocalizedString(@"add_profile_picture", nil);
            [btnChangeImage setTitle:tilte forState:UIControlStateNormal];
            [btnChangeImage setTitle:tilte forState:UIControlStateSelected];
            [btnChangeImage setTitle:tilte forState:UIControlStateHighlighted];
            
        }
        // picChangeStatus=@"FALSE";
        
        imageUserProfile.clipsToBounds=YES;
        
        
        [tblProfileEdit reloadData];
        
    }
    
}


-(void)GalleryOpen
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
    dispatch_async(dispatch_get_main_queue(), ^ {
        
        
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            
            UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:imagePickerController];
            
            [popover presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
            
            
            
            
            
        } else {
            
            
            
        }
        
    });
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    
    NSString *tilte=NSLocalizedString(@"change_picture", nil);
    [btnChangeImage setTitle:tilte forState:UIControlStateNormal];
    [btnChangeImage setTitle:tilte forState:UIControlStateSelected];
    [btnChangeImage setTitle:tilte forState:UIControlStateHighlighted];
    
    //this works, image is displayed
    //UIImage *beforeCrop = [info objectForKey:UIImagePickerControllerOriginalImage];
    //this doesn't work, image is nil
    UIImage *beforeCrop = [info objectForKey:UIImagePickerControllerEditedImage];
    //self.imgView_user.image=image;
    
    CGSize destinationSize = CGSizeMake(200, 200);
    UIGraphicsBeginImageContext(destinationSize);
    [beforeCrop drawInRect:CGRectMake(0,0,destinationSize.width,destinationSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    base64Image = [self encodeToBase64String:newImage];
    
    
    
    //imageUserProfile.image=[self makeRoundedImage:newImage];
    imgURL_removeStatus=[NSString stringWithFormat:@"CameraOption"];
    
    imageUserProfile.image=newImage;
    
    imageUserProfile.clipsToBounds=YES;
    picChangeStatus=@"TRUE";
    flagEditCheck=TRUE;
    flagRemovePic=FALSE;
    
    [tblProfileEdit reloadData];
    [picker dismissViewControllerAnimated:YES completion:nil];
    //UIImage *newImage = image;
}


- (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}



-(void)CameraOpen
{
    NSString *tilte=NSLocalizedString(@"change_picture", nil);
    [btnChangeImage setTitle:tilte forState:UIControlStateNormal];
    [btnChangeImage setTitle:tilte forState:UIControlStateSelected];
    [btnChangeImage setTitle:tilte forState:UIControlStateHighlighted];
    
    
    
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    
    imagePickerController.sourceType =  UIImagePickerControllerSourceTypeCamera;
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        
        
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
            
            UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:imagePickerController];
            
            [popover presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
        }
        else
        {
            [self presentViewController:imagePickerController animated:NO completion:nil];
        }
        
    });
    
}


-(void)openDOBpicker
{
    
    
    for(UIView *subview in [self.view subviews]) {
        
        /*  if ([subview isKindOfClass:[UIButton class]]) {
         [subview removeFromSuperview];
         }*/
        if ([subview isKindOfClass:[UIDatePicker class]]) {
            [subview removeFromSuperview];
        }
        if ([subview isKindOfClass:[UIView class]]) {
            [vw_dateToolbar removeFromSuperview];
        }
        
    }
    [self.view endEditing:YES];
    
    vw_dateToolbar =[[UIView alloc]initWithFrame:CGRectMake(0, fDeviceHeight-270.0, fDeviceWidth, 100)];
    
    vw_dateToolbar.backgroundColor=[UIColor grayColor];
    [self.view addSubview:vw_dateToolbar];
    
    btncancel = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btncancel setTitle:NSLocalizedString(@"cancel", nil) forState:UIControlStateNormal];
    btncancel.titleLabel.textColor=[UIColor blueColor];
    
    btncancel.frame = CGRectMake(10.0,5.0,100.0, 40.0);
    [btncancel addTarget:self
                  action:@selector(cancelPicker:)
        forControlEvents:UIControlEventTouchUpInside];
    [vw_dateToolbar addSubview:btncancel];
    
    
    
    
    
    btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    btnDone.titleLabel.textColor=[UIColor blueColor];
    
    [btnDone setTitle:[NSLocalizedString(@"done", nil) capitalizedString] forState:UIControlStateNormal];
    btnDone.frame = CGRectMake(fDeviceWidth-110,5,100.0, 40.0);
    [btnDone addTarget:self
                action:@selector(HidePicker:)
      forControlEvents:UIControlEventTouchUpInside];
    [vw_dateToolbar addSubview:btnDone];
    
    
    datepicker= [[UIDatePicker alloc] initWithFrame:CGRectMake(0, fDeviceHeight-220, fDeviceWidth,220 )];
    datepicker.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    datepicker.datePickerMode = UIDatePickerModeDate;
    datepicker.backgroundColor = [UIColor whiteColor];
    
    
    
    NSDateComponents *components = [datepicker.calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[NSDate date]];
    
    NSInteger year = components.year;
    NSInteger month = components.month;
    NSInteger day = components.day;
    NSInteger minimumYear = year - 1900;//Given some year here for example
    NSInteger minimumMonth = month - 1;
    NSInteger minimumDay = day - 1;
    [components setYear:-minimumYear];
    [components setMonth:-minimumMonth];
    [components setDay:-minimumDay];
    NSDate *minDate = [datepicker.calendar dateByAddingComponents:components toDate:[NSDate date] options:0];
    
    [datepicker setMinimumDate:minDate];
    [datepicker setMaximumDate:[NSDate date]];
    
    [self.view addSubview:datepicker];
}

-(IBAction)cancelPicker:(id)sender
{
    [self resignFirstResponder];
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         datepicker.frame = CGRectMake(0, -250, fDeviceWidth, 50);
                     } completion:^(BOOL finished) {
                         [datepicker removeFromSuperview];
                         [btnDone removeFromSuperview];
                         [btncancel removeFromSuperview];
                         [vw_dateToolbar removeFromSuperview];
                         
                         
                     }];
    
    [datepicker removeFromSuperview];
}



-(IBAction)HidePicker:(id)sender{
    [self resignFirstResponder];
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         datepicker.frame = CGRectMake(0, -250, fDeviceWidth, 50);
                     } completion:^(BOOL finished) {
                         [datepicker removeFromSuperview];
                         [btnDone removeFromSuperview];
                         [btncancel removeFromSuperview];
                         [vw_dateToolbar removeFromSuperview];
                         
                         
                     }];
    
    [datepicker removeFromSuperview];
    
    
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd-MM-yyyy"];
    NSString *formattedDate = [df stringFromDate:[datepicker date]];
    pickdate=[datepicker date];
    //NSLog(@"formattedDate String : %@",formattedDate);
    
    
    
    NSTimeInterval secondsBetween = [ [NSDate date] timeIntervalSinceDate:pickdate];
    
    int numberOfDays = secondsBetween / 86400;
    
    //NSLog(@"There are %d days in between the two dates.", numberOfDays);
    
    
    if (numberOfDays <365)
    {
        /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
         message:NSLocalizedString(@"age_check_text", nil)                                                           delegate:self
         cancelButtonTitle:NSLocalizedString(@"ok", nil)
         otherButtonTitles:nil];
         [alert show];*/
        
        NSString *errormsg=[NSString stringWithFormat:@"%@",NSLocalizedString(@"age_check_text", nil)];
        [self showToast:errormsg];
        
        
    }
    else
    {
        singleton.profileDOBSelected= formattedDate;
        
        user_dob=formattedDate;
        flagEditCheck=TRUE;
        [tblProfileEdit reloadData];
    }
    
    // 30-11-2001
}




- (IBAction)btnGenderAction:(id)sender {
    header_title_pass= NSLocalizedString(@"profile_gender", nil);
    
    [self loadDataGender];
}
- (IBAction)btnDobAction:(id)sender {
    [self openDOBpicker];
    
}
- (IBAction)btnQualifyAction:(id)sender
{
    NSArray *qualification_arr=  [obj getQualiList];
    TAG_pass=TAG_QUALIFI_PROFILE;
    header_title_pass=NSLocalizedString(@"qualication", nil);
    
    arr_table_pass=[[NSMutableArray alloc]init];
    for (int i=0; i<[qualification_arr count]; i++) {
        [arr_table_pass addObject:[qualification_arr objectAtIndex:i]];
    }
    [self callCustomPicker];
    
}
-(void)showToast :(NSString *)toast
{
    [self.view makeToast:toast duration:5.0 position:CSToastPositionBottom];
}


- (IBAction)btnOccupAction:(id)sender
{
    
    NSArray*occup_arr=[obj getOccupList];
    occup_arr = [occup_arr sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    
    TAG_pass=TAG_OCCUPATION_PROFILE;
    header_title_pass=NSLocalizedString(@"occupation", nil);
    arr_table_pass=[[NSMutableArray alloc]init];
    for (int i=0; i<[occup_arr count]; i++) {
        [arr_table_pass addObject:[occup_arr objectAtIndex:i]];
    }
    [self callCustomPicker];
    
}
- (IBAction)btnStateAction:(id)sender
{
    [self loadDataState];
    flag_apiHitStatus=FALSE;
}
- (IBAction)btnDistrictAction:(id)sender
{
    if ([state_id length]!=0) {
        header_title_pass=NSLocalizedString(@"district", nil);
        [self loadDataDistrict];
    }
    else
    {
        /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
         message:NSLocalizedString(@"please_select_state", nil)                                                           delegate:self
         cancelButtonTitle:NSLocalizedString(@"ok", nil)
         otherButtonTitles:nil];
         [alert show];
         */
        
        NSString *errormsg=[NSString stringWithFormat:@"%@",NSLocalizedString(@"please_select_state", nil)];
        [self showToast:errormsg];
    }
}


-(void)loadDataDistrict
{
    TAG_pass=TAG_DISTRICT;
    
    header_title_pass=NSLocalizedString(@"district", nil);
    
    
    arr_table_pass=[[NSMutableArray alloc]init];
    
    for (int i=0; i<[districtList count]; i++)
    {
        [arr_table_pass addObject:[[districtList objectAtIndex:i] valueForKey:@"dnam"]];
    }
    [self callCustomPicker];
}


-(void)callCustomPicker
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    customVC = [storyboard instantiateViewControllerWithIdentifier:@"EnterRegFieldVC"];
    
    customVC.get_title_pass=header_title_pass;
    customVC.get_arr_element=arr_table_pass;
    customVC.get_TAG=TAG_pass;
    customVC.hidesBottomBarWhenPushed = YES;
    
    if ([tagFrom isEqualToString:@"ISFROMPROFILEUPDATE"]) {
        
        customVC.TAG_FROM=@"ISFROMPROFILEUPDATE";
        [singleton traceEvents:@"Select Custom Picker" withAction:@"Clicked" withLabel:@"UserProfileUpdate" andValue:0];

        [self.navigationController pushViewController:customVC animated:YES];
    }
    else if ([tagFrom isEqualToString:@"ISFROMREGISTRATION"]) {
        
        customVC.TAG_FROM=@"ISFROMREGISTRATION";
        [singleton traceEvents:@"Select Custom Picker" withAction:@"Clicked" withLabel:@"UserProfileRegistration" andValue:0];

        [self presentViewController:customVC animated:NO completion:nil];
    }
    
}







//----- hitAPI for IVR OTP call Type registration ------
-(void)hitCityAPI
{
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    //state_id
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"lang"];//lang is Status of the account
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [dictBody setObject:state_id forKey:@"stid"];  //get from mobile default email //not supported iphone
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_FETCH_CITY withBody:dictBody andTag:TAG_REQUEST_FETCH_CITY completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        if (error == nil) {
            //NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            // NSString *rc=[response valueForKey:@"rc"];
            //  NSString *rs=[response valueForKey:@"rs"];
            
            //NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            // NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                flag_apiHitStatus=TRUE;
                //singleton.user_tkn=tkn;
                citiesList=[[NSMutableArray alloc]init];
                citiesList=[[response valueForKey:@"pd"]valueForKey:@"cities"];
                
                districtList=[[NSMutableArray alloc]init];
                districtList=[[response valueForKey:@"pd"]valueForKey:@"district"];
                
                
                
                
                /*    cid = 9878;
                 cnam = Zira;
                 did = 295;
                 dnam = Amritsar;
                 
                 */
                [self refreshdata];
                
            }
            
        }
        else{
            flag_apiHitStatus=FALSE;
            //NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}


-(void)refreshdata
{
    
    
    
    if ([citiesList count]!=0) {
        NSPredicate *cityfilter = [NSPredicate predicateWithFormat:@"cnam =%@",singleton.notiTypeCitySelected];
        NSArray *cityIdFilter = [citiesList filteredArrayUsingPredicate:cityfilter];
        
        NSPredicate *distfilter = [NSPredicate predicateWithFormat:@"dnam =%@",singleton.notiTypDistricteSelected];
        NSLog(@"singleton.notiTypDistricteSelected=%@",singleton.notiTypDistricteSelected);
        NSLog(@"user_dist=%@",user_dist);
        
        
        if ([singleton.notiTypDistricteSelected length]!=0) {
            user_dist=[NSString stringWithFormat:@"%@",singleton.notiTypDistricteSelected];
            
        }
        NSArray *distIdFilter = [districtList filteredArrayUsingPredicate:distfilter];
        
        district_id=@"";
        
        if ([cityIdFilter count]!=0) {
            city_id=[[cityIdFilter objectAtIndex:0]valueForKey:@"cid"];
            
        }
        if ([distIdFilter count]!=0)
        {
            
            district_id=[[distIdFilter objectAtIndex:0]valueForKey:@"did"];
        }
        
       
    }
    
    [tblProfileEdit reloadData];
    
    
}




-(void)loadDataState
{
    TAG_pass=TAG_STATE_PROFILE;
    header_title_pass=NSLocalizedString(@"states", nil);
    
    NSArray*arry=[obj getStateList];
    arry = [arry sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    header_title_pass=NSLocalizedString(@"states", nil);
    
    arr_table_pass=[[NSMutableArray alloc]init];
    
    arr_table_pass=[arry mutableCopy];
    
    singleton.notiTypeCitySelected=@"";
    //singleton.notiTypDistricteSelected=@"";
    
    [self callCustomPicker];
    
}


- (IBAction)btnAddEmailAction:(id)sender
{
    
    if (user_email.length>0)
    {
        
        
        UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", nil)  destructiveButtonTitle:nil otherButtonTitles:
                                NSLocalizedString(@"update", nil),
                                NSLocalizedString(@"remove", nil),
                                nil];
        popup.tag = 1010;
        [popup showInView:self.view];
        
        
    }
    else
    {
        [sender openAddEmailView];
    }
    
}


-(void)openAddEmailView
{

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddEmailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"AddEmailVC"];
    // [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    vc.tagtopass=tagFrom;
    
    if ([user_email length]!=0) {
        vc.titletopass=NSLocalizedString(@"update_email_address", nil);
    }
    else
    {
        vc.titletopass=NSLocalizedString(@"add_email_address", nil);
        
    }
    
    if ([tagFrom isEqualToString:@"ISFROMPROFILEUPDATE"]) {
        [singleton traceEvents:@"Add Email" withAction:@"Clicked" withLabel:@"UserProfileUpdate" andValue:0];

        [self.navigationController pushViewController:vc animated:YES];
        
    }
    if ([tagFrom isEqualToString:@"ISFROMREGISTRATION"]) {
        [singleton traceEvents:@"Add Email" withAction:@"Clicked" withLabel:@"UserProfileRegistration" andValue:0];

        [self presentViewController:vc animated:NO completion:nil];
    }
}

- (IBAction)btn_AlterMbAction:(id)sender
{
    
    if (user_altermob.length>0)
    {
        
        
        UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", nil)  destructiveButtonTitle:nil otherButtonTitles:
                                NSLocalizedString(@"update", nil),
                                NSLocalizedString(@"remove", nil),
                                nil];
        popup.tag = 2020;
        [popup showInView:self.view];
        
        
    }
    else
    {
        [sender openAlternateMobileView];
    }
    
    
    
}



-(void)openAlternateMobileView
{
    AlternateMobileVC   *vc;
    // new condition add for ipad
    if ([[UIScreen mainScreen]bounds].size.height == 1024)
    {
        vc = [[AlternateMobileVC alloc] initWithNibName:@"AlternateMobile_iPad" bundle:nil];
        
        
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        vc = [storyboard instantiateViewControllerWithIdentifier:@"AlternateMobileVC"];
        
        
    }
    
    
    
    vc.tagtopass=tagFrom;
    
    if ([user_email length]!=0) {
        vc.titletopass=NSLocalizedString(@"update_alt_mob_num", nil);
    }
    else
    {
        vc.titletopass=NSLocalizedString(@"add_alt_mob_num", nil);
        
    }
    
    if ([tagFrom isEqualToString:@"ISFROMPROFILEUPDATE"]) {
        [singleton traceEvents:@"Add Alternate Mobile" withAction:@"Clicked" withLabel:@"UserProfileUpdate" andValue:0];

        [self.navigationController pushViewController:vc animated:YES];
        
    }
    if ([tagFrom isEqualToString:@"ISFROMREGISTRATION"]) {
        [singleton traceEvents:@"Add Alternate Mobile" withAction:@"Clicked" withLabel:@"UserProfileRegistration" andValue:0];

        [self presentViewController:vc animated:NO completion:nil];
    }
    
    
    
    
    
    
    /*
     AlternateMobileVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AlternateMobileVC"];
     [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
     vc.tagtopass=tagFrom;
     if ([user_altermob length]!=0) {
     vc.titletopass=NSLocalizedString(@"update_alt_mob_num", nil);
     }
     else
     {
     vc.titletopass=NSLocalizedString(@"add_alt_mob_num", nil);
     
     }
     
     if ([tagFrom isEqualToString:@"ISFROMPROFILEUPDATE"]) {
     [self.navigationController pushViewController:vc animated:YES];
     
     }
     if ([tagFrom isEqualToString:@"ISFROMREGISTRATION"]) {
     [self presentViewController:vc animated:NO completion:nil];
     }
     */
    
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}


-(void)hideKeyboard
{
    [self.view endEditing:YES];
    
    // [self.txt_mobileNo resignFirstResponder];
}

- (IBAction)btnNextAction:(id)sender {
    
    // Take from  Take photo /choose from gallery / or If link Import from facebook /Import from google
    [self hideKeyboard];
    
    /* if ([self validateNameWithString:user_name]!=TRUE  && [user_name length]!=0)
     
     {
     
     //NSLog(@"Wrong Name");
     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:@"Invalid Name" delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
     [alert show];
     }
     
     
     else */ if ([self validateEmailWithString:user_email]!=TRUE && [user_email length]!=0){
         
         //NSLog(@"Wrong email");
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"enter_correct_email", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
         [alert show];
     }
    
    
     else  if ([self validatePhone:user_altermob]!=TRUE && [user_altermob length]!=0)
     {
         
         //NSLog(@"Wrong Mobile");
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"enter_correct_phone_number", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
         [alert show];
     }
    
     else
     {
         
         if ([tagFrom isEqualToString:@"ISFROMPROFILEUPDATE"]) {
             [self openEnterMPIN];
             
         }
         if ([tagFrom isEqualToString:@"ISFROMREGISTRATION"]) {
             [self hitAPI];
         }
         
     }
    
}
-(void)openEnterMPIN
{
    //[self displayContentController:[self getHomeDetailLayerLeftController]];
    
    [self removelocalimagePath];
    NSString *removePicStatus=@"";
    if (flagRemovePic==FALSE)
    {
        //do nothign
        removePicStatus=@"FALSE";
    }
    else
    {
        // flagRemovePic==FALSE
        removePicStatus=@"TRUE";
        
    }
    
    NSMutableDictionary *user_dic = [NSMutableDictionary new];
    
    NSString *gnder=@"";
    
    if ([singleton.notiTypeGenderSelected isEqualToString:NSLocalizedString(@"gender_male", nil)]) {
        gnder=@"M";
    }
    else if ([singleton.notiTypeGenderSelected isEqualToString:NSLocalizedString(@"gender_female", nil)]) {
        gnder=@"F";
    }
    else if ([singleton.notiTypeGenderSelected isEqualToString:NSLocalizedString(@"gender_transgender", nil)]) {
        gnder=@"T";
    }
    else
    {
        gnder=@"";
        
    }
    
    //base64Image = [self encodeToBase64String:self.imgView_user.image];
    
    
    
    // pass id here
    
    NSString *qual_id=[obj getQualiListCode:user_quali];
    NSString *occup_id=[obj getOccuptCode:user_Occup];
    
    
    
    NSString *qual=qual_id;
    NSString *occup=occup_id;
    NSString *addr=user_address;
    NSString *name=user_name;
    NSString *dob=user_dob;
    NSString *amno=user_altermob;
    NSString *email=user_email;
    
    // NSString *gnder=user_gender;
    if ([amno length]==0) {
        amno=@"";
    }
    if ([email length]==0) {
        email=@"";
    }
    
    if ([qual length]==0) {
        qual=@"";
    }
    
    if ([occup length]==0) {
        occup=@"";
    }
    if ([addr length]==0) {
        addr=@"";
    }
    
    
    
    if ([name length]==0) {
        name=@"";
    }
    
    if ([gnder length]==0) {
        gnder=@"";
    }
    
    
    if ([state_id length]==0)
    {
        state_id=@"";
    }
    
    
    if ([district_id length]==0) {
        district_id=@"";
    }
    
    if ([dob length]==0) {
        dob=@"";
    }
    
    if ([base64Image length]==0) {
        base64Image=@"";
    }
    
    //state_id=[obj getStateCode:user_state];
    
    [user_dic setValue:qual forKey:@"txt_qualification"];
    [user_dic setValue:occup forKey:@"txt_occupation"];
    [user_dic setValue:addr forKey:@"txt_address"];
    [user_dic setValue:name forKey:@"name"];
    [user_dic setValue:gnder forKey:@"gender"];
    [user_dic setValue:state_id forKey:@"state_id"];
    [user_dic setValue:@"" forKey:@"city_id"];
    [user_dic setValue:district_id forKey:@"district_id"];
    [user_dic setValue:@"UPDATE" forKey:@"Tag"];
    [user_dic setValue:amno forKey:@"amno"];
    [user_dic setValue:email forKey:@"email"];
    [user_dic setValue:dob forKey:@"DOB"];
    [user_dic setValue:removePicStatus forKey:@"removePicStatus"];
    
    [user_dic setValue:picChangeStatus forKey:@"picChangeStatus"];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    updMPinVC = [storyboard instantiateViewControllerWithIdentifier:@"UpdMpinVC"];
    
    updMPinVC.dic_info=user_dic;//change it to URL on demand
    
    if ([picChangeStatus isEqualToString:@"FALSE"])
    {
        UIImage *img = [UIImage imageWithData:self.imgtopass];
        updMPinVC.user_img= UIImagePNGRepresentation(img) ;
    }
    else
    {
        updMPinVC.user_img = UIImagePNGRepresentation(imageUserProfile.image);
    }
    
    [self.navigationController pushViewController:updMPinVC animated:YES];
    
}



- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

-(void)removelocalimagePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    singleton.imageLocalpath = [documentsDirectory stringByAppendingPathComponent:
                                @"user_image.png" ];
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    
    NSString *filePath = singleton.imageLocalpath;
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        singleton.imageLocalpath=@"";
        //singleton.user_profile_URL=@"";
        
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
    
}
-(UpdMpinVC*)getHomeDetailLayerLeftController
{
    
    
    
    
    
    
    [self removelocalimagePath];
    NSString *removePicStatus=@"";
    if (flagRemovePic==FALSE)
    {
        //do nothign
        removePicStatus=@"FALSE";
    }
    else
    {
        // flagRemovePic==FALSE
        removePicStatus=@"TRUE";
        
    }
    
    NSMutableDictionary *user_dic = [NSMutableDictionary new];
    
    NSString *gnder=@"";
    
    if ([singleton.notiTypeGenderSelected isEqualToString:NSLocalizedString(@"gender_male", nil)]) {
        gnder=@"M";
    }
    else if ([singleton.notiTypeGenderSelected isEqualToString:NSLocalizedString(@"gender_female", nil)]) {
        gnder=@"F";
    }
    else if ([singleton.notiTypeGenderSelected isEqualToString:NSLocalizedString(@"gender_transgender", nil)]) {
        gnder=@"T";
    }
    else
    {
        gnder=@"";
        
    }
    
    //base64Image = [self encodeToBase64String:self.imgView_user.image];
    
    
    
    // pass id here
    
    NSString *qual_id=[obj getQualiListCode:user_quali];
    NSString *occup_id=[obj getOccuptCode:user_Occup];
    
    
    
    NSString *qual=qual_id;
    NSString *occup=occup_id;
    NSString *addr=user_address;
    NSString *name=user_name;
    NSString *dob=user_dob;
    NSString *amno=user_altermob;
    NSString *email=user_email;
    
    // NSString *gnder=user_gender;
    if ([amno length]==0) {
        amno=@"";
    }
    if ([email length]==0) {
        email=@"";
    }
    
    if ([qual length]==0) {
        qual=@"";
    }
    
    if ([occup length]==0) {
        occup=@"";
    }
    if ([addr length]==0) {
        addr=@"";
    }
    
    
    
    if ([name length]==0) {
        name=@"";
    }
    
    if ([gnder length]==0) {
        gnder=@"";
    }
    
    
    if ([state_id length]==0) {
        state_id=@"";
    }
    
    if ([district_id length]==0) {
        district_id=@"";
    }
    
    if ([dob length]==0) {
        dob=@"";
    }
    
    if ([base64Image length]==0) {
        base64Image=@"";
    }
    
    
    [user_dic setValue:qual forKey:@"txt_qualification"];
    [user_dic setValue:occup forKey:@"txt_occupation"];
    [user_dic setValue:addr forKey:@"txt_address"];
    [user_dic setValue:name forKey:@"name"];
    [user_dic setValue:gnder forKey:@"gender"];
    [user_dic setValue:state_id forKey:@"state_id"];
    [user_dic setValue:@"" forKey:@"city_id"];
    [user_dic setValue:district_id forKey:@"district_id"];
    [user_dic setValue:@"UPDATE" forKey:@"Tag"];
    [user_dic setValue:amno forKey:@"amno"];
    [user_dic setValue:email forKey:@"email"];
    [user_dic setValue:dob forKey:@"DOB"];
    [user_dic setValue:removePicStatus forKey:@"removePicStatus"];
    
    [user_dic setValue:picChangeStatus forKey:@"picChangeStatus"];
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    updMPinVC = [storyboard instantiateViewControllerWithIdentifier:@"UpdMpinVC"];
    //}
    
    updMPinVC.dic_info=user_dic;//change it to URL on demand
    updMPinVC.user_img=imageUserProfile.image;
    
    // UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    //if (itemMoreVC == nil) {
    
    return updMPinVC;
}





- (void) displayContentController:(UIViewController*)content;
{
    // UIViewController *vc=[self topMostController];
    
    [self addChildViewController:content];
    
    content.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [self.view addSubview:content.view];
    [content didMoveToParentViewController:self];
    
    [self showViewControllerFromLeftSide];
}

-(void)showViewControllerFromLeftSide{
    // UIViewController *vc=[self topMostController];
    
    //add animation from left side
    [UIView animateWithDuration:0.4 animations:^{
        updMPinVC.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
}

-(void)hideViewController{
    //  UIViewController *vc=[self topMostController];
    //if (itemMoreVC == nil) {
    updMPinVC = [self getHomeDetailLayerLeftController];
    //}
    //add animation from left side
    [UIView animateWithDuration:0.4 animations:^{
        updMPinVC.view.frame = CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
    
}




//----- hitAPI for IVR OTP call Type registration ------
-(void)hitAPI
{
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //(dd-mm-yyyy)
    //  NSString *encrytSHA256=[NSString stringWithFormat:@"|%@|%@|",mpinStr,SaltRequestControl];
    //NSString *mpinStrEncrypted=[singleton.user_mpin sha256HashFor:singleton.user_mpin];
    NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",singleton.user_mpin,SaltMPIN];
    NSString *mpinStrEncrypted=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
    //NOTE mpinStrEncrypted value will be pass everywher
    // [dictBody setObject:mpinStrEncrypted forKey:@"mpin"];
    
    NSString *userGender=@"";
    
    if ([singleton.notiTypeGenderSelected isEqualToString:NSLocalizedString(@"gender_male", nil)]) {
        user_gender=@"M";
    }
    else if ([singleton.notiTypeGenderSelected isEqualToString:NSLocalizedString(@"gender_female", nil)]) {
        user_gender=@"F";
        
    }
    else
    {
        
        user_gender=@"T";
        
    }
    
    
    
    
    if([user_gender isEqualToString:@"M"]||[user_gender isEqualToString:@"m"]||[user_gender isEqualToString:@"MALE"])
    {
        userGender=@"m";
    }
    else if([user_gender isEqualToString:@"F"]||[user_gender isEqualToString:@"f"]||[user_gender isEqualToString:@"FEMALE"])
    {
        userGender=@"f";
        
    }
    else
    {
        userGender=@"t";
        
    }
    
    
    state_id = [obj getStateCode:user_state];
    //NSLog(@"state_id=%@",state_id);
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    NSString *name=user_name;
    NSString *dob=user_dob;
    NSString *amno=user_altermob;
    NSString *email=user_email;
    
    NSString *qual=user_quali;
    NSString *occup=user_Occup;
    NSString *addr=user_address;
    
    if ([qual length]==0) {
        qual=@"";
    }
    if ([occup length]==0) {
        occup=@"";
    }
    if ([addr length]==0) {
        addr=@"";
    }
    
    
    
    if ([name length]==0) {
        name=@"";
    }
    if ([dob length]==0) {
        dob=@"";
    }
    if ([amno length]==0) {
        amno=@"";
    }
    if ([email length]==0) {
        email=@"";
    }
    
    
    
    
    
    if ([state_id length]==0)
    {
        state_id=@"";
    }
    else
    {
        singleton.stateSelected = state_id;
    }
    
    if ([district_id length]==0) {
        district_id=@"";
    }
    
    
    
    
    
    
    
    if ([base64Image length]==0) {
        base64Image=@"";
    }
    
    
    
    
    [dictBody setObject:name forKey:@"nam"]; //Enter user name
    [dictBody setObject:userGender forKey:@"gndr"];//Enter gender of user(m/f/t)
    [dictBody setObject:dob forKey:@"dob"]; //Enter DOB of User
    [dictBody setObject:@"" forKey:@"cty"]; //Enter District of User
    [dictBody setObject:state_id forKey:@"st"]; //Enter State Of user
    [dictBody setObject:district_id forKey:@"dist"]; //Enter District of User
    
    
    [dictBody setObject:amno forKey:@"amno"];//Enter alternate mobile number of user
    [dictBody setObject:email forKey:@"email"];//Enter email address by user
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"]; //tkn number
    [dictBody setObject:mpinStrEncrypted forKey:@"mpin"];  //MPIN
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [dictBody setObject:occup forKey:@"occup"];
    [dictBody setObject:addr forKey:@"addr"];
    [dictBody setObject:qual forKey:@"qual"];
    [dictBody setObject:@"FIRST" forKey:@"flag"];  //get from mobile default email //not supported iphone
    
    [dictBody setObject:base64Image forKey:@"pic"];  //Check for pic upload
    
    //flag = URL
    //At registration flag=FIRST
    
    
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:YES webServiceURL:UM_API_UPDATE_PROFILE withBody:dictBody andTag:TAG_REQUEST_UPDATE_PROFILE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        
        if (error == nil) {
            //NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            //  NSString *rc=[response valueForKey:@"rc"];
            //  NSString *rs=[response valueForKey:@"rs"];
            //  NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
            
            //NSLog(@"value of rc =%@ \n value of rs=%@ \n value of tkn=%@ ",rc,rs,tkn);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                [self alertwithMsg:rd];
            }
            
        }
        else{
            //NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}


-(void)alertwithMsg:(NSString*)msg
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"profile_label", nil) message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                   {
                                       
                                       if ([tagFrom isEqualToString:@"ISFROMPROFILEUPDATE"]) {
                                           
                                           [singleton traceEvents:@"Open Profile After Update" withAction:@"Clicked" withLabel:@"UserProfileUpdate" andValue:0];

                                           [self.navigationController popViewControllerAnimated:YES];
                                           
                                           
                                           
                                       }
                                       if ([tagFrom isEqualToString:@"ISFROMREGISTRATION"]) {
                                           
                                           [singleton traceEvents:@"Open Home After Registration" withAction:@"Clicked" withLabel:@"UserProfileRegistration" andValue:0];

                                           // [self dismissViewControllerAnimated:NO completion:nil];
                                           
                                           // singleton.user_mpin=@"";
                                           UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                           
                                           UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
                                           tbc.selectedIndex=0;
                                           
                                           [self presentViewController:tbc animated:NO completion:nil];
                                           
                                       }
                                       
                                   }];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}





- (void)viewWillLayoutSubviews {
    
    /*if (UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication.statusBarOrientation))
     {
     }
     else
     {
     
     }*/
    
    btnChangeImage.frame = CGRectMake((self.view.frame.size.width/2)-100, vwHeader.frame.size.height - 50, 200, 30);
    
    [super viewWillLayoutSubviews];
}




- (IBAction)btnSkipAction:(id)sender
{
    [singleton traceEvents:@"Skip Button To Open Home After" withAction:@"Clicked" withLabel:@"UserProfileRegistration" andValue:0];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
    tbc.selectedIndex=0;
    
    [self presentViewController:tbc animated:NO completion:nil];
}




- (void)viewDidLoad {
    
    picChangeStatus=@"FALSE";
    flagEditCheck=FALSE;
    
    
    NSLog(@"%@",self.imgtopass);
    
    singleton=[SharedManager sharedSingleton];
    if ([singleton.user_profile_URL length]<=0) {
        singleton.user_profile_URL=@"";
    }
    imgURL_removeStatus=[NSString stringWithFormat:@"%@",singleton.user_profile_URL];
    
    flagRemovePic=FALSE;
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    //Google Tracking
    
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:USER_EDIT_PROFILE_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    lblHeaderTitle.text = NSLocalizedString(@"edit_profile", nil);
    
    
    
    
    
    NSString *savetilte=NSLocalizedString(@"save", nil);
    
    [btn_save setTitle:savetilte forState:UIControlStateNormal];
    [btn_save setTitle:savetilte forState:UIControlStateSelected];
    [btn_save setTitle:savetilte forState:UIControlStateHighlighted];
    [btn_save addTarget:self action:@selector(btnNextAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    self.old_state=[NSString stringWithFormat:@"%@",singleton.profilestateSelected];
    NSLog(@"self.old_state=%@",self.old_state);
    
    flagURL=FALSE;
    //NSLog(@"dic_info=%@",dic_info);
    flag_apiHitStatus=FALSE;
    
    obj=[[StateList alloc]init];
    state_id=@"";
    
    
    self.navigationController.navigationBarHidden=true;
    @try {
        
        self.navigationItem.title = @"";
        
        __weak id weakSelf = self;
        self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    
    
    // Do any additional setup after loading the view.
    tblProfileEdit.tableHeaderView =  [self designHeaderView];
    tblProfileEdit.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    tblProfileEdit.tableFooterView =  [self designFooterView];
    
    
    
    arrGeneralInformation = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"name_caps", nil),NSLocalizedString(@"gender_caps", nil),NSLocalizedString(@"date_of_birth_caps", nil),NSLocalizedString(@"qualication_caps", nil),NSLocalizedString(@"occupation_caps", nil),NSLocalizedString(@"state_txt_caps", nil),NSLocalizedString(@"district_caps", nil),NSLocalizedString(@"address_caps", nil), nil];
    arrAccountInformation = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"email_address", nil),NSLocalizedString(@"alt_mob_num", nil), nil];
    
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action: @selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    [super viewDidLoad];
    
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}


- (BOOL)validatePhone:(NSString *)phoneNumber
{
    //NSString *phoneRegex = @"[789][0-9]{3}([0-9]{6})?";
  //  NSString *phoneRegex =@"[6789][0-9]{9}";
/*    NSString *phoneRegex =@"^[0-9]+$";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [test evaluateWithObject:phoneNumber];
    */
    if (phoneNumber.length == 0 || phoneNumber.length > 19 )
    {
        return false;
    }
    return true;
}


- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}



-(BOOL)validateNameWithString:(NSString*)nametopass
{
    NSString *nameRegex =@"^[a-zA-Z\\s]*$";
    
    NSPredicate *testRegex = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nameRegex];
    if(![testRegex evaluateWithObject:nametopass])
        return NO;
    else
        return YES;
    
}
//------- End of validation-------




-(void)viewWillAppear:(BOOL)animated
{
    
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        obj=[[StateList alloc]init];
        [obj hitStateQualifiAPI];
        
        
        
        [self performSelector:@selector(loadwithdelay) withObject:nil afterDelay:0.1];
        
        
        
    });
    
    /*
     
     UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bgTouch:)];
     [recognizer setNumberOfTapsRequired:1];
     [recognizer setNumberOfTouchesRequired:1];
     [self.view addGestureRecognizer:recognizer];
     */
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(boolCheckvalueedit:)
                                                 name:@"STARTNOTIFIERUPD"
                                               object:nil];
    
    // [[UILabel appearanceWhenContainedIn:[UITableViewCell class], nil] setFont:[UIFont systemFontOfSize:fontsize]];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [self setViewFont];
    [super viewWillAppear:YES];
}
#pragma mark - Country Picker Action
-(void)addCountryCodeView {
    //.hidden = true;
   // CGRect lblFrame = .frame;
    //lblFrame.size.width = 80;
    NSString *lastcountrycode =  nil;
    NSString *lastcountryImg =  nil;
    
    if (countryView != nil)
    {
        lastcountrycode = countryView.lastCountryCode;
        lastcountryImg = countryView.lastCountryImg;
        [countryView removeFromSuperview];
        countryView = nil;
    }
    
    countryView = [[CountryCodeView alloc] initWithGrayArrowFrame:CGRectMake(0, 0, 0, 0)];
    countryView.didTapUpdateCountry = ^{
        NSLog(@"did tap country change");
    };
    countryView.btnCountryCode.titleLabel.font = [AppFont mediumFont:17.0];
    
    if ( lastcountrycode != nil ) {
        [countryView updateCountryWith:lastcountrycode imageName:lastcountryImg];
    }else {
        [countryView delayUpdate];
    }
    
    //[_txt_enterId setLeftView:countryView];
    // [_txt_enterId setLeftView:countryView];
    //  [vw_login addSubview:countryView];
}
#pragma mark- Font Set to View
-(void)setViewFont
{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    
    lblHeaderTitle.font = [AppFont semiBoldFont:17.0];
    
    [btn_save.titleLabel setFont:[AppFont regularFont:17.0]];
    
}
#pragma mark -
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    if (singleton.profilestateSelected.length != 0)
    {
        singleton.stateSelected = singleton.profilestateSelected;
        singleton.user_StateId = [obj getStateCode:singleton.stateSelected];
    }
    
}


- (void) boolCheckvalueedit:(NSNotification *)notification
{
    NSString *theString = [notification object];
    
    if ([theString isEqualToString:@"No"]) {
        
    }
    else
    {
        flagEditCheck=TRUE;
    }
    
}


-(void)loadwithdelay
{
    
    
    [tblProfileEdit reloadData];
    
    NSLog(@"value of state=%@",singleton.notiTypeGenderSelected);
    
    
    
    
    if ([singleton.notiTypeGenderSelected length]!=0) {
        
        
        // user_gender=[NSString stringWithFormat:@"%@",singleton.notiTypeGenderSelected];
        
    }
    
    if ([singleton.profilestateSelected length]!=0) {
        user_state=[NSString stringWithFormat:@"%@",singleton.profilestateSelected];
        
    }
    if ([singleton.notiTypDistricteSelected length]!=0) {
        user_dist=[NSString stringWithFormat:@"%@",singleton.notiTypDistricteSelected];
        
    }
    if ([singleton.profileDOBSelected length]!=0) {
        user_dob=[NSString stringWithFormat:@"%@",singleton.profileDOBSelected];
        
    }
    
    user_gender=[dic_info valueForKey:@"str_gender"];
    
    user_name=[dic_info valueForKey:@"str_name"];
    if ([singleton.profileNameSelected length]!=0)
    {
        user_name=[NSString stringWithFormat:@"%@",singleton.profileNameSelected];
    }
    
    
    
    
    user_email=[dic_info valueForKey:@"str_emailAddress"];
    
    if ([singleton.profileEmailSelected length]!=0) {
        user_email=[NSString stringWithFormat:@"%@",singleton.profileEmailSelected];
        
    }
    
    
    user_altermob=[dic_info valueForKey:@"str_alternateMb"];
    
    
    if ( [singleton.altermobileNumber length]!=0)
    {
        user_altermob=[NSString stringWithFormat:@"%@", singleton.altermobileNumber];
    }
    
    
    
    
    if ([dic_info valueForKey:@"str_occupation"]) {
        
        user_Occup=[dic_info valueForKey:@"str_occupation"];
    }
    
    if ([dic_info valueForKey:@"str_qualification"]) {
        
        user_quali=[dic_info valueForKey:@"str_qualification"];
    }
    if ([singleton.user_Occupation length]!=0) {
        user_Occup=[NSString stringWithFormat:@"%@",singleton.user_Occupation];
        
    }
    
    if ([singleton.user_Qualification length]!=0) {
        user_quali=[NSString stringWithFormat:@"%@",singleton.user_Qualification];
        
    }
    
    if ([dic_info valueForKey:@"str_address"]) {
        user_address=[dic_info valueForKey:@"str_address"];
        
    }
    if ([singleton.profileUserAddress length]!=0) {
        user_address=[NSString stringWithFormat:@"%@",singleton.profileUserAddress];
        
    }
    
    if (![self.old_state isEqualToString:singleton.profilestateSelected]) {
        user_dist=@"";
        // self.old_district=singleton.notiTypDistricteSelected;
        //singleton.notiTypDistricteSelected=@"";
        NSLog(@"user state are not same old state=%@ and new state=%@",self.old_state , singleton.profilestateSelected);
    }
    
    if (![singleton.profilestateSelected isEqualToString:@""])
    {
        state_id= [obj getStateCode:user_state];
        
        // flag_apiHitStatus=TRUE;
        if (flag_apiHitStatus ==FALSE)
        {
            [self hitCityAPI];
            
        }
    }
    
    /*if ((NSNull *)[dic_info objectForKey:@"socialpd"] != [NSNull null])
     {
     if ([dic_info valueForKey:@"socialpd"])
     {
     
     //NSLog(@"socialpd=%@",[dic_info valueForKey:@"socialpd"]);
     //NSLog(@"goid=%@",[[dic_info valueForKey:@"socialpd"] valueForKey:@"goimg"]);
     
     
     if ([[dic_info valueForKey:@"socialpd"] valueForKey:@"goimg"]) {
     googleId=[NSString stringWithFormat:@"%@",[[dic_info valueForKey:@"socialpd"] valueForKey:@"goimg"]];
     }
     if ([[dic_info valueForKey:@"socialpd"] valueForKey:@"fbid"]) {
     
     if ([[[dic_info valueForKey:@"socialpd"] valueForKey:@"fbid"] isEqualToString:@""]) {
     facebookId=@"";
     }
     else
     {
     facebookId=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",[[dic_info valueForKey:@"socialpd"] valueForKey:@"fbid"]];
     
     }
     
     }
     if ([[dic_info valueForKey:@"socialpd"] valueForKey:@"twitimg"]) {
     twitterId=[NSString stringWithFormat:@"%@",[[dic_info valueForKey:@"socialpd"] valueForKey:@"twitimg"]];
     }
     
     }
     
     }*/
    
    if ([dic_info valueForKey:@"goimg"]) {
        googleId=[NSString stringWithFormat:@"%@",[dic_info valueForKey:@"goimg"]];
        
    }
    
    if ([dic_info valueForKey:@"facebookId"]) {
        facebookId=[NSString stringWithFormat:@"%@",[dic_info valueForKey:@"facebookId"]];
        /* if (![facebookId isEqualToString:@""]) {
         facebookId=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",facebookId];
         }*/
        
    }
    if ([dic_info valueForKey:@"twitimg"]) {
        twitterId=[NSString stringWithFormat:@"%@",[dic_info valueForKey:@"twitimg"]];
        
    }
    
    
    
    
    
    [self refreshdata];
    
}
- (IBAction)btnBackAction:(id)sender {
    
    // flagEditCheck=TRUE;
    if (flagEditCheck==TRUE)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:NSLocalizedString(@"update_profile_save_changes", nil)                                                           delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"no", nil),NSLocalizedString(@"yes", nil),nil];
        alert.tag=101;
        [alert show];
        
    }
    
    else
    {
        
        [self backViewMethod];
    }
    
    
}
-(void)backViewMethod
{
    if ([tagFrom isEqualToString:@"ISFROMPROFILEUPDATE"]) {
        [singleton traceEvents:@"Back Button" withAction:@"Clicked" withLabel:@"UserProfileUpdate" andValue:0];

        customVC.TAG_FROM=@"ISFROMPROFILEUPDATE";
        [self.navigationController popViewControllerAnimated:YES];
        //[self.navigationController popToRootViewControllerAnimated:YES];
        
    }
    if ([tagFrom isEqualToString:@"ISFROMREGISTRATION"]) {
        [singleton traceEvents:@"Back Button" withAction:@"Clicked" withLabel:@"UserProfileRegistration" andValue:0];

        customVC.TAG_FROM=@"ISFROMREGISTRATION";
        
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}
-(void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 101 && buttonIndex == 1)
    { // handle the altdev
        
        [self btnNextAction:self];
        
    }
    else
    {
        if (self.old_state.length == 0)
        {
            singleton.profilestateSelected = @"";
        }
        [self backViewMethod];
    }
    
}


-(UIImage *)makeRoundedImage:(UIImage *) image
                      radius: (float) radius;
{
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    imageLayer.contents = (id) image.CGImage;
    imageLayer.masksToBounds = YES;
    imageLayer.cornerRadius = radius;
    UIGraphicsBeginImageContext(image.size);
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return roundedImage;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        
        if (indexPath.row==0)
        {
            //NSLog(@"indexpath row Name");
            
            
        }
        if (indexPath.row==1)
        {
            [self btnGenderAction:self];
            
        }
        if (indexPath.row==2)
        {
            
            [self btnDobAction:self];
            
        }
        if (indexPath.row==3)
        {
            [self btnQualifyAction:self];
            
        }
        if (indexPath.row==4)
        {
            [self btnOccupAction:self];
            
            
        }
        if (indexPath.row==5)
        {
            
            [self btnStateAction:self];
            
            
        }
        if (indexPath.row==6)
        {
            [self btnDistrictAction:self];
            
        }
        if (indexPath.row==7)
        {
            
            //NSLog(@"indexpath row Address");
            
            
        }
        
        
        
    }
    else  if (indexPath.section == 1)
        
    {
        if (indexPath.row==0)
        {
            
            [self btnAddEmailAction:self];
            
        }
        if (indexPath.row==1)
        {
            [self btn_AlterMbAction:self];
            
            
        }
        
        
        
    }
    
    
}


-(UIImage *)makeRoundedImage:(UIImage *) image
{
    float radius=image.size.height /2;
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    imageLayer.contents = (id) image.CGImage;
    
    imageLayer.masksToBounds = YES;
    imageLayer.cornerRadius = radius;
    
    UIGraphicsBeginImageContext(image.size);
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return roundedImage;
}


-(UIView *)designHeaderView
{
    
    if (vwHeader) {
        [[vwHeader subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [vwHeader removeFromSuperview];
        vwHeader = nil;
    }
    vwHeader = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tblProfileEdit.frame.size.width, 230.0)];
    vwHeader.backgroundColor = [UIColor whiteColor];
    
    
    imageUserProfile =[[UIImageView alloc]initWithFrame:CGRectMake((fDeviceWidth/2)-70, 30, 140, 140)];
    imageUserProfile.contentMode = UIViewContentModeCenter;
    
    /*imageUserProfile.layer.backgroundColor=[[UIColor clearColor] CGColor];
     imageUserProfile.layer.borderColor = [UIColor whiteColor].CGColor;
     imageUserProfile.layer.borderWidth = 1.0;
     imageUserProfile.layer.cornerRadius = 70.0;
     */
    
    imageUserProfile.layer.cornerRadius = imageUserProfile.frame.size.height /2;
    imageUserProfile.layer.masksToBounds = YES;
    imageUserProfile.layer.borderWidth = 2;
    imageUserProfile.layer.borderColor = [UIColor whiteColor].CGColor;
    
    
    [vwHeader addSubview: imageUserProfile];
    
    
    [imageUserProfile setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap setNumberOfTapsRequired:1];
    [imageUserProfile addGestureRecognizer:singleTap];
    
    
    UIView  *vwHeaderLine = [[UIView alloc]initWithFrame:CGRectMake(0,vwHeader.frame.size.height-0.5, vwHeader.frame.size.width,0.5 )];
    vwHeaderLine.backgroundColor = [UIColor lightGrayColor];
    [vwHeader addSubview:vwHeaderLine];
    
    
    
    
    NSString *tilte=NSLocalizedString(@"add_profile_picture", nil);
    
    
    
    if ([singleton.user_profile_URL length]>0) {
        tilte=NSLocalizedString(@"change_picture", nil);
    }
    btnChangeImage = [[UIButton alloc]initWithFrame:CGRectMake((tblProfileEdit.frame.size.width/2)-100, vwHeader.frame.size.height - 50, 200, 30)];
    [btnChangeImage setTitle:tilte forState:UIControlStateNormal];
    [btnChangeImage setTitle:tilte forState:UIControlStateSelected];
    [btnChangeImage setTitle:tilte forState:UIControlStateHighlighted];
    
    
    [btnChangeImage setTintColor:[UIColor blueColor]];
    [btnChangeImage setTitleColor:[UIColor colorWithRed:10.0/255.0 green:90.0/255.0 blue:157.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    btnChangeImage.titleLabel.font = [UIFont boldSystemFontOfSize:14.0];
    [vwHeader addSubview:btnChangeImage];
    [btnChangeImage addTarget:self action:@selector(btnChangeImageClicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    imageUserProfile.image= [UIImage imageWithData:self.imgtopass];
    //imageUserProfile.contentMode=UIViewContentModeScaleAspectFit;
    imageUserProfile.contentMode=UIViewContentModeScaleAspectFill;
    imageUserProfile.clipsToBounds=YES;
    
    
    return vwHeader;
}


-(void)singleTapping:(UIGestureRecognizer *)recognizer
{
    NSLog(@"image clicked");
    
    [self btnChangeImageClicked];
}


-(UIView*)designFooterView
{
    vwFooter = [[UIView alloc]initWithFrame:CGRectMake(0,tblProfileEdit.frame.size.height-150, tblProfileEdit.frame.size.width, 150.0)];
    [tblProfileEdit addSubview:vwFooter];
    vwFooter.backgroundColor = [UIColor whiteColor];
    
    UIButton *btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btnNext setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [btnNext setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnNext.layer.cornerRadius = 3.0f;
    btnNext.clipsToBounds = YES;
    
    if (self.view.frame.size.width < 500)
    {
        btnNext.frame=CGRectMake(30 ,20, self.view.frame.size.width - 60, 50);
    }
    else
    {
        btnNext.frame=CGRectMake(200 ,20, self.view.frame.size.width - 400, 65);
    }
    
    [btnNext setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];
    
    //[btnNext setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateNormal];
    [vwFooter addSubview:btnNext];
    [btnNext addTarget:self action:@selector(btnNextAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    UIButton *btnskip = [[UIButton alloc]initWithFrame:CGRectMake((tblProfileEdit.frame.size.width/2)-100, vwFooter.frame.size.height -50, 200, 30)];
    
    
    [btnskip setTitle:NSLocalizedString(@"skip", nil) forState:UIControlStateNormal];
    [btnskip setTintColor:[UIColor blueColor]];
    [btnskip setTitleColor:[UIColor colorWithRed:10.0/255.0 green:90.0/255.0 blue:157.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    btnskip.titleLabel.font = [UIFont boldSystemFontOfSize:13.0];
    [vwFooter addSubview:btnskip];
    [btnskip addTarget:self action:@selector(btnSkipAction:) forControlEvents:UIControlEventTouchUpInside];
    if ([tagFrom isEqualToString:@"ISFROMPROFILEUPDATE"]) {
        btnskip.hidden=TRUE;
    }
    if ([tagFrom isEqualToString:@"ISFROMREGISTRATION"]) {
        btnskip.hidden=FALSE;
        
    }
    
    
    return vwFooter;
    
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *vwGrayFooter = [[UIView alloc]initWithFrame:CGRectMake(0, vwHeader.frame.size.height + 40, tblProfileEdit.frame.size.width, 30)];
    [tblProfileEdit addSubview:vwGrayFooter];
    vwGrayFooter.backgroundColor = [UIColor colorWithRed:227.0/255.0 green:230.0/255.0 blue:234.0/255.0 alpha:1.0];
    if (section == 0) {
        vwGrayFooter.hidden = NO;
    }
    else{
        vwGrayFooter.hidden = YES;
    }
    return vwGrayFooter;
}




-(void)addShadowToTheView:(UIView*)vwItem
{
    vwItem.layer.shadowOffset = CGSizeMake(-10.0, 1.0);
    vwItem.layer.shadowColor = [UIColor grayColor].CGColor;
    vwItem.layer.shadowRadius = 3;
    vwItem.layer.shadowOpacity = 0.5;
    vwItem.layer.cornerRadius = 3.0;
}




- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *vwHeaderTitle = [[UIView alloc]initWithFrame:CGRectMake(0, vwHeader.frame.size.height, imageUserProfile.frame.size.width, 50.0)];
    [tblProfileEdit addSubview:vwHeaderTitle];
    vwHeaderTitle.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:234.0/255.0 blue:241.0/255.0 alpha:1.0];
    
    
    
    
    UILabel *lblSectionTitle = [[UILabel alloc]initWithFrame:CGRectMake(7, 28, 200, 21.0)];
    lblSectionTitle.font = [UIFont systemFontOfSize:13.0];
    
    CGRect labelFrame =  lblSectionTitle.frame;
    labelFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(tableView.frame) - 100 : 15;
    lblSectionTitle.textColor = [UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0];
    lblSectionTitle.frame = labelFrame;
    
    
    [vwHeaderTitle addSubview:lblSectionTitle];
    //   lblSectionTitle.textColor = [UIColor grayColor];
    // lblSectionTitle.font = [UIFont systemFontOfSize:14.0];
    // lblSectionTitle.adjustsFontSizeToFitWidth = YES;
    
    
    //for grey header Design
    
    
    
    
    if (section == 0)
    {
        // vwGray.hidden = YES;
        
        lblSectionTitle.text =  NSLocalizedString(@"general_information", nil);
    }
    else
        
    {
        // vwGray.hidden = NO;
        lblSectionTitle.text =  NSLocalizedString(@"account_information", nil);
    }
    
    
    
    
    return vwHeaderTitle;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        if (indexPath.row==arrGeneralInformation.count)
        {
            return 185.0;
        }
        else
            return 77.0;
        
        
    }
    return 77.0;
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 60.0;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    if (section == 0)
    {
        // vwGray.hidden = YES;
        return 0.5;
    }
    else
        
    {
        // vwGray.hidden = NO;
        return 0.001;
    }
    
    
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    
    if (section == 0) {
        return  arrGeneralInformation.count;
    }
    else  if (section == 1)
    {
        return arrAccountInformation.count;
    }
    else
        return 0;
}



-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *) cell forRowAtIndexPath:(NSIndexPath *)indexPath // replace "postTableViewCell" with your cell
{
    
    
    if ([cell isKindOfClass:[EditUserCell class]])
    {
        EditUserCell *editCell = (EditUserCell*)cell;
        
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            // editCell.lblName.font = [UIFont systemFontOfSize:16.0];
            // editCell.txtNameFields.font = [UIFont systemFontOfSize:16.0];
            NSInteger fontIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_FONTSIZE_INDEX"];
            
            CGFloat fontsize;
            if (fontIndex==0)
            {
                fontsize=14.0;
                [[UITextField appearance] setFont:[UIFont systemFontOfSize:fontsize]];
            }
            if (fontIndex==1)
            {
                fontsize=16.0;
                [[UITextField appearance] setFont:[UIFont systemFontOfSize:fontsize]];
                
            }
            if (fontIndex==2)
            {
                fontsize=18.0;
                [[UITextField appearance] setFont:[UIFont systemFontOfSize:fontsize]];
                
            }
            
            
            [editCell setNeedsDisplay];
            
            
        });
        
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    
    // static NSString *CellIdentifier = @"EditUserCell";
    //EditUserCell *editCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    
    
    static NSString *simpleTableIdentifier = @"EditUserCell";
    
    EditUserCell *editCell =(EditUserCell *) [self.tblProfileEdit dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (editCell == nil) {
        editCell = [[EditUserCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    //editCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    
    
    //  editCell.lblName.font = [UIFont systemFontOfSize:16.0];
    // editCell.txtNameFields.font = [UIFont systemFontOfSize:16.0];
    
    editCell.lblName.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    
    
    /*
     static NSString *CellIdentifier = @"EditUserCell";
     EditUserCell *editCell = (EditUserCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
     editCell.lblName.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
     // Configure cell
     if (editCell== nil)
     {
     editCell = [[EditUserCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
     }
     */
    /* arrGeneralInformation = [[NSMutableArray alloc]initWithObjects:@"NAME",@"GENDER",@"DATE OF BIRTH",@"QUALIFICATION",@"OCCUPATION",@"STATE/UI",@"DISTRICT",@"ADDRESS", nil];
     arrAccountInformation = [[NSMutableArray alloc]initWithObjects:@"EMAIL ADDRESS",@"ALTERNATE MOBILE NUMBER", nil];*/
    
    editCell.txtNameFields.delegate=self;
    editCell.txtNameFields.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
    
    
    // [editCell.lblName setFont:[UIFont systemFontOfSize:16]];
    //[editCell.txtNameFields setFont:[UIFont systemFontOfSize:16]];
    
    if (indexPath.section == 0)
    {
        editCell.lblName.text = [arrGeneralInformation objectAtIndex:indexPath.row];
        
        //[editCell.btnDropDown setImage:[UIImage imageNamed:@"icon_arrow-1"] forState:UIControlStateNormal];
        // [editCell.btnDropDown setTitle:@"" forState:UIControlStateNormal];
        
        editCell.btnDropDown.hidden=TRUE;
        [editCell.btnDropDown setTitle:@"" forState:UIControlStateNormal];
        [editCell.btnDropDown setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        
        if (indexPath.row==0)
        {
            editCell.txtNameFields.enabled=TRUE;
            editCell.txtNameFields.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
            
            editCell.txtNameFields.tag=101;
            editCell.btnDropDown.hidden=TRUE;
            editCell.accessoryType = UITableViewCellAccessoryNone;
            
            //[editCell.lblName setFont:[UIFont systemFontOfSize:16]];
            // [editCell.txtNameFields setFont:[UIFont systemFontOfSize:16]];
            editCell.txtNameFields.text=user_name;
            
            
            editCell.txtNameFields.clearButtonMode = UITextFieldViewModeWhileEditing;
            
            
        }
        if (indexPath.row==1)
        {
            editCell.txtNameFields.enabled=FALSE;
            
            //  [editCell.lblName setFont:[UIFont systemFontOfSize:16]];
            // [editCell.txtNameFields setFont:[UIFont systemFontOfSize:16]];
            editCell.txtNameFields.text=singleton.notiTypeGenderSelected;
            
            
            editCell.btnDropDown.hidden=FALSE;
            editCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
        }
        if (indexPath.row==2)
        {
            editCell.txtNameFields.enabled=FALSE;
            
            // [editCell.lblName setFont:[UIFont systemFontOfSize:16]];
            //  [editCell.txtNameFields setFont:[UIFont systemFontOfSize:16]];
            
            editCell.txtNameFields.text=user_dob;
            
            editCell.btnDropDown.hidden=FALSE;
            editCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
        }
        if (indexPath.row==3)
        {
            editCell.txtNameFields.enabled=FALSE;
            editCell.btnDropDown.hidden=FALSE;
            
            //  [editCell.lblName setFont:[UIFont systemFontOfSize:16]];
            // [editCell.txtNameFields setFont:[UIFont systemFontOfSize:16]];
            
            
            editCell.txtNameFields.text=user_quali;
            
            editCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            
        }
        if (indexPath.row==4)
        {
            editCell.txtNameFields.enabled=FALSE;
            editCell.btnDropDown.hidden=FALSE;
            
            //   [editCell.lblName setFont:[UIFont systemFontOfSize:16]];
            //   [editCell.txtNameFields setFont:[UIFont systemFontOfSize:16]];
            
            
            editCell.txtNameFields.text=user_Occup;
            editCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            
        }
        if (indexPath.row==5)
        {
            
            editCell.btnDropDown.hidden=FALSE;
            
            //  [editCell.lblName setFont:[UIFont systemFontOfSize:16]];
            //  [editCell.txtNameFields setFont:[UIFont systemFontOfSize:16]];
            
            
            editCell.txtNameFields.text=user_state;
            editCell.txtNameFields.enabled=FALSE;
            
            editCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
        }
        if (indexPath.row==6)
        {
            editCell.btnDropDown.hidden=FALSE;
            
            //  [editCell.lblName setFont:[UIFont systemFontOfSize:16]];
            //  [editCell.txtNameFields setFont:[UIFont systemFontOfSize:16]];
            
            
            editCell.txtNameFields.text=user_dist;
            editCell.txtNameFields.enabled=FALSE;
            
            editCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            
        }
        if (indexPath.row==7)
        {
            editCell.txtNameFields.enabled=TRUE;
            editCell.btnDropDown.hidden=TRUE;
            
            //  [editCell.lblName setFont:[UIFont systemFontOfSize:16]];
            //  [editCell.txtNameFields setFont:[UIFont systemFontOfSize:16]];
            
            
            editCell.txtNameFields.text=user_address;
            editCell.txtNameFields.tag=201;
            editCell.txtNameFields.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
            
            
            editCell.accessoryType = UITableViewCellAccessoryNone;
            //UIView *vw_line=[[UIView alloc]initWithFrame:CGRectMake(15,editCell.txtNameFields.frame.origin.y+ editCell.txtNameFields.frame.size.height-1, fDeviceWidth-15, 0.5)];
            // vw_line.backgroundColor=[UIColor lightGrayColor];
            //  [editCell.contentView addSubview:vw_line];
            editCell.txtNameFields.clearButtonMode = UITextFieldViewModeWhileEditing;
            
            
            
        }
        
        
        
    }
    else  if (indexPath.section == 1)
        
    {
        {
            editCell.lblName.text = [arrAccountInformation objectAtIndex:indexPath.row];
            
            [editCell.btnDropDown setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            editCell.accessoryType = UITableViewCellAccessoryNone;
            
            //   [editCell.lblName setFont:[UIFont systemFontOfSize:16]];
            //   [editCell.txtNameFields setFont:[UIFont systemFontOfSize:16]];
            
            
            if ([[UIScreen mainScreen]bounds].size.height >= 1024)
            {
                editCell.btnDropDown.frame = CGRectMake(self.view.frame.size.width-100, 25, 80, 50);
                
            }
            else
            {
                editCell.btnDropDown.frame = CGRectMake(self.view.frame.size.width-100, 25, 80, 50);
            }
            
            
            if (indexPath.row==0)
            {
                editCell.txtNameFields.enabled=FALSE;
                editCell.btnDropDown.hidden=FALSE;
                //NSLog(@"indexpath row EMAIL=%d",indexPath.row);
                
                editCell.txtNameFields.text=user_email;
                editCell.txtNameFields.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
                
                CGRect butnFrame =  editCell.btnDropDown.frame;
                butnFrame.origin.x = singleton.isArabicSelected ? 10: CGRectGetWidth(tableView.frame) - 80;
                editCell.btnDropDown.frame = butnFrame;
                
                editCell.txtNameFields.tag= 301;
                
                if (editCell.txtNameFields.text.length>0)
                    
                {
                    //[editCell.btnDropDown setTitle:NSLocalizedString(@"change", nil) forState:UIControlStateNormal];
                    [editCell.btnDropDown setImage:[UIImage imageNamed:@"more_info_New"] forState:UIControlStateNormal];
                    
                    
                    [editCell.btnDropDown setTitle:@"" forState:UIControlStateNormal];
                    
                }
                else
                {
                    [editCell.btnDropDown setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                    
                    
                    
                    
                    [editCell.btnDropDown setTitle:NSLocalizedString(@"add", nil) forState:UIControlStateNormal];
                    
                }
            }
            else if (indexPath.row==1)
            {
                editCell.txtNameFields.enabled=FALSE;
                editCell.btnDropDown.hidden=FALSE;
                //NSLog(@"indexpath row ALTERNATE MOBILE=%d",indexPath.row);
                editCell.txtNameFields.text=user_altermob;
                editCell.txtNameFields.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
                
                
                CGRect butnFrame =  editCell.btnDropDown.frame;
                butnFrame.origin.x = singleton.isArabicSelected ? 15: CGRectGetWidth(tableView.frame) - 80;
                editCell.btnDropDown.frame = butnFrame;
                
                
                if (editCell.txtNameFields.text.length>0)
                {
                    //  [editCell.btnDropDown setTitle:NSLocalizedString(@"change", nil) forState:UIControlStateNormal];
                    [editCell.btnDropDown setImage:[UIImage imageNamed:@"more_info_New"] forState:UIControlStateNormal];
                    
                    [editCell.btnDropDown setTitle:@"" forState:UIControlStateNormal];
                    
                }
                else
                {
                    [editCell.btnDropDown setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
                    
                    
                    
                    
                    [editCell.btnDropDown setTitle:NSLocalizedString(@"add", nil) forState:UIControlStateNormal];
                }
                
            }
            
            
            
        }
        
        editCell.btnDropDown.userInteractionEnabled=NO;
        
    }
    NSInteger fontIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"SELECTED_FONTSIZE_INDEX"];
    CGFloat fontsize;
    if (fontIndex==0)
    {
        fontsize=14.0;
        [[UITextField appearance] setFont:[UIFont systemFontOfSize:fontsize]];
    }
    if (fontIndex==1)
    {
        fontsize=16.0;
        [[UITextField appearance] setFont:[UIFont systemFontOfSize:fontsize]];
        
    }
    if (fontIndex==2)
    {
        fontsize=18.0;
        [[UITextField appearance] setFont:[UIFont systemFontOfSize:fontsize]];
        
    }
    
    
    //   editCell.lblName.font = [UIFont systemFontOfSize:16.0];
    //   editCell.txtNameFields.font = [UIFont systemFontOfSize:16.0];
    editCell.selectionStyle = UITableViewCellSelectionStyleNone;
    [editCell setNeedsDisplay];
    
    return editCell;
}




- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag==101)
    {
        singleton.profileNameSelected=textField.text;
        user_name=singleton.profileNameSelected;
    }
    else if (textField.tag==201)
    {
        singleton.profileUserAddress=textField.text;
        user_address=textField.text;
        
    }
    
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

//-(void)showToast :(NSString *)toast {
//    iToast * objiTost = [iToast makeText:toast];
//    [objiTost setFontSize:11];
//    [objiTost setDuration:iToastDurationNormal];
//    [objiTost setGravity:iToastGravityBottom];
//    [objiTost show];
//}

/*
 #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }
 */
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

