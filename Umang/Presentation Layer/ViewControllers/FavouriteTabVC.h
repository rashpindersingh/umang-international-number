//
//  FavouriteTabVC.h
//  Umang
//
//  Created by spice on 15/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavouriteTabVC : UIViewController <UITextFieldDelegate>

{
    
    IBOutlet UITextField *txt_searchField;
    IBOutlet UIView *vw_line;
    UIRefreshControl *refreshController;
    IBOutlet UIView *vw_noServiceFound;
    
    __weak IBOutlet UILabel *toAddFavouriteLabel;
    
    IBOutlet UILabel *noFavouriteFoundLabel;
    
    IBOutlet UIImageView *searchIconImage;
    
}
-(IBAction)btn_noticationAction:(id)sender;
-(IBAction)btn_filterAction:(id)sender;

@end
