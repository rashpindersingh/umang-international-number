//
//  FilterViewController.h
//  Umang
//
//  Created by admin on 07/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnBackHome;
@property (weak, nonatomic) IBOutlet UILabel *lblFilter;
@property (weak, nonatomic) IBOutlet UIButton *btnReset;


@property (weak, nonatomic) IBOutlet UITableView *tblFilter;
@property (nonatomic, assign) BOOL isRegionalSelected;
@property (weak, nonatomic) IBOutlet UIButton *btnApply;

@property(nonatomic,strong) NSString *serviceName;

@property(nonatomic,strong) NSString *fromdatestring;
@property(nonatomic,strong) NSString *todatestring;


- (IBAction)btnApplyClicked:(id)sender;
- (IBAction)btnbackClicked:(id)sender;
- (IBAction)btnResetClicked:(id)sender;
@end
