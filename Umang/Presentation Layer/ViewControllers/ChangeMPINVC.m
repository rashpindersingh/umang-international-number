//
//  ChangeMPINVC.m
//  Umang
//
//  Created by admin on 21/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "ChangeMPINVC.h"
#define kOFFSET_FOR_KEYBOARD 80.0

#import "MyTextField.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"
//#import "LoginViewController.h"
#import "LoginAppVC.h"

#define MAX_LENGTH  4

@interface ChangeMPINVC ()<UITextFieldDelegate>
{
    
    SharedManager *singleton;
    
    __weak IBOutlet UIButton *btnBack;
    NSString *mpinStr;
    NSString *NewmpinStr;
    NSString *CNewmpinStr;
    
    MBProgressHUD *hud ;
    
    IBOutlet UIScrollView *scrollview;
}

@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_titleMsg;
@property (weak, nonatomic) IBOutlet UILabel *lbl_enterMpin;
@property (weak, nonatomic) IBOutlet UITextField *txt_mpin1;


@property (weak, nonatomic) IBOutlet UILabel *lbl_NMpin;

@property (weak, nonatomic) IBOutlet UITextField *txt_Nmpin1;



@property (weak, nonatomic) IBOutlet UILabel *lbl_cNMpin;
@property (weak, nonatomic) IBOutlet UITextField *txt_cNmpin1;



@property (weak, nonatomic) IBOutlet UILabel *lbl_errorMsg;
@property (weak, nonatomic) IBOutlet UIImageView *img_error;
@property (weak, nonatomic) IBOutlet UIButton *btn_next;


@property (weak, nonatomic) IBOutlet UILabel *vw_line1;
@property (weak, nonatomic) IBOutlet UILabel *vw_line2;
@property (weak, nonatomic) IBOutlet UILabel *vw_line3;




@end

@implementation ChangeMPINVC
@synthesize txt_mpin1;
@synthesize  txt_cNmpin1;

@synthesize txt_Nmpin1;

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

-(void)cancelNumberPad
{
    
    [txt_mpin1 resignFirstResponder];
    [txt_cNmpin1 resignFirstResponder];
    [txt_Nmpin1 resignFirstResponder];
    
}





- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.view.frame.size.width > 500)
    {
        self.btn_next.frame = CGRectMake(200, self.btn_next.frame.origin.y, 368, 65);
    }
    
    [self.btn_next setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [self.btn_next setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btn_next.layer.cornerRadius = 3.0f;
    self.btn_next.clipsToBounds = YES;
    
    [txt_mpin1 becomeFirstResponder];
    singleton = [SharedManager sharedSingleton];
    
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:CHANGE_MPIN_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    self.lbl_title.text =  NSLocalizedString(@"changempin_label", nil);
    self.lbl_enterMpin.text = NSLocalizedString(@"old_mpin", nil);
    self.lbl_NMpin.text = NSLocalizedString(@"enter_new_mpin", nil);
    self.lbl_cNMpin.text = NSLocalizedString(@"confirm_new_mpin", nil);
    self.lbl_errorMsg.text = NSLocalizedString(@"mpin_donot_match_txt", nil);
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.backgroundColor = [UIColor lightGrayColor];
    numberToolbar.tintColor = [UIColor whiteColor];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"apply_label", nil) style:UIBarButtonItemStyleDone target:self action:@selector(cancelNumberPad)]];
    [numberToolbar sizeToFit];
    txt_mpin1.inputAccessoryView = numberToolbar;
    txt_Nmpin1.inputAccessoryView = numberToolbar;
    txt_cNmpin1.inputAccessoryView = numberToolbar;
    
    // Do any additional setup after loading the view.
    
    [self textfieldInit];
    
    
    
    //----- Setting delegate for Custom textfield so back space operation work smooth
    
    self.lbl_errorMsg.hidden=TRUE;
    self.img_error.hidden=TRUE;
    [self enableBtnNext:NO];
    self.btn_next.enabled=NO;
    self.view.userInteractionEnabled = YES;
    
    
    
    self.view.backgroundColor= [UIColor colorWithRed:235/255.0 green:234/255.0 blue:241/255.0 alpha:1.0];
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
    
    //--------- Code for handling -------------------
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       CGRect contentRect = CGRectZero;
                       for (UIView *view in scrollview.subviews)
                           contentRect = CGRectUnion(contentRect, view.frame);
                       
                       contentRect.size.height=contentRect.size.height+150;
                       scrollview.contentSize = contentRect.size;
                   });
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
}

-(void)textfieldInit
{
    
    
    txt_mpin1.delegate = self;
    txt_Nmpin1.delegate = self;
    txt_cNmpin1.delegate = self;
    
    //-----dsrawat4u-------
    // [txt_mpin1 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    // [txt_Nmpin1 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    //[txt_cNmpin1 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    //-----dsrawat4u-------
    
    
    
    
}






-(void)hideKeyboard
{
    [scrollview setContentOffset:
     CGPointMake(0, -scrollview.contentInset.top) animated:YES];
    
    [self cancelNumberPad];
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

-(void)viewWillAppear:(BOOL)animated
{
    
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.shouldRotate = NO;
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    
    
    
    
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    [self setViewFont];
    [super viewWillAppear:NO];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    [_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
    _lbl_title.font = [AppFont semiBoldFont:23.0];
    _lbl_titleMsg.font = [AppFont semiBoldFont:16.0];
    _lbl_enterMpin.font = [AppFont mediumFont:16.0];
    _lbl_NMpin.font = [AppFont mediumFont:16.0];
    _lbl_cNMpin.font = [AppFont mediumFont:16.0];
    _lbl_errorMsg.font = [AppFont regularFont:14.0];
    txt_mpin1.font = [AppFont regularFont:21];
    txt_Nmpin1.font = [AppFont regularFont:21];
    txt_cNmpin1.font = [AppFont regularFont:21];
    
}

-(IBAction)backbtnAction:(id)sender
{
    // [self dismissViewControllerAnimated:NO completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:NO completion:nil];
}




-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (textField == txt_cNmpin1)
    {
        self.view.frame = CGRectMake(0, -90, self.view.frame.size.width, self.view.frame.size.height);
    }
    
    
    
    if (fDeviceHeight<=568) {
        [scrollview setContentSize:CGSizeMake(scrollview.frame.size.width, scrollview.frame.size.height+100)];
        [scrollview setContentOffset:CGPointMake(0, 40) animated:YES];
    }
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == txt_cNmpin1)
    {
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
    
    [scrollview setContentSize:CGSizeMake(scrollview.frame.size.width, scrollview.frame.size.height)];
    [scrollview setContentOffset:CGPointZero animated:YES];
    
}

//----- Code to overide delegate method of my custom Textfield so user can trace backspace with empty textfield

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (fDeviceHeight<=568) {
        UITouch * touch = [touches anyObject];
        if(touch.phase == UITouchPhaseBegan) {
            
            [self cancelNumberPad];
        }
    }
}





//---Code for hangle jump from one textfield to another while filling values
- (void)textFieldDidChange:(UITextField *)textField
{
    self.lbl_errorMsg.hidden=TRUE;
    self.img_error.hidden=TRUE;
    
    [self enableBtnNext:NO];
    self.btn_next.enabled=NO;
    
    
    if (textField.text.length >=MAX_LENGTH)
    {
        if ([textField isEqual:self.txt_mpin1]) {
            textField.text = [textField.text substringToIndex:MAX_LENGTH];
            [self.txt_Nmpin1 becomeFirstResponder];
            [self checkValidation];
            
        }
        else if([textField isEqual:self.txt_Nmpin1]) {
            textField.text = [textField.text substringToIndex:MAX_LENGTH];
            [self.txt_cNmpin1 becomeFirstResponder];
            
            [self checkValidation];
            
        }
        else if([textField isEqual:self.txt_cNmpin1]) {
            textField.text = [textField.text substringToIndex:MAX_LENGTH];
            [textField resignFirstResponder];
            [self checkValidation];
        }
        
    }
    
    //-------start dsrawat4u---------
    
    else
    {
        if (textField.text.length == 0)
        {
            
            if ([textField isEqual:self.txt_cNmpin1])
            {
                textField.text = [textField.text substringToIndex:0];
                
                [self.txt_Nmpin1 becomeFirstResponder];
            }
            
            if ([textField isEqual:self.txt_Nmpin1])
            {
                textField.text = [textField.text substringToIndex:0];
                
                [self.txt_mpin1 becomeFirstResponder];
            }
            
            if ([textField isEqual:self.txt_mpin1])
            {
                textField.text = [textField.text substringToIndex:0];
                
                [textField resignFirstResponder];
            }
            
            
        }
        
    }
    
    
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    //Replace the string manually in the textbox
    textField.text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //perform any logic here now that you are sure the textbox text has changed
    [self textFieldDidChange:textField];
    return NO; //this make iOS not to perform any action
}

//-------end dsrawat4u---------


-(void)checkValidation

{
    mpinStr=[NSString stringWithFormat:@"%@",self.txt_mpin1.text];
    NewmpinStr=[NSString stringWithFormat:@"%@",self.txt_Nmpin1.text];
    CNewmpinStr=[NSString stringWithFormat:@"%@",self.txt_cNmpin1.text];
    [self enableDisableNextwithError];
    
    
}

-(void)enableDisableNextwithError
{
    self.lbl_errorMsg.hidden=TRUE;
    self.img_error.hidden=TRUE;
    if(([mpinStr length]>=4) && ([NewmpinStr length]>=4)&& ([CNewmpinStr length]>=4 ))
    {
        //disable
        
        self.btn_next.enabled=YES;
        [self enableBtnNext:YES];
    }
    else
    {
        
        [self enableBtnNext:NO];
        self.btn_next.enabled=NO;
        
        //enable
    }
    /*   if ([NewmpinStr isEqualToString:CNewmpinStr])
     {
     //Enable button and jump to next view
     self.lbl_errorMsg.hidden=TRUE;
     self.img_error.hidden=TRUE;
     self.btn_next.enabled=YES;
     [self enableBtnNext:YES];
     }
     else
     {
     self.lbl_errorMsg.hidden=false;
     self.img_error.hidden=false;
     [self enableBtnNext:NO];
     self.btn_next.enabled=NO;
     }
     */
    
}

-(void)enableBtnNext:(BOOL)status
{
    if (status ==YES)
    {
        [self hideKeyboard];
        
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateNormal];
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateSelected];
        [self.btn_next setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];
        
    }
    else
    {
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateNormal];
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateSelected];
        
        [self.btn_next setBackgroundColor:[UIColor colorWithRed:176.0/255.0f green:176.0/255.0f blue:176.0/255.0f alpha:1.0f]];
    }
    
}

-(IBAction)jumptoNextView:(id)sender
{
    if ([mpinStr length]<4)
    {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"enter_mpin_to_verify", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
        [alert show];
    }
    else  if ([NewmpinStr length]<4)
    {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"enter_mpin_txt", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
        [alert show];
        
        
    }
    else  if ([CNewmpinStr length]<4) {
        
        
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"enter_mpin_txt", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
        
        [alert show];
        
        
    }
    else  if (![NewmpinStr isEqualToString:CNewmpinStr])
    {
        
        
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"mpin_donot_match_txt", nill) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
        txt_mpin1.text = @"";
        txt_cNmpin1.text = @"";
        txt_Nmpin1.text = @"";
        
        [alert show];
        
        
    }
    
    else
    {
        [self hitAPI];
    }
    
    
    
    
}

//----- hitAPI for IVR OTP call Type registration ------

-(void)hitAPI
{
    
    
    
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    NSString *strSaltMPIN = SaltMPIN;//[[SharedManager sharedSingleton] getKeyWithTag:KEYCHAIN_SaltMPIN];

    NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",mpinStr,strSaltMPIN];
    NSString *mpinStrEncrypted=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
    
    
    
    
    NSString *encrytmCPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",CNewmpinStr,strSaltMPIN];
    NSString *CmpinStrEncrypted=[encrytmCPinWithSalt sha256HashFor:encrytmCPinWithSalt];
    
    
    
    
    //    NSString *encrytSHA256=[NSString stringWithFormat:@"|%@|%@|",mpinStr,SaltRequestControl];
    //    NSString *mpinStrEncrypted=[mpinStr sha256HashFor:encrytSHA256];
    //
    singleton.user_mpin=mpinStr; //save value in local db
    
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    
    
    [dictBody setObject:@"" forKey:@"st"];  //state id Optional
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];  //token
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [dictBody setObject:mpinStrEncrypted forKey:@"mpin"];  //MPIN
    [dictBody setObject:CmpinStrEncrypted forKey:@"nmpin"];  //New MPIN
    
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_CHANGE_MPIN withBody:dictBody andTag:TAG_REQUEST_CHANGE_MPIN completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            //  NSString *rc=[response valueForKey:@"rc"];
            //  NSString *rs=[response valueForKey:@"rs"];
            // NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];
            
            // NSLog(@"value of rc =%@ \n value of rs=%@ \n value of tkn=%@ ",rc,rs,tkn);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                
                [self alertwithMsg:rd];
            }
            
        }
        else{
            
            
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            txt_mpin1.text=@"";
            txt_Nmpin1.text = @"";
            txt_cNmpin1.text =@"";
            [alert show];
            
        }
        
    }];
    
}

-(void)alertwithMsg:(NSString*)msg
{
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"success", nil) message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                   {
                                       
                                       [self openLoginView];
                                       
                                   }];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)clearValueOnlogout
{
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:NO forKey:@"LOGIN_KEY"];
    [defaults synchronize];
    
    //------------------------- Encrypt Value------------------------
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    // Encrypt
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"TOKEN_KEY"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"USER_PIC"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"PROFILE_COMPELTE_KEY"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    //------------------------- Encrypt Value------------------------
    //——Remove Sharding Value——
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"NODE_KEY"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //----- remove sharding--------
    
    
    
    singleton.profileUserAddress = @"";
    singleton.imageLocalpath=@"";
    singleton.notiTypeGenderSelected=@"";
    singleton.profileNameSelected =@"";
    singleton.profilestateSelected=@"";
    singleton.notiTypeCitySelected=@"";
    singleton.notiTypDistricteSelected=@"";
    singleton.profileDOBSelected=@"";
    singleton.altermobileNumber=@"";
    singleton.user_Qualification=@"";
    singleton.user_Occupation=@"";
    singleton.user_profile_URL=@"";
    singleton.profileEmailSelected=@"";
    singleton.mobileNumber=@"";
    singleton.user_id=@"";
    singleton.user_tkn=@"";
    singleton.user_mpin=@"";
    singleton.user_aadhar_number=@"";
    singleton.objUserProfile = nil;
    singleton.imageLocalpath=@"";
    @try {
        [singleton.arr_recent_service removeAllObjects];
        
    } @catch (NSException *exception)
    {
        
    } @finally {
        
    }
    
    
    [[NSUserDefaults standardUserDefaults] setInteger:kLoginScreenCase forKey:kInitiateScreenKey];
    
    
    
    //------------------------- Encrypt Value------------------------
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    // Encrypt
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"lastFetchDate"];
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"lastFetchV1"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"USER_ID"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"TOKEN_KEY"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"USER_PIC"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"PROFILE_COMPELTE_KEY"];
    
    //------------------------- Encrypt Value------------------------
    
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSHTTPCookie *cookie;
    for (cookie in [storage cookies])
    {
        
        [storage deleteCookie:cookie];
        
    }
    NSMutableArray *cookieArray = [[NSMutableArray alloc] init];
    [[NSUserDefaults standardUserDefaults] setValue:cookieArray forKey:@"cookieArray"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //——Remove Sharding Value——
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"NODE_KEY"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // Logout from social frameworks as well.
    
    
    //------------------------- Encrypt Value------------------------
    [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
    // Encrypt
    [[NSUserDefaults standardUserDefaults] encryptValue:@"NO" withKey:@"LINKDIGILOCKERSTATUS"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"digilocker_username"];
    [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"digilocker_password"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //------------------------- Encrypt Value------------------------
    
    [singleton.dbManager deleteBannerHomeData];
    [singleton.dbManager  deleteAllServices];
    [singleton.dbManager  deleteSectionData];
    singleton.imageLocalpath=@"";
    singleton.dbManager = [[UMSqliteManager alloc] initWithDatabaseFilename:@"UMANG_DATABASE.db"];
    [singleton.dbManager createUmangDB];
    // Start the notifier, which will cause the reachability object to retain itself!
    
    [defaults synchronize];
    
    
    
}


-(void)openLoginView
{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    [objRequest clearValueOnlogout];
    [singleton.reach stopNotifier];
    //----later add
    [[NSUserDefaults standardUserDefaults] setInteger:kLoginScreenCase forKey:kInitiateScreenKey];
    [[NSUserDefaults standardUserDefaults] setBool:false forKey:kKeepMeLoggedIn];
    [[NSUserDefaults standardUserDefaults] synchronize];    //------
    
    
    
    LoginAppVC *vc;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginAppVC"];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        vc = [[LoginAppVC alloc] initWithNibName:@"LoginAppVC_iPad" bundle:nil];
    }
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.window.rootViewController = vc;
    [self presentViewController:vc animated:YES completion:nil];
    
    
    
    
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}



/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
