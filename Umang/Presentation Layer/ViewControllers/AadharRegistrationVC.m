

//
//  AadharRegistrationVC.m
//  Umang
//
//  Created by admin on 05/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "AadharRegistrationVC.h"
#import "MyTextField.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"
#import "UserEditVC.h"
#import "RegStep2ViewController.h"
#define kOFFSET_FOR_KEYBOARD 80.0



@interface AadharRegistrationVC ()<MyTextFieldDelegate,UIScrollViewDelegate>

{
    BOOL flag_Accept;
    MBProgressHUD *hud;
    NSString *aadharStr;
    
    SharedManager *singleton;
    IBOutlet UIScrollView *scrollview;
    int tagAPI;
    
    __weak IBOutlet UILabel *lblHeader;
    
    __weak IBOutlet UILabel *lblTip;
    __weak IBOutlet UIButton *btnBack;
    __weak IBOutlet UILabel *lblAgreeTerms;
    __weak IBOutlet UILabel *lblVerificationOTP;
    __weak IBOutlet UILabel *lblRegisterAadhar;
    
}
@property (weak, nonatomic) IBOutlet UIButton *btn_next;
@property (weak, nonatomic) IBOutlet MyTextField *txt_aadhar1;
@property (weak, nonatomic) IBOutlet MyTextField *txt_aadhar2;
@property (weak, nonatomic) IBOutlet MyTextField *txt_aadhar3;


@end

@implementation AadharRegistrationVC
@synthesize txt_aadhar1,txt_aadhar2,txt_aadhar3;
@synthesize TYPE_AADHAR_REGISTRATION_FROM;



@synthesize tout,rtry;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.btn_next setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [self.btn_next setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btn_next.layer.cornerRadius = 3.0f;
    self.btn_next.clipsToBounds = YES;
    
     [txt_aadhar1 becomeFirstResponder];
    // Do any additional setup after loading the view.
    
      NSString *str = NSLocalizedString(@"tip", nil);
    lblTip.text = [NSString stringWithFormat:@"%@%@",str,NSLocalizedString(@"aadhaar_mob_tip", nil)];
    
 
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: AADHAR_REGISTRATION_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    lblHeader.text = NSLocalizedString(@"adhaar_details", nil);
    lblRegisterAadhar.text = NSLocalizedString(@"aadhar_intro", nil);
    
   
    lblAgreeTerms.text = NSLocalizedString(@"aadhaar_consent_txt", nil);
    lblAgreeTerms.adjustsFontSizeToFitWidth = YES;
    lblVerificationOTP.text = NSLocalizedString(@"aadhar_sub_intro", nil);
    

    flag_Accept=TRUE;
    singleton=[SharedManager sharedSingleton];
    //----- Setting delegate for Custom textfield so back space operation work smooth
    
    txt_aadhar1.myDelegate = self;
    txt_aadhar2.myDelegate = self;
    txt_aadhar3.myDelegate = self;
    
    [txt_aadhar1 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [txt_aadhar2 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [txt_aadhar3 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    
    if (TYPE_AADHAR_REGISTRATION_FROM == IS_FROM_REG_STEP3_SCREEN)
    {
        _btnSkip.hidden = NO;
    }
    else if (TYPE_AADHAR_REGISTRATION_FROM == IS_FROM_CHOOSE_REGISTRATION)
    {
        _btnSkip.hidden = YES;
    }
    else if (TYPE_AADHAR_REGISTRATION_FROM == IS_FROM_NOT_LINKED)
    {
        _btnSkip.hidden = YES;
    }
    
    self.btn_next.enabled=NO;
    self.view.userInteractionEnabled = YES;
    
    
    [self.btn_acceptTerm setImage:[UIImage imageNamed:@"img_uncheck-1.png"] forState:UIControlStateNormal];
    [self.btn_acceptTerm setImage:[UIImage imageNamed:@"checkbox_marked.png"] forState:UIControlStateSelected];
    
    
    //------ Add dismiss keyboard while background touch-------
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    
    
    CGFloat scrollViewHeight = 0.0f;
    CGFloat scrollViewWidth = 0.0f;
    for (UIView *view in scrollview.subviews) {
        CGFloat height = (view.frame.size.height + view.frame.origin.y);
        scrollViewHeight = ((height > scrollViewHeight) ? height : scrollViewHeight);
        
        CGFloat width = (view.frame.size.width + view.frame.origin.x);
        scrollViewWidth = ((width > scrollViewWidth) ? width : scrollViewWidth);
    }
    
    [scrollview setContentSize:(CGSizeMake(scrollViewWidth, scrollViewHeight))];
    
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setViewFont];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    [_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    lblHeader.font = [AppFont semiBoldFont:22.0];
    lblVerificationOTP.font = [AppFont mediumFont:14];
    lblRegisterAadhar.font = [AppFont semiBoldFont:16.0];
    txt_aadhar1.font = [AppFont regularFont:21.0];
    txt_aadhar2.font = [AppFont regularFont:21.0];
    txt_aadhar3.font = [AppFont regularFont:21.0];
    lblAgreeTerms.font = [AppFont mediumFont:13];
    lblTip.font = [AppFont lightFont:13];
    [_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}
-(void)hideKeyboard
{
    //[self.view endEditing:YES];
    
    [txt_aadhar1 resignFirstResponder];
    [txt_aadhar2 resignFirstResponder];
    [txt_aadhar3 resignFirstResponder];
    
    
}




- (IBAction)btnSkipClicked:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    UserEditVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"UserEditVC"];
    
    vc.tagFrom=@"ISFROMREGISTRATION";
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
   // vc.Edit_Profile_Choose = IsFromAadharScreen;
    //[self.navigationController pushViewController:vc animated:YES];
    [self presentViewController:vc animated:YES completion:nil];
}



- (IBAction)btnAccept_termsClicked:(UIButton*)sender
{
    sender.selected = !(sender.selected);
    
    
    if (((sender.selected == YES))&&(aadharStr.length ==  12))
    {
        [self enableBtnNext:YES];
        self.btn_next.enabled=YES;
        
    }
    else
    {
        [self enableBtnNext:NO];
        self.btn_next.enabled=NO;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

- (void)textFieldDidDelete:(UITextField *)textField
{
    
    if (textField.text.length == 0)
    {
        
        if ([textField isEqual:txt_aadhar3])
        {
            textField.text = [textField.text substringToIndex:0];
            
            [txt_aadhar2 becomeFirstResponder];
        }
        if ([textField isEqual:txt_aadhar2])
        {
            textField.text = [textField.text substringToIndex:0];
            
            [txt_aadhar1 becomeFirstResponder];
        }
        if ([textField isEqual:txt_aadhar1])
        {
            textField.text = [textField.text substringToIndex:0];
            
            [textField resignFirstResponder];
        }
        
    }
    
}


//---Code for hangle jump from one textfield to another while filling values
- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField.text.length >= 4)
    {
        
        if ([textField isEqual:txt_aadhar1])
        {
            textField.text = [textField.text substringToIndex:4];
            
            [txt_aadhar2 becomeFirstResponder];
        }
        if ([textField isEqual:txt_aadhar2])
        {
            textField.text = [textField.text substringToIndex:4];
            
            [txt_aadhar3 becomeFirstResponder];
        }
        if ([textField isEqual:txt_aadhar3])
        {
            textField.text = [textField.text substringToIndex:4];
            
            [textField resignFirstResponder];
        }
        
        
        [self checkValidation];
        
        // NSLog(@"got it");
    }
    else
    {
        flag_Accept=FALSE;
        [self btnAcceptTerm:self];
        
    }
    
    
}


-(void)checkValidation
{
    aadharStr=[NSString stringWithFormat:@"%@%@%@",txt_aadhar1.text, txt_aadhar2.text, txt_aadhar3.text];
    
    if (aadharStr.length <12)
    {
        //Enable button and jump to next view
        
        [self enableBtnNext:NO];
        self.btn_next.enabled=NO;
        
        
    }
    else
    {
        
        //        self.btn_next.enabled=YES;
        //
        //        [self enableBtnNext:YES];
        
        
    }
    
    
}


-(void)enableBtnNext:(BOOL)status
{
    if (status == YES)
    {
//        [self.btn_next setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateNormal];
//        [self.btn_next setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateSelected];
        
        [self.btn_next setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];
    }
    else
    {
//        [self.btn_next setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateNormal];
//        [self.btn_next setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateSelected];
        
        [self.btn_next setBackgroundColor:[UIColor colorWithRed:176.0/255.0f green:176.0/255.0f blue:176.0/255.0f alpha:1.0f]];
        
    }
    
}


- (IBAction)btnNextClicked:(id)sender
{
    
    /* if (TYPE_AADHAR_REGISTRATION_FROM == IS_FROM_REG_STEP3_SCREEN)
     {
     _btnSkip.hidden = NO;
     }
     else if (TYPE_AADHAR_REGISTRATION_FROM == IS_FROM_CHOOSE_REGISTRATION)
     {
     _btnSkip.hidden = YES;
     }
     else if (TYPE_AADHAR_REGISTRATION_FROM == IS_FROM_NOT_LINKED)
     {
     _btnSkip.hidden = YES;
     }
     
     */
    
    
    if (TYPE_AADHAR_REGISTRATION_FROM == IS_FROM_NOT_LINKED)
    {
        // tagAPI=101;
        [self hitAPIforLinkProfile];
    }
    if (TYPE_AADHAR_REGISTRATION_FROM == IS_FROM_REG_STEP3_SCREEN)
    {
        // tagAPI=101;
        [self hitAPIforLinkProfile];
    }
    
    //TYPE_AADHAR_COMING_FROM
    else if (TYPE_AADHAR_REGISTRATION_FROM == IS_FROM_CHOOSE_REGISTRATION)
    {
        [self hitAPI];
    }
    
    
}

- (IBAction)btnBackClicked:(id)sender {
    
    if (self.TYPE_AADHAR_REGISTRATION_FROM == IS_FROM_NOT_LINKED)//login with OTP
    {
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else
    {[self dismissViewControllerAnimated:NO completion:nil];}
}


-(void)hitAPIforLinkProfile
{
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
  hud.label.text = NSLocalizedString(@"loading",nil);
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    [dictBody setObject:@"rgtadhr" forKey:@"ort"];
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];
    [dictBody setObject:aadharStr forKey:@"aadhr"];
    
    //  singleton.mobileNumber=txtMobileNumber.text; //save mobile number for future use of user
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_REGISTRATION withBody:dictBody andTag:TAG_REQUEST_INIT_REG completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
     {
         [hud hideAnimated:YES];
         
         if (error == nil) {
             NSLog(@"Server Response = %@",response);
             //------ Sharding Logic parsing---------------
             
             
             NSString *node=[response valueForKey:@"node"];
             if([node length]>0)
             {
                 [[NSUserDefaults standardUserDefaults] setValue:node forKey:@"NODE_KEY"];
                 [[NSUserDefaults standardUserDefaults]synchronize];
             }
             
             
             
             
             //------ Sharding Logic parsing---------------
             

             //----- below value need to be forword to next view according to requirement after checking //Android apk-----
            // NSString *man=[[response valueForKey:@"pd"] valueForKey:@"man"];
             
             
             
             //NSString *tmsg=[[response valueForKey:@"pd"] valueForKey:@"tmsg"];
             
             
             tout=[[[response valueForKey:@"pd"] valueForKey:@"tout"] intValue];
             rtry=[[response valueForKey:@"pd"] valueForKey:@"rtry"];
             
             
             
             
            // NSString *wmsg=[[response valueForKey:@"pd"] valueForKey:@"wmsg"];
             
            // NSString *rc=[response valueForKey:@"rc"];
            // NSString *rd=[response valueForKey:@"rd"];
            // NSString *rs=[response valueForKey:@"rs"];
             
             
             //----- End value need to be forword to next view according to requirement after checking Android apk-----
             
             if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
             {
                 
                 //    singleton.mobileNumber=txtMobileNumber.text;
                 
                 UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

                 
                 RegStep2ViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"RegStep2ViewController"];
                 [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                 vc.strAadharNumber = aadharStr;
                 
                 
                 
                 vc.lblScreenTitleName.text = @"Mobile Number Verification";
                 
                 
                 vc.tout=tout;
                 vc.rtry=rtry;
                 vc.TYPE_RESEND_OTP_FROM=ISFROMADHARREGISTRATIONVC;
                 
                 
                 if (TYPE_AADHAR_REGISTRATION_FROM == IS_FROM_REG_STEP3_SCREEN)
                 {
                     
                     vc.TYPE_LOGIN_CHOOSEN = Link_Aadhar_After_Mobile;//hit same api as for link after mobile registration process
                     
                     
                     
                 }
                 
                 else if (TYPE_AADHAR_REGISTRATION_FROM == IS_FROM_NOT_LINKED)
                 {
                     vc.TYPE_LOGIN_CHOOSEN = IS_FROM_PROFILE_LINK;
                     
                 }
                 
                 [self presentViewController:vc animated:YES completion:nil];
                 
                 
                 
                 // from profile link and isfromadharregister
                 
             }
             //             682647029382
         }
         else{
             NSLog(@"Error Occured = %@",error.localizedDescription);
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                             message:error.localizedDescription
                                                            delegate:self
                                                   cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                   otherButtonTitles:nil];
             [alert show];
             
         }
         
     }];
    
    
    
}

-(void)hitAPI
{
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
   hud.label.text = NSLocalizedString(@"loading",nil);
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    [dictBody setObject:@"rgtadhr" forKey:@"ort"];
    
    [dictBody setObject:aadharStr forKey:@"aadhr"];
    
    //  singleton.mobileNumber=txtMobileNumber.text; //save mobile number for future use of user
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_REGISTRATION withBody:dictBody andTag:TAG_REQUEST_INIT_REG completionHandler:^(id response, NSError *error, REQUEST_TAG tag)
     {
         [hud hideAnimated:YES];
         
         if (error == nil) {
             NSLog(@"Server Response = %@",response);
             //------ Sharding Logic parsing---------------
             NSString *node=[response valueForKey:@"node"];
             if([node length]>0)
             {
                 [[NSUserDefaults standardUserDefaults] setValue:node forKey:@"NODE_KEY"];
                 [[NSUserDefaults standardUserDefaults]synchronize];
             }
             

             //------ Sharding Logic parsing---------------
             

             //----- below value need to be forword to next view according to requirement after checking Android apk-----
          //   NSString *man=[[response valueForKey:@"pd"] valueForKey:@"man"];
            // NSString *rtry=[[response valueForKey:@"pd"] valueForKey:@"rtry"];
           //  NSString *tmsg=[[response valueForKey:@"pd"] valueForKey:@"tmsg"];
          //   NSString *tout=[[response valueForKey:@"pd"] valueForKey:@"tout"];
           //  NSString *wmsg=[[response valueForKey:@"pd"] valueForKey:@"wmsg"];
             
           //  NSString *rc=[response valueForKey:@"rc"];
            // NSString *rd=[response valueForKey:@"rd"];
           //  NSString *rs=[response valueForKey:@"rs"];
             
           //  NSLog(@"value of man =%@ \n value of rtry=%@ \n value of tmsg=%@ \n value of tout=%@ \n value of wmsg=%@ \n value of rc=%@ \n value of rd=%@ \n value of rs=%@",man,rtry,tmsg,tout,wmsg,rc,rd,rs);
             //----- End value need to be forword to next view according to requirement after checking Android apk-----
             
             if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
             {
                 
                 //    singleton.mobileNumber=txtMobileNumber.text;
                 
                 
                 UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

                 RegStep2ViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"RegStep2ViewController"];
                 [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                 vc.strAadharNumber = aadharStr;
                 vc.TYPE_RESEND_OTP_FROM=ISFROMADHARREGISTRATIONVC;
                 
                 vc.lblScreenTitleName.text = @"Mobile Number Verification";
                 vc.TYPE_LOGIN_CHOOSEN = IS_FROM_AADHAR_REGISTRATION;
                 [self presentViewController:vc animated:YES completion:nil];
                 
               
             }
             //             682647029382
         }
         else{
             NSLog(@"Error Occured = %@",error.localizedDescription);
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                             message:error.localizedDescription
                                                            delegate:self
                                                   cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                   otherButtonTitles:nil];
             [alert show];
             
         }
         
     }];
    
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (fDeviceHeight<=568) {
        [scrollview setContentSize:CGSizeMake(scrollview.frame.size.width, scrollview.frame.size.height+100)];
        [scrollview setContentOffset:CGPointMake(0, 40) animated:YES];
    }
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [scrollview setContentSize:CGSizeMake(scrollview.frame.size.width, scrollview.frame.size.height)];
    [scrollview setContentOffset:CGPointZero animated:YES];
    
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch * touch = [touches anyObject];
    if(touch.phase == UITouchPhaseBegan) {
        [self.txt_aadhar1 resignFirstResponder];
        [self.txt_aadhar2 resignFirstResponder];
        [self.txt_aadhar3 resignFirstResponder];
        
    }
}

- (IBAction)btnAcceptTerm:(id)sender
{
    
    if (flag_Accept==TRUE) {
        self.btn_acceptTerm.selected=TRUE;
        flag_Accept=FALSE;
        
        
    }
    else
    {
        self.btn_acceptTerm.selected=FALSE;
        flag_Accept=TRUE;
        
        
        
    }
    [self checkValidation];
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}*/

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
