//
//  ChooseRegistrationTypeVC.m
//  Umang
//
//  Created by deepak singh rawat on 22/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "ChooseRegistrationTypeVC.h"
//#import "RegStep1ViewController.h"
#import "AadharRegModuleVC.h"
#import "MobileRegistrationVC.h"


@interface ChooseRegistrationTypeVC ()
{
    __weak IBOutlet UILabel *txtMobile;
    
    __weak IBOutlet UILabel *lblHeader;
    __weak IBOutlet UILabel *txtAadhar;
    __weak IBOutlet UIButton *btnaadhar;
    __weak IBOutlet UIButton *btnMobile;
    MobileRegistrationVC *mobileReg;
    
    UIImageView   *imgAadhar ;
    
    UIImageView *imgArrow ;
    UIImageView   *imgMobile ;
    
    UIImageView *imgArrowDocument ;
}
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@end

@implementation ChooseRegistrationTypeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: CHOOSE_REGISTRATION_TYPE_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    [_btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:_btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(_btnBack.frame.origin.x, _btnBack.frame.origin.y, _btnBack.frame.size.width, _btnBack.frame.size.height);
        
        [_btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        _btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    btnaadhar.layer.cornerRadius = 5.0;
    btnMobile.layer.cornerRadius = 5.0;
    [btnaadhar setTitle:NSLocalizedString(@"aadhaar_no_small", nil) forState:UIControlStateNormal];
     [btnMobile setTitle:NSLocalizedString(@"new_mob_num", nil) forState:UIControlStateNormal];
    txtAadhar.text  = NSLocalizedString(@"register_aadhaar_txt", nil);
     txtMobile.text  = NSLocalizedString(@"register_mobile_txt", nil);
     lblHeader.text  = NSLocalizedString(@"select_registration_mode", nil);
    
    
    if ([[UIScreen mainScreen]bounds].size.height == 1024)
    {
        txtAadhar.font = [UIFont systemFontOfSize:22.0];
        txtMobile.font = [UIFont systemFontOfSize:22.0];
        lblHeader.font = [UIFont boldSystemFontOfSize:30.0];
        btnaadhar.titleLabel.font = [UIFont systemFontOfSize:24.0];
         btnMobile.titleLabel.font = [UIFont systemFontOfSize:24.0];
        //txtAadhar.font = [UIFont systemFontOfSize:22.0];
    }

    
    imgAadhar = [[UIImageView alloc]initWithFrame:CGRectMake(10,(btnaadhar.frame.size.height/2)-16, 34, 38)];
    imgAadhar.image = [UIImage imageNamed:@"icon_aadhaar_Latest.png"];
    [btnaadhar addSubview:imgAadhar];
    
    
    imgArrow = [[UIImageView alloc]initWithFrame:CGRectMake(btnaadhar.frame.size.width-30,(btnaadhar.frame.size.height/2)-12, 20, 24)];
    [btnaadhar addSubview:imgArrow];
    imgArrow.image = [UIImage imageNamed:@"arrow_aadhar.png"];
    
    
    
    
    
    imgMobile = [[UIImageView alloc]initWithFrame:CGRectMake(10,(btnMobile.frame.size.height/2)-16, 34, 38)];
    [btnMobile addSubview:imgMobile];
    imgMobile.image = [UIImage imageNamed:@"icon_mobile_Aadhar.png"];
    
    imgArrowDocument = [[UIImageView alloc]initWithFrame:CGRectMake(btnaadhar.frame.size.width-30,(btnaadhar.frame.size.height/2)-12, 20, 24)];
    [btnMobile addSubview:imgArrowDocument];
    imgArrowDocument.image = [UIImage imageNamed:@"arrow_aadhar.png"];
    
    
}

-(void)updateFrame{
    imgAadhar .frame = CGRectMake(10,(btnaadhar.frame.size.height/2)-16, 34, 38);
    
    imgArrow.frame = CGRectMake(btnaadhar.frame.size.width-30,(btnaadhar.frame.size.height/2)-12, 20, 24);
    imgMobile.frame = CGRectMake(10,(btnMobile.frame.size.height/2)-16, 34, 38);

    imgArrowDocument.frame = CGRectMake(btnaadhar.frame.size.width-30,(btnaadhar.frame.size.height/2)-12, 20, 24);

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnBackClicked:(id)sender {
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)loginWithAadharPressed:(id)sender
{
    NSLog(@"Aadhar Pressed");
    AadharRegModuleVC *vc;
    
    if ([[UIScreen mainScreen]bounds].size.height == 1024)
    {
        vc = [[AadharRegModuleVC alloc] initWithNibName:@"AadharRegModuleVC_iPad" bundle:nil];
        //loadNibNamed:@"MobileRegistrationVC_ipad" owner:self options:nil];
        // cell = (MobileRegistrationVC_ipad *)[nib objectAtIndex:0];
        [self presentViewController:vc animated:YES completion:nil];
        
        
    }
    

    else
        
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"AadharRegModuleVC"];
        [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:vc animated:YES completion:nil];
    
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self updateFrame];

    [self setFontforView:self.view andSubViews:YES];
}

-(void)setFontforView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       if ([view isKindOfClass:[UITextField class]])
                       {
                           
                           UITextField *txtfield = (UITextField *)view;
                           NSString *fonttxtFieldName = txtfield.font.fontName;
                           CGFloat fonttxtsize =txtfield.font.pointSize;
                           txtfield.font = nil;
                           
                           txtfield.font = [UIFont fontWithName:fonttxtFieldName size:fonttxtsize];
                           
                           [txtfield layoutIfNeeded]; //Fixes iOS 9 text bounce glitch
                       }
                       
                       
                   });
    
    if ([view isKindOfClass:[UITextView class]])
    {
        
        UITextView *txtview = (UITextView *)view;
        NSString *fonttxtviewName = txtview.font.fontName;
        CGFloat fontbtnsize =txtview.font.pointSize;
        
        txtview.font = [UIFont fontWithName:fonttxtviewName size:fontbtnsize];
        
    }
    
    
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        NSString *fontName = lbl.font.fontName;
        CGFloat fontSize = lbl.font.pointSize;
        
        lbl.font = [UIFont fontWithName:fontName size:fontSize];
    }
    
    
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *button = (UIButton *)view;
        NSString *fontbtnName = button.titleLabel.font.fontName;
        CGFloat fontbtnsize = button.titleLabel.font.pointSize;
        
        [button.titleLabel setFont: [UIFont fontWithName:fontbtnName size:fontbtnsize]];
    }
    
    if (isSubViews)
    {
        
        for (UIView *sview in view.subviews)
        {
            [self setFontforView:sview andSubViews:YES];
        }
    }
    
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation) interfaceOrientation duration:(NSTimeInterval)duration
{
    
    [self updateFrame];
    
}

-(IBAction)registerPressed:(id)sender
{
    NSLog(@"Register Pressed");
    
    
     if ([[UIScreen mainScreen]bounds].size.height == 1024)
     {
        mobileReg = [[MobileRegistrationVC alloc] initWithNibName:@"MobileRegistrationVC_iPad" bundle:nil];
                     //loadNibNamed:@"MobileRegistrationVC_ipad" owner:self options:nil];
       // cell = (MobileRegistrationVC_ipad *)[nib objectAtIndex:0];
           [self presentViewController:mobileReg animated:YES completion:nil];
        
        
    }
    
    else
   
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"EulaScreen" bundle:nil];

        mobileReg = [storyboard instantiateViewControllerWithIdentifier:@"MobileRegistrationVC"];
        [mobileReg setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:mobileReg animated:YES completion:nil];
    
    }
    
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}*/

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
