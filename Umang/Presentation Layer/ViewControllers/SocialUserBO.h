//
//  SocialUserBO.h
//  Umang
//
//  Created by Kuldeep Saini on 11/28/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define LOGIN_TYPE_TWITTER @"TWITTER"
#define LOGIN_TYPE_FACEBOOK @"FACEBOOK"
#define LOGIN_TYPE_GOOGLE @"GOOGLE"

@interface SocialUserBO : NSObject

@property(nonatomic,strong) NSString *social_id;
@property(nonatomic,strong) NSString *auth_token;
@property(nonatomic,strong) NSString *profile_name;
@property(nonatomic,strong) NSString *profile_email;
@property(nonatomic,strong) NSString *profile_image_url;
@property(nonatomic,strong) UIImage *imgProfile;


@property(nonatomic,strong) NSString *auth_type;

@end
