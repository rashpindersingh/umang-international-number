//
//  SettingsViewController.m
//  Umang
//
//  Created by spice on 16/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import "SettingsViewController.h"
#import "SettingsCell.h"
#import "NewLanguageSelectVC.h"
#import "CustomPickerVC.h"


#import "SettingNotificationVwCon.h"
#import "SecuritySettingVC.h"
#import "HelpSettingVC.h"

#import "UMAPIManager.h"
#import "MBProgressHUD.h"

#import "StateList.h"

@interface SettingsViewController ()<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>
{
    BOOL Flag_switch;
    
    CustomPickerVC *customVC;
    __weak IBOutlet UIButton *btnHelp;
    
    MBProgressHUD *hud;
    
    StateList *obj;
}
@property(nonatomic,retain)NSMutableArray *table_data;
@property(nonatomic,retain)NSMutableArray *table_subdata;
@property(nonatomic,retain)NSMutableArray *table_imgdata;
@property(nonatomic,retain)IBOutlet UITableView *table_settingView;

@end

@implementation SettingsViewController
@synthesize table_data,table_settingView;
@synthesize table_subdata,table_imgdata;
@synthesize header_title_pass,arr_table_pass,TAG_pass;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)callStateAPI
{
    obj=[[StateList alloc]init];
    [obj hitStateQualifiAPI];
    if (singleton.user_StateId.length != 0 && ![singleton.user_StateId isEqualToString:@"9999"]) {
        singleton.stateSelected = [obj getStateName:singleton.user_StateId];
        
    }
}

-(NSString*)getSelectedTabName
{
    
    if (singleton.tabSelectedIndex == 0) {
        singleton.tabSelected=NSLocalizedString(@"home_small", nil);
        
    }
    else if (singleton.tabSelectedIndex == 1) {
        singleton.tabSelected=NSLocalizedString(@"favourites_small", nil);
        
    }
    else if (singleton.tabSelectedIndex == 2) {
        singleton.tabSelected=NSLocalizedString(@"all_services_small", nil);
        
        
    }
    else if (singleton.tabSelectedIndex == 3)
    {
        singleton.tabSelected=NSLocalizedString(@"state_txt", nil);
        
    }
    else
    {
        singleton.tabSelected=NSLocalizedString(@"home_small", nil);
    }
    return singleton.tabSelected;
    
}

-(NSString*)getSelectedFontName{
    
    if (singleton.fontSizeSelectedIndex == 0) {
        singleton.fontSizeSelected=NSLocalizedString(@"small", nil);
        
    }
    else if (singleton.fontSizeSelectedIndex == 1) {
        singleton.fontSizeSelected=NSLocalizedString(@"normal", nil);
        
    }
    else if (singleton.fontSizeSelectedIndex == 2) {
        singleton.fontSizeSelected=NSLocalizedString(@"large", nil);
        
        
    }
    else{
        singleton.fontSizeSelected=NSLocalizedString(@"normal", nil);
        
    }
    return singleton.fontSizeSelected;
    
}


- (void)viewDidLoad
{
    singleton = [SharedManager sharedSingleton];
    if (singleton.user_StateId.length != 0 && ![singleton.user_StateId isEqualToString:@"9999"]) {
        singleton.stateSelected = [obj getStateName:singleton.user_StateId];
        
    }
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:SETTING_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    [_btnReset setTitle:NSLocalizedString(@"help", nil) forState:UIControlStateNormal];
    _lblSettings.text = NSLocalizedString(@"settings", nil);
    
    [btnHelp setTitle:NSLocalizedString(@"help", nil) forState:UIControlStateNormal];
    
    
    
    /*if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"TabKey"] isEqualToString:@"YES"])
    {
        table_data=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"default_tab", nil),NSLocalizedString(@"show_banner", nil),NSLocalizedString(@"notifications", nil),
                    NSLocalizedString(@"language", nil),NSLocalizedString(@"state_txt", nil),
                    NSLocalizedString(@"font_size", nil),NSLocalizedString(@"account", nil), nil];
        
        table_subdata=[[NSMutableArray alloc]initWithObjects:[self getSelectedTabName],singleton.bannerStatus,singleton.notificationSelected, singleton.languageSelected,singleton.notiTypeSelected,[self getSelectedFontName], nil];
        
        table_imgdata=[[NSMutableArray alloc]initWithObjects:@"default_tab",@"icon_show_banner",@"icon_notifications",@"icon_language",@"icon_state-1",@"icon_font",@"icon_account_setting", nil];
    }
    else
    {
        
    }*/
    
    table_data=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"default_tab", nil),NSLocalizedString(@"show_banner", nil),NSLocalizedString(@"notifications", nil),
                NSLocalizedString(@"language", nil),
                NSLocalizedString(@"font_size", nil),NSLocalizedString(@"account", nil), nil];
    
    NSString *bannerData;
    
    bannerData = singleton.bannerStatus == YES ? @"Yes" : @"No";
    
    table_subdata=[[NSMutableArray alloc]initWithObjects:[self getSelectedTabName],bannerData,singleton.notificationSelected, singleton.languageSelected,singleton.notiTypeSelected,[self getSelectedFontName], nil];
    
    table_imgdata=[[NSMutableArray alloc]initWithObjects:@"default_tab",@"icon_show_banner",@"icon_notifications",@"icon_language",@"icon_font",@"icon_account_setting", nil];
    
    
    table_settingView.backgroundColor = [UIColor colorWithRed:234.0/255.0 green:235.0/255.0 blue:241.0/255.0 alpha:1.0];
    Flag_switch=TRUE;
    
    
    //  self.view.backgroundColor=[UIColor colorWithRed:250/255.0 green:250/255.0 blue:250/255.0 alpha:1.0];
    
    self.navigationController.navigationBarHidden=true;
    
    
    //self.navigationController.interactivePopGestureRecognizer.delegate = self;
    
    [self setNeedsStatusBarAppearanceUpdate];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}




//-(void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    if (scrollView.contentOffset.y<=0) {
//        scrollView.contentOffset = CGPointZero;
//    }
//}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}



//----------------------------------------------------------
//          START AFTER LANGUAGE CHANGES
//----------------------------------------------------------

-(void)afterlanguageChangeSettings
{
    //Note if internet is not available then user cannot able to change language
    //steps to follow
    //hitInitAPI
    // delete service data
    //delete service section
    //jump to home view
    ///----- check for internet connection ------------------
    NSURL *scriptUrl = [NSURL URLWithString:@"http://www.google.com/m"];
    NSData *data = [NSData dataWithContentsOfURL:scriptUrl];
    if (data)
    {
        NSLog(@"Device is connected to the Internet");
        [self hitInitAPI];
        
    }
    else
    {
        NSLog(@"Device is not connected to the Internet");
    }
    
    
    
}




-(void)hitInitAPI
{
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    singleton = [SharedManager sharedSingleton];
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:@"" forKey:@"lang"];
    
    NSString *userToken;
    
    if (singleton.user_tkn == nil || singleton.user_tkn.length == 0)
    {
        userToken = @"";
    }
    else
    {
        userToken = singleton.user_tkn;
    }
    
    [dictBody setObject:userToken forKey:@"tkn"];
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_INIT withBody:dictBody andTag:TAG_REQUEST_INIT completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            singleton.arr_initResponse=[[NSMutableArray alloc]init];
            singleton.arr_initResponse=[response valueForKey:@"pd"];
            NSLog(@"singleton.arr_initResponse = %@",singleton.arr_initResponse);
            
            [[NSUserDefaults standardUserDefaults] setObject:singleton.arr_initResponse forKey:@"InitAPIResponse"];
            
            NSString*  abbr=[singleton.arr_initResponse valueForKey:@"abbr"];
            NSLog(@"value of abbr=%@",abbr);
            
            
            if ([abbr length]==0) {
                
                abbr=@"";
                
            }
            
            singleton.user_StateId = [singleton.arr_initResponse valueForKey:@"ostate"];
            [singleton setStateId:singleton.user_StateId];
            
            NSString *emblemString = [singleton.arr_initResponse valueForKey:@"stemblem"];
            emblemString = emblemString.length == 0 ? @"":emblemString;
            [[NSUserDefaults standardUserDefaults] setObject:emblemString forKey:@"EMB_STR"];
            
            [[NSUserDefaults standardUserDefaults] setObject:abbr forKey:@"ABBR_KEY"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
            //------------------------- Encrypt Value------------------------
            [[NSUserDefaults standardUserDefaults] setAESKey:@"UMANGIOSAPP"];
            // Encrypt
            [[NSUserDefaults standardUserDefaults] encryptValue:@"" withKey:@"lastFetchDate"];
            [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"lastFetchV1"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            //------------------------- Encrypt Value------------------------
            
            NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
            NSHTTPCookie *cookie;
            for (cookie in [storage cookies]) {
                
                [storage deleteCookie:cookie];
                
            }
            NSMutableArray *cookieArray = [[NSMutableArray alloc] init];
            [[NSUserDefaults standardUserDefaults] setValue:cookieArray forKey:@"cookieArray"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [singleton.dbManager deleteBannerHomeData];
            [singleton.dbManager  deleteAllServices];
            [singleton.dbManager  deleteSectionData];
            
            [self performSelector:@selector(homeviewJump) withObject:nil afterDelay:1];
            
            
            
            // jump to home view  tab
            
            /* facebooklink
             faq
             forceupdate
             googlepluslink
             opensource
             privacypolicy
             splashScreen
             tabordering
             termsandcondition
             twitterlink
             ver
             vermsg
             */
            
            //------ save value in nsuserdefault for relanch app
            // [[NSUserDefaults standardUserDefaults] setObject:response forKey:@"TOUR_Key"];
            //[[NSUserDefaults standardUserDefaults] synchronize];
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
             message:error.localizedDescription
             delegate:self
             cancelButtonTitle:@"OK"
             otherButtonTitles:nil];
             [alert show];*/
        }
        
    }];
    
}

-(void)homeviewJump
{
    
    // UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UITabBarController *tbc = [storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
    // tbc.selectedIndex=[SharedManager getSelectedTabIndex];
    tbc.selectedIndex=0;
    
    [self presentViewController:tbc animated:NO completion:nil];
}
//----------------------------------------------------------
//          END AFTER LANGUAGE CHANGES
//----------------------------------------------------------






-(void)savePreferences
{
    [[NSUserDefaults standardUserDefaults] setInteger:singleton.tabSelectedIndex forKey:@"SELECTED_TAB_INDEX"];
    [[NSUserDefaults standardUserDefaults] setObject:singleton.tabSelected forKey:@"SELECTED_TAB"];
    [[NSUserDefaults standardUserDefaults] setBool:singleton.bannerStatus forKey:@"SELECTED_BANNER"];
    [[NSUserDefaults standardUserDefaults] setObject:singleton.notificationSelected forKey:@"SELECTED_NOTIFICATION_STATUS"];
    
    //    [[NSUserDefaults standardUserDefaults] setObject:singleton.fontSizeSelected forKey:@"SELECTED_FONTSIZE_INDEX"];
    
    [[NSUserDefaults standardUserDefaults] setInteger:singleton.fontSizeSelectedIndex  forKey:@"SELECTED_FONTSIZE_INDEX"];
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}



- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:NO];
    
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    [self savePreferences];
    //[self setHeightOfTableView];
    //table_settingView.contentInset = UIEdgeInsetsMake(-20,0,0,0);
    /*UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(backBtnAction:)];
     [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
     [self.navigationController.view addGestureRecognizer:gestureRecognizer];*/
    
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    
    
    
    [self backToParentView];
    [table_settingView reloadData];
    [table_settingView setNeedsLayout ];
    [table_settingView layoutIfNeeded ];
    [table_settingView reloadData];
    
    [_btnBackMore setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    [btnHelp setTitle:NSLocalizedString(@"help", nil) forState:UIControlStateNormal];
    
    [_btnReset setTitle:NSLocalizedString(@"help", nil) forState:UIControlStateNormal];
    
    
    
    float fontSize = 17.0;
    
    
    
    switch (singleton.fontSizeSelectedIndex) {
            
        case 0:
            
            fontSize = 14.0;
            
            break;
            
        case 1:
            
            fontSize = 17.0;
            
            
            
            break;
            
        case 2:
            
            fontSize = 17.0;
            
            
            
            break;
            
            
            
        default:
            
            break;
            
    }
    
    _btnBackMore.titleLabel.font = [UIFont systemFontOfSize:fontSize];
    
    btnHelp.titleLabel.font = [UIFont systemFontOfSize:fontSize];
    
    _btnReset.titleLabel.font = [UIFont systemFontOfSize:fontSize];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:_btnBackMore.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(_btnBackMore.frame.origin.x, _btnBackMore.frame.origin.y, _btnBackMore.frame.size.width, _btnBackMore.frame.size.height);
        
        [_btnBackMore setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        _btnBackMore.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    [self setViewFont];
    [self callStateAPI];
    
}

#pragma mark- Font Set to View
-(void)setViewFont{
    [_btnBackMore.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    _lblSettings.font = [AppFont semiBoldFont:17];
    btnHelp.titleLabel.font = [AppFont regularFont:17];
    
    
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}


-(void)setHeightOfTableView
{
    
    //table_settingView.contentInset = UIEdgeInsetsMake(-120,0,0,0);
    
    
    /**** set frame size of tableview according to number of cells ****/
    /* float height=45.0f*[table_data count]+40;
     
     [table_settingView setFrame:CGRectMake(table_settingView.frame.origin.x, table_settingView.frame.origin.y, table_settingView.frame.size.width,height-3)];
     
     if (table_settingView.frame.size.height < fDeviceHeight-65) {
     table_settingView.scrollEnabled = NO;
     }
     else {
     table_settingView.scrollEnabled = YES;
     }
     //table_helpView.layer.backgroundColor= [UIColor colorWithRed:206/255.0 green:206/255.0 blue:206/255.0 alpha:1.0].CGColor;
     table_settingView.layer.borderWidth=1;
     table_settingView.layer.borderColor= [UIColor colorWithRed:206/255.0 green:206/255.0 blue:206/255.0 alpha:1.0].CGColor;
     
     [table_settingView reloadData];
     
     table_settingView.contentInset = UIEdgeInsetsMake(0,64,0,0);
     */
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [table_data  count];
}



- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 50;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //Here you must return the number of sectiosn you want
    return 1;
}


- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    return 0;
}





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SettingsCell";
    
    SettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[SettingsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    //Aditi
    
    
    
    // update frame
    
    
    
    //Aditi
    
    cell.backgroundColor = [UIColor clearColor];
    cell.lbl_title.text=[table_data objectAtIndex:indexPath.row];
    cell.separatorInset = UIEdgeInsetsMake(0, 55, 0, 0);
    // main
    
    
    dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(concurrentQueue, ^{
        UIImage *image = nil;
        image = [UIImage imageNamed:[table_imgdata objectAtIndex:indexPath.row]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.img_cell.image=image;
            cell.img_cell.contentMode=UIViewContentModeScaleAspectFit;
            
        });
        
    });
    
    UILabel *cellLabel = [[UILabel alloc] initWithFrame:CGRectMake(fDeviceWidth-130, 11, 100, 25)];
    
    //  CGRect detailLabel = cellLabel.frame;
    //  detailLabel.origin.x = singleton.isArabicSelected ? 10 :fDeviceWidth-130;
    // cellLabel.frame = detailLabel;
    
    cellLabel.textColor=[UIColor lightGrayColor];
    cellLabel.font=[AppFont regularFont:17];
    
    
    /*if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"TabKey"] isEqualToString:@"YES"])
    {
        if (indexPath.row==0)
        {
            
            cell.btn_switch.hidden=TRUE;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            
            cellLabel.text=singleton.tabSelected;
            cellLabel.hidden=FALSE;
            
            // [table_subdata replaceObjectAtIndex:0 withObject:singleton.tabSelected];
            
            
        }
        else if (indexPath.row==1)
        {
            if ([singleton.bannerStatus isEqualToString:NSLocalizedString(@"yes", nil)])
            {
                
                cell.btn_switch.hidden=FALSE;
                
                //            CGRect btnSwitch =  cell.btn_switch.frame;
                //            btnSwitch.origin.x = singleton.isArabicSelected ? 20 : fDeviceWidth-60;
                //            cell.btn_switch.frame = btnSwitch;
                
                cell.btn_switch.on=TRUE;
                cell.lbl_subtitle.hidden=TRUE;
                
                cellLabel.text=@"Yes";
                cellLabel.hidden=TRUE;
                
                cell.accessoryType = UITableViewCellAccessoryNone;
                
            }
            else
            {
                cell.btn_switch.hidden=FALSE;
                cell.btn_switch.on=FALSE;
                
                //cell.lbl_subtitle.text=@"No";
                // cell.lbl_subtitle.hidden=TRUE;
                cell.lbl_subtitle.hidden=TRUE;
                
                cellLabel.text=@"No";
                
                cellLabel.hidden=TRUE;
                
                //cell.img_cell_arrow.hidden=TRUE;
                cell.accessoryType = UITableViewCellAccessoryNone;
                
            }
            
        }
        else if (indexPath.row==2)
        {
            
            singleton=[SharedManager sharedSingleton];
            
            BOOL isEnabled =  NO;
            
            if([singleton.shared_ntft isEqualToString:@"0"]&&[singleton.shared_ntfp isEqualToString:@"0"]){
                isEnabled = NO;
            }
            else{
                isEnabled = YES;
            }
            
            
            if (isEnabled) {
                singleton.notificationSelected = NSLocalizedString(@"enabled", nil);
            }
            else{
                singleton.notificationSelected = NSLocalizedString(@"disabled", nil);
            }
            
            cellLabel.text=singleton.notificationSelected;
            cellLabel.hidden=FALSE;
            //----------
            cell.lbl_subtitle.hidden=TRUE;
            
            
            // cell.lbl_subtitle.text=[table_subdata objectAtIndex:indexPath.row];
            cell.btn_switch.hidden=TRUE;
            // cell.img_cell_arrow.hidden=FALSE;
            
            [table_subdata replaceObjectAtIndex:3 withObject:singleton.notificationSelected];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            
        }
        else if (indexPath.row==3)
        {
            
            
            //cell.lbl_subtitle.text=singleton.languageSelected;
            //cellLabel.text=NSLocalizedString(@"english", nil);
            cell.lbl_subtitle.hidden=TRUE;
            cellLabel.text=singleton.languageSelected;
            cellLabel.hidden=FALSE;
            cell.btn_switch.hidden=TRUE;
            cell.img_cell_arrow.hidden=FALSE;
            
            [table_subdata replaceObjectAtIndex:3 withObject:singleton.languageSelected];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
        }
        
        else if (indexPath.row == 4)
        {
            cell.btn_switch.hidden=TRUE;
            
            obj=[[StateList alloc]init];
            
            singleton.user_StateId = [singleton getStateId];
            NSLog(@"user_StateId user_StateId%@",singleton.user_StateId);
            
            if (singleton.user_StateId.length != 0)
            {
                singleton.stateSelected = [obj getStateName:singleton.user_StateId];
            }
            
            NSString *stateString = singleton.stateSelected;
            
            if ([singleton.stateSelected isEqualToString:NSLocalizedString(@"all", nil)] || [singleton.stateSelected isEqualToString:@"ALL"] || stateString.length == 0)
            {
                
                if (singleton.profilestateSelected.length != 0)
                {
                    singleton.user_StateId = [obj getStateCode:singleton.profilestateSelected];
                    
                    stateString = [obj getStateName:singleton.user_StateId];
                }
                
                if (stateString.length == 0 || [[stateString uppercaseString] isEqualToString:@"ALL"])
                {
                    stateString = NSLocalizedString(@"none", nil);
                }
                
                
            }
            //NSString *stateString = [singleton.stateSelected isEqualToString:NSLocalizedString(@"all", nil)] ? NSLocalizedString(@"none", nil): singleton.stateSelected;
            
            cellLabel.text= stateString;
            cellLabel.hidden=FALSE;
            //----------
            
            cell.lbl_subtitle.hidden=TRUE;
            
            
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        else  if (indexPath.row == 5)
        {
            //--------
            cellLabel.text=singleton.fontSizeSelected;
            cellLabel.hidden=FALSE;
            //----------
            cell.lbl_subtitle.hidden=TRUE;
            
            
            cell.btn_switch.hidden=TRUE;
            [table_subdata replaceObjectAtIndex:4 withObject:singleton.fontSizeSelected];
            
            //cell.img_cell_arrow.hidden=FALSE;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
        }
        else
        {
            cell.btn_switch.hidden=TRUE;
            
            cellLabel.text=@"";
            cellLabel.hidden=FALSE;
            //----------
            
            cell.lbl_subtitle.hidden=TRUE;
            
            
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            
        }
    }
    else
    {
        
    }*/
    
    if (indexPath.row==0)
    {
        
        
        cell.btn_switch.hidden=TRUE;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        
        cellLabel.text=singleton.tabSelected;
        cellLabel.hidden=FALSE;
        
        // [table_subdata replaceObjectAtIndex:0 withObject:singleton.tabSelected];
        
        
    }
    else if (indexPath.row==1)
    {
        if (singleton.bannerStatus == YES)
        {
            
            cell.btn_switch.hidden=FALSE;
            
            //            CGRect btnSwitch =  cell.btn_switch.frame;
            //            btnSwitch.origin.x = singleton.isArabicSelected ? 20 : fDeviceWidth-60;
            //            cell.btn_switch.frame = btnSwitch;
            
            cell.btn_switch.on=TRUE;
            cell.lbl_subtitle.hidden=TRUE;
            
            cellLabel.text=@"Yes";
            cellLabel.hidden=TRUE;
            
            cell.accessoryType = UITableViewCellAccessoryNone;
            
        }
        else
        {
            cell.btn_switch.hidden=FALSE;
            cell.btn_switch.on=FALSE;
            
            //cell.lbl_subtitle.text=@"No";
            // cell.lbl_subtitle.hidden=TRUE;
            cell.lbl_subtitle.hidden=TRUE;
            
            cellLabel.text=@"No";
            
            cellLabel.hidden=TRUE;
            
            //cell.img_cell_arrow.hidden=TRUE;
            cell.accessoryType = UITableViewCellAccessoryNone;
            
        }
        
    }
    else if (indexPath.row==2)
    {
        
        singleton=[SharedManager sharedSingleton];
        
        BOOL isEnabled =  NO;
        
        if([singleton.shared_ntft isEqualToString:@"0"]&&[singleton.shared_ntfp isEqualToString:@"0"])
        {
            isEnabled = NO;
        }
        else{
            isEnabled = YES;
        }
        
        
        if (isEnabled) {
            singleton.notificationSelected = NSLocalizedString(@"enabled", nil);
        }
        else{
            singleton.notificationSelected = NSLocalizedString(@"disabled", nil);
        }
        
        cellLabel.text=singleton.notificationSelected;
        cellLabel.hidden=FALSE;
        //----------
        cell.lbl_subtitle.hidden=TRUE;
        
        
        // cell.lbl_subtitle.text=[table_subdata objectAtIndex:indexPath.row];
        cell.btn_switch.hidden=TRUE;
        // cell.img_cell_arrow.hidden=FALSE;
        
        [table_subdata replaceObjectAtIndex:3 withObject:singleton.notificationSelected];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        
    }
    else if (indexPath.row==3)
    {
        
        
        //cell.lbl_subtitle.text=singleton.languageSelected;
        //cellLabel.text=NSLocalizedString(@"english", nil);
        cell.lbl_subtitle.hidden=TRUE;
        cellLabel.text=singleton.languageSelected;
        cellLabel.hidden=FALSE;
        cell.btn_switch.hidden=TRUE;
        cell.img_cell_arrow.hidden=FALSE;
        
        [table_subdata replaceObjectAtIndex:3 withObject:singleton.languageSelected];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    else  if (indexPath.row==4)
    {
        //--------
        cellLabel.text=singleton.fontSizeSelected;
        cellLabel.hidden=FALSE;
        //----------
        cell.lbl_subtitle.hidden=TRUE;
        
        
        cell.btn_switch.hidden=TRUE;
        [table_subdata replaceObjectAtIndex:4 withObject:singleton.fontSizeSelected];
        
        //cell.img_cell_arrow.hidden=FALSE;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    else
    {
        cell.btn_switch.hidden=TRUE;
        
        cellLabel.text=@"";
        cellLabel.hidden=FALSE;
        //----------
        
        cell.lbl_subtitle.hidden=TRUE;
        
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        
    }
    
    cell.btn_switch.tag=indexPath.row;
    
    [cell.btn_switch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
    
    cell.contentView.superview.backgroundColor =[UIColor whiteColor];
    cellLabel.backgroundColor=[UIColor whiteColor];
    cellLabel.textAlignment = NSTextAlignmentRight;
    [cell.contentView addSubview:cellLabel];
    
    if (singleton.isArabicSelected==TRUE)
    {
        cell.btn_switch.transform = CGAffineTransformMakeRotation(-M_PI);
        cell.img_cell.transform = CGAffineTransformMakeRotation(-M_PI);
        cell.lbl_title.transform= CGAffineTransformMakeRotation(-M_PI);
        cell.lbl_title.textAlignment=NSTextAlignmentRight;
        cell.lbl_subtitle.transform=  CGAffineTransformMakeRotation(-M_PI);
        cell.lbl_subtitle.textAlignment=NSTextAlignmentRight;
        cellLabel.transform=  CGAffineTransformMakeRotation(-M_PI);
        cellLabel.textAlignment=NSTextAlignmentLeft;
        cell.transform = CGAffineTransformMakeRotation(180*0.0174532925);
    }
    cell.lbl_title.font = [AppFont regularFont:17];
    cell.lbl_subtitle.font = [AppFont regularFont:16];
    
    return cell;
}

- (void)changeSwitch:(UISwitch *)sender
{
    
    NSString *value;
    if([sender isOn]){
        NSLog(@"Switch is ON");
        value=@"Yes";
        //singleton.bannerStatus=@"Yes";
        
        singleton.bannerStatus = YES;
        
    } else{
        NSLog(@"Switch is OFF");
        value=@"No";
        //singleton.bannerStatus=@"No";
        singleton.bannerStatus = NO;
        
    }
    [self savePreferences];

    [[NSNotificationCenter defaultCenter] postNotificationName: Banner_State_Changed object:nil userInfo:nil];
    
    NSInteger tagV=[sender tag];
    
    // Launch reload for the two index path
    
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:tagV inSection:0];
    //SettingsCell *cell =(SettingsCell*) [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    // set *cell = [table_settingView cellForRowAtIndexPath:indexPath];
    SettingsCell *cell =(SettingsCell*) [table_settingView cellForRowAtIndexPath:indexPath];
    cell.lbl_subtitle.text = value;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    /*if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"TabKey"] isEqualToString:@"YES"])
    {
        if (indexPath.row==0)//tab picker
        {
            header_title_pass=NSLocalizedString(@"default_tab", nil);
            [self initTabData];
            
        }
        if (indexPath.row==2)//language picker
        {
            header_title_pass= NSLocalizedString(@"notifications", nil);
            
            [self initNotification];
            
        }
        if (indexPath.row==3)//language picker
        {
            [self openLanguageSelection];
            
        }
        
        if (indexPath.row == 4) //state picker
        {
            header_title_pass = NSLocalizedString(@"region", nil);
            [self initStateData];
        }
        
        if (indexPath.row == 5) //Font picker
        {
            [self initFontSize];
        }
        
        if (indexPath.row == 6) //Account Setting
        {
            [self SecuritySettingVC];
        }
    }
    else
    {
        
    }*/
    
    
    if (indexPath.row==0)//tab picker
    {
        header_title_pass=NSLocalizedString(@"default_tab", nil);
        [self initTabData];
        
    }
    if (indexPath.row==2)//language picker
    {
        header_title_pass= NSLocalizedString(@"notifications", nil);
        
        [self initNotification];
        
    }
    if (indexPath.row==3)//language picker
    {
        
        [self openLanguageSelection];
        
    }
    
    if (indexPath.row==4) //Font picker
    {
        [self initFontSize];
    }
    
    if (indexPath.row==5) //Account Setting
    {
        [self SecuritySettingVC];
    }
    
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1010)
    { // handle the altdev
        if (buttonIndex==0)
        {
            [self openLanguageSelection];
            
        }
    }
}



-(void)openLanguageSelection
{
    
    header_title_pass=@"Language";
    
    [self initLanguageData];
    
    
}

-(void)initFontSize
{
    
    TAG_pass=TAG_FONTSIZE;
    
    arr_table_pass=[[NSMutableArray alloc]initWithObjects: NSLocalizedString(@"small", nil),NSLocalizedString(@"normal", nil),NSLocalizedString(@"large", nil), nil];
    
    [self callCustomPicker];
    
    
    
}
-(void)SecuritySettingVC
{
    // SettingsViewController
    //UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SecuritySettingVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"SecuritySettingVC"];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}



//-------- backToParentView delegate method--------
-(void)backToParentView
{
    [table_settingView reloadData];
}

//-----------------------------------------



-(void)callCustomPicker
{
    
    if ([TAG_pass isEqualToString:TAG_LANG])
    {
        UIStoryboard *storyboard  = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
        NewLanguageSelectVC *langVC = [storyboard instantiateViewControllerWithIdentifier:@"NewLanguageSelectVC"];
        __weak __typeof(self) weakSelf = self;
        
        langVC.LanguageSelect = ^(LanguageModel *model) {
            //[weakSelf viewDidLoad];
            [weakSelf hitInitAPI];
        };
        langVC.sender = @"SettingsViewController";
        
        [self.navigationController pushViewController:langVC animated:YES];
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        customVC = [storyboard instantiateViewControllerWithIdentifier:@"CustomPickerVC"];
        
        customVC.arrLanguageLocale = @[@"en",@"hi-IN",@"as-IN",@"bn-IN",@"gu-IN",@"kn-IN",@"ml-IN",@"mr-IN",@"or-IN",@"pa-IN",@"ta-IN",@"te-IN",@"ur-IN"];
        
        
        customVC.delegate=self;
        customVC.get_title_pass=header_title_pass;
        customVC.get_arr_element=arr_table_pass;
        customVC.get_TAG = TAG_pass;
        [self.navigationController pushViewController:customVC animated:YES];
    }
    
    /*UIStoryboard *storyboard  = [UIStoryboard storyboardWithName:kDetailServiceStoryBoard bundle:nil];
    NewLanguageSelectVC *langVC = [storyboard instantiateViewControllerWithIdentifier:@"NewLanguageSelectVC"];
    __weak __typeof(self) weakSelf = self;

    langVC.LanguageSelect = ^(LanguageModel *model) {
        //[weakSelf viewDidLoad];
         [weakSelf hitInitAPI];
    };
    langVC.sender = @"SettingsViewController";
   
    [self.navigationController pushViewController:langVC animated:YES];*/
    
    
    
    // [customVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    //[self presentViewController:customVC animated:NO completion:nil];
    
    
}



-(IBAction)backBtnAction:(id)sender
{
    //[self dismissViewControllerAnimated:NO completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initLanguageData
{
    
    TAG_pass=TAG_LANG;
    
    arr_table_pass=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"english", nil),NSLocalizedString(@"hindi", nil),NSLocalizedString(@"assamese", nil),NSLocalizedString(@"bengali", nil),NSLocalizedString(@"gujarati", nil),NSLocalizedString(@"kannada", nil),NSLocalizedString(@"malayalam", nil),NSLocalizedString(@"marathi", nil),NSLocalizedString(@"oriya", nil),NSLocalizedString(@"punjabi", nil),NSLocalizedString(@"tamil", nil),NSLocalizedString(@"telugu", nil),NSLocalizedString(@"urdu", nil), nil];
    
    //aditi
    
    [self callCustomPicker];
    
    
    
}



-(void)initNotification
{
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingNotificationVwCon *vc = [storyboard instantiateViewControllerWithIdentifier:@"SettingNotificationVwCon"];
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}


-(void)initTabData
{
    
    
    TAG_pass=TAG_TAB;
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"TabKey"] isEqualToString:@"YES"])
    {
        arr_table_pass=[[NSMutableArray alloc]initWithObjects: NSLocalizedString(@"home_small", nil),NSLocalizedString(@"favourites_small", nil),NSLocalizedString(@"all_services_small", nil),NSLocalizedString(@"state_txt", nil), nil];
    }
    else
    {
        arr_table_pass=[[NSMutableArray alloc]initWithObjects: NSLocalizedString(@"home_small", nil),NSLocalizedString(@"favourites_small", nil),NSLocalizedString(@"all_services_small", nil), nil];
    }
    
    
    //0 2 3
    [self callCustomPicker];
    
    
}

-(void)initStateData
{
    
    
    NSArray *arrState=[obj getStateList];
    
    arrState = [arrState sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    
    TAG_pass=TAG_STATE;
    
    //NSDictionary* dummyDictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"StateList" ofType:@"plist"]];
    
    
    
    // NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:nil ascending:YES selector:@selector(localizedCompare:)];
    
    
    
    
    //NSArray* sortedCategories = [dummyDictionary.allKeys sortedArrayUsingDescriptors:nil];
    
    
    
    
    //NSString *categoryName;
    //NSArray *currentCategory;
    
    arr_table_pass=[[NSMutableArray alloc]init];
    
    
    //    for (int i = 0; i < [dummyDictionary.allKeys count]; i++)
    //    {
    //        categoryName = [sortedCategories objectAtIndex:i];
    //        currentCategory = [dummyDictionary objectForKey:categoryName];
    //        [arr_table_pass addObject:currentCategory];
    //    }
    
    
    
    //NSArray* sortedArray = [arr_table_pass sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    [arr_table_pass removeAllObjects];
    
    arr_table_pass = [arrState mutableCopy];
    
    //    arr_table_pass=[sortedArray mutableCopy];
    
    
    [self callCustomPicker];
    
    
    
    
}

-(IBAction)resetBtnAction:(id)sender
{
    /* UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Reset all settings?" preferredStyle:UIAlertControllerStyleAlert];
     
     UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:nil];
     [alertController addAction:ok];
     UIAlertAction* no = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:nil];
     [alertController addAction:no];
     
     [self presentViewController:alertController animated:YES completion:nil];
     */
    
    // UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    HelpSettingVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HelpSettingVC"];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

//- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
//    return UIInterfaceOrientationMaskPortrait;
//}

/*
 
 #if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }
 */
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
