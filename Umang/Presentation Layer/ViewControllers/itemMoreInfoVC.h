//
//  itemMoreInfoVC.h
//  Umang
//
//  Created by spice on 15/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"
#import "AMRatingControl.h"

@interface itemMoreInfoVC : UIViewController
{
    IBOutlet  UIView *vw_bg;
    
    
    IBOutlet  UIView *vw_withname;
    IBOutlet   UIView *vw_withrate;
    IBOutlet  UIView *vw_withdesc;
    IBOutlet  UIView *vw_withbtns;
    
    IBOutlet UIButton *btn_close;
    
    IBOutlet  UIButton *btn_visitMap;
    IBOutlet  UIButton *btn_visitWeb;
    IBOutlet  UIButton *btn_contact;
    
    
    IBOutlet  UITextView *txt_description;
    
    IBOutlet  UILabel *lbl_name;
    IBOutlet  UILabel *lbl_category;
    IBOutlet  UILabel *lbl_rating;
    
    IBOutlet  UILabel *lbl_rateService;
    
    IBOutlet UILabel *lbl_visitOnMap;
    IBOutlet UILabel *lbl_visitWeb;
    IBOutlet UILabel *lbl_contact;
    
    IBOutlet UIImageView *img_service;
    
    IBOutlet IBOutlet UIView *vw_RateUs;
    
    IBOutlet  UIButton *btn_edit;
    IBOutlet UIImageView *img_btn_edit;
    
    BOOL FlagCheckEdit;
    
}

@property (nonatomic, strong)IBOutlet HCSStarRatingView *starRatingView;

@property (nonatomic, strong)IBOutlet AMRatingControl *imagesRatingControl;

@property(weak,nonatomic)NSDictionary *dic_serviceInfo;//change it to URL on demand



-(IBAction)btn_editAction:(id)sender;

-(IBAction)btn_contact_Action:(id)sender;
-(IBAction)btn_viewWeb_Action:(id)sender;
-(IBAction)btn_viewMap_Action:(id)sender;
-(IBAction)btn_VisitService_Action:(id)sender;

-(IBAction)btn_close_Action:(id)sender;


@end
