//
//  AboutInfoVC.m
//  Umang
//
//  Created by deepak singh rawat on 23/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "AboutInfoVC.h"
#import "FAQWebVC.h"

@implementation AboutInfoVC


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    singleton = [SharedManager sharedSingleton];
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    self.view.backgroundColor= [UIColor colorWithRed:235/255.0 green:234/255.0 blue:241/255.0 alpha:1.0];
    self.navigationController.navigationBarHidden=true;
    
    
    if (self.view.frame.size.height < 500)
    {
        NSLog(@"%@",umangDescLabel.font);
        for (NSLayoutConstraint *top in topConstraint) {
            if (singleton.fontSizeSelectedIndex == 2)
            {
                if ([top.identifier isEqualToString:@"20"]) {
                    top.constant = 10;
                    continue;
                }
            }
            if ([top.identifier isEqualToString:@"30"]) {
                continue;
            }
            top.constant = 30;
        }
    }
    else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        for (NSLayoutConstraint *top in topConstraint) {
            if ([top.identifier isEqualToString:@"30"]) {
                top.constant = 250.0;
                continue;
            }
            top.constant = 100.0;
        }
    }
    [self.view layoutIfNeeded];
    [self.view updateConstraintsIfNeeded];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
     AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
     appDelegate.shouldRotate = NO;
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    lblTitleAbout.text = NSLocalizedString(@"about", nil);
    // lblUmang.text = NSLocalizedString(@"TXT_APP_NAME", nil);
    
    
    NSString *copyrightStr = [NSString stringWithFormat:NSLocalizedString(@"copyright_txt", nil),@"2018"];
    lblCopyrgt.text = copyrightStr;
    NSString *versionNum = @"1.0.6";

    
    
    //open this for Testfligh and close second line
    // NSString *versionStr = [NSString stringWithFormat:@"%@ %@\nBuild Sharding Proudction:v1.0.31",NSLocalizedString(@"version_txt", nil),versionNum];
    
    NSString *versionStr = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"version_txt", nil),versionNum];
    
    lblVersion.text = versionStr;
    
    // lblPrivacyPolicy.text = NSLocalizedString(@"privacy_policy", nil);
    // lblTermsservice.text = NSLocalizedString(@"terms_of_services", nil);
    
    
    [btnTermsService setTitle:NSLocalizedString(@"terms_of_services", nil) forState:UIControlStateNormal];
    [btnPrivacyPolicy setTitle:NSLocalizedString(@"privacy_policy", nil) forState:UIControlStateNormal];
    [cancelRefButton setTitle:[NSLocalizedString(@"ref_cancel_policy", nil) uppercaseString] forState:UIControlStateNormal];
    
    // Buidl Version_Date
    // 1.0.151S
    //  BUILD 1.0.113P 15092017
    NSString * strBuildVersion_Date = @"";
    
    
    // [self setNeedsStatusBarAppearanceUpdate];
    //  [self performSelector:@selector(setHeightOfTableView) withObject:nil afterDelay:.1];
    /*  UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(backbtnAction:)];
     [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
     [self.navigationController.view addGestureRecognizer:gestureRecognizer];
     */
    
    __weak id weakSelf = self;
    self.navigationController.interactivePopGestureRecognizer.delegate = weakSelf;
    
    
//    if (singleton.fontSizeSelectedIndex == 2)
//    {
//        NSLog(@"Large");
//        umangDescLabel.font = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];
//        lblVersion.font     = [UIFont systemFontOfSize:20 weight:UIFontWeightMedium];
//        lblUmang.font       = [UIFont systemFontOfSize:28 weight:UIFontWeightMedium];
//
//    }
//    else if (singleton.fontSizeSelectedIndex == 1)
//    {
//        umangDescLabel.font = [UIFont systemFontOfSize:11 weight:UIFontWeightMedium];
//        lblVersion.font     = [UIFont systemFontOfSize:18 weight:UIFontWeightMedium];
//        lblUmang.font       = [UIFont systemFontOfSize:24 weight:UIFontWeightMedium];
//    }
//    else
//    {
//        umangDescLabel.font = [UIFont systemFontOfSize:10 weight:UIFontWeightMedium];
//        lblVersion.font     = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
//        lblUmang.font       = [UIFont systemFontOfSize:20 weight:UIFontWeightMedium];
//
//    }
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [self setViewFont];
    [super viewWillAppear:NO];
}

#pragma mark- Font Set to View
-(void)setViewFont
{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    [btnTermsService.titleLabel setFont:[AppFont semiBoldFont:13.0]];
    [btnPrivacyPolicy.titleLabel setFont:[AppFont semiBoldFont:13.0]];
    [cancelRefButton.titleLabel setFont:[AppFont semiBoldFont:13.0]];
    
    lblTitleAbout.font = [AppFont semiBoldFont:16.0];
    umangDescLabel.font = [AppFont mediumFont:11.0];
    lblVersion.font = [AppFont mediumFont:18.0];
    lblBuildVersion_Date.font = [AppFont lightFont:11];
}
#pragma mark -

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}


-(IBAction)backbtnAction:(id)sender
{
    // [self dismissViewControllerAnimated:NO completion:nil];
    // [self dismissViewControllerAnimated:NO completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    
}

/*urlString=[singleton.arr_initResponse valueForKey:@"faq"];
 */

-(IBAction)termAction:(id)sender
{
    NSString *url=[NSString stringWithFormat:@"%@",[singleton.arr_initResponse valueForKey:@"tndc"]];
    
    
    
    // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    NSString *title=NSLocalizedString(@"terms_of_services", nil);
    [self openFAQWebVC:url withTitle:title];
}


-(void)openFAQWebVC:(NSString *)url withTitle:(NSString*)title
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    FAQWebVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FAQWebVC"];
    vc.urltoOpen=url;
    vc.titleOpen=title;
    
    //[vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self.navigationController pushViewController:vc animated:YES];
    
    
    
}

//-(IBAction)btn_SpiceAction:(id)sender
//{
//    NSString *url=[NSString stringWithFormat:@"http://www.spicedigital.in"];
//
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
//
//}
-(IBAction)privacyAction:(id)sender
{
    NSString *url=[NSString stringWithFormat:@"%@",[singleton.arr_initResponse valueForKey:@"ppol"]];
    // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    NSString *title=NSLocalizedString(@"privacy_policy", nil);
    
    [self openFAQWebVC:url withTitle:title];
    
}

-(IBAction)cancellationAndRefundAction:(id)sender
{
    NSString *url=[NSString stringWithFormat:@"%@",[singleton.arr_initResponse valueForKey:@"canrefpol"]];
    // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    NSString *title=NSLocalizedString(@"ref_cancel_policy", nil);
    
    [self openFAQWebVC:url withTitle:title];
}
- (UIStatusBarStyle)preferredStatusBarStyle

{
    return UIStatusBarStyleDefault;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/


@end

