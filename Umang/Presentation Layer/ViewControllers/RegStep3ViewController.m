//
//  RegStep3ViewController.m
//  SpiceRegistration
//
//  Created by spice_digital on 21/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "RegStep3ViewController.h"

#define kOFFSET_FOR_KEYBOARD 80.0

#import "MyTextField.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"
#import "AadharRegistrationVC.h"
#import "UserEditVC.h"
#define MAX_LENGTH 4


@interface RegStep3ViewController ()<UITextFieldDelegate,UIScrollViewDelegate>

{
    __weak IBOutlet UILabel *lblTipMPin;
    __weak IBOutlet UIButton *btnBack;
    NSString *mpinStr;
    NSString *CmpinStr;
    MBProgressHUD *hud ;
      IBOutlet UIScrollView *scrollview;
    __weak IBOutlet UILabel *llblMsgAboutMpin;
}


@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_titleMsg;
@property (weak, nonatomic) IBOutlet UILabel *lbl_enterMpin;
@property (weak, nonatomic) IBOutlet UITextField *txt_mpin1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_cMpin;
@property (weak, nonatomic) IBOutlet UITextField *txt_Cmpin1;

@property (weak, nonatomic) IBOutlet UILabel *lbl_errorMsg;
@property (weak, nonatomic) IBOutlet UIImageView *img_error;
@property (weak, nonatomic) IBOutlet UIButton *btn_next;
@property (weak, nonatomic) IBOutlet UILabel *vw_line1;
@property (weak, nonatomic) IBOutlet UILabel *vw_line2;
@end

@implementation RegStep3ViewController
@synthesize txt_mpin1,txt_Cmpin1;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    singleton = [SharedManager sharedSingleton];

    [self.btn_next setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [self.btn_next setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btn_next.layer.cornerRadius = 3.0f;
    self.btn_next.clipsToBounds = YES;
    
   // NSString *str = NSLocalizedString(@"tip", nil);
    lblTipMPin.text = NSLocalizedString(@"set_mpin_sub_heading", nil);
    
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:REGISTRATION_MPIN_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    llblMsgAboutMpin.text = NSLocalizedString(@"set_mpin_sub_heading", nil);
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    self.lbl_title.text = NSLocalizedString(@"setmpin_label", nil);
     self.lbl_titleMsg.text = NSLocalizedString(@"set_mpin_heading", nil);
     self.lbl_enterMpin.text = NSLocalizedString(@"enter_mpin_verify", nil);
     self.lbl_cMpin.text = NSLocalizedString(@"confirm_mpin", nil);
    self.lbl_errorMsg.text = NSLocalizedString(@"mpin_donot_match_txt", nil);

   
    
    [self textfieldInit];
   
    //----- Setting delegate for Custom textfield so back space operation work smooth
    txt_mpin1.delegate = self;
    txt_Cmpin1.delegate = self;

    self.lbl_errorMsg.hidden=TRUE;
    self.img_error.hidden=TRUE;
    self.btn_next.enabled=NO;
    self.view.userInteractionEnabled = YES;

    // Do any additional setup after loading the view.
    //------ Add dismiss keyboard while background touch-------
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    
}

#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    [_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
    _lbl_title.font = [AppFont semiBoldFont:23.0];
    _lbl_titleMsg.font = [AppFont semiBoldFont:16.0];
    _lbl_enterMpin.font = [AppFont mediumFont:16.0];
    _lbl_cMpin.font = [AppFont mediumFont:16.0];
    lblTipMPin.font = [AppFont mediumFont:13.0];
    _lbl_errorMsg.font = [AppFont regularFont:14.0];
    txt_mpin1.font = [AppFont regularFont:21];
    txt_Cmpin1.font = [AppFont regularFont:21];
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setViewFont];
}
-(void)hideKeyboard
{
    
    [scrollview setContentOffset:
     CGPointMake(0, -scrollview.contentInset.top) animated:YES];
    
    [self.txt_mpin1 resignFirstResponder];
    [self.txt_Cmpin1 resignFirstResponder];
    
}


- (IBAction)btnBackClicked:(id)sender {
    
  [self dismissViewControllerAnimated:NO completion:nil];
}


-(void)textfieldInit
{
    [txt_mpin1 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [txt_Cmpin1 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    


}




- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}





//----- Code to overide delegate method of my custom Textfield so user can trace backspace with empty textfield



//---Code for hangle jump from one textfield to another while filling values
- (void)textFieldDidChange:(UITextField *)textField
{
    self.lbl_errorMsg.hidden=TRUE;
    self.img_error.hidden=TRUE;
    
    [self enableBtnNext:NO];
    self.btn_next.enabled=NO;
    
    
    if (textField.text.length >= MAX_LENGTH)
    {
        
        if ([textField isEqual:self.txt_mpin1]) {
            textField.text = [textField.text substringToIndex:MAX_LENGTH];
            
            [self.txt_Cmpin1 becomeFirstResponder];
            
            [self checkValidation];
            
        }
        else if([textField isEqual:self.txt_Cmpin1]) {
            textField.text = [textField.text substringToIndex:MAX_LENGTH];
            
            [textField resignFirstResponder];
            [self checkValidation];
        }
        
    }
}




-(void)checkValidation

{
    mpinStr=[NSString stringWithFormat:@"%@",self.txt_mpin1.text];
    CmpinStr=[NSString stringWithFormat:@"%@",self.txt_Cmpin1.text];
    
    CmpinStr=[NSString stringWithFormat:@"%@",self.txt_Cmpin1.text];
    mpinStr=[NSString stringWithFormat:@"%@",self.txt_mpin1.text];
    
    if (CmpinStr.length == 4 && mpinStr.length == 4 )
    {
        
        self.btn_next.enabled=YES;
        [self enableBtnNext:YES];
    }
    else
    {
        self.btn_next.enabled=NO;
        [self enableBtnNext: NO];
    }
    
}




-(void)enableBtnNext:(BOOL)status
{
    if (status ==YES)
    {
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateNormal];
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateSelected];
        
        [self.btn_next setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];
    }
    else
    {
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateNormal];
        //[self.btn_next setImage:[UIImage imageNamed:@"icon_gray_btn"] forState:UIControlStateSelected];
        
        [self.btn_next setBackgroundColor:[UIColor colorWithRed:176.0/255.0f green:176.0/255.0f blue:176.0/255.0f alpha:1.0f]];
    }
    
}

-(IBAction)jumptoNextView:(id)sender
{
    [self hitAPI];


}

//----- hitAPI for IVR OTP call Type registration ------

-(void)hitAPI
{
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
 hud.label.text = NSLocalizedString(@"loading",nil);
    NSString *strSaltMPIN = SaltMPIN;//[[SharedManager sharedSingleton] getKeyWithTag:KEYCHAIN_SaltMPIN];

    NSString *encrytmPinWithSalt=[NSString stringWithFormat:@"|%@|%@|",mpinStr,strSaltMPIN];
    NSString *mpinStrEncrypted=[encrytmPinWithSalt sha256HashFor:encrytmPinWithSalt];
    
    
    
    //682647029382
    //    NSString *encrytSHA256=[NSString stringWithFormat:@"|%@|%@|",mpinStr,SaltRequestControl];
    //    NSString *mpinStrEncrypted=[mpinStr sha256HashFor:encrytSHA256];
    //
    singleton.user_mpin=mpinStr; //save value in local db
    
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [dictBody setObject:mpinStrEncrypted forKey:@"mpin"];
 
  if (singleton.user_aadhar_number.length) {
    [dictBody setObject:singleton.user_aadhar_number forKey:@"aadhr"];
  }

    //Type for which OTP to be intiate eg register,login,forgot mpin
    NSLog(@"Dict body is :%@",dictBody);
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_SET_MPIN withBody:dictBody andTag:TAG_REQUEST_SET_MPIN completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];

        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            NSString *tkn=[[response valueForKey:@"pd"]valueForKey:@"tkn"];

            NSLog(@"value of rc =%@ \n value of rs=%@ \n value of tkn=%@ ",rc,rs,tkn);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            NSString *rd=[response valueForKey:@"rd"];

            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            
            {
                singleton.user_tkn=tkn;
                
                // Parse user profile data
                singleton.objUserProfile = nil;
                singleton.objUserProfile = [[ProfileDataBO alloc] initWithResponse:[response valueForKey:@"pd"]];
                 [self alertwithMsg:rd];
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}

-(void)alertwithMsg:(NSString*)msg
{
    
    
    [self openNextView];

 }



-(void)openNextView
{
  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
  if (self.isFromAadharRegistration)
  {
      UserEditVC *profileEdit = [storyboard instantiateViewControllerWithIdentifier:@"UserEditVC"];
    
      profileEdit.tagFrom=@"ISFROMREGISTRATION";
      [profileEdit setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
      [self presentViewController:profileEdit animated:NO completion:nil];

      
//    UITabBarController *tbc = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
//    tbc.selectedIndex=0;
//    [self presentViewController:tbc animated:NO completion:nil];
  }
  else
  {
    
    AadharRegistrationVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"AadharRegistrationVC"];
       vc.TYPE_AADHAR_REGISTRATION_FROM = IS_FROM_REG_STEP3_SCREEN; //after mobile set mpin
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:vc animated:NO completion:nil];
      
  }
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -kOFFSET_FOR_KEYBOARD, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
    
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (fDeviceHeight<=568) {
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.3];
        [UIView setAnimationBeginsFromCurrentState:TRUE];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +kOFFSET_FOR_KEYBOARD, self.view.frame.size.width, self.view.frame.size.height);
        
        [UIView commitAnimations];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (fDeviceHeight<=568) {
        UITouch * touch = [touches anyObject];
        if(touch.phase == UITouchPhaseBegan)
        {
            [self.txt_mpin1 resignFirstResponder];
            [self.txt_Cmpin1 resignFirstResponder];
            
        }
    }
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}

*/
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
