//
//  HomeContainerCellView.m
//  Home
//
//  Created by deepak singh rawat on 26/09/16.
//  Copyright © 2016 deepak singh rawat. All rights reserved.
//

#import "HomeContainerCellView.h"

#import <QuartzCore/QuartzCore.h>

#import "ServiceCollectionViewCell.h"

#import "itemMoreInfoVC.h"
#import "UIImageView+WebCache.h"
#import "HomeTabVC.h"
#import "DetailServiceNewVC.h"
#import "DetailServiceNewVC.h"
@interface HomeContainerCellView () <UICollectionViewDataSource, UICollectionViewDelegate,UIActionSheetDelegate,UIActionSheetDelegate>
{
    itemMoreInfoVC *itemMoreVC;
    SharedManager *singleton;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSArray *collectionData;
@property(nonatomic,retain)NSDictionary *cellData;

@end

@implementation HomeContainerCellView
@synthesize cellData;

- (void)awakeFromNib
{
    singleton=[SharedManager sharedSingleton];
    self.collectionView.backgroundColor = [UIColor colorWithRed:248.0/255.0 green:246.0/255.0 blue:247.0/255.0 alpha:1.0];
    
    //rgb(233,234,237)
    
    //self.collectionView.backgroundColor = [UIColor clearColor];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    //flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;

   
    
    flowLayout.sectionInset = UIEdgeInsetsMake(5, 10, 5, 10);
    //flowLayout.itemSize = CGSizeMake(150.0, 200.0);
    /* if([[UIScreen mainScreen] bounds].size.height == 1024)
     {
     self.collectionView.frame = CGRectMake(0, self.collectionView.frame.origin.y, self.collectionView.frame.size.width, self.collectionView.frame.size.height + 50);
     }*/
    
    if ([[UIScreen mainScreen]bounds].size.height >= 1024)
    {
        flowLayout.itemSize = CGSizeMake(150.0, 220.0);
    }
    else
    {
        flowLayout.itemSize = CGSizeMake(105.0, 150.0);
        
    }
    [self.collectionView setCollectionViewLayout:flowLayout];
    // Register the colleciton cell
    
    if ([[ UIScreen mainScreen] bounds].size.height >= 1024)
    {
        [_collectionView registerNib:[UINib nibWithNibName:@"ServiceCollectionViewCell_iPad" bundle:nil] forCellWithReuseIdentifier:@"ServiceCollectionViewCell"];
        //self.selectedMarks = [[NSMutableArray alloc] init];
    }
    else
    {
        
        [_collectionView registerNib:[UINib nibWithNibName:@"ServiceCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ServiceCollectionViewCell"];
        //self.selectedMarks = [[NSMutableArray alloc] init];
        
        [super awakeFromNib];
    }
    
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

#pragma mark - Getter/Setter overrides
- (void)setCollectionData:(NSArray *)collectionData
{
    _collectionData = collectionData;
    [_collectionView setShowsHorizontalScrollIndicator:NO];
    [_collectionView setShowsVerticalScrollIndicator:NO];
    [_collectionView setContentOffset:CGPointZero animated:NO];
    [_collectionView reloadData];
}
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
//    CGFloat itemsPerRow=4;
//    CGFloat hardCodedPadding=5;
//
//    CGFloat itemWidth=(collectionView.frame.size.width / itemsPerRow) - hardCodedPadding;
//    CGFloat itemHeight = collectionView.frame.size.height - (2 * hardCodedPadding);
//
//    return CGSizeMake(itemWidth, itemHeight);
//}


#pragma mark - UICollectionViewDataSource methods
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if([self.collectionData count]>6)
    {
        return 6;
    }
    else
        return [self.collectionData count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ServiceCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ServiceCollectionViewCell" forIndexPath:indexPath];
    cellData = (NSDictionary*)[self.collectionData objectAtIndex:[indexPath row]];
    cell.serviceTitle.text = [cellData objectForKey:@"SERVICE_NAME"];
    
    NSURL *url=[NSURL URLWithString:[cellData objectForKey:@"SERVICE_IMAGE"]];
    
    [cell.serviceImage sd_setImageWithURL:url
                         placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
    
    // NSData *data = [NSData dataWithContentsOfURL :url ];
    //UIImage *image = [UIImage imageWithData: data];
    
    
    
    // cell.serviceImage.image=image;
    
    NSLog(@"[indexPath row]=%ld",(long)[indexPath row]);
    NSLog(@"[indexPath section]=%ld",(long)[indexPath section]);
    
    
    cell.serviceFav.tag=indexPath.section*1000+indexPath.row;
    //int tagvalue= (int)(indexPath.section*1000+indexPath.row);
    
    cell.serviceFav.usdata=[cellData objectForKey:@"SERVICE_NAME"];
    
    cell.serviceInfo.celldata=[cellData mutableCopy];
    
    
    [cell.serviceInfo  addTarget:self action:@selector(moreInfo:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [cell.serviceFav  addTarget:self action:@selector(fav_action:) forControlEvents:UIControlEventTouchUpInside];
//    
//    UIImage * btnImage1 = [UIImage imageNamed:@"icon_favourite"];
//    UIImage * btnImage2 = [UIImage imageNamed:@"icon_favourite_red"];
//    
//    [cell.serviceFav setImage:btnImage1 forState:UIControlStateNormal];
//    [cell.serviceFav setImage:btnImage2 forState:UIControlStateSelected];
    
    NSString *rating=[cellData objectForKey:@"SERVICE_RATING"];
    
    float fCost = [rating floatValue];
    cell.starRatingView.value = fCost;

    //[cell.starRatingView setScore:fCost*2 withAnimation:NO];
    
    
    
    
    
    
    NSString *serviceid=[NSString stringWithFormat:@"%@",[cellData objectForKey:@"SERVICE_ID"]];
    
    NSString *serviceFav=[singleton.dbManager getServiceFavStatus:serviceid];
    
    
    
    if ([serviceFav isEqualToString:@"true"])
    {
        cell.serviceFav .selected=YES;
    }else{
        cell.serviceFav .selected=NO;
    }
    
    cell.serviceTitle.font = [AppFont regularFont:14.0];
    
    //new changes
    
    if (singleton.isArabicSelected==YES)
    {
        cell.serviceImage.transform = CGAffineTransformMakeScale(-1, 1);
        cell.serviceTitle.transform = CGAffineTransformMakeScale(-1, 1);
        cell.serviceFav.transform= CGAffineTransformMakeScale(-1, 1);
        cell.serviceTitle.textAlignment=NSTextAlignmentRight;//right here
    }
   
 
  
    //----- Adding shadow-----------------
    
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *cellData =(NSDictionary*) [self.collectionData objectAtIndex:[indexPath row]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"didSelectItemFromCollectionView" object:cellData];
}

- (UIEdgeInsets)collectionView:(UICollectionView *) collectionView
                        layout:(UICollectionViewLayout *) collectionViewLayout
        insetForSectionAtIndex:(NSInteger) section {
    
    return UIEdgeInsetsMake(0,16,0, 16); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *) collectionView
                   layout:(UICollectionViewLayout *) collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger) section {
    return 5.0;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGFloat height = 135 ;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
        
        height = 220;
        
    }else {
        if (fDeviceWidth == 320)
        {
            height = 135;
        }
        else if (fDeviceWidth == 375)
        {
            height = 145;
        }
        else if (fDeviceWidth > 375)
        {
            height = 145;// no need to increase more then this as we have max size 150 height in xib
        }
        
    }
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return CGSizeMake((fDeviceWidth - 75 ) / 4.0, height);
    }
    else if (fDeviceWidth > 375)
    {
        return  CGSizeMake((fDeviceWidth - 90 ) / 3.0, height);
    }
    return  CGSizeMake((fDeviceWidth - 60 ) / 3.0, height);
    /*
     if (UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication.statusBarOrientation))
     {
     
     NSLog(@"UIInterfaceOrientationIsLandscape xxxxxxx value=%f",[[ UIScreen mainScreen] bounds].size.height);
     // 375.000000
     
     if ([[ UIScreen mainScreen] bounds].size.height >= 768)
     {
     return CGSizeMake(150, 220);
     }
     else if ([[UIScreen mainScreen] bounds].size.height <= 320)
     {
     return CGSizeMake(90, 135);
     }
     else if ([[UIScreen mainScreen] bounds].size.height <= 375)
     {
     return CGSizeMake(105, 150);
     }
     else
     return CGSizeMake(105, 150);
     }
     else
     {
     //667
     NSLog(@"UIInterfaceOrientationIsLandscape NOT xxxxxxx value=%f",[[ UIScreen mainScreen] bounds].size.height);
     if ([[ UIScreen mainScreen] bounds].size.height >= 1023)
     {
     return CGSizeMake(150, 220);
     }else if ([[UIScreen mainScreen] bounds].size.height >= 667)
     {
     return CGSizeMake(150, 220);
     }
     else if ([[UIScreen mainScreen] bounds].size.height >= 568)
     {
     return CGSizeMake(90, 135);
     }
     
     else
     return CGSizeMake(105, 150);
     
     
     
     
     }
     
     return CGSizeMake(105, 150);*/
    
    
}

//below code is old code of height
/*- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        return CGSizeMake(170, 220);
    }
    return CGSizeMake(105, 150);
}
*/
//==================================
//       CUSTOM PICKER STARTS
//==================================
- (void)btnOpenSheet

{
    NSString *information=NSLocalizedString(@"information", nil);
    NSString *viewMap=NSLocalizedString(@"view_on_map", nil);
    NSString *cancelinfo=NSLocalizedString(@"cancel", nil);
    
    
    UIActionSheet *  sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                         delegate:self
                                                cancelButtonTitle:cancelinfo
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:information,viewMap, nil];
    
    //  [[[sheet valueForKey:@"_buttons"] objectAtIndex:0] setImage:[UIImage imageNamed:@"serviceinfo"] forState:UIControlStateNormal];
    
    //  [[[sheet valueForKey:@"_buttons"] objectAtIndex:1] setImage:[UIImage imageNamed:@"serivemap"] forState:UIControlStateNormal];
    
    UIViewController *vc=[self topMostController];
    // Show the sheet
    [sheet showInView:vc.view];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            [singleton traceEvents:@"Department Information" withAction:@"Clicked" withLabel:@"Home Tab" andValue:0];
            [self callDetailServiceVC];
        }
            break;
        case 1:
        {
            
            [singleton traceEvents:@"View On Map" withAction:@"Clicked" withLabel:@"Home Tab" andValue:0];
            
            NSLog(@"VISIT MAP LONG=%@", [NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]]);
            
            NSString *latitude=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LATITUDE"]];
            
            NSString *longitute=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]];
            
            NSString *deptName=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_NAME"]];
            deptName = [deptName stringByReplacingOccurrencesOfString:@" " withString:@"+"];
            
            
            
            if ([[UIApplication sharedApplication] canOpenURL:
                 [NSURL URLWithString:@"comgooglemaps://"]])
                
            {
                NSLog(@"Map App Found");
                
                NSString* googleMapsURLString = [NSString stringWithFormat:@"comgooglemaps://?q=%.6f,%.6f(%@)&center=%.6f,%.6f&zoom=15&views=traffic",[latitude doubleValue], [longitute doubleValue],deptName,[latitude doubleValue], [longitute doubleValue]];
                
                // googleMapsURLString = [googleMapsURLString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]; //IOS 9 and above use this line
                
                NSURL *mapURL=[NSURL URLWithString:[googleMapsURLString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
                NSLog(@"mapURL= %@",mapURL);
                
                [[UIApplication sharedApplication] openURL:mapURL];
                
            } else
            {
                NSString* googleMapsURLString = [NSString stringWithFormat:@"https://maps.google.com/maps?&z=15&q=%.6f+%.6f&ll=%.6f+%.6f",[latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
                
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:googleMapsURLString]];
                
            }
            
        }
            break;
        case 2:
        {
            
        }
            break;
        default:
            break;
    }
}

//==================================
//          CUSTOM PICKER ENDS
//==================================


//------------ Show more info-------------
-(IBAction)moreInfo:(MyButton*)sender
{
    self.cellDataOfmore=[[NSMutableDictionary alloc]init];
    
    self.cellDataOfmore=(NSMutableDictionary*)sender.celldata;
    
    [singleton traceEvents:@"More on Home Cell" withAction:@"Clicked" withLabel:@"Home Tab" andValue:0];
    
    [self btnOpenSheet];
    
    /*
     NSLog(@"Data 1 = %@",sender.celldata);
     self.cellDataOfmore=[[NSMutableDictionary alloc]init];
     
     self.cellDataOfmore=(NSMutableDictionary*)sender.celldata;
     
     
     NSString *titlename=[NSString stringWithFormat:@"%@",[self.cellDataOfmore objectForKey:@"SERVICE_NAME"]];
     NSString *information=NSLocalizedString(@"information", nil);
     NSString *viewMap=NSLocalizedString(@"view_on_map", nil);
     NSString *cancelinfo=NSLocalizedString(@"cancel", nil);
     
     UIAlertController * alert=   [UIAlertController
     alertControllerWithTitle:titlename
     message:nil
     preferredStyle:UIAlertControllerStyleAlert];
     
     UIAlertAction* info = [UIAlertAction
     actionWithTitle:information
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     [self callDetailServiceVC];
     [alert dismissViewControllerAnimated:YES completion:nil];
     
     }];
     UIAlertAction* map = [UIAlertAction
     actionWithTitle:viewMap
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     
     NSLog(@"VISIT MAP LONG=%@", [NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]]);
     
     NSString *latitude=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LATITUDE"]];
     
     NSString *longitute=[NSString stringWithFormat:@"%@",[self.cellDataOfmore valueForKey:@"SERVICE_LONGITUDE"]];
     
     if ([[UIApplication sharedApplication] canOpenURL:
     [NSURL URLWithString:@"comgooglemaps://"]])
     
     {
     NSLog(@"Map App Found");
     
     NSString* googleMapsURLString = [NSString stringWithFormat:@"comgooglemaps://?q=%.6f,%.6f&center=%.6f,%.6f&zoom=15&views=traffic", [latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsURLString]];
     } else
     {
     NSString* googleMapsURLString = [NSString stringWithFormat:@"https://maps.google.com/maps?&z=15&q=%.6f+%.6f&ll=%.6f+%.6f",[latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
     
     [[UIApplication sharedApplication]openURL:[NSURL URLWithString:googleMapsURLString]];
     
     }
     
     [alert dismissViewControllerAnimated:YES completion:nil];
     }];
     UIAlertAction* cancel = [UIAlertAction
     actionWithTitle:cancelinfo
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action)
     {
     [alert dismissViewControllerAnimated:YES completion:nil];
     
     }];
     
     
     [alert addAction:info];
     [alert addAction:map];
     [alert addAction:cancel];
     UIViewController *vc=[self topMostController];
     [vc presentViewController:alert animated:NO completion:nil];
     */
}



-(void)callDetailServiceVC
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName: kDetailServiceStoryBoard bundle:nil];
    // UIStoryboard *storyboard = [self grabStoryboard];
    //#import "DetailServiceNewVC.h"
    DetailServiceNewVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DetailServiceNewVC"];
    
    
    
    vc.dic_serviceInfo=self.cellDataOfmore;//change it to URL on demand
    
    UIViewController *topvc=[self topMostController];
    //[topvc.navigationController pushViewController:vc animated:YES];
    
    [topvc presentViewController:vc animated:NO completion:nil];
    
}

//----------- END OF MORE INFO POP UP VIEW---------------

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}


-(itemMoreInfoVC*)getHomeDetailLayerLeftController
{
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    // UIStoryboard *storyboard = [self grabStoryboard];
    
    //if (itemMoreVC == nil) {
    itemMoreVC = [storyboard instantiateViewControllerWithIdentifier:@"itemMoreInfoVC"];
    //}
    
    itemMoreVC.dic_serviceInfo=self.cellDataOfmore;//change it to URL on demand
    
    
    return itemMoreVC;
}




- (void) displayContentController: (UIViewController*) content;
{
    UIViewController *vc=[self topMostController];
    
    [vc addChildViewController:content];
    
    
    
    content.view.frame = CGRectMake(0, 0, vc.view.frame.size.width, vc.view.frame.size.height);
    
    [vc.view addSubview:content.view];
    [content didMoveToParentViewController:vc];
    
    [self showViewControllerFromLeftSide];
}

-(void)showViewControllerFromLeftSide{
    UIViewController *vc=[self topMostController];
    
    
    [UIView animateWithDuration:0.4 animations:^{
        itemMoreVC.view.frame = CGRectMake(0, 0, vc.view.frame.size.width, vc.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
}

-(void)hideViewController{
    UIViewController *vc=[self topMostController];
    
    //if (itemMoreVC == nil) {
    itemMoreVC = [self getHomeDetailLayerLeftController];
    //}
    //add animation from left side
    [UIView animateWithDuration:0.4 animations:^{
        itemMoreVC.view.frame = CGRectMake(vc.view.frame.size.width, 0, vc.view.frame.size.width, vc.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
    
}









//------------------------
-(IBAction)fav_action:(MyButton*)sender
{
    [singleton traceEvents:@"Favourite button" withAction:@"Clicked" withLabel:@"Home Tab" andValue:0];
    
    MyButton *button = (MyButton *)sender; //instance of UIButton
    int indexOfTheRow=(int)button.tag;   //tag of the button
    cellData = (NSDictionary*)[self.collectionData objectAtIndex:indexOfTheRow];
    
    
    
    
    NSLog(@"tabledata=%@",cellData);
    
    // Add image to button for pressed state
    UIImage * btnImage1 = [UIImage imageNamed:@"icon_favourite"];
    UIImage * btnImage2 = [UIImage imageNamed:@"icon_favourite_red"];
    
    [button setImage:btnImage1 forState:UIControlStateNormal];
    [button setImage:btnImage2 forState:UIControlStateSelected];
    
    NSString *serviceId=[cellData valueForKey:@"SERVICE_ID"];
    
    NSString *serviceFav=[singleton.dbManager getServiceFavStatus:serviceId];
    
    
    
    // NSString *serviceFav=[cellData valueForKey:@"SERVICE_IS_FAV"];
    if ([serviceFav isEqualToString:@"true"])// Is selected?
    {
        button.selected=FALSE;

        [singleton.dbManager updateServiceIsFav:serviceId serviceIsFav:@"false" hitAPI:@"Yes"];
        
    }
    else
    {
        button.selected=true;

        [singleton.dbManager updateServiceIsFav:serviceId serviceIsFav:@"true" hitAPI:@"Yes"];
        
    }
    
    //[self startCollectionNotifier];
    [self performSelector:@selector(startCollectionNotifier) withObject:nil afterDelay:0.20];
    
    // [_collectionView reloadData];
}
- (void) startCollectionNotifier
{
    // All instances of TestClass will be notified
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADCOLLECTIONVIEW" object:nil];
    
    
}


@end
