//
//  DetailServiceVC.m
//  Umang
//
//  Created by admin on 29/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "DetailServiceVC.h"
#import "DescriptionCell.h"
#import "AddressCell.h"
#import "WorkingHoursCell.h"
#import "MainDetailCell.h"
#import "ContactDetailCell.h"
#import "SimpleBarChart.h"
#import "AMRatingControl.h"
#import "RatingGraphView.h"
#import "RatingBarCell.h"
#import "UIImageView+WebCache.h"
#import <MessageUI/MessageUI.h>
#import "HomeDetailVC.h"
#import "UMAPIManager.h"
#import "MBProgressHUD.h"
#import "serviceCallContactVC.h"

#import "rateCommentVC.h"

#import "UIView+Toast.h"
#import "DetailCustomTableViewCell.h"
static const NSInteger kStarWidthAndHeight = 25;


@interface DetailServiceVC () <UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate,UIActionSheetDelegate>
{
    __weak IBOutlet UITableView *tblDetail;
    BOOL isRatingShown;
    BOOL isRatingCellShown;
    
    
    __weak IBOutlet UIButton *btnBack;
    AddressCell *addressCell;
    ContactDetailCell *contactCell;
    MainDetailCell *mainCell;
    SharedManager *singleton;
    MBProgressHUD *hud ;
    serviceCallContactVC *serviceVC;
    rateCommentVC *rateVc;
    IBOutlet UIButton *vistiService;
    RatingGraphView *vwRating ;
    
    CGRect framevwRatebtn;    //deepak change
    
    int totalDownload;
    
    CGFloat fontApplied;
    CGFloat headerTitleHeight;
    
}

@property (nonatomic, strong)IBOutlet AMRatingControl *imagesRatingControl;
@property (weak)IBOutlet UILabel *titleview;
@property(nonatomic,retain)NSString *ratePass;
@property(nonatomic,retain)NSString *commentPass;


@property(nonatomic,retain)NSString *rt1;
@property(nonatomic,retain)NSString *rt2;
@property(nonatomic,retain)NSString *rt3;
@property(nonatomic,retain)NSString *rt4;
@property(nonatomic,retain)NSString *rt5;
@property(nonatomic,retain)NSArray *contactitems ;
@end


@implementation DetailServiceVC
@synthesize imagesRatingControl;
@synthesize dic_serviceInfo;
@synthesize commentPass;

@synthesize rt1,rt2,rt3,rt4,rt5;


- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    fontApplied = 14.0;
    headerTitleHeight = 50.0;
    
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.shouldRotate = NO;
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    
    totalDownload=0;
    NSLog(@"dic_serviceInfo=%@",dic_serviceInfo);
    [btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    
    NSString *currentSelectLang = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if ([currentSelectLang isEqualToString:@"ta-IN"])//te-IN if telgu
    {
        
        CGSize size = [NSLocalizedString(@"back", nil) sizeWithAttributes:
                       @{NSFontAttributeName: [UIFont systemFontOfSize:btnBack.titleLabel.font.pointSize]}];
        
        CGRect framebtn=CGRectMake(btnBack.frame.origin.x, btnBack.frame.origin.y, btnBack.frame.size.width, btnBack.frame.size.height);
        
        [btnBack setFrame:CGRectMake(framebtn.origin.x,framebtn.origin.y,size.width+30, framebtn.size.height)];
        btnBack.contentEdgeInsets = UIEdgeInsetsMake(0,10, 0, 0);
        
    }
    
    [btnBack setTitle:@"" forState:UIControlStateNormal];
    singleton=[SharedManager sharedSingleton];
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:DETAIL_SERVICE_SCREEN];
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    [contactCell.btnCall setTitle:NSLocalizedString(@"call", nil) forState:UIControlStateNormal];
    [vistiService setTitle:NSLocalizedString(@"visit_service", nil) forState:UIControlStateNormal];
    addressCell.btn_viewmap.titleLabel.text=NSLocalizedString(@"view_on_map", nil);
    
    
    UIImage *dot, *star;
    dot = [UIImage imageNamed:@"star_border"];
    star = [UIImage imageNamed:@"star_filled"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hitFetchUserRatingAPI)
                                                 name:@"RELOADRATING"
                                               object:nil];
    
    
    
    imagesRatingControl = [[AMRatingControl alloc] initWithLocation:CGPointMake(fDeviceWidth/2-30,mainCell.lbl_titlerateus.frame.size.height+mainCell.lbl_titlerateus.frame.origin.y+10)emptyImage:dot solidImage:star andMaxRating:5];
    
    mainCell.lbl_titlerateus.text = NSLocalizedString(@"rate_service_on_umang", nil);
    mainCell.lbl_serviceRating.text= [NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_RATING"]];
    [mainCell.btnViewDetails setTitle:NSLocalizedString(@"view_ratings", nil) forState:UIControlStateNormal];
    
    // Do any additional setup after loading the view.
    isRatingShown = YES;
    
    //_titleview.frame=CGRectMake(84, 30, fDeviceWidth-100, 30);
    
    _titleview.text= [NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_NAME"]];
    
    
    NSLog(@"dic_serviceInfo=%@",dic_serviceInfo);
    
    [tblDetail reloadData];
    
    if ([self.isFrom isEqualToString:@"DEPARTMENT"])
    {
        [self scrollToBottom];
    }
    else
    {
        
    }
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
}

- (void)scrollToBottom
{
    CGFloat yOffset = 0;
    
    if (tblDetail.contentSize.height > tblDetail.bounds.size.height) {
        yOffset = tblDetail.contentSize.height - tblDetail.bounds.size.height;
    }
    
    [tblDetail setContentOffset:CGPointMake(0, yOffset) animated:NO];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    mainCell.lbl_titlerateus.text = NSLocalizedString(@"rate_service_on_umang", nil);
    [mainCell.btnViewDetails  setTitle:NSLocalizedString(@"view_ratings", nil) forState:UIControlStateNormal];
    
    mainCell.btnViewDetails.titleLabel.text=NSLocalizedString(@"view_ratings", nil);
    
    if (![self connected]) {
        // Not connected
        
    } else {
        // Connected. Do some Internet stuff
        [self hitFetchUserRatingAPI];
        
    }
    [tblDetail reloadData];
    
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [tblDetail reloadData];
            
        });
    });
    
    
    
    
    [super viewWillAppear:NO];
}

- (BOOL)connected
{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
    
}


-(IBAction)vistiServiceAction:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    HomeDetailVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeDetailVC"];
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    vc.dic_serviceInfo=dic_serviceInfo ;
    vc.tagComeFrom=@"OTHERS";
    
    [self presentViewController:vc animated:NO completion:nil];
}


-(void)ratingView
{
    // - and max rating
    [self displayRateUsController:[self getrateCommentVc]];
    
}


-(rateCommentVC*)getrateCommentVc
{
    
    
    NSLog(@"value fo rate=%@",self.ratePass);
    NSLog(@"value fo commentPass=%@",self.commentPass);
    
    
    
    
    
    if ([self.ratePass length]==0) {
        self.ratePass=@"";
    }
    if ([commentPass length]==0) {
        commentPass=@"";
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    rateVc = [storyboard instantiateViewControllerWithIdentifier:@"rateCommentVC"];
    
    rateVc.ratedBy=self.ratePass;
    rateVc.comment=commentPass;
    rateVc.dictionary=dic_serviceInfo;
    
    
    return rateVc;
}


- (void) displayRateUsController: (UIViewController*) content;
{
    [self addChildViewController:content];
    content.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:content.view];
    [content didMoveToParentViewController:self];
    [self showRateUSViewpopUp];
}

-(void)showRateUSViewpopUp{
    
    [UIView animateWithDuration:0.4 animations:^{
        rateVc.view.frame = CGRectMake(0, 0,self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
}





//----------------
-(serviceCallContactVC*)getserviceCallContacts
{
    
    NSString *contacts=[dic_serviceInfo valueForKey:@"SERVICE_PHONE_NUMBER"];
    NSArray *contactitems = [contacts componentsSeparatedByString:@","];
    NSLog(@"contactitems to be shown in pop up =%@",contactitems);
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    serviceVC = [storyboard instantiateViewControllerWithIdentifier:@"serviceCallContactVC"];
    serviceVC.tablearray=contactitems;//change it to URL on demand
    return serviceVC;
}





//----------

- (void) displayContentController: (UIViewController*) content;
{
    UIViewController *vc=[self topMostController];
    
    [vc addChildViewController:content];
    
    
    
    content.view.frame = CGRectMake(0, 0, vc.view.frame.size.width, vc.view.frame.size.height);
    
    [vc.view addSubview:content.view];
    [content didMoveToParentViewController:vc];
    
    [self showViewControllerFromLeftSide];
}

-(void)showViewControllerFromLeftSide{
    UIViewController *vc=[self topMostController];
    
    //if (itemMoreVC == nil)
    //{
    //itemMoreVC = [self getHomeDetailLayerLeftController];
    //}
    //add animation from left side
    [UIView animateWithDuration:0.4 animations:^{
        serviceVC.view.frame = CGRectMake(0, 0, vc.view.frame.size.width, vc.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
}

-(void)hideViewController{
    UIViewController *vc=[self topMostController];
    
    //if (itemMoreVC == nil) {
    serviceVC = [self getserviceCallContacts];
    //}
    //add animation from left side
    [UIView animateWithDuration:0.4 animations:^{
        serviceVC.view.frame = CGRectMake(vc.view.frame.size.width, 0, vc.view.frame.size.width, vc.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
    
}

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}









- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 5.0;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    return 5.0;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat heightRow = 0.0;
    switch (indexPath.section) {
        case 0:
            if (isRatingCellShown) {
                heightRow = 380.0;
            }
            else{
                heightRow = 230.0;
            }
            break;
            
        case 1:
        {
            NSString *serviceData = [dic_serviceInfo valueForKey:@"SERVICE_DESC"];
            CGRect dynamicHeight = [self rectForText:serviceData usingFont:[UIFont systemFontOfSize:fontApplied] boundedBySize:CGSizeMake(self.view.frame.size.width-40, 1000.0)];
            
            CGFloat textHeight = MAX(30, dynamicHeight.size.height);
            
            heightRow = textHeight + headerTitleHeight;
        }
            break;
        case 2:
        {
            NSString *workingHoursData = [dic_serviceInfo valueForKey:@"SERVICE_WORKINGHOURS"];
            CGRect dynamicHeight = [self rectForText:workingHoursData usingFont:[UIFont systemFontOfSize:fontApplied] boundedBySize:CGSizeMake(self.view.frame.size.width-40, 1000.0)];
            
            CGFloat textHeight = MAX(30, dynamicHeight.size.height);
            
            heightRow = textHeight + headerTitleHeight;
        }
            break;
        case 3:
        {
            NSString *addressData = [dic_serviceInfo valueForKey:@"SERVICE_DEPTADDRESS"];
            CGRect dynamicHeight = [self rectForText:addressData usingFont:[UIFont systemFontOfSize:fontApplied] boundedBySize:CGSizeMake(self.view.frame.size.width-40, 1000.0)];
            
            CGFloat textHeight = MAX(30, dynamicHeight.size.height);
            
            heightRow = textHeight + headerTitleHeight + 30;
        }
            break;
        case 4:
            heightRow = 180.0;
            break;
            
        default:
            break;
    }
    
    return heightRow;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;              // Default is 1 if not implemented
{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        static NSString *CellIdentifier = @"MainDetailCell";
        mainCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        [mainCell.starRatingView setScore:.1*2 withAnimation:NO];//pass data here

        //UIImage *dot, *star;
        //dot = [UIImage imageNamed:@"star_border"];
        //star = [UIImage imageNamed:@"star_filled"];
        
        NSString *rating=@"";
        if ([[dic_serviceInfo objectForKey:@"SERVICE_RATING"] length]!=0)
        {
            rating=[dic_serviceInfo objectForKey:@"SERVICE_RATING"];
            float fCost = [rating floatValue]/10;
            [mainCell.starRatingView setScore:fCost*2 withAnimation:NO];
        }
        else
        {
            [mainCell.starRatingView setScore:0*2 withAnimation:NO];
        }
        imagesRatingControl.frame = CGRectMake(fDeviceWidth/2-(5 * kStarWidthAndHeight+70)/2,mainCell.lbl_titlerateus.frame.size.height+mainCell.lbl_titlerateus.frame.origin.y+10,(5 * kStarWidthAndHeight+70),kStarWidthAndHeight);
        
        
        [mainCell.btnViewDetails addTarget:self action:@selector(btnViewDetailsClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        __weak typeof(self) weakSelf = self;
        imagesRatingControl.editingDidEndBlock = ^(NSUInteger rating)
        {
            NSLog(@"editingDidEndBlock %@",[NSString stringWithFormat:@"%lu_star", (unsigned long)rating]);
            weakSelf.ratePass=[NSString stringWithFormat:@"%lu",(unsigned long)rating];
            // [weakSelf hitRatingserviceAPI:ratingStar];
            [weakSelf ratingView];
        };
        //
        // Add the control(s) as a subview of your view
        [mainCell.contentView addSubview:imagesRatingControl];
        mainCell.contentView.backgroundColor=[UIColor clearColor];
        mainCell.lbl_titlerateus.text = NSLocalizedString(@"rate_service_on_umang", nil);
        
        NSString *category = [dic_serviceInfo valueForKey:@"SERVICE_CATEGORY"];
        
        mainCell.lbl_serviceCat.text= category;
        mainCell.lbl_serviceCat.backgroundColor = [UIColor clearColor];
        mainCell.lbl_serviceCat.layer.cornerRadius = 8.0;
        mainCell.lbl_serviceCat.layer.backgroundColor=[UIColor colorWithRed:235/255.0 green:86/255.0 blue:2/255.0 alpha:1.0].CGColor;
        mainCell.lbl_serviceCat.textColor = [UIColor whiteColor];
        mainCell.lbl_serviceCat.textAlignment = NSTextAlignmentCenter;
        
        for(UIView *subview in [mainCell.contentView subviews])
        {
            if(subview.tag==9912)
            {
                [subview removeFromSuperview];
            }
        }
        
        
        UIView *vwRate=[self addRatingGraphView];
        vwRate.tag=9912;
        mainCell.btnViewDetails.backgroundColor = [UIColor clearColor];
        if (isRatingCellShown)
        {
            [mainCell.contentView addSubview:vwRate];
            [mainCell.btnViewDetails  setTitle:NSLocalizedString(@"hide_ratings", nil) forState:UIControlStateNormal];
            
        }
        else
        {
            [vwRate removeFromSuperview];
            [mainCell.btnViewDetails  setTitle:NSLocalizedString(@"view_ratings", nil) forState:UIControlStateNormal];
            framevwRatebtn =CGRectMake(fDeviceWidth/2-100, 195, 200, 30);
            
            mainCell.btnViewDetails.frame=framevwRatebtn;
            
        }
        //---- Show service Image-----
        NSURL *url=[NSURL URLWithString:[dic_serviceInfo objectForKey:@"SERVICE_IMAGE"]];
        [mainCell.img_service sd_setImageWithURL:url
                                placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
        mainCell.lbl_serviceName.text=[dic_serviceInfo valueForKey:@"SERVICE_NAME"];
        mainCell.lbl_serviceName.numberOfLines = 0;
        mainCell.lbl_serviceCat.layer.cornerRadius = 8.0;
        mainCell.lbl_serviceCat.font = [UIFont systemFontOfSize:13.0];
        
        //NSLog(@"get_BUcolorcode=%@",get_BUcolorcode);
        
        return mainCell;
        
    }
    else if (indexPath.section == 1 ){
        static NSString *cellIdentifier = @"DescCell";
        DetailCustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[DetailCustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            [cell designInterfaceForFrame:tableView.frame withFontSizeApplied:fontApplied withMapOptionRequired:NO];
        }
        
        
        NSString *descriptionTxt=[dic_serviceInfo valueForKey:@"SERVICE_DESC"];
        [cell bindCellDataWithTitle:NSLocalizedString(@"description", nil) andDesc:descriptionTxt withScreenWidth:tableView.frame.size.width - 30];
        
        return  cell;
        
    }
    else if (indexPath.section == 2 ){
        static NSString *cellIdentifier = @"workingHours";
        DetailCustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[DetailCustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            [cell designInterfaceForFrame:tableView.frame withFontSizeApplied:fontApplied withMapOptionRequired:NO];
        }
        
        NSString *workingText=[dic_serviceInfo valueForKey:@"SERVICE_WORKINGHOURS"];
        [cell bindCellDataWithTitle:NSLocalizedString(@"working_hours", nil) andDesc:workingText withScreenWidth:tableView.frame.size.width - 30];
        
        return  cell;
        
    }
    else if (indexPath.section == 3 ){
        static NSString *cellIdentifier = @"addressCell";
        DetailCustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[DetailCustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            [cell designInterfaceForFrame:tableView.frame withFontSizeApplied:fontApplied withMapOptionRequired:YES];
        }
        
        NSString *addressText=[dic_serviceInfo valueForKey:@"SERVICE_DEPTADDRESS"];
        
        [cell bindCellDataWithTitle:NSLocalizedString(@"address", nil) andDesc:addressText withScreenWidth:tableView.frame.size.width - 30];
        
        [cell.btnViewOnMap addTarget:self action:@selector(viewOnMap) forControlEvents:UIControlEventTouchUpInside];
        cell.btnViewOnMap.titleLabel.textColor = [UIColor colorWithRed:9.0/255.0 green:59.0/255.0 blue:135.0/255.0 alpha:1.0];
        
        return  cell;
    }
    else
    {
        static NSString *CellIdentifier = @"ContactDetailCell";
        contactCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        contactCell.lbl_title_contact.text=NSLocalizedString(@"contact", nil);
        CGRect contactFrame =    contactCell.lbl_title_contact.frame;
        contactCell.lbl_title_contact.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
        
        
        //  contactFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(contactCell.frame) - 100 : 15;
        contactCell.lbl_title_contact.frame = contactFrame;
        // contactCell.lbl_title_contact.frame = CGRectMake(contactFrame.origin.x, contactFrame.origin.y, contactFrame.size.width+70, contactFrame.size.height);
        
        NSString *phoneNumber = [dic_serviceInfo valueForKey:@"SERVICE_PHONE_NUMBER"];
        NSString *emailID = [dic_serviceInfo valueForKey:@"SERVICE_EMAIL"];
        NSString *webSiteLink = [dic_serviceInfo valueForKey:@"SERVICE_WEBSITE"];
        
        if (phoneNumber.length) {
            contactCell.btnCall.hidden = NO;
            [contactCell.btnCall setTitle:NSLocalizedString(@"call", nil) forState:UIControlStateNormal];
        }
        else{
            contactCell.btnCall.hidden = YES;
        }
        
        [contactCell.btn_email setTitle:emailID forState:UIControlStateNormal];
        [contactCell.btn_visitWebsite setTitle:NSLocalizedString(@"visit_website", nil) forState:UIControlStateNormal];
        [contactCell.btn_visitWebsite setTitle:NSLocalizedString(@"visit_website", nil) forState:UIControlStateSelected];
        
        [contactCell.btn_email addTarget:self action:@selector(emailAction) forControlEvents:UIControlEventTouchUpInside];
        [contactCell.btn_visitWebsite addTarget:self action:@selector(visitAction) forControlEvents:UIControlEventTouchUpInside];
        [contactCell.btnCall addTarget:self action:@selector(callAction) forControlEvents:UIControlEventTouchUpInside];
        
        [contactCell showEmailOption:NO];
        [contactCell showVisitWebsiteOption:NO];
        
        
        CGRect callBtnFrame = contactCell.btnCall.frame;
        
        CGFloat padding = 50;
        // Both options must be available
        if (emailID.length && webSiteLink.length) {
            
            [contactCell showEmailOption:YES];
            [contactCell showVisitWebsiteOption:YES];
        }
        else if (emailID.length){
            [contactCell showEmailOption:YES];
            [contactCell showVisitWebsiteOption:NO];
            padding += 50;
            
        }
        else if (webSiteLink.length){
            [contactCell showEmailOption:NO];
            [contactCell showVisitWebsiteOption:YES];
            
            //Move Website option to the upper side
            contactCell.imgWebsite.frame = contactCell.imgViewMail.frame;
            contactCell.lineWeb.frame = contactCell.lineImgEmail.frame;
            contactCell.btn_visitWebsite.frame = contactCell.btn_email.frame;
            
            padding += 50;
            
            
        }
        
        callBtnFrame.origin.y = contactCell.frame.size.height - padding;
        
        contactCell.btnCall.frame = callBtnFrame;
        
        
        return contactCell;
        
    }
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell isKindOfClass:[MainDetailCell class]])
    {
        
        MainDetailCell *cellMain = (MainDetailCell*)cell;
        
        /*  CGRect frameImageVw = cellMain.img_service.frame;
         CGRect frameServiceName = cellMain.lbl_serviceName.frame;
         CGRect frameRatingText = cellMain.lbl_serviceRating.frame;
         CGRect frameCategory = cellMain.lbl_serviceCat.frame;
         CGRect frameRatingView = cellMain.starRatingView.frame;
         
         if (singleton.isArabicSelected) {
         cellMain.lbl_serviceName.textAlignment = NSTextAlignmentRight;
         cellMain.lbl_serviceRating.textAlignment = NSTextAlignmentRight;
         cellMain.lbl_serviceCat.textAlignment = NSTextAlignmentRight;
         
         frameImageVw = CGRectMake(CGRectGetWidth(cellMain.frame)-frameImageVw.size.width - 10, frameImageVw.origin.y, frameImageVw.size.width, frameImageVw.size.height);
         
         CGFloat yPosition = 10;
         CGFloat xPosition = 80;
         
         CGFloat width = cellMain.frame.size.width - 100;
         
         frameServiceName = CGRectMake(80, yPosition, width, 40);
         frameCategory = CGRectMake(80, yPosition, width, 40);
         frameServiceName = CGRectMake(80, yPosition, width, 40);
         
         }
         else{
         cellMain.lbl_serviceName.textAlignment = NSTextAlignmentLeft;
         cellMain.lbl_serviceRating.textAlignment = NSTextAlignmentLeft;
         cellMain.lbl_serviceCat.textAlignment = NSTextAlignmentLeft;
         
         frameImageVw = CGRectMake(10, frameImageVw.origin.y, frameImageVw.size.width, frameImageVw.size.height);
         
         }*/
        
        CGRect buttonImageDetailFrame =  cellMain.img_service.frame;
        buttonImageDetailFrame.origin.x = singleton.isArabicSelected ?CGRectGetWidth(cellMain.frame) - 60 : 12;
        cellMain.img_service.frame = buttonImageDetailFrame;
        
        
        CGRect lblserviceNameFrame =  cellMain.lbl_serviceName.frame;
        lblserviceNameFrame.origin.x = singleton.isArabicSelected ? 10  : 80;
        lblserviceNameFrame.size.width = fDeviceWidth - 80;
        cellMain.lbl_serviceName.frame = lblserviceNameFrame;
        cellMain.lbl_serviceName.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
        
        NSString *category = [dic_serviceInfo valueForKey:@"SERVICE_CATEGORY"];
        
        cellMain.lbl_serviceCat.text= category;
        
        
        CGRect lblserviceRatingFrame =  cellMain.lbl_serviceRating.frame;
        lblserviceRatingFrame.origin.x = singleton.isArabicSelected ? fDeviceWidth - 105  : 80;
        cellMain.lbl_serviceRating.frame = lblserviceRatingFrame;
        
        cellMain.lbl_serviceCat.font = [UIFont systemFontOfSize:13.0];
        CGRect frame = [self rectForText:category usingFont:[UIFont systemFontOfSize:12.0] boundedBySize:CGSizeMake(self.view.frame.size.width, cellMain.lbl_serviceCat.frame.size.height)];
        
        
        
        cellMain.lbl_serviceCat.frame=CGRectMake(cellMain.lbl_serviceCat.frame.origin.x, cellMain.lbl_serviceCat.frame.origin.y, frame.size.width+20,cellMain.lbl_serviceCat.frame.size.height);
        
        cellMain.lbl_serviceCat.layer.backgroundColor=[UIColor colorWithRed:235/255.0 green:86/255.0 blue:2/255.0 alpha:1.0].CGColor;
        cellMain.lbl_serviceCat.textAlignment=NSTextAlignmentCenter;
        
        
        cellMain.lbl_serviceRating.text= [NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_RATING"]];
        
        cellMain.lbl_serviceRating.textAlignment = singleton.isArabicSelected?NSTextAlignmentRight: NSTextAlignmentLeft;
        CGRect starImageFrame =  cellMain.starRatingView.frame;
        starImageFrame.origin.x = singleton.isArabicSelected ? lblserviceRatingFrame.origin.x-60  : lblserviceRatingFrame.origin.x  + 30 ;
        cellMain.starRatingView.frame = starImageFrame;
        
        
        
        cellMain.lbl_serviceCat.font = [UIFont systemFontOfSize:14.0];
        CGRect catFrame = [self rectForText:category usingFont:[UIFont systemFontOfSize:14.0] boundedBySize:CGSizeMake(self.view.frame.size.width - 80, 60)];
        if (singleton.isArabicSelected)
        {
            cellMain.lbl_serviceCat.frame = CGRectMake(fDeviceWidth - 80 - catFrame.size.width, 52, catFrame.size.width+10, catFrame.size.height+3);
        }
        else
        {
            cellMain.lbl_serviceCat.frame= CGRectMake(80, 52, catFrame.size.width+10, catFrame.size.height+3);
        }
        
        
    }
}

/*
 - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
 {
 
 
 if (indexPath.section == 0)
 {
 if (isRatingCellShown==YES)
 {
 return 160+220;
 }
 else
 return  220.0;
 
 }
 
 
 else if (indexPath.section == 1)
 {
 
 CGFloat retVal = 120.0;
 
 //  NSString *txtService = [dic_serviceInfo valueForKey:@"SERVICE_DEPTDESCRIPTION"];
 NSString *txtService=[dic_serviceInfo valueForKey:@"SERVICE_DESC"];
 
 
 CGRect dynamicHeight = [self rectForText:txtService usingFont:[UIFont systemFontOfSize:17.0] boundedBySize:CGSizeMake(self.view.frame.size.width-40, 1000.0)];
 retVal = MAX(120, dynamicHeight.size.height) + 30;
 
 retVal = retVal > 120 ? retVal - 20 : retVal;
 
 //    NSLog(@"Height = %lf",retVal);
 
 return retVal;
 
 
 
 }
 else if (indexPath.section ==2)
 {
 CGFloat retVal = 54.0;
 
 NSString *txtService =  [dic_serviceInfo valueForKey:@"SERVICE_WORKINGHOURS"];
 CGRect dynamicHeight = [self rectForText:txtService usingFont:[UIFont systemFontOfSize:17.0] boundedBySize:CGSizeMake(self.view.frame.size.width-40, 1000.0)];
 retVal = MAX(65.0, dynamicHeight.size.height) + 15;
 
 //    NSLog(@"Height = %lf",retVal);
 
 return retVal;
 
 }
 else if (indexPath.section == 3)
 {
 
 CGFloat retVal = 80.0;
 
 NSString *txtService =  [dic_serviceInfo valueForKey:@"SERVICE_DEPTADDRESS"];
 
 
 
 CGRect dynamicHeight = [self rectForText:txtService usingFont:[UIFont systemFontOfSize:17.0] boundedBySize:CGSizeMake(self.view.frame.size.width, 1000.0)];
 retVal = MAX(80.0, dynamicHeight.size.height) + 40;
 
 retVal = retVal > 120 ? retVal+20  : retVal +10 ;
 
 //    NSLog(@"Height = %lf",retVal);
 
 return retVal;
 }
 else
 return 180;
 
 
 
 }
 
 
 
 - (void)setUILabelTextWithVerticalAlignTop:(NSString *)theText withtargetLabel:(UILabel*)targetLabel
 {
 CGSize labelSize = CGSizeMake(targetLabel.frame.size.width-20, 300);
 CGSize theStringSize = [theText sizeWithFont:targetLabel.font constrainedToSize:labelSize lineBreakMode:targetLabel.lineBreakMode];
 targetLabel.frame = CGRectMake(targetLabel.frame.origin.x, targetLabel.frame.origin.y, theStringSize.width, theStringSize.height);
 targetLabel.text = theText;
 }
 
 
 
 
 
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
 {
 if (indexPath.section == 0)
 {
 static NSString *CellIdentifier = @"MainDetailCell";
 mainCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 [mainCell.starRatingView setScore:.1*2 withAnimation:NO];//pass data here
 UIImage *dot, *star;
 dot = [UIImage imageNamed:@"star_border"];
 star = [UIImage imageNamed:@"star_filled"];
 NSString *rating=@"";
 if ([[dic_serviceInfo objectForKey:@"SERVICE_RATING"] length]!=0)
 {
 rating=[dic_serviceInfo objectForKey:@"SERVICE_RATING"];
 float fCost = [rating floatValue]/10;
 [mainCell.starRatingView setScore:fCost*2 withAnimation:NO];
 }
 else
 {
 [mainCell.starRatingView setScore:0*2 withAnimation:NO];
 }
 imagesRatingControl.frame = CGRectMake(fDeviceWidth/2-(5 * kStarWidthAndHeight+70)/2,mainCell.lbl_titlerateus.frame.size.height+mainCell.lbl_titlerateus.frame.origin.y+10,(5 * kStarWidthAndHeight+70),kStarWidthAndHeight);
 
 
 [mainCell.btnViewDetails addTarget:self action:@selector(btnViewDetailsClicked:) forControlEvents:UIControlEventTouchUpInside];
 
 __weak typeof(self) weakSelf = self;
 imagesRatingControl.editingDidEndBlock = ^(NSUInteger rating)
 {
 NSLog(@"editingDidEndBlock %@",[NSString stringWithFormat:@"%lu_star", (unsigned long)rating]);
 weakSelf.ratePass=[NSString stringWithFormat:@"%lu",(unsigned long)rating];
 // [weakSelf hitRatingserviceAPI:ratingStar];
 [weakSelf ratingView];
 };
 //
 // Add the control(s) as a subview of your view
 [mainCell.contentView addSubview:imagesRatingControl];
 mainCell.contentView.backgroundColor=[UIColor clearColor];
 mainCell.lbl_titlerateus.text = NSLocalizedString(@"rate_service_on_umang", nil);
 
 for(UIView *subview in [mainCell.contentView subviews])
 {
 if(subview.tag==9912)
 
 {
 [subview removeFromSuperview];
 }
 else
 
 {
 
 }
 }
 
 
 UIView *vwRate=[self addRatingGraphView];
 vwRate.tag=9912;
 
 if (isRatingCellShown)
 {
 [mainCell.contentView addSubview:vwRate];
 [mainCell.btnViewDetails  setTitle:NSLocalizedString(@"hide_ratings", nil) forState:UIControlStateNormal];
 
 }
 else
 {
 [vwRate removeFromSuperview];
 [mainCell.btnViewDetails  setTitle:NSLocalizedString(@"view_ratings", nil) forState:UIControlStateNormal];
 
 }
 //---- Show service Image-----
 NSURL *url=[NSURL URLWithString:[dic_serviceInfo objectForKey:@"SERVICE_IMAGE"]];
 [mainCell.img_service sd_setImageWithURL:url
 placeholderImage:[UIImage imageNamed:@"img_loadertime.png"]];
 mainCell.lbl_serviceName.text=[dic_serviceInfo valueForKey:@"SERVICE_NAME"];
 mainCell.lbl_serviceName.numberOfLines = 0;
 //mainCell.btn_serviceCat.titleLabel.text=[dic_serviceInfo valueForKey:@"SERVICE_CATEGORY"];
 
 mainCell.lbl_serviceCat.backgroundColor = [UIColor clearColor];
 mainCell.lbl_serviceCat.layer.cornerRadius = 8.0;
 //NSLog(@"get_BUcolorcode=%@",get_BUcolorcode);
 mainCell.lbl_serviceCat.textColor=[UIColor whiteColor];
 
 
 
 
 //Aditi
 
 
 
 
 
 return mainCell;
 
 }
 
 else if (indexPath.section == 1)
 {
 static NSString *CellIdentifier = @"DescriptionCell";
 DescriptionCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 // @synthesize lbl_desc,lbl_title_desc;
 cell.lbl_title_desc.text=NSLocalizedString(@"description", nil);
 
 CGRect labelFrame =    cell.lbl_title_desc.frame;
 labelFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(cell.frame) - 100 : 15;
 cell.lbl_title_desc.frame = labelFrame;
 
 NSString *descriptionTxt=[dic_serviceInfo valueForKey:@"SERVICE_DESC"];
 NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[descriptionTxt dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
 
 CGRect dynamicHeight = [self rectForText:descriptionTxt usingFont:[UIFont systemFontOfSize:17.0] boundedBySize:CGSizeMake(tblDetail.frame.size.width-40, 1000.0)];
 CGRect addressLabelFrame =    cell.lbl_desc.frame;
 addressLabelFrame.origin.x = 15;
 addressLabelFrame.origin.y = 40;
 addressLabelFrame.size.height = dynamicHeight.size.height + 10;
 addressLabelFrame.size.width = CGRectGetWidth(cell.frame) - 30;
 cell.lbl_desc.frame = addressLabelFrame;
 
 
 cell.lbl_desc.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
 
 cell.lbl_desc.attributedText=attrStr;//SERVICE_DESC
 
 cell.lbl_desc.font =[UIFont systemFontOfSize:14];
 cell.lbl_desc.textColor=[UIColor grayColor];
 // cell.lbl_desc.textAlignment = NSTextAlignmentCenter;
 // [self setUILabelTextWithVerticalAlignTop:descriptionTxt withtargetLabel:cell.lbl_desc];
 
 [cell.lbl_desc setNumberOfLines:0];
 [cell.lbl_desc sizeToFit];
 
 // CGRect frameBG = cell.frame;
 // frameBG.size.height = MAX(100.0, dynamicHeight.size.height);
 //cell.frame = frameBG;
 
 //cell.lbl_desc.font=[UIFont systemFontOfSize:14];
 // [cell.lbl_desc setNumberOfLines:0];
 // [cell.lbl_desc sizeToFit];
 return cell;
 
 // cell.lbl_desc.text = [self getNewlineString:descriptionTxt];
 
 //[cell.lbl_desc sizeToFit];
 
 //        CGRect frameBG = cell.frame;
 //        frameBG.size.height = MAX(100.0, dynamicHeight.size.height) + 20;
 //        // frameBG.origin.y = 30;
 //        cell.frame = frameBG;
 
 //cell.lbl_desc.font=[UIFont systemFontOfSize:14];
 // [cell.lbl_desc setNumberOfLines:0];
 // [cell.lbl_desc sizeToFit];,,,,,,,,
 
 }
 
 else if (indexPath.section == 2)
 {
 static NSString *CellIdentifier = @"WorkingHoursCell";
 WorkingHoursCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 cell.btn_workinghours.titleLabel.text=NSLocalizedString(@"working_hours", nil);
 
 CGRect workingHourFrame =    cell.btn_workinghours.frame;
 workingHourFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(cell.frame) - 100 : 15;
 cell.btn_workinghours.frame = workingHourFrame;
 
 
 [cell.btn_workinghours setTitle:NSLocalizedString(@"working_hours", nil) forState:UIControlStateNormal];
 [ cell.btn_workinghours setTitle:NSLocalizedString(@"working_hours", nil) forState:UIControlStateSelected];
 
 
 
 CGRect addressLabelFrame =    cell.lbl_workinghours.frame;
 addressLabelFrame.origin.x = 15;
 addressLabelFrame.size.width = CGRectGetWidth(tableView.frame) - 30;
 cell.lbl_workinghours.frame = addressLabelFrame;
 cell.lbl_workinghours.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
 
 
 
 cell.lbl_workinghours.text=[dic_serviceInfo valueForKey:@"SERVICE_WORKINGHOURS"];
 CGRect dynamicHeight = [self rectForText:[dic_serviceInfo valueForKey:@"SERVICE_WORKINGHOURS"] usingFont:[UIFont systemFontOfSize:14.0] boundedBySize:CGSizeMake(tblDetail.frame.size.width-40, 1000.0)];
 cell.lbl_workinghours.textColor=[UIColor grayColor];
 cell.lbl_workinghours.lineBreakMode = NSLineBreakByWordWrapping;
 cell.lbl_workinghours.font = [UIFont systemFontOfSize:15.0];
 cell.lbl_workinghours.numberOfLines = 2;
 
 
 CGRect frameBG = cell.frame;
 frameBG.size.height = MAX(65.0, dynamicHeight.size.height) + 15;
 // frameBG.origin.y = 30;
 cell.frame = frameBG;
 
 
 cell.lbl_workinghours.frame = CGRectMake(addressLabelFrame.origin.x, addressLabelFrame.origin.y, addressLabelFrame.size.width, frameBG.size.height - 30);
 
 return cell;
 
 }
 else if (indexPath.section == 3)
 {
 static NSString *CellIdentifier = @"AddressCell";
 addressCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 addressCell.lbl_title_address.text= NSLocalizedString(@"address", nil);
 CGRect addressFrame =    addressCell.lbl_title_address.frame;
 addressFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(addressCell.frame) - 100 : 15;
 addressCell.lbl_title_address.frame = addressFrame;
 
 CGRect addressLabelFrame =    addressCell.lbl_address.frame;
 addressLabelFrame.origin.x = 15;
 addressLabelFrame.size.width = CGRectGetWidth(addressCell.frame) - 30;
 addressCell.lbl_address.frame = addressLabelFrame;
 addressCell.lbl_address.textAlignment = singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
 
 
 
 
 
 NSString *addressTxt=[dic_serviceInfo valueForKey:@"SERVICE_DEPTADDRESS"];;
 NSAttributedString * attrAddressTxtStr = [[NSAttributedString alloc] initWithData:[addressTxt dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
 addressCell.lbl_address.attributedText=attrAddressTxtStr;//SERVICE_DESC
 
 
 addressCell.lbl_address.font =[UIFont systemFontOfSize:14];
 addressCell.lbl_address.textColor=[UIColor grayColor];
 //addressCell.lbl_address.text=[dic_serviceInfo valueForKey:@"SERVICE_DEPTADDRESS"];
 
 //addressCell.lbl_address.text=[dic_serviceInfo valueForKey:@"SERVICE_DEPTADDRESS"];
 CGRect dynamicHeight = [self rectForText:[dic_serviceInfo valueForKey:@"SERVICE_DEPTADDRESS"] usingFont:[UIFont systemFontOfSize:14.0] boundedBySize:CGSizeMake(tblDetail.frame.size.width-40, 1000.0)];
 addressCell.btn_viewmap.titleLabel.text=NSLocalizedString(@"view_on_map", nil);
 
 
 [addressCell.btn_viewmap setTitle:NSLocalizedString(@"view_on_map", nil) forState:UIControlStateNormal];
 [addressCell.btn_viewmap setTitle:NSLocalizedString(@"view_on_map", nil) forState:UIControlStateSelected];
 
 
 
 
 // CGRect btnViewmapFrame =    addressCell.btn_viewmap.frame;
 //btnViewmapFrame.origin.x = singleton.isArabicSelected ? 15 :CGRectGetWidth(tableView.frame) - 160;
 
 // btnViewmapFrame.origin.x = singleton.isArabicSelected ? 15 :CGRectGetWidth(addressCell.frame) - 160;
 
 //  addressCell.btn_viewmap.frame = btnViewmapFrame;
 
 
 
 // [addressCell.lbl_address sizeToFit];
 
 [addressCell.btn_viewmap addTarget:self action:@selector(viewOnMap) forControlEvents:UIControlEventTouchUpInside];
 //  @synthesize lbl_address,lbl_title_address,btn_viewmap;
 
 
 CGRect frameBG = addressCell.frame;
 
 
 CGFloat cellHeight = MAX(65.0, dynamicHeight.size.height) + 15;
 
 frameBG.size.height = cellHeight > 80 ? cellHeight : cellHeight - 30 ;
 
 addressCell.lbl_address.frame = CGRectMake(addressFrame.origin.x, addressFrame.origin.y +20, addressFrame.size.width, frameBG.size.height - 15);
 addressCell.lbl_address.lineBreakMode = NSLineBreakByWordWrapping;
 addressCell.lbl_address.numberOfLines = 0;
 
 // frameBG.origin.y = 30;
 addressCell.frame = frameBG;
 
 
 return addressCell;
 
 
 }
 else
 {
 static NSString *CellIdentifier = @"ContactDetailCell";
 contactCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 contactCell.lbl_title_contact.text=NSLocalizedString(@"contact", nil);
 CGRect contactFrame =    contactCell.lbl_title_contact.frame;
 contactFrame.origin.x = singleton.isArabicSelected ? CGRectGetWidth(contactCell.frame) - 100 : 15;
 //contactCell.lbl_title_contact.frame = contactFrame;
 contactCell.lbl_title_contact.frame = CGRectMake(contactFrame.origin.x, contactFrame.origin.y, contactFrame.size.width+70, contactFrame.size.height);
 
 NSString *phoneNumber = [dic_serviceInfo valueForKey:@"SERVICE_PHONE_NUMBER"];
 NSString *emailID = [dic_serviceInfo valueForKey:@"SERVICE_EMAIL"];
 NSString *webSiteLink = [dic_serviceInfo valueForKey:@"SERVICE_WEBSITE"];
 
 if (phoneNumber.length) {
 contactCell.btnCall.hidden = NO;
 [contactCell.btnCall setTitle:NSLocalizedString(@"call", nil) forState:UIControlStateNormal];
 }
 else{
 contactCell.btnCall.hidden = YES;
 }
 
 [contactCell.btn_email setTitle:emailID forState:UIControlStateNormal];
 [contactCell.btn_visitWebsite setTitle:NSLocalizedString(@"visit_website", nil) forState:UIControlStateNormal];
 [contactCell.btn_visitWebsite setTitle:NSLocalizedString(@"visit_website", nil) forState:UIControlStateSelected];
 
 [contactCell.btn_email addTarget:self action:@selector(emailAction) forControlEvents:UIControlEventTouchUpInside];
 [contactCell.btn_visitWebsite addTarget:self action:@selector(visitAction) forControlEvents:UIControlEventTouchUpInside];
 [contactCell.btnCall addTarget:self action:@selector(callAction) forControlEvents:UIControlEventTouchUpInside];
 
 [contactCell showEmailOption:NO];
 [contactCell showVisitWebsiteOption:NO];
 
 
 CGRect callBtnFrame = contactCell.btnCall.frame;
 
 CGFloat padding = 50;
 // Both options must be available
 if (emailID.length && webSiteLink.length) {
 
 [contactCell showEmailOption:YES];
 [contactCell showVisitWebsiteOption:YES];
 }
 else if (emailID.length){
 [contactCell showEmailOption:YES];
 [contactCell showVisitWebsiteOption:NO];
 padding += 50;
 
 }
 else if (webSiteLink.length){
 [contactCell showEmailOption:NO];
 [contactCell showVisitWebsiteOption:YES];
 
 //Move Website option to the upper side
 contactCell.imgWebsite.frame = contactCell.imgViewMail.frame;
 contactCell.lineWeb.frame = contactCell.lineImgEmail.frame;
 contactCell.btn_visitWebsite.frame = contactCell.btn_email.frame;
 
 padding += 50;
 
 
 }
 
 callBtnFrame.origin.y = contactCell.frame.size.height - padding;
 
 contactCell.btnCall.frame = callBtnFrame;
 
 
 return contactCell;
 
 }
 
 
 }
 
 
 */



-(void)visitAction
{
    //[dic_serviceInfo valueForKey:@"SERVICE_WEBSITE"];
    NSString* website = [NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_WEBSITE"]];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:website]];
}



//----- hitAPI for IVR OTP call Type registration ------
-(void)hitFetchUserRatingAPI
{
    NSString *service_id=[NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_ID"]];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    [dictBody setObject:service_id forKey:@"sid"];//Enter mobile number of user
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"]; //tkn number
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_FETCH_USER_RATING withBody:dictBody andTag:TAG_REQUEST_FETCH_USER_RATING completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            // NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                commentPass=[[response valueForKey:@"pd"]valueForKey:@"cmt"];
                if ([[[response valueForKey:@"pd"]valueForKey:@"rt1"] length]!=0) {
                    rt1=[[response valueForKey:@"pd"]valueForKey:@"rt1"];
                }
                if ([[[response valueForKey:@"pd"]valueForKey:@"rt2"] length]!=0) {
                    rt2=[[response valueForKey:@"pd"]valueForKey:@"rt2"];
                }
                
                if ([[[response valueForKey:@"pd"]valueForKey:@"rt3"] length]!=0) {
                    rt3=[[response valueForKey:@"pd"]valueForKey:@"rt3"];
                }
                
                if ([[[response valueForKey:@"pd"]valueForKey:@"rt4"] length]!=0) {
                    rt4=[[response valueForKey:@"pd"]valueForKey:@"rt4"];
                }
                if ([[[response valueForKey:@"pd"]valueForKey:@"rt5"] length]!=0) {
                    rt5=[[response valueForKey:@"pd"]valueForKey:@"rt5"];
                }
                
                
                
                
                //cmt
                /*
                 
                 NSString *rating
                 pd =     {
                 cmt = "";
                 rating = "<null>";
                 rt1 = 11;
                 rt2 = 6;
                 rt3 = 6;
                 rt4 = 13;
                 rt5 = 32;
                 sid = 9;
                 };
                 rc
                 
                 
                 
                 */
                
                NSString *rating=@"";
                
                if ([[[response valueForKey:@"pd"]valueForKey:@"rating"] isKindOfClass:[NSNull class]])
                {
                    rating=@"0";
                    [imagesRatingControl setRating:[rating intValue]];
                    
                }
                
                
                else  if ([[[response valueForKey:@"pd"]valueForKey:@"rating"] length]!=0)
                {
                    
                    rating=[[response valueForKey:@"pd"]valueForKey:@"rating"];
                    
                    [imagesRatingControl setRating:[rating intValue]];
                    
                }
                
                [tblDetail reloadData];
                
                //[imagesRatingControl setRating:3];
                //----JUST FOR TEST
                
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            
            [self showToast:error.localizedDescription];
            
            /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
             message:error.localizedDescription
             delegate:self
             cancelButtonTitle:NSLocalizedString(@"ok", nil)
             otherButtonTitles:nil];
             [alert show];
             */
        }
        
    }];
    
}


-(void)showToast :(NSString *)toast
{
    [self.view makeToast:toast duration:5.0 position:CSToastPositionBottom];
}



-(NSString*)getNewlineString:(NSString*)existingStr{
    NSString *mainString = existingStr;
    for (int i = 0; i<10; i++) {
        NSString *tobeReplaced = [NSString stringWithFormat:@" %i.",i];
        NSString *replacedString = [NSString stringWithFormat:@"\n%i.",i];
        mainString = [mainString stringByReplacingOccurrencesOfString:tobeReplaced withString:replacedString];
    }
    return mainString;
}

-(void)hitRatingserviceAPI:(NSString*)rating
{
    
    NSString *service_id=[NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_ID"]];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"loading",nil);
    
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    
    [dictBody setObject:rating forKey:@"rating"];//Enter mobile number of user
    [dictBody setObject:service_id forKey:@"sid"];//Enter mobile number of user
    [dictBody setObject:singleton.mobileNumber forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"]; //tkn number
    [dictBody setObject:@"" forKey:@"st"]; //tkn number
    // [dictBody setObject:@"en" forKey:@"lcle"]; //tkn number
    
    
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile default email //not supported iphone
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_RATINGS withBody:dictBody andTag:TAG_REQUEST_API_RATINGS completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        [hud hideAnimated:YES];
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            NSString *rc=[response valueForKey:@"rc"];
            NSString *rs=[response valueForKey:@"rs"];
            
            NSLog(@"value of rc =%@ \n value of rs=%@ \n  ",rc,rs);
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            // NSString *rd=[response valueForKey:@"rd"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
                
            {
                
                //[imagesRatingControl setRating:3];
                //----JUST FOR TEST
                [tblDetail reloadData];
                
            }
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}



-(void)emailAction
{
    //  [dic_serviceInfo valueForKey:@"SERVICE_EMAIL"]
    
    
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    if ([MFMailComposeViewController canSendMail]) {
        
        
        NSString *receipt=[dic_serviceInfo valueForKey:@"SERVICE_EMAIL"];
        // Email Subject
        NSString *emailTitle = @"Umang Services Email";
        // Email Content
        NSString *messageBody = @"Sent by Umang App!";
        // To address
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:receipt];
        
        picker.mailComposeDelegate = self;
        [picker setSubject:emailTitle];
        [picker setMessageBody:messageBody isHTML:YES];
        [picker setToRecipients:toRecipents];
        
        // Present mail view controller on screen
        [self presentViewController:picker animated:YES completion:NULL];
    }
    
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (void)openContactList
{
    // Create the sheet with only cancel button
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:NSLocalizedString(@"call", nil)
                                  delegate:self
                                  cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:nil];
    
    // Add buttons one by one (e.g. in a loop from array etc...)
    for (int i=0; i<[self.contactitems count]; i++)
    {
        NSString *contact=[NSString stringWithFormat:@"%@",[self.contactitems objectAtIndex:i]];
        [actionSheet addButtonWithTitle:contact];
        
    }
    
    
    [actionSheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    if  (actionSheet.cancelButtonIndex == buttonIndex)
    {
        return;
    }
    else
    {
        if(buttonIndex>0)
        {
            buttonIndex=buttonIndex-1;
        }
        NSString*  callNumber=[NSString stringWithFormat:@"%@",[self.contactitems objectAtIndex:buttonIndex]];
        callNumber = [callNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *mobileNumber=[NSString stringWithFormat:@"telprompt://%@",callNumber];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mobileNumber]];
    }
}



-(void)callAction
{
    
    NSString *contacts=[dic_serviceInfo valueForKey:@"SERVICE_PHONE_NUMBER"];
    
    if (contacts.length>0)
    {
        NSString *contacts=[dic_serviceInfo valueForKey:@"SERVICE_PHONE_NUMBER"];
        self.contactitems = [contacts componentsSeparatedByString:@","];
        NSLog(@"contactitems to be shown in pop up =%@",self.contactitems);
        [self openContactList];
        
        // [self displayContentController:[self getserviceCallContacts]];
    }
    else
    {
        //do nothing
        contactCell.btnCall.hidden = YES;
        
        
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:NSLocalizedString(@"phone_support", nil)                         message:NSLocalizedString(@"no_result_found", nil)
                              delegate:self
                              cancelButtonTitle:nil
                              otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
        [alert show];
    }
}


-(void)viewOnMap
{
    NSString *latitude= [dic_serviceInfo valueForKey:@"SERVICE_LATITUDE"];
    
    
    NSString *longitute=  [dic_serviceInfo valueForKey:@"SERVICE_LONGITUDE"];
    
    
    // NSString* googleMapsURLString = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@,%@",latitude, longitute];
    
    // [[UIApplication sharedApplication] openURL: [NSURL URLWithString: googleMapsURLString]];
    
    NSString *deptName=[NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_NAME"]];
    deptName = [deptName stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    
    
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://"]])
        
    {
        NSLog(@"Map App Found");
        
        NSString* googleMapsURLString = [NSString stringWithFormat:@"comgooglemaps://?q=%.6f,%.6f(%@)&center=%.6f,%.6f&zoom=15&views=traffic",[latitude doubleValue], [longitute doubleValue],deptName,[latitude doubleValue], [longitute doubleValue]];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsURLString]];
        
        
    } else
    {
        NSString* googleMapsURLString = [NSString stringWithFormat:@"https://maps.google.com/maps?&z=15&q=%.6f+%.6f&ll=%.6f+%.6f",[latitude doubleValue], [longitute doubleValue], [latitude doubleValue], [longitute doubleValue]];
        
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:googleMapsURLString]];
        
    }
    
    
}
//deepak change below function

-(void)btnViewDetailsClicked :(NSIndexPath*)indexPath
{
    if ( [mainCell.btnViewDetails.titleLabel.text isEqualToString:NSLocalizedString(@"view_ratings", nil)])
    {
        [mainCell.btnViewDetails setTitle:NSLocalizedString(@"hide_ratings", nil) forState:UIControlStateNormal];
        
    }
    else
    {
        [mainCell.btnViewDetails setTitle:NSLocalizedString(@"view_ratings", nil) forState:UIControlStateNormal];
        
    }
    mainCell.btnViewDetails.backgroundColor = [UIColor clearColor];
    
    
    if (isRatingShown==YES) {
        isRatingCellShown = YES;
        //[mainCell.btnViewDetails setTitle:NSLocalizedString(@"hide_ratings", nil) forState:UIControlStateSelected];
        // mainCell.btnViewDetails.backgroundColor = [UIColor redColor];
        
        framevwRatebtn =CGRectMake(fDeviceWidth/2-100, 340, 200, 30);
        
        mainCell.btnViewDetails.frame=framevwRatebtn;
        
        isRatingShown=NO;
    }
    else
    {
        
        // [mainCell.btnViewDetails setTitle:@"view_ratings" forState:UIControlStateNormal];
        
        framevwRatebtn =CGRectMake(fDeviceWidth/2-100, 195, 200, 30);
        
        
        mainCell.btnViewDetails.frame=framevwRatebtn;
        
        //  mainCell.btnViewDetails.backgroundColor = [UIColor greenColor];
        isRatingCellShown = NO;
        
        isRatingShown=YES;
        
    }
    
    [tblDetail reloadData];
    /*
     indexPath = [NSIndexPath indexPathForRow:0 inSection:1] ;
     [tblDetail beginUpdates];
     [tblDetail insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
     */
    //[self addRatingGraphView];
    
    
}





-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}

-(UIView *)addRatingGraphView
{
    if ([rt1 length]==0) {
        rt1=@"0";
    }
    if ([rt2 length]==0) {
        rt2=@"0";
        
    }
    if ([rt3 length]==0) {
        rt3=@"0";
        
    }
    if ([rt4 length]==0) {
        rt4=@"0";
        
    }
    if ([rt5 length]==0) {
        rt5=@"0";
        
    }
    int rate1=[rt1 intValue];
    int rate2=[rt2 intValue];
    int rate3=[rt3 intValue];
    int rate4=[rt4 intValue];
    int rate5=[rt5 intValue];
    
    NSNumber *myNum1 = [NSNumber numberWithInt:rate1];
    NSNumber *myNum2 = [NSNumber numberWithInt:rate2];
    NSNumber *myNum3 = [NSNumber numberWithInt:rate3];
    NSNumber *myNum4 = [NSNumber numberWithInt:rate4];
    NSNumber *myNum5 = [NSNumber numberWithInt:rate5];
    //NSArray *valuesTopass = @[myNum1, myNum2,myNum3, myNum4, myNum5];
    NSArray *valuesTopass = @[myNum5, myNum4,myNum3, myNum2, myNum1];
    
    //deepak change
    
    //vwRating = [[RatingGraphView alloc] initWithFrame:CGRectMake(10,framevwRatebtn.origin.y + 30, CGRectGetWidth(self.view.frame) - 20, 160) withXValues:valuesTopass];
    NSString*ratingDept= [NSString stringWithFormat:@"%@",[dic_serviceInfo valueForKey:@"SERVICE_RATING"]];
    
    NSString*totalDownloadStr= [NSString stringWithFormat:@"%d",totalDownload];
    
    @try {
        totalDownload=rate1+rate2+rate3+rate4+rate5;
        
        NSLog(@"totalDownload=%d",totalDownload);
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
    // vwRating = [[RatingGraphView alloc] initWithFrame:CGRectMake(10,170 + 30, CGRectGetWidth(self.view.frame) - 20, 160) withXValues:valuesTopass withRating:ratingDept withTotalDownload:totalDownloadStr];
    
    
    vwRating = [[RatingGraphView alloc] initWithFrame:CGRectMake(10,142 + 45, CGRectGetWidth(self.view.frame) - 20, 160) withXValues:valuesTopass withRating:ratingDept withTotalDownload:totalDownloadStr];
    
    //vwRating = [[RatingGraphView alloc] initWithFrame:CGRectMake(10,170 + 30, CGRectGetWidth(self.view.frame) - 20, 160) withXValues:valuesTopass withRating:ratingDept withTotalDownload:totalDownloadStr];
    
    vwRating.tag=1099;
    //[self.view addSubview:vwRating];
    return vwRating;
}


-(IBAction)backbtnAction:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
