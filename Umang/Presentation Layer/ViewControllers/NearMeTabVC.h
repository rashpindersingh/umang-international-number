//
//  AllServicesTabVC.h
//  Umang
//
//  Created by spice on 15/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdvertisingColumn.h"
#import "HeaderCollectionReusableView.h"
#import "HeaderStateBannerCollReusableView.h"

@interface NearMeTabVC : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate>

{
    
    IBOutlet UITextField *txt_searchField;
    
    //NSMutableArray *table_data;
    
    IBOutlet UIView *vw_line;
    
    UIRefreshControl *refreshController;
    
    
    IBOutlet UIView *vw_banner;
   // IBOutlet UIButton *btn_location;
    IBOutlet UIImageView *img_location;
    IBOutlet UILabel *lbl_location;
    
    IBOutlet UIView *vw_noServiceFound;
    IBOutlet UILabel *lbl_noservicefound;
    __weak IBOutlet UILabel *clickHereLabel;
    AdvertisingColumn *_headerView;
    
    IBOutlet UIImageView *searchIconImage;
    
}


@property(nonatomic,retain)NSString *str_city;
@property(nonatomic,retain)NSString *str_state;

@property(nonatomic, retain) IBOutlet UICollectionView *allSer_collectionView;


-(IBAction)btn_locationAction:(id)sender;
-(IBAction)btn_filterAction:(id)sender;
-(IBAction)btn_noticationAction:(id)sender;




@end
