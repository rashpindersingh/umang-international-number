//
//  DigiLockerWebVC.h
//  Umang
//
//  Created by deepak singh rawat on 21/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DigiLockerWebVC : UIViewController<UIWebViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate>
{
    //IBOutlet UIWebView *webview;
    IBOutlet UILabel *lbltitle;
}
@property(nonatomic,retain)NSString *urltoOpen;
@property(nonatomic,retain)NSString *titleOpen;
@property (strong, nonatomic)IBOutlet UIWebView *webView;

@end
