//
//  LanguageSelectVC.m
//  Umang
//
//  Created by admin on 16/09/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "LanguageSelectVC.h"
#import "LanguageTableCell.h"
#import "LanguageBO.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "CZPickerView.h"
#import "NotificationFilterVC.h"
#import "NSBundle+Language.h"
#import "Constant.h"
#import "UmangEulaVC.h"
#import "UIView+Toast.h"

@interface LanguageSelectVC ()<CZPickerViewDelegate,CZPickerViewDataSource>
{
    LanguageTableCell *languageCell;
    NSMutableArray *arrLanguageNames;
    LanguageBO *selectedLanguage;
    int selectedIndex;
    NSString *selectLanguageType;
    NSString *preferedLocale;
    
    
}
@end

@implementation LanguageSelectVC
@synthesize languageScreenShown;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:LANGUAGE_SELECTION_SCREEN];
    
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    
    languageScreenShown = true;
    [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"languageScreenShown"];
    
    self.isLanguageScreen = YES;
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isAppSuccessfullyLaunched"]){
        // else do your task
        NSLog(@"Popup already shown");
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isAppSuccessfullyLaunched"];
        // Show Popup here
        
      /*
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

        UmangEulaVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"UmangEulaVC"];
        [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:vc animated:NO completion:nil];
        */
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
        [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:vc animated:NO completion:nil];
        
    }
    
    
    
    [_btndropDown addTarget:self action:@selector(btnDropDownClicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self prepareTempDataForLanguageType];
    
    
    // Update Key for Language Screen
    [[NSUserDefaults standardUserDefaults] setInteger:kLanguageScreenCase forKey:kInitiateScreenKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


-(void)btnDropDownClicked
{
    [self showLanguageOptions];
    
}

#pragma mark- Language Picker

- (void)czpickerView:(CZPickerView *)pickerView
didConfirmWithItemAtRow:(NSInteger)row
{
    if (selectedLanguage != nil)
    {
        selectedLanguage.isLanguageSelected = NO;
    }
    [[SharedManager sharedSingleton] traceEvents:@"Language Selected " withAction:@"Clicked" withLabel:@"Language Select" andValue:0];

    selectedLanguage = arrLanguageNames [row];
    
    [_btndropDown setTitle:selectedLanguage.languageName forState:UIControlStateNormal];
    selectedLanguage.isLanguageSelected = YES;
    _btndropDown.titleLabel.font = [UIFont systemFontOfSize:17.0];
    
    
    // Update Langugae Bundle
    [[NSUserDefaults standardUserDefaults] setObject:selectedLanguage.localeName forKey:KEY_PREFERED_LOCALE];
    
    // code for selection of language with back press fix
    if (selectedLanguage.languageName!=nil) {
           [[NSUserDefaults standardUserDefaults] setObject:selectedLanguage.languageName forKey:@"KEY_PREFERED_LANGUAGENAME"];
    }
    // code for selection of language with back press fix

    [NSBundle setLanguage:selectedLanguage.localeName];
    
    [self checkLanguageSelected];
    
    
    self.lblSelectLanguage.text = NSLocalizedString(@"choose_your_language", nil);
}
-(void)showToast :(NSString *)toast
{
    [self.view makeToast:toast duration:15.0 position:CSToastPositionBottom];
}
-(void)checkLanguageSelected
{
    SharedManager*  singleton=[SharedManager sharedSingleton];
    
    singleton.languageSelected=selectedLanguage.localeName;
    
    
    
    // Check existing language if set
    NSString *existingLanguageSaved = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    NSLog(@"existingLanguageSaved====>%@",existingLanguageSaved);
    if (existingLanguageSaved == nil) { // There is no language selected. Set English by default
        [NSBundle setLanguage:TXT_LANGUAGE_ENGLISH];
        [[NSUserDefaults standardUserDefaults] setObject:TXT_LANGUAGE_ENGLISH forKey:KEY_PREFERED_LOCALE];
        singleton.languageSelected=NSLocalizedString(@"english", nil);
        
    }
    else // Set already saved language
    {
        [NSBundle setLanguage:existingLanguageSaved];
        if ([existingLanguageSaved containsString:@"en"]) {
            
            singleton.languageSelected= NSLocalizedString(@"english", nil);
            
        }
        
        if ([existingLanguageSaved containsString:@"hi"])
        {
            
            singleton.languageSelected=NSLocalizedString(@"hindi", nil);
            
        }
        
        if ([existingLanguageSaved containsString:@"as"])
        {
            
            singleton.languageSelected=NSLocalizedString(@"assamese", nil);
            
        }
        
        if ([existingLanguageSaved containsString:@"bn"]) {
            
            singleton.languageSelected=NSLocalizedString(@"bengali", nil);
            
        }
        if ([existingLanguageSaved containsString:@"gu"]) {
            
            singleton.languageSelected=NSLocalizedString(@"gujarati", nil);
            
        }
        if ([existingLanguageSaved containsString:@"kn"]) {
            
            singleton.languageSelected=NSLocalizedString(@"kannada", nil);
            
        }
        if ([existingLanguageSaved containsString:@"ml"]) {
            
            singleton.languageSelected=NSLocalizedString(@"malayalam", nil);
            
        }
        
        if ([existingLanguageSaved containsString:@"mr"]) {
            
            singleton.languageSelected=NSLocalizedString(@"marathi", nil);
            
        }
        
        if ([existingLanguageSaved containsString:@"or"]) {
            
            singleton.languageSelected=NSLocalizedString(@"oriya", nil);
            
        }
        
        
        
        if ([existingLanguageSaved containsString:@"pa"]) {
            
            singleton.languageSelected=NSLocalizedString(@"punjabi", nil);
            
        }
        
        
        if ([existingLanguageSaved containsString:@"ta"]) {
            
            singleton.languageSelected=NSLocalizedString(@"tamil", nil);
            
        }
        
        if ([existingLanguageSaved containsString:@"te"]) {
            
            singleton.languageSelected=NSLocalizedString(@"telugu", nil);
            
        }
        
        
        if ([existingLanguageSaved containsString:@"ur"]) {
            
            singleton.languageSelected=NSLocalizedString(@"urdu", nil);
            
        }
        
        
        singleton.tabSelected=NSLocalizedString(@"home_small", nil);
        
    }
    
}

-(void)showLanguageOptions
{
    [[SharedManager sharedSingleton] traceEvents:@"Change Language Button" withAction:@"Clicked" withLabel:@"Language Select" andValue:0];

    CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:NSLocalizedString(@"choose_language", nil) cancelButtonTitle:nil confirmButtonTitle:NSLocalizedString(@"Cancel", nil)];
    picker.delegate = self;
    picker.dataSource = self;
    picker.allowMultipleSelection = NO;
    picker.allowRadioButtons = NO;
    picker.needFooterView = NO;
    picker.headerTitleColor = [UIColor colorWithRed:12.0/255.0 green:97.0/255.0 blue:161.0/255.0 alpha:1.0];
    picker.isClearOptionRequired = NO;
    picker.isLanguageScreenSelected = YES;
    [picker show];
    
}

- (NSAttributedString *)czpickerView:(CZPickerView *)pickerView
               attributedTitleForRow:(NSInteger)row
{
    NSAttributedString *att;
    
    LanguageBO *objService = arrLanguageNames [row];
    att = [[NSAttributedString alloc]
           initWithString:NSLocalizedString(objService.languageName, nil)
           attributes:@{
                        NSFontAttributeName:[UIFont fontWithName:@"Avenir-Light" size:18.0]
                        }];
    
    return att;
}

- (NSString *)czpickerView:(CZPickerView *)pickerView
               titleForRow:(NSInteger)row
{
    
    return arrLanguageNames[row];
    
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView
{
    return arrLanguageNames.count;
}






- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView
{
    [[SharedManager sharedSingleton] traceEvents:@"Cancel Language Select Button" withAction:@"Clicked" withLabel:@"Language Select" andValue:0];

    NSLog(@"Canceled.");
}



-(void)viewWillAppear:(BOOL)animated{
    
    // Check existing language if set
    NSString *existingLanguageSaved = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    NSLog(@"========> language selected in language=%@",existingLanguageSaved);

    //[self showToast:existingLanguageSaved];
    //——————— Add to handle portrait mode only———
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.shouldRotate = NO;
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——
    
    
    // Now update the top header text
    self.lblSelectLanguage.text =NSLocalizedString(@"choose_your_language", nil);
    
   // code for selection of language with back press fix
    NSString *selectedLanguageName = [[NSUserDefaults standardUserDefaults] objectForKey:@"KEY_PREFERED_LANGUAGENAME"];
    if (selectedLanguageName != nil)
    {
     [_btndropDown setTitle:selectedLanguageName forState:UIControlStateNormal];
    _btndropDown.titleLabel.font = [UIFont systemFontOfSize:17.0];
        
    }
    // code for selection of language with back press fix

   
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [super viewWillAppear:animated];
    
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

-(void)prepareTempDataForLanguageType
{
    
    arrLanguageNames = [NSMutableArray new];
    
    LanguageBO *objService = [[LanguageBO alloc] init];
    objService.languageName = @"English";
    objService.localeName = @"en";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    objService = [[LanguageBO alloc] init];
    objService.languageName = @"हिंदी";
    objService.localeName = @"hi-IN";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    objService = [[LanguageBO alloc] init];
    objService.languageName = @"অসমীয়া";
    objService.localeName = @"as-IN";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    
    objService = [[LanguageBO alloc] init];
    objService.languageName = @"বাংলা";
    objService.localeName = @"bn-IN";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    objService = [[LanguageBO alloc] init];
    objService.languageName = @"ગુજરાતી";
    objService.localeName = @"gu-IN";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    objService = [[LanguageBO alloc] init];
    objService.languageName = @"ಕನ್ನಡ";
    objService.localeName = @"kn-IN";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    objService = [[LanguageBO alloc] init];
    objService.languageName = @"മലയാളം";
    objService.localeName = @"ml-IN";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    
    objService = [[LanguageBO alloc] init];
    objService.languageName = @"मराठी";
    objService.localeName = @"mr-IN";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    objService = [[LanguageBO alloc] init];
    objService.languageName = @"ଓଡ଼ିଆ";
    objService.localeName = @"or-IN";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    
    objService = [[LanguageBO alloc] init];
    objService.languageName = @"ਪੰਜਾਬੀ";
    objService.localeName = @"pa-IN";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    
    
    objService = [[LanguageBO alloc] init];
    objService.languageName = @"தமிழ்";
    objService.localeName = @"ta-IN";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    
    objService = [[LanguageBO alloc] init];
    objService.languageName = @"తెలుగు";
    objService.localeName = @"te-IN";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    objService = [[LanguageBO alloc] init];
    objService.languageName = @"اردو";
    objService.localeName = @"ur-IN";
    [arrLanguageNames addObject:objService];
    objService = nil;
    
    
    
}



-(void)btnLanguageClicked:(UIButton*)btnSender
{
    [[SharedManager sharedSingleton] traceEvents:@"Change Language Button" withAction:@"Clicked" withLabel:@"Language Select" andValue:0];

    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:btnSender.tag-500 inSection:0];
    [self checkUncheckLanguageOptions:indexPath];
}

-(void)checkUncheckLanguageOptions:(NSIndexPath*)indexPath
{
    // Uncheck previous selected row
    if (selectedLanguage != nil) {
        selectedLanguage.isLanguageSelected = NO;
    }
    
    
    selectedLanguage = [arrLanguageNames objectAtIndex:indexPath.row];
    selectedLanguage.isLanguageSelected = YES;
    
    // Update Langugae Bundle
    [[NSUserDefaults standardUserDefaults] setObject:selectedLanguage.localeName forKey:KEY_PREFERED_LOCALE];
    [NSBundle setLanguage:selectedLanguage.localeName];
    
    SharedManager *singleton = [SharedManager sharedSingleton];
    NSLog(@"electedLanguage.localeName=%@",selectedLanguage.localeName);
    if ([selectedLanguage.localeName isEqualToString:@"ur-IN"])
    {
        singleton.isArabicSelected = YES;
    }
    else
    {
        singleton.isArabicSelected = NO;
    }
    
    // Now update the top header text
    self.lblSelectLanguage.text = NSLocalizedString(@"choose_your_language", nil);
    self.lblSelectLanguage.adjustsFontSizeToFitWidth = YES;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)btnNextClicked:(UIButton*)sender
{
    
    /* ViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
     [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
     [self presentViewController:vc animated:NO completion:nil];
     //jump to next step here
     */
    //    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag-500 inSection:0];
    //
    //    [self checkUncheckLanguageOptions:indexPath];
    
    // Update Langugae Bundle
      [[SharedManager sharedSingleton] traceEvents:@"Next Button" withAction:@"Clicked" withLabel:@"Language Select" andValue:0];
    
    NSString *existingLanguageSaved = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_PREFERED_LOCALE];
    if(([selectedLanguage.localeName length]==0 ||selectedLanguage.localeName==nil ) && existingLanguageSaved.length!=0)
    {
        
        
        NSArray *  checkArray = [arrLanguageNames filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"localeName CONTAINS[c] %@", existingLanguageSaved]];
        NSLog(@"========> checkArray=%@",checkArray);
        if ([checkArray count]>0)
        {
            NSInteger languageIndex=[arrLanguageNames indexOfObject:[checkArray objectAtIndex:0]];
            selectedLanguage = arrLanguageNames [languageIndex];
            NSLog(@"========> selectedLanguage.localeName selected in tour=%@ %d",selectedLanguage.localeName,languageIndex);

            
        }
       
        
    }
   
  [[NSUserDefaults standardUserDefaults] setObject:selectedLanguage.localeName forKey:KEY_PREFERED_LOCALE];
    NSLog(@"========> selectedLanguage.localeName selected in tour=%@",selectedLanguage.localeName);

    [NSBundle setLanguage:selectedLanguage.localeName];
    
    SharedManager *singleton = [SharedManager sharedSingleton];
    
    if ([selectedLanguage.localeName isEqualToString:@"ur-IN"])
    {
        singleton.isArabicSelected = YES;
    }
    else
    {
        singleton.isArabicSelected = NO;
    }
    
    
    /*
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    UmangEulaVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"UmangEulaVC"];
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:vc animated:NO completion:nil];
    */
    //added as it is in next button action in eula screen
    [[NSUserDefaults standardUserDefaults] setInteger:kTutorialScreenCase forKey:kInitiateScreenKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [vc setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:vc animated:NO completion:nil];
    
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
 - (NSUInteger)supportedInterfaceOrientations
 #else
 - (UIInterfaceOrientationMask)supportedInterfaceOrientations
 #endif
 {
 // Return a bitmask of supported orientations. If you need more,
 // use bitwise or (see the commented return).
 return UIInterfaceOrientationMaskPortrait;
 // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
 }
 
 - (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
 // Return the orientation you'd prefer - this is what it launches to. The
 // user can still rotate. You don't have to implement this method, in which
 // case it launches in the current orientation
 return UIInterfaceOrientationPortrait;
 }*/


@end
