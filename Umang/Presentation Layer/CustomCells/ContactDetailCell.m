//
//  ContactDetailCell.m
//  Umang
//
//  Created by admin on 29/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "ContactDetailCell.h"

@implementation ContactDetailCell


@synthesize btnCall,btn_email,btn_visitWebsite;


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
    self.btnCall.layer.borderWidth = 1.0;
    //    self.btnCall.layer.borderColor = (__bridge CGColorRef _Nullable)([UIColor colorWithRed:42.0/255.0 green:154.0/255.0 blue:177.0/255.0 alpha:1.0]);
    
    self.btnCall.layer.cornerRadius = 5.0;
    self.btnCall.layer.borderColor= [UIColor colorWithRed:56.0/255.0 green:182.0/255.0 blue:105.0/255.0 alpha:1.0].CGColor;
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)showEmailOption:(BOOL)needToShow{
    
    if (needToShow) {
        self.btn_email.hidden = NO;
        self.imgViewMail.hidden = NO;
    }
    else{
        self.btn_email.hidden = YES;
        self.imgViewMail.hidden = YES;
    }
}

-(void)showVisitWebsiteOption:(BOOL)needToShow{
    
    if (needToShow) {
        self.btn_visitWebsite.hidden = NO;
        self.imgWebsite.hidden = NO;
    }
    else{
        self.btn_visitWebsite.hidden = YES;
        self.imgWebsite.hidden = YES;
    }
}



@end
