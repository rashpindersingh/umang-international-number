//
//  AadharDetailInfoCell.h
//  Umang
//
//  Created by admin on 09/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AadharDetailInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleValue;

@end
