//
//  AllServiceCell.m
//  Umang
//
//  Created by admin on 19/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "AllServiceCell.h"
#import "NotificationItemBO.h"

@implementation AllServiceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lblServiceTitle.font = [AppFont regularFont:14];

    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
