//
//  AadharDetailProfileCell.h
//  Umang
//
//  Created by admin on 09/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AadharDetailProfileCell : UITableViewCell
{
    SharedManager *singleton;
}
typedef enum
{
    
    AADHARWITHPIC= 10,
    AADHARWITHOUTPIC,
    NOTLINKED,
    
    
}TYPE_AADHAR;

@property (weak, nonatomic) IBOutlet UIImageView *imgAadhar;
@property (weak, nonatomic) IBOutlet UILabel *lblAadharNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblaadharDigit;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *lblAadharInformation;
@property (assign,nonatomic )  TYPE_AADHAR TYPE_AADHAR_CHOOSEN;

//@property (nonatomic,assign) BOOL isAadhardetailWithoutPic;
//@property (nonatomic,assign) BOOL isNotLinkedAadharCon;



-(void)bindDataForHeaderView;

@end
