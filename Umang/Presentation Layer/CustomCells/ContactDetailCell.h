//
//  ContactDetailCell.h
//  Umang
//
//  Created by admin on 29/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnMap;
@property (weak, nonatomic) IBOutlet UIImageView *imageMap;
@property (weak, nonatomic) IBOutlet UIButton *btnCall;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewMail;
@property(weak,nonatomic)IBOutlet UIButton *btn_email;
@property (weak, nonatomic) IBOutlet UIImageView *imgWebsite;
@property(weak,nonatomic)IBOutlet UIButton *btn_visitWebsite;

-(void)showEmailOption:(BOOL)needToShow;
-(void)showVisitWebsiteOption:(BOOL)needToShow;


@end
