//
//  NotificationViewCell.m
//  Umang
//
//  Created by admin on 9/26/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "NotificationViewCell.h"

@implementation NotificationViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = [UIColor clearColor];
    //  self.panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panHandler:)];
    //    self.panGesture.delegate = self;
    //    [self addGestureRecognizer:self.panGesture];
    //
    //    [self.btnClearCell setImage:[UIImage imageNamed:@"icon_delete"] forState:UIControlStateNormal];
    //
    //    [self.btnClearCell addTarget:self action:@selector(deleteButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    self.vwDeleteBG.hidden  =YES;
}

-(void) panHandler: (UIPanGestureRecognizer *)recognizer
{
    
    if (recognizer != self.panGesture) {
        return;
    }
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        // when the gesture begins, record the current center location
        originalCenter = self.vwContentBG.center;
    }
    
    if (recognizer.state == UIGestureRecognizerStateChanged) {
        
        CGPoint translation = [recognizer translationInView:self];
        if (translation.x < 0) {
            self.vwContentBG.center = CGPointMake(originalCenter.x + translation.x, originalCenter.y);
            deleteOnDragRelease = self.vwContentBG.frame.origin.x < -self.frame.size.width / 2.0;
        }
    }
    
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        if (deleteOnDragRelease) {
            CGRect originalFrame = CGRectMake(-140, 0, self.frame.size.width, self.frame.size.height);
            [UIView animateWithDuration:0.2 animations:^{
                self.vwContentBG.frame = originalFrame;
            }];
            
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(pannedCell:)]) {
                [self.delegate performSelector:@selector(pannedCell:) withObject:self];
            }
        }
        else{
            [self resetFramesForPannedArea];
        }
    }
    
    /*
     if (recognizer.state == UIGestureRecognizerStateEnded) {
     CGRect originalFrame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
     if (deleteOnDragRelease) {
     if (self.delegate != nil && [self.delegate respondsToSelector:@selector(deleteNotificationItemFromTableRow:)]) {
     [self.delegate performSelector:@selector(deleteNotificationItemFromTableRow:) withObject:self.objCellItem];
     }
     }
     [UIView animateWithDuration:0.2 animations:^{
     self.vwContentBG.frame = originalFrame;
     }];
     }
     */
}


-(void)resetFramesForPannedArea{
    CGRect originalFrame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    [UIView animateWithDuration:0.2 animations:^{
        self.vwContentBG.frame = originalFrame;
    }];
}


-(void)deleteButtonClicked{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(deleteNotificationItemFromTableRow:)]) {
        [self.delegate performSelector:@selector(deleteNotificationItemFromTableRow:) withObject:self.objCellItem];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if ([otherGestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]]) {
        return NO;
    }
    
    return YES;
}

-(BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)gestureRecognizer {
    
    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        CGPoint translation = [gestureRecognizer translationInView:self];
        // Check for horizontal gesture
        if (fabs(translation.x) > fabs(translation.y)) {
            return YES;
        }
    }
    return NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
