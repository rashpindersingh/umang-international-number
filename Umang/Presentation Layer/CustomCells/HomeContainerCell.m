//
//  HomeContainerCell.m
//  Home
//
//  Created by deepak singh rawat on 26/09/16.
//  Copyright © 2016 deepak singh rawat. All rights reserved.
//

#import "HomeContainerCell.h"
#import "HomeContainerCellView.h"

@interface HomeContainerCell ()
@property (strong, nonatomic) HomeContainerCellView *collectionView;
@end


@implementation HomeContainerCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // Initialization code
        _collectionView = [[NSBundle mainBundle] loadNibNamed:@"HomeContainerCellView" owner:self options:nil][0];
        _collectionView.frame = self.bounds;
        // i added
        
        
        
       SharedManager *singleton=[SharedManager sharedSingleton];

        if (singleton.isArabicSelected==YES)
        {
            
            [ self.collectionView setTransform:CGAffineTransformMakeScale(-1, 1)];
            
        }

        [self.contentView addSubview:_collectionView];

    }
    return self;
}


//add below code of layer in  it
- (void)drawRect:(CGRect)rect
{
   //Comment for removing line at top and bottom of UITableview
   // [self addLayerAndShadow];
}

-(void)addLayerAndShadow
{
    
    self.layer.shadowOffset = CGSizeMake(0.2, 1.5);
    self.layer.shadowColor = [UIColor grayColor].CGColor;
    self.layer.shadowRadius = 2;
    self.layer.shadowOpacity = 0.5;
    self.layer.masksToBounds = NO;
    //add border
    UIView *borderView = [UIView new];
    borderView.backgroundColor = [UIColor whiteColor];
    borderView.frame = self.bounds;
    borderView.layer.cornerRadius = 5.0;
    borderView.clipsToBounds = true;
    // borderView.layer.borderWidth=0.1;
    //  borderView.layer.borderColor = [UIColor grayColor].CGColor;
    borderView.layer.masksToBounds = true;
    [self addSubview:borderView];
    [self sendSubviewToBack:borderView];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)setCollectionData:(NSArray *)collectionData {
    

    [_collectionView setCollectionData:collectionData];
}
@end

