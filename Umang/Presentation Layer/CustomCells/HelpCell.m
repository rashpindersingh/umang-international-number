//
//  HelpCell.m
//  Umang
//
//  Created by spice on 21/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import "HelpCell.h"

@implementation HelpCell

@synthesize lbl_celltitle,img_cell;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
