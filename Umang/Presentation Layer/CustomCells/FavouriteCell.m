//
//  FavouriteCell.m
//  Umang
//
//  Created by spice_digital on 28/09/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "FavouriteCell.h"
#import "SDCapsuleButton.h"
#import "StateList.h"
@implementation FavouriteCell

@synthesize lbl_serviceName;
@synthesize lbl_serviceDesc;
@synthesize lbl_serviceCateg;
@synthesize img_serviceCateg;
@synthesize btn_serviceFav;
//@synthesize starRatingView;
@synthesize lbl_rating;
@synthesize img_star;

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
    lbl_serviceCateg.hidden = YES;
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}
-(void)stateNameWithCategory:(NSDictionary*)dic ofCell:(FavouriteCell*)cell andTagIndex:(NSIndexPath *)indexpath{
    NSString *category = [dic valueForKey:@"CATEGORY_NAME"];
    if (category == nil ) {
        category = [dic valueForKey:@"SERVICE_CATEGORY"];
    }
    if (category == nil ) {
        category = @"";
    }
    NSMutableArray *arrCat = [[NSMutableArray alloc] init];
    /*NSArray *arr = [category componentsSeparatedByString:@","];
     for (NSString *cat in arr) {
     [arrCat addObject:cat];
     }*/
    [arrCat addObject:category];
    StateList *obj = [[StateList alloc]init];
    NSString *stateId = [dic valueForKey:@"STATE"];
    if (stateId == nil ) {
        stateId = [dic valueForKey:@"SERVICE_STATE"];
    }
    NSString *stateName = [obj getStateName:stateId];
    if (stateId.length == 0 || [stateId isEqualToString:@"99"])
    {
        stateName =  NSLocalizedString(@"central", @"");
    }
    NSMutableArray *arrState = [[NSMutableArray alloc] initWithObjects:stateName, nil];
    NSMutableArray *otherStatesNames = [[NSMutableArray alloc] init] ;
    if (stateId.length != 0 && ![stateId isEqualToString:@"99"])
    {
        NSString *otherStateId = [dic valueForKey:@"SERVICE_OTHER_STATE"];
        if (otherStateId != nil && otherStateId.length != 0) {
            
            //if else is added to parse value without | seprated
            if ([otherStateId containsString:@"|"])
            {
                NSString *newString = [otherStateId substringToIndex:[otherStateId length]-1];
                NSString *finalString = [newString substringFromIndex:1];
                if (finalString.length != 0)
                {
                    NSArray *otherStates = [finalString componentsSeparatedByString:@"|"];
                    NSLog(@"%@",otherStates);
                    for (NSString *stateIDS in otherStates) {
                        [otherStatesNames addObject: [obj getStateName:stateIDS]];
                    }
                }
            }
            else
            {
                // else is added to parse value  , comma seprated string of other states

                if (otherStateId.length != 0)
                {
                    NSArray *otherStates = [otherStateId componentsSeparatedByString:@","];
                    NSLog(@"%@",otherStates);
                    for (NSString *stateIDS in otherStates) {
                        [otherStatesNames addObject: [obj getStateName:stateIDS]];
                    }
                }
              //  [otherStatesNames addObject: [obj getStateName:otherStateId]];

           
                
            }
            
            NSLog(@"otherStatesNames======>%@",otherStatesNames);
        }
        
    }
    if ([otherStatesNames containsObject:stateName]) {
        [otherStatesNames removeObject:stateName];
    }
    if (otherStatesNames.count != 0) {
        [arrState addObjectsFromArray:otherStatesNames];
    }
    CGFloat fontSize = 14.0;
    [cell addCategoryStateItemsToScrollView:arrCat state:arrState withFont:fontSize cell:cell andTagIndex:indexpath];
}
-(void)addCategoryStateItemsToScrollView:(NSArray*)arrCat state:(NSArray*)arrStates withFont:(CGFloat)fontSize cell:(FavouriteCell*)cell andTagIndex:(NSIndexPath *)indexpath
{
    
    if (_scrollView_nouse != nil)
    {
        [_scrollView_nouse removeFromSuperview];
        _scrollView_nouse = nil;
        //return;
    }

    CGFloat scrollWidth = fDeviceWidth - 150;
    
    if (cell.btn_moreInfo.hidden) {
        scrollWidth = fDeviceWidth - 75;
    }
    
    _scrollView_nouse = [[UIScrollView alloc]
                         initWithFrame:CGRectMake(CGRectGetMaxX(lbl_rating.frame) + 8, CGRectGetMinY(lbl_serviceCateg.frame), scrollWidth, 30)];
    
    SharedManager *singlton = [SharedManager sharedSingleton];
    if (singlton.isArabicSelected)
    {
        _scrollView_nouse = nil;
        scrollWidth = fDeviceWidth - 170;
         CGFloat xValue = CGRectGetMaxX(cell.btn_moreInfo.frame) + 4;
        if (cell.btn_moreInfo.hidden) {
            scrollWidth = fDeviceWidth - 200;
            xValue = CGRectGetMinX(cell.frame) + 8;
        }
        _scrollView_nouse = [[UIScrollView alloc]
                             initWithFrame:CGRectMake(xValue, CGRectGetMinY(lbl_serviceCateg.frame), scrollWidth, 30)];
        //self.semanticContentAttribute = UISemanticContentAttributeForceRightToLeft;
    }
    
    [self addSubview:_scrollView_nouse];
    _scrollView_nouse.showsHorizontalScrollIndicator = false;
    _scrollView_nouse.showsHorizontalScrollIndicator = false;
    
    cell.lbl_serviceCateg.hidden = YES;
    CGFloat xCord = singlton.isArabicSelected ? scrollWidth : 4;
    CGFloat yCord = 2;
    CGFloat padding = 5;
    CGFloat itemHeight = 25;
    CGFloat arabicContentSize = 4;
    for (int i = 0; i < arrCat.count; i++)
    {
        NSString *stateName = arrCat[i ];
        if (stateName.length == 0) {
            continue;
        }
        CGRect frame = [self rectForText:stateName usingFont:[AppFont regularFont:fontSize] boundedBySize:CGSizeMake(1000.0, itemHeight)];
        frame.size.width = frame.size.width + 30;
        UILabel *lbl = [self labelWithBorderColor:[UIColor colorWithRed:228/255.0 green:100.0/255.0 blue:52.0/255.0 alpha:1.0]];//[[SDCapsuleButton alloc] initWithFrameWithFullBorder:CGRectMake(xCord, yCord, frame.size.width, itemHeight)];
        
        lbl.font = [AppFont regularFont:fontSize];
        lbl.frame = CGRectMake(xCord, yCord, frame.size.width, itemHeight);

        if (singlton.isArabicSelected) {
            xCord -= (frame.size.width + padding);
            lbl.frame = CGRectMake(xCord, yCord, frame.size.width, itemHeight);
            arabicContentSize += frame.size.width + padding;
        } else {
            xCord+=frame.size.width + padding;
        }
        lbl.text = stateName;
        lbl.userInteractionEnabled = YES;
        lbl.tag = indexpath.row;
        [_scrollView_nouse addSubview:lbl];
    }
    // *** custom logic for user state selected ***
    StateList *obj = [[StateList alloc]init];
    singlton.user_StateId = [singlton getStateId];
    NSString *stateTab = @"";
    NSString *userState = @"";
    if (singlton.profilestateSelected.length != 0)
    {
        userState =singlton.profilestateSelected; //[obj getStateName:singlton.profilestateSelected];
    }
    if (singlton.user_StateId.length != 0)
    {
        stateTab = [obj getStateName:singlton.user_StateId];
    }
    NSMutableArray *arrFinal = [[NSMutableArray alloc] init];
    NSMutableArray *arrStatesAll = [[NSMutableArray alloc] initWithArray:arrStates];
    if (userState.length != 0 || stateTab.length != 0) {
        if ([arrStatesAll containsObject:userState]) {
            [arrFinal insertObject:userState atIndex:0];
            [arrStatesAll removeObject:userState];
        }
        else if ([arrStatesAll containsObject:stateTab]) {
            [arrFinal addObject:stateTab];
            [arrStatesAll removeObject:stateTab];
        }
    }
    NSArray *sortedArrayOfString = [arrStatesAll sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [(NSString *)obj1 compare:(NSString *)obj2 options:NSNumericSearch];
    }];
    [arrFinal addObjectsFromArray:sortedArrayOfString];
    
    for (int i = 0; i < arrFinal.count; i++)
    {
        
        NSString *stateName = arrFinal[i ];
        if (stateName.length == 0) {
            continue;
        }
        CGRect frame = [self rectForText:stateName usingFont:[AppFont regularFont:fontSize] boundedBySize:CGSizeMake(1000.0, itemHeight)];
        frame.size.width = frame.size.width + 30;
        UILabel *lbl = [self labelWithBorderColor:[UIColor colorWithRed:86.0/255.0 green:190.0/255.0 blue:113.0/255.0 alpha:1.0]];//[[SDCapsuleButton alloc] initWithFrameWithFullBorder:CGRectMake(xCord, yCord, frame.size.width, itemHeight)];
        lbl.font = [AppFont regularFont:fontSize];
        lbl.frame = CGRectMake(xCord, yCord, frame.size.width, itemHeight);
        if (singlton.isArabicSelected) {
            xCord -= (frame.size.width + padding);
            lbl.frame = CGRectMake(xCord, yCord, frame.size.width, itemHeight);
            arabicContentSize += frame.size.width + padding;
        } else {
            xCord+=frame.size.width + padding;
        }
        lbl.text = stateName;
        lbl.userInteractionEnabled = YES;
        lbl.tag = indexpath.row;
        [_scrollView_nouse addSubview:lbl];
    }
    xCord = singlton.isArabicSelected ? arabicContentSize : xCord;
    [_scrollView_nouse setContentSize:CGSizeMake(xCord, _scrollView_nouse.frame.size.height)];
    if (singlton.isArabicSelected) {
         [_scrollView_nouse setContentInset:UIEdgeInsetsMake(0, arabicContentSize/2, 0,0)];
    }
    
    [cell bringSubviewToFront:_scrollView_nouse];
}

-(UILabel*)labelWithBorderColor:(UIColor*)borderColor {
    UILabel *lbl = [[UILabel alloc] init];
    lbl.textColor = borderColor;
    lbl.layer.cornerRadius = 12.5;
    lbl.layer.masksToBounds = YES;
    lbl.clipsToBounds = YES;
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.layer.borderColor = [borderColor CGColor];
    lbl.layer.borderWidth = 1.0;
    return lbl;
}
@end

