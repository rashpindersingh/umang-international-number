//
//  NormalHeaderCell.h
//  Umang
//
//  Created by admin on 09/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NormalHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderTitle;

@end
