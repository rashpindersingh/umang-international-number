//
//  NotiSettingCell.h
//  Umang
//
//  Created by admin on 19/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotiSettingCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblNotification;
@property (weak, nonatomic) IBOutlet UISwitch *switchOnOff;

@end
