//
//  HeaderStateBannerCollReusableView.h
//  Umang
//
//  Created by admin on 11/10/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderStateBannerCollReusableView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UIButton *btn_Location;
@property (weak, nonatomic) IBOutlet UIView *vw_banner;

@end
