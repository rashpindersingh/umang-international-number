//
//  NotificationSearchCell.h
//  Umang
//
//  Created by admin on 18/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationSearchCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *searchImage;
@property (weak, nonatomic) IBOutlet UILabel *searchHeader;
@property (weak, nonatomic) IBOutlet UILabel *searchDescription;
@property (weak, nonatomic) IBOutlet UILabel *searchDate;
@property (weak, nonatomic) IBOutlet UIImageView *searchPDImg;

@end
