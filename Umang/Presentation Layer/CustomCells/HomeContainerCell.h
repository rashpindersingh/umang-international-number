//
//  HomeContainerCell.h
//  Home
//
//  Created by deepak singh rawat on 26/09/16.
//  Copyright © 2016 deepak singh rawat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeContainerCell : UITableViewCell
- (void)setCollectionData:(NSArray *)collectionData;

@end
