//
//  DescriptionCell.h
//  Umang
//
//  Created by admin on 29/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DescriptionCell : UITableViewCell
@property(nonatomic,retain)IBOutlet UILabel *lbl_desc;
@property (weak, nonatomic) IBOutlet UIButton *btnNeedMore;

@end
