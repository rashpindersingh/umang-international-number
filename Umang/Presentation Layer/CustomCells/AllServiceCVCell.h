//
//  AllServiceCVCell.h
//  AllServiceTabVC
//
//  Created by deepak singh rawat on 28/09/16.
//  Copyright © 2016 deepak singh rawat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"
#import "MyButton.h"

@interface AllServiceCVCell : UICollectionViewCell
@property (weak) IBOutlet UIImageView *serviceImage;
@property (weak) IBOutlet UILabel *serviceTitle;
@property (weak) IBOutlet MyFavButton *serviceFav;
@property (weak) IBOutlet MyButton *serviceInfo;
@property (nonatomic, strong)IBOutlet HCSStarRatingView *starRatingView;


@end
