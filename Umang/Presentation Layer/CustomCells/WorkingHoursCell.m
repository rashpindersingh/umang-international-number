//
//  WorkingHoursCell.m
//  Umang
//
//  Created by admin on 29/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "WorkingHoursCell.h"

@implementation WorkingHoursCell

@synthesize  btn_workinghours;
@synthesize lbl_workinghours;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    SharedManager *singlton = [SharedManager sharedSingleton];
    self.lblAddress.textAlignment = NSTextAlignmentLeft;
    self.lbl_workinghours.textAlignment = NSTextAlignmentLeft;

    self.addressOfService.textAlignment = NSTextAlignmentLeft;
    self.btn_workinghours.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;

    if (singlton.isArabicSelected) {
        self.lblAddress.textAlignment = NSTextAlignmentRight;
        self.lbl_workinghours.textAlignment = NSTextAlignmentRight;
        self.addressOfService.textAlignment = NSTextAlignmentRight;
        self.btn_workinghours.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
