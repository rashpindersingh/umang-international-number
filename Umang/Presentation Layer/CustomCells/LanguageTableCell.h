//
//  LanguageTableCell.h
//  Umang
//
//  Created by admin on 16/09/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LanguageTableCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIButton *btnLanguage;
@property (weak, nonatomic) IBOutlet UILabel *lblLanguageType;
@property (weak, nonatomic) IBOutlet UIView *vwLineBlue;

@end
