//
//  DescriptionCell.m
//  Umang
//
//  Created by admin on 29/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "DescriptionCell.h"

@implementation DescriptionCell
@synthesize lbl_desc,btnNeedMore;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    SharedManager *singlton = [SharedManager sharedSingleton];
    self.lbl_desc.textAlignment = NSTextAlignmentLeft;
    self.btnNeedMore.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    if (singlton.isArabicSelected) {
        self.lbl_desc.textAlignment = NSTextAlignmentRight;
        CGRect frameNeed = self.btnNeedMore.frame;
        frameNeed.origin.x = 10 ;
        self.btnNeedMore.translatesAutoresizingMaskIntoConstraints = true;
        self.btnNeedMore.frame = frameNeed;
        self.btnNeedMore.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [self layoutSubviews];
        [self updateConstraintsIfNeeded];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
