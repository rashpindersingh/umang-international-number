//
//  AllServiceCell.h
//  Umang
//
//  Created by admin on 19/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationItemBO.h"

@interface AllServiceCell : UITableViewCell


@property(nonatomic,retain)IBOutlet UIImageView *imgService;
@property(nonatomic,retain)IBOutlet UILabel *lblServiceTitle;

@property (weak, nonatomic) IBOutlet UISwitch *btnSwitch;

@end
