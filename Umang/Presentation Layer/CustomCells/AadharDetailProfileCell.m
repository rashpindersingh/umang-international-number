//
//  AadharDetailProfileCell.m
//  Umang
//
//  Created by admin on 09/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "AadharDetailProfileCell.h"

@implementation AadharDetailProfileCell
@synthesize TYPE_AADHAR_CHOOSEN;



- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
    
   
    self.imgUserProfilePic.layer.borderColor = [UIColor whiteColor].CGColor;
    self.imgUserProfilePic.layer.borderWidth = 2.0;
    
   singleton = [SharedManager sharedSingleton];
    
}



-(void)bindDataForHeaderView
{
    
    CGRect fullFrame = self.frame;
    CGFloat yPos = _imgAadhar.frame.origin.y +_imgAadhar.frame.size.height + 10;
    
    if(TYPE_AADHAR_CHOOSEN == AADHARWITHOUTPIC)
    {
        _lblAadharInformation.hidden = YES;
        _imgUserProfilePic.hidden = YES;
        _lblAadharNumber.frame = CGRectMake(30, yPos, self.frame.size.width - 60, 30);
        _lblaadharDigit.frame = CGRectMake(30, _lblAadharNumber.frame.origin.y +  50 ,self.frame.size.width - 60, 30);
        fullFrame.size.height = self.frame.size.height - 80;
        
        _lblAadharNumber.text = NSLocalizedString(@"your_aadhaar_number", nil);
       

        self.frame = fullFrame;
        
    }
    
    else if (TYPE_AADHAR_CHOOSEN == NOTLINKED)
    {
        _lblAadharInformation.hidden = YES;
        _imgUserProfilePic.hidden = YES;
        _lblaadharDigit.hidden = YES;
        _lblAadharNumber.frame = CGRectMake(30, yPos, self.frame.size.width - 60, 30);
        
        _lblAadharNumber.text = NSLocalizedString(@"aadhaar_num_not_linked", nil);
       // _lblAadharNumber.font=[UIFont systemFontOfSize:14.0];
        
        _lblAadharNumber.font =[UIFont systemFontOfSize:20 weight:UIFontWeightRegular];
        
        //        UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(_lblAadharNumber.frame.origin.x+30, yPos+4, 25, 25)];
//        [self addSubview:image];
//        image.image = [UIImage imageNamed:@"icon_not_linked_id"];
        
        fullFrame.size.height = self.frame.size.height - 110;

         self.frame = fullFrame;
        
    }
   
    else if (TYPE_AADHAR_CHOOSEN == AADHARWITHPIC)
    {
        _lblAadharInformation.text= NSLocalizedString(@"aadhaar_info", nil);
        _lblAadharNumber.text = NSLocalizedString(@"your_aadhaar_number", nil);
         _lblAadharInformation.textAlignment =  singleton.isArabicSelected ? NSTextAlignmentRight : NSTextAlignmentLeft;
        
        _lblaadharDigit.text = singleton.objUserProfile.objAadhar.aadhar_number;
       
        if (singleton.objUserProfile.objAadhar.aadhar_image_url) {
            // donwload image url
            
            dispatch_queue_t queue = dispatch_queue_create("com.spice.umang", NULL);
            dispatch_async(queue, ^{
                //code to be executed in the background
                NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:singleton.objUserProfile.objAadhar.aadhar_image_url]];
                UIImage *imageUser = [UIImage imageWithData:imageData];
                dispatch_async(dispatch_get_main_queue(), ^{
                    //code to be executed on the main thread when background task is finished
                    //update UI with new database inserted means reload uicollectionview
                    _imgUserProfilePic.image = imageUser;
                    
                });
                
            });

        }
        
    }
    //_lblAadharInformation.text= NSLocalizedString(@"aadhaar_info", nil);

    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
