//
//  ContactDetailServiceCell.h
//  Umang
//
//  Created by admin on 18/07/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceContactCollectionCell.h"



typedef void(^ContactServiceBlock)(NSString *contactType);

@interface ContactDetailServiceCell : UITableViewCell <UICollectionViewDelegate, UICollectionViewDataSource>
@property (strong, nonatomic) IBOutlet UICollectionView *serviceContactCollection;
@property(strong, nonatomic) NSMutableArray *dicContact;
@property(nonatomic)CGFloat widthAdded;
@property(copy) void(^ContactServiceBlock)(NSString *contactType);

-(void)setupDataSource:(NSMutableArray*) dataSource;
@end
