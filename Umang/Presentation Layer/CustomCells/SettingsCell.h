//
//  SettingsCell.h
//  Umang
//
//  Created by spice on 16/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface SettingsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_subtitle;
@property (weak, nonatomic) IBOutlet UIImageView *img_cell;
@property (weak, nonatomic) IBOutlet UISwitch *btn_switch;
@property (weak, nonatomic) IBOutlet UIImageView *img_cell_arrow;




@property (weak, nonatomic) IBOutlet UIImageView *imgSettings;

@property (weak, nonatomic) IBOutlet UILabel *lblReceiveSetting;

@property (weak, nonatomic) IBOutlet UILabel *lblAll;

@end
