//
//  FeedbackCell.h
//  Umang
//
//  Created by admin on 29/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgFeedback;
@property (weak, nonatomic) IBOutlet UILabel *lblFeedback;
@property (weak, nonatomic) IBOutlet UIButton *statusFeedback;

@end
