//
//  AllServiceCVCellForiPad.m
//  Umang
//
//  Created by admin on 12/04/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "AllServiceCVCellForiPad.h"
#import <QuartzCore/QuartzCore.h>

#import "ScrollNotificationVC.h"



@implementation AllServiceCVCellForiPad
@synthesize starRatingView;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.serviceTitle.font = [UIFont systemFontOfSize:18];

    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [self addLayerAndShadow];
    
}
//    // Drawing code
//    self.layer.borderColor = [[UIColor colorWithRed:178.0/255.0 green:178.0/255.0 blue:178.0/255.0 alpha:1.0] CGColor];
//    self.layer.borderWidth = 0.7;
//    self.layer.cornerRadius = 2;
//    self.layer.masksToBounds = true;
//


-(void)addLayerAndShadow
{
    
    self.layer.shadowOffset = CGSizeMake(0.2, 1.5);
    self.layer.shadowColor = [UIColor grayColor].CGColor;
    self.layer.shadowRadius = 2;
    self.layer.shadowOpacity = 0.5;
    self.layer.masksToBounds = NO;
    
    
    
    //add border
    
    UIView *borderView = [UIView new];
    borderView.backgroundColor = [UIColor whiteColor];
    borderView.frame = self.bounds;
    borderView.layer.cornerRadius = 5.0;
    borderView.clipsToBounds = true;
    // borderView.layer.borderWidth=0.1;
    //  borderView.layer.borderColor = [UIColor grayColor].CGColor;
    borderView.layer.masksToBounds = true;
    [self addSubview:borderView];
    [self sendSubviewToBack:borderView];
    
}


@end
