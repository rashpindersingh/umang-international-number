//
//  MyprofileCell.h
//  Umang
//
//  Created by deepak singh rawat on 18/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyprofileCell : UITableViewCell
@property(nonatomic,retain)IBOutlet UIImageView *img_Cell;
@property(nonatomic,retain)IBOutlet UILabel *lbl_Title;
@property(nonatomic,retain)IBOutlet UILabel *lbl_subTitle;
@property(nonatomic,retain)IBOutlet UIButton *btn_resend;
@property(nonatomic,retain)IBOutlet UIImageView *img_arrow;

@end
