//
//  HelpCell.h
//  Umang
//
//  Created by spice on 21/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpCell : UITableViewCell
@property(nonatomic,retain)IBOutlet UILabel *lbl_celltitle;
@property(nonatomic,retain)IBOutlet UIImageView *img_cell;

@end
