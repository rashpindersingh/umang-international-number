//
//  SettingsCell.m
//  Umang
//
//  Created by spice on 16/09/16.
//  Copyright (c) 2016 SpiceDigital. All rights reserved.
//

#import "SettingsCell.h"

@implementation SettingsCell
@synthesize lbl_title;
@synthesize lbl_subtitle;
@synthesize img_cell;
@synthesize btn_switch;
@synthesize img_cell_arrow;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
