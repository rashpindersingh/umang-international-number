//
//  NotificationTypeCell.h
//  Umang
//
//  Created by admin on 07/10/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationTypeCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblType;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckUncheck;

@property (weak, nonatomic) IBOutlet UIImageView *imgNotification;

@end
