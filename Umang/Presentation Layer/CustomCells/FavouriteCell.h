//
//  FavouriteCell.h
//  Umang
//
//  Created by spice_digital on 28/09/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyButton.h"
#import "HCSStarRatingView.h"
#import "MyFavButton.h"

@interface FavouriteCell : UITableViewCell
@property(nonatomic,retain)IBOutlet UILabel *lbl_serviceName;
@property(nonatomic,retain)IBOutlet UILabel *lbl_serviceDesc;
@property(nonatomic,retain)IBOutlet UILabel *lbl_serviceCateg;
@property(nonatomic,retain)IBOutlet UIImageView *img_serviceCateg;
@property(nonatomic,retain)IBOutlet MyFavButton *btn_serviceFav;
@property(nonatomic,retain)IBOutlet UILabel *lbl_nouse;
@property(nonatomic,retain)IBOutlet UIButton *btn_moreInfo;
//@property (nonatomic, strong)IBOutlet TQStarRatingView *starRatingView;
@property(retain, nonatomic) IBOutlet UIScrollView *scrollView_nouse;

@property(nonatomic,retain)IBOutlet UILabel *lbl_rating;
@property(nonatomic,retain)IBOutlet UIImageView *img_star;

-(void)addCategoryStateItemsToScrollView:(NSArray*)arrCat state:(NSArray*)arrStates withFont:(CGFloat)fontSize cell:(FavouriteCell*)cell andTagIndex:(NSIndexPath *)indexpath;
-(void)stateNameWithCategory:(NSDictionary*)dic ofCell:(FavouriteCell*)cell andTagIndex:(NSIndexPath *)indexpath;
@end

