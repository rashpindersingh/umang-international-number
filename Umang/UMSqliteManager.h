//
//  UMSqliteManager.h
//  Umang
//
//  Created by deepak singh rawat on 20/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
//@import UIKit;
@interface UMSqliteManager : NSObject
{
    sqlite3  *umangDB;
    
}

/*
 -(void)insertNotifData:(NSString*)notifId
 notifTitle:(NSString*)notifTitle
 notifImg:(NSString*)notifImg
 notifMsg:(NSString*)notifMsg
 notifType:(NSString*)notifType
 notifDate:(NSString*)notifDate
 notifTime:(NSString*)notifTime
 notifState:(NSString*)notifState
 notifIsFav:(NSString*)notifIsFav
 serviceId:(NSString*)serviceId
 currentTimeMills:(NSString*)currentTimeMills
 subType:(NSString*)subType
 url:(NSString*)url
 screenName:(NSString*)screenName
 receiveDateTime:(NSString*)receiveDateTime
 dialogMsg:(NSString*)dialogMsg
 webpageTitle:(NSString*)webpageTitle;
 */
/*-(void)insertNotifData:(NSString*)notifId
            notifTitle:(NSString*)notifTitle
              notifImg:(NSString*)notifImg
              notifMsg:(NSString*)notifMsg
             notifType:(NSString*)notifType
             notifDate:(NSString*)notifDate
             notifTime:(NSString*)notifTime
            notifState:(NSString*)notifState
            notifIsFav:(NSString*)notifIsFav
             serviceId:(NSString*)serviceId
      currentTimeMills:(NSString*)currentTimeMills
               subType:(NSString*)subType
                   url:(NSString*)url
            screenName:(NSString*)screenName
       receiveDateTime:(NSString*)receiveDateTime
             dialogMsg:(NSString*)dialogMsg
          webpageTitle:(NSString*)webpageTitle
               user_id:(NSString*)user_id;


*/
-(void)insertNotifData:(NSString*)notifId
            notifTitle:(NSString*)notifTitle
              notifImg:(NSString*)notifImg
              notifMsg:(NSString*)notifMsg
             notifType:(NSString*)notifType
             notifDate:(NSString*)notifDate
             notifTime:(NSString*)notifTime
            notifState:(NSString*)notifState
            notifIsFav:(NSString*)notifIsFav
             serviceId:(NSString*)serviceId
      currentTimeMills:(NSString*)currentTimeMills
               subType:(NSString*)subType
                   url:(NSString*)url
            screenName:(NSString*)screenName
       receiveDateTime:(NSString*)receiveDateTime
             dialogMsg:(NSString*)dialogMsg
          webpageTitle:(NSString*)webpageTitle
           servicename:(NSString*)servicename
           deptname:(NSString*)deptname
               user_id:(NSString*)user_id;





-(void)insertServiceSections:(NSString*)sectionName
                  sectionImg:(NSString*)sectionImg
             sectionServices:(NSString*)sectionServices; //recent,trending

//-(void)updateNotifIsFav:(NSString*)notifId notifIsFav:(NSString*)notifIsFav;   // notification fv or not
-(void)updateNotifIsFav:(NSString*)notifId notifIsFav:(NSString*)notifIsFav withUser_id:(NSString*)user_id;   // notification fv or not


/*-(void)insertServicesData:(NSString*)serviceId
 serviceName:(NSString*)serviceName
 serviceDesc:(NSString*)serviceDesc
 serviceImg:(NSString*)serviceImg
 serviceCategory:(NSString*)serviceCategory
 serviceSubCat:(NSString*)serviceSubCat
 serviceRating:(NSString*)serviceRating
 serviceUrl:(NSString*)serviceUrl
 serviceState:(NSString*)serviceState
 serviceLat:(NSString*)serviceLat
 serviceLng:(NSString*)serviceLng
 serviceIsFav:(NSString*)serviceIsFav
 serviceIsHidden:(NSString*)serviceIsHidden
 servicePhoneNumber:(NSString*)servicePhoneNumber
 serviceisNotifEnabled:(NSString*)serviceisNotifEnabled
 serviceWebsite:(NSString*)serviceWebsite
 servicelang:(NSString*)servicelang
 servicedeptAddress:(NSString*)servicedeptAddress
 serviceworkingHours:(NSString*)serviceworkingHours
 servicedeptDescription:(NSString*)servicedeptDescription
 serviceemail:(NSString*)serviceemail
 popularity:(NSString*)popularity
 servicecategoryId:(NSString*)servicecategoryId;
 
 
 
 -(void)updateServicesData:(NSString*)serviceId
 serviceName:(NSString*)serviceName
 serviceDesc:(NSString*)serviceDesc
 serviceImg:(NSString*)serviceImg
 serviceCategory:(NSString*)serviceCategory
 serviceSubCat:(NSString*)serviceSubCat
 serviceRating:(NSString*)serviceRating
 serviceUrl:(NSString*)serviceUrl
 serviceState:(NSString*)serviceState
 serviceLat:(NSString*)serviceLat
 serviceLng:(NSString*)serviceLng
 serviceIsFav:(NSString*)serviceIsFav
 serviceIsHidden:(NSString*)serviceIsHidden
 servicePhoneNumber:(NSString*)servicePhoneNumber
 serviceisNotifEnabled:(NSString*)serviceisNotifEnabled
 serviceWebsite:(NSString*)serviceWebsite
 servicelang:(NSString*)servicelang
 servicedeptAddress:(NSString*)servicedeptAddress
 serviceworkingHours:(NSString*)serviceworkingHours
 servicedeptDescription:(NSString*)servicedeptDescription
 serviceemail:(NSString*)serviceemail
 popularity:(NSString*)popularity
 servicecategoryId:(NSString*)servicecategoryId;
 
 */

//======= need to close above code


/*
-(void)insertServicesData:(NSString*)serviceId
              serviceName:(NSString*)serviceName
              serviceDesc:(NSString*)serviceDesc
               serviceImg:(NSString*)serviceImg
          serviceCategory:(NSString*)serviceCategory
            serviceSubCat:(NSString*)serviceSubCat
            serviceRating:(NSString*)serviceRating
               serviceUrl:(NSString*)serviceUrl
             serviceState:(NSString*)serviceState
               serviceLat:(NSString*)serviceLat
               serviceLng:(NSString*)serviceLng
             serviceIsFav:(NSString*)serviceIsFav
          serviceIsHidden:(NSString*)serviceIsHidden
       servicePhoneNumber:(NSString*)servicePhoneNumber
    serviceisNotifEnabled:(NSString*)serviceisNotifEnabled
           serviceWebsite:(NSString*)serviceWebsite
              servicelang:(NSString*)servicelang
       servicedeptAddress:(NSString*)servicedeptAddress
      serviceworkingHours:(NSString*)serviceworkingHours
   servicedeptDescription:(NSString*)servicedeptDescription
             serviceemail:(NSString*)serviceemail
               popularity:(NSString*)popularity
        servicecategoryId:(NSString*)servicecategoryId
        serviceOtherState:(NSString*)serviceOtherState;



-(void)updateServicesData:(NSString*)serviceId
              serviceName:(NSString*)serviceName
              serviceDesc:(NSString*)serviceDesc
               serviceImg:(NSString*)serviceImg
          serviceCategory:(NSString*)serviceCategory
            serviceSubCat:(NSString*)serviceSubCat
            serviceRating:(NSString*)serviceRating
               serviceUrl:(NSString*)serviceUrl
             serviceState:(NSString*)serviceState
               serviceLat:(NSString*)serviceLat
               serviceLng:(NSString*)serviceLng
             serviceIsFav:(NSString*)serviceIsFav
          serviceIsHidden:(NSString*)serviceIsHidden
       servicePhoneNumber:(NSString*)servicePhoneNumber
    serviceisNotifEnabled:(NSString*)serviceisNotifEnabled
           serviceWebsite:(NSString*)serviceWebsite
              servicelang:(NSString*)servicelang
       servicedeptAddress:(NSString*)servicedeptAddress
      serviceworkingHours:(NSString*)serviceworkingHours
   servicedeptDescription:(NSString*)servicedeptDescription
             serviceemail:(NSString*)serviceemail
               popularity:(NSString*)popularity
        servicecategoryId:(NSString*)servicecategoryId
        serviceOtherState:(NSString*)serviceOtherState;




*/












-(void)updateServiceIsFav:(NSString*)serviceId
             serviceIsFav:(NSString*)serviceIsFav hitAPI:(NSString*)hitStatus;


-(void)updateServiceIsNotifEnabled:(NSString*)serviceId
                    notifIsEnabled:(NSString*)notifIsEnabled;

-(NSArray*)getTrendingServiceData;

//-(void)deleteNotification:(NSString*)notifId;
-(void)deleteNotification:(NSString*)notifId withUser_id:(NSString*)user_id;


//-(void)deleteAllNotifications;
-(void)deleteAllNotifications:(NSString*)user_id;

-(void)deleteServiceData:(NSString*)serviceId;
-(void)deleteAllServices;
-(void)deleteSectionData;

-(void)createUmangDB;
-(NSArray*)loadDataServiceSection;
-(NSArray*)loadDataServiceData;



-(NSArray*)getServiceData:(NSString*)serviceId;
-(NSString*)getServiceFavStatus:(NSString*)serviceId;

-(NSArray*)getCentralServiceData;


//-(NSArray*)getServiceDataForServiceID:(NSString*)serviceId;


-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename;

-(NSArray*)getAllServiceDataNotCentral;

-(void)deleteBannerHomeData;

-(void)deleteBannerStateData;


-(void)insertBannerHomeData:(NSString*)bannerImgUrl
           bannerActionType:(NSString*)bannerActionType
            bannerActionUrl:(NSString*)bannerActionUrl
                 bannerDesc:(NSString*)bannerDesc;


-(void)insertBannerStateData:(NSString*)bannerImgUrl
            bannerActionType:(NSString*)bannerActionType
             bannerActionUrl:(NSString*)bannerActionUrl
                  bannerDesc:(NSString*)bannerDesc;

-(NSArray*)getBannerStateData;
-(NSArray*)getBannerHomeData;
-(NSArray*)loadServiceCategory;
-(NSString*)getServiceCategoryId:(NSString*)catName;


-(NSArray*)getFilteredServiceData:(NSString*)sortBy serviceType:(NSString*)serviceType stateIdAlist:(NSArray*)stateIdAlist categoryList:(NSArray*)categoryList;


//-(NSArray*)getFilteredNotifData:(NSString*)sortBy notifType:(NSString*)notifType serviceIdAlist:(NSArray*)serviceIdAlist stateIdAlist:(NSArray*)stateIdAlist startDate:(NSString*)startDate  endDate:(NSString*)endDate;

-(NSArray*)getFilteredNotifData:(NSString*)sortBy notifType:(NSString*)notifType serviceIdAlist:(NSArray*)serviceIdAlist stateIdAlist:(NSArray*)stateIdAlist startDate:(NSString*)startDate  endDate:(NSString*)endDate user_id:(NSString*)user_id;


-(NSArray*)getServicesDataForNotifSettings;

//-(NSArray*)getNotifData;
-(NSArray*)getNotifData:(NSString*)user_id;;

//-(NSArray*)getNotifDatatrans
-(NSArray*)getNotifDatatrans:(NSString*)user_id; //no call found


//-(NSArray*)getNotifDatapromo;
-(NSArray*)getNotifDatapromo:(NSString*)user_id ;

//-(NSArray*)getNotifDatafavourite;
-(NSArray*)getNotifDatafavourite:(NSString*)user_id; //no call found


-(NSArray*)getEducationList;
-(NSArray*)getOccupationList;


-(BOOL)getServiceLanguage:(NSString*)serviceId withDeviceLang:(NSString*)langDevice;

//-(NSString*)getNotifyFavStatus:(NSString*)notifyId;
-(NSString*)getNotifyFavStatus:(NSString*)notifyId withUser_id:(NSString*)user_id;

-(NSString*)getServiceId:(NSString*)serviceName;


-(NSArray*)getFilteredFavouriteServiceData:(NSString*)sortBy serviceType:(NSString*)serviceType stateIdAlist:(NSArray*)stateIdAlist categoryList:(NSArray*)categoryList;

-(void)saveImagesInLocalDirectory:(NSString*)imgURL;
-(UIImage*)loadImage;

/*-(void)insertBooksData:(NSString*) userId
                bookId:(NSString*) bookId
             bookClass:(NSString*) bookClass
             bookImage:(NSString*) bookImage
              bookLang:(NSString*) bookLang
              bookName:(NSString*) bookName
               bookSub:(NSString*) bookSub
                   cId:(NSString*) cId
            cClassBook:(NSString*) cClassBook
             cEpubLink:(NSString*) cEpubLink
                cTitle:(NSString*) cTitle
                   cNo:(NSString*) cNo
               cEnmLay:(NSString*) cEnmLay
              cAllData:(NSString*) cAllData
              cEnmType:(NSString*) cEnmType
              cHashKey:(NSString*) cHashKey
                 cPath:(NSString*) cPath;
*/

/*
-(void)insertBooksData:(NSString*) userId
                bookId:(NSString*) bookId
             bookClass:(NSString*) bookClass
             bookImage:(NSString*) bookImage
              bookLang:(NSString*) bookLang
              bookName:(NSString*) bookName
               bookSub:(NSString*) bookSub
                   cId:(NSString*) cId
            cClassBook:(NSString*) cClassBook
             cEpubLink:(NSString*) cEpubLink
                cTitle:(NSString*) cTitle
                   cNo:(NSString*) cNo
               cEnmLay:(NSString*) cEnmLay
              cAllData:(NSString*) cAllData
              cEnmType:(NSString*) cEnmType
              cHashKey:(NSString*) cHashKey
                 cPath:(NSString*) cPath
         cbookCategory:(NSString*) cbookCategory;

*/
-(void)insertBooksData:(NSString*) userId
                bookId:(NSString*) bookId
             bookClass:(NSString*) bookClass
             bookImage:(NSString*) bookImage
              bookLang:(NSString*) bookLang
              bookName:(NSString*) bookName
               bookSub:(NSString*) bookSub
                   cId:(NSString*) cId
            cClassBook:(NSString*) cClassBook
             cEpubLink:(NSString*) cEpubLink
                cTitle:(NSString*) cTitle
                   cNo:(NSString*) cNo
               cEnmLay:(NSString*) cEnmLay
              cAllData:(NSString*) cAllData
              cEnmType:(NSString*) cEnmType
              cHashKey:(NSString*) cHashKey
                 cPath:(NSString*) cPath
         cbookCategory:(NSString*) cbookCategory
           category:(NSString*)Category;








-(NSString*)deleteChapter:(NSString*)userId withChapterid:(NSString*)chapterId;
-(NSArray*)getAllBooksData:(NSString*)userId;
-(NSArray*)getUniqueBooksData:(NSString*)userId withBookid:(NSString*)bookId;
-(NSArray*)getChaptersDataFromBookId:(NSString*)userId withBookId:(NSString*)book_id;
-(NSArray*)getChapterDetailFromChapterId:(NSString*)userId withchapterId:(NSString*)chapterId;
-(NSArray*)getChaptersIdFromBookId:(NSString*)userId withbookId:(NSString*)bookId;


+(NSString* )getDatabasePath;
+(NSMutableArray *)executeQuery:(NSString*)str;
+(NSString*)encodedString:(const unsigned char *)ch;
+(BOOL)executeScalarQuery:(NSString*)str;


-(void)updateServiceMostPopular:(NSString*)serviceId withRating:(NSString*)MpRating;







-(void)deleteServicesDirectory;
-(void)deleteServiceDirDataWithId:(NSString*)serviceId;



-(NSArray*)getAllServicesDirData;

-(NSArray*)getServiceStateData:(NSString*)stateId;
-(void)onUpgrade:(NSInteger)oldVersion withnewVersion:(NSInteger)newVersion;
-(NSArray*)getAllUMANGServicesDirData;
-(NSArray*)getOTHERServicesDirData;

-(NSString*)getServiceURL:(NSString*) serviceId;
/*
 -(void)deleteDatabase;
 -(void)upgradeDatabaseIfRequired;
 */
-(NSArray*)getServiceStateAvailable;
-(NSArray*)getUpcommingStates;




-(void)insertServicesData:(NSString*)serviceId
              serviceName:(NSString*)serviceName
              serviceDesc:(NSString*)serviceDesc
               serviceImg:(NSString*)serviceImg
          serviceCategory:(NSString*)serviceCategory
            serviceSubCat:(NSString*)serviceSubCat
            serviceRating:(NSString*)serviceRating
               serviceUrl:(NSString*)serviceUrl
             serviceState:(NSString*)serviceState
               serviceLat:(NSString*)serviceLat
               serviceLng:(NSString*)serviceLng
             serviceIsFav:(NSString*)serviceIsFav
          serviceIsHidden:(NSString*)serviceIsHidden
       servicePhoneNumber:(NSString*)servicePhoneNumber
    serviceisNotifEnabled:(NSString*)serviceisNotifEnabled
           serviceWebsite:(NSString*)serviceWebsite
              servicelang:(NSString*)servicelang
       servicedeptAddress:(NSString*)servicedeptAddress
      serviceworkingHours:(NSString*)serviceworkingHours
   servicedeptDescription:(NSString*)servicedeptDescription
             serviceemail:(NSString*)serviceemail
               popularity:(NSString*)popularity
        servicecategoryId:(NSString*)servicecategoryId
        serviceOtherState:(NSString*)serviceOtherState
         otherwebsite:(NSString*)otherwebsite;


-(void)updateServicesData:(NSString*)serviceId
              serviceName:(NSString*)serviceName
              serviceDesc:(NSString*)serviceDesc
               serviceImg:(NSString*)serviceImg
          serviceCategory:(NSString*)serviceCategory
            serviceSubCat:(NSString*)serviceSubCat
            serviceRating:(NSString*)serviceRating
               serviceUrl:(NSString*)serviceUrl
             serviceState:(NSString*)serviceState
               serviceLat:(NSString*)serviceLat
               serviceLng:(NSString*)serviceLng
             serviceIsFav:(NSString*)serviceIsFav
          serviceIsHidden:(NSString*)serviceIsHidden
       servicePhoneNumber:(NSString*)servicePhoneNumber
    serviceisNotifEnabled:(NSString*)serviceisNotifEnabled
           serviceWebsite:(NSString*)serviceWebsite
              servicelang:(NSString*)servicelang
       servicedeptAddress:(NSString*)servicedeptAddress
      serviceworkingHours:(NSString*)serviceworkingHours
   servicedeptDescription:(NSString*)servicedeptDescription
             serviceemail:(NSString*)serviceemail
               popularity:(NSString*)popularity
        servicecategoryId:(NSString*)servicecategoryId
        serviceOtherState:(NSString*)serviceOtherState
        otherwebsite:(NSString*)otherwebsite;

/*
-(void)updateServicesDirData:(NSString*)serviceId
                 serviceName:(NSString*)serviceName
                 serviceDesc:(NSString*)serviceDesc
                  serviceImg:(NSString*)serviceImg
                  serviceLat:(NSString*)serviceLat
                 serviceLong:(NSString*)serviceLong
              servicePhoneno:(NSString*)servicePhoneno
              serviceWebsite:(NSString*)serviceWebsite
                serviceEmail:(NSString*)serviceEmail
              serviceAddress:(NSString*)serviceAddress
          serviceWorkingHour:(NSString*)serviceWorkingHour
            serviceOtherInfo:(NSString*)serviceOtherInfo
            serviceNativeApp:(NSString*)serviceNativeApp
                 isavailable:(NSString*)isavailable
               nativeAppName:(NSString*)nativeAppName;
 
 
 -(void)insertServicesDirData:(NSString*)serviceId
 serviceName:(NSString*)serviceName
 serviceDesc:(NSString*)serviceDesc
 serviceImg:(NSString*)serviceImg
 serviceLat:(NSString*)serviceLat
 serviceLong:(NSString*)serviceLong
 servicePhoneno:(NSString*)servicePhoneno
 serviceWebsite:(NSString*)serviceWebsite
 serviceEmail:(NSString*)serviceEmail
 serviceAddress:(NSString*)serviceAddress
 serviceWorkingHour:(NSString*)serviceWorkingHour
 serviceOtherInfo:(NSString*)serviceOtherInfo
 serviceNativeApp:(NSString*)serviceNativeApp
 isavailable:(NSString*)isavailable
 nativeAppName:(NSString*)nativeAppName;

 
 */

/*-(void)updateServicesDirData:(NSString*)serviceId
                 serviceName:(NSString*)serviceName
                 serviceDesc:(NSString*)serviceDesc
                  serviceImg:(NSString*)serviceImg
                  serviceLat:(NSString*)serviceLat
                 serviceLong:(NSString*)serviceLong
              servicePhoneno:(NSString*)servicePhoneno
              serviceWebsite:(NSString*)serviceWebsite
                serviceEmail:(NSString*)serviceEmail
              serviceAddress:(NSString*)serviceAddress
          serviceWorkingHour:(NSString*)serviceWorkingHour
            serviceOtherInfo:(NSString*)serviceOtherInfo
            serviceNativeApp:(NSString*)serviceNativeApp
                 isavailable:(NSString*)isavailable
               nativeAppName:(NSString*)nativeAppName
                otherwebsite:(NSString*)otherwebsite
                     andLink:(NSString*)andLink;
*/

-(void)updateServicesDirData:(NSString*)serviceId
                 serviceName:(NSString*)serviceName
                 serviceDesc:(NSString*)serviceDesc
                  serviceImg:(NSString*)serviceImg
                  serviceLat:(NSString*)serviceLat
                 serviceLong:(NSString*)serviceLong
              servicePhoneno:(NSString*)servicePhoneno
              serviceWebsite:(NSString*)serviceWebsite
                serviceEmail:(NSString*)serviceEmail
              serviceAddress:(NSString*)serviceAddress
          serviceWorkingHour:(NSString*)serviceWorkingHour
            serviceOtherInfo:(NSString*)serviceOtherInfo
            serviceNativeApp:(NSString*)serviceNativeApp
                 isavailable:(NSString*)isavailable
               nativeAppName:(NSString*)nativeAppName
                otherwebsite:(NSString*)otherwebsite
                     andLink:(NSString*)andLink
                  categoryid:(NSString*)categoryid
                     stateid:(NSString*)stateid
                  otherstate:(NSString*)otherstate
                categoryname:(NSString*)categoryname;

/*-(void)insertServicesDirData:(NSString*)serviceId
                 serviceName:(NSString*)serviceName
                 serviceDesc:(NSString*)serviceDesc
                  serviceImg:(NSString*)serviceImg
                  serviceLat:(NSString*)serviceLat
                 serviceLong:(NSString*)serviceLong
              servicePhoneno:(NSString*)servicePhoneno
              serviceWebsite:(NSString*)serviceWebsite
                serviceEmail:(NSString*)serviceEmail
              serviceAddress:(NSString*)serviceAddress
          serviceWorkingHour:(NSString*)serviceWorkingHour
            serviceOtherInfo:(NSString*)serviceOtherInfo
            serviceNativeApp:(NSString*)serviceNativeApp
                 isavailable:(NSString*)isavailable
               nativeAppName:(NSString*)nativeAppName
                otherwebsite:(NSString*)otherwebsite
                     andLink:(NSString*)andLink;*/

-(void)insertServicesDirData:(NSString*)serviceId
                 serviceName:(NSString*)serviceName
                 serviceDesc:(NSString*)serviceDesc
                  serviceImg:(NSString*)serviceImg
                  serviceLat:(NSString*)serviceLat
                 serviceLong:(NSString*)serviceLong
              servicePhoneno:(NSString*)servicePhoneno
              serviceWebsite:(NSString*)serviceWebsite
                serviceEmail:(NSString*)serviceEmail
              serviceAddress:(NSString*)serviceAddress
          serviceWorkingHour:(NSString*)serviceWorkingHour
            serviceOtherInfo:(NSString*)serviceOtherInfo
            serviceNativeApp:(NSString*)serviceNativeApp
                 isavailable:(NSString*)isavailable
               nativeAppName:(NSString*)nativeAppName
                otherwebsite:(NSString*)otherwebsite
                     andLink:(NSString*)andLink
                  categoryid:(NSString*)categoryid
                     stateid:(NSString*)stateid
                  otherstate:(NSString*)otherstate
                categoryname:(NSString*)categoryname;

-(NSArray*)getStateWiseUMANGServicesDirData:(NSString*)state_Id;

-(void)clearSingletonProfileValue;

-(BOOL)isLibertyPatchJailBroken;
-(NSArray*)getServiceStateAvailableInEnglishOnly;
-(NSString*)getStateEnglishName:(NSString*)state_id;
-(NSString*)getStateCodeEnglish:(NSString*)stateName;
-(NSMutableArray*)loadCountryCodeData;
-(NSMutableArray*)loadStaticCountry;

@end
