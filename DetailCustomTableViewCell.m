//
//  DetailCustomTableViewCell.m
//  Umang
//
//  Created by Kuldeep Saini on 27/05/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "DetailCustomTableViewCell.h"
#import "SharedManager.h"
@implementation DetailCustomTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)designInterfaceForFrame:(CGRect)frame withFontSizeApplied:(CGFloat)fontSize withMapOptionRequired:(BOOL)isMapRequired{
    
    self.fontSizeApplied = fontSize;
    
    
    CGFloat xPos = 15;
    CGFloat yPos = 10;
    CGFloat width = frame.size.width - 2*xPos;
    
    self.lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, width, 30)];
    self.lblTitle.textColor = [UIColor blackColor];
    self.lblTitle.font = [UIFont boldSystemFontOfSize:17.0];
    [self.contentView addSubview:self.lblTitle];
    
    yPos+= 30;
    
    self.lblDesc = [[UILabel alloc] initWithFrame:CGRectMake(xPos, yPos, width, 30)];
    self.lblDesc.numberOfLines = 0;
    self.lblDesc.font = [UIFont systemFontOfSize:self.fontSizeApplied];
    [self.contentView addSubview:self.lblDesc];
    
    yPos+= 40;
    
    if (isMapRequired) {
        
        self.bottomMapView = [[UIView alloc] initWithFrame:CGRectMake(0, yPos, frame.size.width, 40)];
        self.bottomMapView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.bottomMapView];
        
        
        self.imgSeparator = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 1)];
        self.imgSeparator.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:239.0/255.0 alpha:0.8];
        [self.bottomMapView addSubview:self.imgSeparator];
        
        self.btnViewOnMap = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btnViewOnMap.frame = CGRectMake(xPos, 0, 100, 40.0);
        [self.btnViewOnMap setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [self.btnViewOnMap setTitle:NSLocalizedString(@"view_on_map", nil) forState:UIControlStateNormal];
        self.btnViewOnMap.titleLabel.font = [UIFont systemFontOfSize:13.0];
        [self.bottomMapView addSubview:self.btnViewOnMap];
        
    }
}


-(void)bindCellDataWithTitle:(NSString*)title andDesc:(NSString*)desc withScreenWidth:(CGFloat)width{
    
    self.lblTitle.text = title;
    
    
    NSAttributedString * attStringForDesc = [[NSAttributedString alloc] initWithData:[desc dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    self.lblDesc.attributedText = attStringForDesc;
    self.lblDesc.textColor = [UIColor grayColor];
    self.lblDesc.font = [UIFont systemFontOfSize:self.fontSizeApplied-1];
    
    
    CGRect dynamicHeight = [self rectForText:desc usingFont:[UIFont systemFontOfSize:self.fontSizeApplied] boundedBySize:CGSizeMake(width, 1000.0)];
    
    CGFloat xPos = 15;
    CGFloat yPos = 40;
    
    self.lblDesc.frame = CGRectMake(xPos, yPos, width, dynamicHeight.size.height);
    
    yPos+=dynamicHeight.size.height + 10;
    
    if (self.bottomMapView != nil) {
        self.bottomMapView.frame = CGRectMake(0, yPos, width+30, 30);
        
        
    }
    
    SharedManager *singelton = [SharedManager sharedSingleton];
    
    if (singelton.isArabicSelected) {
        self.lblTitle.textAlignment = NSTextAlignmentRight;
        self.lblDesc.textAlignment = NSTextAlignmentRight;
        
        self.btnViewOnMap.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        self.btnViewOnMap.frame = CGRectMake(xPos, 0, 100, self.bottomMapView.frame.size.height);
        
    }
    else{
        self.lblTitle.textAlignment = NSTextAlignmentLeft;
        self.lblDesc.textAlignment = NSTextAlignmentLeft;
        
        self.btnViewOnMap.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        self.btnViewOnMap.frame = CGRectMake(width+30-100, 0, 100, self.bottomMapView.frame.size.height);
        
    }
}


-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
    
    NSDictionary *dictFont = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:dictFont context:nil];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
