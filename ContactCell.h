//
//  ContactCell.h
//  Umang
//
//  Created by admin on 28/11/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *HeadOfficeName;
@property (weak, nonatomic) IBOutlet UILabel *subHeaderlbl;
@property (weak, nonatomic) IBOutlet UILabel *addresslbl;
@property (weak, nonatomic) IBOutlet UILabel *statePinLbl;
@property (weak, nonatomic) IBOutlet UILabel *phoneLbl;

@end
