//
//  newAadharDetailTableViewCell.h
//  Umang
//
//  Created by admin on 06/04/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface newAadharDetailTableViewCell : UITableViewCell
{
    SharedManager *singleton;
}
typedef enum
{
    
    AADHARWITHPIC1= 10,
    AADHARWITHOUTPIC1,
    NOTLINKED1,
    
    
}TYPE_AADHAR1;

@property (weak, nonatomic) IBOutlet UIImageView *imgAadhar;
@property (weak, nonatomic) IBOutlet UILabel *lblAadharNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblaadharDigit;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *lblAadharInformation;
@property (assign,nonatomic ) TYPE_AADHAR1 TYPE_AADHAR_CHOOSEN;

//@property (nonatomic,assign) BOOL isAadhardetailWithoutPic;
//@property (nonatomic,assign) BOOL isNotLinkedAadharCon;

@property (weak, nonatomic) IBOutlet UILabel *lbllangDefault;
@property (weak, nonatomic) IBOutlet UILabel *lbllangLocal;
@property (weak, nonatomic) IBOutlet UISwitch *btn_switch;


-(void)bindDataForHeaderView;

@end
