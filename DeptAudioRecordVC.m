//
//  DeptAudioRecordVC.m
//  Umang
//
//  Created by admin on 12/06/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import "DeptAudioRecordVC.h"



static float totaltime;
static float totalPlayTime = 0.0;

@interface DeptAudioRecordVC ()<AVAudioPlayerDelegate, AVAudioRecorderDelegate>
{
    NSURL *audioFileURL ;
}


@end

@implementation DeptAudioRecordVC
- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Audio Recording Setup
    self.view.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:0.5];
    [self.audioRecordProgress setProgress:0];
    [self.audioProgress setHidden:YES];
    
    self.audioProgress.transform = CGAffineTransformMakeScale(1.0, 1.0);
    self.audioProgress.progressTintColor =[UIColor blueColor];
    self.audioProgress.trackTintColor = [UIColor lightGrayColor];
    
    
    
    self.audioRecordProgress.transform = CGAffineTransformMakeScale(1.0, 1.0);
    self.audioRecordProgress.progressTintColor =[UIColor blueColor];
    self.audioRecordProgress.trackTintColor = [UIColor lightGrayColor];
    
     NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:[NSTemporaryDirectory() stringByAppendingString:@"audioRecording.m4a"]  error:NULL];
    
        
    audioFileURL = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingString:@"audioRecording.m4a"]];
    
    NSDictionary *audioSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [NSNumber numberWithFloat:44100],AVSampleRateKey,
                                   [NSNumber numberWithInt: kAudioFormatMPEG4AAC],AVFormatIDKey,
                                   [NSNumber numberWithInt: 1],AVNumberOfChannelsKey,
                                   [NSNumber numberWithInt:AVAudioQualityMedium],AVEncoderAudioQualityKey,nil];
    
    self.audioRecorder = [[AVAudioRecorder alloc]
                          initWithURL:audioFileURL
                          settings:audioSettings
                          error:nil];
    
    
    [self updateAudioPlayerLayout:true];
    [btnDone setTitle:NSLocalizedString(@"done", nil) forState:UIControlStateNormal];
    [btnCancel setTitle:NSLocalizedString(@"cancel_caps", nil) forState:UIControlStateNormal];
    lblMaximumAudio.text = NSLocalizedString(@"max_audio_limit", nil);
    
    lblAudiotimer.text = NSLocalizedString(@"audio_recording", nil);
    
    [audioStopButton setHidden:true];
    totalPlayTime = 0.0;
    
    
    recordButtonCentre.constant = 0.0;
    
    // Do any additional setup after loading the view from its nib.
}

- (NSURL*)grabFileURL:(NSString *)fileName
{
    
    // find Documents directory
    NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    
    // append a file name to it
    documentsURL = [documentsURL URLByAppendingPathComponent:fileName];
    
    
    
    
    return documentsURL;
}

-(void)updateAudioPlayerLayout:(BOOL)hide {
    [audioPlayButton setHidden:hide];
    [audioRecordButton setHidden:!hide];
}
- (NSString *) base64StringFromFileAtPath: (NSURL*) filePath {
    // NSData * dataFromFile = [NSData dataWithContentsOfFile:filePath];
    
    // NSString *path = [filePath path];
    NSData *fileData = [NSData dataWithContentsOfFile:filePath.path];
    
    if(fileData==nil)
    {
        //No file found base64 blank
    }
    
    return [fileData base64Encoding];
}


- (NSData*) dataFrom64String : (NSString*) stringEncodedWithBase64
{
    NSData *dataFromBase64 = [[NSData alloc] initWithBase64Encoding:stringEncodedWithBase64];
    return dataFromBase64;
}

- (IBAction)bgTouch:(id)sender
{
    [self dismissViewWithDealloc];
    
    [self.audioPlayer stop];
    [self.audioRecorder stop];
    
    audioFileURL = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingString:@"audioRecording.m4a"]];
    //self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audioFileURL error:nil];
    //[self updateAudioPlayerLayout:false];
//    [audioPlayButton setSelected: false];
//    self.audioPlayer.delegate = self;
    [self getAudioFileSize:audioFileURL];
    
    
    NSString * base64String =[self base64StringFromFileAtPath:audioFileURL];
    
    if([base64String length]==0)
    {
        base64String=@"";
    }
    
    NSData *audioData = [self dataFrom64String:base64String];
    
    NSDictionary *dic_temp = [NSDictionary dictionaryWithObjectsAndKeys:base64String,@"base64String",[NSNumber numberWithInteger:audioData.length],@"audioData", nil];
    
    [self audioBase64ReturnToDept:dic_temp];
    // [self dismissViewControllerAnimated:NO completion: nil];
}
-(void)dismissViewWithDealloc{
    [self.audioProgress setProgress:0];
    totaltime=0;
    
    
    [self.Recordtimer invalidate];
    self.Recordtimer = nil;
    [self.timer invalidate];
    self.timer = nil;
}

- (void)audioBase64ReturnToDept:(NSDictionary*)parameters
{
    NSLog(@"parameters=%@",parameters);
    
    if ([self.delegate respondsToSelector:@selector(audioBase64ReturnToDept:)]) {
        [self.delegate audioBase64ReturnToDept:parameters];
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
    
}

- (IBAction)audioRecord:(UIButton*)sender
{
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [audioStopButton setHidden: false];
    
    recordButtonCentre.constant = -22.0;
    
    if (sender.isSelected) {
        if (_audioRecorder.recording) {
            [self.audioRecorder pause];
            [session setActive:false error:nil];
            [self.Recordtimer invalidate];
            self.Recordtimer = nil;
            [sender setImage:[UIImage imageNamed:@"btn_record_aud.png"] forState:UIControlStateSelected];
            //pauseRecord
        }else {
            self.Recordtimer = nil;
            self.Recordtimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                                target:self
                                                              selector:@selector(updateRecordProgress)
                                                              userInfo:nil
                                                               repeats:YES];
            [session setActive:true error:nil];
            
            [session setCategory:AVAudioSessionCategoryRecord error:nil];
            [sender setImage:[UIImage imageNamed:@"pauseRecord"] forState:UIControlStateSelected];
            [self.audioRecorder recordAtTime:self.audioRecorder.currentTime];
        }
    }else
    {
        sender.selected = !sender.isSelected;
        [self.timer invalidate];
        self.timer = nil;
        
        [self.Recordtimer invalidate];
        self.Recordtimer = nil;
        [self.audioRecordProgress setHidden:NO];
        [self.audioProgress setHidden:YES];
        [self.audioRecordProgress setProgress:0];
        totaltime=0;
        self.Recordtimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                            target:self
                                                          selector:@selector(updateRecordProgress)
                                                          userInfo:nil
                                                           repeats:YES];
        [session setActive:true error:nil];
        [session setCategory:AVAudioSessionCategoryRecord error:nil];
        self.audioRecorder.delegate = self;
        self.audioRecorder.meteringEnabled = YES;
        [self.audioRecorder prepareToRecord];
        [self.audioRecorder recordForDuration:120.0];
    }
    
}
- (void)updateRecordProgress
{
    totaltime= totaltime+ 1.0;
    NSInteger timeLeft = totaltime;
    
    
    
    NSString *seconds = timeLeft%60 < 10 ? [NSString stringWithFormat:@"0%ld",timeLeft%60]  :[NSString stringWithFormat:@"%ld",timeLeft%60];
    lbltimer.text = [NSString stringWithFormat:@"0%ld:%@",timeLeft/60,seconds];
    
    
    //lbltimer.text=[NSString stringWithFormat:@"%.2f",timeLeft];
    
    self.audioRecordProgress.progress = totaltime/120.0;
    if (totaltime>120)
    {
        //  [self.Recordtimer invalidate];
        //self.Recordtimer = nil;
        lbltimer.text=@"120.00";
        self.audioRecordProgress.progress = 1;
        //        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Time Reach Maximum Value" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        //        [alert show];
        
        [self audioStop:self];
    }
}

- (void)updateProgress
{
    
    
    float timeLeft = self.audioPlayer.currentTime/self.audioPlayer.duration;
    //    NSLog(@"timeLeft=%f cuurent Time --%f ---duration --%f",timeLeft, self.audioPlayer.currentTime, self.audioPlayer.duration);
    //    float newProgess = totalPlayTime - 1.0;
    //    self.audioProgress.progress= newProgess/ totalPlayTime;
    //    totalPlayTime= totalPlayTime - 1.0;
    // upate the UIProgress
    NSInteger playTime = self.audioPlayer.currentTime;
    
    
    
    NSString *seconds = playTime%60 < 10 ? [NSString stringWithFormat:@"0%ld",playTime%60]  :[NSString stringWithFormat:@"%ld",playTime%60];
    lbltimer.text = [NSString stringWithFormat:@"0%ld:%@",playTime/60,seconds];
    
    
    //lbltimer.text=[NSString stringWithFormat:@"%ld",(long)playTime];
    
    self.audioProgress.progress= timeLeft;
}



- (IBAction)audioStop:(id)sender
{
    [self.audioRecordProgress setHidden:YES];
    [self.audioProgress setHidden:YES];
    
    [self.Recordtimer invalidate];
    self.Recordtimer = nil;
    totalPlayTime = totaltime;
    
    totaltime = 0.0;
    
    [self.timer invalidate];
    self.timer = nil;
    
    
    lbltimer.text=@"00.00";
    
    
    [self.audioPlayer stop];
    [self.audioRecorder stop];
    
    audioFileURL = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingString:@"audioRecording.m4a"]];
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audioFileURL error:nil];
    [self updateAudioPlayerLayout:false];
    [audioPlayButton setSelected: false];
    self.audioPlayer.delegate = self;
    [self getAudioFileSize:audioFileURL];
    
    
}

-(void)getAudioFileSize:(NSURL*)fileURL
{
    
    
    
    NSNumber *fileSizeValue = nil;
    NSError *fileSizeError = nil;
    [fileURL getResourceValue:&fileSizeValue
                       forKey:NSURLFileSizeKey
                        error:&fileSizeError];
    if (fileSizeValue) {
        NSLog(@"value for %@ is %@", fileURL, fileSizeValue);
    }
    else {
        NSLog(@"error getting size for url %@ error was %@", fileURL, fileSizeError);
        
    }
    
    
    NSLog(@"downloadfile size %@", [self readableValueWithBytes:fileSizeValue]);
    
    NSString *filesizeString=[NSString stringWithFormat:@"%@",[self readableValueWithBytes:fileSizeValue]];
    
    if ([filesizeString rangeOfString:@"MB"].location == NSNotFound) {
        NSLog(@"filesizeString does not contain MB");
    } else {
        NSLog(@"filesizeString contains MB!");
        NSString *str=[filesizeString stringByReplacingOccurrencesOfString:@"MB" withString:@""];
        
        str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        int mbValue=[str intValue];
        if (mbValue>10)
        {
            
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"File Size More then 10 MB to attach" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [alertView show];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            NSLog(@"value of fileURL=%@",[fileURL path]);
            [fileManager removeItemAtPath:[fileURL path]  error:NULL];
            
        }
    }
    
    
    
    
}

- (NSString *)readableValueWithBytes:(id)bytes
{
    NSString *readable;
    
    
    //round bytes to one kilobyte, if less than 1024 bytes
    if (([bytes longLongValue] < 1024)){
        
        unsigned long long b = ([bytes longLongValue]);
        float a = (float) b;
        readable = [NSString stringWithFormat:@"%.02f KB", a];
        
        //readable = [NSString stringWithFormat:@"1 KB"];
    }
    
    //kilobytes
    if (([bytes longLongValue]/1024)>=1){
        
        
        
        //readable = [NSString stringWithFormat:@"%lld KB", ([bytes longLongValue]/1024)];
        
        unsigned long long b = ([bytes longLongValue]/1024);
        float a = (float) b;
        
        readable = [NSString stringWithFormat:@"%.02f KB", a];
        
    }
    
    //megabytes
    if (([bytes longLongValue]/1024/1024)>=1){
        
        //  readable = [NSString stringWithFormat:@"%lld MB", ([bytes longLongValue]/1024/1024)];
        
        
        unsigned long long b = ([bytes longLongValue]/1024/1024);
        float a = (float) b;
        
        readable = [NSString stringWithFormat:@"%.02f MB", a];
        
    }
    
    return readable;
}



- (IBAction)audioPlay:(UIButton*)sender
{
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    if (sender.isSelected) {
        NSLog(@"cuurent Time Audio Player --%f", self.audioPlayer.currentTime);
        
        if (self.audioPlayer.playing) {
            [self.audioPlayer pause];
            [session setActive:false error:nil];
            [self.timer invalidate];
            self.timer = nil;
            [sender setImage:[UIImage imageNamed:@"ic_play_grey.png"] forState:UIControlStateSelected];
            [self.audioPlayer updateMeters];
            
        }else {
            self.timer = nil;
            self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                          target:self
                                                        selector:@selector(updateProgress)
                                                        userInfo:nil
                                                         repeats:YES];
            [session setActive:true error:nil];
            [sender setImage:[UIImage imageNamed:@"pauseRecord"] forState:UIControlStateSelected];
            // [self.audioPlayer updateMeters];
            //[self.audioPlayer playAtTime:self.audioPlayer.currentTime];
            [self.audioPlayer play];
        }
        
    }
    else
    {
        sender.selected = !sender.isSelected;
        [self.Recordtimer invalidate];
        self.Recordtimer = nil;
        [self.timer invalidate];
        self.timer = nil;
        [self.audioProgress setHidden:NO];
        [self.audioRecordProgress setHidden:YES];
        
        [session setActive:true error:nil];
        [session setCategory:AVAudioSessionCategoryPlayback error:nil];
        
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                      target:self
                                                    selector:@selector(updateProgress)
                                                    userInfo:nil
                                                     repeats:YES];
        self.audioPlayer.meteringEnabled = true ;
        [self.audioPlayer prepareToPlay];
        [self.audioPlayer play];
        
    }
    
}

#pragma mark - Cancel & Done Button Actionn
- (IBAction)didTapCancelBtnAction:(UIButton *)sender {
    [self dismissViewWithDealloc];
    [self audioBase64ReturnToDept:nil];
    
}
- (IBAction)didTapDoneBtnAction:(UIButton *)sender
{
    [self bgTouch:sender];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - Audio Player Delegate
-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    if (!player.playing) {
        [self audioStop:nil];
    }
    
}
-(void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error {
    NSLog(@"error player --@%", error);
}
-(void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag {
    
}
@end
