//
//  XMPPMessageCoreDataObject.h
//  SimpleChat
//
//  Created by Sanjay Chauhan on 18/12/13.
//  Copyright (c) 2013 Sanjay Chauhan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class XMPPMessageUserCoreDataObject;

@interface XMPPMessageCoreDataObject : NSManagedObject

@property (nonatomic, retain) NSData * actualData;
@property (nonatomic, retain) NSString * body;
@property (nonatomic, retain) NSString * messageReceipant;
@property (nonatomic, retain) NSNumber * selfReplied;
@property (nonatomic, retain) NSString * sendDate;
@property (nonatomic, retain) NSData * thumbnail;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) XMPPMessageUserCoreDataObject *whoOwns;

@end
