//
//  XMPPPresence+AddStatus.h
//  SimpleChat
//
//  Created by Sanjay Chauhan on 12/1/14.
//  Copyright (c) 2014 Sanjay Chauhan. All rights reserved.
//

#import "XMPPPresence.h"

@interface XMPPPresence (AddStatus)
-(void) addStatus:(NSString*)status;
@end
