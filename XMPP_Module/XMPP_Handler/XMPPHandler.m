//
//  XMPPHandler.m
//  PanX_iPod
//
//  Created by Lokesh Jain on 12/12/13.
//  Copyright (c) 2013 Lokesh Jain. All rights reserved.
//

#import "XMPPHandler.h"
#import "XMPPPresence+AddStatus.h"
#import "XMPPMessage+CreateMessage.h"
#import "XMPPMessageCoreDataObject+AddMessage.h"
#import "XMPPMessageUserCoreDataObject+Update.h"
#import "XMPPRoster+AddUser.h"
#import "XMPPMessage+XEP_0085.h"
#import "XMPPMessage+XEP0045.h"
#import "AppDelegate.h"
NSString *const XXMPPmyJID = @"XXMPPmyJID";
NSString *const XXMPPmyPassword = @"XXMPPmyPassword";


@interface XMPPHandler()
{
    BOOL customCertEvaluation;
}


@property (nonatomic, strong) XMPPReconnect *xmppReconnect;
@property (nonatomic, strong) XMPPRoster *xmppRoster;
@property (nonatomic, strong) XMPPRosterCoreDataStorage *xmppRosterStorage;


@property (nonatomic, strong) XMPPCapabilities *xmppCapabilities;
@property (nonatomic, strong) XMPPCapabilitiesCoreDataStorage *xmppCapabilitiesStorage;
//@property (nonatomic, strong) XMPPvCardCoreDataStorage *xmppvCardStorage;

@property BOOL     allowSelfSignedCertificates;
@property BOOL allowSSLHostNameMismatch;

@property (nonatomic,strong)NSManagedObjectContext *managedObjectContext_roster;
@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext_capabilities;
@property (nonatomic,strong) UIManagedDocument *messageDocument;

@property (nonatomic,strong) NSString *sendingFilePath;




-(NSString*) getActualFileNameFromThumbnail:(NSString*)thumbnailFileName;

@end

@implementation XMPPHandler
@synthesize xmppStream = _xmppStream;
@synthesize xmppReconnect = _xmppReconnect;
@synthesize xmppRoster = _xmppRoster;
@synthesize xmppRosterStorage = _xmppRosterStorage;

@synthesize allowSelfSignedCertificates = _allowSelfSignedCertificates;
@synthesize allowSSLHostNameMismatch = _allowSSLHostNameMismatch;


@synthesize username = _username;
@synthesize password = _password;
@synthesize hostname = _hostname;

@synthesize delegate = _delegate;


@synthesize managedObjectContext_roster = _managedObjectContext_roster;
@synthesize managedObjectContext_capabilities = _managedObjectContext_capabilities;
@synthesize messageDocument = _messageDocument;

@synthesize sendingFilePath = _sendingFilePath;
@synthesize mPresenceArray;


#pragma mark --- SETUP STREAM METHOD ---
-(void) setupStream
{
    if (!self.xmppStream)
    {
        self.xmppStream = [[XMPPStream alloc] init];
    }
    
    self.xmppStream.hostName = self.hostname;
    NSLog(@"id is %@",self.hostname);
    
    mPresenceArray=[[NSMutableArray alloc]init];
    
    self.xmppStream.startTLSPolicy = XMPPStreamStartTLSPolicyRequired;
    
    customCertEvaluation = YES;
    
#if !TARGET_IPHONE_SIMULATOR
    {
        // Want xmpp to run in the background?
        //
        // P.S. - The simulator doesn't support backgrounding yet.
        //        When you try to set the associated property on the simulator, it simply fails.
        //        And when you background an app on the simulator,
        //        it just queues network traffic til the app is foregrounded again.
        //        We are patiently waiting for a fix from Apple.
        //        If you do enableBackgroundingOnSocket on the simulator,
        //        you will simply see an error message from the xmpp stack when it fails to set the property.
        
        self.xmppStream.enableBackgroundingOnSocket = YES;
    }
#endif
    
    
    if (!self.xmppReconnect)
    {
        self.xmppReconnect = [[XMPPReconnect alloc] init];
    }
    
    if (!self.xmppRosterStorage)
    {
        self.xmppRosterStorage = [[XMPPRosterCoreDataStorage alloc] init];
    }
    
    
    if (!self.xmppRoster)
    {
        self.xmppRoster = [[XMPPRoster alloc] initWithRosterStorage:self.xmppRosterStorage];
    }
    
    self.xmppRoster.autoFetchRoster = YES;
    self.xmppRoster.autoAcceptKnownPresenceSubscriptionRequests = YES;
    
    // Setup vCard support
    //
    // The vCard Avatar module works in conjuction with the standard vCard Temp module to download user avatars.
    // The XMPPRoster will automatically integrate with XMPPvCardAvatarModule to cache roster photos in the roster.
    
    /**-------------------------------------------------------------------------------------
     INITIALIZING XMPP VCARD STORAGE AND MODULE
     ---------------------------------------------------------------------------------------*/
    self.xmppvCardStorage = [XMPPvCardCoreDataStorage sharedInstance];
    self.xmppvCardTempModule = [[XMPPvCardTempModule alloc] initWithvCardStorage:self.xmppvCardStorage];
    self.xmppvCardAvatarModule = [[XMPPvCardAvatarModule alloc] initWithvCardTempModule:self.xmppvCardTempModule];
    
    
    // Setup capabilities
    //
    // The XMPPCapabilities module handles all the complex hashing of the caps protocol (XEP-0115).
    // Basically, when other clients broadcast their presence on the network
    // they include information about what capabilities their client supports (audio, video, file transfer, etc).
    // But as you can imagine, this list starts to get pretty big.
    // This is where the hashing stuff comes into play.
    // Most people running the same version of the same client are going to have the same list of capabilities.
    // So the protocol defines a standardized way to hash the list of capabilities.
    // Clients then broadcast the tiny hash instead of the big list.
    // The XMPPCapabilities protocol automatically handles figuring out what these hashes mean,
    // and also persistently storing the hashes so lookups aren't needed in the future.
    //
    // Similarly to the roster, the storage of the module is abstracted.
    // You are strongly encouraged to persist caps information across sessions.
    //
    // The XMPPCapabilitiesCoreDataStorage is an ideal solution.
    // It can also be shared amongst multiple streams to further reduce hash lookups.
    
    self.xmppCapabilitiesStorage = [XMPPCapabilitiesCoreDataStorage sharedInstance];
    self.xmppCapabilities = [[XMPPCapabilities alloc] initWithCapabilitiesStorage:self.xmppCapabilitiesStorage];
    
    self.xmppCapabilities.autoFetchHashedCapabilities = YES;
    self.xmppCapabilities.autoFetchNonHashedCapabilities = NO;
    
    //Activate modules
    [self.xmppReconnect         activate:self.xmppStream];
    [self.xmppRoster            activate:self.xmppStream];
    [self.xmppvCardTempModule   activate:self.xmppStream];
    [self.xmppvCardAvatarModule activate:self.xmppStream];
    [self.xmppCapabilities      activate:self.xmppStream];
    
    [self.xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()];
    [self.xmppRoster addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    
    /**-------------------------------------------------------------------------------------
     IMPLEMENTING XMPP MESSAGE ARCHIVING
     ---------------------------------------------------------------------------------------*/
    
    if (!self.xmppMessageArchivingCoreDataStorage)
    {
        self.xmppMessageArchivingCoreDataStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    }
    
    if (!self.xmppMessageArchivingModule)
    {
        self.xmppMessageArchivingModule = [[XMPPMessageArchiving alloc]initWithMessageArchivingStorage:self.xmppMessageArchivingCoreDataStorage];
    }
    
    [self.xmppMessageArchivingModule setClientSideMessageArchivingOnly:NO];
    [self.xmppMessageArchivingModule activate:self.xmppStream];
    [self.xmppMessageArchivingModule addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    
    /**-------------------------------------------------------------------------------------
     IMPLEMENTING XMPPPRIVACY (BLOCK USER)
     ---------------------------------------------------------------------------------------*/
    self.xmppPrivacy = [[XMPPPrivacy alloc] initWithDispatchQueue:dispatch_get_main_queue()];
    //Activate xmpp modules
    [self.xmppPrivacy setAutoRetrievePrivacyListItems:YES];
    [self.xmppPrivacy setAutoRetrievePrivacyListNames:YES];
    //[self.xmppPrivacy addDelegate:self delegateQueue:dispatch_get_main_queue()];
    [self.xmppPrivacy activate:self.xmppStream];
    
    [self.xmppStream setHostPort:5333];
    self.allowSelfSignedCertificates = YES;
    self.allowSSLHostNameMismatch = NO;
    
    
    [self getmanagedObjectMessage];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goOnline) name:@"saveStatus" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(messageIsSending:) name:@"messageSent" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fileIsSending:) name:@"fileSent" object:nil];
    
}

#pragma mark --- STREAM CONNECT METHOD ---
-(BOOL)connect
{
    
    //[self.xmppStream setMyJID:[XMPPJID jidWithString:self.username]];
    
    
    if (![self.xmppStream isDisconnected]) {
        return YES;
    }
    
    NSString *myJID = [[NSUserDefaults standardUserDefaults] stringForKey:XXMPPmyJID];
    NSString *myPassword = [[NSUserDefaults standardUserDefaults] stringForKey:XXMPPmyPassword];
    
    //
    // If you don't want to use the Settings view to set the JID,
    // uncomment the section below to hard code a JID and password.
    //
    // myJID = @"user@gmail.com/xmppframework";
    // myPassword = @"";
    
    if (myJID == nil || myPassword == nil) {
        return NO;
    }
    
    [self.xmppStream setMyJID:[XMPPJID jidWithString:myJID]];
    self.password = myPassword;
    
    NSError *error = nil;
    
    
    if (![self.xmppStream oldSchoolSecureConnectWithTimeout:XMPPStreamTimeoutNone error:&error])
    {
        NSLog(@"%@",error.description);
        
        return NO;
    }
    
    /*if (![self.xmppStream connectWithTimeout:XMPPStreamTimeoutNone error:&error])
     {
     UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error connecting"
     message:@"See console for error details."
     delegate:nil
     cancelButtonTitle:@"Ok"
     otherButtonTitles:nil];
     [alertView show];
     
     //DDLogError(@"Error connecting: %@", error);
     
     return NO;
     }*/
    return YES;
}

- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq
{
    NSXMLElement *queryElement = [iq elementForName: @"query" xmlns: @"http://jabber.org/protocol/disco#items"];
    NSLog(@"queryelement %@",queryElement);
    if (queryElement) {
        NSLog(@"query in if");
        NSArray *itemElements = [queryElement elementsForName: @"item"];
        NSMutableArray *mArray = [[NSMutableArray alloc] init];
        for (int i=0; i<[itemElements count]; i++) {
            NSString *jid=[[[itemElements objectAtIndex:i] attributeForName:@"jid"] stringValue];
            [mArray addObject:jid];
            NSLog(@"User in OpenFire = %@",jid);
        }
        NSLog(@"mArray = %@",mArray);
    }
    return NO;
}


#pragma mark --- TEARDOWN STREAM METHOD #ON LOGOUT #ON DISCONNECT OR ERROR ---
- (void)teardownStream
{
    [self.xmppStream disconnect];
    [self.xmppStream removeDelegate:self];
    
    self.isXmppConnected = NO;
    
    self.xmppStream = nil;
    
    [self.xmppReconnect         deactivate];
    [self.xmppRoster            deactivate];
    self.xmppReconnect = nil;
    self.xmppRoster = nil;
    self.xmppRosterStorage = nil;
    self.xmppvCardStorage = nil;
    self.xmppvCardTempModule = nil;
    self.xmppvCardAvatarModule = nil;
    self.xmppCapabilities = nil;
    self.xmppCapabilitiesStorage = nil;
    self.xmppPrivacy=nil;
    self.xmppMessageArchivingModule=nil;
    
    
    [self.xmppMessageArchivingModule removeDelegate:self];
    [self.xmppMessageArchivingModule deactivate];
    self.xmppMessageArchivingModule = nil;
    self.xmppMessageArchivingCoreDataStorage = nil;
    
    
    
    [self.xmppRoster removeDelegate:self];
    
    
    [self.xmppvCardTempModule   deactivate];
    [self.xmppvCardAvatarModule deactivate];
    [self.xmppCapabilities      deactivate];
    [self.xmppPrivacy           deactivate];
    [self.xmppMessageArchivingModule deactivate];
    
    
    [self clearLocalRosterData];
    
    
    //self.managedObjectContext_roster = nil;
}

-(XMPPJID *) getMyJid
{
    return [self.xmppStream myJID];
}


-(void) disconnect
{
    if (self.xmppStream.isConnected) {
        [self.xmppStream disconnect];
    }
}

- (void)goOnline
{
    XMPPPresence *presence = [XMPPPresence presence]; // type="available" is implicit
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *presenceStat = [prefs stringForKey:@"avatarChatStatus"];
    if (!presenceStat) {
        presenceStat = @"Available";
    }
    [presence addStatus:presenceStat];
    
    
    [[self xmppStream] sendElement:presence];
}

- (void)goOffline
{
    XMPPPresence *presence = [XMPPPresence presenceWithType:@"unavailable"];
    
    [[self xmppStream] sendElement:presence];
    
}

- (void)clearLocalRosterData {
    
    [[[self xmppRoster] xmppRosterStorage] clearAllUsersAndResourcesForXMPPStream:self.xmppStream];
    
    [self clearData];
}

- (void)clearData
{
    NSManagedObjectContext *moc = [self getManagedObjectRoster];
    if (moc == nil) {
        return;
    }
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject"
                                              inManagedObjectContext:moc];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    [fetchRequest setFetchBatchSize:0];
    
    NSArray *allUsers = [[self getManagedObjectRoster] executeFetchRequest:fetchRequest error:nil];
    
    for (XMPPUserCoreDataStorageObject *user in allUsers)
    {
        [[self getManagedObjectRoster] deleteObject:user];
    }
    
    [self.getManagedObjectRoster save:nil];
}

////////////////////////////////////
/////XMPP STREAM DELEGATES/////////
//////////////////////////////////
-(void)xmppStream:(XMPPStream *)sender socketDidConnect:(GCDAsyncSocket *)socket {
    NSLog(@"Socket Connected");
}

- (void)xmppStream:(XMPPStream *)sender willSecureWithSettings:(NSMutableDictionary *)settings
{
    //DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    
    [settings setObject:self.xmppStream.myJID.domain forKey:(NSString *)kCFStreamSSLPeerName];
    
    if (customCertEvaluation)
    {
        [settings setObject:@(YES) forKey:GCDAsyncSocketManuallyEvaluateTrust];
    }
    
    
    //    if (self.allowSelfSignedCertificates)
    //    {
    //        [settings setObject:[NSNumber numberWithBool:YES] forKey:(NSString *)kCFStreamSSLAllowsAnyRoot];
    //    }
    
    if (self.allowSSLHostNameMismatch)
    {
        [settings setObject:[NSNull null] forKey:(NSString *)kCFStreamSSLPeerName];
    }
    else
    {
        // Google does things incorrectly (does not conform to RFC).
        // Because so many people ask questions about this (assume xmpp framework is broken),
        // I've explicitly added code that shows how other xmpp clients "do the right thing"
        // when connecting to a google server (gmail, or google apps for domains).
        
        NSString *expectedCertName = nil;
        
        NSString *serverDomain = self.xmppStream.hostName;
        NSString *virtualDomain = [self.xmppStream.myJID domain];
        
        if ([serverDomain isEqualToString:@"talk.google.com"])
        {
            if ([virtualDomain isEqualToString:@"gmail.com"])
            {
                expectedCertName = virtualDomain;
            }
            else
            {
                expectedCertName = serverDomain;
            }
        }
        else if (serverDomain == nil)
        {
            expectedCertName = virtualDomain;
        }
        else
        {
            expectedCertName = serverDomain;
        }
        
        if (expectedCertName)
        {
            [settings setObject:expectedCertName forKey:(NSString *)kCFStreamSSLPeerName];
        }
    }
}

- (void)xmppStreamDidSecure:(XMPPStream *)sender
{
    //DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
}

-(void)xmppStreamDidConnect:(XMPPStream *)sender
{
    NSLog(@"XMPP Server Baglantisi Kuruldu");
    self.isXmppConnected = YES;
    NSError *error;
    if ([self.xmppStream authenticateWithPassword:self.password error:&error]) {
        NSLog(@"Authentication Username: %@ Pass: %@ ",self.username,self.password);
    }
    
}



-(void)xmppStream:(XMPPStream *)sender didNotAuthenticate:(DDXMLElement *)error
{
    NSLog(@"Not Authenticated: %@",[error description]);
    [self.delegate setXMPPHandlerConnectionStatus:NO];
}


-(void)xmppStreamDidDisconnect:(XMPPStream *)sender withError:(NSError *)error {
    
    
    NSLog(@"Error Connecting Stream %@",[error description]);
    NSLog(@"error Code %ld",error.code);
    
    if (self.isXmppConnected)
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"N" forKey:@"ChatSession"];
        [self teardownStream];
        
        NSLog(@"END CHAT PRESSED");
        // [self.delegate chatSessionExpiredWithStatus:NO];
        
    }
}

-(void)xmppStreamDidAuthenticate:(XMPPStream *)sender {
    //NSLog(@"Authentication Basarili");
    NSLog(@"ISConnection Secured %d",sender.isSecure);
    [self goOnline];
    [self.delegate setXMPPHandlerConnectionStatus:YES];
}

-(void) xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence
{
    NSLog(@"Presence Received From: %@ Type: %@",[presence fromStr],[presence type]);
    
    
    if([mPresenceArray count] > 0) {
        
        [[mPresenceArray mutableCopy] enumerateObjectsUsingBlock:^(id tempDict, NSUInteger idx, BOOL *stop) {
            NSLog(@"value is here %@",tempDict);
            NSLog(@"value mPresenceArray is here %@",mPresenceArray);
            
            if([[tempDict valueForKey:@"OtherUser"]isEqualToString:[presence fromStr]]) {
                NSLog(@"Status is here %@",[tempDict valueForKey:@"Status"]);
                [mPresenceArray removeObjectAtIndex:idx];
                NSLog(@"mPresenceArray is here %@",mPresenceArray);
                
            }
        }];
    }
    NSMutableDictionary *mUserStatusDict = [[NSMutableDictionary alloc] init];
    [mUserStatusDict setValue:[presence type] forKey:@"Status"];
    [mUserStatusDict setValue:[presence fromStr] forKey:@"OtherUser"];
    [self.mPresenceArray addObject:mUserStatusDict];
    mUserStatusDict = nil;
    
    //    NSString *presenceString=[presence fromStr];
    //    NSString *string = @"@company.com";
    /*
     //NSLog(@"Presence Received From: %@ Type: %@",[presence fromStr],[presence type]);
     NSString *status=@"Unknown";
     if (!presence.status) {
     NSLog(@"Presence Status Nil");
     }
     else {
     NSLog(@"Presence Status: %@",presence.status);
     status = [presence status];
     
     }
     [self.delegate presenceStatusChanged:presence.from withStatus:status];
     */
}

- (void)xmppStream:(XMPPStream *)sender didReceiveTrust:(SecTrustRef)trust completionHandler:(void (^)(BOOL shouldTrustPeer))completionHandler
{
    /* Custom validation for your certificate on server should be performed */
    
    completionHandler(YES); // After this line, SSL connection will be established
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark XMPPRosterDelegate
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)xmppRoster:(XMPPRoster *)sender didReceiveBuddyRequest:(XMPPPresence *)presence
{
    //DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    XMPPUserCoreDataStorageObject *user = [self.xmppRosterStorage userForJID:[presence from]
                                                                  xmppStream:self.xmppStream
                                                        managedObjectContext:[self managedObjectContext_roster]];
    
    NSString *displayName = [user displayName];
    NSString *jidStrBare = [presence fromStr];
    NSString *body = nil;
    
    if (![displayName isEqualToString:jidStrBare])
    {
        body = [NSString stringWithFormat:@"Buddy request from %@ <%@>", displayName, jidStrBare];
    }
    else
    {
        body = [NSString stringWithFormat:@"Buddy request from %@", displayName];
    }
    
    
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:displayName
                                                            message:body
                                                           delegate:nil
                                                  cancelButtonTitle:@"Not implemented"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
    else
    {
        // We are not active, so use a local notification instead
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        localNotification.alertAction = @"Not implemented";
        localNotification.alertBody = body;
        
        [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
    }
    
}

-(void) xmppRoster:(XMPPRoster *)sender didReceivePresenceSubscriptionRequest:(XMPPPresence *)presence
{
    NSLog(@"Presence Request");
}
-(NSString*)unescapeHtmlCodes:(NSString*)input
{
    
    NSRange rangeOfHTMLEntity = [input rangeOfString:@"&#"];
    if( NSNotFound == rangeOfHTMLEntity.location ) {
        return input;
    }
    
    
    NSMutableString* answer = [[NSMutableString alloc] init];
    
    NSScanner* scanner = [NSScanner scannerWithString:input];
    [scanner setCharactersToBeSkipped:nil]; // we want all white-space
    
    while( ![scanner isAtEnd] ) {
        
        NSString* fragment;
        [scanner scanUpToString:@"&#" intoString:&fragment];
        if( nil != fragment ) { // e.g. '&#38; B'
            [answer appendString:fragment];
        }
        
        if( ![scanner isAtEnd] ) { // implicitly we scanned to the next '&#'
            
            int scanLocation = (int)[scanner scanLocation];
            [scanner setScanLocation:scanLocation+2]; // skip over '&#'
            
            int htmlCode;
            if( [scanner scanInt:&htmlCode] ) {
                char c = htmlCode;
                [answer appendFormat:@"%c", c];
                
                scanLocation = (int)[scanner scanLocation];
                [scanner setScanLocation:scanLocation+1]; // skip over ';'
                
            } else {
                // err ?
            }
        }
        
    }
    
    return answer;
    
}
-(void)updateBadgeIconWithLocalNotification:(XMPPMessage*)message {
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground)
    {
        NSLog(@"localNotification Recieved localNotification %@",message);
        if ([[message elementForName:@"body"] stringValue] != nil) {
            NSString *body = [[message elementForName:@"body"] stringValue];
            NSArray *stringArray = [body componentsSeparatedByString:@"~~~"];
            NSString *message = [self unescapeHtmlCodes:stringArray.firstObject];
            
            //  NSString *displayName = [user displayName];
            // NSString *sentTime = [currentTime description];
            UILocalNotification *localNotification = [[UILocalNotification alloc] init];
            localNotification.alertAction = @"Ok";
            localNotification.category = @"chat";
            localNotification.fireDate = [[NSDate alloc]init];
            localNotification.applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
            localNotification.alertBody = [NSString stringWithFormat:@"%@",message];
            
            [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
            
            [UIApplication sharedApplication].applicationIconBadgeNumber += 1;
            // [[UIApplication sharedApplication]cancelAllLocalNotifications];
        }
    }
    else if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive)
    {
        if ( [[message elementForName:@"body"] stringValue] != nil) {
            SharedManager *shared = [SharedManager sharedSingleton];
            NSString *oldBadge = shared.chatBadgeCount;
            NSUInteger newBadge = [oldBadge integerValue] + 1 ;
            shared.chatBadgeCount = [NSString stringWithFormat:@"%lu",(unsigned long)newBadge];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CHAT_MESSAGE object:nil];
        }
    }
    NSLog(@"chat array ---%@", [AppDelegate sharedDelegate].chatBotLoad);
    if ([[message elementForName:@"body"] stringValue] != nil)
    {
        NSDate *currentTime = [NSDate date];
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat: @"hh:mm a"];
        NSString *fetchtime = [dateFormatter
                               stringFromDate:currentTime]; //Put whatever date you want
        NSMutableDictionary *dic=[NSMutableDictionary new];
        NSString *body = [[message elementForName:@"body"] stringValue];
        NSArray *stringArray = [body componentsSeparatedByString:@"~~~"];
        NSString *message = [self unescapeHtmlCodes:stringArray.firstObject];
        [dic setObject:@"agent" forKey:@"agentId"];
        [dic setObject:message forKey:@"msg"];
        [dic setObject:fetchtime forKey:@"time"];
        [[AppDelegate sharedDelegate].chatHistoryLoad addObject:dic];
        //[APP_DELEGATE.chatBotLoad addObject:dic];
        
    }
}
- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message
{
    //DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    // A simple example of inbound message handling.
    NSLog(@"Message Recieved is %@",message);
    XMPPUserCoreDataStorageObject *user = [self.xmppRosterStorage userForJID:[message from]
                                                                  xmppStream:self.xmppStream
                                                        managedObjectContext:[self getManagedObjectRoster]];
    
    self.isAgentTyping = NO;
    
    NSDate *currentTime = [NSDate date];
    if([message hasChatState] && ![message isErrorMessage])
    {
        
        if ([message hasComposingChatState])
        {
            self.isAgentTyping = YES;
        }
        else if ([message hasPausedChatState])
        {
            self.isAgentTyping = NO;
        }
        
        NSLog(@"Is Typing");
        
    }
    [self updateBadgeIconWithLocalNotification:message];
    
    if ([message isActualChatMessage])
    {
        NSLog(@"This is Chat Message");
        //[self.delegate reloadTableRows];
        NSString *body = [[message elementForName:@"body"] stringValue];
        NSString *displayName = [user displayName];
        NSString *sentTime = [currentTime description];
        
        
        //Alttaki iki satir direk jid stringi aldigimizda gelen sacma karakterleri elemine etmek icindir
        XMPPJID *receipantID = [message to];
        NSString *fullReceipantID = [[receipantID user] stringByAppendingString:@"@"];
        NSString *receipantStr = [fullReceipantID stringByAppendingString:[receipantID domain]];
        
        //Alttaki iki satir direk jid stringi aldigimizda gelen sacma karakterleri elemine etmek icindir
        XMPPJID *messageFrom = [message from];
        NSString *fullJid = [[messageFrom user] stringByAppendingString:@"@"];
        NSString *jidStr = [fullJid stringByAppendingString:[messageFrom domain]];
        
        if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive)
        {
            /*
             UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:displayName
             message:body
             delegate:nil
             cancelButtonTitle:@"Ok"
             otherButtonTitles:nil];
             [alertView show];
             */
        }
        else
        {
            // We are not active, so use a local notification instead
            UILocalNotification *localNotification = [[UILocalNotification alloc] init];
            localNotification.alertAction = @"Ok";
            localNotification.alertBody = [NSString stringWithFormat:@"From: %@\n\n%@ At:%@\n\n",displayName,body,sentTime];
            
            [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
        }
        
        
        
        XMPPMessageCoreDataObject *messageCoreData = [XMPPMessageCoreDataObject insertMessageWithBody:body andSendDate:sentTime andMessageReceipant:receipantStr withType:@"chat" withThumbnail:nil withActualData:nil includingUserJid:jidStr andUserDisplay:displayName inManagedObjectContext:[self getmanagedObjectMessage] withSelfRepliedStatus:[NSNumber numberWithInt:0]];
        
        
        
        if (!messageCoreData) {
            NSLog(@"No Message Data");
        }
        
    }
    else if ([message isImageMessage])
    {
        NSArray *imageMessage = [message getFileMessageContent];
        NSString *thumbnailPath = [imageMessage objectAtIndex:0];
        NSString *actualDataPath = [imageMessage objectAtIndex:1];
        NSLog(@"Image Message Thumbnail: %@  ActualData: %@",thumbnailPath,actualDataPath);
        
        if (thumbnailPath && actualDataPath)
        {
            //NSString *localDir = [NSString stringWithFormat:@"%@-%@",user.jidStr,self.username];
            //[self.ftpHandler downloadFile:thumbnailPath inFolder:localDir withType:@"thumbnail" fromUser:user.jidStr];
        }
        
    }
    else if ([message isAudioMessage])
    {
        NSArray *audioMessage = [message getFileMessageContent];
        NSString *actualDataPath = [audioMessage objectAtIndex:1];
        //NSLog(@"Audio Message Geldi ActualDataPath: %@",actualDataPath);
        
        if (actualDataPath) {
            
            //Gelen datayi local olarak nereye koyacagini ayarlar
            NSString *localDir = [NSString stringWithFormat:@"%@-%@",user.jidStr,self.username];
            NSString *fileName = [actualDataPath lastPathComponent];
            NSArray *documentsTmpPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsPath = [documentsTmpPath objectAtIndex:0];
            NSString *outputDirPath = [documentsPath stringByAppendingPathComponent:localDir];
            NSString *fullLocalPath = [outputDirPath stringByAppendingPathComponent:fileName];
            //[self.ftpHandler addLocalFile:fullLocalPath forRemoteFile:actualDataPath];
            
            //Gelen dataya uygun thumbnaili ayarlar
            NSString *thumbnailPath = [[NSBundle mainBundle] pathForResource:@"mic" ofType:@"png"];
            NSData *thumbnailData = [NSData dataWithContentsOfFile:thumbnailPath];
            NSString *recipantStr = [[message to] bare];
            NSString *fromJidStr = [[message from] bare];
            
            //Gelen data ile ilgili bilgiyi database e yazar
            XMPPMessageCoreDataObject *messageCoreData = [XMPPMessageCoreDataObject insertMessageWithBody:fullLocalPath andSendDate:[currentTime description] andMessageReceipant:recipantStr withType:@"audio" withThumbnail:thumbnailData withActualData:nil includingUserJid:fromJidStr andUserDisplay:user.displayName inManagedObjectContext:[self getmanagedObjectMessage] withSelfRepliedStatus:[NSNumber numberWithInt:0]];
            
            if (!messageCoreData) {
                NSLog(@"No Message Data");
            }
            
        }
    }
    else if ([message isCoordMessage])
    {
        NSLog(@"Coordinate Message");
        NSArray *coordMessage = [message getCoordMessageContent];
        NSString *latStr = [coordMessage objectAtIndex:0];
        NSString *lonStr = [coordMessage objectAtIndex:1];
        NSString *messageStr = [latStr stringByAppendingFormat:@",%@",lonStr];
        NSString *recipantStr = [[message to] bare];
        NSString *fromJidStr = [[message from] bare];
        
        NSString *iconPath = [[NSBundle mainBundle] pathForResource:@"googlemaps" ofType:@"png"];
        NSData *iconData = [NSData dataWithContentsOfFile:iconPath];
        
        XMPPMessageCoreDataObject *messageCoreData = [XMPPMessageCoreDataObject insertMessageWithBody:messageStr andSendDate:[currentTime description] andMessageReceipant:recipantStr withType:@"coordinate" withThumbnail:iconData withActualData:nil includingUserJid:fromJidStr andUserDisplay:user.displayName inManagedObjectContext:[self getmanagedObjectMessage] withSelfRepliedStatus:[NSNumber numberWithInt:0]];
        
        if (!messageCoreData) {
            NSLog(@"No Message Data");
        }
        
    }
    else if ([message isContactMessage])
    {
        NSLog(@"Contact Message");
        NSArray *contactMessage = [message getContactMessageContent];
        NSString *firstName = [contactMessage objectAtIndex:0];
        NSString *lastName = [contactMessage objectAtIndex:1];
        NSString *mobileNo = [contactMessage objectAtIndex:2];
        NSString *iphoneNo = [contactMessage objectAtIndex:3];
        NSString *messageStr = [NSString stringWithFormat:@"%@,%@,%@,%@",firstName,lastName,mobileNo,iphoneNo];
        
        NSString *recipantStr = [[message to] bare];
        NSString *fromJidStr = [[message from] bare];
        
        NSString *iconPath = [[NSBundle mainBundle] pathForResource:@"adressbook" ofType:@"png"];
        NSData *iconData = [NSData dataWithContentsOfFile:iconPath];
        
        XMPPMessageCoreDataObject *messageCoreData = [XMPPMessageCoreDataObject insertMessageWithBody:messageStr andSendDate:[currentTime description] andMessageReceipant:recipantStr withType:@"contact" withThumbnail:iconData withActualData:nil includingUserJid:fromJidStr andUserDisplay:user.displayName inManagedObjectContext:[self getmanagedObjectMessage] withSelfRepliedStatus:[NSNumber numberWithInt:0]];
        
        if (!messageCoreData) {
            NSLog(@"Gelen Contact Mesaj DB ye Eklenemedi");
        }
        
    }
    
    
    NSString *body = [[message elementForName:@"body"] stringValue];
    
    if ([[message elementForName:@"body"] stringValue] != nil)
    {
        NSArray *stringArray = [body componentsSeparatedByString:@"~~~"];
        NSString *agentId = stringArray.count >= 3 ? [stringArray objectAtIndex:2]:@"";
        
        self.retreivedAgentId = agentId;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ISTYPINGIDENTIFIER"  object:self];
    
    NSLog(@"Message Received From: %@",[[message from] full]);
    
}
/*
 - (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message
 {
 //DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
 
 // A simple example of inbound message handling.
 NSLog(@"Message Recieved is %@",message);
 XMPPUserCoreDataStorageObject *user = [self.xmppRosterStorage userForJID:[message from]
 xmppStream:self.xmppStream
 managedObjectContext:[self getManagedObjectRoster]];
 
 NSDate *currentTime = [NSDate date];
 if([message hasChatState] && ![message isErrorMessage])
 {
 
 NSString *body = [[message elementForName:@"body"] stringValue];
 
 NSArray *stringArray = [body componentsSeparatedByString:@"~~~"];
 NSString *agentId = stringArray.count >= 2 ? [stringArray objectAtIndex:2]:@"";
 
 self.retreivedAgentId = agentId;
 
 NSLog(@"Is Typing");
 
 return;
 }
 [self updateBadgeIconWithLocalNotification:message];
 
 if ([message isActualChatMessage])
 {
 NSLog(@"This is Chat Message");
 //[self.delegate reloadTableRows];
 NSString *body = [[message elementForName:@"body"] stringValue];
 NSString *displayName = [user displayName];
 NSString *sentTime = [currentTime description];
 
 
 //Alttaki iki satir direk jid stringi aldigimizda gelen sacma karakterleri elemine etmek icindir
 XMPPJID *receipantID = [message to];
 NSString *fullReceipantID = [[receipantID user] stringByAppendingString:@"@"];
 NSString *receipantStr = [fullReceipantID stringByAppendingString:[receipantID domain]];
 
 //Alttaki iki satir direk jid stringi aldigimizda gelen sacma karakterleri elemine etmek icindir
 XMPPJID *messageFrom = [message from];
 NSString *fullJid = [[messageFrom user] stringByAppendingString:@"@"];
 NSString *jidStr = [fullJid stringByAppendingString:[messageFrom domain]];
 
 if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive)
 {
 /*
 UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:displayName
 message:body
 delegate:nil
 cancelButtonTitle:@"Ok"
 otherButtonTitles:nil];
 [alertView show];
 
 }
 else
 {
 // We are not active, so use a local notification instead
 UILocalNotification *localNotification = [[UILocalNotification alloc] init];
 localNotification.alertAction = @"Ok";
 localNotification.alertBody = [NSString stringWithFormat:@"From: %@\n\n%@ At:%@\n\n",displayName,body,sentTime];
 
 [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
 }
 
 
 
 XMPPMessageCoreDataObject *messageCoreData = [XMPPMessageCoreDataObject insertMessageWithBody:body andSendDate:sentTime andMessageReceipant:receipantStr withType:@"chat" withThumbnail:nil withActualData:nil includingUserJid:jidStr andUserDisplay:displayName inManagedObjectContext:[self getmanagedObjectMessage] withSelfRepliedStatus:[NSNumber numberWithInt:0]];
 
 
 
 if (!messageCoreData) {
 NSLog(@"No Message Data");
 }
 
 }
 else if ([message isImageMessage])
 {
 NSArray *imageMessage = [message getFileMessageContent];
 NSString *thumbnailPath = [imageMessage objectAtIndex:0];
 NSString *actualDataPath = [imageMessage objectAtIndex:1];
 NSLog(@"Image Message Thumbnail: %@  ActualData: %@",thumbnailPath,actualDataPath);
 
 if (thumbnailPath && actualDataPath)
 {
 //NSString *localDir = [NSString stringWithFormat:@"%@-%@",user.jidStr,self.username];
 //[self.ftpHandler downloadFile:thumbnailPath inFolder:localDir withType:@"thumbnail" fromUser:user.jidStr];
 }
 
 }
 else if ([message isAudioMessage])
 {
 NSArray *audioMessage = [message getFileMessageContent];
 NSString *actualDataPath = [audioMessage objectAtIndex:1];
 //NSLog(@"Audio Message Geldi ActualDataPath: %@",actualDataPath);
 
 if (actualDataPath) {
 
 //Gelen datayi local olarak nereye koyacagini ayarlar
 NSString *localDir = [NSString stringWithFormat:@"%@-%@",user.jidStr,self.username];
 NSString *fileName = [actualDataPath lastPathComponent];
 NSArray *documentsTmpPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 NSString *documentsPath = [documentsTmpPath objectAtIndex:0];
 NSString *outputDirPath = [documentsPath stringByAppendingPathComponent:localDir];
 NSString *fullLocalPath = [outputDirPath stringByAppendingPathComponent:fileName];
 //[self.ftpHandler addLocalFile:fullLocalPath forRemoteFile:actualDataPath];
 
 //Gelen dataya uygun thumbnaili ayarlar
 NSString *thumbnailPath = [[NSBundle mainBundle] pathForResource:@"mic" ofType:@"png"];
 NSData *thumbnailData = [NSData dataWithContentsOfFile:thumbnailPath];
 NSString *recipantStr = [[message to] bare];
 NSString *fromJidStr = [[message from] bare];
 
 //Gelen data ile ilgili bilgiyi database e yazar
 XMPPMessageCoreDataObject *messageCoreData = [XMPPMessageCoreDataObject insertMessageWithBody:fullLocalPath andSendDate:[currentTime description] andMessageReceipant:recipantStr withType:@"audio" withThumbnail:thumbnailData withActualData:nil includingUserJid:fromJidStr andUserDisplay:user.displayName inManagedObjectContext:[self getmanagedObjectMessage] withSelfRepliedStatus:[NSNumber numberWithInt:0]];
 
 if (!messageCoreData) {
 NSLog(@"No Message Data");
 }
 
 }
 }
 else if ([message isCoordMessage])
 {
 NSLog(@"Coordinate Message");
 NSArray *coordMessage = [message getCoordMessageContent];
 NSString *latStr = [coordMessage objectAtIndex:0];
 NSString *lonStr = [coordMessage objectAtIndex:1];
 NSString *messageStr = [latStr stringByAppendingFormat:@",%@",lonStr];
 NSString *recipantStr = [[message to] bare];
 NSString *fromJidStr = [[message from] bare];
 
 NSString *iconPath = [[NSBundle mainBundle] pathForResource:@"googlemaps" ofType:@"png"];
 NSData *iconData = [NSData dataWithContentsOfFile:iconPath];
 
 XMPPMessageCoreDataObject *messageCoreData = [XMPPMessageCoreDataObject insertMessageWithBody:messageStr andSendDate:[currentTime description] andMessageReceipant:recipantStr withType:@"coordinate" withThumbnail:iconData withActualData:nil includingUserJid:fromJidStr andUserDisplay:user.displayName inManagedObjectContext:[self getmanagedObjectMessage] withSelfRepliedStatus:[NSNumber numberWithInt:0]];
 
 if (!messageCoreData) {
 NSLog(@"No Message Data");
 }
 
 }
 else if ([message isContactMessage])
 {
 NSLog(@"Contact Message");
 NSArray *contactMessage = [message getContactMessageContent];
 NSString *firstName = [contactMessage objectAtIndex:0];
 NSString *lastName = [contactMessage objectAtIndex:1];
 NSString *mobileNo = [contactMessage objectAtIndex:2];
 NSString *iphoneNo = [contactMessage objectAtIndex:3];
 NSString *messageStr = [NSString stringWithFormat:@"%@,%@,%@,%@",firstName,lastName,mobileNo,iphoneNo];
 
 NSString *recipantStr = [[message to] bare];
 NSString *fromJidStr = [[message from] bare];
 
 NSString *iconPath = [[NSBundle mainBundle] pathForResource:@"adressbook" ofType:@"png"];
 NSData *iconData = [NSData dataWithContentsOfFile:iconPath];
 
 XMPPMessageCoreDataObject *messageCoreData = [XMPPMessageCoreDataObject insertMessageWithBody:messageStr andSendDate:[currentTime description] andMessageReceipant:recipantStr withType:@"contact" withThumbnail:iconData withActualData:nil includingUserJid:fromJidStr andUserDisplay:user.displayName inManagedObjectContext:[self getmanagedObjectMessage] withSelfRepliedStatus:[NSNumber numberWithInt:0]];
 
 if (!messageCoreData) {
 NSLog(@"Gelen Contact Mesaj DB ye Eklenemedi");
 }
 
 }
 
 NSLog(@"Message Received From: %@",[[message from] full]);
 
 }
 */
-(void) xmppStream:(XMPPStream *)sender didSendMessage:(XMPPMessage *)message
{
    NSLog(@"We got Message");
    NSLog(@"Message is %@",message);
    
    NSLog(@"Message To %@",[message to]);
    XMPPUserCoreDataStorageObject *user = [self.xmppRosterStorage userForJID:[message to]
                                                                  xmppStream:self.xmppStream
                                                        managedObjectContext:[self getManagedObjectRoster]];
    
    NSDate *currentTime = [NSDate date];
    NSString *sentTime = [currentTime description];
    
    if ([message elementForName:@"PHOTO"])
    {
        
        //Get Image from base64String
        NSString *photoHash = [[message elementForName:@"PHOTO"] stringValue];
        NSData* data = [photoHash dataUsingEncoding:NSUTF8StringEncoding];
        NSData *nsdataDecoded = [data initWithBase64EncodedData:data options:0];
        
        //Here you can get image.
        UIImage *image = [UIImage imageWithData:nsdataDecoded];
        NSLog(@"%@",image);
    }
    
    if ([message isActualChatMessage])
    {
        
        /*   XMPPJID *receivingID = [message to];
         NSString *receiverName =[[receivingID user] stringByAppendingString:@"@"];
         NSString *receiverStr = [receiverName stringByAppendingString:[receivingID domain]];
         
         
         NSString *body = [[message elementForName:@"body"] stringValue];
         */
        
        /* XMPPMessageCoreDataObject *messageCoreData = [XMPPMessageCoreDataObject insertMessageWithBody:body andSendDate:sentTime andMessageReceipant:self.username withType:@"chat" withThumbnail:nil withActualData:nil includingUserJid:receiverStr andUserDisplay:user.displayName inManagedObjectContext:[self getmanagedObjectMessage] withSelfRepliedStatus:[NSNumber numberWithInt:1]];
         
         
         if (!messageCoreData) {
         NSLog(@"No Message Data");
         }*/
        
    }
    else if ([message isContactMessage])
    {
        NSArray *contactMessage = [message getCoordMessageContent];
        NSString *firstName = [contactMessage objectAtIndex:0];
        NSString *lastName = [contactMessage objectAtIndex:1];
        NSString *mobileNo = [contactMessage objectAtIndex:2];
        NSString *iphoneNo = [contactMessage objectAtIndex:3];
        NSString *messageStr = [NSString stringWithFormat:@"%@,%@,%@,%@",firstName,lastName,mobileNo,iphoneNo];
        
        NSString *iconPath = [[NSBundle mainBundle] pathForResource:@"adressbook" ofType:@"png"];
        NSData *iconData = [NSData dataWithContentsOfFile:iconPath];
        
        XMPPMessageCoreDataObject *messageCoreData = [XMPPMessageCoreDataObject insertMessageWithBody:messageStr andSendDate:sentTime andMessageReceipant:self.username withType:@"contact" withThumbnail:iconData withActualData:nil includingUserJid:user.jidStr andUserDisplay:user.displayName inManagedObjectContext:[self getmanagedObjectMessage] withSelfRepliedStatus:[NSNumber numberWithInt:1]];
        
        if (!messageCoreData) {
            NSLog(@"Gonderilen Contact Mesaj DB ye Eklenemedi");
        }
        
    }
    
}


-(void) sendMessage:(NSString*)messageBody toAdress:(NSString*)to withType:(NSString*)type
{
    XMPPJID *toJid = [XMPPJID jidWithString:to];
    XMPPMessage *newMessage = [[XMPPMessage alloc] initWithType:@"chat" to:toJid];
    [newMessage addSubjectToMessage:type];
    [newMessage addBodyToMessage:messageBody];
    
    XMPPElementReceipt *recepit = nil;
    [self.xmppStream sendElement:newMessage
                   andGetReceipt:&recepit];
    [recepit wait:15];
}

-(void)sendImageMessage:(UIImage *)messageImage toAdress:(NSString*)to withType:(NSString*)type
{
    
    /*XMPPJID *toJid = [XMPPJID jidWithString:to];
     
     NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
     
     [message addAttributeWithName:@"type" stringValue:@"chat"];
     
     [message addAttributeWithName:@"to" stringValue:[toJid full]];
     
     NSData *dataPic =  UIImagePNGRepresentation(messageImage);
     
     NSXMLElement *photo = [NSXMLElement elementWithName:@"PHOTO"];
     
     NSXMLElement *binval = [NSXMLElement elementWithName:@"BINVAL"];
     
     [photo addChild:binval];
     
     NSString *base64String = [dataPic base64EncodedStringWithOptions:0];
     
     [binval setStringValue:base64String];
     
     [message addChild:photo];
     [self.xmppStream sendElement:message];*/
    
    XMPPJID *toJid = [XMPPJID jidWithString:to];
    
    NSData *dataPic =  UIImagePNGRepresentation(messageImage);
    
    NSXMLElement *photo = [NSXMLElement elementWithName:@"PHOTO"];
    NSXMLElement *binval = [NSXMLElement elementWithName:@"BINVAL"];
    [photo addChild:binval];
    
    //convert image to base64String
    NSString *base64String = [dataPic base64EncodedStringWithOptions:0];
    [binval setStringValue:base64String];
    
    XMPPMessage *newMessage = [[XMPPMessage alloc] initWithType:@"chat" to:toJid elementID:nil child:photo];
    [newMessage addSubjectToMessage:type];
    //[newMessage addBodyToMessage:@""];
    
    XMPPElementReceipt *recepit = nil;
    [self.xmppStream sendElement:newMessage
                   andGetReceipt:&recepit];
    [recepit wait:15];
    
}

- (void)dealloc
{
    [self teardownStream];
}

- (NSManagedObjectContext *)getManagedObjectRoster
{
    NSAssert([NSThread isMainThread],
             @"NSManagedObjectContext is not thread safe. It must always be used on the same thread/queue");
    
    if (self.managedObjectContext_roster == nil)
    {
        self.managedObjectContext_roster = [[NSManagedObjectContext alloc] init];
        
        NSPersistentStoreCoordinator *psc = [self.xmppRosterStorage persistentStoreCoordinator];
        [self.managedObjectContext_roster setPersistentStoreCoordinator:psc];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(contextDidSave:)
                                                     name:NSManagedObjectContextDidSaveNotification
                                                   object:nil];
    }
    
    return self.managedObjectContext_roster;
}

- (NSManagedObjectContext*)getmanagedObjectMessage
{
    NSAssert([NSThread isMainThread],
             @"NSManagedObjectContext is not thread safe. It must always be used on the same thread/queue");
    
    if (!self.messageDocument) {
        NSLog(@"Managed Document Olusturuluyor");
        NSArray *documentsDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = [documentsDir objectAtIndex:0];
        NSString *filePath = [documentsPath stringByAppendingPathComponent:@"messageDocument"];
        NSURL *fileUrl = [[NSURL alloc] initFileURLWithPath:filePath];
        
        self.messageDocument = [[UIManagedDocument alloc] initWithFileURL:fileUrl];
        
        
        NSDictionary *optionsForStore = [NSDictionary dictionaryWithObjectsAndKeys:
                                         
                                         [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                                         
                                         [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
        
        [self.messageDocument setPersistentStoreOptions:optionsForStore];
        
        
        
        //Dokuman daha onceden varsa siler
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
        }
        
        
        //Dokumani olusturur
        if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            [self.messageDocument saveToURL:self.messageDocument.fileURL forSaveOperation:UIDocumentSaveForCreating completionHandler:nil];
        }
    }
    
    return self.messageDocument.managedObjectContext;
}

- (void)contextDidSave:(NSNotification *)notification
{
    NSManagedObjectContext *sender = (NSManagedObjectContext *)[notification object];
    
    if (sender != self.managedObjectContext_roster &&
        [sender persistentStoreCoordinator] == [self.managedObjectContext_roster persistentStoreCoordinator])
    {
        //DDLogVerbose(@"%@: %@ - Merging changes into managedObjectContext_roster", THIS_FILE, THIS_METHOD);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            [self.managedObjectContext_roster mergeChangesFromContextDidSaveNotification:notification];
        });
    }
}



-(void) messageIsSending:(NSNotification*)notification
{
    NSDictionary *messageContent = [notification userInfo];
    [self sendMessage:[messageContent valueForKey:@"body"] toAdress:[messageContent valueForKey:@"to"] withType:[messageContent valueForKey:@"type"]];
}

-(void) sendFileMessage:(NSString*)thumbnailUrl withActualData:(NSString*)actualDataUrl toAdress:(XMPPJID*)toUser withType:(NSString*)type
{
    XMPPMessage *newMessage = [[XMPPMessage alloc] initWithType:@"chat" to:toUser];
    [newMessage addSubjectToMessage:type];
    [newMessage addThumbNailPath:thumbnailUrl withActualDataPath:actualDataUrl];
    
    [self.xmppStream sendElement:newMessage];
}


-(void) fileIsSending:(NSNotification*)notification
{
    // NSDictionary *fileContent = [notification userInfo];
    //  [self transferFile:[fileContent valueForKey:@"filePath"] fromUser:[fileContent valueForKey:@"fromUserJid"] toUser:[fileContent valueForKey:@"toUserJid"] withType:[fileContent valueForKey:@"type"]];
}





//////////Adress Book XMPP Entegrasyonu////////////////////////

-(NSArray *) queryAllRoster
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"XMPPUserCoreDataStorageObject"];
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"displayName" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:descriptor];
    
    
    request.sortDescriptors = sortDescriptors;
    
    NSError *error;
    NSArray *matches = [[self getManagedObjectRoster] executeFetchRequest:request error:&error];
    return matches;
}

-(NSString*) convertPhoneNumberToInternational:(NSString*)originalPhone
{
    NSString *result = originalPhone;
    if (originalPhone.length == 11 && [originalPhone characterAtIndex:0] == '0')
    {
        result = [NSString stringWithFormat:@"+9%@",originalPhone]; //Burasi gercek ulke kodu ile degisecek
    }
    else if ([originalPhone rangeOfString:@")"].location > 0)
    {
        /*
         result = [originalPhone stringByReplacingOccurrencesOfString:@")" withString:@""];
         result = [result stringByReplacingOccurrencesOfString:@"(" withString:@""];
         result = [result stringByReplacingOccurrencesOfString:@" " withString:@""];
         return [self convertPhoneNumberToInternational:result];
         */
    }
    
    return result;
}




////FILE OPERATIONS////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////

-(NSString*) getActualFileNameFromThumbnail:(NSString *)thumbnailFileName
{
    NSString *result;
    
    NSString *withoutExt = [thumbnailFileName substringToIndex:[thumbnailFileName length]-14];
    result = [withoutExt stringByAppendingString:@".png"];
    
    return result;
}

-(NSString*) getThumbnailFileNameFromFileName:(NSString*)fileName {
    NSString *result;
    
    
    NSString *withoutExt = [fileName substringToIndex:[fileName length]-4];
    result = [withoutExt stringByAppendingString:@"_thumbnail.png"];
    
    return result;
}

-(void) insertActualDataToDownloadedMessage:(NSString*)bodyMessage actualFileName:(NSString*)fileName andUsername:(NSString*)username
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"XMPPMessageCoreDataObject"];
    request.predicate = [NSPredicate predicateWithFormat:@"body = %@ AND whoOwns.jidStr = %@",bodyMessage,username];
    
    NSError *error;
    NSArray *matches = [self.getmanagedObjectMessage executeFetchRequest:request error:&error];
    
    if ([matches count] > 0) {
        XMPPMessageCoreDataObject *messageCoreData = [matches lastObject];
        messageCoreData.actualData = [NSData dataWithContentsOfFile:fileName];
        
        NSLog(@"Gercek Data Kayida Eklendi");
    }
    
}

-(void) insertActualDataToUploadedMessage:(NSString*)bodyMessage withActualLocalFile:(NSString*)localFileName andActualRemoteFile:(NSString*)remoteFileName includingRemoteThumb:(NSString*)remoteThumb toUserName:(NSString*)toUser andType:(NSString*)type
{
    XMPPJID *toUserJid = [XMPPJID jidWithString:toUser];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"XMPPMessageCoreDataObject"];
    request.predicate = [NSPredicate predicateWithFormat:@"body = %@ AND whoOwns.jidStr = %@",bodyMessage,toUser];
    //NSSortDescriptor *sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"displayName" ascending:YES];
    //request.sortDescriptors = [NSArray arrayWithObject:sortDesc];
    
    NSError *error;
    NSArray *matches = [self.getmanagedObjectMessage executeFetchRequest:request error:&error];
    
    if ([matches count] > 0) {
        XMPPMessageCoreDataObject *messageCoreData = [matches lastObject];
        messageCoreData.actualData = [NSData dataWithContentsOfFile:localFileName];
        
        //NSLog(@"Sending Remote Thumbnail: %@",remoteThumb);
        NSLog(@"Sending Remote File: %@",remoteFileName);
        [self sendFileMessage:remoteThumb withActualData:remoteFileName toAdress:toUserJid withType:type];
    }
}


@end

