//
//  XMPPMessageCoreDataObject+AddMessage.h
//  SimpleChat
//
//  Created by Sanjay Chauhan on 1/12/13.
//  Copyright (c) 2013 Sanjay Chauhan. All rights reserved.
//

#import "XMPPMessageCoreDataObject.h"

@interface XMPPMessageCoreDataObject (AddMessage)
+(XMPPMessageCoreDataObject*) insertMessageWithBody:(NSString *)body andSendDate:(NSString *)sendDate andMessageReceipant:(NSString*)messageReceipant withType:(NSString*)type withThumbnail:(NSData*)thumbNail withActualData:(NSData*)actualData includingUserJid:(NSString*)jidStr andUserDisplay:(NSString*)displayName inManagedObjectContext:(NSManagedObjectContext*)context withSelfRepliedStatus:(NSNumber*)status;
@end
