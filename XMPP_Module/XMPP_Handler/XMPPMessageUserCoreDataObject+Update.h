//
//  XMPPMessageUserCoreDataObject+Update.h
//  SimpleChat
//
//  Created by Sanjay Chauhan on 22/12/13.
//  Copyright (c) 2013 Sanjay Chauhan. All rights reserved.
//

#import "XMPPMessageUserCoreDataObject.h"

@interface XMPPMessageUserCoreDataObject (Update)
+(XMPPMessageUserCoreDataObject *) messageUserWithJid:(NSString*)jidStr andDisplayName:(NSString*)displayName inManagedObjectContext:(NSManagedObjectContext*)context;
@end
