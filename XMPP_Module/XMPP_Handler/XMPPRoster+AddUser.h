//
//  XMPPRoster+AddUser.h
//  PanX
//
//  Created by Sanjay Chauhan on 12/1/14.
//  Copyright (c) 2014 Sanjay Chauhan. All rights reserved.
//

#import "XMPPRoster.h"

@interface XMPPRoster (AddUser)
- (void) addUser:(XMPPJID *)jid withNickname:(NSString *)optionalName withSubscriptionType:(NSString*)subsType;
@end
