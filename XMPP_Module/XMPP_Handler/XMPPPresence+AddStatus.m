//
//  XMPPPresence+AddStatus.m
//  SimpleChat
//
//  Created by Sanjay Chauhan on 12/1/14.
//  Copyright (c) 2014 Sanjay Chauhan. All rights reserved.
//

#import "XMPPPresence+AddStatus.h"

@implementation XMPPPresence (AddStatus)

-(void) addStatus:(NSString *)status
{
    //NSXMLElement *showElement = [NSXMLElement elementWithName:@"show" stringValue:@"online"];
    NSXMLElement *statusElement = [NSXMLElement elementWithName:@"status" stringValue:status];
    //[self addChild:showElement];
    [self addChild:statusElement];
}

@end
