//
//  XMPPHandler.h
//  PanX_iPod
//
//  Created by Sanjay Chauhan on 12/12/13.
//  Copyright (c) 2013 Sanjay Chauhan. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "XMPPFramework.h"
#import "XMPPvCardTemp.h"
#import "XMPPRoomMemoryStorage.h"
#import "XMPPPrivacy.h"

#import "XMPPMessageArchivingCoreDataStorage.h"
#import "XMPPMessageArchiving.h"

/* __unused denotes variables and functions that may not be used, preventing
 
 * the compiler from warning about it if not used.
 
 */

#define __unused    __attribute__((unused))



/* __used forces variables and functions to be included even if it appears
 
 * to the compiler that they are not used (and would thust be discarded).
 
 */

#define __used     __attribute__((used))



/* __deprecated causes the compiler to produce a warning when encountering
 
 * code using the deprecated functionality.  This may require turning on
 
 * such wardning with the -Wdeprecated flag.
 
 */

#define __deprecated    __attribute__((deprecated))



/* __unavailable causes the compiler to error out when encountering
 
 * code using the tagged function of variable.
 
 */

#define __unavailable    __attribute__((unavailable))

//NS_AVAILABLE_IOS(4_0)

typedef void (^addUserBlock) (NSError *error, NSMutableArray *userInfo);

@protocol XMPPHandlerDelegate <NSObject>

-(void) setXMPPHandlerConnectionStatus:(BOOL)status;
-(void) presenceStatusChanged:(XMPPJID*)jid withStatus:(NSString*)status;
-(void)chatSessionExpiredWithStatus:(BOOL)status;
-(void)reloadTableRows;
@end

@interface XMPPHandler : NSObject <XMPPRosterDelegate,XMPPStreamDelegate,XMPPMUCDelegate,XMPPRoomMemoryStorageDelegate,XMPPMUCDelegate,XMPPPrivacyDelegate>

@property (nonatomic,strong) NSString *username;
@property (nonatomic,strong) NSString *password;
@property (nonatomic,strong) NSString *hostname;
@property (nonatomic,assign) BOOL isXmppConnected;
//@property (nonatomic,strong) XMPPRosterCoreDataStorage *xmppRosterStorage;

@property (nonatomic,strong)NSMutableArray *mPresenceArray;


@property (nonatomic, strong) XMPPStream *xmppStream;
@property (nonatomic, strong) XMPPMessageArchivingCoreDataStorage *xmppMessageArchivingCoreDataStorage;
@property (nonatomic, strong) XMPPMessageArchiving *xmppMessageArchivingModule;
@property (nonatomic, strong) XMPPvCardTempModule *xmppvCardTempModule;
@property (nonatomic, strong) XMPPvCardAvatarModule *xmppvCardAvatarModule;
@property (nonatomic, strong) XMPPvCardCoreDataStorage *xmppvCardStorage;
@property (nonatomic, strong) XMPPRoom *xmppRoom;
@property (nonatomic, strong) XMPPPrivacy *xmppPrivacy;

@property (nonatomic,assign) id<XMPPHandlerDelegate> delegate;
@property (nonatomic,copy) NSString *retreivedAgentId;
@property (nonatomic,assign) BOOL isAgentTyping;

/** .Method Declaration to Setup the stream.
 *
 .**/
-(void) setupStream;

/** .Method Declaration to Configure the Socket.
 * . Always specify a full JID instead of Bare.
 .**/
-(void) configure;

/** .Method Declaration to connect the Socket with Host.
 * . It will pair your host and port with the Socket.
 .**/
-(BOOL) connect;

/** .Method Declaration to disconnect the socket connection.
 * .
 .**/
-(void) disconnect;


/** .Method Declaration to setup Privacy/Block User Setting.
 * . Initialize this after the stream.
 .**/
- (void)setupXMPPPrivacy __unused;

/** .Method Declaration to send Message to user.
 * . Need to send message, address of other user and the type #chat for onetoone chat.
 .**/
-(void) sendMessage:(NSString*)messageBody toAdress:(NSString*)to withType:(NSString*)type;

/** .Method Declaration to send Image Message to user.
 * . Need to send Image, address of other user and the type #chat for onetoone chat.
 .**/
-(void)sendImageMessage:(UIImage *)messageImage toAdress:(NSString*)to withType:(NSString*)type;

/** .Method Declaration to get JID of logged in User.
 * . You can get bare/full/string from this JID.
 .**/
-(XMPPJID*) getMyJid;

/** .Method Declaration to clear the Roster.
 * . Always clear Roster on Logout #Else you will see unwanted Roster.
 .**/
- (void)clearLocalRosterData;

/** .Method Declaration to TearDown Streams.
 * . Always Call this method on logout.
 .**/
- (void)teardownStream;

/** .Method Declaration to get Managed Object Context.
 * . This will provide the Context for Roster.
 .**/
- (NSManagedObjectContext *)getManagedObjectRoster;

/** .Method Declaration to get Managed Object Context.
 * . This will provide the Context for Messages.
 .**/
- (NSManagedObjectContext *)getmanagedObjectMessage;

@end

