//
//  XMPPMessageUserCoreDataObject.m
//  SimpleChat
//
//  Created by Sanjay Chauhan on 22/12/13.
//  Copyright (c) 2013 Sanjay Chauhan. All rights reserved.
//

#import "XMPPMessageUserCoreDataObject.h"
#import "XMPPMessageCoreDataObject.h"


@implementation XMPPMessageUserCoreDataObject

@dynamic displayName;
@dynamic jidStr;
@dynamic messages;

@end
