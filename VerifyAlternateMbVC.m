//
//  VerifyAlternateMbVC.m
//  Umang
//
//  Created by deepak singh rawat on 05/12/16.
//  Copyright © 2016 SpiceDigital. All rights reserved.
//

#import "VerifyAlternateMbVC.h"
#define MAX_LENGTH 6
#define kOFFSET_FOR_KEYBOARD 80.0

#import "MBProgressHUD.h"
#import <QuartzCore/QuartzCore.h>

#import "MyTextField.h"
#import "UMAPIManager.h"

#import "SecuritySettingVC.h"

#import "UserEditVC.h"
#import "AlternateMobileVC.h"


#import "UserEditVC.h"

@interface VerifyAlternateMbVC ()<UITextFieldDelegate>
{
    SharedManager *singleton;
    MBProgressHUD *hud ;
    
    int apiTag;
    int count;
    __weak IBOutlet UIButton *btnBack;

   
    IBOutlet UIButton *btn_callme;
    NSString *retryResend;
    NSString *retryCall;
    
    __weak IBOutlet UILabel *lblTitleHeader;
    __weak IBOutlet UILabel *lblheaderDescription;
    __weak IBOutlet UILabel *lblsubHeaderDescription;
    __weak IBOutlet UILabel *lblEnter6Digit;
    __weak IBOutlet UILabel *lblReceiveOTP;
    __weak IBOutlet UIButton *btnResendOTP;
    
    
    
}
@property (nonatomic, strong) IBOutlet UIProgressView *progressView;
@property (weak) NSTimer *myTimer;
@property (weak, nonatomic) IBOutlet UILabel *lb_timer;

@property (weak, nonatomic) IBOutlet UIView *vw_line;
@property (weak, nonatomic) IBOutlet UITextField *txt1;
@property (weak, nonatomic) IBOutlet UIView *vw_line1;


@property (weak, nonatomic) IBOutlet UILabel *lb_rtryResend;
@property (weak, nonatomic) IBOutlet UILabel *lb_rtryCall;

@end

@implementation VerifyAlternateMbVC
@synthesize TAGFROM;
@synthesize altmobile;

@synthesize rtry;
@synthesize tout;









- (void)updateUI:(NSTimer *)timer
{
    if (count <=0)
    {
        [self.myTimer invalidate];
        self.myTimer = nil;
        resendOTPview.hidden=FALSE;
        
        
        
        int rtryOtp=[retryResend intValue];
        int rtryCall=[retryCall intValue];
        
        if (rtryOtp <=0)
        {
            btnResendOTP.hidden=TRUE;
            self.lb_rtryResend.hidden=TRUE;
            
        }
        if (rtryCall<=0)
        {
            
            btn_callme.hidden=TRUE;
            self.lb_rtryCall.hidden=TRUE;
            
            
        }
        if (rtryOtp <=0 && rtryCall<=0)
        {
            resendOTPview.hidden=TRUE;
        }
        
        
        
        
    }
    else
    {
        
        count --;
        
        self.progressView.progress = (float)count/120.0f;
        
        self.lb_timer.text=[self timeFormatted:count];
        
    }
    
    
    
    
    
    
}


-(void)hitResendOTP:(NSString*)channelType
{
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"Wait...", @"Wait");
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:altmobile forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:channelType forKey:@"chnl"]; //chnl : type sms for OTP and IVR for call
    
    
    [dictBody setObject:@"" forKey:@"lang"]; //lang
    
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];  //get from mobile contact //not supported iphone
    [dictBody setObject:@"altmno" forKey:@"ort"];  //Type for which OTP to be intiate eg register,login,forgot mpin
    
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];  //Type for which OTP to be intiate eg register,login,forgot mpin //valtmno
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_IVR_OTP withBody:dictBody andTag:TAG_REQUEST_IVR_OTP completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        
        [hud hideAnimated:YES];
        // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
        
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            //[self openNextView];
            //------ Sharding Logic parsing---------------
            NSString *node=[response valueForKey:@"node"];
            if([node length]>0)
            {
                [[NSUserDefaults standardUserDefaults] setValue:node forKey:@"NODE_KEY"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            

            //------ Sharding Logic parsing---------------
            
            

            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
            //  NSString *rc=[response valueForKey:@"rc"];
            //  NSString *rd=[response valueForKey:@"rd"];
            //  NSString *rs=[response valueForKey:@"rs"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                //[self alertwithMsg:rd];
                // [self hitUpdateAlterMob];
                // man=[[response valueForKey:@"pd"] valueForKey:@"man"];
                rtry=[[response valueForKey:@"pd"] valueForKey:@"rtry"];
                // tmsg=[[response valueForKey:@"pd"] valueForKey:@"tmsg"];
                tout=[[[response valueForKey:@"pd"] valueForKey:@"tout"] intValue];
                
                count =tout;// count++;
                
                
                NSArray *retryItems = [rtry componentsSeparatedByString:@"|"];
                
                retryResend=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:0]];
                retryCall=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:1]];
                
                btnResendOTP.enabled=TRUE;
                btn_callme.enabled=TRUE;
                
                
                NSString *strRetryValue = NSLocalizedString(@"attempts_left", nil);
                
                self.lb_rtryResend.text= [NSString stringWithFormat:@"(%@ %@)",retryResend,strRetryValue];
                
                
                self.lb_rtryCall.text= [NSString stringWithFormat:@"(%@ %@)",retryCall,strRetryValue];
                
                
                int rtryOtp=[retryResend intValue];
                int rtryCall=[retryCall intValue];
                
                if (rtryOtp <=0)
                {
                    btnResendOTP.hidden=TRUE;
                    self.lb_rtryResend.hidden=TRUE;
                    
                }
                if (rtryCall<=0)
                {
                    
                    btn_callme.hidden=TRUE;
                    self.lb_rtryCall.hidden=TRUE;
                    
                    
                }
                if (rtryOtp <=0 && rtryCall<=0)
                {
                    resendOTPview.hidden=TRUE;
                }
                
                
                
                
                
                
                
                count =tout;// count++
                
                
                
                // wmsg=[[response valueForKey:@"pd"] valueForKey:@"wmsg"];
                
                if ( [self.myTimer isValid]){
                    [self.myTimer invalidate], self.myTimer=nil;
                }
                
                self.myTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateUI:) userInfo:nil repeats:YES];
                [[NSRunLoop mainRunLoop] addTimer:_myTimer forMode:NSRunLoopCommonModes];
                
                
            }
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}



-(IBAction)btn_resendAction:(id)sender
{
    [self hitResendOTP:@"sms"];
    resendOTPview.hidden=TRUE;
    
}


-(IBAction)btn_callmeAction:(id)sender
{
    // UM_API_IVR_OTP
    [self hitResendOTP:@"ivr"];
    resendOTPview.hidden=TRUE;
    
}

-(IBAction)bgtouch:(id)sender
{
    
    [_txt1 resignFirstResponder];
    
    
}

-(void)setFontforView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       if ([view isKindOfClass:[UITextField class]])
                       {
                           
                           UITextField *txtfield = (UITextField *)view;
                           NSString *fonttxtFieldName = txtfield.font.fontName;
                           CGFloat fonttxtsize =txtfield.font.pointSize;
                           txtfield.font = nil;
                           
                           txtfield.font = [UIFont fontWithName:fonttxtFieldName size:fonttxtsize];
                           
                           [txtfield layoutIfNeeded]; //Fixes iOS 9 text bounce glitch
                       }
                       
                       
                   });
    
    if ([view isKindOfClass:[UITextView class]])
    {
        
        UITextView *txtview = (UITextView *)view;
        NSString *fonttxtviewName = txtview.font.fontName;
        CGFloat fontbtnsize =txtview.font.pointSize;
        
        txtview.font = [UIFont fontWithName:fonttxtviewName size:fontbtnsize];
        
    }
    
    
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        NSString *fontName = lbl.font.fontName;
        CGFloat fontSize = lbl.font.pointSize;
        
        lbl.font = [UIFont fontWithName:fontName size:fontSize];
    }
    
    
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *button = (UIButton *)view;
        NSString *fontbtnName = button.titleLabel.font.fontName;
        CGFloat fontbtnsize = button.titleLabel.font.pointSize;
        
        [button.titleLabel setFont: [UIFont fontWithName:fontbtnName size:fontbtnsize]];
    }
    
    if (isSubViews)
    {
        
        for (UIView *sview in view.subviews)
        {
            [self setFontforView:sview andSubViews:YES];
        }
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    //——————— Add to handle portrait mode only———
    /*[[UIDevice currentDevice]performSelector:@selector(setOrientation:) withObject:(__bridge id)((void *)UIInterfaceOrientationPortrait)];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.shouldRotate = NO;
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait|UIInterfaceOrientationPortraitUpsideDown animated:NO];*/
    //——————— Add to handle portrait mode only———
    //———— Add to handle network bar of offline——
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"NOTABBAR" forKey:@"CLASSTYPE"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"NETWORKBARCHECK" object:nil userInfo:userInfo];
    //———— Add to handle network bar of offline——

    
    NSArray *retryItems = [rtry componentsSeparatedByString:@"|"];
    
    retryResend=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:0]];
    retryCall=[NSString stringWithFormat:@"%@",[retryItems objectAtIndex:1]];
    
        
    
    NSString *strRetryValue = NSLocalizedString(@"attempts_left", nil);
    
    self.lb_rtryResend.text= [NSString stringWithFormat:@"(%@ %@)",[retryItems objectAtIndex:0],strRetryValue];
    
    
    self.lb_rtryCall.text= [NSString stringWithFormat:@"(%@ %@)",[retryItems objectAtIndex:1],strRetryValue];
    
    count =tout;// count++;
    
    resendOTPview.hidden=TRUE;
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       CGRect contentRect = CGRectZero;
                       for (UIView *view in scrollview.subviews)
                           contentRect = CGRectUnion(contentRect, view.frame);
                       
                       contentRect.size.height=contentRect.size.height+100;
                       scrollview.contentSize = contentRect.size;
                   });
    
    
    
    if ( [self.myTimer isValid]){
        [self.myTimer invalidate], self.myTimer=nil;
    }
    
    self.myTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateUI:) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:_myTimer forMode:NSRunLoopCommonModes];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [self setViewFont];
    [super viewWillAppear:NO];
}
#pragma mark- Font Set to View
-(void)setViewFont{
    [btnBack.titleLabel setFont:[AppFont regularFont:17.0]];
    //[_btnSkip.titleLabel setFont:[AppFont mediumFont:18.0]];
    lblTitleHeader.font = [AppFont semiBoldFont:22.0];
    _lblScreenTitleName.font = [AppFont semiBoldFont:22.0];
    lblheaderDescription.font = [AppFont semiBoldFont:17.0];
    // _maskedMobileLabel.font = [AppFont regularFont:18.0];
    lblsubHeaderDescription.font = [AppFont mediumFont:14.0];
    _txt1.font = [AppFont regularFont:21.0];
    lblEnter6Digit.font = [AppFont mediumFont:14.0];
    _lb_timer.font = [AppFont mediumFont:14];
    // lblWaitingSMS.font = [AppFont mediumFont:14];
    lblReceiveOTP.font = [AppFont regularFont:16.0];
    btnResendOTP.titleLabel.font = [AppFont mediumFont:15.0];
    btn_callme.titleLabel.font = [AppFont mediumFont:15.0];
    _lb_rtryResend.font = [AppFont regularFont:13.0];
    _lb_rtryCall.font = [AppFont regularFont:13.0];
    
    
    //[_btn_next.titleLabel setFont:[AppFont mediumFont:19.0]];
}
- (void)viewDidLoad {
    
    [self.btnNext setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [self.btnNext setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btnNext.layer.cornerRadius = 3.0f;
    self.btnNext.clipsToBounds = YES;
    [self enableBtnNext:NO];
    //Google Tracking
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value: VERIFY_ALTERNATE_MOBILE_SCREEN];
    
    [[GAI sharedInstance].defaultTracker send:
     [[GAIDictionaryBuilder createScreenView] build]];
    

    apiTag=0;
    btnResendOTP.alpha = 1.0;
    btn_callme.alpha = 1.0;
    
    
    lblTitleHeader.text = NSLocalizedString(@"verify_using_otp", nil);
    lblheaderDescription.text = NSLocalizedString(@"register_verify_otp_heading", nil);
    lblsubHeaderDescription.text = NSLocalizedString(@"register_verify_otp_sub_heading", nil);
    lblEnter6Digit.text = NSLocalizedString(@"enter_6_digit_otp", nil);
  
   
    lblReceiveOTP.text = NSLocalizedString(@"didnt_receive_otp", nil);
    [btn_callme setTitle:NSLocalizedString(@"call_me", nil) forState:UIControlStateNormal];
    [btnResendOTP setTitle:NSLocalizedString(@"resend_otp", nil) forState:UIControlStateNormal];
    
    
    
    singleton = [SharedManager sharedSingleton];
    //----- Setting delegate for Custom textfield so back space operation work smooth
    _txt1.delegate = self;
    
    
   // self.vw_line.frame=CGRectMake(53, 492, 268, 0.5);
    self.view.userInteractionEnabled = YES;
    
    
    
    
    [self textfieldInit];
    
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=true;
    [self setNeedsStatusBarAppearanceUpdate];
    
    //------ Add dismiss keyboard while background touch-------
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [scrollview addGestureRecognizer:tapGesture];
    
    
    
}
-(void)hideKeyboard
{
    [scrollview setContentOffset:
     CGPointMake(0, -scrollview.contentInset.top) animated:YES];
    
    [self.view endEditing:YES];
}


- (void) viewDidDisappear:(BOOL)animated
{
    [self.myTimer invalidate];
    self.myTimer = nil;
    
    [super viewDidDisappear:NO];
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}






-(IBAction)backbtnAction:(id)sender
{
    // [self dismissViewControllerAnimated:NO completion:nil];
    
    singleton.altermobileNumber=@"";
    [self closeview];
}


- (NSString *)timeFormatted:(int)totalSeconds{
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    // int hours = totalSeconds / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
}



-(void)textfieldInit
{
    [_txt1 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}
#pragma mark -
#pragma mark UITextFieldDelegate methods
- (void)textFieldDidChange:(UITextField *)textField
{
    
    if (textField.text.length >= MAX_LENGTH)
    {
        textField.text = [textField.text substringToIndex:MAX_LENGTH];
        [textField resignFirstResponder];
        
        [self checkValidation];
    }
    
}

-(void) textFieldDidEndEditing:(UITextField *)textField
{
    [self setFontforView:self.view andSubViews:YES];
}

-(void)enableBtnNext:(BOOL)status
{
    self.btnNext.userInteractionEnabled = status;

    if (status ==YES)
    {
        [self hideKeyboard];
        [self.btnNext setBackgroundColor:[UIColor colorWithRed:84.0/255.0f green:185.0/255.0f blue:105.0/255.0f alpha:1.0f]];
    }
    else
    {
        [self.btnNext setBackgroundColor:[UIColor colorWithRed:176.0/255.0f green:176.0/255.0f blue:176.0/255.0f alpha:1.0f]];
    }
    
}

- (IBAction)didTapNextBtnAction:(UIButton *)sender {
    if (_txt1.text.length < 6)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"enter_otp_is_not_valid", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
        [alert show];
        
        
    }
    else
    {
        [self hitAlternateAPI]; //case of altenate mobile number
        
        [self.myTimer invalidate];
        self.myTimer = nil;
        
        
    }
}

-(void)checkValidation
{
    [self enableBtnNext:YES];
    return;
    if (_txt1.text.length < 6)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"enter_otp_is_not_valid", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil];
        [alert show];
        
        
    }
    else
    {
        [self hitAlternateAPI]; //case of altenate mobile number
        
        [self.myTimer invalidate];
        self.myTimer = nil;
        
        
    }
}



-(void)hitAlternateAPI
{
    apiTag=1;
    
    
    NSString *otpString=[NSString stringWithFormat:@"%@",_txt1.text];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Set the label text.
   hud.label.text = NSLocalizedString(@"loading",nil);
    // You can also adjust other label properties if needed.
    // hud.label.font = [UIFont italicSystemFontOfSize:16.f];
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:altmobile forKey:@"mno"];//Enter mobile number of user
    [dictBody setObject:otpString forKey:@"otp"];//Enter OTP number by user
    [dictBody setObject:@"sms" forKey:@"chnl"]; //chnl : type sms for OTP and IVR for call
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile contact //not supported iphone
    [dictBody setObject:@"altmno" forKey:@"ort"];  //Type for which OTP to be intiate eg register,login,forgot mpin
    
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];  //Type for which OTP to be intiate eg register,login,forgot mpin //valtmno
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_VALIDATE_OTP withBody:dictBody andTag:TAG_REQUEST_ALTERNATE_VALIDATE_OTP completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        
        [hud hideAnimated:YES];
        // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
        
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            //[self openNextView];
            
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
//            NSString *rc=[response valueForKey:@"rc"];
            NSString *rd=[response valueForKey:@"rd"];
//            NSString *rs=[response valueForKey:@"rs"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                //[self alertwithMsg:rd];
                //new changes for alternate mobile number from 
                 if ([TAGFROM isEqualToString:@"ISFROMREGISTRATION"])
                 {
                    //
                     if (altmobile.length!=0)
                     {
                         singleton.altermobileNumber=altmobile;

                     }
                     [self alertwithMsg:rd];

                }
               else
               {
                   singleton.altermobileNumber=altmobile;
                   [self closeview];
                   //[self hitUpdateAlterMob];
               }
                
            }
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        
    }];
    
}




//----- hitAPI for IVR OTP call Type registration ------
-(void)hitUpdateAlterMob
{
    
    apiTag=2;
    
    NSString *otpString=[NSString stringWithFormat:@"%@",_txt1.text];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Set the label text.
  hud.label.text = NSLocalizedString(@"loading",nil);
    // You can also adjust other label properties if needed.
    // hud.label.font = [UIFont italicSystemFontOfSize:16.f];
    
    
    UMAPIManager *objRequest = [[UMAPIManager alloc] init];
    
    
    NSMutableDictionary *dictBody = [NSMutableDictionary new];
    [dictBody setObject:altmobile forKey:@"amno"];//Enter mobile number of user
    [dictBody setObject:otpString forKey:@"otp"];//Enter OTP number by user
    [dictBody setObject:@"sms" forKey:@"chnl"]; //chnl : type sms for OTP and IVR for call
    [dictBody setObject:@"" forKey:@"peml"];  //get from mobile contact //not supported iphone
    [dictBody setObject:@"altmno" forKey:@"ort"];  //Type for which OTP to be intiate eg register,login,forgot mpin
    
    [dictBody setObject:singleton.user_tkn forKey:@"tkn"];  //Type for which OTP to be intiate eg register,login,forgot mpin //valtmno
    
    
    [objRequest hitWebServiceAPIWithPostMethod:YES isAccessTokenRequired:NO webServiceURL:UM_API_UPDATEALTERNATEMOBILE withBody:dictBody andTag:TAG_REQUEST_UPDATEALTERNATEMOBILE completionHandler:^(id response, NSError *error, REQUEST_TAG tag) {
        
        
        [hud hideAnimated:YES];
        // [self performSelector:@selector(checkValidation) withObject:nil afterDelay:3.f];
        
        
        if (error == nil) {
            NSLog(@"Server Response = %@",response);
            //[self openNextView];
            singleton.altermobileNumber=altmobile;
            //----- below value need to be forword to next view according to requirement after checking Android apk-----
            
     //       NSString *rc=[response valueForKey:@"rc"];
            NSString *rd=[response valueForKey:@"rd"];
     //       NSString *rs=[response valueForKey:@"rs"];
            
            if ([[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE]||[[response objectForKey:@"rs"] isEqualToString:API_SUCCESS_CASE1])
            {
                
                [self alertwithMsg:rd];
            }
            //----- End value need to be forword to next view according to requirement after checking Android apk-----
            
            
        }
        else{
            NSLog(@"Error Occured = %@",error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil)
                                                            message:error.localizedDescription
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            singleton.altermobileNumber=@"";
            
            //singleton.altermobileNumber=@"";
        }
        
    }];
}






-(void)alertwithMsg:(NSString*)msg
{
    [self closeview];
    
    
    /*   UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success!" message:msg preferredStyle:UIAlertControllerStyleAlert];
     UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
     
     {
     
     [self closeview];
     
     }];
     [alert addAction:cancelAction];
     [self presentViewController:alert animated:YES completion:nil];*/
}

/*
 #import "SecuritySettingVC.h"
 #import "RegStep5ViewController.h"
 #import "MyProfileVC.h"
 
 = 100,
 ,
 ,
 */


-(void)closeview
{
    if (self.myTimer)
    {
        
        [self.myTimer invalidate];
        self.myTimer = nil;
    }
    
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    
    
    if ([TAGFROM isEqualToString:@"ISFROMPROFILEUPDATE"]) {
        for (UIViewController *aViewController in allViewControllers) {
            if ([aViewController isKindOfClass:[UserEditVC class]]) {
                [self.navigationController popToViewController:aViewController animated:NO];
            }
        }
        
    }
    if ([TAGFROM isEqualToString:@"ISFROMREGISTRATION"])
    {
        
        
        // [self dismissViewControllerAnimated:NO completion:nil];
        // [self dismissViewControllerAnimated:YES completion:nil];
        [self startNotifierIsFromNotify];
        
        [self.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        
        
        // [self.presentingViewController.presentingViewController dismissModalViewControllerAnimated:YES];
        
        
        
    }
    
    
    
    if ([TAGFROM isEqualToString:@"ISFROMSETTINGS"]) {
        for (UIViewController *aViewController in allViewControllers) {
            if ([aViewController isKindOfClass:[SecuritySettingVC class]]) {
                [self.navigationController popToViewController:aViewController animated:NO];
            }
        }
        
    }
    
    
    
    
}


- (void) startNotifierIsFromNotify
{
    // All instances of TestClass will be notified
    [[NSNotificationCenter defaultCenter] postNotificationName:@"STARTNOTIFIERREGIS" object:nil];
    
    
    
}





- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch * touch = [touches anyObject];
    if(touch.phase == UITouchPhaseBegan) {
        [self.txt1 resignFirstResponder];
              
    }
}


#if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_9_0
#define supportedInterfaceOrientationsReturnType NSUInteger
#else
#define supportedInterfaceOrientationsReturnType UIInterfaceOrientationMask
#endif

- (supportedInterfaceOrientationsReturnType)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

/*
#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    // Return a bitmask of supported orientations. If you need more,
    // use bitwise or (see the commented return).
    return UIInterfaceOrientationMaskPortrait;
    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}
*/

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
