//
//  FavFilterResultsVC.h
//  Umang
//
//  Created by admin on 21/01/17.
//  Copyright © 2017 SpiceDigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterNoResultView.h"
@protocol filterChangeDelegate <NSObject>

- (void)filterChanged:(NSMutableArray*)parameters andCategoryDict:(NSMutableDictionary *)dict;

@end

@interface FavFilterResultsVC : UIViewController
{
    IBOutlet UIView *vw_noresults;
    IBOutlet UILabel *lb_noresults;
    FilterNoResultView *noResultsVW;
    
}

@property (nonatomic, weak) id<filterChangeDelegate> delegate;
@property (nonatomic,strong) NSMutableArray *filterBOArray;

@property (weak, nonatomic) IBOutlet UITableView *tblSearchFilter;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnFilter;

@property(nonatomic,assign) BOOL isFromHomeFilter;
@property (weak, nonatomic) IBOutlet UIButton *btnSort;
@property (weak, nonatomic) IBOutlet UIButton *btnSettingAgain;

@property (nonatomic,copy) NSString *comingFromFilter;

- (IBAction)btnSettingAgainClicked:(id)sender;

@property(nonatomic,strong)NSMutableDictionary *dictFilterParams;

@property(nonatomic,strong)NSMutableArray *arrFilterResponse;

@property(nonatomic,assign) NSInteger isFromIndexFilter;


- (IBAction)btnSortActionClicked:(UIButton *)sender;

- (IBAction)btnFilterClicked:(id)sender;

-(IBAction)btnBackClicked:(id)sender;


@end

